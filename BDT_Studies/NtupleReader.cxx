#include <vector>
#include <cmath>
#include <string>
#include <iostream>
#include "TROOT.h"
#include "TChain.h"
#include "TH1F.h"
#include "TFile.h"
#include "TH2F.h"
#include "TMVA/Factory.h"
#include "TMVA/Tools.h"
#include "TMVA/Reader.h"
using namespace std;

void NtupleReader(TString in,
		  TString out,
		  TString wgtdir,
		  TString multiVar,
		  TString type, 
		  TString name,
		  int isZJ,
		  double low_m4l_range,
		  int useMw){
  
  TH1::SetDefaultSumw2(kTRUE);
  TH2::SetDefaultSumw2(kTRUE);

  
  TH1F *bkg_MLPBNN       = new  TH1F("bkg_MLPBNN"      ,"",80,-2,2);
  TH1F *bkg_BDT          = new  TH1F("bkg_BDT"         ,"",80,-2,2);
  TH1F *bkg_mass         = new  TH1F("bkg_mass"        ,"",25,100000,150000);
  TH1F *bkg_massMLP      = new  TH1F("bkg_massMLP"     ,"",25,100000,150000);
  TH1F *bkg_massBDT      = new  TH1F("bkg_massBDT"     ,"",25,100000,150000);
  TH2F *bkg_MLPBNNvsmass = new  TH2F("bkg_MLPBNNvsmass","",50,100000,150000,80,-2,2); 
  TH2F *bkg_BDTvsmass    = new  TH2F("bkg_BDTvsmass"   ,"",50,100000,150000,80,-2,2);      


  TH1F *sig_MLPBNN       = new  TH1F("sig_MLPBNN"      ,"",80,-2,2);
  TH1F *sig_BDT          = new  TH1F("sig_BDT"         ,"",80,-2,2);
  TH1F *sig_mass         = new  TH1F("sig_mass"        ,"",25,100000,150000);
  TH1F *sig_massMLP      = new  TH1F("sig_massMLP"     ,"",25,100000,150000);
  TH1F *sig_massBDT      = new  TH1F("sig_massBDT"     ,"",25,100000,150000);
  TH2F *sig_MLPBNNvsmass = new  TH2F("sig_MLPBNNvsmass","",50,100000,150000,80,-2,2);
  TH2F *sig_BDTvsmass    = new  TH2F("sig_BDTvsmass"   ,"",50,100000,150000,80,-2,2);      


  TH1F *BDT4mu   = new TH1F(name+"_4mu",""  ,10,-1,1);
  TH1F *BDT4e    = new TH1F(name+"_4e",""   ,10,-1,1);
  TH1F *BDT2e2mu = new TH1F(name+"_2e2mu","",10,-1,1);
  TH1F *BDT2mu2e = new TH1F(name+"_2mu2e","",10,-1,1);

  
  TH2F *BDT4mu2d   = new TH2F(name+"_4mu2d",""  ,10,-1,1,15,low_m4l_range,130000);
  TH2F *BDT4e2d    = new TH2F(name+"_4e2d",""   ,10,-1,1,15,low_m4l_range,130000);
  TH2F *BDT2e2mu2d = new TH2F(name+"_2e2mu2d","",10,-1,1,15,low_m4l_range,130000);
  TH2F *BDT2mu2e2d = new TH2F(name+"_2mu2e2d","",10,-1,1,15,low_m4l_range,130000);

  TChain* sig = new TChain("tmva","treeMVA"); 
  
  
  sig->Add(in);
   
  float decayPhi;
  float prodPhi1;
  float prodCosThetaStar;
  float decayCosTheta1;
  float decayCosTheta2;
  float mZ1;
  float mZ2;
  float eta;
  float pt;
  float m4l;
  int   quadType;
  float weight;
  float m4l_constr;
  float me_0p;

  sig->SetBranchAddress("decayPhi"        ,  &decayPhi	             );  
  sig->SetBranchAddress("prodPhi1"        ,   &prodPhi1		     );
  sig->SetBranchAddress("prodCosThetaStar",   &prodCosThetaStar   );
  sig->SetBranchAddress("decayCosTheta1"  ,   &decayCosTheta1     );	
  sig->SetBranchAddress("decayCosTheta2"  ,   &decayCosTheta2     );	
  sig->SetBranchAddress("mZ1"  ,      &mZ1			     );
  sig->SetBranchAddress("mZ2"  ,      &mZ2			     );
  sig->SetBranchAddress("eta"  ,      &eta			     );
  sig->SetBranchAddress("pt"   ,      &pt			     );
  sig->SetBranchAddress("m4l"  ,      &m4l                           );  
  sig->SetBranchAddress("quadType", &quadType);
  sig->SetBranchAddress("weight", &weight);
  sig->SetBranchAddress("m4l_constr"  , &m4l_constr                           );
  sig->SetBranchAddress("me_0p"  ,      &me_0p                           );
  sig->SetBranchStatus("*",1);
  
  double outBDTG,outBDTG_all,outMLPNN,outM4l,outQuadType,outWeight, outM4l_constr;
  TFile *f1 = TFile::Open( out+"_"+type+".root","RECREATE");

   TTree *t = new TTree("outMVA","");
   t->Branch("outBDTG" ,  &outBDTG );
   t->Branch("outBDTG_all" ,  &outBDTG_all );
   t->Branch("outMLPNN",  &outMLPNN );
   t->Branch("outM4l",  &outM4l );
   t->Branch("outQuadType",&outQuadType);
   t->Branch("outWeight",&outWeight);
   t->Branch("outM4l_constr",  &outM4l_constr );


  TMVA::Reader* reader = new TMVA::Reader( "" );
  //  reader->AddVariables();
  if(multiVar=="RemoveOneVar" || multiVar=="TestOtherMasses" ){
    
    if(type!="WithoutPhi")
      reader->AddVariable( "decayPhi"         , &decayPhi         );
    if(type!="WithoutPhi1")
      reader->AddVariable( "prodPhi1"         , &prodPhi1         );
    if(type!="WithoutCosThetaStar")
      reader->AddVariable( "prodCosThetaStar" , &prodCosThetaStar );
    if(type!="WithoutCosTheta1")
      reader->AddVariable( "decayCosTheta1"   , &decayCosTheta1   );
    if(type!="WithoutCosTheta2")
      reader->AddVariable( "decayCosTheta2"   , &decayCosTheta2   );
    if(type!="WithoutMZ1")
      reader->AddVariable( "mZ1"              , &mZ1              );
    if(type!="WithoutMZ2")
      reader->AddVariable( "mZ2"              , &mZ2              );
    if(type!="WithoutEta")
      reader->AddVariable( "eta"              , &eta              );
    if(type!="WithoutPt")
      reader->AddVariable( "pt"               , &pt               );
    
  }
  else if(multiVar=="RemoveMz2PlusOne"){
    
    if(type!="WithoutPhi")
      reader->AddVariable( "decayPhi"         , &decayPhi         );
    if(type!="WithoutPhi1")
      reader->AddVariable( "prodPhi1"         , &prodPhi1         );
    if(type!="WithoutCosThetaStar")
      reader->AddVariable( "prodCosThetaStar" , &prodCosThetaStar );
    if(type!="WithoutCosTheta1")
      reader->AddVariable( "decayCosTheta1"   , &decayCosTheta1   );
    if(type!="WithoutCosTheta2")
      reader->AddVariable( "decayCosTheta2"   , &decayCosTheta2   );
    if(type!="WithoutMZ1")
      reader->AddVariable( "mZ1"              , &mZ1              );
    if(type!="WithoutEta")
      reader->AddVariable( "eta"              , &eta              );
    if(type!="WithoutPt")
      reader->AddVariable( "pt"               , &pt               );
    
  }
  else if(multiVar=="RemoveMz2PtPlusOne"){

    if(type!="WithoutPhi")
      reader->AddVariable( "decayPhi"         , &decayPhi         );
    if(type!="WithoutPhi1")
      reader->AddVariable( "prodPhi1"         , &prodPhi1         );
    if(type!="WithoutCosThetaStar")
      reader->AddVariable( "prodCosThetaStar" , &prodCosThetaStar );
    if(type!="WithoutCosTheta1")
      reader->AddVariable( "decayCosTheta1"   , &decayCosTheta1   );
    if(type!="WithoutCosTheta2")
      reader->AddVariable( "decayCosTheta2"   , &decayCosTheta2   );
    if(type!="WithoutMZ1")
      reader->AddVariable( "mZ1"              , &mZ1              );
    if(type!="WithoutEta")
      reader->AddVariable( "eta"              , &eta              );
    
  }
  else if(multiVar=="RemoveMz2PtEtaPlusOne"){

    if(type!="WithoutPhi")
      reader->AddVariable( "decayPhi"         , &decayPhi         );
    if(type!="WithoutPhi1")
      reader->AddVariable( "prodPhi1"         , &prodPhi1         );
    if(type!="WithoutCosThetaStar")
      reader->AddVariable( "prodCosThetaStar" , &prodCosThetaStar );
    if(type!="WithoutCosTheta1")
      reader->AddVariable( "decayCosTheta1"   , &decayCosTheta1   );
    if(type!="WithoutCosTheta2")
      reader->AddVariable( "decayCosTheta2"   , &decayCosTheta2   );
    if(type!="WithoutMZ1")
      reader->AddVariable( "mZ1"              , &mZ1              );
    
  }
  else if(multiVar=="RemoveMz2PtEtaMz1PlusOne"){
    
    if(type!="WithoutPhi")
      reader->AddVariable( "decayPhi"         , &decayPhi         );
    if(type!="WithoutPhi1")
      reader->AddVariable( "prodPhi1"         , &prodPhi1         );
    if(type!="WithoutCosThetaStar")
      reader->AddVariable( "prodCosThetaStar" , &prodCosThetaStar );
    if(type!="WithoutCosTheta1")
      reader->AddVariable( "decayCosTheta1"   , &decayCosTheta1   );
    if(type!="WithoutCosTheta2")
      reader->AddVariable( "decayCosTheta2"   , &decayCosTheta2   );
        
  }
  else if(multiVar=="RemoveMz2PtEtaMz1CosTheta1PlusOne"){
    
    if(type!="WithoutPhi")
      reader->AddVariable( "decayPhi"         , &decayPhi         );
    if(type!="WithoutPhi1")
      reader->AddVariable( "prodPhi1"         , &prodPhi1         );
    if(type!="WithoutCosThetaStar")
      reader->AddVariable( "prodCosThetaStar" , &prodCosThetaStar );
    if(type!="WithoutCosTheta2")
      reader->AddVariable( "decayCosTheta2"   , &decayCosTheta2   );
    
  }
  else if(multiVar=="RemoveMz2PtEtaMz1CosTheta1Phi1PlusOne"){
    
    if(type!="WithoutPhi")
      reader->AddVariable( "decayPhi"         , &decayPhi         );
    if(type!="WithoutCosThetaStar")
      reader->AddVariable( "prodCosThetaStar" , &prodCosThetaStar );
    if(type!="WithoutCosTheta2")
      reader->AddVariable( "decayCosTheta2"   , &decayCosTheta2   );
    
  }
  else if(multiVar=="RemoveMz2PtEtaMz1CosTheta1Phi1CosThetaStarPlusOne"){
    
    if(type!="WithoutPhi")
      reader->AddVariable( "decayPhi"         , &decayPhi         );
    if(type!="WithoutCosTheta2")
      reader->AddVariable( "decayCosTheta2"   , &decayCosTheta2   );
    
  }
  else if(multiVar=="RemovePhiPlusOne"){
      
    if(type!="WithoutPhi1")
      reader->AddVariable( "prodPhi1"         , &prodPhi1         );
    if(type!="WithoutCosThetaStar")
      reader->AddVariable( "prodCosThetaStar" , &prodCosThetaStar );
    if(type!="WithoutCosTheta1")
      reader->AddVariable( "decayCosTheta1"   , &decayCosTheta1   );
    if(type!="WithoutCosTheta2")
      reader->AddVariable( "decayCosTheta2"   , &decayCosTheta2   );
    if(type!="WithoutMZ1")
      reader->AddVariable( "mZ1"              , &mZ1              );
    if(type!="WithoutEta")
      reader->AddVariable( "eta"              , &eta              );
    if(type!="WithoutPt")
      reader->AddVariable( "pt"               , &pt               );
    if(type!="WithoutMZ2")
      reader->AddVariable( "mZ2"              , &mZ2              );
    
  }
  else if(multiVar=="RemovePhiCosTheta2PlusOne"){
    
    if(type!="WithoutPhi1")
      reader->AddVariable( "prodPhi1"         , &prodPhi1         );
    if(type!="WithoutCosThetaStar")
      reader->AddVariable( "prodCosThetaStar" , &prodCosThetaStar );
    if(type!="WithoutCosTheta1")
      reader->AddVariable( "decayCosTheta1"   , &decayCosTheta1   );
    if(type!="WithoutMZ1")
      reader->AddVariable( "mZ1"              , &mZ1              );
    if(type!="WithoutEta")
      reader->AddVariable( "eta"              , &eta              );
    if(type!="WithoutPt")
      reader->AddVariable( "pt"               , &pt               );
    if(type!="WithoutMZ2")
      reader->AddVariable( "mZ2"              , &mZ2              );
    
  }
  else if(multiVar=="RemovePhiCosTheta2CosThetaStarPlusOne"){
    
    if(type!="WithoutPhi1")
      reader->AddVariable( "prodPhi1"         , &prodPhi1         );
    if(type!="WithoutCosTheta1")
      reader->AddVariable( "decayCosTheta1"   , &decayCosTheta1   );
    if(type!="WithoutMZ1")
      reader->AddVariable( "mZ1"              , &mZ1              );
    if(type!="WithoutEta")
      reader->AddVariable( "eta"              , &eta              );
    if(type!="WithoutPt")
      reader->AddVariable( "pt"               , &pt               );
    if(type!="WithoutMZ2")
      reader->AddVariable( "mZ2"              , &mZ2              );
    
  }
  else if(multiVar=="RemovePhiCosTheta2CosThetaStarPhi1PlusOne"){
    
    if(type!="WithoutCosTheta1")
      reader->AddVariable( "decayCosTheta1"   , &decayCosTheta1   );
    if(type!="WithoutMZ1")
      reader->AddVariable( "mZ1"              , &mZ1              );
    if(type!="WithoutEta")
      reader->AddVariable( "eta"              , &eta              );
    if(type!="WithoutPt")
      reader->AddVariable( "pt"               , &pt               );
    if(type!="WithoutMZ2")
      reader->AddVariable( "mZ2"              , &mZ2              );
    
  }
  else if(multiVar=="RemovePhiCosTheta2CosThetaStarPhi1CosTheta1PlusOne"){
    
    if(type!="WithoutMZ1")
      reader->AddVariable( "mZ1"              , &mZ1              );
    if(type!="WithoutEta")
      reader->AddVariable( "eta"              , &eta              );
    if(type!="WithoutPt")
      reader->AddVariable( "pt"               , &pt               );
    if(type!="WithoutMZ2")
      reader->AddVariable( "mZ2"              , &mZ2              );
    
  }
  else if(multiVar=="RemovePhiCosTheta2CosThetaStarPhi1CosTheta1Mz1PlusOne"){
    
    if(type!="WithoutEta")
      reader->AddVariable( "eta"              , &eta              );
    if(type!="WithoutPt")
      reader->AddVariable( "pt"               , &pt               );
    if(type!="WithoutMZ2")
      reader->AddVariable( "mZ2"              , &mZ2              );
    
  }
  
  //reader->AddVariable( "me_0p"            , &me_0p            );
  reader->AddSpectator( "m4l"      ,&m4l                      );  
  reader->AddSpectator( "m4l_constr"      ,&m4l_constr        );          
  reader->AddSpectator( "quadType"      ,&quadType        );

  reader->BookMVA( "BDTG_4mu",   wgtdir+"/weights_4mu/TMVA_training_BDTG.weights.xml" );
  reader->BookMVA( "BDTG_4e" ,   wgtdir+"/weights_4e/TMVA_training_BDTG.weights.xml" );
  reader->BookMVA( "BDTG_2e2mu", wgtdir+"/weights_2e2mu/TMVA_training_BDTG.weights.xml" );
  reader->BookMVA( "BDTG_2mu2e", wgtdir+"/weights_2mu2e/TMVA_training_BDTG.weights.xml" );
  reader->BookMVA( "BDTG_all",   wgtdir+"/weights_all/TMVA_training_BDTG.weights.xml" );
  
  long maxEvts= sig->GetEntries();
  
  
  for (long n=0;n<maxEvts;n++){
    //if (n % 1000 ==0 ) std::cout<< n <<std::endl;
    
    if ( sig->GetEntry( n ) < 0 ) {
      std::cout << "error" << std::endl;
      break;
    }
    
    bool mass_window=false;
    if(useMw){
      //mass_window = m4l>low_m4l_range && m4l<130000 ;
      mass_window = m4l_constr>low_m4l_range && m4l_constr<130000 ;
    }
    else{
      mass_window=true;
    }
    if(mass_window){
      
      double out_bdt=0;
      
      if ((quadType==0)) out_bdt = reader->EvaluateMVA("BDTG_4e");
      if ((quadType==1)) out_bdt = reader->EvaluateMVA("BDTG_4mu");
      if ((quadType==2)) out_bdt = reader->EvaluateMVA("BDTG_2e2mu");
      if ((quadType==3)) out_bdt = reader->EvaluateMVA("BDTG_2mu2e");
      
      outBDTG_all= reader->EvaluateMVA("BDTG_all");
      
      double out_mlp=0;
      outBDTG=out_bdt;
      outMLPNN=out_mlp;
      outM4l=m4l;
      outQuadType=quadType;
      outWeight=weight;
      outM4l_constr=m4l_constr;

      //BDT->Fill(out_bdt,weight);
      if (quadType==1) BDT4mu->Fill(out_bdt,weight);
      if (quadType==0) BDT4e->Fill(out_bdt,weight);
      if (quadType==2) BDT2e2mu->Fill(out_bdt,weight);
      if (quadType==3) BDT2mu2e->Fill(out_bdt,weight);

      if (isZJ==0){
        if (quadType==1) BDT4mu2d->Fill(out_bdt,m4l,weight);
        if (quadType==0) BDT4e2d->Fill(out_bdt,m4l,weight);
        if (quadType==2) BDT2e2mu2d->Fill(out_bdt,m4l,weight);
        if (quadType==3) BDT2mu2e2d->Fill(out_bdt,m4l,weight);
      }
      if (isZJ==1){
         BDT4mu2d->Fill(out_bdt,m4l,weight);
         BDT4e2d->Fill(out_bdt,m4l,weight);
         BDT2e2mu2d->Fill(out_bdt,m4l,weight);
         BDT2mu2e2d->Fill(out_bdt,m4l,weight);
      }

      t->Fill();
    }
  }
   BDT4mu->Write();
   BDT4e->Write();
   BDT2e2mu->Write();
   BDT2mu2e->Write();  
   BDT4mu2d->Write();
   BDT4e2d->Write();
   BDT2e2mu2d->Write();
   BDT2mu2e2d->Write();
  f1->Write();
  
  //f1->Close();
 
}
