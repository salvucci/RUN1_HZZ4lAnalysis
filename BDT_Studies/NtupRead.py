#!/usr/bin/python
import os
import sys

InDir        = "MVAInput"
OutDir       = "OUTPUT"
TrainType    = "Train_115-130"
RankType     = "RemoveOneVar"
LowMass      = 115000
UseZjet      = 0
UseMW        = 1

Channels     = ["signal_H4l","zz_bkg"]
ChanInFiles  = ["signal_ggf_125_merged.root","ZZ_LowMass_gg2ZZ_merged.root"]
ChanOutFiles = ["out_moriond_signal_ggf_125","out_moriond_zz"]

if RankType=="RemoveOneVar":
    Types = ["AllVars","WithoutCosTheta1","WithoutCosTheta2",
             "WithoutCosThetaStar","WithoutEta","WithoutMZ2",
             "WithoutMZ1","WithoutPhi","WithoutPhi1","WithoutPt"]
    
elif RankType=="RemoveMz2PlusOne":
    Types = ["WithoutCosTheta1","WithoutCosTheta2",
             "WithoutCosThetaStar","WithoutEta",
             "WithoutMZ1","WithoutPhi","WithoutPhi1","WithoutPt"]

elif RankType=="RemoveMz2PtPlusOne":
    Types = ["WithoutCosTheta1","WithoutCosTheta2",
             "WithoutCosThetaStar","WithoutEta",
             "WithoutMZ1","WithoutPhi","WithoutPhi1"]
    
elif RankType=="RemoveMz2PtEtaPlusOne":
    Types = ["WithoutCosTheta1","WithoutCosTheta2",
             "WithoutCosThetaStar","WithoutMZ1",
             "WithoutPhi","WithoutPhi1"]
    
elif RankType=="RemoveMz2PtEtaMz1PlusOne":
    Types = ["WithoutCosTheta1","WithoutCosTheta2",
             "WithoutCosThetaStar",
             "WithoutPhi","WithoutPhi1"]

elif RankType=="RemoveMz2PtEtaMz1CosTheta1PlusOne":
    Types = ["WithoutCosTheta2",
             "WithoutCosThetaStar",
             "WithoutPhi","WithoutPhi1"]

elif RankType=="RemoveMz2PtEtaMz1CosTheta1Phi1PlusOne":
    Types = ["WithoutCosTheta2",
             "WithoutCosThetaStar",
             "WithoutPhi"]

elif RankType=="RemoveMz2PtEtaMz1CosTheta1Phi1CosThetaStarPlusOne":
    Types = ["WithoutCosTheta2",
             "WithoutPhi"]

elif RankType=="TestOtherMasses":
    Types = ["AllVars"]

elif RankType=="RemovePhiPlusOne":
    Types = ["WithoutCosTheta2"]

elif RankType=="RemovePhiCosTheta2PlusOne":
    Types = ["WithoutCosThetaStar"]

elif RankType=="RemovePhiCosTheta2CosThetaStarPlusOne":
    Types = ["WithoutPhi1"]

elif RankType=="RemovePhiCosTheta2CosThetaStarPhi1PlusOne":
    Types = ["WithoutCosTheta1"]

elif RankType=="RemovePhiCosTheta2CosThetaStarPhi1CosTheta1PlusOne":
    Types = ["WithoutMZ1"]

elif RankType=="RemovePhiCosTheta2CosThetaStarPhi1CosTheta1Mz1PlusOne":
    Types = ["WithoutEta"]

ch = 0
for m_chan in Channels:
    
    for m_type in Types:

        print "processing channel "+m_chan+":\n"
        print "                   Train = "+TrainType+", Vars = "+m_type+" \n"
        
        program = "root -b -q "
        
        option1 = InDir+"/"+ChanInFiles[ch]
        option2 = OutDir+"/"+RankType+"/"+ChanOutFiles[ch]
        if m_type!="AllVars":
            option3 = OutDir+"/"+RankType+"/"+TrainType+"_"+m_type
        else:
            option3 = OutDir+"/"+TrainType+"_"+m_type
        option4 = m_type
        option5 = Channels[ch]
        option6 = UseZjet
        option7 = LowMass
        option8 = RankType
        option9 = UseMW
        
        cmd1 = option1+"\",\""+option2+"\",\""+option3+"\",\""+option8
        cmd2 = option4+"\",\""+option5+"\","+str(option6)+","+str(option7)+","+str(option9)

        command = program+"\'NtupRead.C(\""+cmd1+"\",\""+cmd2+")\'"
        
        print command

        os.system(command)
    
    ch=ch+1

