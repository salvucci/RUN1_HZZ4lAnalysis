#!/usr/bin/python
import os
import sys

InDir        = "MVAInput"
OutDir       = "OUTPUT"
TrainType    = "Train_115-130"
RankType     = "RemoveOneVar"
UseUncM4l    = 0

ChanInFiles  = ["signal_ggf_125_merged.root","ZZ_LowMass_merged.root"]

if RankType=="RemoveOneVar":
    Types        = ["AllVars","WithoutCosTheta1","WithoutCosTheta2",
                    "WithoutCosThetaStar","WithoutEta","WithoutMZ2",
                    "WithoutMZ1","WithoutPhi","WithoutPhi1","WithoutPt"]
    
elif RankType=="RemoveMz2PlusOne":
    Types        = ["WithoutCosTheta1","WithoutCosTheta2",
                    "WithoutCosThetaStar","WithoutEta",
                    "WithoutMZ1","WithoutPhi","WithoutPhi1","WithoutPt"]

elif RankType=="RemoveMz2PtPlusOne":
    Types        = ["WithoutCosTheta1","WithoutCosTheta2",
                    "WithoutCosThetaStar","WithoutEta",
                    "WithoutMZ1","WithoutPhi","WithoutPhi1"]
    
elif RankType=="RemoveMz2PtEtaPlusOne":
    Types        = ["WithoutCosTheta1","WithoutCosTheta2",
                    "WithoutCosThetaStar","WithoutMZ1",
                    "WithoutPhi","WithoutPhi1"]

elif RankType=="RemoveMz2PtEtaMz1PlusOne":
    Types        = ["WithoutCosTheta1","WithoutCosTheta2",
                    "WithoutCosThetaStar",
                    "WithoutPhi","WithoutPhi1"]

elif RankType=="RemoveMz2PtEtaMz1CosTheta1PlusOne":
    Types        = ["WithoutCosTheta2",
                    "WithoutCosThetaStar",
                    "WithoutPhi","WithoutPhi1"]

elif RankType=="RemoveMz2PtEtaMz1CosTheta1Phi1PlusOne":
    Types        = ["WithoutCosTheta2",
                    "WithoutCosThetaStar",
                    "WithoutPhi"]

elif RankType=="RemoveMz2PtEtaMz1CosTheta1Phi1CosThetaStarPlusOne":
    Types        = ["WithoutCosTheta2",
                     "WithoutPhi"]

elif RankType=="RemovePhiPlusOne":
    Types        = ["WithoutCosTheta2"]

elif RankType=="RemovePhiCosTheta2PlusOne":
    Types         = ["WithoutCosThetaStar"]

elif RankType=="RemovePhiCosTheta2CosThetaStarPlusOne":
    Types        = ["WithoutPhi1"]

elif RankType=="RemovePhiCosTheta2CosThetaStarPhi1PlusOne":
    Types         = ["WithoutCosTheta1"]

elif RankType=="RemovePhiCosTheta2CosThetaStarPhi1CosTheta1PlusOne":
    Types         = ["WithoutMZ1"]

elif RankType=="RemovePhiCosTheta2CosThetaStarPhi1CosTheta1Mz1PlusOne":
    Types         = ["WithoutEta"]

for m_type in Types:
    
    print "making Training for :\n"
    print "               Train = "+TrainType+", Vars = "+m_type+" \n"
    
    program = "root -b -q "
    
    option1 = InDir+"/"+ChanInFiles[0]
    option2 = InDir+"/"+ChanInFiles[1]
    if m_type!="AllVars":
        option3 = OutDir+"/"+RankType+"/"+TrainType+"_"+m_type
    else:
        option3 = OutDir+"/"+TrainType+"_"+m_type
    option4 = RankType
    option5 = m_type
    option6 = UseUncM4l
    
    cmd1 = option1+"\",\""+option2+"\",\""+option3+"\",\""+option4
    cmd2 = option5+"\","+str(option6)

    logfile = " > "+option3+"/LogFile_"+TrainType+"_"+m_type+".dat"
    
    print logfile

    command = program+"\'TMVATrain.C(\""+cmd1+"\",\""+cmd2+")\'"+logfile
        
    print command

    os.system(command)
