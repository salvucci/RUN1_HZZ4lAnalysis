#!/usr/bin/python
import os
import sys

UseZZOnly    = 1
M4lStart     = 120
TrainAllChan = 0
OutDir       = "OUTPUT/Significance"
RankType     = "RemoveOneVar"
TrainType    = "Train_115-130"

if TrainAllChan==1:
    TrainChan = "TrainAllChans"
    RankType  = ""
    Types     = ["AllVars"]

elif TrainAllChan==0:
    TrainChan = "TrainSingleChan"
    if RankType=="RemoveOneVar":
        Types  = ["AllVars","WithoutCosTheta1","WithoutCosTheta2",
                  "WithoutCosThetaStar","WithoutEta","WithoutMZ2",
                  "WithoutMZ1","WithoutPhi","WithoutPhi1","WithoutPt"]
        
    elif RankType=="RemoveMz2PlusOne":
        Types  = ["WithoutCosTheta1","WithoutCosTheta2",
                  "WithoutCosThetaStar","WithoutEta",
                  "WithoutMZ1","WithoutPhi","WithoutPhi1","WithoutPt"]

    elif RankType=="RemoveMz2PtPlusOne":
        Types = ["WithoutCosTheta1","WithoutCosTheta2",
                 "WithoutCosThetaStar","WithoutEta",
                 "WithoutMZ1","WithoutPhi","WithoutPhi1"]
        
    elif RankType=="RemoveMz2PtEtaPlusOne":
        Types = ["WithoutCosTheta1","WithoutCosTheta2",
                 "WithoutCosThetaStar","WithoutMZ1",
                 "WithoutPhi","WithoutPhi1"]

    elif RankType=="RemoveMz2PtEtaMz1PlusOne":
        Types = ["WithoutCosTheta1","WithoutCosTheta2",
                 "WithoutCosThetaStar",
                 "WithoutPhi","WithoutPhi1"]

    elif RankType=="RemoveMz2PtEtaMz1CosTheta1PlusOne":
        Types = ["WithoutCosTheta2",
                 "WithoutCosThetaStar",
                 "WithoutPhi","WithoutPhi1"]

    elif RankType=="RemoveMz2PtEtaMz1CosTheta1Phi1PlusOne":
        Types = ["WithoutCosTheta2",
                 "WithoutCosThetaStar",
                 "WithoutPhi"]

    elif RankType=="RemoveMz2PtEtaMz1CosTheta1Phi1CosThetaStarPlusOne":
        Types = ["WithoutCosTheta2",
                 "WithoutPhi"]

    elif RankType=="RemovePhiPlusOne":
        Types = ["WithoutCosTheta2"]

    elif RankType=="RemovePhiCosTheta2PlusOne":
        Types = ["WithoutCosThetaStar"]

    elif RankType=="RemovePhiCosTheta2CosThetaStarPlusOne":
        Types = ["WithoutPhi1"]

    elif RankType=="RemovePhiCosTheta2CosThetaStarPhi1PlusOne":
        Types = ["WithoutCosTheta1"]

    elif RankType=="RemovePhiCosTheta2CosThetaStarPhi1CosTheta1PlusOne":
        Types = ["WithoutMZ1"]

    elif RankType=="RemovePhiCosTheta2CosThetaStarPhi1CosTheta1Mz1PlusOne":
        Types = ["WithoutEta"]

for m_type in Types:
    
    print " Calculating significance for :\n"
    print "                   Train = "+TrainType+", Vars = "+m_type+" \n"
        
    program = "root -b -q "
        
    option1 = UseZZOnly
    option2 = M4lStart
    option3 = m_type
    option4 = TrainAllChan
    option5 = RankType

    logfile = " > "+OutDir+"/"+RankType+"/SignLogFile_"+TrainType+"_"+TrainChan+"_"+m_type+".dat"
    
    cmd1 = str(option1)+","+str(option2)+",\""+option3+"\",\""+option5+"\","+str(option4)
    
    command = program+"\'SignPlots.C("+cmd1+")\'"+logfile
        
    print command
    
    os.system(command)
