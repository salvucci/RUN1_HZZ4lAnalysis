/* ROOT includes */
#include "TH1F.h"
#include "TString.h"
#include "TLine.h"
#include "TGraph.h"
#include "TFile.h"
#include "TH1F.h"
#include "TCanvas.h"
#include "TApplication.h"
#include "TROOT.h"
#include "TStyle.h"
#include "TLatex.h"
#include "TLegend.h"
/*C++ includes */
#include <iostream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <algorithm>

void DrawCPResults(TString InputFile,  TString CoupType,
		   TString OutName,    TString ATLtext,
		   Double_t OneSigVal, Double_t TwoSigVal,
		   Double_t Xwidth,    Double_t Ywidth,
		   bool AddZZ,	       bool AddWW,
		   TString Leg1,       TString Leg2,
		   TString Leg3,       TString Leg4,
		   TString Leg5,       TString Hist1,
		   TString Hist2,      TString Hist3,
		   bool Add9D);

TLine* DefineHorizLine(Double_t Yval,Double_t Xmin);

void SetStyleATLAS();

void ATLASLabel(TString ATLtext);

void DrawLegend(TString Leg1,TString Leg2,TString Leg3,
		TString Leg4,TString Leg5);

TString RetrieveLabel(TString lab);

void SetMuSMStyle(Double_t YMax,Double_t XMin,
		  TString YLab,TString XLab,
		  bool Solid);

void SetMuProfStyle(Double_t YMax,Double_t XMin,
		    TString YLab,TString XLab,
		    bool Solid);

void SetObsStyle(Double_t YMax,Double_t XMin,
		 TString YLab,TString XLab);

void SetHorizLineStyle();

void DrawChannel(bool AddZZ,bool AddWW,bool Add9D);

TGraph *MuSM;
TGraph *MuProf;
TGraph *Observed;

TLine *Line1;
TLine *Line2;

bool AddZZ;
bool AddWW;
