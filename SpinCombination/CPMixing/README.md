############ README FOR CP MIXING PLOTS ###########

the package contains two scripts:

1) LikelihoodDist_ExclLimit.C

2) DrawCPResults.C

Compilation:

	simple 'make'

Running:

	after compilation you will have:
	from script 1) => LikelDist_ExclLimit
	from script 2) => DrawCPResults


Let's have a look to the first one (LikelDist_ExclLimit):

   This script will produce CP results given an specific input file.
   Running "./LikelDist_ExclLimit" (without options) you will have
   the following ouput:

   "  Usage   : ./LikelDist_ExclLimit [Options]"
   "  Needed Options : "
   "                   --InputFile [Input ROOT File]"
   "                   --Label [Histogram label: Observed,MuProfiled,MuSM]"
   "                   --CoupType [Specify couplings type: Khvv, Kavv]"
   "  Other Options : "
   "                   --RecreateOutFile [Enable Recreation Out File]"
   this means that, for instance, to create the result curve for data
   and coupling Khvv you have to use the command

   ./LikelDist_ExclLimit --InputFile datafile.root --Label Observed --CoupType Khvv

   the option --RecreateOutFile has to be specified only the first
   time: you want to have an output root file with the 3 histograms 
   (bserved,MuProfiled,MuSM), so in sequence you have to use the commands:

   ./LikelDist_ExclLimit --InputFile datafile.root --Label Observed --CoupType Khvv --RecreateOutFile

   ./LikelDist_ExclLimit --InputFile muSMfile.root --Label MuSM --CoupType Khvv

   ./LikelDist_ExclLimit --InputFile muProfiledfile.root --Label MuProfiled --CoupType Khvv

   The same for the coupling Kavv:
   option --CoupType Kavv instead of --CoupType Khvv




Let's have a look to the first one (DrawCPResults):

   This script will produce CP results for the paper overimposing
   all curves and 1sigma and 2 sigma lines.
   Running "./DrawCPResults" (without options) you will have
   the following ouput:

    "DrawCPResults: Make CP mixing plots!!"
    "  Usage   : ./DrawCPResults [Options]"
    "  Needed Options : "
    "                   --InputFile [Input ROOT File]"<<endl;
    "                   --CoupType [X axis range +-Value]"<<endl;
    "  Other Options : "
    "                   --AddPreliminary [Enable 'Preliminary' label: default false]"
    "                   --OneSigmaVal [Set 1 Sigma Likelihood value: default 1]"
    "                   --TwoSigmaVal [Set 2 Sigma Likelihood value: default 3.92]"
    "                   --XRange [Set X axis range: default is 10]"
    "                   --YRange [Set Y axis range: default is 21]"
    "                   --Channel [Specify Higgs decay: all, WW, ZZ: "" is default]"


    To have the final plot for Kavv couplings you have to use the
    command:

    ./DrawCPResults --InputFile kavvHistFile.root --CoupType Kavv --AddPreliminary --Channel all 