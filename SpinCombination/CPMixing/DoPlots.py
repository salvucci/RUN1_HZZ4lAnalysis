#!/usr/bin/python

import os
import sys
import shutil
import getopt
import string


AddInternal    = False
AddPreliminary = False

CP = ["expected_muhat_kavv",
      "expected_muhat_khvv",
      "HSG2_kavv_fullSyst",
      "HSG2_khvv_fullSyst",
      "kavv",
      "khvv",
      "observed_full_channel_kavv",
      "observed_full_channel_khvv",
      "HSG2_kavv_9D",
      "HSG2_khvv_9D"]


OutName = ["CPMix_Expected",
           "CPMix_Expected",
           "CPMix_HZZ_Observed",
           "CPMix_HZZ_Observed",
           "CPMix_Observed_Comb",
           "CPMix_Observed_Comb",
           "CPMix_Observed_Chans",
           "CPMix_Observed_Chans",
           "CPMix_HZZ_Observed_9D",
           "CPMix_HZZ_Observed_9D"]
         
#Yrange = [20,
#          33,
#          18,
#          30,
#          28,
#          33,
#          26,
#          25]

Yrange = [32,32,32,32,32,
          32,32,32,32,37]

Step=int(0)

for cp in CP:

    InFile  = cp+".root"
    Outname = OutName[Step]
    AddWW=True

    cmd  = "./DrawCPResults --InputFile "+InFile
    cmd += " --YRange "+str(Yrange[Step])
    cmd += " --OutName "+Outname
    if AddInternal:
        cmd += " --AddInternal"
    if AddPreliminary:
        cmd += " --AddPreliminary"
        
    if cp.find("HSG2")!=-1:
        AddWW=False

    if cp.find("9D")!=-1:
        cmd += " --Add9D"

    if AddWW:
        cmd += " --AddZZ --AddWW"
    else:
        cmd += " --AddZZ"

    if cp.find("kavv")!=-1:
        cmd += " --CoupType Kavv"
    elif cp.find("khvv")!=-1:
        cmd += " --CoupType Khvv"
    if cp.find("observed")!=-1:
        cmd += " --Leg1Label Combined"
        cmd += " --Leg2Label ObsHZZ"
        cmd += " --Leg3Label ObsHWW"
        if cp.find("channel")!=-1:
            cmd += " --Leg4Label scale68CL"
            cmd += " --Leg5Label scale95CL"
    elif cp.find("expected")!=-1:
        cmd += " --Leg1Label ExpCombined"
        cmd += " --Leg2Label ExpHZZ"
        cmd += " --Leg3Label ExpHWW"
        cmd += " --Leg4Label scale68CL"
        cmd += " --Leg5Label scale95CL"
        
    print cmd
    os.system(cmd)
        
    Step+=1
