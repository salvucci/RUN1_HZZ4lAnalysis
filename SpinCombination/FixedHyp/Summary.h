/* ROOT includes */
#include "TH1F.h"
#include "TString.h"
#include "TLine.h"
#include "TGraphErrors.h"
#include "TGraphAsymmErrors.h"
#include "TArrow.h"
/* C++ includes */
#include <vector>
#include <iostream>
#include <fstream>

void Summary(TString InputExpFile,
	     TString InputObsFile,
	     TString OutName,
	     TString ATLtext,
	     bool AddZZ,
	     bool AddWW,
	     bool AddGamGam,
	     bool ExtY);

void SetStyleATLAS();

void ATLASLabel(TString ATLtext);

void DrawLegend();//TString ObsLabel,TString ExpLabel, TString ErrLabel);

void DrawChannel(bool AddZZ,bool AddWW,bool AddGamGam);

void DrawSigma();

TString AltHypLabel(std::string input);

void SetValues();

void SetExpStyle();

void SetErrStyle();

void SetDataStyle();

void SetSkeletonStyle();

void SetSigmaStyle();

void SetSepStyle();

void GetExpErrVal(TString file);

void GetObsVal(TString file);

TLine* DefineVertLine(Double_t Xval);

TLine* DefineHorLine(Double_t Yval);

void DrawObsArrowLimit();
void DrawExpArrowLimit();

void DefineForbiddenReg(Double_t Lim);

std::vector<std::string> Hyp;
std::vector<double>      Exp;
std::vector<double>      Obs;
std::vector<double>      ErrUp;
std::vector<double>      ErrDown;
std::vector<int>         ExpLim;
std::vector<int>         ObsLim;

bool m_ObsArr;
bool m_ExpArr;
bool m_ExtY;

TH1F *Skeleton;
TGraphErrors* Expected;
TGraphErrors* ExpectedLim;
TGraphErrors* Observed;
TGraphErrors* ObservedLim;
TGraphAsymmErrors* Error;
TGraphAsymmErrors* ProbReg;

TLine *lSigma1;
TLine *lSigma2;
TLine *lSigma3;
TLine *lSigma4;
TLine *lSep1;
TLine *lSep2;
TLine *lSep3;
TLine *lSep4;
TLine *lSep5;
TLine *lSep6;

TArrow *ObsArr;
TArrow *ExpArr;

std::ifstream InValues; //!
