#!/usr/bin/python

import os
import sys
import shutil
import getopt
import string

AddInternal    = False
AddPreliminary = False
BinWidth       = False
ShadeArea      = True

Fprefix = "Out_Significance_with_median_"

CP = ["HSG1_HSG2_HSG3_lg125_MuHat","HSG1_HSG2_HSG3_lg125_MuSM",
      "HSG1_HSG2_HSG3_lg300_MuHat","HSG1_HSG2_HSG3_lg300_MuSM",
      "HSG1_HSG2_HSG3_lq125_MuHat","HSG1_HSG2_HSG3_lq125_MuSM",
      "HSG1_HSG2_HSG3_lq300_MuHat","HSG1_HSG2_HSG3_lq300_MuSM",
      "HSG1_HSG2_HSG3_UC_MuHat","HSG1_HSG2_HSG3_UC_MuSM",
      "HSG2_HSG3_0p0m_MuHat","HSG2_HSG3_0p0m_MuSM",
      "HSG2_HSG3_0p0ph_MuHat","HSG2_HSG3_0p0ph_MuSM"]

Label = ["lg125","lg125",
         "lg300","lg300",
         "lq125","lq125",
         "lq300","lq300",
         "UC","UC",
         "0m","0m",
         "0ph","0ph"]
         
Xrange = [30,30,#45,45,#25,20,
          45,45,#45,45,#40,35,
          30,30,#45,45,#25,25,
          45,45,#45,45,#45,40,
          30,30,#45,45,#25,25,
          30,30,#45,45,#30,25,
          30,30]#45,45]#20,20]

Step=int(0)

for cp in CP:

    InFile  = Fprefix+cp+".root"
    OutName = cp
    AddGamGam=True

    cmd  = "./DrawSign --InputFile "+InFile
    cmd += " --Xrange "+str(Xrange[Step])
    cmd += " --OutName "+OutName
    if AddInternal:
        cmd += " --AddInternal"
    if AddPreliminary:
        cmd += " --AddPreliminary"
        
    cmd += " --AddZZ --AddWW"

    if cp.find("0m")!=-1 or cp.find("0ph")!=-1:
        AddGamGam=False
        
    if AddGamGam:
        cmd += " --AddGamGam"
        
    cmd += " --SMLegLabel 0p --AltHypLegLabel "+Label[Step]

    if BinWidth:
        cmd += " --UseBinWidth"

    if ShadeArea:
        cmd += " --UseShadeArea"

    print cmd
    os.system(cmd)
        
    cmd += " --UseLogScale "
    print cmd
    os.system(cmd)

    Step+=1
