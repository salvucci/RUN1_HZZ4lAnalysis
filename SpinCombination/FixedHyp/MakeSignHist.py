#!/usr/bin/python
import os
import sys

0p0m = ["0p0m",
        
        "Toys/HSG3_HSG2_0p0m_epsilon_0_mu_hat/HSG3_HSG2_0p0m_epsilon_0_mu_hat.root",
        "Toys/HSG3_HSG2_0p0m_epsilon_1_mu_hat/HSG3_HSG2_0p0m_epsilon_1_mu_hat.root",
        "Toys/HSG3_HSG2_0p0m_epsilon1_mu_1/HSG3_HSG2_0p0m_epsilon1_mu_1.root"]

Workspaces   = ["0p0m","0p0ph","UC","lg125","lg300","lq125","lq300"]
InputDirName = "Toys_epsilon0"
OutDir       = "output/"
OutFile      = OutDir+InputDirName
cmd = "hadd "+OutFile+"_"+Workspace+"_merged.root "

for file in os.listdir(OutDir+InputDirName+"/"):
    if file.find(".root")!=-1:
        fullname=os.path.join(OutDir+InputDirName+"/",file)
        print "found root file ",fullname
        if fullname is not None:
            cmd += fullname+" "

print ""
print "Merging root files in "+OutFile+"_merged.root"
#print cmd
os.system(cmd)
