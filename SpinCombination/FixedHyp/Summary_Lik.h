/* ROOT includes */
#include "TH1F.h"
#include "TString.h"
#include "TLine.h"
#include "TGraphErrors.h"
#include "TGraphAsymmErrors.h"
#include "TArrow.h"
/* C++ includes */
#include <vector>
#include <iostream>
#include <fstream>

void Summary_Lik(TString InputSMFile,
		 TString InputAltHypFile,
		 TString InputExpFile,
		 TString InputObsFile,
		 TString OutName,
		 TString ATLtext,
		 bool AddZZ,
		 bool AddWW,
		 bool AddGamGam);

void SetStyleATLAS();

void ATLASLabel(TString ATLtext);

void DrawLegend();

void DrawChannel(bool AddZZ,bool AddWW,bool AddGamGam);

void DrawSigma();

TString AltHypLabel(std::string input);

void SetValues();

void SetSMStyle();

void SetAltHypStyle();

void SetExpStyle();

void SetDataStyle();

void SetSkeletonStyle();

void SetSepStyle();

void GetSMVal(TString file);

void GetAltHypVal(TString file);

void GetExpVal(TString file);

void GetObsVal(TString file);

TLine* DefineVertLine(Double_t Xval);

TLine* DefineHorLine(Double_t Yval);

std::vector<std::string> Hyp;
std::vector<double>      ExpSM;
std::vector<double>      ExpAH;
std::vector<double>      Obs;
std::vector<double>      P1SigmaSM;
std::vector<double>      M1SigmaSM;
std::vector<double>      P2SigmaSM;
std::vector<double>      M2SigmaSM;
std::vector<double>      P3SigmaSM;
std::vector<double>      M3SigmaSM;
std::vector<double>      P1SigmaAH;
std::vector<double>      M1SigmaAH;
std::vector<double>      P2SigmaAH;
std::vector<double>      M2SigmaAH;
std::vector<double>      P3SigmaAH;
std::vector<double>      M3SigmaAH;

TH1F *Skeleton;
TGraphAsymmErrors* Sigma1SM;
TGraphAsymmErrors* Sigma2SM;
TGraphAsymmErrors* Sigma3SM;
TGraphAsymmErrors* Sigma1AH;
TGraphAsymmErrors* Sigma2AH;
TGraphAsymmErrors* Sigma3AH;
TGraphErrors* ExpectedSM;
TGraphErrors* ExpectedAH;
TGraphErrors* Observed;

TLine *lSep1;
TLine *lSep2;
TLine *lSep3;
TLine *lSep4;
TLine *lSep5;
TLine *lSep6;

std::ifstream InValues; //!
