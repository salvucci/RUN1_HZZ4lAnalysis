#include "TString.h"

void SignCalc_WithMedian(TString Epsilon1Tree_SM,
			 TString Epsilon0Tree_AltHyp,
			 float ObservedValue,
			 TString WsName,
			 float AsimovValue=1.2,
			 int KindOfCalc=1);
