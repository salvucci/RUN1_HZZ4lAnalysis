#!/usr/bin/python
import os
import sys

Workspace    = "fit_to_data_snapshot"
InputDirName = "Toys_epsilon0"
OutDir       = "output/"
OutFile      = OutDir+InputDirName
cmd = "hadd "+OutFile+"_"+Workspace+"_merged.root "

for file in os.listdir(OutDir+InputDirName+"/"):
    if file.find(".root")!=-1:
        fullname=os.path.join(OutDir+InputDirName+"/",file)
        print "found root file ",fullname
        if fullname is not None:
            cmd += fullname+" "

print ""
print "Merging root files in "+OutFile+"_merged.root"
#print cmd
os.system(cmd)
