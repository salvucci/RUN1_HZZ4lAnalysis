#include "TH1F.h"
#include "TString.h"
#include "TLine.h"

void DrawSign(TString InputFile,
	      double xRange,
	      TString OutName,
	      TString ATLtext,
	      bool AddZZ,
	      bool AddWW,
	      bool AddGamGam,
	      TString ObsLabel,
	      TString SMLabel,
	      TString AltHypLabel,
	      bool UseYlog,
	      bool BinWidth,
	      bool Shade);

TLine* DefineMedian(int Bin);

void SetStyleATLAS();

void ATLASLabel(TString ATLtext);

void DrawLegend(TString ObsLabel,TString SMLabel, TString AltHypLabel);

TString SMLegLabel(TString input);

TString AltHypLegLabel(TString input);

void DrawChannel(bool AddZZ,bool AddWW,bool AddGamGam);

void SetSMStyle(bool Ylog, bool BinWidth);

void SetAltHypStyle(bool Ylog, bool BinWidth);

void SetDataStyle();

void SetShadeStyle(bool Ylog, bool BinWidth);

TH1F *SM;
TH1F *SM_Med;
TH1F *MedObs;
TH1F *AltHyp;
TH1F *AltHyp_Med;
TH1F *Observed;
TH1F *SM_Shade;
TH1F *AltHyp_Shade;

TLine *lSM_Med;
TLine *lAltHyp_Med;
TLine *lObs_Med;
