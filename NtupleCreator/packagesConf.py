#!/usr/bin/python
import os
import sys
from packagesDef import *

#Download packages
print "\n"
for pack in Packages:
    
    print "  Downloading package "+pack[0]
    command = "svn co "+SVNPath+pack[2]+pack[1]+" "+PackagesDir+pack[0]
    os.system(command)
    print ""
    print "  Package "+pack[0]+" downloaded!!"
    if pack[0]=="egammaEvent":
        continue
    print "     Copying needed Makefile for package "+pack[0]
    os.chdir(PackagesDir+pack[0]+"/cmt")
    retval = os.getcwd()

    print "Directory changed successfully %s" % retval

    os.system("cp "+MakefilesDir+pack[0]+"_"+Makefile+" "+Makefile)
    
    os.chdir("../../../")
    retval = os.getcwd()

    print "Directory changed successfully %s" % retval

    print ""


#Download needed input files
if DownFile:
    print "Downloading PileUp reweighting files ..."
    os.chdir("Utils/Inputs/")
    retval = os.getcwd()
    print "Directory changed successfully %s" % retval
    #os.system("cp /afs/cern.ch/atlas/groups/HSG2/H4l_2013/Moriond/ExtendedPileUpReweight4l/MC11c.prw.root .")
    os.system("cp /afs/cern.ch/atlas/groups/HSG2/H4l_2013/Autumn/ExtendedPileUpReweight4l/MC11c.prw.root .")
    #os.system("cp /afs/cern.ch/atlas/groups/HSG2/H4l_2013/Moriond/ExtendedPileUpReweight4l/ilumicalc_2011_AllYear_All_Good.root .")
    os.system("cp /afs/cern.ch/atlas/groups/HSG2/H4l_2013/Autumn/ExtendedPileUpReweight4l/ilumicalc_2011_AllYear_All_Good.root .")
    
    #os.system("cp /afs/cern.ch/atlas/groups/HSG2/H4l_2013/Moriond/ExtendedPileUpReweight4l/MC12a.prw.root .")
    os.system("cp /afs/cern.ch/atlas/groups/HSG2/H4l_2013/Autumn/ExtendedPileUpReweight4l/MC12a.prw.root .")
    os.system("cp /afs/cern.ch/atlas/groups/HSG2/H4l_2013/Autumn/ExtendedPileUpReweight4l/MC12b.prw.root .")
    os.system("cp /afs/cern.ch/atlas/groups/HSG2/H4l_2013/Autumn/ExtendedPileUpReweight4l/MC12bBCH.prw.root .")
    
    #os.system("cp /afs/cern.ch/atlas/groups/HSG2/H4l_2013/Moriond/ExtendedPileUpReweight4l/ilumicalc_2012_AllYear_All_Good.root .")
    os.system("cp /afs/cern.ch/atlas/groups/HSG2/H4l_2013/Autumn/ExtendedPileUpReweight4l/ilumicalc_2012_AllYear_All_Good.root .")
    
    #print "Downloading SF reweighting files ..."
    #os.system("cp /afs/cern.ch/user/t/takashi/public/muon_trigger_sf_mc11c.root .")
    #os.system("cp /afs/cern.ch/user/t/takashi/public/SF2012/muon_trigger_sf_2012_AtoL.p1328.root .")

    os.chdir("../GRL_xmls/AllGood/")
    retval = os.getcwd()
    print "Directory changed successfully %s" % retval
    print "Downloading GRL files ... "
    os.system("cp /afs/cern.ch/atlas/groups/HSG2/H4l_2013/Autumn/GRL/data11_7TeV.periodAllYear_DetStatus-v36-pro10-02_CoolRunQuery-00-04-08_All_Good.xml .")
    os.system("cp /afs/cern.ch/atlas/groups/HSG2/H4l_2013/Autumn/GRL/data12_8TeV.periodAllYear_DetStatus-v61-pro14-02_DQDefects-00-01-00_PHYS_StandardGRL_All_Good.xml .")

print "All needed files downloaded!!"
    
print "PackageConfig:: Everything Done!!"
