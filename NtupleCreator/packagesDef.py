#!/usr/bin/python
import os
import sys

#Packages Variables definition
SVNPath="svn+ssh://svn.cern.ch/reps/"

PATCorePath  = "atlasoff/PhysicsAnalysis/AnalysisCommon/PATCore/tags/PATCore-"
GRLPath      = "atlasoff/DataQuality/GoodRunsLists/tags/GoodRunsLists-"
EgammaPath   = "atlasoff/Reconstruction/egamma/egammaAnalysis/egammaAnalysisUtils/tags/egammaAnalysisUtils-"
EgammaEvPath = "atlasoff/Reconstruction/egamma/egammaEvent/tags/egammaEvent-"
ElPhSelPath  = "atlasoff/PhysicsAnalysis/ElectronPhotonID/ElectronPhotonSelectorTools/tags/ElectronPhotonSelectorTools-"
TrigEffPath  = "atlasoff/Trigger/TrigAnalysis/TrigMuonEfficiency/tags/TrigMuonEfficiency-"
PileUpPath   = "atlasoff/PhysicsAnalysis/AnalysisCommon/PileupReweighting/tags/PileupReweighting-"
ZmassPath    = "atlasphys/Physics/Higgs/HSG2/Code/ZMassConstraint/tags/ZMassConstraint-"
MuMomCorPath = "atlasoff/PhysicsAnalysis/MuonID/MuonIDAnalysis/MuonMomentumCorrections/tags/MuonMomentumCorrections-"
MuEffPath    = "atlasoff/PhysicsAnalysis/MuonID/MuonIDAnalysis/MuonEfficiencyCorrections/tags/MuonEfficiencyCorrections-"
ElEffPath    = "atlasoff/PhysicsAnalysis/ElectronPhotonID/ElectronEfficiencyCorrection/tags/ElectronEfficiencyCorrection-"
EgMomErrPath = "Reconstruction/egamma/egammaAnalysis/egammaFourMomentumError/tags/egammaFourMomentumError-"
EgParConPath = "atlasoff/Reconstruction/egamma/egammaAnalysis/egParticleConditions/tags/egParticleConditions-"
TileReadPath = "atlasoff/PhysicsAnalysis/TileID/TileTripReader/tags/TileTripReader-"
JetCalibPath = "atlasoff/Reconstruction/Jet/ApplyJetCalibration/tags/ApplyJetCalibration-"
ElPhMomPath  = "atlasoff/PhysicsAnalysis/ElectronPhotonID/ElectronPhotonFourMomentumCorrection/tags/ElectronPhotonFourMomentumCorrection-"
EGammRecPath = "atlasoff/Reconstruction/egamma/egammaLayerRecalibTool/tags/egammaLayerRecalibTool-"
EGammMVAPath = "atlasoff/Reconstruction/egamma/egammaMVACalib/tags/egammaMVACalib-"
MisMasCPath  = "atlasoff/PhysicsAnalysis/AnalysisCommon/MissingMassCalculator/tags/MissingMassCalculator-"

Packages = [ ["PATCore","00-00-16",PATCorePath],                                    #done
             ["GoodRunsLists","00-01-09",GRLPath],                                  #done
             ["PileupReweighting","00-02-12",PileUpPath],                           #done
             ["egammaAnalysisUtils","00-04-58",EgammaPath],                         #done -> old 00-04-20
             ["egammaEvent","04-00-00",EgammaEvPath],                               #at the moment cannot be compiled
             ["ElectronEfficiencyCorrection","00-00-52",ElEffPath],                 #done
             ["ElectronPhotonSelectorTools","00-00-67",ElPhSelPath],                #done
             ["TrigMuonEfficiency","00-02-50",TrigEffPath],                         #done
             ["ZMassConstraint","00-00-19",ZmassPath],                              #done
             ["MuonMomentumCorrections","00-09-28",MuMomCorPath],                   #done
             ["MuonEfficiencyCorrections","02-01-19-02",MuEffPath],                 #done
             ["TileTripReader","00-00-19",TileReadPath],                            #done
             ["ApplyJetCalibration","00-03-20",JetCalibPath],                       #done
             ["ElectronPhotonFourMomentumCorrection","00-00-32",ElPhMomPath],       #done
             ["egammaLayerRecalibTool","00-01-14",EGammRecPath],                    #done
             ["egammaMVACalib","00-00-29",EGammMVAPath],                            #done
             ["MissingMassCalculator","00-00-15-06",MisMasCPath] ]

#Build variables definition
MacPro   = False
DownFile = False
if os.uname()[1]=='macpro':
    MacPro   = True
    #DownFile = True

Makefile    = "Makefile.Standalone"
PackagesDir = "Packages/"
MakefilesDir = "../../../Utils/Makefiles/"
if MacPro: 
    MakefilesDir += "MacPro/"
