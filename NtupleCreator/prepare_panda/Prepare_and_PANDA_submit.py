import os,subprocess,string


output = subprocess.check_output("voms-proxy-info --all | grep nickname", shell=True)
name=string.split(output)[4]

print "username used to submit: "+name
print "Starting to prepare H4lNtupleCreator.tar.gz "
os.system('cd ..;cd ..; tar --exclude-vcs --exclude \'*.so\' --exclude \'*.o\' --exclude=H4l --exclude=H4lNtupleCreator/prepare_panda -czf H4lNtupleCreator.tar.gz H4lNtupleCreator;mv H4lNtupleCreator.tar.gz H4lNtupleCreator/prepare_panda/ ;cd H4lNtupleCreator/prepare_panda' )

f = open('Datasets.list', 'r')

files=f.readlines()

for f in files:

    input=f.rstrip('\n')

    if f!='':
        
        index=input.index("NTUP_HSG2")
        new_in=input[0:index-1]
        output='user.'+name+'.'+new_in+'_test006'
        print ""
        print "Submitting "+output
        cmd='prun --exec \"echo %IN > inputFiles.txt; tar xzvf H4lNtupleCreator.tar.gz; cd H4lNtupleCreator; cp ../*.root* . ; source SetupGrid.sh;pwd;./H4l --channel name --type mc --year 2012 --outname test.root --mctype mc12c --minVar --useParMinInfo ; cp test.root ../ \"'
        cmd+=' --express --maxFileSize 1099511627776 ' #--extFile H4lNtupleCreator.tar.gz  '
        cmd+=' --rootVer=5.34/22 --cmtConfig=x86_64-slc6-gcc48-opt '
        cmd+=' --inDS '+input
        cmd+=' --skipScout  --nGBPerJob 9 --outputs=XYZ:test.root --outDS '+output
        #print cmd
        os.system(cmd)

os.system('rm H4lNtupleCreator.tar.gz')
