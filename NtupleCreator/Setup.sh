#!/bin/bash

unamestr=`uname -n`
if [ "$unamestr" == 'macpro' ]; then
    echo "Using macpro host:: no extra lib path needed!!"
    echo "Adding Input files path ... "
    export ROOTCOREBIN=Utils/Inputs
    python packagesConf.py
    #python createPAR.py
    python packagesCompile.py
    make -f Makefile.MacPro
else
    echo "Using CERN based machine:: addid CLHEP path ... "
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc48-opt/19.1.0/sw/lcg/external/clhep/1.9.4.7/x86_64-slc6-gcc48-opt/lib
    echo "Adding Input files path ... "
    export ROOTCOREBIN=Utils/Inputs
    python packagesConf.py
    #python createPAR.py
    python packagesCompile.py
    make
fi
