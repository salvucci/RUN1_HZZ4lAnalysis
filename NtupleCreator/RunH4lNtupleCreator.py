#!/usr/bin/python

import os
import sys
import shutil
import getopt
import string
import subprocess

def main():
    # parse command line options
    try:
        opts, args = getopt.getopt(sys.argv[1:], "h", ["help"])
    except getopt.error, msg:
        print msg
        print "for help use --help"
        sys.exit(2)
    # process options
    for o, a in opts:
        if o in ("-h", "--help"):
            ShowDoc()
            sys.exit(0)
    
    #Assign None to all variable
    m_ChanName   = None
    m_Year       = None
    m_Type       = None
    m_SimulVer   = None
    m_DataStream = None
    m_Path       = None
    m_List       = None
    m_Debug      = None
    m_WriteInfo  = None
    m_Proof      = None
    m_TauRemove  = None
    m_MinVar     = None
    m_Correct    = None
    m_Weight     = None
    m_MinParInfo = None
    m_Grid       = None
    m_Tag        = None
    m_NtupTau    = None
    # process arguments
    for arg in args:
        if arg.find('=') > -1:
            key,value = arg.split('=',1)

            if key=="channame":
                m_ChanName=value
                
            elif key=="year":
                m_Year=value
                
            elif key=="type":
                m_Type=value

            elif key=="simulver":
                m_SimulVer=value

            elif key=="datastream":
                m_DataStream=value

            elif key=="path":
                m_Path=value

            elif key=="list":
                m_List=value
                
            elif key=="debug":
                m_Debug=True
                
            elif key=="writeinfo":
                m_WriteInfo=True

            elif key=="proof":
                m_Proof=value

            elif key=="tauremove":
                m_TauRemove=True

            elif key=="minvar":
                m_MinVar=True
                
            elif key=="corrections":
                m_Correct=True

            elif key=="weights":
                m_Weight=True

            elif key=="minparinfo":
                m_MinParInfo=True

            elif key=="gridjob":
                m_Grid=True
                
            elif key=="tag":
                m_Tag=value

            elif key=="ntuptau":
                m_NtupTau=True

    if not m_ChanName:
        print "Channel Name needed: please provi it -> channame=[ggH,data,...]"
        sys.exit(0)
    if not m_Year:
        print "Year needed: Please provide it! -> year=[2011,2012]"
        sys.exit(0)
    if not m_Type:
        print "Data Type needed: Please provide it! -> type=[data,mc]"
        sys.exit(0)
    if not m_SimulVer:
        print "MC Version needed: Please provide it! -> simultver=[mc11d,mc12a,...]"
        sys.exit(0)
    if m_Type=="data" and not m_DataStream:
        print "Data Stream needed: Please provide it! -> datastream=[Muon,Egamma]"
        sys.exit(0)
        
    if not m_Path:
        print "Using default path"
        m_Path="./"
    if not m_List:
        print "Using default List Name"
        m_List="input.txt"
    if not m_Debug:
        m_Debug=False
    if not m_WriteInfo:
        m_WriteInfo=False
    if not m_Proof:
        m_Proof=""
    if not m_TauRemove:
        m_TauRemove=False
    if not m_MinVar:
        m_MinVar=False
    if not m_Correct:
        m_Correct=False
    if not m_Weight:
        m_Weight=False
    if not m_MinParInfo:
        m_MinParInfo=False
    if m_Grid and not m_Tag:
        print "Sample Tag needed when submitting on Grid: please provide it -> tag=V1.0"
        sys.exit(0)
    if not m_Grid:
        m_Grid=False
    if not m_NtupTau:
        m_NtupTau=False
    if m_Type=="data":
        m_Correct=False
        m_Weight=False
        print "Running on Data: Switching Off MC Corrections/Weights - Not Applicable!"
        
    RunH4lNtupleCreator(m_ChanName,m_Year,m_Type,m_SimulVer,
                        m_DataStream,m_Path,m_List,m_Debug,
                        m_WriteInfo,m_Proof,m_TauRemove,m_MinVar,
                        m_Correct,m_Weight,m_MinParInfo,m_Grid,
                        m_Tag,m_NtupTau)



def RunH4lNtupleCreator(m_channame,
                        m_year,
                        m_type,
                        m_simulver,
                        m_datastream,
                        m_path,
                        m_list,
                        m_debug,
                        m_writeinfo,
                        m_proof,
                        m_tauremove,
                        m_minvar,
                        m_correct,
                        m_weight,
                        m_minparinfo,
                        m_grid,
                        m_tag,
                        m_ntuptau):

    #Configuration
    if m_type=="data":
        PostFix = "_"+m_datastream
    else:
        PostFix = ""

    ChanName   = m_channame
    Year       = m_year
    Type       = m_type
    SimulVer   = m_simulver
    DataStream = m_datastream
    OutName    = ""
    Path       = m_path
    List       = m_list
    Debug      = m_debug
    WriteInfo  = m_writeinfo
    Proof      = m_proof
    TauRemove  = m_tauremove
    MinVar     = m_minvar
    Correct    = m_correct
    Weight     = m_weight
    MinParInfo = m_minparinfo
    Grid       = m_grid
    Tag        = m_tag
    NtupTau    = m_ntuptau

    #Setting Needed Options
    command  = "./H4l --year "+Year+" --type "+Type+" --mctype "+SimulVer
    if not Grid:
        command += " --channel "+ChanName
    if Grid:
        OutName="sample.root"
    else:
        OutName=ChanName+PostFix+".root"
    command += " --outname "+OutName

    #Setting Other Options
    if Path!="./":
        if Grid:
            print "Warning: for Grid Submission Path option unavailable:"
            print "         setting default value"
        else:
            command += " --path "+Path
    if List!="input.txt":
        if Grid:
            print "Warning: for Grid Submission List options unavailable:"
            print "         setting default value"
        else:
            command += " --list "+List
    if Debug==True:
        command += " -g"
    if WriteInfo==True:
        print "Write information enabled!"
        command += " --infoFile "+ChanName+PostFix+".dat"
    if Proof!="":
        print "Proof mode enabled: "+Proof
        command += " --proof "+Proof
    if TauRemove==True:
        print "Tau Event Skipping enabled!"
        command += " --notau"
    if MinVar==True:
        print "Using Minimal Set of Variables!"
        command += " --minVar"
    if Correct==True:
        print "Applying MC Corrections!"
        command += " --useCorr"
    if Weight==True:
        print "Applying MC Weights!"
        command += " --useWeight"
    if MinParInfo==True:
        print "Using Particle Minimal Information!"
        command += " --useParMinInfo"
    if NtupTau==True:
        print "Using NTUP TAU samples!"
        command += " --useNTUPTAU"
        
    if not Grid:
        print "Submitting Job on Local machine ... "
        if not os.environ.get('ROOTCOREBIN'):
            print "Setting ROOTCOREBIN Environment varables ... "
            os.environ["ROOTCOREBIN"]="Utils/Inputs"

        #print command
        os.system(command)
        print "    Local Submission Done!"
    else:
        print "Submitting Job on the Grid ... "
        
        output = subprocess.check_output("voms-proxy-info --all | grep nickname", shell=True)
        nickname=string.split(output)[4]
        print "     username that will be used is : "+nickname
        
        print "     Starting to prepare H4lNtupleCreator.tar.gz"
        os.chdir("../")
        cmd_tar  = " tar --exclude-vcs --exclude \'*.so\' --exclude \'*.o\' --exclude=H4l --exclude=H4lNtupleCreator/prepare_panda "
        cmd_tar += "-czf H4lNtupleCreator.tar.gz H4lNtupleCreator; "
        cmd_tar += "mv H4lNtupleCreator.tar.gz H4lNtupleCreator/prepare_panda ; "
        os.system(cmd_tar)
        os.chdir("H4lNtupleCreator/prepare_panda/")
        os.system("pwd")
        
        print "     Opening Datasets List "
        if not os.path.isfile("Datasets.list"):
            print "Dataset List file not found ... aborting"
            sys.exit(0)
        f = open("Datasets.list", "r")
        files=f.readlines()

        for f in files:
            input=f.rstrip("\n")
            if f!="":
                index=input.index("merge")#"NTUP_HSG2")
                new_in=input[0:index-1]
                output='user.'+nickname+'.'+new_in+'_'+Tag
                print ""
                print "Submitting "+output
                grcmd  = 'prun --exec \"echo %IN > inputFiles.txt; tar xzvf H4lNtupleCreator.tar.gz; '
                grcmd += 'cd H4lNtupleCreator; cp ../*.root* . ; source SetupGrid.sh; pwd; '
                grcmd += command+' --channel '+new_in+' ; '
                grcmd += 'cp sample.root ../ \"'
                grcmd += ' --express --maxFileSize 1099511627776 '
                grcmd += ' --rootVer=5.34/22 --cmtConfig=x86_64-slc6-gcc48-opt '
                grcmd += ' --inDS '+input
                grcmd += ' --skipScout  --nGBPerJob 9 --outputs=XYZ:sample.root --outDS '+output
                #print grcmd
                os.system(grcmd)
                print "    Grid Submission Done!"

        os.system('rm H4lNtupleCreator.tar.gz')

        
def ShowDoc():
    print ""
    print "Script to run H4lNtupleCreator over all datasets!"
    print "  Usage   : ./RunH4lNtupleCreator.py [Options]!"
    print "  Needed Options : "
    print "                   channame=JHU_ggH125             : specify channel name"
    print "                   year=[2011,2012]                : year of data/MC or selection to run"
    print "                   type=[data,mc]                  : type of data"
    print "                   simulver=[mc11d,mc12c,...]      : specify simulation version (Also when running on data)"
    print "                   datastream=[Muon,Egamma]        : nedeed if type=data, data Stream"
    print ""
    print "  Other Options  : "
    print "                   path=/tmp/sampleLists/          : specify a different path to sample list: default is ./"
    print "                   list=Data.txt                   : specify list of files to process: default is input.txt"
    print "                   debug=[True,False]              : enable/disable debug mode"
    print "                   writeinfo=[True,False]          : enable/disable writing information on file:"
    print "                                                   : (Selected Leptons-Jets, Event CutFlow)"
    print "                   proof=[lite,pod,pod_panda]      : enable/disable proof mode"
    print "                   tauremove=[True,False]          : enable/disable skipping events with taus"
    print "                   minvar=[True,False]             : enable/disable using minimal set of variables"
    print "                   corrections=[True,False]        : enable/disable MC corrections (smearing,scale factors,etc)"
    print "                   weights=[True,False]            : enable/disable MC weights (vertex,pileup,trigger,etc)"
    print "                   minparinfo=[True,False]         : enable/disable minimal Particles Information"
    print "                   gridjob=[True,False]            : enable/disable Grid Submission"
    print "                   tag=V1.0                        : Set output Grid sample tag"
    print "                   ntuptau=[True,False]            : Set NTUP TAU reading"
    print ""
    print "Examples:"
    print "   1) Run over data (default path and list) the 2011 selection:"
    print "          ./RunH4lNtupleCreator.py channel=data_PeriodB year=2011 type=data datastream=Muon"
    print "          simultype=mc11d outname=PeriodB.root"
    print ""
    print "   2) Run over MC (default path and list) the 2012 selection:"
    print "          ./RunH4lNtupleCreator.py channel=ggF_125 year=2012 type=mc simultype=mc12c"
    print "          outname=ggF_125.root"
    print ""
    print "   3) Run over MC (default path and list) the 2012 analysis enabling MC corrections"
    print "      and weights:"
    print "          ./RunH4lNtupleCreator.py channel=ggF_125 year=2012 type=mc simultype=mc12c"
    print "          corrections=True weights=True outname=ggF_125.root"


if __name__ == "__main__":
    main()
