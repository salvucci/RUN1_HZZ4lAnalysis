#!/usr/bin/python
import os
import sys
from packagesDef import *

#Build packages
print "\n"
for pack in Packages:

    if pack[0]=="egammaEvent":
        continue
    print "     Compiling package "+pack[0]
    os.chdir(PackagesDir+pack[0]+"/cmt")
    retval = os.getcwd()

    print "Directory changed successfully %s" % retval
    
    os.system("make -f "+Makefile)
    print "  Package "+pack[0]+" compiled!!"
    os.chdir("../../../")
    retval = os.getcwd()

    print "Directory changed successfully %s" % retval

    print ""
    
print "PackageCompile:: Everything Done!!"
