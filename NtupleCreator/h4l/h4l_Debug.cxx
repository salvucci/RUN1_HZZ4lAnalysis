#include "h4l.h"
/*ROOT Headers*/
#include <TH1F.h>
/*Utils*/
#include "../Utils/CrossSections.h"

void h4l::SetDebug(bool debug){
  m_Debug=debug;
  if(m_Debug){
    std::cout<<"H4lNtupleCreator:: DEBUG MODE ON!!"<<std::endl;
  }
  else{
    std::cout<<"H4lNtupleCreator:: DEBUG MODE OFF!!"<<std::endl;
  }
}

void h4l::Print(std::string field){
  if(m_Debug){
    std::cout<<"DEBUG:: "+field<<std::endl;
  }
}

void h4l::PrintOut(int value){
  if(m_Debug){
    std::cout<<value;
  }
}

void h4l::PrintOut(std::string field){
  if(m_Debug){
    std::cout<<field;
  }
}

void h4l::PrintOut(float value){
  if(m_Debug){
    std::cout<<value;
  }
}

void h4l::PrintOut(double value){
  if(m_Debug){
    std::cout<<value;
  }
}

void h4l::PrintEndl(){
  if(m_Debug){
    std::cout<<""<<std::endl;
  }
}

void h4l::WriteElectronCounts(){

  Info<<"-----------------------------------------------------------------------"<<std::endl;
  Info<<"              Electron Counting: "<<m_channel<<"                             "<<std::endl;
  Info<<"-----------------------------------------------------------------------"<<std::endl;
  Info<<""<<std::endl;
  Info.width(27); Info<<"GSF_Elec"; Info<<std::endl;
  
  Info.width(15); Info<<"Vertex";
  Info.width(12); Info<<VertexElec<<std::endl;
  
  Info.width(15); Info<<"Trigger";
  Info.width(12); Info<<TriggerElec<<std::endl;
  
  Info.width(15); Info<<"Author";
  Info.width(12); Info<<Part.getElAuthor()<<std::endl;
  
  Info.width(15); Info<<"LoosePP";
  Info.width(12); Info<<Part.getElLoosePP()<<std::endl;
  
  Info.width(15); Info<<"Eta";
  Info.width(12); Info<<Part.getElEta()<<std::endl;
  
  Info.width(15); Info<<"Et";
  Info.width(12); Info<<Part.getElEt()<<std::endl;
   
  Info.width(15); Info<<"OQ";
  Info.width(12); Info<<Part.getElOQ()<<std::endl;
   
  Info.width(15); Info<<"z0";
  Info.width(12); Info<<Part.getElZ0()<<std::endl;
   
  Info.width(15); Info<<"e-e Overlap";
  Info.width(12); Info<<Part.getElOverlap()<<std::endl;
  
  if(m_year==YEAR_2012){
    Info.width(15); Info<<"e-e CL Overlap";
    Info.width(12); Info<<Part.getElOverlapCl()<<std::endl;
  }
  
  Info.width(15); Info<<"e-mu Overlap";
  Info.width(12); Info<<Part.getElMuOverlap()<<std::endl;
  
  Info<<""<<std::endl;
  Info<<"    Electrons vector size: "<<nelectrons<<std::endl;

}

void h4l::WriteMuonCounts(){

  Info<<"-----------------------------------------------------------------------"<<std::endl;
  Info<<"              Muon Counting: "<<m_channel<<"                             "<<std::endl;
  Info<<"-----------------------------------------------------------------------"<<std::endl;
  Info<<""<<std::endl;
  Info.width(27); Info<<"CB+ST";       
  Info.width(12); Info<<"CB+ST+SA"; 
  Info.width(15); Info<<"CB+ST+SA+CA"; Info<<std::endl;

  Info.width(15); Info<<"Vertex";
  Info.width(12); Info<<VertexStaco;
  Info.width(12); Info<<VertexStaco;
  Info.width(12); Info<<VertexStaco+VertexCalo<<std::endl;
  
  Info.width(15); Info<<"Trigger";
  Info.width(12); Info<<TriggerStaco;
  Info.width(12); Info<<TriggerStaco;
  Info.width(12); Info<<TriggerStaco+TriggerCalo<<std::endl;

  Info.width(15); Info<<"Author";
  Info.width(12); Info<<Part.getStacoAthCBTag();
  Info.width(12); Info<<Part.getStacoAthCBTag()+Part.getStacoAthSA();
  Info.width(12); Info<<Part.getStacoAthCBTag()+Part.getStacoAthSA()+Part.getCaloAth()<<std::endl;
  
  Info.width(15); Info<<"pT";
  Info.width(12); Info<<Part.getStacoPtCBTag();
  Info.width(12); Info<<Part.getStacoPtCBTag()+Part.getStacoPtSA();
  Info.width(12); Info<<Part.getStacoPtCBTag()+Part.getStacoPtSA()+Part.getCaloPt()<<std::endl;
  
  Info.width(15); Info<<"BLayer";
  Info.width(12); Info<<Part.getStacoBl();
  Info.width(12); Info<<Part.getStacoBl()+Part.getStacoMS();
  Info.width(12); Info<<Part.getStacoBl()+Part.getStacoMS()+Part.getCaloBl()<<std::endl;
  
  Info.width(15); Info<<"Pixel";
  Info.width(12); Info<<Part.getStacoPxl();
  Info.width(12); Info<<Part.getStacoPxl()+Part.getStacoMS();
  Info.width(12); Info<<Part.getStacoPxl()+Part.getStacoMS()+Part.getCaloPxl()<<std::endl;
  
  Info.width(15); Info<<"SCT";
  Info.width(12); Info<<Part.getStacoSCT();
  Info.width(12); Info<<Part.getStacoSCT()+Part.getStacoMS();
  Info.width(12); Info<<Part.getStacoSCT()+Part.getStacoMS()+Part.getCaloSCT()<<std::endl;
  
  Info.width(15); Info<<"Holes";
  Info.width(12); Info<<Part.getStacoHol();
  Info.width(12); Info<<Part.getStacoHol()+Part.getStacoMS();
  Info.width(12); Info<<Part.getStacoHol()+Part.getStacoMS()+Part.getCaloHol()<<std::endl;
  
  Info.width(15); Info<<"TRT/Outlayers";
  Info.width(12); Info<<Part.getStacoTRT();
  Info.width(12); Info<<Part.getStacoTRT()+Part.getStacoMS();
  Info.width(12); Info<<Part.getStacoTRT()+Part.getStacoMS()+Part.getCaloTRT()<<std::endl;
  
  Info.width(15); Info<<"d0/z0";
  Info.width(12); Info<<Part.getStacoD0Z0();
  Info.width(12); Info<<Part.getStacoD0Z0()+Part.getStacoMS();
  Info.width(12); Info<<Part.getStacoD0Z0()+Part.getStacoMS()+Part.getCaloD0Z0()<<std::endl;

  Info.width(15); Info<<"Calo Overlap";
  Info.width(12); Info<<Part.getStacoD0Z0();
  Info.width(12); Info<<Part.getStacoD0Z0()+Part.getStacoMS();
  Info.width(12); Info<<Part.getStacoD0Z0()+Part.getStacoMS()+Part.getOverlapCalo()<<std::endl;
  
  if(m_year==YEAR_2012){
    Info.width(15); Info<<"St/Sa Overlap";
    Info.width(12); Info<<Part.getStacoD0Z0();
    Info.width(12); Info<<Part.getStacoD0Z0()+Part.getStacoMS();
    Info.width(12); Info<<Part.getOverlapSTSA()<<std::endl;
  }
  
  Info<<""<<std::endl;
  Info<<"    Muons vector size: "<<nmuons<<std::endl;

}

void h4l::WriteTauCounts(){

  Info<<"-----------------------------------------------------------------------"<<std::endl;
  Info<<"              Tau Counting: "<<m_channel<<"                            "<<std::endl;
  Info<<"-----------------------------------------------------------------------"<<std::endl;
  Info<<""<<std::endl;
  Info.width(27); Info<<"Tau"; Info<<std::endl;
  
  Info.width(15); Info<<"Vertex";
  Info.width(12); Info<<VertexTau<<std::endl;
  
  Info.width(15); Info<<"Trigger";
  Info.width(12); Info<<TriggerTau<<std::endl;
  
  Info.width(15); Info<<"Pt";
  Info.width(12); Info<<Part.getTauPt()<<std::endl;
  
  Info.width(15); Info<<"Eta";
  Info.width(12); Info<<Part.getTauEta()<<std::endl;
  
  Info.width(15); Info<<"Num. Track";
  Info.width(12); Info<<Part.getTauNTrk()<<std::endl;
  
  Info.width(15); Info<<"Charge";
  Info.width(12); Info<<Part.getTauCharge()<<std::endl;
   
  Info.width(15); Info<<"JetBDT";
  Info.width(12); Info<<Part.getTauJetBDT()<<std::endl;

  Info.width(15); Info<<"Elec. Veto";
  Info.width(12); Info<<Part.getTauElVeto()<<std::endl;
  
  Info.width(15); Info<<"Muon Veto";
  Info.width(12); Info<<Part.getTauMuVeto()<<std::endl;

  Info.width(15); Info<<"Tau-Mu Overlap";
  Info.width(12); Info<<Part.getTauMuOverlap()<<std::endl;

  Info.width(15); Info<<"Tau-e Overlap";
  Info.width(12); Info<<Part.getTauElOverlap()<<std::endl;
  
  Info<<""<<std::endl;
  Info<<"    Taus vector size: "<<ntaus<<std::endl;
  
}


void h4l::WriteJetCounts(){

  Info<<"-----------------------------------------------------------------------"<<std::endl;
  Info<<"              Jet Counting: "<<m_channel<<"                            "<<std::endl;
  Info<<"-----------------------------------------------------------------------"<<std::endl;
  Info<<""<<std::endl;
  Info.width(27); Info<<"AntiKt4TopoEM"; Info<<std::endl;
  
  Info.width(15); Info<<"Vertex";
  Info.width(12); Info<<VertexJet<<std::endl;
  
  Info.width(15); Info<<"Trigger";
  Info.width(12); Info<<TriggerJet<<std::endl;
  
  Info.width(15); Info<<"Pt";
  Info.width(12); Info<<Part.getJetPt()<<std::endl;
  
  Info.width(15); Info<<"Eta";
  Info.width(12); Info<<Part.getJetEta()<<std::endl;
  
  Info.width(15); Info<<"PileUp";
  Info.width(12); Info<<Part.getJetPU()<<std::endl;
  
  Info.width(15); Info<<"Cleaning";
  Info.width(12); Info<<Part.getJetClean()<<std::endl;
   
  Info.width(15); Info<<"Jet-e Overlap";
  Info.width(12); Info<<Part.getJetElOverlap()<<std::endl;
   
  Info<<""<<std::endl;
  Info<<"    Jets vector size: "<<njets<<std::endl;

}

void h4l::CutFlow(){

  Print("H4lNtupleCreator:: Doing Event CutFlow ... ");
  
  int LarOk=0, GrlOk=0, TilOk=0, EvtOk=0, TilcOk=0;
  if(m_type==DATA){
    GrlOk = 1;
  }
  else if(m_type==MC){
    GrlOk = 0;
  }
  double MCpileVxtW=1.;
  if(m_UseWeights){
    MCpileVxtW=pileupweight*mcweight*vxtweight;
  }
  
  if( isLArError==LarOk ){
    LArEvt+=MCpileVxtW;
    if( isTileError==TilOk ){
      TileEvt+=MCpileVxtW;
      if( isBadEvent==EvtOk ){
	BadEvt+=MCpileVxtW;
	if( isTileCorrupted==TilcOk ){
	  TilCorrEvt+=MCpileVxtW;
	  if( isInGRL==GrlOk ){
	    GRLEvt+=MCpileVxtW;
	    if( isVertexGood ){
	      VertexEvt+=MCpileVxtW;
	      if( isTrigGood ){
		TriggerEvt+=MCpileVxtW;
	      }
	    }
	  }
	}
      }
    }
  }
  CutFlow4mu();
  CutFlow4e();
  CutFlow2l2l();
  
}

void h4l::CutFlow4mu(){

  PrintOut("                          4mu selection "); PrintEndl();

  int Pos = -1;
    
  if(nmusel>3){
    isGoodCandidate_4mu=true;
    
    /*Quadruplets*/
    for(UInt_t q=0; q<Quadruplets.size(); q++){
      if( QuadSubChan.at(q)==1 ){
	isGoodSFOS_4mu=true;
	break;
      }
    }
    /*muon D0*/
    for(UInt_t q=0; q<IsD0Good.size(); q++){
      if( QuadSubChan.at(q)==1 && 
	  IsD0Good.at(q)==1      ){
	isGoodD0_4mu=true;
	break;
      }
    }
    /*Kinematic*/
    for(UInt_t q=0; q<IsKinematicGood.size(); q++){
      if( QuadSubChan.at(q)==1     &&
	  IsD0Good.at(q)==1        &&
	  IsKinematicGood.at(q)==1   ){
	isGoodKine_4mu=true;
	break;
      }
    }
    /*TrigMatch*/
    for(UInt_t q=0; q<IsTrigMatchGood.size(); q++){
      if( QuadSubChan.at(q)==1     &&
	  IsD0Good.at(q)==1        &&
	  IsKinematicGood.at(q)==1 &&
	  IsTrigMatchGood.at(q)==1   ){
	isGoodTrigMatch_4mu=true;
	break;
      }
    }
    /*Best quadruplet*/
    for(UInt_t q=0; q<IsBestQuadruplet.size(); q++){
      if( QuadSubChan.at(q)==1      &&
	  IsD0Good.at(q)==1         &&
	  IsKinematicGood.at(q)==1  &&
	  IsTrigMatchGood.at(q)==1  &&
	  IsBestQuadruplet.at(q)==1   ){
	isBestQuadruplet_4mu=true;
	break;
      }
    }
    /*Z1Good*/
    for(UInt_t q=0; q<IsZ1MassGood.size(); q++){
      if( QuadSubChan.at(q)==1      &&
	  IsD0Good.at(q)==1         &&
	  IsKinematicGood.at(q)==1  &&
	  IsTrigMatchGood.at(q)==1  &&
	  IsBestQuadruplet.at(q)==1 &&
	  IsZ1MassGood.at(q)==1       ){
	isGoodMz1_4mu=true;
	Pos=q;
	break;
      }
    }
    /*Z2Good*/
    for(UInt_t q=0; q<IsZ2MassGood.size(); q++){
      if( QuadSubChan.at(q)==1      &&
	  IsD0Good.at(q)==1         &&
	  IsKinematicGood.at(q)==1  &&
	  IsTrigMatchGood.at(q)==1  &&
	  IsBestQuadruplet.at(q)==1 &&
	  IsZ1MassGood.at(q)==1     &&
	  IsZ2MassGood.at(q)==1       ){
	isGoodMz2_4mu=true;
	Pos=q;
	break;
      }
    }
    /*DeltaR*/
    for(UInt_t q=0; q<IsDeltaRGood.size(); q++){
      if( QuadSubChan.at(q)==1      &&
	  IsD0Good.at(q)==1         &&
	  IsKinematicGood.at(q)==1  &&
	  IsTrigMatchGood.at(q)==1  &&
	  IsBestQuadruplet.at(q)==1 &&
	  IsZ1MassGood.at(q)==1     &&
	  IsZ2MassGood.at(q)==1     &&
	  IsDeltaRGood.at(q)==1       ){
	isGoodDR_4mu=true;
	Pos=q;
	break;
      }
    }
    /*JPsiVeto*/
    for(UInt_t q=0; q<IsJpsiVetoGood.size(); q++){
      if( QuadSubChan.at(q)==1      &&
	  IsD0Good.at(q)==1         &&   
	  IsKinematicGood.at(q)==1  &&
	  IsTrigMatchGood.at(q)==1  &&
	  IsBestQuadruplet.at(q)==1 &&
	  IsZ1MassGood.at(q)==1     &&
	  IsZ2MassGood.at(q)==1     &&
	  IsDeltaRGood.at(q)==1     &&
	  IsJpsiVetoGood.at(q)==1     ){
	isGoodJpsiVeto_4mu=true;
	Pos=q;
	break;
      }
    }
    /*TrackIso*/
    for(UInt_t q=0; q<IsTrackIsoGood.size(); q++){
      if( QuadSubChan.at(q)==1      &&
	  IsD0Good.at(q)==1         &&
	  IsKinematicGood.at(q)==1  &&
	  IsTrigMatchGood.at(q)==1  &&
	  IsBestQuadruplet.at(q)==1 &&
	  IsZ1MassGood.at(q)==1     && 
	  IsZ2MassGood.at(q)==1     &&
	  IsDeltaRGood.at(q)==1     &&
	  IsJpsiVetoGood.at(q)==1   &&
	  IsTrackIsoGood.at(q)==1     ){
	isGoodTrkIso_4mu=true;
	Pos=q;
	break;
      }
    }
    /*CaloIso*/
    for(UInt_t q=0; q<IsCaloIsoGood.size(); q++){
      if( QuadSubChan.at(q)==1      &&
	  IsD0Good.at(q)==1         &&
	  IsKinematicGood.at(q)==1  &&
	  IsTrigMatchGood.at(q)==1  &&
	  IsBestQuadruplet.at(q)==1 &&
	  IsZ1MassGood.at(q)==1     && 
	  IsZ2MassGood.at(q)==1     &&
	  IsDeltaRGood.at(q)==1     &&
	  IsJpsiVetoGood.at(q)==1   &&
	  IsTrackIsoGood.at(q)==1   &&
	  IsCaloIsoGood.at(q)==1      ){
	isGoodCaloIso_4mu=true;
	Pos=q;
	break;
      }
    }
    /*D0Sign*/
    for(UInt_t q=0; q<IsD0SignGood.size(); q++){
      if( QuadSubChan.at(q)==1      &&
	  IsD0Good.at(q)==1         &&
	  IsKinematicGood.at(q)==1  &&
	  IsTrigMatchGood.at(q)==1  &&
	  IsBestQuadruplet.at(q)==1 &&
	  IsZ1MassGood.at(q)==1     && 
	  IsZ2MassGood.at(q)==1     &&
	  IsDeltaRGood.at(q)==1     &&
	  IsJpsiVetoGood.at(q)==1   &&
	  IsTrackIsoGood.at(q)==1   &&
	  IsCaloIsoGood.at(q)==1    &&
	  IsD0SignGood.at(q)==1       ){
	isGoodD0Sign_4mu=true;
	Pos=q;
	break;
      }
    }
    
  }
  
  double quadW=1, MCpileVxtW=1.;
  if(m_UseWeights){
    MCpileVxtW=pileupweight*mcweight*vxtweight;
    if(Pos!=-1){
      quadW=TrigScaleFact.at(Pos)*EffyScaleFact.at(Pos);
    }
  }
  int LarOk=0, GrlOk=0, TilOk=0, EvtOk=0, TilcOk=0;
  if(m_type==DATA){
    GrlOk = 1;
  }
  else if(m_type==MC){
    GrlOk = 0;
  }
  if( isLArError==LarOk ){
    if(isTileError==TilOk){
      if(isBadEvent==EvtOk){
	if( isTileCorrupted==TilcOk ){
	  if( isInGRL==GrlOk ){
	    if( isVertexGood ){
	      if( isTrigGood ){
		if( isGoodCandidate_4mu ){ 
		  LeptonsSelEvt_4mu+=MCpileVxtW;
		  if( isGoodSFOS_4mu ){
		    SFOSEvt_4mu+=MCpileVxtW;
		    if( isGoodD0_4mu ){
		      D0Evt_4mu+=MCpileVxtW;
		      if( isGoodKine_4mu ){
			KinematicEvt_4mu+=MCpileVxtW;
			if( isGoodTrigMatch_4mu ){
			  TrigMatchEvt_4mu+=MCpileVxtW;
			  if( isBestQuadruplet_4mu ){
			    BestQuadEvt_4mu+=MCpileVxtW;
			    if( isGoodMz1_4mu ){
			      Z1MassEvt_4mu+=(MCpileVxtW*quadW);
			      if( isGoodMz2_4mu ){
				Z2MassEvt_4mu+=(MCpileVxtW*quadW);
				if( isGoodDR_4mu ){
				  DeltaREvt_4mu +=(MCpileVxtW*quadW);
				  if(isGoodJpsiVeto_4mu){
				    JPsiVetoEvt_4mu+=(MCpileVxtW*quadW);
				    if( isGoodTrkIso_4mu ){
				      TrkIsoEvt_4mu+=(MCpileVxtW*quadW);
				      if( isGoodCaloIso_4mu ){
					CaloIsoEvt_4mu+=(MCpileVxtW*quadW);
					if( isGoodD0Sign_4mu  ){
					  D0SignEvt_4mu+=(MCpileVxtW*quadW);
					}
				      }
				    }
				  }
				}
			      }
			    }
			  }
			}
		      }
		    }
		  }
		}
	      }
	  }
	  }
	}
      }
    }
  }
  
}

void h4l::CutFlow4e(){

  PrintOut("                          4e selection "); PrintEndl();
  
  int Pos = -1;
    
  if(nelecsel>3){
    isGoodCandidate_4e=true;
    
    /*Quadruplets*/
    for(UInt_t q=0; q<Quadruplets.size(); q++){
      if( QuadSubChan.at(q)==0 ){
	isGoodSFOS_4e=true;
	break;
      }
    }
    /*muon D0*/
    for(UInt_t q=0; q<IsD0Good.size(); q++){
      if( QuadSubChan.at(q)==0 && 
	  IsD0Good.at(q)==1      ){
	isGoodD0_4e=true;
	break;
      }
    }
    /*Kinematic*/
    for(UInt_t q=0; q<IsKinematicGood.size(); q++){
      if( QuadSubChan.at(q)==0     &&
	  IsD0Good.at(q)==1        &&
	  IsKinematicGood.at(q)==1   ){
	isGoodKine_4e=true;
	break;
      }
    }
    /*TrigMatch*/
    for(UInt_t q=0; q<IsTrigMatchGood.size(); q++){
      if( QuadSubChan.at(q)==0     &&
	  IsD0Good.at(q)==1        &&
	  IsKinematicGood.at(q)==1 &&
	  IsTrigMatchGood.at(q)==1   ){
	isGoodTrigMatch_4e=true;
	break;
      }
    }
    /*Best quadruplet*/
    for(UInt_t q=0; q<IsBestQuadruplet.size(); q++){
      if( QuadSubChan.at(q)==0      &&
	  IsD0Good.at(q)==1         &&
	  IsKinematicGood.at(q)==1  &&
	  IsTrigMatchGood.at(q)==1  &&
	  IsBestQuadruplet.at(q)==1   ){
	isBestQuadruplet_4e=true;
	break;
      }
    }
    /*Z1Good*/
    for(UInt_t q=0; q<IsZ1MassGood.size(); q++){
      if( QuadSubChan.at(q)==0      &&
	  IsD0Good.at(q)==1         &&
	  IsKinematicGood.at(q)==1  &&
	  IsTrigMatchGood.at(q)==1  &&
	  IsBestQuadruplet.at(q)==1 &&
	  IsZ1MassGood.at(q)==1       ){
	isGoodMz1_4e=true;
	Pos=q;
	break;
      }
    }
    /*Z2Good*/
    for(UInt_t q=0; q<IsZ2MassGood.size(); q++){
      if( QuadSubChan.at(q)==0      &&
	  IsD0Good.at(q)==1         &&
	  IsKinematicGood.at(q)==1  &&
	  IsTrigMatchGood.at(q)==1  &&
	  IsBestQuadruplet.at(q)==1 &&
	  IsZ1MassGood.at(q)==1     &&
	  IsZ2MassGood.at(q)==1       ){
	isGoodMz2_4e=true;
	Pos=q;
	break;
      }
    }
    /*DeltaR*/
    for(UInt_t q=0; q<IsDeltaRGood.size(); q++){
      if( QuadSubChan.at(q)==0      &&
	  IsD0Good.at(q)==1         &&
	  IsKinematicGood.at(q)==1  &&
	  IsTrigMatchGood.at(q)==1  &&
	  IsBestQuadruplet.at(q)==1 &&
	  IsZ1MassGood.at(q)==1     &&
	  IsZ2MassGood.at(q)==1     &&
	  IsDeltaRGood.at(q)==1       ){
	isGoodDR_4e=true;
	Pos=q;
	break;
      }
    }
    /*JPsiVeto*/
    for(UInt_t q=0; q<IsJpsiVetoGood.size(); q++){
      if( QuadSubChan.at(q)==0      &&
	  IsD0Good.at(q)==1         &&   
	  IsKinematicGood.at(q)==1  &&
	  IsTrigMatchGood.at(q)==1  &&
	  IsBestQuadruplet.at(q)==1 &&
	  IsZ1MassGood.at(q)==1     &&
	  IsZ2MassGood.at(q)==1     &&
	  IsDeltaRGood.at(q)==1     &&
	  IsJpsiVetoGood.at(q)==1     ){
	isGoodJpsiVeto_4e=true;
	Pos=q;
	break;
      }
    }
    /*TrackIso*/
    for(UInt_t q=0; q<IsTrackIsoGood.size(); q++){
      if( QuadSubChan.at(q)==0      &&
	  IsD0Good.at(q)==1         &&
	  IsKinematicGood.at(q)==1  &&
	  IsTrigMatchGood.at(q)==1  &&
	  IsBestQuadruplet.at(q)==1 &&
	  IsZ1MassGood.at(q)==1     && 
	  IsZ2MassGood.at(q)==1     &&
	  IsDeltaRGood.at(q)==1     &&
	  IsJpsiVetoGood.at(q)==1   &&
	  IsTrackIsoGood.at(q)==1     ){
	isGoodTrkIso_4e=true;
	Pos=q;
	break;
      }
    }
    /*CaloIso*/
    for(UInt_t q=0; q<IsCaloIsoGood.size(); q++){
      if( QuadSubChan.at(q)==0      &&
	  IsD0Good.at(q)==1         &&
	  IsKinematicGood.at(q)==1  &&
	  IsTrigMatchGood.at(q)==1  &&
	  IsBestQuadruplet.at(q)==1 &&
	  IsZ1MassGood.at(q)==1     && 
	  IsZ2MassGood.at(q)==1     &&
	  IsDeltaRGood.at(q)==1     &&
	  IsJpsiVetoGood.at(q)==1   &&
	  IsTrackIsoGood.at(q)==1   &&
	  IsCaloIsoGood.at(q)==1      ){
	isGoodCaloIso_4e=true;
	Pos=q;
	break;
      }
    }
    /*D0Sign*/
    for(UInt_t q=0; q<IsD0SignGood.size(); q++){
      if( QuadSubChan.at(q)==0      &&
	  IsD0Good.at(q)==1         &&
	  IsKinematicGood.at(q)==1  &&
	  IsTrigMatchGood.at(q)==1  &&
	  IsBestQuadruplet.at(q)==1 &&
	  IsZ1MassGood.at(q)==1     && 
	  IsZ2MassGood.at(q)==1     &&
	  IsDeltaRGood.at(q)==1     &&
	  IsJpsiVetoGood.at(q)==1   &&
	  IsTrackIsoGood.at(q)==1   &&
	  IsCaloIsoGood.at(q)==1    &&
	  IsD0SignGood.at(q)==1       ){
	isGoodD0Sign_4e=true;
	Pos=q;
	break;
      }
    }
    
  }
  
  double quadW=1, MCpileVxtW=1.;
  if(m_UseWeights){
    MCpileVxtW=pileupweight*mcweight*vxtweight;
    if(Pos!=-1){
      quadW=TrigScaleFact.at(Pos)*EffyScaleFact.at(Pos);
    }
  }
  int LarOk=0, GrlOk=0, TilOk=0, EvtOk=0, TilcOk=0;
  if(m_type==DATA){
    GrlOk = 1;
  }
  else if(m_type==MC){
    GrlOk = 0;
  }
  if( isLArError==LarOk ){
    if(isTileError==TilOk){
      if(isBadEvent==EvtOk){
	if( isTileCorrupted==TilcOk ){
	  if( isInGRL==GrlOk ){
	    if( isVertexGood ){
	      if( isTrigGood ){
		if( isGoodCandidate_4e ){ 
		  LeptonsSelEvt_4e+=MCpileVxtW;
		  if( isGoodSFOS_4e ){
		    SFOSEvt_4e+=MCpileVxtW;
		    if( isGoodD0_4e ){
		      D0Evt_4e+=MCpileVxtW;
		      if( isGoodKine_4e ){
			KinematicEvt_4e+=MCpileVxtW;
			if( isGoodTrigMatch_4e ){
			  TrigMatchEvt_4e+=MCpileVxtW;
			  if( isBestQuadruplet_4e ){
			    BestQuadEvt_4e+=MCpileVxtW;
			    if( isGoodMz1_4e ){
			      Z1MassEvt_4e+=(MCpileVxtW*quadW);
			      if( isGoodMz2_4e ){
				Z2MassEvt_4e+=(MCpileVxtW*quadW);
				if( isGoodDR_4e ){
				  DeltaREvt_4e +=(MCpileVxtW*quadW);
				  if(isGoodJpsiVeto_4e){
				    JPsiVetoEvt_4e+=(MCpileVxtW*quadW);
				    if( isGoodTrkIso_4e ){
				      TrkIsoEvt_4e+=(MCpileVxtW*quadW);
				      if( isGoodCaloIso_4e ){
					CaloIsoEvt_4e+=(MCpileVxtW*quadW);
					if( isGoodD0Sign_4e  ){
					  D0SignEvt_4e+=(MCpileVxtW*quadW);
					}
				      }
				    }
				  }
				}
			      }
			    }
			  }
			}
		      }
		    }
		  }
		}
	      }
	    }
	  }
	}
      }
    }
  }
  
}

void h4l::CutFlow2l2l(){

  PrintOut("                          2e2mu/2mu2e selection "); PrintEndl();
  
  int Pos = -1;
    
  if(nmusel>1 && nelecsel>1){
    isGoodCandidate_2l2l=true;
    
    /*Quadruplets*/
    for(UInt_t q=0; q<Quadruplets.size(); q++){
      if( (QuadSubChan.at(q)==2 || QuadSubChan.at(q)==3) ){
	isGoodSFOS_2l2l=true;
	break;
      }
    }
    /*muon D0*/
    for(UInt_t q=0; q<IsD0Good.size(); q++){
      if( (QuadSubChan.at(q)==2 || QuadSubChan.at(q)==3) && 
	  IsD0Good.at(q)==1                                ){
	isGoodD0_2l2l=true;
	break;
      }
    }
    /*Kinematic*/
    for(UInt_t q=0; q<IsKinematicGood.size(); q++){
      if( (QuadSubChan.at(q)==2 || QuadSubChan.at(q)==3) && 
	  IsD0Good.at(q)==1                              &&
	  IsKinematicGood.at(q)==1                         ){
	isGoodKine_2l2l=true;
	break;
      }
    }
    /*TrigMatch*/
    for(UInt_t q=0; q<IsTrigMatchGood.size(); q++){
      if( (QuadSubChan.at(q)==2 || QuadSubChan.at(q)==3) && 
	  IsD0Good.at(q)==1                              &&
	  IsKinematicGood.at(q)==1                       &&
	  IsTrigMatchGood.at(q)==1                         ){
	isGoodTrigMatch_2l2l=true;
	break;
      }
    }
    /*Best quadruplet*/
    for(UInt_t q=0; q<IsBestQuadruplet.size(); q++){
      if( (QuadSubChan.at(q)==2 || QuadSubChan.at(q)==3) && 
	  IsD0Good.at(q)==1                              &&
	  IsKinematicGood.at(q)==1                       &&
	  IsTrigMatchGood.at(q)==1                       &&
	  IsBestQuadruplet.at(q)==1                        ){
	isBestQuadruplet_2l2l=true;
	break;
      }
    }
    /*Z1Good*/
    for(UInt_t q=0; q<IsZ1MassGood.size(); q++){
      if( (QuadSubChan.at(q)==2 || QuadSubChan.at(q)==3) && 
	  IsD0Good.at(q)==1                              &&
	  IsKinematicGood.at(q)==1                       &&
	  IsTrigMatchGood.at(q)==1                       &&
	  IsBestQuadruplet.at(q)==1                      &&
	  IsZ1MassGood.at(q)==1                            ){
	isGoodMz1_2l2l=true;
	Pos=q;
	break;
      }
    }
    /*Z2Good*/
    for(UInt_t q=0; q<IsZ2MassGood.size(); q++){
      if( (QuadSubChan.at(q)==2 || QuadSubChan.at(q)==3) && 
	  IsD0Good.at(q)==1                              &&
	  IsKinematicGood.at(q)==1                       &&
	  IsTrigMatchGood.at(q)==1                       &&
	  IsBestQuadruplet.at(q)==1                      &&
	  IsZ1MassGood.at(q)==1                          &&
	  IsZ2MassGood.at(q)==1                            ){
	isGoodMz2_2l2l=true;
	Pos=q;
	break;
      }
    }
    /*DeltaR*/
    for(UInt_t q=0; q<IsDeltaRGood.size(); q++){
      if( (QuadSubChan.at(q)==2 || QuadSubChan.at(q)==3) && 
	  IsD0Good.at(q)==1                              &&
	  IsKinematicGood.at(q)==1                       &&
	  IsTrigMatchGood.at(q)==1                       &&
	  IsBestQuadruplet.at(q)==1                      &&
	  IsZ1MassGood.at(q)==1                          &&
	  IsZ2MassGood.at(q)==1                          &&
	  IsDeltaRGood.at(q)==1                            ){
	isGoodDR_2l2l=true;
	Pos=q;
	break;
      }
    }
    /*JPsiVeto*/
    for(UInt_t q=0; q<IsJpsiVetoGood.size(); q++){
      if( (QuadSubChan.at(q)==2 || QuadSubChan.at(q)==3) && 
	  IsD0Good.at(q)==1                              &&   
	  IsKinematicGood.at(q)==1                       &&
	  IsTrigMatchGood.at(q)==1                       &&
	  IsBestQuadruplet.at(q)==1                      &&
	  IsZ1MassGood.at(q)==1                          &&
	  IsZ2MassGood.at(q)==1                          &&
	  IsDeltaRGood.at(q)==1                          &&
	  IsJpsiVetoGood.at(q)==1                          ){
	isGoodJpsiVeto_2l2l=true;
	Pos=q;
	break;
      }
    }
    /*TrackIso*/
    for(UInt_t q=0; q<IsTrackIsoGood.size(); q++){
      if( (QuadSubChan.at(q)==2 || QuadSubChan.at(q)==3) && 
	  IsD0Good.at(q)==1                              &&
	  IsKinematicGood.at(q)==1                       &&
	  IsTrigMatchGood.at(q)==1                       &&
	  IsBestQuadruplet.at(q)==1                      &&
	  IsZ1MassGood.at(q)==1                          && 
	  IsZ2MassGood.at(q)==1                          &&
	  IsDeltaRGood.at(q)==1                          &&
	  IsJpsiVetoGood.at(q)==1                        &&
	  IsTrackIsoGood.at(q)==1                          ){
	isGoodTrkIso_2l2l=true;
	Pos=q;
	break;
      }
    }
    /*CaloIso*/
    for(UInt_t q=0; q<IsCaloIsoGood.size(); q++){
      if( (QuadSubChan.at(q)==2 || QuadSubChan.at(q)==3) && 
	  IsD0Good.at(q)==1                              &&
	  IsKinematicGood.at(q)==1                       &&
	  IsTrigMatchGood.at(q)==1                       &&
	  IsBestQuadruplet.at(q)==1                      &&
	  IsZ1MassGood.at(q)==1                          && 
	  IsZ2MassGood.at(q)==1                          &&
	  IsDeltaRGood.at(q)==1                          &&
	  IsJpsiVetoGood.at(q)==1                        &&
	  IsTrackIsoGood.at(q)==1                        &&
	  IsCaloIsoGood.at(q)==1                           ){
	isGoodCaloIso_2l2l=true;
	Pos=q;
	break;
      }
    }
    /*D0Sign*/
    for(UInt_t q=0; q<IsD0SignGood.size(); q++){
      if( (QuadSubChan.at(q)==2 || QuadSubChan.at(q)==3) && 
	  IsD0Good.at(q)==1                              &&
	  IsKinematicGood.at(q)==1                       &&
	  IsTrigMatchGood.at(q)==1                       &&
	  IsBestQuadruplet.at(q)==1                      &&
	  IsZ1MassGood.at(q)==1                          && 
	  IsZ2MassGood.at(q)==1                          &&
	  IsDeltaRGood.at(q)==1                          &&
	  IsJpsiVetoGood.at(q)==1                        &&
	  IsTrackIsoGood.at(q)==1                        &&
	  IsCaloIsoGood.at(q)==1                         &&
	  IsD0SignGood.at(q)==1                            ){
	isGoodD0Sign_2l2l=true;
	Pos=q;
	break;
      }
    }
    
  }
  
  double quadW=1, MCpileVxtW=1.;
  if(m_UseWeights){
    MCpileVxtW=pileupweight*mcweight*vxtweight;
    if(Pos!=-1){
      quadW=TrigScaleFact.at(Pos)*EffyScaleFact.at(Pos);
    }
  }
  int LarOk=0, GrlOk=0;
  int TilOk=0, EvtOk=0;
  if(m_type==DATA){
    GrlOk = 1;
  }
  else if(m_type==MC){
    GrlOk = 0;
  }
  if( isLArError==LarOk ){
    if(isTileError==TilOk){
      if(isBadEvent==EvtOk){
	if( isInGRL==GrlOk ){
	  if( isVertexGood ){
	    if( isTrigGood ){
	      if( isGoodCandidate_2l2l ){ 
		LeptonsSelEvt_2l2l+=MCpileVxtW;
		if( isGoodSFOS_2l2l ){
		  SFOSEvt_2l2l+=MCpileVxtW;
		  if( isGoodD0_2l2l ){
		    D0Evt_2l2l+=MCpileVxtW;
		    if( isGoodKine_2l2l ){
		      KinematicEvt_2l2l+=MCpileVxtW;
		      if( isGoodTrigMatch_2l2l ){
			TrigMatchEvt_2l2l+=MCpileVxtW;
			if( isBestQuadruplet_2l2l ){
			  BestQuadEvt_2l2l+=MCpileVxtW;
			  if( isGoodMz1_2l2l ){
			    Z1MassEvt_2l2l+=(MCpileVxtW*quadW);
			    if( isGoodMz2_2l2l ){
			      Z2MassEvt_2l2l+=(MCpileVxtW*quadW);
			      if( isGoodDR_2l2l ){
				DeltaREvt_2l2l +=(MCpileVxtW*quadW);
				if(isGoodJpsiVeto_2l2l){
				  JPsiVetoEvt_2l2l+=(MCpileVxtW*quadW);
				  if( isGoodTrkIso_2l2l ){
				    TrkIsoEvt_2l2l+=(MCpileVxtW*quadW);
				    if( isGoodCaloIso_2l2l ){
				      CaloIsoEvt_2l2l+=(MCpileVxtW*quadW);
				      if( isGoodD0Sign_2l2l  ){
					D0SignEvt_2l2l+=(MCpileVxtW*quadW);
				      }
				    }
				  }
				}
			      }
			    }
			  }
			}
		      }
		    }
		  }
		}
	      }
	    }
	  }
	}
      }
    }
  }
  
}

void h4l::WriteCutFlow(){

  Info<<""<<std::endl;
  Info<<""<<std::endl;
  Info<<"-----------------------------------------------------------------------"<<std::endl;
  Info<<"              Event CutFlow: "<<m_channel<<"                             "<<std::endl;
  Info<<"-----------------------------------------------------------------------"<<std::endl;
  Info<<""<<std::endl;
  Info.width(27); Info<<"4mu"; 
  Info.width(12); Info<<"4e"; 
  Info.width(15); Info<<"2mu2e/2e2mu"<<std::endl;
  
  Info.width(15); Info<<"LArError";
  Info.width(12); Info<<LArEvt; 
  Info.width(12); Info<<LArEvt; 
  Info.width(12); Info<<LArEvt<<std::endl; 

  Info.width(15); Info<<"TileError";
  Info.width(12); Info<<TileEvt;
  Info.width(12); Info<<TileEvt;
  Info.width(12); Info<<TileEvt<<std::endl;
  
  Info.width(15); Info<<"Bad-Events";
  Info.width(12); Info<<BadEvt;
  Info.width(12); Info<<BadEvt;
  Info.width(12); Info<<BadEvt<<std::endl;

  Info.width(15); Info<<"TileCorr";
  Info.width(12); Info<<TilCorrEvt;
  Info.width(12); Info<<TilCorrEvt;
  Info.width(12); Info<<TilCorrEvt<<std::endl;

  Info.width(15); Info<<"GRL";
  Info.width(12); Info<<GRLEvt;
  Info.width(12); Info<<GRLEvt;
  Info.width(12); Info<<GRLEvt<<std::endl;
  
  Info.width(15); Info<<"Vertex";
  Info.width(12); Info<<VertexEvt;
  Info.width(12); Info<<VertexEvt;
  Info.width(12); Info<<VertexEvt<<std::endl;
  
  Info.width(15); Info<<"Trigger";
  Info.width(12); Info<<TriggerEvt;
  Info.width(12); Info<<TriggerEvt;
  Info.width(12); Info<<TriggerEvt<<std::endl;
  
  Info.width(15); Info<<"Sel. Leptons";
  Info.width(12); Info<<LeptonsSelEvt_4mu;
  Info.width(12); Info<<LeptonsSelEvt_4e;
  Info.width(12); Info<<LeptonsSelEvt_2l2l<<std::endl;

  Info.width(15); Info<<"SFOS";
  Info.width(12); Info<<SFOSEvt_4mu;
  Info.width(12); Info<<SFOSEvt_4e;
  Info.width(12); Info<<SFOSEvt_2l2l<<std::endl;
  
  Info.width(15); Info<<"D0";
  Info.width(12); Info<<D0Evt_4mu;
  Info.width(12); Info<<D0Evt_4e;
  Info.width(12); Info<<D0Evt_2l2l<<std::endl;

  Info.width(15); Info<<"Kinematic";
  Info.width(12); Info<<KinematicEvt_4mu;
  Info.width(12); Info<<KinematicEvt_4e;
  Info.width(12); Info<<KinematicEvt_2l2l<<std::endl;
  
  Info.width(15); Info<<"Trig. Match";
  Info.width(12); Info<<TrigMatchEvt_4mu;
  Info.width(12); Info<<TrigMatchEvt_4e;
  Info.width(12); Info<<TrigMatchEvt_2l2l<<std::endl;
   
  Info.width(15); Info<<"Best Quad.";
  Info.width(12); Info<<BestQuadEvt_4mu;
  Info.width(12); Info<<BestQuadEvt_4e;
  Info.width(12); Info<<BestQuadEvt_2l2l<<std::endl;
  
  Info.width(15); Info<<"Z1 Mass";
  Info.width(12); Info<<Z1MassEvt_4mu;
  Info.width(12); Info<<Z1MassEvt_4e;
  Info.width(12); Info<<Z1MassEvt_2l2l<<std::endl;
  
  Info.width(15); Info<<"Z2 Mass";
  Info.width(12); Info<<Z2MassEvt_4mu;
  Info.width(12); Info<<Z2MassEvt_4e;
  Info.width(12); Info<<Z2MassEvt_2l2l<<std::endl;
  
  Info.width(15); Info<<"DeltaR";
  Info.width(12); Info<<DeltaREvt_4mu;
  Info.width(12); Info<<DeltaREvt_4e;
  Info.width(12); Info<<DeltaREvt_2l2l<<std::endl;
  
  Info.width(15); Info<<"JPsi Veto";
  Info.width(12); Info<<JPsiVetoEvt_4mu;
  Info.width(12); Info<<JPsiVetoEvt_4e;
  Info.width(12); Info<<JPsiVetoEvt_2l2l<<std::endl;
  
  Info.width(15); Info<<"Track Iso.";
  Info.width(12); Info<<TrkIsoEvt_4mu;
  Info.width(12); Info<<TrkIsoEvt_4e;
  Info.width(12); Info<<TrkIsoEvt_2l2l<<std::endl;
  
  Info.width(15); Info<<"Calo Iso.";
  Info.width(12); Info<<CaloIsoEvt_4mu;
  Info.width(12); Info<<CaloIsoEvt_4e;
  Info.width(12); Info<<CaloIsoEvt_2l2l<<std::endl;
  
  Info.width(15); Info<<"D0 Sign.";
  Info.width(12); Info<<D0SignEvt_4mu;
  Info.width(12); Info<<D0SignEvt_4e;
  Info.width(12); Info<<D0SignEvt_2l2l<<std::endl;
  Info<<""<<std::endl;
  
}

void h4l::CreateInfoHistograms(){
  
  info->SetBinContent(1,Tprocessed);
  info->GetXaxis()->SetBinLabel(1,"Tot_Processed");
  if(m_type==MC){
    info->SetBinContent(2,TnegWeight);
    info->GetXaxis()->SetBinLabel(2,"Neg_Weight");
    info->SetBinContent(3,TposWeight);
    info->GetXaxis()->SetBinLabel(3,"Pos_Weight");
  }
    
  cutFlow_4mu->SetBinContent(1,LArEvt);
  cutFlow_4mu->GetXaxis()->SetBinLabel(1,"LArError");
  cutFlow_4mu->SetBinContent(2,TileEvt);
  cutFlow_4mu->GetXaxis()->SetBinLabel(2,"TileError");
  cutFlow_4mu->SetBinContent(3,BadEvt);
  cutFlow_4mu->GetXaxis()->SetBinLabel(3,"Bad-Events");
  cutFlow_4mu->SetBinContent(4,TilCorrEvt);
  cutFlow_4mu->GetXaxis()->SetBinLabel(4,"TileCorr");
  cutFlow_4mu->SetBinContent(5,GRLEvt);
  cutFlow_4mu->GetXaxis()->SetBinLabel(5,"GRL");
  cutFlow_4mu->SetBinContent(6,VertexEvt);
  cutFlow_4mu->GetXaxis()->SetBinLabel(6,"Vertex");
  cutFlow_4mu->SetBinContent(7,TriggerEvt);
  cutFlow_4mu->GetXaxis()->SetBinLabel(7,"Trigger");
  cutFlow_4mu->SetBinContent(8,LeptonsSelEvt_4mu);
  cutFlow_4mu->GetXaxis()->SetBinLabel(8,"Sel. Leptons");
  cutFlow_4mu->SetBinContent(9,SFOSEvt_4mu);
  cutFlow_4mu->GetXaxis()->SetBinLabel(9,"SFOS");
  cutFlow_4mu->SetBinContent(10,D0Evt_4mu);
  cutFlow_4mu->GetXaxis()->SetBinLabel(10,"D0");
  cutFlow_4mu->SetBinContent(11,KinematicEvt_4mu);
  cutFlow_4mu->GetXaxis()->SetBinLabel(11,"Kinematic");
  cutFlow_4mu->SetBinContent(12,TrigMatchEvt_4mu);
  cutFlow_4mu->GetXaxis()->SetBinLabel(12,"Trig. Match");
  cutFlow_4mu->SetBinContent(13,BestQuadEvt_4mu);
  cutFlow_4mu->GetXaxis()->SetBinLabel(13,"Best Quad.");
  cutFlow_4mu->SetBinContent(14,Z1MassEvt_4mu);
  cutFlow_4mu->GetXaxis()->SetBinLabel(14,"Z1 Mass");
  cutFlow_4mu->SetBinContent(15,Z2MassEvt_4mu);
  cutFlow_4mu->GetXaxis()->SetBinLabel(15,"Z2 Mass");
  cutFlow_4mu->SetBinContent(16,DeltaREvt_4mu);
  cutFlow_4mu->GetXaxis()->SetBinLabel(16,"DeltaR");
  cutFlow_4mu->SetBinContent(17,JPsiVetoEvt_4mu);
  cutFlow_4mu->GetXaxis()->SetBinLabel(17,"JPsiVeto");
  cutFlow_4mu->SetBinContent(18,TrkIsoEvt_4mu);
  cutFlow_4mu->GetXaxis()->SetBinLabel(18,"Track Iso");
  cutFlow_4mu->SetBinContent(19,CaloIsoEvt_4mu);
  cutFlow_4mu->GetXaxis()->SetBinLabel(19,"Calo Iso");
  cutFlow_4mu->SetBinContent(20,D0SignEvt_4mu);
  cutFlow_4mu->GetXaxis()->SetBinLabel(20,"D0 Sign.");

  cutFlow_4e->SetBinContent(1,LArEvt);
  cutFlow_4e->GetXaxis()->SetBinLabel(1,"LArError");
  cutFlow_4e->SetBinContent(2,TileEvt);
  cutFlow_4e->GetXaxis()->SetBinLabel(2,"TileError");
  cutFlow_4e->SetBinContent(3,BadEvt);
  cutFlow_4e->GetXaxis()->SetBinLabel(3,"Bad-Events");
  cutFlow_4e->SetBinContent(4,TilCorrEvt);
  cutFlow_4e->GetXaxis()->SetBinLabel(4,"TileCorr");
  cutFlow_4e->SetBinContent(5,GRLEvt);
  cutFlow_4e->GetXaxis()->SetBinLabel(5,"GRL");
  cutFlow_4e->SetBinContent(6,VertexEvt);
  cutFlow_4e->GetXaxis()->SetBinLabel(6,"Vertex");
  cutFlow_4e->SetBinContent(7,TriggerEvt);
  cutFlow_4e->GetXaxis()->SetBinLabel(7,"Trigger");
  cutFlow_4e->SetBinContent(8,LeptonsSelEvt_4e);
  cutFlow_4e->GetXaxis()->SetBinLabel(8,"Sel. Leptons");
  cutFlow_4e->SetBinContent(9,SFOSEvt_4e);
  cutFlow_4e->GetXaxis()->SetBinLabel(9,"SFOS");
  cutFlow_4e->SetBinContent(10,D0Evt_4e);
  cutFlow_4e->GetXaxis()->SetBinLabel(10,"D0");
  cutFlow_4e->SetBinContent(11,KinematicEvt_4e);
  cutFlow_4e->GetXaxis()->SetBinLabel(11,"Kinematic");
  cutFlow_4e->SetBinContent(12,TrigMatchEvt_4e);
  cutFlow_4e->GetXaxis()->SetBinLabel(12,"Trig. Match");
  cutFlow_4e->SetBinContent(13,BestQuadEvt_4e);
  cutFlow_4e->GetXaxis()->SetBinLabel(13,"Best Quad.");
  cutFlow_4e->SetBinContent(14,Z1MassEvt_4e);
  cutFlow_4e->GetXaxis()->SetBinLabel(14,"Z1 Mass");
  cutFlow_4e->SetBinContent(15,Z2MassEvt_4e);
  cutFlow_4e->GetXaxis()->SetBinLabel(15,"Z2 Mass");
  cutFlow_4e->SetBinContent(16,DeltaREvt_4e);
  cutFlow_4e->GetXaxis()->SetBinLabel(16,"DeltaR");
  cutFlow_4e->SetBinContent(17,JPsiVetoEvt_4e);
  cutFlow_4e->GetXaxis()->SetBinLabel(17,"JPsiVeto");
  cutFlow_4e->SetBinContent(18,TrkIsoEvt_4e);
  cutFlow_4e->GetXaxis()->SetBinLabel(18,"Track Iso");
  cutFlow_4e->SetBinContent(19,CaloIsoEvt_4e);
  cutFlow_4e->GetXaxis()->SetBinLabel(19,"Calo Iso");
  cutFlow_4e->SetBinContent(20,D0SignEvt_4e);
  cutFlow_4e->GetXaxis()->SetBinLabel(20,"D0 Sign.");

  cutFlow_2l2l->SetBinContent(1,LArEvt);
  cutFlow_2l2l->GetXaxis()->SetBinLabel(1,"LArError");
  cutFlow_2l2l->SetBinContent(2,TileEvt);
  cutFlow_2l2l->GetXaxis()->SetBinLabel(2,"TileError");
  cutFlow_2l2l->SetBinContent(3,BadEvt);
  cutFlow_2l2l->GetXaxis()->SetBinLabel(3,"Bad-Events");
  cutFlow_2l2l->SetBinContent(4,TilCorrEvt);
  cutFlow_2l2l->GetXaxis()->SetBinLabel(4,"TileCorr");
  cutFlow_2l2l->SetBinContent(5,GRLEvt);
  cutFlow_2l2l->GetXaxis()->SetBinLabel(5,"GRL");
  cutFlow_2l2l->SetBinContent(6,VertexEvt);
  cutFlow_2l2l->GetXaxis()->SetBinLabel(6,"Vertex");
  cutFlow_2l2l->SetBinContent(7,TriggerEvt);
  cutFlow_2l2l->GetXaxis()->SetBinLabel(7,"Trigger");
  cutFlow_2l2l->SetBinContent(8,LeptonsSelEvt_2l2l);
  cutFlow_2l2l->GetXaxis()->SetBinLabel(8,"Sel. Leptons");
  cutFlow_2l2l->SetBinContent(9,SFOSEvt_2l2l);
  cutFlow_2l2l->GetXaxis()->SetBinLabel(9,"SFOS");
  cutFlow_2l2l->SetBinContent(10,D0Evt_2l2l);
  cutFlow_2l2l->GetXaxis()->SetBinLabel(10,"D0");
  cutFlow_2l2l->SetBinContent(11,KinematicEvt_2l2l);
  cutFlow_2l2l->GetXaxis()->SetBinLabel(11,"Kinematic");
  cutFlow_2l2l->SetBinContent(12,TrigMatchEvt_2l2l);
  cutFlow_2l2l->GetXaxis()->SetBinLabel(12,"Trig. Match");
  cutFlow_2l2l->SetBinContent(13,BestQuadEvt_2l2l);
  cutFlow_2l2l->GetXaxis()->SetBinLabel(13,"Best Quad.");
  cutFlow_2l2l->SetBinContent(14,Z1MassEvt_2l2l);
  cutFlow_2l2l->GetXaxis()->SetBinLabel(14,"Z1 Mass");
  cutFlow_2l2l->SetBinContent(15,Z2MassEvt_2l2l);
  cutFlow_2l2l->GetXaxis()->SetBinLabel(15,"Z2 Mass");
  cutFlow_2l2l->SetBinContent(16,DeltaREvt_2l2l);
  cutFlow_2l2l->GetXaxis()->SetBinLabel(16,"DeltaR");
  cutFlow_2l2l->SetBinContent(17,JPsiVetoEvt_2l2l);
  cutFlow_2l2l->GetXaxis()->SetBinLabel(17,"JPsiVeto");
  cutFlow_2l2l->SetBinContent(18,TrkIsoEvt_2l2l);
  cutFlow_2l2l->GetXaxis()->SetBinLabel(18,"Track Iso");
  cutFlow_2l2l->SetBinContent(19,CaloIsoEvt_2l2l);
  cutFlow_2l2l->GetXaxis()->SetBinLabel(19,"Calo Iso");
  cutFlow_2l2l->SetBinContent(20,D0SignEvt_2l2l);
  cutFlow_2l2l->GetXaxis()->SetBinLabel(20,"D0 Sign.");
    
  ElecCount->SetBinContent(1,VertexElec);
  ElecCount->GetXaxis()->SetBinLabel(1,"Vertex");
  ElecCount->SetBinContent(2,TriggerElec);
  ElecCount->GetXaxis()->SetBinLabel(2,"Trigger");
  ElecCount->SetBinContent(3,Part.getElAuthor() );
  ElecCount->GetXaxis()->SetBinLabel(3,"Author");
  ElecCount->SetBinContent(4,Part.getElLoosePP() );
  ElecCount->GetXaxis()->SetBinLabel(4,"LoosePP");
  ElecCount->SetBinContent(5,Part.getElEta() );
  ElecCount->GetXaxis()->SetBinLabel(5,"Eta");
  ElecCount->SetBinContent(6,Part.getElEt() );
  ElecCount->GetXaxis()->SetBinLabel(6,"Et");
  ElecCount->SetBinContent(7,Part.getElOQ() );
  ElecCount->GetXaxis()->SetBinLabel(7,"ObjQual");
  ElecCount->SetBinContent(8,Part.getElZ0() );
  ElecCount->GetXaxis()->SetBinLabel(8,"z0");
  ElecCount->SetBinContent(9,Part.getElOverlap() );
  ElecCount->GetXaxis()->SetBinLabel(9,"e-e Overlap");
  if(m_year==YEAR_2012){
    ElecCount->SetBinContent(10,Part.getElOverlapCl() );
    ElecCount->GetXaxis()->SetBinLabel(10,"e-e CL Overlap");
    ElecCount->SetBinContent(11,Part.getElMuOverlap() );
    ElecCount->GetXaxis()->SetBinLabel(11,"e-mu Overlap");
  }
  else if(m_year==YEAR_2011){
    ElecCount->SetBinContent(10,Part.getElMuOverlap() );
    ElecCount->GetXaxis()->SetBinLabel(10,"e-mu Overlap");
  }

  MuCBST->SetBinContent(1,VertexStaco);
  MuCBST->GetXaxis()->SetBinLabel(1,"Vertex");
  MuCBST->SetBinContent(2,TriggerStaco);
  MuCBST->GetXaxis()->SetBinLabel(2,"Trigger");
  MuCBST->SetBinContent(3,Part.getStacoAthCBTag() );
  MuCBST->GetXaxis()->SetBinLabel(3,"Author");
  MuCBST->SetBinContent(4,Part.getStacoPtCBTag() );
  MuCBST->GetXaxis()->SetBinLabel(4,"pT");
  MuCBST->SetBinContent(5,Part.getStacoBl() );
  MuCBST->GetXaxis()->SetBinLabel(5,"BLayer");
  MuCBST->SetBinContent(6,Part.getStacoPxl() );
  MuCBST->GetXaxis()->SetBinLabel(6,"Pixel");
  MuCBST->SetBinContent(7,Part.getStacoSCT() );
  MuCBST->GetXaxis()->SetBinLabel(7,"SCT");
  MuCBST->SetBinContent(8,Part.getStacoHol() );
  MuCBST->GetXaxis()->SetBinLabel(8,"Holes");
  MuCBST->SetBinContent(9,Part.getStacoTRT() );
  MuCBST->GetXaxis()->SetBinLabel(9,"TRT/OutLayers");
  MuCBST->SetBinContent(10,Part.getStacoD0Z0() );
  MuCBST->GetXaxis()->SetBinLabel(10,"d0/z0");
  MuCBST->SetBinContent(11,Part.getStacoD0Z0() );
  MuCBST->GetXaxis()->SetBinLabel(11,"Calo Overlap");
  MuCBST->SetBinContent(12,Part.getStacoD0Z0() );
  MuCBST->GetXaxis()->SetBinLabel(12,"St/Sa Overlap");
  
  MuCBSTSA->SetBinContent(1,VertexStaco);
  MuCBSTSA->GetXaxis()->SetBinLabel(1,"Vertex");
  MuCBSTSA->SetBinContent(2,TriggerStaco);
  MuCBSTSA->GetXaxis()->SetBinLabel(2,"Trigger");
  MuCBSTSA->SetBinContent(3,Part.getStacoAthCBTag()+Part.getStacoAthSA() );
  MuCBSTSA->GetXaxis()->SetBinLabel(3,"Author");
  MuCBSTSA->SetBinContent(4,Part.getStacoPtCBTag()+Part.getStacoPtSA() );
  MuCBSTSA->GetXaxis()->SetBinLabel(4,"pT");
  MuCBSTSA->SetBinContent(5,Part.getStacoBl()+Part.getStacoMS() );
  MuCBSTSA->GetXaxis()->SetBinLabel(5,"BLayer");
  MuCBSTSA->SetBinContent(6,Part.getStacoPxl()+Part.getStacoMS() );
  MuCBSTSA->GetXaxis()->SetBinLabel(6,"Pixel");
  MuCBSTSA->SetBinContent(7,Part.getStacoSCT()+Part.getStacoMS() );
  MuCBSTSA->GetXaxis()->SetBinLabel(7,"SCT");
  MuCBSTSA->SetBinContent(8,Part.getStacoHol()+Part.getStacoMS() );
  MuCBSTSA->GetXaxis()->SetBinLabel(8,"Holes");
  MuCBSTSA->SetBinContent(9,Part.getStacoTRT()+Part.getStacoMS() );
  MuCBSTSA->GetXaxis()->SetBinLabel(9,"TRT/OutLayers");
  MuCBSTSA->SetBinContent(10,Part.getStacoD0Z0()+Part.getStacoMS() );
  MuCBSTSA->GetXaxis()->SetBinLabel(10,"d0/z0");
  MuCBSTSA->SetBinContent(11,Part.getStacoD0Z0()+Part.getStacoMS() );
  MuCBSTSA->GetXaxis()->SetBinLabel(11,"Calo Overlap");
  MuCBSTSA->SetBinContent(12,Part.getStacoD0Z0()+Part.getStacoMS() );
  MuCBSTSA->GetXaxis()->SetBinLabel(12,"St/Sa Overlap");
  
  MuCBSTSACA->SetBinContent(1,VertexStaco+VertexCalo);
  MuCBSTSACA->GetXaxis()->SetBinLabel(1,"Vertex");
  MuCBSTSACA->SetBinContent(2,TriggerStaco+TriggerCalo);
  MuCBSTSACA->GetXaxis()->SetBinLabel(2,"Trigger");
  MuCBSTSACA->SetBinContent(3,Part.getStacoAthCBTag()+Part.getStacoAthSA()+Part.getCaloAth() );
  MuCBSTSACA->GetXaxis()->SetBinLabel(3,"Author");
  MuCBSTSACA->SetBinContent(4,Part.getStacoPtCBTag()+Part.getStacoPtSA()+Part.getCaloPt() );
  MuCBSTSACA->GetXaxis()->SetBinLabel(4,"pT");
  MuCBSTSACA->SetBinContent(5,Part.getStacoBl()+Part.getStacoMS()+Part.getCaloBl() );
  MuCBSTSACA->GetXaxis()->SetBinLabel(5,"BLayer");
  MuCBSTSACA->SetBinContent(6,Part.getStacoPxl()+Part.getStacoMS()+Part.getCaloPxl() );
  MuCBSTSACA->GetXaxis()->SetBinLabel(6,"Pixel");
  MuCBSTSACA->SetBinContent(7,Part.getStacoSCT()+Part.getStacoMS()+Part.getCaloSCT() );
  MuCBSTSACA->GetXaxis()->SetBinLabel(7,"SCT");
  MuCBSTSACA->SetBinContent(8,Part.getStacoHol()+Part.getStacoMS()+Part.getCaloHol() );
  MuCBSTSACA->GetXaxis()->SetBinLabel(8,"Holes");
  MuCBSTSACA->SetBinContent(9,Part.getStacoTRT()+Part.getStacoMS()+Part.getCaloTRT() );
  MuCBSTSACA->GetXaxis()->SetBinLabel(9,"TRT/OutLayers");
  MuCBSTSACA->SetBinContent(10,Part.getStacoD0Z0()+Part.getStacoMS()+Part.getCaloD0Z0() );
  MuCBSTSACA->GetXaxis()->SetBinLabel(10,"d0/z0");
  MuCBSTSACA->SetBinContent(11,Part.getStacoD0Z0()+Part.getStacoMS()+Part.getOverlapCalo() );
  MuCBSTSACA->GetXaxis()->SetBinLabel(11,"Calo Overlap");
  MuCBSTSACA->SetBinContent(12,Part.getOverlapSTSA() );
  MuCBSTSACA->GetXaxis()->SetBinLabel(12,"St/Sa Overlap");

  TauCount->SetBinContent(1,VertexTau);
  TauCount->GetXaxis()->SetBinLabel(1,"Vertex");
  TauCount->SetBinContent(2,TriggerTau);
  TauCount->GetXaxis()->SetBinLabel(2,"Trigger");
  TauCount->SetBinContent(3,Part.getTauPt() );
  TauCount->GetXaxis()->SetBinLabel(3,"Pt");
  TauCount->SetBinContent(4,Part.getTauEta() );
  TauCount->GetXaxis()->SetBinLabel(4,"Eta");
  TauCount->SetBinContent(5,Part.getTauNTrk() );
  TauCount->GetXaxis()->SetBinLabel(5,"Num. Track");
  TauCount->SetBinContent(6,Part.getTauCharge() );
  TauCount->GetXaxis()->SetBinLabel(6,"Charge");
  TauCount->SetBinContent(7,Part.getTauJetBDT() );
  TauCount->GetXaxis()->SetBinLabel(7,"JetBDT");
  TauCount->SetBinContent(8,Part.getTauElVeto() );
  TauCount->GetXaxis()->SetBinLabel(8,"Elec. Veto");
  TauCount->SetBinContent(9,Part.getTauMuVeto() );
  TauCount->GetXaxis()->SetBinLabel(9,"Muon Veto");
  TauCount->SetBinContent(10,Part.getTauMuOverlap() );
  TauCount->GetXaxis()->SetBinLabel(10,"Tau-Mu Overlap");
  TauCount->SetBinContent(11,Part.getTauElOverlap() );
  TauCount->GetXaxis()->SetBinLabel(11,"Tau-e Overlap");
  
  JetCount->SetBinContent(1,VertexJet);
  JetCount->GetXaxis()->SetBinLabel(1,"Vertex");
  JetCount->SetBinContent(2,TriggerJet);
  JetCount->GetXaxis()->SetBinLabel(2,"Trigger");
  JetCount->SetBinContent(3,Part.getJetPt() );
  JetCount->GetXaxis()->SetBinLabel(3,"Pt");
  JetCount->SetBinContent(4,Part.getJetEta() );
  JetCount->GetXaxis()->SetBinLabel(4,"Eta");
  JetCount->SetBinContent(5,Part.getJetPU() );
  JetCount->GetXaxis()->SetBinLabel(5,"PileUp");
  JetCount->SetBinContent(6,Part.getJetClean() );
  JetCount->GetXaxis()->SetBinLabel(6,"Cleaning");
  JetCount->SetBinContent(7,Part.getJetElOverlap() );
  JetCount->GetXaxis()->SetBinLabel(7,"Jet-e Overlap");
    
  if(!m_UseProof){
    info->Write();
    cutFlow_4mu->Write();
    cutFlow_4e->Write();
    cutFlow_2l2l->Write();
    ElecCount->Write();
    MuCBST->Write();
    MuCBSTSA->Write();
    MuCBSTSACA->Write();
    TauCount->Write();
    JetCount->Write();
  }
  
}
