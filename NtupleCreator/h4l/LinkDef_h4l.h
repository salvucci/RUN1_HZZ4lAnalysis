#ifndef __h4lLinkDef_h__
#define __h4lLinkDef_h__

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link off all typedef;
#pragma link C++ nestedclass;
#pragma link C++ class h4l+;
#pragma link C++ class std::vector<std::vector<int> >+;

#endif

#endif
