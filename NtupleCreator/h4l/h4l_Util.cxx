#include "h4l.h"

void h4l::RetrieveLumiFractions(){
  
  Print("H4lNtupleCreator:: Retrieving Luminosity Fractions ...");
  
  if(m_year==YEAR_2011){
    m_periodB = PileUpTool->GetIntegratedLumi(177986,178109);
    m_periodD = PileUpTool->GetIntegratedLumi(179710,180481);
    m_periodE = PileUpTool->GetIntegratedLumi(180614,180776);
    m_periodF = PileUpTool->GetIntegratedLumi(182013,182519);
    m_periodG = PileUpTool->GetIntegratedLumi(182726,183462);
    m_periodH = PileUpTool->GetIntegratedLumi(183544,184169);
    m_periodI = PileUpTool->GetIntegratedLumi(185353,186493);
    m_periodJ = PileUpTool->GetIntegratedLumi(186516,186755);
    m_periodK = PileUpTool->GetIntegratedLumi(186873,187815);
    m_periodL = PileUpTool->GetIntegratedLumi(188902,190343);
    m_periodM = PileUpTool->GetIntegratedLumi(190503,191933);
    
    m_frac1   = m_periodI/(m_periodI+m_periodJ+m_periodK);
    m_frac2   = (m_periodI+m_periodJ)/(m_periodI+m_periodJ+m_periodK);
  }
  else if(m_year==YEAR_2012){
    m_periodA = PileUpTool->GetIntegratedLumi(200804,201556);
    m_periodB = PileUpTool->GetIntegratedLumi(202660,205113);
    m_periodC = PileUpTool->GetIntegratedLumi(206248,207397);
    m_periodD = PileUpTool->GetIntegratedLumi(207447,209025);
    m_periodE = PileUpTool->GetIntegratedLumi(209074,210308);
    m_periodF = 0.0;
    m_periodG = PileUpTool->GetIntegratedLumi(211522,212272);
    m_periodH = PileUpTool->GetIntegratedLumi(212619,213359);
    m_periodI = PileUpTool->GetIntegratedLumi(213431,213819);
    m_periodJ = PileUpTool->GetIntegratedLumi(213900,215091);
    m_periodK = 0.0;
    m_periodL = PileUpTool->GetIntegratedLumi(215414,215643);
    m_periodM = PileUpTool->GetIntegratedLumi(216399,216432);
  }
  
}

double h4l::GetPileUpWeight(){
  
  Print("H4lNtupleCreator:: Retrieving PileUp Weight ...");
  
  double pileupW = 1.;
  if(m_UseWeights){
    double mu = 0;
    if(m_year==YEAR_2011){
      mu = averageIntPerXing;
    }
    else if(m_year==YEAR_2012){
      mu = (lbn==1&&int(averageIntPerXing+0.5)==1)?0.:averageIntPerXing;
    }
    pileupW = PileUpTool->GetCombinedWeight(RunNumber,
					    mc_channel_number,
					    mu);
  }
  PrintOut("                    PileUp Weight = ");
  PrintOut(pileupW); PrintEndl();
  return pileupW;
  
}

double h4l::GetVertexWeight(){
  
  Print("H4lNtupleCreator:: Retrieving Vertex Weight ...");
  
  double vertexW = 1.;
  if(m_UseWeights){
    //if(m_year==YEAR_2012){
    if(mc_n>1){
      vertexW = VertexRew->GetWeight( mc_vx_z->at(2) );
    }
    //}
  }
  PrintOut("                    Vertex Weight = ");
  PrintOut(vertexW); PrintEndl();
  return vertexW;
  
}

bool h4l::GRLCheck(int Run,
		   int Lbn){
  
  Print("H4lNtupleCreator:: Checking GoodRunLists (only data) ...");
  
  //bool grl=false;
  if(m_type==DATA){
    //grl=DQ::PassRunLB(Run,Lbn);
    if(Grl.HasRunLumiBlock(Run,Lbn))
      return true;
  }
  //else{
  //grl=false;
  //}
  return false;
}

bool h4l::LarErrorCheck(int larerror){
  
  Print("H4lNtupleCreator:: Cheching LAr error ...");
    
  if(m_type==DATA){
    if(larerror==2){
      return true;
    }
    else{
      return false;
    }
  }
  return false;
  
}

bool h4l::TileErrorCheck(int terror){
  
  Print("H4lNtupleCreator:: Cheching Tile error ...");
  
  if(m_type==DATA){
    if(terror==2){
      return true;
    }
    else{
      return false;
    }
  }
  return false;
  
}

bool h4l::BadEventCheck(){
  
  Print("H4lNtupleCreator:: Cheching possible Bad Event  ...");
  
  if(m_type==DATA){
    if( (coreFlags&0x40000)!=0){
      return true;
    }
    else{
      return false;
    }
  }
  return false;
  
}

bool h4l::TileCorruptionCheck(int Run,
			      int Lbn,
			      int Evt){
  
  Print("H4lNtupleCreator:: Cheching possible Tile Corrupted Event  ...");
  
  if(m_type==DATA){
    if( TripRead->checkEvent(Run,Lbn,Evt)==0 ){
      return true;
    }
    else{
      return false;
    }
  }
  return false;
  
}

int h4l::DetectPeriod(Int_t runNum){

  Print("H4lNtupleCreator:: Detecting Data or MC Related Period ...");
  
  int period=-1;
  
  if(m_type==DATA){
    /*2011 data*/
    if (runNum<177966)      period = 0;  //2011 A
    else if (runNum<178110) period = 1;  //2011 B
    else if (runNum<178265) period = 2;  //2011 C
    else if (runNum<180482) period = 3;  //2011 D
    else if (runNum<180777) period = 4;  //2011 E
    else if (runNum<182520) period = 5;  //2011 F
    else if (runNum<183463) period = 6;  //2011 G
    else if (runNum<184170) period = 7;  //2011 H
    else if (runNum<186494) period = 8;  //2011 I
    else if (runNum<186756) period = 9;  //2011 J
    else if (runNum<187816) period = 10; //2011 K
    else if (runNum<190344) period = 11; //2011 L
    else if (runNum<191934) period = 12; //2011 M
    /*2012 data*/
    else if (runNum<201557) period = 13; //2012 A
    else if (runNum<205114) period = 14; //2012 B
    else if (runNum<207398) period = 15; //2012 C
    else if (runNum<209026) period = 16; //2012 D
    else if (runNum<210309) period = 17; //2012 E
    else if (runNum<212273) period = 18; //2012 G
    else if (runNum<213360) period = 19; //2012 H
    else if (runNum<213820) period = 20; //2012 I
    else if (runNum<215092) period = 21; //2012 J
    else if (runNum<215644) period = 22; //2012 L
  }			    
  else{
    if(m_year==YEAR_2011){
      PileUpTool->SetRandomSeed(314159+mc_channel_number*2718+EventNumber);
      Int_t runnum = PileUpTool->GetRandomRunNumber(runNum);
      if (runnum<177966)      period = 0;  //2011 A
      else if (runnum<178110) period = 1;  //2011 B
      else if (runnum<178265) period = 2;  //2011 C
      else if (runnum<180482) period = 3;  //2011 D
      else if (runnum<180777) period = 4;  //2011 E
      else if (runnum<182520) period = 5;  //2011 F
      else if (runnum<183463) period = 6;  //2011 G
      else if (runnum<184170) period = 7;  //2011 H
      
      else if (runnum<186516) period = 8;  //2011 I
      else if (runnum<186873) period = 9;  //2011 J
      else if (runnum<187816) period = 10; //2011 K
      
      //else if (runNum>=184170 && runNum<187816){
      //	Double_t seed=EventNumber*mc_channel_number;
      //	m_rnd->SetSeed(seed);
      //	Double_t rd=m_rnd->Uniform();
      //	if(rd<frac1)          period = 8;  //2011 I
      //	else if(rd>=frac1 && 
      //		rd<frac2)     period = 9;  //2011 J
      //	else if(rd>=frac2)    period = 10; //2011 K
      //}
      
      else if (runnum<190344) period = 11; //2011 L
      else if (runnum<191934) period = 12; //2011 M
    }
    else if(m_year==YEAR_2012){
      if (runNum<177966)      period = 0;  //2011 A
      else if (runNum<178110) period = 1;  //2011 B
      else if (runNum<178265) period = 2;  //2011 C
      else if (runNum<180482) period = 3;  //2011 D
      else if (runNum<180777) period = 4;  //2011 E
      else if (runNum<182520) period = 5;  //2011 F
      else if (runNum<183463) period = 6;  //2011 G
      else if (runNum<184170) period = 7;  //2011 H
      else if (runNum<186516) period = 8;  //2011 I
      else if (runNum<186756) period = 9;  //2011 J
      else if (runNum<187816) period = 10; //2011 K
      else if (runNum<190344) period = 11; //2011 L
      else if (runNum<191934) period = 12; //2011 M
      else{
	period = 13; //2012
      }
    }
  }
  return period;
  
}

int h4l::Detect4LepSubChan(int id1,
			   int id2,
			   int id3,
			   int id4){

  Print("H4lNtupleCreator:: Detecting 4l sub-channel ... ");
  
  int subChan=-1;
  if( fabs(id1*id2*id3*id4)==pow(m_MuId,4) )      { subChan=1; }
  
  else if( fabs(id1*id2*id3*id4)==pow(m_ElId,4) ) { subChan=0; }
  
  else if( fabs(id1*id2*id3*id4)==pow(m_ElId,2)*pow(m_MuId,2) ){
    
    if( fabs(id1*id2)==pow(m_ElId,2) )      { subChan=2; }
    else if( fabs(id1*id2)==pow(m_MuId,2) ) { subChan=3; }
    
  }
  else if( fabs(id1*id2*id3*id4)==pow(m_TaId,4) ) { subChan=4; }

  else if( fabs(id1*id2*id3*id4)==pow(m_TaId,2)*pow(m_MuId,2) ){
    
    if( fabs(id1*id2)==pow(m_MuId,2) )      { subChan=5; }
    else if( fabs(id1*id2)==pow(m_TaId,2) ) { subChan=6; }
    
  }

  else if( fabs(id1*id2*id3*id4)==pow(m_TaId,2)*pow(m_ElId,2) ){
    
    if( fabs(id1*id2)==pow(m_ElId,2) )      { subChan=7; }
    else if( fabs(id1*id2)==pow(m_TaId,2) ) { subChan=8; }
    
  }
  
  PrintOut("                            sub-channel = ");
  PrintOut(subChan); PrintEndl();
  
  return subChan;
  
}

TLorentzVector h4l::GetLepLorentzVec(int idx,
				     bool useMMC,
				     bool useMeV){

  TLorentzVector lep;
  double unit=1., p_mass=0;
  if(useMeV){ unit=GeV; }
  
  if( abs(Lept_Id.at(idx))==m_MuId ){ p_mass=m_MuMass; }
  
  else if(abs(Lept_Id.at(idx))==m_ElId ){ p_mass=m_ElMass;}

  else if(abs(Lept_Id.at(idx))==m_TaId ){
    if(useMMC){
      if(Lept_numTrack.at(idx)==1 || Lept_numTrack.at(idx)==2){
	p_mass=m_TaMass1p;
      }
      else if(Lept_numTrack.at(idx)==3 || Lept_numTrack.at(idx)==4){
	p_mass=m_TaMass3p;
      }
      else{ p_mass=m_TaMass; }
    }
    else{
      p_mass=m_TaMass;
    }
  }
  
  lep.SetPtEtaPhiM( Lept_pt.at(idx)*unit,Lept_eta.at(idx),
		    Lept_phi.at(idx)    ,p_mass*unit );
  
  return lep;
  
}

TLorentzVector h4l::GetMCLorentzVec(int idx,
				    bool useMeV){

  TLorentzVector lep;
  double unit=1.;
  if(useMeV){ unit=GeV; }
  
  lep.SetPtEtaPhiM( mc_pt->at(idx)*unit,mc_eta->at(idx),
		    mc_phi->at(idx),mc_m->at(idx)*unit  );
  
  return lep;
  
}

void h4l::VertexCheck(){
  
  Print("H4lNtupleCreator:: Checking Vertex ... ");
  
  isVertexGood = false;
  nvx=vxp_n;
  nPV=0;
  for(int v=0;v<nvx; v++){
    if(vxp_trk_n->at(v)>=2){
      nPV++;
    }
    if(vxp_trk_n->at(v)>=3){
      isVertexGood = true;
    }
  }
  if(isVertexGood){
    if(m_year==YEAR_2011){
      VertexElec += el_GSF_n;
    }
    else if(m_year==YEAR_2012){
      VertexElec += el_n;
    }
    VertexStaco += mu_staco_n;
    VertexCalo  += mu_calo_n;
    VertexTau   += tau_n;
    VertexJet   += jet_akt4topoem_n;
  }
  
}

void h4l::TriggerCheck(int period){

  Print("H4lNtupleCreator:: Checking Trigger ...");
  isTrigGood        = false;
  isTrigEFelecGood  = false;
  isTrigEF2elecGood = false;
  isTrigEFmuGood    = false;
  isTrigEF2muGood   = false;
  isTrigEFemuGood   = false;
  
  if(period<9){
    if(EF_e20_medium) { isTrigEFelecGood=true; }
    if(EF_2e12_medium) { isTrigEF2elecGood=true; }
    if(EF_mu18_MG) { isTrigEFmuGood=true; }
    if(EF_2mu10_loose) { isTrigEF2muGood=true; }
    if(EF_e10_medium_mu6) {isTrigEFemuGood=true; }
  }
  if(period<10){
    if(EF_e20_medium) { isTrigEFelecGood=true; }
    if(EF_2e12_medium) { isTrigEF2elecGood=true; }
    if(EF_mu18_MG_medium) { isTrigEFmuGood=true; }
    if(EF_2mu10_loose) { isTrigEF2muGood=true; }
    if(EF_e10_medium_mu6) {isTrigEFemuGood=true; }
  }
  else if(period<11){
    if(EF_e22_medium) { isTrigEFelecGood=true; }
    if(EF_2e12T_medium) { isTrigEF2elecGood=true; }
    if(EF_mu18_MG_medium) { isTrigEFmuGood=true; }
    if(EF_2mu10_loose) { isTrigEF2muGood=true; }
    if(EF_e10_medium_mu6) {isTrigEFemuGood=true; }
  }
  else if(period<13){
    if(EF_e22vh_medium1) { isTrigEFelecGood=true; }
    if(EF_2e12Tvh_medium) { isTrigEF2elecGood=true; }
    if(EF_mu18_MG_medium) { isTrigEFmuGood=true; }
    if(EF_2mu10_loose) { isTrigEF2muGood=true; }
    if(EF_e10_medium_mu6) {isTrigEFemuGood=true; }
  }
  else if(period<23){
    if( EF_e24vhi_medium1 || EF_e60_medium1 ) { isTrigEFelecGood=true; }
    if( EF_2e12Tvh_loose1_ ) { isTrigEF2elecGood=true; }
    if( EF_mu24i_tight || EF_mu36_tight ) { isTrigEFmuGood=true; }
    if( EF_2mu13 || EF_mu18_tight_mu8_EFFS) { isTrigEF2muGood=true; }
    if( EF_e12Tvh_medium1_mu8_ || EF_e24vhi_loose1_mu8 ) {isTrigEFemuGood=true; }
  }
  
  if( isTrigEFelecGood || isTrigEF2elecGood ||
      isTrigEFmuGood   || isTrigEF2muGood   ||
      isTrigEFemuGood                        ){
    isTrigGood=true;
  }
  
  if(isTrigGood&&isVertexGood){
    if(m_year==YEAR_2011){
      TriggerElec += el_GSF_n;
    }
    else if(m_year==YEAR_2012){
      TriggerElec += el_n;
    }
    TriggerStaco += mu_staco_n;
    TriggerCalo  += mu_calo_n;
    TriggerTau   += tau_n;
    TriggerJet   += jet_akt4topoem_n;
  }
  PrintOut("                             Single Lep Good:   Elec = "); 
  PrintOut(isTrigEFelecGood); PrintOut("   Muon = ");
  PrintOut(isTrigEFmuGood); PrintEndl();
  PrintOut("                             Di Lep Good    :   Elec = ");
  PrintOut(isTrigEF2elecGood); PrintOut("   Muon = ");
  PrintOut(isTrigEF2muGood); PrintOut("   Elec-Muon = ");
  PrintOut(isTrigEFemuGood); PrintEndl();
  PrintOut("                             General Good   : ");
  PrintOut(isTrigGood); PrintEndl();
  
}

void h4l::MissingEnergy(){

  Print("H4lNtupleCreator:: Retrieving Missing Energy Information ...");
  
  met_et    = MET_RefFinal_et/GeV;
  met_etx   = (MET_RefFinal_et*TMath::Cos(MET_RefFinal_phi))/GeV;
  met_ety   = (MET_RefFinal_et*TMath::Sin(MET_RefFinal_phi))/GeV;
  met_sumet = MET_RefFinal_sumet/GeV;
  met_phi   = MET_RefFinal_phi;

  if(m_NtupTau){
    met_bdt_et    = MET_RefFinal_BDTMedium_et/GeV;
    met_bdt_etx   = MET_RefFinal_BDTMedium_etx/GeV;
    met_bdt_ety   = MET_RefFinal_BDTMedium_ety/GeV;
    met_bdt_sumet = MET_RefFinal_BDTMedium_sumet/GeV;
    met_bdt_phi   = MET_RefFinal_BDTMedium_phi;
  }
  
}

int h4l::GetGoodQuadrupletPosition(){

  Print("H4lNtupleCreator:: Retrieving Good Quadruplet Position ... ");
  
  int LarOk=0, GrlOk=0;
  int TilOk=0, EvtOk=0;
  if(m_type==DATA){
    GrlOk = 1;
  }
  else if(m_type==MC){
    GrlOk = 0;
  }
  
  if( Quadruplets.size()>0 )
    isGood4lSFOS=true;
  
  if( isLArError==LarOk  && isTileError==TilOk &&
      isBadEvent==EvtOk  && isInGRL==GrlOk     &&
      isVertexGood       && isTrigGood         &&
      isGood4lCandidate  && isGood4lSFOS        ){

    for(UInt_t q=0; q<Quadruplets.size(); q++){
      
      if( IsBestQuadruplet.at(q)==1 &&
	  IsZ1MassGood.at(q)==1     &&
	  IsZ2MassGood.at(q)==1     &&
	  IsDeltaRGood.at(q)==1     &&
	  IsJpsiVetoGood.at(q)==1   &&
	  IsTrackIsoGood.at(q)==1   &&
	  IsCaloIsoGood.at(q)==1    &&
	  IsD0SignGood.at(q)==1       ){
	Channel=QuadSubChan.at(q);
	return q;
      }
    }
    
  }
  Channel=-1;
  return -1;
  
}

std::vector<int> h4l::MisPairing(){
  
  Print("H4lNtupleCreator:: Checking Leptons MisPairing ... ");
  
  m_IsGoodPair.clear();
  for(UInt_t q=0; q<Quadruplets.size(); q++){
    PrintOut("                    Quadruplet: "); 
    PrintOut((int)q); PrintEndl(); 
    m_IsGoodPair.push_back( CheckMisPairing(q) );
  }
  return m_IsGoodPair;
  
}

std::vector<int> h4l::MisPairLabel(){

  Print("H4lNtupleCreator:: Checking Leptons MisPairing and Zs Labeling ...");

  m_IsGoodPairLabel.clear();
  for(UInt_t q=0; q<Quadruplets.size(); q++){
    PrintOut("                    Quadruplet: "); 
    PrintOut((int)q); PrintEndl(); 
    m_IsGoodPairLabel.push_back( CheckMisPairingLabeling(q) );
  }
  return m_IsGoodPairLabel;
  
}

bool h4l::CheckMisPairing(int Qpos){
  
  if(QuadSubChan.at(Qpos)==2 || QuadSubChan.at(Qpos)==3)
    return false;
  
  int l1=Quadruplets.at(Qpos).at(0);
  int l2=Quadruplets.at(Qpos).at(1);
  int l3=Quadruplets.at(Qpos).at(2);
  int l4=Quadruplets.at(Qpos).at(3);
  PrintOut("                    l1_bcode = "); 
  PrintOut(GetLepMothBcode(l1));
  PrintOut("                    l2_bcode = "); 
  PrintOut(GetLepMothBcode(l2));
  PrintOut("                    l3_bcode = "); 
  PrintOut(GetLepMothBcode(l3));
  PrintOut("                    l4_bcode = "); 
  PrintOut(GetLepMothBcode(l4));
  PrintEndl();

  if( GetLepMothBcode(l1)!=GetLepMothBcode(l2) ||
      GetLepMothBcode(l3)!=GetLepMothBcode(l4)   ){
    PrintOut("               Mis-Pairing Found!"); PrintEndl();
    return true;
  }
  PrintOut("               No Mis-Pairing Found!"); PrintEndl();
  return false;
  
}

bool h4l::CheckMisPairingLabeling(int Qpos){
  
  if(QuadSubChan.at(Qpos)==2 || QuadSubChan.at(Qpos)==3)
    return false;
  
  int l1=Quadruplets.at(Qpos).at(0);
  int l2=Quadruplets.at(Qpos).at(1);
  int l3=Quadruplets.at(Qpos).at(2);
  int l4=Quadruplets.at(Qpos).at(3);
  PrintOut("                    l1_bcode = "); PrintOut(GetLepMothBcode(l1));
  PrintOut("                    l2_bcode = "); PrintOut(GetLepMothBcode(l2));
  PrintOut("                    Z1_bcode = "); PrintOut(Z1_bcode_truth);
  PrintEndl();
  PrintOut("                    l3_bcode = "); PrintOut(GetLepMothBcode(l3));
  PrintOut("                    l4_bcode = "); PrintOut(GetLepMothBcode(l4));
  PrintOut("                    Z2_bcode = "); PrintOut(Z2_bcode_truth);
  PrintEndl();
  if( GetLepMothBcode(l1)!=Z1_bcode_truth ||
      GetLepMothBcode(l2)!=Z1_bcode_truth ||
      GetLepMothBcode(l3)!=Z2_bcode_truth ||
      GetLepMothBcode(l4)!=Z2_bcode_truth   ){
    PrintOut("               Mis-Pairing + Mis-Labeling Found!"); PrintEndl();
    return true;
  }
  PrintOut("               No Mis-Pairing + Mis-Labeling Found!"); PrintEndl();
  return false;
  
}

int h4l::GetLepMothBcode(int idl){
  
  int bcode=-1;
  int lind=Lept_Index.at(idl);
  
  if(abs(Lept_Id.at(idl))==m_MuId){
    if(Lept_CALOgood.at(idl)==1){
      bcode=mu_calo_truth_motherbarcode->at(lind);
    }
    else{
      bcode=mu_staco_truth_motherbarcode->at(lind);
    }
  }
  else if(abs(Lept_Id.at(idl))==m_ElId){
    if(m_year==YEAR_2011){
      bcode=el_GSF_truth_motherbarcode->at(lind);
    }
    else if(m_year==YEAR_2012){
      bcode=el_truth_motherbarcode->at(lind);
    }
  }
  
  return bcode;
  
}

void h4l::PhotonsInformation(){
  
  Print("H4lNtupleCreator:: Retrieving Photons Information ... ");
  
  for(UInt_t i=0; i<ph_eta->size(); i++){
    Photon_eta.push_back( ph_eta->at(i) );
    Photon_phi.push_back( ph_phi->at(i) );
    Photon_E.push_back( ph_E->at(i) );
    Photon_Et.push_back( ph_Et->at(i) );
    Photon_author.push_back( ph_author->at(i) );
    Photon_f1.push_back( ph_f1->at(i) );
  }
  
}

void h4l::MCEventOverlap(HforToolD3PD hforTool,
			    UInt_t datasetNumber){
  
  Print("H4lNtupleCreator:: Removing MC overlap events ...");
  
  int hfor_type = hforTool.getDecision(datasetNumber,mc_n,mc_pt,mc_eta,
				       mc_phi,mc_m,mc_pdgId,mc_status,
				       mc_parent_index,mc_child_index);
  hfor = hfor_type;
  
}

bool h4l::TauEventRemoval(){
  
  Print("H4muNtupleCreator:: Checking presence of taus ...");
  std::vector<int> Zpos;
  for(Int_t np=0; np<mc_n; np++){
    if(mc_pdgId->at(np)==23){
      Zpos.push_back(np);
      PrintOut("                    Z found in pos. "); 
      PrintOut(np); PrintEndl();
    }
  }
  PrintOut("                    Number of Z found: "); 
  PrintOut((int)Zpos.size()); PrintEndl();
  for(UInt_t z=0; z<Zpos.size(); z++){
    int Nchild=(mc_child_index->at(Zpos.at(z))).size();
    PrintOut("                    Number of children: ");
    PrintOut(Nchild); PrintEndl();
    for(int ch=0; ch<Nchild; ch++){
      int PdgId=mc_pdgId->at(mc_child_index->at(Zpos.at(z)).at(ch));
      PrintOut("                            child's PdgId = ");
      PrintOut(PdgId); PrintEndl();
      if( PdgId==m_TaId ){
      	Print("                    Tau Found -> this event will be skipped!");
      	PrintEndl();
      	return true;
      }
    }
  }
  return false;
}

int h4l::ZStatusInTruth(){
  
  int stat=0;
  
  if(m_year==YEAR_2011){
    stat=2;
  }
  
  if(m_year==YEAR_2012){
    if( m_channel.find("JHU")!=std::string::npos ) { stat=22; }
    if( m_channel.find("Powheg")!=std::string::npos &&
	m_channel.find("_ZZ_")!=std::string::npos    ) { stat=62; }
    if( m_channel.find("Powheg")!=std::string::npos &&
	m_channel.find("ggH")!=std::string::npos     ) { stat=22; }
    if( m_channel.find("Powheg")!=std::string::npos &&
	m_channel.find("VBF")!=std::string::npos     ) { stat=22; }
  }
  
  return stat;
  
}

void h4l::MCInfo(){
  
  Print("H4lNtupleCreator:: Writing MC truth Information ... ");
  
  std::vector<int> Zpos;
  int Zstatus=0;

  Zstatus=ZStatusInTruth();
    
  for(Int_t np=0; np<mc_n; np++){
    if(mc_pdgId->at(np)==23 && mc_status->at(np)==Zstatus){
      Zpos.push_back(np);
    }
  }
  PrintOut("                    Number of Z: "); 
  PrintOut( (int)Zpos.size() ); PrintEndl();
  
  if(Zpos.size()<2)
    return;
 
  /*Z1*/
  int Z1pos=Zpos.at(0);
  int NchildZ1=(mc_child_index->at(Z1pos)).size();
  std::vector<int> Z1LepPos;
  PrintOut("                    Muon or Electron from 1st Z: ");PrintEndl();
  for(int ch=0; ch<NchildZ1; ch++){
    int PdgId=mc_pdgId->at(mc_child_index->at(Z1pos).at(ch));
    int lepstatus=mc_status->at(mc_child_index->at(Z1pos).at(ch));
    if( ( (fabs(PdgId)==m_MuId || fabs(PdgId)==m_ElId) && lepstatus==1) ||
	( fabs(PdgId)==m_TaId && lepstatus==2 ) ){
      PrintOut("                        PdgId = "); PrintOut(PdgId); 
      PrintOut(" Status = "); PrintOut(lepstatus); PrintEndl();
      Z1LepPos.push_back(mc_child_index->at(Z1pos).at(ch));
    }
  }
  PrintOut("                    Number of mu or el from 1st Z: "); 
  PrintOut( (int)Z1LepPos.size() ); PrintEndl();
  if(Z1LepPos.size()<2)
    return;
  
  TLorentzVector LEP1, LEP2;
  double Z1m_tmp;
  std::vector<double> LepZ1Pt, LepZ1Eta, LepZ1Phi, LepZ1M, LepZ1PdgId;
  int lep1=Z1LepPos.at(0);
  int lep2=Z1LepPos.at(1);
  if( mc_charge->at(lep1)>mc_charge->at(lep2) ){
    LEP1=GetMCLorentzVec(lep2);
    LEP2=GetMCLorentzVec(lep1);
    
    LepZ1Pt.push_back( mc_pt->at(lep2)/GeV );
    LepZ1Pt.push_back( mc_pt->at(lep1)/GeV );
    LepZ1Eta.push_back( mc_eta->at(lep2) );
    LepZ1Eta.push_back( mc_eta->at(lep1) );
    LepZ1Phi.push_back( mc_phi->at(lep2) );
    LepZ1Phi.push_back( mc_phi->at(lep1) );
    LepZ1M.push_back( mc_m->at(lep2)/GeV );
    LepZ1M.push_back( mc_m->at(lep1)/GeV );
    LepZ1PdgId.push_back( mc_pdgId->at(lep2) );
    LepZ1PdgId.push_back( mc_pdgId->at(lep1) );
  }
  else{
    LEP1=GetMCLorentzVec(lep1);
    LEP2=GetMCLorentzVec(lep2);
    
    LepZ1Pt.push_back( mc_pt->at(lep1)/GeV );
    LepZ1Pt.push_back( mc_pt->at(lep2)/GeV );
    LepZ1Eta.push_back( mc_eta->at(lep1) );
    LepZ1Eta.push_back( mc_eta->at(lep2) );
    LepZ1Phi.push_back( mc_phi->at(lep1) );
    LepZ1Phi.push_back( mc_phi->at(lep2) );
    LepZ1M.push_back( mc_m->at(lep1)/GeV );
    LepZ1M.push_back( mc_m->at(lep2)/GeV );
    LepZ1PdgId.push_back( mc_pdgId->at(lep1) );
    LepZ1PdgId.push_back( mc_pdgId->at(lep2) );
  }
  Z1m_tmp=(LEP1+LEP2).M()/GeV;
  
  /*Z2*/
  int Z2pos=Zpos.at(1);
  int NchildZ2=(mc_child_index->at(Z2pos)).size();
  std::vector<int> Z2LepPos;
  PrintOut("                    Muon or Electron from 2nd Z: "); PrintEndl();
  for(int ch=0; ch<NchildZ2; ch++){
    int PdgId=mc_pdgId->at(mc_child_index->at(Z2pos).at(ch));
    int lepstatus=mc_status->at(mc_child_index->at(Z2pos).at(ch));
    if( ( (fabs(PdgId)==m_MuId || fabs(PdgId)==m_ElId) && lepstatus==1) ||
	( fabs(PdgId)==m_TaId && lepstatus==2 ) ){       
      PrintOut("                        PdgId = "); PrintOut(PdgId); 
      PrintOut(" Status = "); PrintOut(lepstatus); PrintEndl();
      Z2LepPos.push_back(mc_child_index->at(Z2pos).at(ch));
    }
  }
  PrintOut("                    Number of mu or el from 2nd Z: "); 
  PrintOut( (int)Z2LepPos.size() ); PrintEndl();
  if(Z2LepPos.size()<2)
    return;

  TLorentzVector LEP3, LEP4;
  double Z2m_tmp;
  std::vector<double> LepZ2Pt, LepZ2Eta, LepZ2Phi, LepZ2M, LepZ2PdgId;
  int lep3=Z2LepPos.at(0);
  int lep4=Z2LepPos.at(1);
  if( mc_charge->at(lep3)>mc_charge->at(lep4) ){
    LEP3=GetMCLorentzVec(lep4);
    LEP4=GetMCLorentzVec(lep3);

    LepZ2Pt.push_back( mc_pt->at(lep4)/GeV );
    LepZ2Pt.push_back( mc_pt->at(lep3)/GeV );
    LepZ2Eta.push_back( mc_eta->at(lep4) );
    LepZ2Eta.push_back( mc_eta->at(lep3) );
    LepZ2Phi.push_back( mc_phi->at(lep4) );
    LepZ2Phi.push_back( mc_phi->at(lep3) );
    LepZ2M.push_back( mc_m->at(lep4)/GeV );
    LepZ2M.push_back( mc_m->at(lep3)/GeV );
    LepZ2PdgId.push_back( mc_pdgId->at(lep4) );
    LepZ2PdgId.push_back( mc_pdgId->at(lep3) );
  }
  else{
    LEP3=GetMCLorentzVec(lep3);
    LEP4=GetMCLorentzVec(lep4);

    LepZ2Pt.push_back( mc_pt->at(lep3)/GeV );
    LepZ2Pt.push_back( mc_pt->at(lep4)/GeV );
    LepZ2Eta.push_back( mc_eta->at(lep3) );
    LepZ2Eta.push_back( mc_eta->at(lep4) );
    LepZ2Phi.push_back( mc_phi->at(lep3) );
    LepZ2Phi.push_back( mc_phi->at(lep4) );
    LepZ2M.push_back( mc_m->at(lep3)/GeV );
    LepZ2M.push_back( mc_m->at(lep4)/GeV );
    LepZ2PdgId.push_back( mc_pdgId->at(lep3) );
    LepZ2PdgId.push_back( mc_pdgId->at(lep4) );
  }
  Z2m_tmp=(LEP3+LEP4).M()/GeV;
  
  
  TLorentzVector Z1, Z2, H, v1, v2, v3, v4;
  if( fabs(Z1m_tmp-m_Zmass)<fabs(Z2m_tmp-m_Zmass) ){
    v1=LEP1;  v2=LEP2;
    v3=LEP3;  v4=LEP4;
    Z1_bcode_truth=mc_barcode->at(Zpos.at(0));
    Z2_bcode_truth=mc_barcode->at(Zpos.at(1));
    LepZ1Pt_truth    = LepZ1Pt;
    LepZ1Eta_truth   = LepZ1Eta;
    LepZ1Phi_truth   = LepZ1Phi;
    LepZ1M_truth     = LepZ1M;
    LepZ1PdgId_truth = LepZ1PdgId;
    LepZ2Pt_truth    = LepZ2Pt;
    LepZ2Eta_truth   = LepZ2Eta;
    LepZ2Phi_truth   = LepZ2Phi;
    LepZ2M_truth     = LepZ2M;
    LepZ2PdgId_truth = LepZ2PdgId;
  }
  else{
    v1=LEP3;  v2=LEP4;  
    v3=LEP1;  v4=LEP2;
    Z1_bcode_truth=mc_barcode->at(Zpos.at(1));
    Z2_bcode_truth=mc_barcode->at(Zpos.at(0));
    LepZ1Pt_truth    = LepZ2Pt;
    LepZ1Eta_truth   = LepZ2Eta;
    LepZ1Phi_truth   = LepZ2Phi;
    LepZ1M_truth     = LepZ2M;
    LepZ1PdgId_truth = LepZ2PdgId;
    LepZ2Pt_truth    = LepZ1Pt;
    LepZ2Eta_truth   = LepZ1Eta;
    LepZ2Phi_truth   = LepZ1Phi;
    LepZ2M_truth     = LepZ1M;
    LepZ2PdgId_truth = LepZ1PdgId;
  }
  Z1=v1+v2;
  Z2=v3+v4;
  H=Z1+Z2;
  
  /*Fill truth quantities*/
  Z1mass_truth  = Z1.M()/GeV;
  Z2mass_truth  = Z2.M()/GeV;
  Hmass_truth   = H.M()/GeV;
  HpT_truth     = H.Pt()/GeV;
  Heta_truth    = H.Eta();

  /*Calculate Z1-Z2 DeltaPhi in Lab*/
  double DPhi = fabs( Z1.Phi()-Z2.Phi() );
  double DPhiLab = 0;
  if( DPhi>TMath::Pi() ){
    DPhiLab = 2*TMath::Pi()-DPhi;
  }
  else{
    DPhiLab=DPhi;
  }
  DeltaPhiLab_truth = DPhiLab;
  
  /*Calculate acoplanarity*/
  double phi_Z1 = atan2( Z1.Py(),Z1.Px() );
  double phi_Z2 = atan2( Z2.Py(),Z2.Px() );
  
  double a_cop = fabs(phi_Z1-phi_Z2)<TMath::Pi() ? fabs(phi_Z1-phi_Z2) : 
    2*TMath::Pi()-fabs(phi_Z1-phi_Z2);
  
  Acoplanarity_truth = a_cop;
    
  /*Calculate thetastar and its cosin*/
  TLorentzVector q;
  q.SetPxPyPzE( 0, 0, ( H.E() + H.Pz() ) / 2., ( H.E() + H.Pz() ) / 2. );
  q.Boost( -( H.BoostVector() ) );
  
  const TVector3 parton = q.Vect().Unit();
  
  Z1.Boost( -( H.BoostVector() ) ); // go to Higgs RFR
  Z2.Boost( -( H.BoostVector() ) );
  
  const TVector3 z1 = Z1.Vect().Unit();
  const TVector3 z2 = Z2.Vect().Unit();
  
  double costhetastar = TMath::Cos( parton.Angle( z1 ) );
  double thetastar = parton.Angle( z1 );
  
  CosThetaStar_truth = costhetastar;
  ThetaStar_truth    = thetastar;

  /*Boosting leptons into the Higgs rest frame*/
  TLorentzVector vv1( v1 ), vv2( v2 ), vv3( v3 ), vv4( v4 );
  vv1.Boost( -( H.BoostVector() ) ); // go to Higgs RFR
  vv2.Boost( -( H.BoostVector() ) );
  vv3.Boost( -( H.BoostVector() ) );
  vv4.Boost( -( H.BoostVector() ) );
  
  const TVector3 v1p = vv1.Vect();
  const TVector3 v2p = vv2.Vect();
  const TVector3 v3p = vv3.Vect();
  const TVector3 v4p = vv4.Vect();
  const TVector3 nz( 0, 0, 1. );
  
  /*Calculate DeltaPhi, DeltaPhi1*/
  const TVector3 n1p = v1p.Cross( v2p ).Unit();
  const TVector3 n2p = v3p.Cross( v4p ).Unit();
  const TVector3 nscp = nz.Cross( z1 ).Unit();
  double deltaphi = ( z1.Dot( n1p.Cross( n2p ) ) /
		      TMath::Abs( z1.Dot( n1p.Cross( n2p ) ) ) *
		      TMath::ACos( -n1p.Dot( n2p ) ) );
  double deltaphi1 = ( z1.Dot( n1p.Cross( nscp ) ) /
		       TMath::Abs( z1.Dot( n1p.Cross( nscp ) ) ) *
		       TMath::ACos( n1p.Dot( nscp ) ) );
  
  DeltaPhi_truth = deltaphi;
  DeltaPhi1_truth = deltaphi1;
  
  /*Calculating costheta1, costheta2*/
  TLorentzVector Z2_rfr_Z1 = Z2; // it's still in H RFR
  Z2_rfr_Z1.Boost( -( Z1.BoostVector() ) ); // now it's in Z1 RFR (both Z1 and Z2 are in H RFR)
  const TVector3 z2_rfr_Z1 = Z2_rfr_Z1.Vect();
    
  TLorentzVector Z1_rfr_Z2 = Z1; // it's still in H RFR
  Z1_rfr_Z2.Boost( -( Z2.BoostVector() ) ); // now it's in Z2 RFR (both Z1 and Z2 are still in H RFR)
  const TVector3 z1_rfr_Z2 = Z1_rfr_Z2.Vect();
  
  vv1.Boost( -( Z1.BoostVector() ) ); // Z1 and Z2 still in H RFR: put leptons in their Z's reference frame
  vv3.Boost( -( Z2.BoostVector() ) );
  
  double costheta1 = - ( z2_rfr_Z1.Dot( vv1.Vect() ) /
			 TMath::Abs( z2_rfr_Z1.Mag() * vv1.Vect().Mag() ) );
  double costheta2 = - ( z1_rfr_Z2.Dot( vv3.Vect() ) /
			 TMath::Abs( z1_rfr_Z2.Mag() * vv3.Vect().Mag() ) );
  
  CosTheta1_truth = costheta1;
  CosTheta2_truth = costheta2;
  
}

void h4l::TausTruthMatching(){

  Print("H4lNtupleCreator:: Checking Taus truth matching ... ");
  
  vector<TLorentzVector> truth_tau;
  vector<TLorentzVector> truth_el;
  vector<TLorentzVector> truth_mu;
  vector<TLorentzVector> reco_tau;
  
  for(UInt_t p=0; p<LepZ1Pt_truth.size(); p++){
    if(abs(LepZ1PdgId_truth.at(p))==15) {
      TLorentzVector uno_t;
      uno_t.SetPtEtaPhiM(LepZ1Pt_truth.at(p),LepZ1Eta_truth.at(p),LepZ1Phi_truth.at(p),LepZ1M_truth.at(p));
      truth_tau.push_back(uno_t);
    } else if (abs(LepZ1PdgId_truth.at(p))==11) {
      TLorentzVector uno_e;
      uno_e.SetPtEtaPhiM(LepZ1Pt_truth.at(p),LepZ1Eta_truth.at(p),LepZ1Phi_truth.at(p),LepZ1M_truth.at(p));
      truth_el.push_back(uno_e);
    } else if (abs(LepZ1PdgId_truth.at(p))==13) {
      TLorentzVector uno_m;
      uno_m.SetPtEtaPhiM(LepZ1Pt_truth.at(p),LepZ1Eta_truth.at(p),LepZ1Phi_truth.at(p),LepZ1M_truth.at(p));
      truth_mu.push_back(uno_m);
    }
  }

  for(UInt_t d=0; d<LepZ2Pt_truth.size(); d++){
    if(abs(LepZ2PdgId_truth.at(d))==15) {
      TLorentzVector due_t;
      due_t.SetPtEtaPhiM(LepZ2Pt_truth.at(d),LepZ2Eta_truth.at(d),LepZ2Phi_truth.at(d),LepZ2M_truth.at(d));
      truth_tau.push_back(due_t);
    } else if (abs(LepZ2PdgId_truth.at(d))==11) {
      TLorentzVector due_e;
      due_e.SetPtEtaPhiM(LepZ2Pt_truth.at(d),LepZ2Eta_truth.at(d),LepZ2Phi_truth.at(d),LepZ2M_truth.at(d));
      truth_el.push_back(due_e);
    } else if (abs(LepZ2PdgId_truth.at(d))==13) {
      TLorentzVector due_m;
      due_m.SetPtEtaPhiM(LepZ2Pt_truth.at(d),LepZ2Eta_truth.at(d),LepZ2Phi_truth.at(d),LepZ2M_truth.at(d));
      truth_mu.push_back(due_m);
    }
  }
  
  for(UInt_t k=0; k<Lept_pt.size(); k++){
    if(abs(Lept_Id.at(k))==15) {
      //std::cout<<"Reco Tau Pos"<<k<<std::endl;
      TLorentzVector tre;
      tre.SetPtEtaPhiE(Lept_pt.at(k),Lept_eta.at(k),Lept_phi.at(k),Lept_E.at(k));
      reco_tau.push_back(tre);
    }
  }
  
  double Dcone_test;
  double eta_reco_tau,phi_reco_tau,eta_truth_tau,phi_truth_tau,eta_truth_el,phi_truth_el,eta_truth_mu,phi_truth_mu;
  int found_tau;
  int found_el;
  int found_mu;
  
  for(UInt_t q=0; q<reco_tau.size(); q++){
    //std::cout<<"reco tau "<<q<<std::endl;
    TLorentzVector rt = reco_tau.at(q);
    eta_reco_tau = rt.Eta();
    phi_reco_tau = rt.Phi();
    Dcone_test = 99999999.;
    found_tau = 0;
    found_el = 0;
    found_mu = 0;
    //taus 
    for(UInt_t d=0; d<truth_tau.size(); d++){
      TLorentzVector tt = truth_tau.at(d);
      eta_truth_tau = tt.Eta();
      phi_truth_tau = tt.Phi();
      double Dcone_tau = ComputeDeltaR(eta_reco_tau,phi_reco_tau,eta_truth_tau,phi_truth_tau);
      //std::cout<<" Dr tautau "<<Dcone_tau<<std::endl;
      if ( Dcone_tau < 0.2 ) {
	if ( Dcone_tau < Dcone_test ) {
	  found_tau = 1;
	  Dcone_test = Dcone_tau;
	}
      }
      //std::cout<<" tautau match "<<found_tau<<std::endl;
    }
    //electrons
    for(UInt_t w=0; w<truth_el.size(); w++){
      TLorentzVector te = truth_el.at(w);
      eta_truth_el = te.Eta();
      phi_truth_el = te.Phi();
      double Dcone_el = ComputeDeltaR(eta_reco_tau,phi_reco_tau,eta_truth_el,phi_truth_el);
      //std::cout<<" Dr tauel "<<Dcone_el<<std::endl;
      if ( Dcone_el < 0.2 ) {
	if ( Dcone_el < Dcone_test ) {
	  found_el = 1;
	  Dcone_test = Dcone_el;
	}
      }
      //std::cout<<" tauel match "<<found_el<<std::endl;
    }
    //muons
    for(UInt_t y=0; y<truth_mu.size(); y++){
      TLorentzVector tm = truth_mu.at(y);
      eta_truth_mu = tm.Eta();
      phi_truth_mu = tm.Phi();
      double Dcone_mu = ComputeDeltaR(eta_reco_tau,phi_reco_tau,eta_truth_mu,phi_truth_mu);
      //std::cout<<" Dr taumu "<<Dcone_mu<<std::endl;
      if ( Dcone_mu < 0.2 ) {
	if ( Dcone_mu < Dcone_test ) {
	  found_mu = 1;
	  Dcone_test = Dcone_mu;
	}
      }
      //std::cout<<" taumu match "<<found_mu<<std::endl;
    }
    
    Tau_isTruthMatchTau.push_back(found_tau);
    Tau_isTruthMatchEl.push_back(found_el);
    Tau_isTruthMatchMu.push_back(found_mu);
    
  }//end of tau reco loop   

}

void h4l::TauTruthMatch(){

  Print("H4lNtupleCreator:: Checking Taus truth matching ... ");

  for(UInt_t k=0; k<Lept_pt.size(); k++){

    m_typematch.clear(); m_posmatch.clear(); m_drmatch.clear();
    if( abs(Lept_Id.at(k))!=m_TaId ) continue;
    bool Tlep_match=false, Mlep_match=false, Elep_match=false;
    PrintOut("                    Reco Tau: Lept Pos. = ");
    PrintOut((int)k); PrintOut(" Id = ");
    PrintOut(Lept_Id.at(k)); PrintEndl();
    
    for(UInt_t p1=0; p1<LepZ1Pt_truth.size(); p1++){
      PrintOut("                      True Lep (Z1): Lept Pos. = ");
      PrintOut((int)p1); PrintOut(" Id = ");
      PrintOut(LepZ1PdgId_truth.at(p1)); PrintEndl();

      double DR=ComputeDeltaR( Lept_eta.at(k),
			       Lept_phi.at(k),
			       LepZ1Eta_truth.at(p1),
			       LepZ1Phi_truth.at(p1) );

      PrintOut("                      DeltaR = ");
      PrintOut(DR);
      
      if(DR<0.1){
	if( abs(LepZ1PdgId_truth.at(p1))==m_TaId ){
	  Tlep_match=true;
	  m_typematch.push_back( 1015 );
	  m_posmatch.push_back( p1 );
	  m_drmatch.push_back( DR );
	  PrintOut(" -> Match with True Tau!");
	}
	else if( abs(LepZ1PdgId_truth.at(p1))==m_MuId ){
	  Mlep_match=true;
	  m_typematch.push_back( 1013 );
	  m_posmatch.push_back( p1 );
	  m_drmatch.push_back( DR );
	  PrintOut(" -> Match with True Muon!");
	}
	else if( abs(LepZ1PdgId_truth.at(p1))==m_ElId ){
	  Elep_match=true;
	  m_typematch.push_back( 1011 );
	  m_posmatch.push_back( p1 );
	  m_drmatch.push_back( DR );
	  PrintOut(" -> Match with True Electron!");
	}
      }
      PrintEndl();
    }
    
    for(UInt_t p2=0; p2<LepZ2Pt_truth.size(); p2++){

      PrintOut("                      True Lep (Z2): Lept Pos. = ");
      PrintOut((int)p2); PrintOut(" Id = ");
      PrintOut(LepZ2PdgId_truth.at(p2)); PrintEndl();
      
      double DR=ComputeDeltaR( Lept_eta.at(k),
			       Lept_phi.at(k),
			       LepZ2Eta_truth.at(p2),
			       LepZ2Phi_truth.at(p2) );

      PrintOut("                      DeltaR = ");
      PrintOut(DR); 
      
      if(DR<0.1){
	if( abs(LepZ2PdgId_truth.at(p2))==m_TaId ){
	  Tlep_match=true;
	  m_typematch.push_back( 2015 );
	  m_posmatch.push_back( p2 );
	  m_drmatch.push_back( DR );
	  PrintOut(" -> Match with True Tau!");
	}
	else if( abs(LepZ2PdgId_truth.at(p2))==m_MuId ){
	  Mlep_match=true;
	  m_typematch.push_back( 2013 );
	  m_posmatch.push_back( p2 );
	  m_drmatch.push_back( DR );
	  PrintOut(" -> Match with True Muon!");
	}
	else if( abs(LepZ2PdgId_truth.at(p2))==m_ElId ){
	  Elep_match=true;
	  m_typematch.push_back( 2011 );
	  m_posmatch.push_back( p2 );
	  m_drmatch.push_back( DR );
	  PrintOut(" -> Match with True Electron!");
	}
      }
      PrintEndl();
    }
    Tau_isTruthMatchTau.push_back(Tlep_match);
    Tau_isTruthMatchEl.push_back(Elep_match);
    Tau_isTruthMatchMu.push_back(Mlep_match);
    Tau_TruthMatchType.push_back(m_typematch);
    Tau_TruthMatchPos.push_back(m_posmatch);
    Tau_TruthMatchDr.push_back(m_drmatch);
  }
  
}

bool h4l::MCSampleOverlap(UInt_t mc_channum){
  
  Print("H4lNtupleCreator:: Checking MC Samples Overlap ... ");
  
  PrintOut("                     MC ChanNumber: "); PrintOut( (int)mc_channum ); 
  
  if( mc_channum!=126937 && mc_channum!=126938 && mc_channum!=126940 && 
      mc_channum!=167162 && mc_channum!=167163 && mc_channum!=167165 &&
      mc_channum!=169690 && mc_channum!=169691 && mc_channum!=169692 &&
      mc_channum!=110001 ){
    PrintOut(" -> KeepEvent: True"); PrintEndl();
    return true;
  }
  else{
    if(mc_channum==110001){
      
      double m_TTm1 = 40.;
      double m_TTm2 = 12.;
      std::vector<UInt_t> tmpPos;
      for(Int_t p=0; p<mc_n; p++){
	if( mc_pt->at(p)<5.e3 ) continue;
	if( mc_status->at(p)!=1 ) continue;
	if( fabs(mc_pdgId->at(p))==m_MuId ||
	    fabs(mc_pdgId->at(p))==m_ElId  ){
	  tmpPos.push_back(p);
	}
      }
      int nlep=tmpPos.size();
      if(nlep>3){
	for(int l1=0; l1<nlep; l1++){
	  for(int l2=0; l2<nlep; l2++){
	    if(l1==l2) continue;
	    for(int l3=0; l3<nlep; l3++){
	      if(l1==l3 || l2==l3) continue;
	      for(int l4=0; l4<nlep; l4++){
		if(l1==l4 || l2==l4 || l3==l4) continue;
		
		int l1idx = tmpPos.at(l1);
		int l2idx = tmpPos.at(l2);
		int l3idx = tmpPos.at(l3);
		int l4idx = tmpPos.at(l4);
		
		TLorentzVector Lep1, Lep2,Lep3,Lep4;
		Lep1.SetPtEtaPhiM( mc_pt->at(l1idx)/GeV,mc_eta->at(l1idx),
				   mc_phi->at(l1idx),mc_m->at(l1idx)/GeV );
		Lep2.SetPtEtaPhiM( mc_pt->at(l2idx)/GeV,mc_eta->at(l2idx),
				   mc_phi->at(l2idx),mc_m->at(l2idx)/GeV );
		Lep3.SetPtEtaPhiM( mc_pt->at(l3idx)/GeV,mc_eta->at(l3idx),
				   mc_phi->at(l3idx),mc_m->at(l3idx)/GeV );
		Lep4.SetPtEtaPhiM( mc_pt->at(l4idx)/GeV,mc_eta->at(l4idx),
				   mc_phi->at(l4idx),mc_m->at(l4idx)/GeV );
		double m1 = (Lep1+Lep2).M();
		double m2 = (Lep3+Lep4).M();
		if( m1>m_TTm1 && m2>m_TTm2 ){
		  PrintOut(" -> KeepEvent: False"); PrintEndl();
		  return false;
		}
	      }
	    }
	  }
	}
      }
      
    }
    else{
      
      int m_ZZType      = 0;
      double m_ZZmUp    = 150.;
      double m_ZZmDown  = 110.;
      double m_ZZmDown2 = 550.;
    
      if( mc_channum==126937 || mc_channum==126938 || mc_channum==126940 ){
	m_ZZType=0;
      }
      if( mc_channum==167162 || mc_channum==167163 || mc_channum==167165 ){
	m_ZZType=1;
      }
      if( mc_channum==169690 || mc_channum==169691 || mc_channum==169692 ){
	m_ZZType=2;
      }
    
      std::vector<UInt_t> tmpPos;
      for(Int_t p=0; p<mc_n; p++){
	if( mc_pt->at(p)<3.e3 ) continue;
	if( mc_status->at(p)!=1 ) continue;
      
	if( fabs(mc_pdgId->at(p))==m_MuId ||
	    fabs(mc_pdgId->at(p))==m_ElId ){
	  tmpPos.push_back(p);
	}
      }
      int nlep=tmpPos.size();
      if(nlep>3){
	for(int l1=0; l1<nlep; l1++){
	  for(int l2=0; l2<nlep; l2++){
	    if(l1==l2) continue;
	    for(int l3=0; l3<nlep; l3++){
	      if(l1==l3 || l2==l3) continue;
	      for(int l4=0; l4<nlep; l4++){
		if(l1==l4 || l2==l4 || l3==l4) continue;

		int l1idx = tmpPos.at(l1);
		int l2idx = tmpPos.at(l2);
		int l3idx = tmpPos.at(l3);
		int l4idx = tmpPos.at(l4);
		
		TLorentzVector Lep1, Lep2,Lep3,Lep4;
		Lep1.SetPtEtaPhiM( mc_pt->at(l1idx)/GeV,mc_eta->at(l1idx),
				   mc_phi->at(l1idx),mc_m->at(l1idx)/GeV );
		Lep2.SetPtEtaPhiM( mc_pt->at(l2idx)/GeV,mc_eta->at(l2idx),
				   mc_phi->at(l2idx),mc_m->at(l2idx)/GeV );
		Lep3.SetPtEtaPhiM( mc_pt->at(l3idx)/GeV,mc_eta->at(l3idx),
				   mc_phi->at(l3idx),mc_m->at(l3idx)/GeV );
		Lep4.SetPtEtaPhiM( mc_pt->at(l4idx)/GeV,mc_eta->at(l4idx),
				   mc_phi->at(l4idx),mc_m->at(l4idx)/GeV );
		double m = (Lep1+Lep2+Lep3+Lep4).M();

		if(m_ZZType==0){
		  if( fabs(mc_eta->at(l1idx))<5. &&
		      fabs(mc_eta->at(l2idx))<5. &&
		      fabs(mc_eta->at(l3idx))<5. &&
		      fabs(mc_eta->at(l4idx))<5. &&
		      m>m_ZZmDown && m<m_ZZmUp    ){
		    PrintOut(" -> KeepEvent: False"); PrintEndl();
		    return false;
		  }
		  if( fabs(mc_eta->at(l1idx))<5. && 
		      fabs(mc_eta->at(l2idx))<5. &&
		      fabs(mc_eta->at(l3idx))<5. && 
		      fabs(mc_eta->at(l4idx))<5. &&
		      m>m_ZZmDown2                ){
		    PrintOut(" -> KeepEvent: False"); PrintEndl();
		    return false;
		  }
		}
		else if(m_ZZType==1){
		  if( !( fabs(mc_eta->at(l1idx))<5. &&
			 fabs(mc_eta->at(l2idx))<5. &&
			 fabs(mc_eta->at(l3idx))<5. &&
			 fabs(mc_eta->at(l4idx))<5.  )  ||
		      ( m<m_ZZmDown && m>m_ZZmUp )         ){
		    PrintOut(" -> KeepEvent: False"); PrintEndl();
		    return false;
		  }
		}
		else if(m_ZZType==2){
		  if( !( fabs(mc_eta->at(l1idx))<5. && 
			 fabs(mc_eta->at(l2idx))<5. && 
			 fabs(mc_eta->at(l3idx))<5. && 
			 fabs(mc_eta->at(l4idx))<5.  )  || 
		      (m<m_ZZmDown2)                     ){
		    PrintOut(" -> KeepEvent: False"); PrintEndl();
		    return false;
		  }
		}
	      }
	    }
	  }
	}
      }
    }
  }
  PrintOut(" -> KeepEvent: True"); PrintEndl();
  return true;
  
}

void h4l::LeptonHistForEfficiencies(){
  
  Print("H4lNtupleCreator:: Filling Histograms for leptons efficiencies ...");
  
  for(UInt_t mu=0; mu<Lept_Index.size(); mu++){
    if( abs(Lept_Id.at(mu))!=m_MuId ||
	abs(Lept_Id.at(mu))!=m_TaId  )
      continue;
    MuReco_pT->Fill( Lept_pt.at(mu) );//Muon_pt.at(mu) );
    MuReco_eta->Fill( Lept_eta.at(mu) );//Muon_eta.at(mu) );
  }

  for(UInt_t el=0; el<Lept_Index.size(); el++){
    if( abs(Lept_Id.at(el))!=m_ElId ||
	abs(Lept_Id.at(el))!=m_TaId  )
      continue;
    ElReco_eT->Fill( Lept_pt.at(el) );//Elec_pt.at(el) );
    ElReco_eta->Fill( Lept_eta.at(el) );//Elec_eta.at(el) );
  }

  for(Int_t thp=0; thp<mc_n; thp++){
    
    if( fabs(mc_pdgId->at(thp))==m_MuId && 
        mc_status->at(thp)==1           &&
	mc_pt->at(thp)/GeV>5.            ){
      
      MuTruth_pT->Fill(mc_pt->at(thp)/GeV);
      MuTruth_eta->Fill(mc_eta->at(thp));
    }
    
    if( fabs(mc_pdgId->at(thp))==m_ElId && 
	mc_status->at(thp)==1           && 
	mc_pt->at(thp)/GeV>6.            ){
      
      ElTruth_eT->Fill(mc_pt->at(thp)/GeV);
      ElTruth_eta->Fill(mc_eta->at(thp));
      
    }
  }
  
}

void h4l::LeptonEfficiencies(){
  
  Print("H4lNtupleCreator:: Creating leptons efficiencies histograms ...");
  
  MuEff_pT_Graph = new TGraphAsymmErrors();
  MuEff_pT_Graph = GDivide(MuReco_pT,MuTruth_pT,"MuEff_pT_Graph");
  MuEff_pT_Graph->GetXaxis()->SetTitle("p_{T} [GeV]");
  MuEff_pT_Graph->GetYaxis()->SetTitle("#epsilon");
  MuEff_pT_Graph->GetXaxis()->SetRangeUser(0.,120);
  MuEff_pT_Graph->GetYaxis()->SetRangeUser(0.,1.2);
  MuEff_pT_Graph->SetMarkerStyle(33);
  MuEff_pT_Graph->SetMarkerSize(0.9);
  
  MuReco_pT->Sumw2();
  MuTruth_pT->Sumw2();
  MuEff_pT->Divide(MuReco_pT,MuTruth_pT);
  MuEff_pT->GetXaxis()->SetTitle("p_{T} [GeV]");
  MuEff_pT->GetYaxis()->SetTitle("#epsilon");
  MuEff_pT->GetXaxis()->SetRangeUser(0.,120);
  MuEff_pT->GetYaxis()->SetRangeUser(0.,1.2);
  MuEff_pT->SetMarkerStyle(33);
  MuEff_pT->SetMarkerSize(0.9);
  
  MuEff_eta_Graph = new TGraphAsymmErrors();
  MuEff_eta_Graph = GDivide(MuReco_eta,MuTruth_eta,"MuEff_eta_Graph");
  MuEff_eta_Graph->GetXaxis()->SetTitle("#eta");
  MuEff_eta_Graph->GetYaxis()->SetTitle("#epsilon");
  MuEff_eta_Graph->GetXaxis()->SetRangeUser(-3.2,3.2);
  MuEff_eta_Graph->GetYaxis()->SetRangeUser(0.,1.2);
  MuEff_eta_Graph->SetMarkerStyle(33);
  MuEff_eta_Graph->SetMarkerSize(0.9);
  
  MuReco_eta->Sumw2();
  MuTruth_eta->Sumw2();
  MuEff_eta->Divide(MuReco_eta,MuTruth_eta);
  MuEff_eta->GetXaxis()->SetTitle("#eta");
  MuEff_eta->GetYaxis()->SetTitle("#epsilon");
  MuEff_eta->GetXaxis()->SetRangeUser(-3.2,3.2);
  MuEff_eta->GetYaxis()->SetRangeUser(0.,1.2);
  MuEff_eta->SetMarkerStyle(33);
  MuEff_eta->SetMarkerSize(0.9);
  
  ElEff_eT_Graph = new TGraphAsymmErrors();
  ElEff_eT_Graph = GDivide(ElReco_eT,ElTruth_eT,"ElEff_eT_Graph");
  ElEff_eT_Graph->GetXaxis()->SetTitle("E_{T} [GeV]");
  ElEff_eT_Graph->GetYaxis()->SetTitle("#epsilon");
  ElEff_eT_Graph->GetXaxis()->SetRangeUser(0.,120);
  ElEff_eT_Graph->GetYaxis()->SetRangeUser(0.,1.2);
  ElEff_eT_Graph->SetMarkerStyle(33);
  ElEff_eT_Graph->SetMarkerSize(0.9);

  ElReco_eT->Sumw2();
  ElTruth_eT->Sumw2();
  ElEff_eT->Divide(ElReco_eT,ElTruth_eT);
  ElEff_eT->GetXaxis()->SetTitle("E_{T} [GeV]");
  ElEff_eT->GetYaxis()->SetTitle("#epsilon");
  ElEff_eT->GetXaxis()->SetRangeUser(0.,120);
  ElEff_eT->GetYaxis()->SetRangeUser(0.,1.2);
  ElEff_eT->SetMarkerStyle(33);
  ElEff_eT->SetMarkerSize(0.9);
  
  ElEff_eta_Graph = new TGraphAsymmErrors();
  ElEff_eta_Graph = GDivide(ElReco_eta,ElTruth_eta,"ElEff_eta_Graph");
  ElEff_eta_Graph->GetXaxis()->SetTitle("#eta");
  ElEff_eta_Graph->GetYaxis()->SetTitle("#epsilon");
  ElEff_eta_Graph->GetXaxis()->SetRangeUser(-3.2,3.2);
  ElEff_eta_Graph->GetYaxis()->SetRangeUser(0.,1.2);
  ElEff_eta_Graph->SetMarkerStyle(33);
  ElEff_eta_Graph->SetMarkerSize(0.9);

  ElReco_eta->Sumw2();
  ElTruth_eta->Sumw2();
  ElEff_eta->Divide(ElReco_eta,ElTruth_eta);
  ElEff_eta->GetXaxis()->SetTitle("#eta");
  ElEff_eta->GetYaxis()->SetTitle("#epsilon");
  ElEff_eta->GetXaxis()->SetRangeUser(-3.2,3.2);
  ElEff_eta->GetYaxis()->SetRangeUser(0.,1.2);
  ElEff_eta->SetMarkerStyle(33);
  ElEff_eta->SetMarkerSize(0.9);
  
  if(!m_UseProof){
    MuReco_pT->Write();
    MuReco_eta->Write();
    MuTruth_pT->Write();
    MuTruth_eta->Write();
    ElReco_eT->Write();
    ElReco_eta->Write();
    ElTruth_eT->Write();
    ElTruth_eta->Write();
    MuEff_pT->Write();
    MuEff_eta->Write();
    ElEff_eT->Write();
    ElEff_eta->Write();
    MuEff_pT_Graph->Write();
    MuEff_eta_Graph->Write();
    ElEff_eT_Graph->Write();
    ElEff_eta_Graph->Write();
  }
  
}

TGraphAsymmErrors* h4l::GDivide(TH1F* h1,
				   TH1F* h2,
				   const char*newname){
  
  TAxis* t1 = h1->GetXaxis();
  Int_t Nbx = t1->GetNbins();
  
  TGraphAsymmErrors *gtmp = new TGraphAsymmErrors(Nbx);
  gtmp->SetName(newname);
  Double_t x1,x2,xval;
  Double_t effo,erro;
  Double_t eup,edo;
  for (Int_t bin=0;bin<Nbx;bin++){
    xval=h1->GetBinCenter(bin);
    x1 = h1->GetBinContent(bin);
    x2 = h2->GetBinContent(bin);
    effo=0.5;
    eup=1.;
    edo=0.;
    erro=0.5;
    if (x2>0) {
      effo = x1/x2;
      erro=pow(4.*effo*(1.-effo)/x2+1./(x2*x2),0.5)/(1.+1./x2)/2.;
      eup=(2.*effo+1./x2)/(1.+1./x2)/2.+erro; //upper limit
      edo=(2.*effo+1./x2)/(1.+1./x2)/2.-erro; //lower limit
    }
    //if(effo>1) { effo=1; }
    gtmp->SetPoint(bin,xval,effo);
    gtmp->SetPointEYhigh(bin,eup-effo);
    gtmp->SetPointEYlow(bin,effo-edo);
  }
  return gtmp;
}


CLHEP::HepMatrix h4l::GetLepCovMatrix(TLorentzVector lep,
					 int idl){
  
  CLHEP::HepMatrix Mat;
  int lidx=Lept_Index.at(idl);
  if(abs(Lept_Id.at(idl))==m_MuId){
    if(Lept_CALOgood.at(idl)==1){
      Mat = ZMassConstraint::getCovarianceMatrixd0z0PhiThetaPMuon( lep.P(),
								   mu_calo_cov_d0_exPV->at(lidx),
								   mu_calo_cov_z0_exPV->at(lidx),
								   mu_calo_cov_phi_exPV->at(lidx),
								   mu_calo_cov_theta_exPV->at(lidx),
								   mu_calo_cov_qoverp_exPV->at(lidx)*pow(Lept_resoscf.at(idl),2),
								   mu_calo_cov_d0_z0_exPV->at(lidx),
								   mu_calo_cov_d0_phi_exPV->at(lidx),
								   mu_calo_cov_d0_theta_exPV->at(lidx),
								   mu_calo_cov_d0_qoverp_exPV->at(lidx)*Lept_resoscf.at(idl),
								   mu_calo_cov_z0_phi_exPV->at(lidx),
								   mu_calo_cov_z0_theta_exPV->at(lidx),
								   mu_calo_cov_z0_qoverp_exPV->at(lidx)*Lept_resoscf.at(idl),
								   mu_calo_cov_phi_theta_exPV->at(lidx),
								   mu_calo_cov_phi_qoverp_exPV->at(lidx)*Lept_resoscf.at(idl),
								   mu_calo_cov_theta_qoverp_exPV->at(lidx)*Lept_resoscf.at(idl) );
      
    }
    else{
      Mat = ZMassConstraint::getCovarianceMatrixd0z0PhiThetaPMuon( lep.P(),
								   mu_staco_cov_d0_exPV->at(lidx),
								   mu_staco_cov_z0_exPV->at(lidx),
								   mu_staco_cov_phi_exPV->at(lidx),
								   mu_staco_cov_theta_exPV->at(lidx),
								   mu_staco_cov_qoverp_exPV->at(lidx)*pow(Lept_resoscf.at(idl),2),
								   mu_staco_cov_d0_z0_exPV->at(lidx),
								   mu_staco_cov_d0_phi_exPV->at(lidx),
								   mu_staco_cov_d0_theta_exPV->at(lidx),
								   mu_staco_cov_d0_qoverp_exPV->at(lidx)*Lept_resoscf.at(idl),
								   mu_staco_cov_z0_phi_exPV->at(lidx),
								   mu_staco_cov_z0_theta_exPV->at(lidx),
								   mu_staco_cov_z0_qoverp_exPV->at(lidx)*Lept_resoscf.at(idl),
								   mu_staco_cov_phi_theta_exPV->at(lidx),
								   mu_staco_cov_phi_qoverp_exPV->at(lidx)*Lept_resoscf.at(idl),
								   mu_staco_cov_theta_qoverp_exPV->at(lidx)*Lept_resoscf.at(idl) );
    }
  }
  else if(abs(Lept_Id.at(idl))==m_ElId){
    if(m_year==YEAR_2011){
      Mat = ZMassConstraint::getCovarianceMatrixd0z0PhiThetaPElectron( Lept_Ereso.at(idl),
								       el_GSF_trackcov_d0->at(lidx),
								       el_GSF_trackcov_z0->at(lidx),
								       el_GSF_trackcov_phi->at(lidx),
								       el_GSF_trackcov_theta->at(lidx),
								       el_GSF_trackcov_d0_z0->at(lidx),
								       el_GSF_trackcov_d0_phi->at(lidx),
								       el_GSF_trackcov_d0_theta->at(lidx),
								       el_GSF_trackcov_z0_phi->at(lidx),
								       el_GSF_trackcov_z0_theta->at(lidx),
								       el_GSF_trackcov_phi_theta->at(lidx) );
								       
    }
    else if(m_year==YEAR_2012){
      Mat = ZMassConstraint::getCovarianceMatrixd0z0PhiThetaPElectron( Lept_Ereso.at(idl),
								       el_trackcov_d0->at(lidx),
								       el_trackcov_z0->at(lidx),
								       el_trackcov_phi->at(lidx),
								       el_trackcov_theta->at(lidx),
								       el_trackcov_d0_z0->at(lidx),
								       el_trackcov_d0_phi->at(lidx),
								       el_trackcov_d0_theta->at(lidx),
								       el_trackcov_z0_phi->at(lidx),
								       el_trackcov_z0_theta->at(lidx),
								       el_trackcov_phi_theta->at(lidx) );
      
    }
  }
  
  return Mat;
  
}

CLHEP::HepMatrix h4l::GetPhCovMatrix(double enres){
  
  CLHEP::HepMatrix Mat;
  Mat=ZMassConstraint::getCovarianceMatrixd0z0PhiThetaPElectron(enres,
								0.000001,  
								0.000001,  
								0.000001,  
								0.000001,  
								0.0,  
								0.0,  
								0.0,
								0.0,
								0.0,  
								0.0);
  
  return Mat;
  
}
