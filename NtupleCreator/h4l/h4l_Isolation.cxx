#include "h4l.h"

std::vector<int> h4l::TrackIsoCut(bool Corrections){
  
  Print("H4lNtupleCreator:: Applying Track Isolation Cut ...");
  
  m_TrackIso.clear();
  bool trkiso=false;
  TrackIsoCorrection();
  
  for(UInt_t q=0; q<Quadruplets.size(); q++){
    trkiso=false;
    
    if(QuadSubChan.at(q)<4){
      int l1=Quadruplets.at(q).at(0);
      int l2=Quadruplets.at(q).at(1);
      int l3=Quadruplets.at(q).at(2);
      int l4=Quadruplets.at(q).at(3);
      double ptcone20_l1=0,ptcone20_l2=0,ptcone20_l3=0,ptcone20_l4=0;
      
      if(!Corrections){
	ptcone20_l1=GetLeptonTrkIso(l1).at(0);
	ptcone20_l2=GetLeptonTrkIso(l2).at(0);
	ptcone20_l3=GetLeptonTrkIso(l3).at(0);
	ptcone20_l4=GetLeptonTrkIso(l4).at(0);
      }
      else{
	ptcone20_l1=IsoTrk20_corr.at(q).at(0);
	ptcone20_l2=IsoTrk20_corr.at(q).at(1);
	ptcone20_l3=IsoTrk20_corr.at(q).at(2);
	ptcone20_l4=IsoTrk20_corr.at(q).at(3);
      }
      
      if(LeptonTrkIso(ptcone20_l1,l1) && LeptonTrkIso(ptcone20_l2,l2) &&
	 LeptonTrkIso(ptcone20_l3,l3) && LeptonTrkIso(ptcone20_l4,l4) ){
	trkiso=true;
      }
    }
    m_TrackIso.push_back(trkiso);
    PrintOut("                           Quadruplet: "); 
    PrintOut((int)q); PrintEndl();
    PrintOut("                               TrackIsoGood: "); 
    PrintOut(m_TrackIso.at(q)); PrintEndl();
  }
  return m_TrackIso;
  
}

void h4l::TrackIsoCorrection(){
  
  Print("H4lNtupleCreator:: Correcting Track Isolation for Leptons in Quadruplet ... ");
  
  double corr20=0., corr30=0., corr40=0.;
  
  for(UInt_t q=0; q<Quadruplets.size(); q++){
    m_tmpiso20.clear(), m_tmpiso30.clear(), m_tmpiso40.clear();
    
    if(QuadSubChan.at(q)<4){
      for(UInt_t l1=0; l1<Quadruplets.at(q).size(); l1++){
	int idxl1=Quadruplets.at(q).at(l1);
	corr20=0., corr30=0., corr40=0.;
	
	for(UInt_t l2=0; l2<Quadruplets.at(q).size(); l2++){
	  int idxl2=Quadruplets.at(q).at(l2);
	  
	  if(l2==l1) continue;
	  if(Lept_SAgood.at(idxl2)==1) continue;
	  double etaL1=Lept_eta.at(idxl1);
	  double etaL2=Lept_eta.at(idxl2);
	  double phiL1=Lept_phi.at(idxl1);
	  double phiL2=Lept_phi.at(idxl2);
	  
	  double Dr=ComputeDeltaR(etaL1,phiL1,etaL2,phiL2);
	  
	  if(Dr<0.20){
	    PrintOut("                  Dr<0.20 found! Correcting ... "); PrintEndl();
	    //if(l2<2)
	    //corr20+=Muon_pid[idxl2]*sin(Muon_theta_id[idxl2]);
	    //else
	    //corr20+=Elec_trkpt[idxl2];
	    corr20+=LeptonTrkIsoCorrection(idxl2);
	  }
	  if(Dr<0.30){
	    PrintOut("                  Dr<0.30 found! Correcting ... "); PrintEndl();
	    //if(l2<2)
	    //  corr30+=Muon_pid[idxl2]*sin(Muon_theta_id[idxl2]); 
	    //else
	    //  corr30+=Elec_trkpt[idxl2];
	    corr30+=LeptonTrkIsoCorrection(idxl2);
	  }
	  if(Dr<0.40){
	    PrintOut("                  Dr<0.40 found! Correcting ... "); PrintEndl();
	  //if(l2<2)
	  //  corr40+=Muon_pid[idxl2]*sin(Muon_theta_id[idxl2]); 
	  //else
	  //  corr40+=Elec_trkpt[idxl2];
	  corr40+=LeptonTrkIsoCorrection(idxl2);
	  }
	}
	
	//if(l1<2){
	//	tmpiso20.push_back(Muon_isotrk20[idxl1]-corr20);
	//	tmpiso30.push_back(Muon_isotrk30[idxl1]-corr30);
	//	tmpiso40.push_back(Muon_isotrk40[idxl1]-corr40);
	//}
	//else{
	//	tmpiso20.push_back(Elec_isotrk20[idxl1]-corr20);
	//	tmpiso30.push_back(Elec_isotrk30[idxl1]-corr30);
	//	tmpiso40.push_back(Elec_isotrk40[idxl1]-corr40);
	//}
	m_tmpiso20.push_back( GetLeptonTrkIso(idxl1).at(0)-corr20 );
	m_tmpiso30.push_back( GetLeptonTrkIso(idxl1).at(1)-corr30 );
	m_tmpiso40.push_back( GetLeptonTrkIso(idxl1).at(2)-corr40 );
      }
    }
    else{
      m_tmpiso20.push_back( 0. );
      m_tmpiso30.push_back( 0. );
      m_tmpiso40.push_back( 0. );
    }
    IsoTrk20_corr.push_back(m_tmpiso20);
    IsoTrk30_corr.push_back(m_tmpiso30);
    IsoTrk40_corr.push_back(m_tmpiso40);
  }
  
}

double h4l::LeptonTrkIsoCorrection(int idl){

  double corr=0.;
  int lind=Lept_Index.at(idl);
  
  if(abs(Lept_Id.at(idl))==13){
    if(Lept_CALOgood.at(idl)==1){
      corr = ( (fabs(1./mu_calo_id_qoverp->at(lind))/GeV)*
	       mu_calo_id_theta->at(lind) );
    }
    else{
      corr = ( (fabs(1./mu_staco_id_qoverp->at(lind))/GeV)*
	       mu_staco_id_theta->at(lind) );
    }
  }
  else if(abs(Lept_Id.at(idl))==11){
    if(m_year==YEAR_2011){
      corr = el_GSF_trackpt->at(lind)/GeV;
    }
    else if(m_year==YEAR_2012){
      corr = el_trackpt->at(lind)/GeV;
    }
  }
  
  return corr;
}

std::vector<double> h4l::GetLeptonTrkIso(int idl){
  
  m_trkiso.clear();
  int lind=Lept_Index.at(idl);
  
  if(abs(Lept_Id.at(idl))==13){
    if(Lept_CALOgood.at(idl)==1){
      m_trkiso.push_back(mu_calo_ptcone20->at(lind)/GeV);
      m_trkiso.push_back(mu_calo_ptcone30->at(lind)/GeV);
      m_trkiso.push_back(mu_calo_ptcone40->at(lind)/GeV);
    }
    else{
      m_trkiso.push_back(mu_staco_ptcone20->at(lind)/GeV);
      m_trkiso.push_back(mu_staco_ptcone30->at(lind)/GeV);
      m_trkiso.push_back(mu_staco_ptcone40->at(lind)/GeV);
    }
  }
  else if(abs(Lept_Id.at(idl))==11){
    if(m_year==YEAR_2011){
      m_trkiso.push_back(el_GSF_ptcone20->at(lind)/GeV);
      m_trkiso.push_back(el_GSF_ptcone30->at(lind)/GeV);
      m_trkiso.push_back(el_GSF_ptcone40->at(lind)/GeV);
    }
    else if(m_year==YEAR_2012){
      m_trkiso.push_back(el_ptcone20->at(lind)/GeV);
      m_trkiso.push_back(el_ptcone30->at(lind)/GeV);
      m_trkiso.push_back(el_ptcone40->at(lind)/GeV);
    }
  }
  
  return m_trkiso;
}

bool h4l::LeptonTrkIso(double ptcone,
		       int idl){
  
  double pt_et=0.;
  int lind=Lept_Index.at(idl);
  if(abs(Lept_Id.at(idl))==13){
    pt_et=Lept_pt.at(idl);
  }
  else if(abs(Lept_Id.at(idl))==11){
    if(m_year==YEAR_2011){
      pt_et=Lept_E.at(idl)/TMath::CosH(el_GSF_tracketa->at(lind));
    }
    else if(m_year==YEAR_2012){
      pt_et=Lept_E.at(idl)/TMath::CosH(el_tracketa->at(lind));
    }
  }
  
  if(ptcone/pt_et<m_TrkIsoCut)
    return true;

  return false;
  
}

std::vector<int> h4l::CaloIsoCut(bool Corrections){

  Print("H4lNtupleCreator:: Applying Calo Isolation Cut ...");
  
  m_CaloIso.clear();
  bool isocut=false;
  CaloIsoCorrection();
    
  for(UInt_t q=0; q<Quadruplets.size(); q++){
    isocut=false;
    if(QuadSubChan.at(q)<4){
      int l1=Quadruplets.at(q).at(0);
      int l2=Quadruplets.at(q).at(1);
      int l3=Quadruplets.at(q).at(2);
      int l4=Quadruplets.at(q).at(3);
      double etcone20_l1,etcone20_l2,etcone20_l3,etcone20_l4;
      
      if(!Corrections){
	etcone20_l1=GetLeptonCaloIso(l1).at(0);
	etcone20_l2=GetLeptonCaloIso(l2).at(0);
	etcone20_l3=GetLeptonCaloIso(l3).at(0);
	etcone20_l4=GetLeptonCaloIso(l4).at(0);
      }
      else{
      etcone20_l1=IsoCalo20_corr.at(q).at(0);
      etcone20_l2=IsoCalo20_corr.at(q).at(1);
      etcone20_l3=IsoCalo20_corr.at(q).at(2);
      etcone20_l4=IsoCalo20_corr.at(q).at(3);
      }
      
      if(LeptonCaloIso(etcone20_l1,l1) && LeptonCaloIso(etcone20_l2,l2) && 
	 LeptonCaloIso(etcone20_l3,l3) && LeptonCaloIso(etcone20_l4,l4) ){
	isocut=true;
      }
    }
    m_CaloIso.push_back(isocut);
    PrintOut("                           Quadruplet: "); 
    PrintOut((int)q); PrintEndl();
    PrintOut("                               CaloIsoGood: "); 
    PrintOut(m_CaloIso.at(q));PrintEndl();
  }
  return m_CaloIso;
  
}

void h4l::CaloIsoCorrection(){
  
  Print("H4lNtupleCreator:: Correcting calo isolation for Electrons in Quadruplets ... ");
  
  double corr20=0., corr30=0., corr40=0.;
  double TMPcorr20=0., TMPcorr30=0., TMPcorr40=0.;
  bool type=true;
  if(m_type==DATA)
    type=false;

  for(UInt_t q=0; q<Quadruplets.size(); q++){
    m_tmpiso20.clear(), m_tmpiso30.clear(), m_tmpiso40.clear();
    
    if(QuadSubChan.at(q)<4){
      for(UInt_t l1=0; l1<Quadruplets.at(q).size(); l1++){
	int idxl1=Quadruplets.at(q).at(l1);
	corr20=0., corr30=0., corr40=0.;
	TMPcorr20=0., TMPcorr30=0., TMPcorr40=0.;
	double etaL1=0, phiL1=0;
	if(abs(Lept_Id.at(idxl1))==13){
	  TMPcorr20 = GetLeptonCaloIso(idxl1).at(0)*GeV;
	  TMPcorr30 = GetLeptonCaloIso(idxl1).at(1)*GeV;
	  TMPcorr40 = GetLeptonCaloIso(idxl1).at(2)*GeV;
	  etaL1 = Lept_eta.at(idxl1);
	  phiL1 = Lept_phi.at(idxl1);
	}
	else{
	  if(m_UseCorrections){
	    TMPcorr20 = GetCorrElecCaloIso(idxl1,type).at(0);
	  TMPcorr30 = GetCorrElecCaloIso(idxl1,type).at(1);
	  TMPcorr40 = GetCorrElecCaloIso(idxl1,type).at(2);
	  }
	  else{
	    TMPcorr20 = GetLeptonCaloIso(idxl1).at(0)*GeV;
	    TMPcorr30 = GetLeptonCaloIso(idxl1).at(1)*GeV;
	    TMPcorr40 = GetLeptonCaloIso(idxl1).at(2)*GeV;
	  }
	  if(m_year==YEAR_2011){
	    etaL1 = Lept_eta.at(idxl1);
	    phiL1 = Lept_phi.at(idxl1);
	  }
	  else if(m_year==YEAR_2012){
	    etaL1 = el_etas2->at(Lept_Index.at(idxl1));
	    phiL1 = el_phis2->at(Lept_Index.at(idxl1));
	  }
	}
	for(UInt_t l2=0; l2<Quadruplets.at(q).size(); l2++){
	  int idxl2=Quadruplets.at(q).at(l2);
	  
	  //if(l2==l1 || l2<2) continue;
	  if(l2==l1) continue;
	  if(abs(Lept_Id.at(idxl2))==13) continue;
	  //double etaL1=0, phiL1=0;
	  //if(l1<2){
	  //  etaL1 = Muon_eta.at(idxl1);
	  //  phiL1 = Muon_phi.at(idxl1);
	  //}
	  //else{
	  //  if(m_year=="2011"){
	  //    etaL1 = Elec_eta.at(idxl1);
	  //    phiL1 = Elec_phi.at(idxl1);
	  //  }
	  //  else if(m_year=="2012"){
	  //    etaL1 = Elec_etaS2.at(idxl1);
	  //    phiL1 = Elec_phiS2.at(idxl1);
	  //  }
	  //}
	  double etaL2=0, phiL2=0;
	  if(m_year==YEAR_2011){
	    etaL2 = Lept_eta.at(idxl2);
	    phiL2 = Lept_phi.at(idxl2);
	  }
	  else if(m_year==YEAR_2012){
	    etaL2 = el_etas2->at(Lept_Index.at(idxl2));
	    phiL2 = el_phis2->at(Lept_Index.at(idxl2));
	  }
	  double Dr = ComputeDeltaR( etaL1,phiL1,etaL2,phiL2 );
	  double En = 0;
	  if(m_year==YEAR_2011){
	    En = Lept_E.at(idxl2);
	  }
	  else if(m_year==YEAR_2012){
	    En = el_rawcl_E->at(Lept_Index.at(idxl2))/GeV;
	  }
	
	  if(Dr<0.18){
	    PrintOut("                  Dr<0.18 found! Correcting ... "); PrintEndl();
	    corr20+=En/TMath::CosH(Lept_eta.at(idxl2));
	  }
	  if(Dr<0.30){ 
	    PrintOut("                  Dr<0.30 found! Correcting ... "); PrintEndl();
	    corr30+=En/TMath::CosH(Lept_eta.at(idxl2));
	  }
	  if(Dr<0.40){ 
	    PrintOut("                  Dr<0.40 found! Correcting ... "); PrintEndl();
	    corr40+=En/TMath::CosH(Lept_eta.at(idxl2));
	  }
	
	}
	m_tmpiso20.push_back(TMPcorr20/GeV-corr20);
	m_tmpiso30.push_back(TMPcorr30/GeV-corr30);
	m_tmpiso40.push_back(TMPcorr40/GeV-corr40);
      }
    }
    else{
      m_tmpiso20.push_back( 0. );
      m_tmpiso30.push_back( 0. );
      m_tmpiso40.push_back( 0. );
    }
    IsoCalo20_corr.push_back(m_tmpiso20);
    IsoCalo30_corr.push_back(m_tmpiso30);
    IsoCalo40_corr.push_back(m_tmpiso40);
  }
  
}

std::vector<double> h4l::GetLeptonCaloIso(int idl){
  
  m_caloiso.clear();
  int lind=Lept_Index.at(idl);
  
  if(abs(Lept_Id.at(idl))==13){
    if(Lept_CALOgood.at(idl)==1){
      m_caloiso.push_back(mu_calo_etcone20->at(lind)/GeV);
      m_caloiso.push_back(mu_calo_etcone30->at(lind)/GeV);
      m_caloiso.push_back(mu_calo_etcone40->at(lind)/GeV);
    }
    else{
      m_caloiso.push_back(mu_staco_etcone20->at(lind)/GeV);
      m_caloiso.push_back(mu_staco_etcone30->at(lind)/GeV);
      m_caloiso.push_back(mu_staco_etcone40->at(lind)/GeV);
    }
  }
  else if(abs(Lept_Id.at(idl))==11){
    if(m_year==YEAR_2011){
      m_caloiso.push_back(el_GSF_Etcone20->at(lind)/GeV);
      m_caloiso.push_back(el_GSF_Etcone30->at(lind)/GeV);
      m_caloiso.push_back(el_GSF_Etcone40->at(lind)/GeV);
    }
    else if(m_year==YEAR_2012){
      m_caloiso.push_back(el_topoEtcone20->at(lind)/GeV);
      m_caloiso.push_back(el_topoEtcone30->at(lind)/GeV);
      m_caloiso.push_back(el_topoEtcone40->at(lind)/GeV);
    }
  }
  
  return m_caloiso;
}

std::vector<double> h4l::GetCorrElecCaloIso(int idl,
					    bool type){
  
  m_elcaloiso.clear();
  int lind=Lept_Index.at(idl);
  double v20=0., v30=0., v40=0.;
  if(m_year==YEAR_2011){
    v20 = CaloIsoCorrection::GetNPVCorrectedIsolation(nPV,
						      el_GSF_etas2->at(lind),
						      20.0,
						      type,
						      el_GSF_Etcone20->at(lind),
						      CaloIsoCorrection::ELECTRON,
						      CaloIsoCorrection::REL17);
    
    v30 = CaloIsoCorrection::GetNPVCorrectedIsolation(nPV,
						      el_GSF_etas2->at(lind),
						      30.0,
						      type,
						      el_GSF_Etcone30->at(lind),
						      CaloIsoCorrection::ELECTRON,
						      CaloIsoCorrection::REL17);
    
    v40 = CaloIsoCorrection::GetNPVCorrectedIsolation(nPV,
						      el_GSF_etas2->at(lind),
						      40.0,
						      type,
						      el_GSF_Etcone40->at(lind),
						      CaloIsoCorrection::ELECTRON,
						      CaloIsoCorrection::REL17);
  }
  else if(m_year==YEAR_2012){
    v20 = CaloIsoCorrection::GetPtEDCorrectedTopoIsolation(el_ED_median->at(lind),
							   el_cl_E->at(lind),//Lept_E.at(idl)*GeV,
							   el_etas2->at(lind),
							   el_etap->at(lind),
							   el_cl_eta->at(lind),
							   20.0,
							   type,
							   el_topoEtcone20->at(lind),
							   false,
							   CaloIsoCorrection::ELECTRON,
							   CaloIsoCorrection::REL17_2);
    
    v30 = CaloIsoCorrection::GetPtEDCorrectedTopoIsolation(el_ED_median->at(lind),
							   el_cl_E->at(lind),//Lept_E.at(idl)*GeV,
							   el_etas2->at(lind),
							   el_etap->at(lind),
							   el_cl_eta->at(lind),
							   30.0,
							   type,
							   el_topoEtcone30->at(lind),
							   false,
							   CaloIsoCorrection::ELECTRON,
							   CaloIsoCorrection::REL17_2);
    
    v40 = CaloIsoCorrection::GetPtEDCorrectedTopoIsolation(el_ED_median->at(lind),
							   el_cl_E->at(lind),//Lept_E.at(idl)*GeV,
							   el_etas2->at(lind),
							   el_etap->at(lind),
							   el_cl_eta->at(lind),
							   40.0,
							   type,
							   el_topoEtcone40->at(lind),
							   false,
							   CaloIsoCorrection::ELECTRON,
							   CaloIsoCorrection::REL17_2);
  }
  m_elcaloiso.push_back(v20);
  m_elcaloiso.push_back(v30);
  m_elcaloiso.push_back(v40);
  
  return m_elcaloiso;
  
}

bool h4l::LeptonCaloIso(double etcone,
			   int idl){
  
  double pt_et=0.;
  double CaloIsoCut=0.;
  int lind=Lept_Index.at(idl);
  if(abs(Lept_Id.at(idl))==13){
    pt_et=Lept_pt.at(idl);
    if(Lept_SAgood.at(idl)==1){ CaloIsoCut=m_CalIsoCutSAMu; }
    else{ CaloIsoCut=m_CalIsoCutMu; }
  }
  else if(abs(Lept_Id.at(idl))==11){
    CaloIsoCut=m_CalIsoCutEl;
    if(m_year==YEAR_2011){
      pt_et=Lept_E.at(idl)/TMath::CosH(el_GSF_tracketa->at(lind));
    }
    else if(m_year==YEAR_2012){
      pt_et=Lept_E.at(idl)/TMath::CosH(el_tracketa->at(lind));
    }
  }
  
  if(etcone/pt_et<CaloIsoCut)
    return true;

  return false;
  
}
