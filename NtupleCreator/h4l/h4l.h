#ifndef h4l_h
#define h4l_h

/* basic C++ headers */
#include <vector>
#include <fstream>
#include <sstream>
#include <string>
#include <utility>
/* ROOT headers */
#include "TChain.h"
#include "TFile.h"
#include "TRandom3.h"
#include "TSelector.h"
#include "TH1F.h"
#include "TGraphAsymmErrors.h"
#include "TPython.h"
/* Utils */
#include "PileupReweighting/TPileupReweighting.h"
#include "MuonMomentumCorrections/SmearingClass.h"
#include "MuonMomentumCorrections/MuonResolutionAndMomentumScaleFactors.h"
#include "MuonEfficiencyCorrections/AnalysisMuonEfficiencyScaleFactors.h"
#include "MuonEfficiencyCorrections/AnalysisMuonConfigurableScaleFactors.h"
#include "TrigMuonEfficiency/TriggerNavigationVariables.h"
#include "TrigMuonEfficiency/MuonTriggerMatching.h"
#include "TrigMuonEfficiency/ElectronTriggerMatching.h"
#include "TrigMuonEfficiency/LeptonTriggerSF.h"
#include "egammaAnalysisUtils/CaloIsoCorrection.h"
//#include "egammaAnalysisUtils/EnergyRescalerUpgrade.h"
#include "egammaAnalysisUtils/VertexPositionReweightingTool.h"
#include "egammaAnalysisUtils/FsrPhotons.h"
#include "ZMassConstraint/ZMassConstraint.h"
#include "TileTripReader/TTileTripReader.h"
#include "GoodRunsLists/TGoodRunsListReader.h"
#include "GoodRunsLists/TGoodRunsList.h"
#include "ElectronPhotonSelectorTools/TElectronIsEMSelector.h"
#include "ElectronPhotonSelectorTools/TElectronLikelihoodTool.h"
#include "ApplyJetCalibration/ApplyJetCalibration.h"
#include "ElectronPhotonFourMomentumCorrection/egammaEnergyCorrectionTool.h"
#include "MissingMassCalculator/MissingMassCalculator.h"
#include "../HforToolD3PD/HforToolD3PD.h"
#include "../Particles/Particles.h"

typedef enum{
  YEAR_2011=1,
  YEAR_2012=2,
  YEAR_2015=3
} YEARTYPE;
typedef enum{
  DATA=1,
  MC=2
} DATATYPE;
typedef enum{
  MC11C=1,
  MC11D=2,
  MC12A=3,
  MC12B=4,
  MC12Bd=5,
  MC12C=6
} MCTYPE;

class h4l : public TSelector {
 public :
    
  /*Constructors*/
  h4l();
  
  /*Destructors*/
  virtual ~h4l();
  
  /*Selector Methods and Members*/
  virtual Int_t   Version() const { return 2; }
  
  virtual void    Begin(TTree *tree);
  
  virtual void    SlaveBegin(TTree *tree);
  
  virtual void    Init(TTree *tree);
  
  virtual Bool_t  Notify();
  
  virtual Bool_t  Process(Long64_t entry);
  
  virtual Int_t   GetEntry(Long64_t entry, Int_t getall = 0) { 
    return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0; }
  
  virtual void    SetOption(const char *option) { fOption = option; }
  
  virtual void    SetObject(TObject *obj) { fObject = obj; }

  virtual void    SetInputList(TList *input) { fInput = input; }
  
  virtual TList  *GetOutputList() const { return fOutput; }
  
  virtual void    SlaveTerminate();
  
  virtual void    Terminate();
  
  TTree          *fChain;
  
  ClassDef(h4l,0);

  /* Class Methods */
  
  /*Class Configuration*/
  void Config(std::string,std::string,
	      std::string,std::string);

  /* Set Data Type, MC Type and Year */
  void SetDataType(std::string);
  void SetMCType(std::string);
  void SetYear(std::string);

  /* Tools Year Specific settings */
  void SettingTools_2011();
  void SettingTools_2012();
  
  /* Set Out Files Name*/
  void SetOutFileName(std::string,std::string);
  
  /* Set Initial Values*/
  void SetInitialValues();

  /* Reset Event Flags */
  void ResetEventFlags();
  
  /* Set Tree Branches*/
  void SetTreeBranches(TTree*);

  /* Enable Minimal Set of Variables */
  void UseMinimalVariables(bool);

  /* Enable MC Corrections */
  void UseCorrections(bool);
  
  /* Enable MC Weights */
  void UseWeights(bool);

  /* Enable Particle Minimal Information */
  void UseParticleMinimalInfo(bool);

  /* Enable MET BDT Medium Reading */
  void UseNtupTau(bool);

  /* Set PileUpReweighting Tool*/
  void SetPileUpReweight();

  /* Set Electron Energy Rescaler */
  void SetElecEnergyRescaler();

  /* Set Electron Identification */
  void SetElectronId();
  
  /* Set MuonMomentumCorrections Tool */
  void SetMuonMomentumCorrections();
  
  /* Set MuonEfficiencyCorrections Tool */
  void SetMuonEfficiencyCorrections();

  /* Set MissingMassCalculator Tool */
  void SetMissMassCalc();
  
  /* Add Luminosity per Period to MuonEfficiencyCorrections Tools */
  void AddLumiPerPeriodToMuonEffCorr(Analysis::AnalysisMuonConfigurableScaleFactors*);

  /* Set ElectronEfficiencyCorrections Tool */
  void SetElectronEfficiencyCorrections();
    
  /* Set Trigger Navigation */
  void SetTriggerNavigation(TriggerNavigationVariables*,bool,bool);
  
  /* Set GRL Xml File */
  void SetGRL();

  /* Debug methods */
  void SetDebug(bool);

  void Print(std::string);
  
  void PrintOut(int);
  
  void PrintOut(std::string);
  
  void PrintOut(float);
  
  void PrintOut(double);
  
  void PrintEndl();

  /* Retrieve Luminosity Fractions */
  void RetrieveLumiFractions();

  /* Retrieve Pile Up Weight */
  double GetPileUpWeight();
  
  /* Retrieve Vertex Weight */
  double GetVertexWeight();
  
  /* LAr Error Check */
  bool LarErrorCheck(int);

  /* Tile Error Check */
  bool TileErrorCheck(int);
  
  /* Bad Event Check */
  bool BadEventCheck();

  /* Tile Corruption Error */ 
  bool TileCorruptionCheck(int,int,int);
      
  /* Check GRL */
  bool GRLCheck(int,int);
  
  /* Detect Data/MC Period*/
  int DetectPeriod(Int_t);
  
  /* Vertex Check */
  void VertexCheck();
  
  /* Trigger Check */
  void TriggerCheck(int);

  /* Fill Missing Energy Variables */
  void MissingEnergy();

  /* Fill Jets Information Variables*/
  //void JetsInformation();

  /* Fill Photons Information Variables */
  void PhotonsInformation();
  
  /* Removing MC Event Overlap*/
  void MCEventOverlap(HforToolD3PD,UInt_t);
  
  /* Correct Leptons Track Isolation */
  void TrackIsoCorrection();
  
  /* Correct Lepton Track Isolation */
  double LeptonTrkIsoCorrection(int);

  /* Get Lepton Track Isolation */
  std::vector<double> GetLeptonTrkIso(int);
  
  /* Check Lepton Track Isolation */
  bool LeptonTrkIso(double,int);

  /* Correct Electron Calo Isolation*/
  void CaloIsoCorrection();
  
  /* Get Lepton Calo Isolation */
  std::vector<double> GetLeptonCaloIso(int);

  /* Get Electron Calo Isolation */
  std::vector<double> GetCorrElecCaloIso(int,bool);

  /* Check Lepton Calo Isolation */
  bool LeptonCaloIso(double,int);
  
  /* Select Lepton Quadruplets */
  std::vector<std::vector<int> > SelectQuadruplets();
  
  /* Detect FourLepton Sub-Channel */
  int Detect4LepSubChan(int,int,int,int);//,bool);

  /* Create Lepton/MC TLorentz Vector */
  TLorentzVector GetLepLorentzVec(int,bool useMMC=false,
				  bool useMeV=false);
  TLorentzVector GetMCLorentzVec(int,bool useMeV=false);
 
  /* Select Di-Lepton Pairs */
  std::vector<std::pair<int,int> > SelectDiLeptonPairs();
  
  /* Check D0 Cut */
  std::vector<int> CheckD0Cut();
  
  /* Lepton D0 Cut */
  bool LeptonD0(int);
  
  /* Check Leptons Kinematic*/
  std::vector<int> CheckKinematic();
    
  /* Check Trigger Matching */
  void CheckTriggerMatch(MuonTriggerMatching*,MuonTriggerMatching*,
			 ElectronTriggerMatching*,int);

  bool SingleTrigMatching(MuonTriggerMatching*,
			  ElectronTriggerMatching*,int,UInt_t);
  
  bool DiLepTrigMatching(MuonTriggerMatching*,
			 ElectronTriggerMatching*,int,UInt_t);
  
  bool MuElTrigMatching(ElectronTriggerMatching*,UInt_t);
  
  bool DiMuTrigMatch(MuonTriggerMatching*,std::vector<std::string>,
		     std::vector<double>,UInt_t);

  bool DiMuTrigMatch(MuonTriggerMatching*,std::vector<std::string>,
	   	     std::vector<double>,int,int);

  bool DiElTrigMatch(ElectronTriggerMatching*,std::vector<std::string>,
		     std::vector<double>,UInt_t,int);
  
  bool DiElTrigMatch(ElectronTriggerMatching*,std::vector<std::string>,
		     std::vector<double>,int,int,int);
  
  bool ElMuTrigMatch(ElectronTriggerMatching*,std::vector<std::string>,
		     int,int);

  /* Compute Trigger Scale Factor */
  std::vector<double> ComputeTriggerScaleFactor(LeptonTriggerSF*);
    
  /* Compute Efficiency Scale Factor */
  std::vector<double> ComputeEfficiencyScaleFactor();
  
  
  /* Calculate Z1 and Z2 Masses */
  std::vector<double> ComputeZ1Mass();
  std::vector<double> ComputeZ2Mass();
  std::vector<double> ComputeZ1MMCMass();
  std::vector<double> ComputeZ2MMCMass();
    
  /* Calculate 4lep Mass*/
  std::vector<double> Mass4Lep();
    
  /* Apply Z1 and Z2 Mass Cuts */
  std::vector<int> Z1MassCut();
  std::vector<int> Z2MassCut();
    
  /* Apply DeltaR Cut */
  std::vector<int> CheckDeltaR();
  
  /* Apply JpsiVeto Cut */
  std::vector<int> CheckJpsiVeto();
  
  /* Find Best Quadruplet */
  std::vector<int> FindBestQuadruplet();
    
  /* Apply Track Isolation Cut */
  std::vector<int> TrackIsoCut(bool);
    
  /* Apply Calo Isolation Cut */
  std::vector<int> CaloIsoCut(bool);
    
  /* Apply D0 Significance Cut */
  std::vector<int> D0SignificanceCut();
    
  /* Check Lepton D0 Significance Cut*/
  bool LeptonD0Sign(int);
  
  /* Apply FSR Correction */
  void ApplyFSRCorrection();
  //void DoMuonElectronFSRCorrection();
    
  /* Apply ZMass Constraint */
  void ApplyZMassConstraint();
  //void DoMuonElectronZMassConstraint();
  //void DoElectronMuonZMassConstraint();

  /* Get Lepton and Photon Covariance Matrix */
  CLHEP::HepMatrix GetLepCovMatrix(TLorentzVector,int);
  CLHEP::HepMatrix GetPhCovMatrix(double);
  
  /* Get Final Good Quadruplet Position */
  int GetGoodQuadrupletPosition();

  /* Compute Angular Variables */
  void ComputeAngularVariables();
  //void ComputeMuElAngularVariables();
  //void ComputeElMuAngularVariables();

  /* Check Letons MisPairing+MisLabeling */
  std::vector<int> MisPairing();
  std::vector<int> MisPairLabel();

  /* Tau-events removal */
  void SetTauEventRemoval(bool);
  bool TauEventRemoval();

  /* MC Information */
  int ZStatusInTruth();
  int GetLepMothBcode(int);
  void MCInfo();
  void TausTruthMatching();
  void TauTruthMatch();
  bool CheckMisPairing(int);
  bool CheckMisPairingLabeling(int);
  bool MCSampleOverlap(UInt_t);
  
  /* Di-Lepton Mass */
  double DiLeptonMass(int,int);
  double DiLeptonMMCMass(int,int);
			 
  /* Di-Tau Mass */
  double DiTauMass(TLorentzVector,int,TLorentzVector,int);
    
  /* Compute DeltaR */
  double ComputeDeltaR(double,double,double,double);
  
  /* Load Muon-Electron-Tau-Jet Vectors */
  void LoadMuonVectors();
  void LoadElectronVectors();
  void LoadTauVectors();
  void LoadParticleVectors();

  /* Calculate Lepton Efficiencies */
  void LeptonHistForEfficiencies();
  void LeptonEfficiencies();
  TGraphAsymmErrors* GDivide(TH1F*,TH1F*,const char*);
  
  /* Clear Objects */
  void ClearVectors();

  /* Write Results */
  void WriteElectronCounts();
  void WriteMuonCounts();
  void WriteTauCounts();
  void WriteJetCounts();
  void CutFlow();
  void CutFlow4mu();
  void CutFlow4e();
  void CutFlow2l2l();
  void WriteCutFlow();
  void CreateInfoHistograms();
  
  /* Utils Objects */
  Particles Part;
  FsrPhotons *FSRph;
  Root::TPileupReweighting* PileUpTool;
  AtlasRoot::egammaEnergyCorrectionTool* El_ERescaler;
  //egRescaler::EnergyRescalerUpgrade* El_ERescaler;
  MuonSmear::SmearingClass* MuSmear;
  Analysis::MuonResolutionAndMomentumScaleFactors *MuResoSCF;
  Analysis::AnalysisMuonConfigurableScaleFactors *StacoSCF;
  Analysis::AnalysisMuonConfigurableScaleFactors *StacoSASCF;
  Analysis::AnalysisMuonConfigurableScaleFactors *CaloSCF;
  Root::TElectronEfficiencyCorrectionTool *El_SCFID;
  Root::TElectronEfficiencyCorrectionTool *El_SCFReco;
  TriggerNavigationVariables* TrigNav_Elec;
  ElectronTriggerMatching* TrigMatch_Elec;
  TriggerNavigationVariables* TrigNav_SingleMu;
  MuonTriggerMatching* TrigMatch_SingleMu;
  TriggerNavigationVariables* TrigNav_DiMu;
  MuonTriggerMatching* TrigMatch_DiMu;
  HforToolD3PD hforTool;
  LeptonTriggerSF* LeptonSF;
  VertexPositionReweightingTool* VertexRew;
  Root::TTileTripReader *TripRead;
  Root::TGoodRunsListReader *GrlReader;
  Root::TGoodRunsList Grl;
  Root::TElectronIsEMSelector* ElId2011;
  Root::TElectronLikelihoodTool* ElId2012;
  JetAnalysisCalib::JetCalibrationTool* JetCalib;
  MissingMassCalculator* MMC;
  
 protected:
  YEARTYPE m_year;
  DATATYPE m_type;
  MCTYPE   m_mctype;
  int m_MuId;
  int m_ElId;
  int m_TaId;
  TTree *h4lTree;
  TFile *newfile;
  TH1F *info;
  TH1F *cutFlow_4mu;
  TH1F *cutFlow_4e;
  TH1F *cutFlow_2l2l;
  TH1F *ElecCount;
  TH1F *MuCBST;
  TH1F *MuCBSTSA;
  TH1F *MuCBSTSACA;
  TH1F *TauCount;
  TH1F *JetCount;
  TH1F *MuReco_pT;
  TH1F *MuReco_eta;
  TH1F *MuTruth_pT;
  TH1F *MuTruth_eta;
  TH1F *ElReco_eT;
  TH1F *ElReco_eta;
  TH1F *ElTruth_eT;
  TH1F *ElTruth_eta;
  TH1F *MuEff_pT; 
  TH1F *MuEff_eta;
  TH1F *ElEff_eT; 
  TH1F *ElEff_eta;
  TGraphAsymmErrors *MuEff_pT_Graph; 
  TGraphAsymmErrors *MuEff_eta_Graph;
  TGraphAsymmErrors *ElEff_eT_Graph; 
  TGraphAsymmErrors *ElEff_eta_Graph;
  std::ofstream Info;
  std::string m_outname;
  std::string m_infoFile;
  std::string m_DirPrefix;
  bool m_UseProof;
  /* Constructors Variables */
  bool m_Debug;
  bool m_TauRemove;
  std::string m_channel;
  std::string m_channum;
  bool m_UseMinVar;
  bool m_UseCorrections;
  bool m_UseWeights;
  bool m_ParMinInfo;
  bool m_NtupTau;
  double pi;
  double m_Zmass;
  double m_Zwidth;
  double m_MuMass;
  double m_ElMass;
  double m_TaMass;
  double m_TaMass1p;
  double m_TaMass3p;
  double GeV;
  double m_SelD0Cut;
  double m_pT1Cut;
  double m_pT2Cut;
  double m_pT3Cut;
  double m_pT4CutMu;
  double m_pT4CutEl;
  double m_ElMatchCut_2011_a;
  double m_ElMatchCut_2011_b;
  double m_ElMatchCut_2012_a;
  double m_ElMatchCut_2012_b;
  double m_DiElMatchCut_2011;
  double m_DiElMatchCut_2012;
  double m_MuMatchCut_2011;
  double m_MuMatchCut_2012_a;
  double m_MuMatchCut_2012_b;
  double m_DiMuMatchCut_2011;
  double m_DiMuMatchCut_2012_a;
  double m_DiMuMatchCut_2012_b;
  double m_DiMuMatchCut_2012_c;
  double m_Z1LowCut;
  double m_Z1HighCut;
  double m_FsrZ1Low;
  double m_FsrZ1High;
  double m_FsrZ1Thr;
  double m_DeltaRCutSF;
  double m_DeltaRCutOF;
  double m_JpsiCut;
  double m_TrkIsoCut;
  double m_CalIsoCutEl;
  double m_CalIsoCutMu;
  double m_CalIsoCutSAMu;
  double m_D0SignCutEl;
  double m_D0SignCutMu;
  /* electrons counter variable*/
  int nelectrons;
  int VertexElec;
  int TriggerElec;
  /* muons counter variable*/
  int nmuons;
  int VertexStaco;
  int VertexCalo;
  int TriggerStaco;
  int TriggerCalo;
  /* taus counter variables */
  int ntaus;
  int VertexTau;
  int TriggerTau;
  /* jets counter variables */
  int njets;
  int VertexJet;
  int TriggerJet;
  /* sample information counters */
  int Tprocessed;
  int TnegWeight;
  int TposWeight;
  /* Event Counter variables*/
  double LArEvt;
  double TileEvt;
  double BadEvt;
  double TilCorrEvt;
  double GRLEvt;
  double VertexEvt;
  double TriggerEvt;
  double LeptonsSelEvt_2l2l;
  double SFOSEvt_2l2l;
  double D0Evt_2l2l;
  double KinematicEvt_2l2l;
  double TrigMatchEvt_2l2l;
  double Z1MassEvt_2l2l;
  double Z2MassEvt_2l2l;
  double DeltaREvt_2l2l;
  double JPsiVetoEvt_2l2l;
  double BestQuadEvt_2l2l;
  double TrkIsoEvt_2l2l;
  double CaloIsoEvt_2l2l;
  double D0SignEvt_2l2l;
  double LeptonsSelEvt_4mu;
  double SFOSEvt_4mu;
  double D0Evt_4mu;
  double KinematicEvt_4mu;
  double TrigMatchEvt_4mu;
  double Z1MassEvt_4mu;
  double Z2MassEvt_4mu;
  double DeltaREvt_4mu;
  double JPsiVetoEvt_4mu;
  double BestQuadEvt_4mu;
  double TrkIsoEvt_4mu;
  double CaloIsoEvt_4mu;
  double D0SignEvt_4mu;
  double LeptonsSelEvt_4e;
  double SFOSEvt_4e;
  double D0Evt_4e;
  double KinematicEvt_4e;
  double TrigMatchEvt_4e;
  double Z1MassEvt_4e;
  double Z2MassEvt_4e;
  double DeltaREvt_4e;
  double JPsiVetoEvt_4e;
  double BestQuadEvt_4e;
  double TrkIsoEvt_4e;
  double CaloIsoEvt_4e;
  double D0SignEvt_4e;
  
  /* PileUpReweighting and Luminosity Fractions */
  std::string m_name_PU;
  std::string m_lumiCalcFile;
  int m_defChannel;
  int m_dataAction;
  Double_t m_periodA;
  Double_t m_periodB;
  Double_t m_periodC;
  Double_t m_periodD;
  Double_t m_periodE;
  Double_t m_periodF;
  Double_t m_periodG;
  Double_t m_periodH;
  Double_t m_periodI;
  Double_t m_periodJ;
  Double_t m_periodK;
  Double_t m_periodL;
  Double_t m_periodM;
  Double_t m_frac1;
  Double_t m_frac2;
  /* Electron Energy Rescaler */
  std::string m_EResInput;
  /* Electron ID */
  TString m_ElIdConf;
  std::string m_ElIdInput;
  /* Trigger Mathcing variables */
  bool m_match_tool;
  bool m_SingleLep_Match;
  bool m_DiLep_Match;
  bool m_ElMu_Match;
  /*GRL variable*/
  std::string m_GRLxml;
  /* MuonMomentumCorrections variables */
  std::string m_Data;
  std::string m_Algo;
  std::string m_Smear;
  std::string m_Rel;
  std::string m_SmDir;
  /* MuonMomentumUncertainty variables */
  std::string m_muResDir;
  std::string m_muResFile;
  /* MuonEfficiencyCorrections variables */  
  Analysis::AnalysisMuonConfigurableScaleFactors::Configuration m_confMu;
  Analysis::AnalysisMuonConfigurableScaleFactors::Configuration m_confMu_SA;
  std::string m_muType_staco;
  std::string m_muType_stacoSA;
  std::string m_muType_calo;
  std::string m_SfDir;
  /* ElectronEfficiencyCorrections */
  std::string m_ElEffDir; 
  std::string m_ElEffFileId;
  std::string m_ElEffFileReco;
  /* Electron CaloIsoCorrection */
  std::string m_LeakCorrFile;
  /* TriggerScaleFactors variables */
  int m_TrigYear;
  std::string m_TrigDir;
  std::string m_TrigFile;
  std::string m_TrigElDir;
  std::string m_TrigRecVr;
  /* Vertex Reweighting */
  std::string m_VertDir;
  /* Jet Calibration */
  std::string m_JetCalibConf;
  std::string m_JetAlgorithm;
  /* event variables*/
  int run;                     //Run Number
  int event;                   //Event Number
  double event_shRhoKt4EM;     //Event Shape Info EM
  double IntPerXing;           //Interactions per Xcrossing
  int lumiblock;               //Luminosity block
  int LArErr;                  //Liquid Argon error
  int TileErr;                 //Tile Error
  int nelecsel;                //number of electrons selected
  int nmusel;                  //number of muons selected
  int ntausel;                 //number of taus selected
  int njetsel;                 //number of jets selected
  int nvx;                     //Number of vertex's track 
  int nPV;                     //times nvx>=2
  int Period;                  //Data/MC run Period
  bool isInGRL;                //GoodRunList flag
  bool isLArError;             //Liquid Argor Error in event
  bool isTileError;            //Tile Error in event
  bool isBadEvent;             //Bad Event flag
  bool isTileCorrupted;        //Tile Corruption flag
  bool isTrigGood;             //Good Trig event
  bool isTrigEFelecGood;       //Good single elec Trig event
  bool isTrigEF2elecGood;      //Good di-elec Trig event
  bool isTrigEFmuGood;         //Good single muon Trig event
  bool isTrigEF2muGood;        //Good di-muon Trig event
  bool isTrigEFemuGood;        //Good elec-muon Trig event
  bool isVertexGood;           //Good Vertex event
  bool isGood4lCandidate;      //Good 4l candidate
  bool isGood4lSFOS;           //Good 4l SFOS candidate
  bool isGoodCandidate_4mu;    //Good 4mu candidate
  bool isGoodSFOS_4mu;         //Good 4mu SFOS candidate
  bool isGoodD0_4mu;           //Good 4mu D0 candidate
  bool isGoodKine_4mu;         //Good 4mu Kinematics candidate
  bool isGoodTrigMatch_4mu;    //Good 4mu Trigger Match candidate
  bool isGoodMz1_4mu;          //Good 4mu Z1 mass candidate
  bool isGoodMz2_4mu;          //Good 4mu Z2 mass candidate
  bool isGoodDR_4mu;           //Good 4mu DeltaR candidate
  bool isGoodJpsiVeto_4mu;     //Good 4mu J/Psi Veto candidate
  bool isBestQuadruplet_4mu;   //Best 4mu Quadruplet flag
  bool isGoodTrkIso_4mu;       //Good 4mu Track Isolation candidate
  bool isGoodCaloIso_4mu;      //Good 4mu Calo Isolation candidate
  bool isGoodD0Sign_4mu;       //Good 4mu D0 Significance candidate
  bool isGoodCandidate_4e;     //Good 4e candidate
  bool isGoodSFOS_4e;          //Good 4e SFOS candidate
  bool isGoodD0_4e;            //Good 4e D0 candidate
  bool isGoodKine_4e;          //Good 4e Kinematics candidate
  bool isGoodTrigMatch_4e;     //Good 4e Trigger Match candidate
  bool isGoodMz1_4e;           //Good 4e Z1 mass candidate
  bool isGoodMz2_4e;           //Good 4e Z2 mass candidate
  bool isGoodDR_4e;            //Good 4e DeltaR candidate
  bool isGoodJpsiVeto_4e;      //Good 4e J/Psi Veto candidate
  bool isBestQuadruplet_4e;    //Best 4e Quadruplet flag
  bool isGoodTrkIso_4e;        //Good 4e Track Isolation candidate
  bool isGoodCaloIso_4e;       //Good 4e Calo Isolation candidate
  bool isGoodD0Sign_4e;        //Good 4e D0 Significance candidate
  bool isGoodCandidate_2l2l;   //Good 2l2l candidate
  bool isGoodSFOS_2l2l;        //Good 2l2l SFOS candidate
  bool isGoodD0_2l2l;          //Good 2l2l D0 candidate
  bool isGoodKine_2l2l;        //Good 2l2l Kinematics candidate
  bool isGoodTrigMatch_2l2l;   //Good 2l2l Trigger Match candidate
  bool isGoodMz1_2l2l;         //Good 2l2l Z1 mass candidate
  bool isGoodMz2_2l2l;         //Good 2l2l Z2 mass candidate
  bool isGoodDR_2l2l;          //Good 2l2l DeltaR candidate
  bool isGoodJpsiVeto_2l2l;    //Good 2l2l J/Psi Veto candidate
  bool isBestQuadruplet_2l2l;  //Best 2l2l Quadruplet flag
  bool isGoodTrkIso_2l2l;      //Good 2l2l Track Isolation candidate
  bool isGoodCaloIso_2l2l;     //Good 2l2l Calo Isolation candidate
  bool isGoodD0Sign_2l2l;      //Good 2l2l D0 Significance candidate
  bool isAllPassed;            //Good Event: passed all cuts
  int  isAllPassedPos;         //Position of Final Good Quadruplet
  bool isMisPairing;           //Mispairing in the Z reconstruction
  bool isMisPairLabel;         //Mispairing and MisLabeling in the Z reconstruction
  bool isKeepEvent;            //Keep event after MC samples overlap
  int Channel;                 //channel information -> 2) 2e2mu, 3) 2mu2e
  float met_et;                //Missing energy Et
  float met_etx;               //Missing energy Etx
  float met_ety;               //Missing energy Ety
  float met_sumet;             //Missing energy sum
  float met_phi;               //Missing energy Phi
  float met_bdt_et;            //Missing energy BDT Et
  float met_bdt_etx;           //Missing energy BDT Etx
  float met_bdt_ety;           //Missing energy BDT Ety
  float met_bdt_sumet;         //Missing energy BDT sum
  float met_bdt_phi;           //Missing energy BDT Phi
  int hfor;                    //overlap MC event
  int MCchanNum;               //MC Channel Number
  double MCxsec;               //MC samples xsec
  double TrueMCxsec;           //MC samples xsec no aprox. (signal)
  double mcweight;             //mcweight of event
  double pileupweight;         //pileup reweighting
  double vxtweight;            //vertex reweighting
  /* particles properties: vectors */
  std::vector<int>    Lept_Index;         //lepton index
  std::vector<int>    Lept_Id;            //lepton id
  std::vector<int>    Lept_Author;        //lepton author
  std::vector<int>    Lept_charge;        //lepton charge
  std::vector<double> Lept_d0_unbias;     //lepton d0 unbiased
  std::vector<double> Lept_scf;           //lepton scale factor
  std::vector<double> Lept_resoscf;       //lepton resolution scale factor
  std::vector<double> Lept_z0_unbias;     //lepton z0 unbiased
  std::vector<double> Lept_E;             //lepton E
  std::vector<double> Lept_pt;            //lepton pt
  std::vector<double> Lept_eta;           //lepton eta
  std::vector<double> Lept_phi;           //lepton phi
  std::vector<double> Lept_Ereso;         //lepton E resolution
  std::vector<int>    Lept_SAgood;        //is good SA lepton
  std::vector<int>    Lept_CALOgood;      //is good CALO lepton
  std::vector<int>    Lept_numTrack;      //num. hadrons tracks
  
  /* electrons properties: vectors */
  std::vector<int>    Elec_Index;              //electron index
  std::vector<int>    Elec_Id;                 //electron id
  std::vector<int>    Elec_Author;             //electron author
  std::vector<int>    Elec_charge;             //electron charge
  std::vector<double> Elec_d0_unbias;          //electron d0 unbiased
  std::vector<double> Elec_scf;                //electron efficiency scale factor
  std::vector<double> Elec_z0_unbias;          //electron z0 unbiased
  std::vector<double> Elec_E;                  //electron E
  std::vector<double> Elec_pt;                 //electron pt
  std::vector<double> Elec_eta;                //electron eta
  std::vector<double> Elec_phi;                //electron phi
  std::vector<double> Elec_Ereso;              //electron energy resolution
  
  std::vector<double> Elec_d0;                 //electron d0 (trkd0)
  std::vector<double> Elec_sigd0_unbias;       //electron sigd0 unbiased
  std::vector<double> Elec_z0;                 //electron z0
  std::vector<double> Elec_Eclraw;             //electron E cluster raw
  std::vector<double> Elec_p;                  //electron p
  std::vector<double> Elec_px;                 //electron px
  std::vector<double> Elec_py;                 //electron py
  std::vector<double> Elec_pz;                 //electron pz
  std::vector<double> Elec_pid;                //electron pid
  std::vector<double> Elec_trkpt;              //electron trkpt
  std::vector<double> Elec_Et;                 //electron Et
  std::vector<double> Elec_theta;              //electron theta 
  std::vector<double> Elec_etaS2;              //electron eta in cluster
  std::vector<double> Elec_etacl;              //electron eta in cluster
  std::vector<double> Elec_etap;               //electron eta in cluster (topo)
  std::vector<double> Elec_phiS2;              //electron phi in cluster (S2)
  std::vector<double> Elec_phicl;              //electron phi in cluster
  std::vector<double> Elec_isocalo20;          //electron calo isol. cone 20
  std::vector<double> Elec_isocalo30;          //electron calo isol. cone 30
  std::vector<double> Elec_isocalo40;          //electron calo isol. cone 40
  std::vector<double> Elec_topoisocalo20;      //electron calo isol. cone 20 (topo)
  std::vector<double> Elec_topoisocalo30;      //electron calo isol. cone 30 (topo)
  std::vector<double> Elec_topoisocalo40;      //electron calo isol. cone 40 (topo)
  std::vector<double> Elec_isotrk20;           //electron track isol. cone 20 
  std::vector<double> Elec_isotrk30;           //electron track isol. cone 30 
  std::vector<double> Elec_isotrk40;           //electron track isol. cone 40
  std::vector<double> Elec_EDmedian;           //electron ED median
  std::vector<double> Elec_pt_truth;           //electron pt (truth)
  std::vector<double> Elec_eta_truth;          //electron eta (truth)
  std::vector<double> Elec_phi_truth;          //electron phi (truth)
  std::vector<double> Elec_E_truth;            //electron E (truth)
  std::vector<int>    Elec_mother_truth;       //electron mother (truth)
  std::vector<int>    Elec_motherbcode_truth;  //electron mother barcode (truth)
  std::vector<double> Elec_cov_d0;             //electron cov. matrix d0       
  std::vector<double> Elec_cov_z0;             //electron cov. matrix z0       
  std::vector<double> Elec_cov_phi;            //electron cov. matrix phi      
  std::vector<double> Elec_cov_theta;          //electron cov. matrix theta    
  std::vector<double> Elec_cov_d0_z0;          //electron cov. matrix d0-z0    
  std::vector<double> Elec_cov_d0_phi;         //electron cov. matrix d0-phi   
  std::vector<double> Elec_cov_d0_theta;       //electron cov. matrix d0-theta 
  std::vector<double> Elec_cov_z0_phi;         //electron cov. matrix z0-phi   
  std::vector<double> Elec_cov_z0_theta;       //electron cov. matrix z0-theta 
  std::vector<double> Elec_cov_phi_theta;      //electron cov. matrix phi-theta
  
  /* muons proprierties: vectors */
  std::vector<int>    Muon_Index;              //muon index
  std::vector<int>    Muon_Id;                 //muon id
  std::vector<int>    Muon_Author;             //muon author
  std::vector<int>    Muon_charge;             //muon charge
  std::vector<double> Muon_d0_unbias;          //muon d0 unbiased
  std::vector<double> Muon_scf;                //muon scale factor
  std::vector<double> Muon_resoscf;            //muon resolution scale factor
  std::vector<double> Muon_z0_unbias;          //muon z0 unbiased
  std::vector<double> Muon_pt;                 //muon pt
  std::vector<double> Muon_eta;                //muon eta
  std::vector<double> Muon_phi;                //muon phi
  std::vector<int>    Muon_isSAmu;             //isSAmuon
  std::vector<int>    Muon_isSTmu;             //isSTmuon
  std::vector<int>    Muon_BLYgood;            //is good Blayer muon
  std::vector<int>    Muon_PXLgood;            //is good Pixel muon
  std::vector<int>    Muon_SCTgood;            //is good SCT muon
  std::vector<int>    Muon_HOLgood;            //is good holes muon
  std::vector<int>    Muon_TRTgood;            //is good TRT muon
  std::vector<int>    Muon_D0good;             //is good D0 muon
  std::vector<int>    Muon_Z0good;             //is good Z0 muon
  std::vector<int>    Muon_MCPgood;            //is good MCP muon
  std::vector<int>    Muon_MSgood;             //is good MS muon
  //to remove ---
  std::vector<int>    Muon_OurMSgood;          //is good MS muon (our definition)
  //----
  std::vector<int>    Muon_CBTAGgood;          //is good CB or Tag muon
  std::vector<int>    Muon_SAgood;             //is good SA muon
  std::vector<int>    Muon_CALOgood;           //is good CALO muon
  std::vector<int>    Muon_n_exblh;            //expect Blayer hit
  std::vector<int>    Muon_n_blh;              //Blayer hits
  std::vector<int>    Muon_n_pxlh;             //pixel hits
  std::vector<int>    Muon_n_pxlds;            //pixel dead sensors
  std::vector<int>    Muon_n_pxlhol;           //pixel holes
  std::vector<int>    Muon_n_scth;             //sct hits
  std::vector<int>    Muon_n_sctds;            //sct dead sensors
  std::vector<int>    Muon_n_scthol;           //sct holes
  std::vector<int>    Muon_n_hol;              //pixel+sct holes
  std::vector<int>    Muon_n_trth;             //trt hits
  std::vector<int>    Muon_n_trto;             //trt out layers
  std::vector<int>    Muon_n_trtho;            //trt hits+outlayers
  std::vector<int>    Muon_n_mdth;             //mdt hits
  std::vector<int>    Muon_n_mdtBIh;           //mdt BI hits
  std::vector<int>    Muon_n_mdtBMh;           //mdt BM hits
  std::vector<int>    Muon_n_mdtBOh;           //mdt BO hits
  std::vector<int>    Muon_n_mdtBEEh;          //mdt BEE hits
  std::vector<int>    Muon_n_mdtBIS78h;        //mdt BIS78 hits
  std::vector<int>    Muon_n_mdtEIh;           //mdt EI hits
  std::vector<int>    Muon_n_mdtEMh;           //mdt EM hits
  std::vector<int>    Muon_n_mdtEOh;           //mdt EO hits
  std::vector<int>    Muon_n_mdtEEh;           //mdt EE hits
  std::vector<int>    Muon_n_cscEtah;          //cscEta hits
  std::vector<int>    Muon_n_cscPhih;          //cscPhi hits
  std::vector<int>    Muon_n_rpcEtah;          //rpc Eta hits
  std::vector<int>    Muon_n_rpcPhih;          //prc Phi hits
  std::vector<int>    Muon_n_tgcEtah;          //tgc Eta hits 
  std::vector<int>    Muon_n_tgcPhih;          //tgc Phi hits
  std::vector<double> Muon_d0;                 //muon d0 (exPV)
  std::vector<double> Muon_errd0;              //muon d0 error (exPV)
  std::vector<double> Muon_sigd0_unbias;       //muon sigd0 unbiased
  std::vector<double> Muon_z0;                 //muon z0 (exPV)
  std::vector<double> Muon_p;                  //muon p
  std::vector<double> Muon_px;                 //muon px
  std::vector<double> Muon_py;                 //muon py
  std::vector<double> Muon_pz;                 //muon pz
  std::vector<double> Muon_pid;                //muon pid
  std::vector<double> Muon_E;                  //muon pt
  std::vector<double> Muon_theta;              //muon theta 
  std::vector<double> Muon_theta_id;           //muon theta on ID
  std::vector<double> Muon_eta_id;             //muon eta on ID
  std::vector<double> Muon_phi_id;             //muon phi on ID
  std::vector<double> Muon_isocalo20;          //muon calo isol. cone 20
  std::vector<double> Muon_isocalo30;          //muon calo isol. cone 30
  std::vector<double> Muon_isocalo40;          //muon calo isol. cone 40
  std::vector<double> Muon_isotrk20;           //muon track isol. cone 20 
  std::vector<double> Muon_isotrk30;           //muon track isol. cone 30 
  std::vector<double> Muon_isotrk40;           //muon track isol. cone 40 
  std::vector<double> Muon_pt_truth;           //muon pt (truth)
  std::vector<double> Muon_eta_truth;          //muon eta (truth)
  std::vector<double> Muon_phi_truth;          //muon phi (truth)
  std::vector<double> Muon_E_truth;            //muon E (truth)
  std::vector<int>    Muon_mother_truth;       //muon mother (truth)
  std::vector<int>    Muon_motherbcode_truth;  //muon mother barcode(truth)
  std::vector<double> Muon_cov_d0;             //muon cov. matrix d0
  std::vector<double> Muon_cov_z0;	       //muon cov. matrix z0
  std::vector<double> Muon_cov_phi;	       //muon cov. matrix phi
  std::vector<double> Muon_cov_theta;	       //muon cov. matrix theta    
  std::vector<double> Muon_cov_qoverp;	       //muon cov. matrix qoverp
  std::vector<double> Muon_cov_d0_z0;	       //muon cov. matrix d0-z0   
  std::vector<double> Muon_cov_d0_phi;	       //muon cov. matrix d0-phi
  std::vector<double> Muon_cov_d0_theta;       //muon cov. matrix d0-theta
  std::vector<double> Muon_cov_d0_qoverp;      //muon cov. matrix d0-qoverp
  std::vector<double> Muon_cov_z0_phi;	       //muon cov. matrix z0-phi
  std::vector<double> Muon_cov_z0_theta;       //muon cov. matrix z0-theta
  std::vector<double> Muon_cov_z0_qoverp;      //muon cov. matrix z0-qoverp
  std::vector<double> Muon_cov_phi_theta;      //muon cov. matrix phi-theta
  std::vector<double> Muon_cov_phi_qoverp;     //muon cov. matrix phi-qoverp
  std::vector<double> Muon_cov_theta_qoverp;   //muon cov. matrix theta-qoverp
  
  /* jets proprierties: vectors */
  std::vector<int>    Jet_Index;               //jet index
  std::vector<double> Jet_pt;                  //jet pt
  std::vector<double> Jet_eta;                 //jet eta
  std::vector<double> Jet_phi;                 //jet phi
  std::vector<double> Jet_E;                   //jet E
  std::vector<double> Jet_m;                   //jet m
  std::vector<double> Jet_jvtx;                //jet vertex fraction

  /* photons proprierties: vectors */
  std::vector<double> Photon_eta;
  std::vector<double> Photon_phi;
  std::vector<double> Photon_E;
  std::vector<double> Photon_Et;
  std::vector<int>    Photon_author;
  std::vector<double> Photon_f1;

  /* taus proprierties: vectors */
  std::vector<int>             Tau_Index;
  std::vector<int>             Tau_Id;
  std::vector<int>             Tau_Author;
  std::vector<double>          Tau_Et;
  std::vector<double>          Tau_pt;
  std::vector<double>          Tau_m;
  std::vector<double>          Tau_eta;
  std::vector<double>          Tau_phi;
  std::vector<int>             Tau_charge;
  std::vector<double>          Tau_likelihood;
  std::vector<double>          Tau_SafeLikelihood;
  std::vector<double>          Tau_electronVetoLoose;
  std::vector<double>          Tau_electronVetoMedium;
  std::vector<double>          Tau_electronVetoTight;
  std::vector<double>          Tau_muonVeto;
  std::vector<double>          Tau_tauLlhLoose;
  std::vector<double>          Tau_tauLlhMedium;
  std::vector<double>          Tau_tauLlhTight;
  std::vector<double>          Tau_JetBDTSigLoose;
  std::vector<double>          Tau_JetBDTSigMedium;
  std::vector<double>          Tau_JetBDTSigTight;
  std::vector<double>          Tau_EleBDTLoose;
  std::vector<double>          Tau_EleBDTMedium;
  std::vector<double>          Tau_EleBDTTight;
  std::vector<int>             Tau_numTrack;
  std::vector<double>          Tau_BDTJetScore;
  std::vector<double>          Tau_BDTEleScore;
  std::vector<int>             Tau_isTruthMatchTau;
  std::vector<int>             Tau_isTruthMatchEl;
  std::vector<int>             Tau_isTruthMatchMu;
  std::vector<vector<int> >    Tau_TruthMatchType;
  std::vector<vector<int> >    Tau_TruthMatchPos;
  std::vector<vector<double> > Tau_TruthMatchDr;

  /* 4-Leptons variables */
  std::vector<std::vector<int> >    Quadruplets;       //quadruplets
  std::vector<int>                  QuadSubChan;       //quadruplet sub-channel
  std::vector<int>                  IsD0Good;          //quadruplet d0 cut passed
  std::vector<int>                  IsKinematicGood;   //quadruplet kinematics cut passed
  std::vector<int>                  IsTrigMatchGood;   //quadruplet trigger match (cut passed)
  std::vector<int>                  IsSingleLepMatch;  //quadruplet single lep trigger match flag
  std::vector<int>                  IsDiLepMatch;      //quadruplet di-lep trigger match flag
  std::vector<double>               TrigScaleFact;     //quadruplet trigger scale factor
  std::vector<double>               EffyScaleFact;     //quadruplet efficiency scale factor
  std::vector<double>               Z1Mass;            //M12 di-lepton masses
  std::vector<double>               Z2Mass;            //M34 di-lepton masses
  std::vector<double>               Z1MMCMass;         //M12 di-lepton MMC masses
  std::vector<double>               Z2MMCMass;         //M34 di-lepton MMC masses
  std::vector<double>               FourLepMass;       //quadruplet masses
  std::vector<double>               FourLepPt;         //quadruplet pT
  std::vector<double>               FourLepEta;        //quadruplet eta
  std::vector<double>               FourLepPhi;        //quadruplet phi
  std::vector<int>                  IsZ1MassGood;      //quadruplet good Z1 mass (cut passed)
  std::vector<int>                  IsZ2MassGood;      //quadruplet good Z2 mass (cut passed)
  std::vector<int>                  IsBestQuadruplet;  //Best quadruplet flag
  std::vector<int>                  IsDeltaRGood;      //quadruplet good DeltaR (cut passed)
  std::vector<int>                  IsJpsiVetoGood;    //quadruplet good J/Psi veto (cut passed)
  std::vector<std::vector<double> > IsoTrk20_corr;     //quadruplet Trk Iso corrections (cone 20)
  std::vector<std::vector<double> > IsoTrk30_corr;     //quadruplet Trk Iso corrections (cone 30)
  std::vector<std::vector<double> > IsoTrk40_corr;     //quadruplet Trk Iso corrections (cone 40)
  std::vector<int>                  IsTrackIsoGood;    //quadruplet good Track Iso (cut passed)
  std::vector<std::vector<double> > IsoCalo20_corr;    //quadruplet Calo Iso corrections (cone 20)
  std::vector<std::vector<double> > IsoCalo30_corr;    //quadruplet Calo Iso corrections (cone 30)
  std::vector<std::vector<double> > IsoCalo40_corr;    //quadruplet Calo Iso corrections (cone 40)
  std::vector<int>                  IsCaloIsoGood;     //quadruplet good Calo Iso (cut passed)
  std::vector<int>                  IsD0SignGood;      //quadruplet D0 Sign. (cut passed)
  std::vector<double>               IsFsrCand;         //quadruplet fsr correction applied
  std::vector<double>               FsrCandEt;         //quadruplet fsr: photon Et
  std::vector<double>               FsrCandEta;        //quadruplet fsr: photon eta
  std::vector<double>               FsrCandPhi;        //quadruplet fsr: photon phi
  std::vector<double>               FsrZ1Mass;         //quadruplet fsr: new Z1 mass
  std::vector<double>               FsrFourLepMass;    //quadruplet fsr: new 4l mass
  std::vector<double>               ConstrZ1Mass;      //quadruplet constrained Z1 masses
  std::vector<double>               ConstrZ2Mass;      //quadruplet constrained Z2 masses
  std::vector<double>               ConstrFourLepMass; //quadruplet constrained 4 lepton masses
  std::vector<double>               DeltaPhiLab;       //quandruplet deltaphi in lab
  std::vector<double>               Acoplanarity;      //quandruplet acoplanarity
  std::vector<double>               CosThetaStar;      //quandruplet costhetastar
  std::vector<double>               ThetaStar;	       //quandruplet thetastar
  std::vector<double>               DeltaPhi;	       //quandruplet deltaphi
  std::vector<double>               DeltaPhi1;         //quandruplet deltaphi1
  std::vector<double>               CosTheta1; 	       //quandruplet costheta1
  std::vector<double>               CosTheta2;         //quandruplet costheta2
  std::vector<int>                  QuadMisPairing;    //quadruplet good pairing
  std::vector<int>                  QuadMisPairLabel;  //quadruplet good pairing and labeling
  
  /*Truth Info*/
  double Z1mass_truth;
  double Z2mass_truth;
  int Z1_bcode_truth;
  int Z2_bcode_truth;
  double Hmass_truth;
  double HpT_truth;
  double Heta_truth;
  double DeltaPhiLab_truth;
  double Acoplanarity_truth;
  double CosThetaStar_truth;
  double ThetaStar_truth;
  double DeltaPhi_truth;
  double DeltaPhi1_truth;
  double CosTheta1_truth;
  double CosTheta2_truth;
  std::vector<double> LepZ1Pt_truth;
  std::vector<double> LepZ2Pt_truth;
  std::vector<double> LepZ1Eta_truth;
  std::vector<double> LepZ2Eta_truth;
  std::vector<double> LepZ1Phi_truth;
  std::vector<double> LepZ2Phi_truth;
  std::vector<double> LepZ1M_truth;
  std::vector<double> LepZ2M_truth;
  std::vector<double> LepZ1PdgId_truth;
  std::vector<double> LepZ2PdgId_truth;
  
  /*Internal methods variables*/
  static TRandom3* m_rnd;
  bool m_smu1;
  bool m_smu2;
  bool m_dmu1;
  bool m_dmu2;
  std::vector<std::string> m_Elchain;
  std::vector<double>      m_ElThreshold;
  std::vector<std::string> m_Muchain;
  std::vector<double>      m_MuThreshold;
  
  std::vector<std::string> m_DiElchain;
  std::vector<double>      m_DiElThreshold;
  std::vector<std::string> m_DiMuchain;
  std::vector<double>      m_DiMuThreshold;
  
  std::vector<std::string> m_ElMuchain;
  
  std::vector<double> m_DmVal;
  std::vector<double> m_tmpiso20;
  std::vector<double> m_tmpiso30;
  std::vector<double> m_tmpiso40;
  std::vector<double> m_trkiso;
  std::vector<double> m_caloiso;
  std::vector<double> m_elcaloiso;
  
  std::vector< std::pair<int,int> > m_Pairs;
  std::vector< std::pair<int,int> > m_ElectronPairs;
  std::vector< std::pair<int,int> > m_MuonPairs;
  std::vector< std::pair<int,int> > m_LeptonPairs;
  std::vector<std::vector<int> > m_Quadruplets;
  std::vector<int>    tmpQuadruplet;
  std::vector<int>    m_Quadruplet;
  std::vector<int>    m_IsD0Good;
  std::vector<int>    m_IsKineGood;
  std::vector<double> m_MassZ1;
  std::vector<double> m_MassZ2;
  std::vector<double> m_Mass4l;
  std::vector<int>    m_MassZ1Cut;
  std::vector<int>    m_MassZ2Cut;
  std::vector<int>    m_DeltaR;
  std::vector<int>    m_JpsiVeto;
  std::vector<int>    m_BestQuadruplet;
  std::vector<int>    m_TrackIso;
  std::vector<int>    m_CaloIso;
  std::vector<int>    m_D0Sign;
  std::vector<double> m_TrigScf;
  std::vector<double> m_EffyScf;
  std::vector<int>    m_IsGoodPair;
  std::vector<int>    m_IsGoodPairLabel;
  std::vector<int>    m_typematch;
  std::vector<int>    m_posmatch;
  std::vector<double> m_drmatch;
   
};
#endif // #ifdef h4l_h
