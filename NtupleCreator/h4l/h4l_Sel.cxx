#include "h4l.h"

std::vector< std::vector<int> > h4l::SelectQuadruplets(){

  Print("H4lNtupleCreator:: Selecting Quadruplets ...");
  
  m_Quadruplets.clear();
  m_LeptonPairs.clear();
  m_LeptonPairs = SelectDiLeptonPairs();

  if( m_LeptonPairs.size()<2 ) { return m_Quadruplets; }
  
  for(UInt_t p1=0; p1<m_LeptonPairs.size(); p1++){
    int l1=m_LeptonPairs.at(p1).first;
    int l2=m_LeptonPairs.at(p1).second;
    
    for(UInt_t p2=0; p2<m_LeptonPairs.size(); p2++){
      if(p2==p1) continue;
      m_Quadruplet.clear();
      int l3=m_LeptonPairs.at(p2).first;
      int l4=m_LeptonPairs.at(p2).second;
      
      PrintOut("                           Leptons (pos.):");
      PrintOut(" l1 = "); PrintOut(l1); PrintOut(" [id = "); 
      PrintOut(Lept_Id.at(l1)); PrintOut(" ]"); 
      PrintOut(" l2 = "); PrintOut(l2); PrintOut(" [id = "); 
      PrintOut(Lept_Id.at(l2)); PrintOut(" ]");
      PrintOut(" l3 = "); PrintOut(l3); PrintOut(" [id = "); 
      PrintOut(Lept_Id.at(l3)); PrintOut(" ]"); 
      PrintOut(" l4 = "); PrintOut(l4); PrintOut(" [id = "); 
      PrintOut(Lept_Id.at(l4)); PrintOut(" ]"); 
      PrintEndl();

      bool NoSameLep=false;
      int subchan=Detect4LepSubChan(Lept_Id.at(l1),Lept_Id.at(l2),
				    Lept_Id.at(l3),Lept_Id.at(l4));

      if( subchan==0 || subchan==1 || subchan==4){
	if( l1!=l3 && l1!=l4 && l2!=l3 && l2!=l4 ){ NoSameLep=true; }
      }
      else{ NoSameLep=true; }
      
      if(NoSameLep){
	
	int SAcheck   = ( Lept_SAgood.at(l1)+Lept_SAgood.at(l2)+
			  Lept_SAgood.at(l3)+Lept_SAgood.at(l4) );
	int CALOcheck =( Lept_CALOgood.at(l1)+Lept_CALOgood.at(l2)+
			 Lept_CALOgood.at(l3)+Lept_CALOgood.at(l4) );
	
	if( (SAcheck<2&&CALOcheck==0) || (CALOcheck<2&&SAcheck==0) ){
	  
	  PrintOut("                        Good Quadruplet Found: -> ");
	  PrintOut("["); PrintOut(l1); PrintOut(","); 
	  PrintOut(Lept_Id.at(l1)); PrintOut("] , ["); PrintOut(l2); 
	  PrintOut(","); PrintOut(Lept_Id.at(l2)); PrintOut("] , [");
	  PrintOut(l3); PrintOut(","); PrintOut(Lept_Id.at(l3)); 
	  PrintOut("] , ["); PrintOut(l4); PrintOut(",");
	  PrintOut(Lept_Id.at(l3)); PrintOut("]"); PrintEndl();
	  
	  m_Quadruplet.push_back(l1);
	  m_Quadruplet.push_back(l2);
	  m_Quadruplet.push_back(l3);
	  m_Quadruplet.push_back(l4);
	  QuadSubChan.push_back(subchan);
	  m_Quadruplets.push_back(m_Quadruplet);
	}
      }
    }
  }
  PrintOut("                     Number of Quadruplets: ");
  PrintOut((int)m_Quadruplets.size()); PrintEndl();
  
  return m_Quadruplets;
}

std::vector< std::pair<int,int> > h4l::SelectDiLeptonPairs(){
  
  Print("H4lNtupleCreator:: Selecting Di-Lepton Pairs ...");
  
  m_Pairs.clear();
  std::pair<int,int> tmpPair;
  for(UInt_t p1=0; p1<Lept_Index.size()-1; p1++){
    
    for(UInt_t p2=p1+1; p2<Lept_Index.size(); p2++){
            
      if( abs(Lept_Id.at(p1)*Lept_Id.at(p2))==(m_MuId*m_ElId) ||
	  abs(Lept_Id.at(p1)*Lept_Id.at(p2))==(m_MuId*m_TaId) ||
	  abs(Lept_Id.at(p1)*Lept_Id.at(p2))==(m_ElId*m_TaId)  )
	continue;

      if( Lept_charge.at(p1)*Lept_charge.at(p2)<0 ){
	tmpPair=std::make_pair(p1,p2);
	m_Pairs.push_back(tmpPair);
	
	PrintOut("                           Good Di-Lepton Pair Found: "); 
	PrintOut(" pos. lep1 = "); PrintOut(tmpPair.first);
	PrintOut(" [ch = "); PrintOut(Lept_charge.at(p1)); 
	PrintOut(" , Id = "); PrintOut(Lept_Id.at(p1)); PrintOut("]"); 
	PrintOut(" pos. lep2 = "); PrintOut(tmpPair.second);
	PrintOut(" [ch = "); PrintOut(Lept_charge.at(p2)); 
	PrintOut(" , Id = "); PrintOut(Lept_Id.at(p2)); PrintOut("]"); 
	PrintEndl();
      }
    }
  }
  PrintOut("                           Number of Di-Lepton Pairs: ");
  PrintOut((int)m_Pairs.size()); PrintEndl();
  return m_Pairs;

}


std::vector<double> h4l::ComputeTriggerScaleFactor(LeptonTriggerSF* leptonSF){
  
  Print("H4lNtupleCreator:: Computing Trigger Scale Factor ... ");

  m_TrigScf.clear();
  
  muon_quality muQual = loose; 
  electron_quality elQual = loosepp;
  if(m_year==YEAR_2011){
    elQual = loosepp;
  }
  else if(m_year==YEAR_2012){
    elQual = ML;
  }
  std::vector<TLorentzVector> Mu_tlv, El_tlv;
  for(UInt_t q=0; q<Quadruplets.size(); q++){
    
    if(m_UseWeights && QuadSubChan.at(q)<3){ //don't compute for quad with taus
      
      if(isTrigEFelecGood || isTrigEFmuGood){
	UInt_t idx1 = Quadruplets.at(q).at(0);
	UInt_t idx2 = Quadruplets.at(q).at(1);
	UInt_t idx3 = Quadruplets.at(q).at(2);
	UInt_t idx4 = Quadruplets.at(q).at(3);
	Mu_tlv.clear(); 
	El_tlv.clear();
	TLorentzVector lep1,lep2,lep3,lep4;
	lep1=GetLepLorentzVec(idx1);
	lep2=GetLepLorentzVec(idx2);
	lep3=GetLepLorentzVec(idx3);
	lep4=GetLepLorentzVec(idx4);
	if( abs(Lept_Id.at(idx1))==m_MuId ){ Mu_tlv.push_back(lep1); }
	else{ El_tlv.push_back(lep1); }
	if( abs(Lept_Id.at(idx2))==m_MuId ){ Mu_tlv.push_back(lep2); }
	else{ El_tlv.push_back(lep2); }
	if( abs(Lept_Id.at(idx3))==m_MuId ){ Mu_tlv.push_back(lep3); }
	else{ El_tlv.push_back(lep3); }
	if( abs(Lept_Id.at(idx4))==m_MuId ){ Mu_tlv.push_back(lep4); }
	else{ El_tlv.push_back(lep4); }	
	
	PileUpTool->SetRandomSeed(314159+mc_channel_number*2718+EventNumber);
	UInt_t RunNumSF = PileUpTool->GetRandomRunNumber(RunNumber);
	std::pair<double,double> SF = leptonSF->GetTriggerSF(RunNumSF,
							     true,
							     Mu_tlv,
							     muQual,
							     El_tlv,
							     elQual,
							     0);
	m_TrigScf.push_back( SF.first );
      }
      else{
	m_TrigScf.push_back(1.);
      }
    }
    else{
      m_TrigScf.push_back(1.);
    }
    PrintOut("                        Quadruplet: ");
    PrintOut((int)q); PrintEndl();
    PrintOut("                           Trig. Scf = ");
    PrintOut(m_TrigScf.at(q)); PrintEndl();
  }
  return m_TrigScf;
  
}

std::vector<double> h4l::ComputeEfficiencyScaleFactor(){
  
  Print("H4lNtupleCreator:: Computing Efficiency Scale Factor ... ");
  
  m_EffyScf.clear();
  for(UInt_t q=0; q<Quadruplets.size(); q++){
    
    UInt_t idx1 = Quadruplets.at(q).at(0);
    UInt_t idx2 = Quadruplets.at(q).at(1);
    UInt_t idx3 = Quadruplets.at(q).at(2);
    UInt_t idx4 = Quadruplets.at(q).at(3);
    
    double effyScf= ( Lept_scf.at(idx1)*Lept_scf.at(idx2)*
		      Lept_scf.at(idx3)*Lept_scf.at(idx4) );
    
    m_EffyScf.push_back( effyScf );
    PrintOut("                        Quadruplet: ");
    PrintOut((int)q); PrintEndl();
    PrintOut("                           Effy. Scf = ");
    PrintOut(m_EffyScf.at(q)); PrintEndl();
  }
  return m_EffyScf;
  
}

std::vector<int> h4l::CheckD0Cut(){
  
  Print("H4lNtupleCreator:: Checking D0Cut ... ");
  
  m_IsD0Good.clear();
  bool d0good=false;
  for(UInt_t q=0; q<Quadruplets.size(); q++){
    d0good=false;
    int l1=Quadruplets.at(q).at(0);
    int l2=Quadruplets.at(q).at(1);
    int l3=Quadruplets.at(q).at(2);
    int l4=Quadruplets.at(q).at(3);
    
    if( LeptonD0(l1) && LeptonD0(l2) &&
	LeptonD0(l3) && LeptonD0(l4)   ){
      d0good=true;
    }
    
    m_IsD0Good.push_back(d0good);
    PrintOut("                        Quadruplet: ");
    PrintOut((int)q); PrintEndl();
    PrintOut("                           D0 Good: ");
    PrintOut(m_IsD0Good.at(q)); PrintEndl();
  }
  return m_IsD0Good;
  
}

bool h4l::LeptonD0(int idl){
  
  if( abs(Lept_Id.at(idl))==m_MuId ){
    
    if( Lept_SAgood.at(idl)==1                  || 
	fabs(Lept_d0_unbias.at(idl))<m_SelD0Cut   ){
      return true;
    }
  } 
  else{ 
    return true;
  }
  
  return false;
  
}

std::vector<int> h4l::CheckKinematic(){

  Print("H4lNtupleCreator:: Checking Leptons Kinematics ...");
  
  m_IsKineGood.clear();
  bool pT1=false, pT2=false, pT3=false, pT4=false;
  UInt_t pT1_idx=0, pT2_idx=0, pT3_idx=0, pT4_idx=0;
  double pT4Cut=0;
  
  for(UInt_t q=0; q<Quadruplets.size(); q++){
    pT1=false, pT2=false, pT3=false, pT4=false;
    pT1_idx=-1, pT2_idx=-1, pT3_idx=-1, pT4_idx=-1;

    for(UInt_t l=0; l<Quadruplets.at(q).size(); l++){
      if(Lept_pt.at(Quadruplets.at(q).at(l))>m_pT1Cut){
	pT1=true;
	pT1_idx=l;
	break;
      }
    }
    
    for(UInt_t l=0; l<Quadruplets.at(q).size(); l++){
      if(Lept_pt.at(Quadruplets.at(q).at(l))>m_pT2Cut &&
	 l!=pT1_idx){
	pT2=true;
	pT2_idx=l;
	break;
      }
    }
    
    for(UInt_t l=0; l<Quadruplets.at(q).size(); l++){
      if(Lept_pt.at(Quadruplets.at(q).at(l))>m_pT3Cut &&
	 l!=pT1_idx && l!=pT2_idx){
	pT3=true;
	pT3_idx=l;
	break;
      }
    }
    
    for(UInt_t l=0; l<Quadruplets.at(q).size(); l++){
      
      if(abs(Lept_Id.at(Quadruplets.at(q).at(l)))==m_MuId){ 
	pT4Cut=m_pT4CutMu; 
      }
      else if(abs(Lept_Id.at(Quadruplets.at(q).at(l)))==m_ElId){ 
	pT4Cut=m_pT4CutEl; 
      }
      else if(abs(Lept_Id.at(Quadruplets.at(q).at(l)))==m_TaId){
	pT4Cut=m_pT4CutMu; //maybe choose one for tau?
      }
      
      if(Lept_pt.at(Quadruplets.at(q).at(l))>pT4Cut &&
	 l!=pT1_idx && l!=pT2_idx && l!=pT3_idx){
	pT4=true;
	pT4_idx=l;
	break;
      }
    }
    
    if(pT1 && pT2 && pT3 && pT4){
      m_IsKineGood.push_back(true);
    }
    else{
      m_IsKineGood.push_back(false);
    }
    PrintOut("                           Quadruplet: "); 
    PrintOut((int)q); PrintEndl();
    PrintOut("                               Kinematics Good: "); 
    PrintOut(m_IsKineGood.at(q)); 
    PrintOut(" Leptons Indices in Quadruplet: ");
    PrintOut((int)pT1_idx); PrintOut(","); 
    PrintOut((int)pT2_idx); PrintOut(","); 
    PrintOut((int)pT3_idx); PrintOut(","); 
    PrintOut((int)pT4_idx); PrintEndl();
  }
  return m_IsKineGood; 
}

double h4l::DiLeptonMass(int l1, 
			 int l2){
  
  double m = 0;
  TLorentzVector Lep1, Lep2;
  Lep1 = GetLepLorentzVec(l1);
  Lep2 = GetLepLorentzVec(l2);
  m = (Lep1+Lep2).M();
  return m;
  
}

double h4l::DiLeptonMMCMass(int l1,
			    int l2){
  
  double m = 0;
  TLorentzVector Lep1, Lep2;
  Lep1 = GetLepLorentzVec(l1,true);
  Lep2 = GetLepLorentzVec(l2,true);
  if( abs(Lept_Id.at(l1))*abs(Lept_Id.at(l2))==pow(m_TaId,2) ){
    m = DiTauMass(Lep1,l1,Lep2,l2);
  }
  else{
    m = (Lep1+Lep2).M();
  }
  return m;
  
}

double h4l::DiTauMass(TLorentzVector Tau0, int idtau0,
		      TLorentzVector Tau1, int idtau1){

  Print("H4lNtupleCreator:: Calculating di-Tau Mass ...");
  
  double diTmass=0.;
  double m_etx=0., m_ety=0., m_sumet=0.;
  if(m_NtupTau){
    m_etx   = met_bdt_etx;
    m_ety   = met_bdt_ety;
    m_sumet = met_bdt_sumet;
  }
  else{
    m_etx   = met_etx;
    m_ety   = met_ety;
    m_sumet = met_sumet;
  }
  TVector2 MetVec(m_etx,m_ety);

  MMC->SetMetVec(MetVec);
  MMC->SetVisTauVec(0,Tau0);
  MMC->SetVisTauVec(1,Tau1);
  MMC->SetVisTauType(0,Lept_numTrack.at(idtau0)*10);
  MMC->SetVisTauType(1,Lept_numTrack.at(idtau1)*10);
  MMC->SetSumEt(m_sumet);
  MMC->SetNjet25(Part.getNjet30());

  //std::cout<<" met_etx "<<met_etx<<"  met_ety "<<met_ety
  //	   <<" met_sumet "<<met_sumet<<std::endl;
  //std::cout<<"  Lept_numTrack.at(idtau0)*10 "<< Lept_numTrack.at(idtau0)*10 <<" Lept_numTrack.at(idtau1)*10 "<<Lept_numTrack.at(idtau1)*10<<std::endl;
  //std::cout<<" Part.getNjet30() "<<Part.getNjet30()<<std::endl;

  PrintOut("                     Running MMC ... "); PrintEndl();
  int MMCtest=MMC->RunMissingMassCalculator();
  PrintOut("                     MMCtest = ");
  PrintOut(MMCtest);
  int FitStatus=MMC->GetFitStatus();
  PrintOut("  ,  FitStatus = "); PrintOut(FitStatus); PrintEndl();
  
  if(FitStatus==1){
    diTmass=MMC->GetFittedMass(1);
    PrintOut("                     Using Fitted Mass: ");
    PrintOut(diTmass); PrintOut(" GeV");
    PrintOut("  [Vis. Mass = "); PrintOut((double)(Tau0+Tau1).M());
    PrintOut("]"); PrintEndl();
  }
  else{
    diTmass = (Tau0+Tau1).M();
    PrintOut("                     Using Visible Mass: ");
    PrintOut(diTmass); PrintOut(" GeV"); PrintEndl();
  }
  return diTmass;

}

std::vector<double> h4l::ComputeZ1Mass(){
  
  Print("H4lNtupleCreator:: Calculating Z1 Mass ...");
  
  m_MassZ1.clear();
  double m12=0;
  for(UInt_t z1=0; z1<Quadruplets.size(); z1++){
    m12=0;
    int l1=Quadruplets.at(z1).at(0);
    int l2=Quadruplets.at(z1).at(1);
    m12=DiLeptonMass(l1,l2);
    m_MassZ1.push_back(m12);
  }
  return m_MassZ1;
  
}

std::vector<double> h4l::ComputeZ1MMCMass(){

  Print("H4lNtupleCreator:: Calculating Z1 MMC Mass ...");
  
  m_MassZ1.clear();
  double m12=0;
  for(UInt_t z1=0; z1<Quadruplets.size(); z1++){
    m12=0;
    int l1=Quadruplets.at(z1).at(0);
    int l2=Quadruplets.at(z1).at(1);
    m12=DiLeptonMMCMass(l1,l2);
    m_MassZ1.push_back(m12);
  }
  return m_MassZ1;
  
}

std::vector<double> h4l::ComputeZ2Mass(){

  Print("H4lNtupleCreator:: Calculating Z2 Mass ...");
  
  m_MassZ2.clear();
  double m34=0;
  for(UInt_t z2=0; z2<Quadruplets.size(); z2++){
    m34=0;
    int l3=Quadruplets.at(z2).at(2);
    int l4=Quadruplets.at(z2).at(3);
    m34=DiLeptonMass(l3,l4);
    m_MassZ2.push_back(m34);
  }
  return m_MassZ2;
  
}

std::vector<double> h4l::ComputeZ2MMCMass(){

  Print("H4lNtupleCreator:: Calculating Z2 MMC Mass ...");
  
  m_MassZ2.clear();
  double m34=0;
  for(UInt_t z2=0; z2<Quadruplets.size(); z2++){
    m34=0;
    int l3=Quadruplets.at(z2).at(2);
    int l4=Quadruplets.at(z2).at(3);
    m34=DiLeptonMMCMass(l3,l4);
    m_MassZ2.push_back(m34);
  }
  return m_MassZ2;
  
}

std::vector<double> h4l::Mass4Lep(){
  
  Print("H4lNtupleCreator:: Calculating 4 leptons Mass ...");
  
  m_Mass4l.clear();
  for(UInt_t q=0; q<Quadruplets.size(); q++){
    int idxl1=Quadruplets.at(q).at(0);
    int idxl2=Quadruplets.at(q).at(1);
    int idxl3=Quadruplets.at(q).at(2);
    int idxl4=Quadruplets.at(q).at(3);
    
    TLorentzVector Lep1,Lep2,Lep3,Lep4,H;
    Lep1 = GetLepLorentzVec(idxl1);
    Lep2 = GetLepLorentzVec(idxl2);
    Lep3 = GetLepLorentzVec(idxl3);
    Lep4 = GetLepLorentzVec(idxl4);
    
    H = Lep1+Lep2+Lep3+Lep4;
    
    m_Mass4l.push_back( H.M() );
    FourLepPt.push_back( H.Pt() );
    FourLepEta.push_back( H.Eta() );
    FourLepPhi.push_back( H.Phi() );
  }
  return m_Mass4l;
  
}

//std::vector<double> h4l::MMCMass4Lep(){
//  
//  Print("H4lNtupleCreator:: Calculating 4 leptons MMC Mass ...");
//  
//  m_Mass4l.clear();
//  for(UInt_t q=0; q<Quadruplets.size(); q++){
//    int idxl1=Quadruplets.at(q).at(0);
//    int idxl2=Quadruplets.at(q).at(1);
//    int idxl3=Quadruplets.at(q).at(2);
//    int idxl4=Quadruplets.at(q).at(3);
//    
//    TLorentzVector Lep1,Lep2,Lep3,Lep4,H;
//    Lep1 = GetLepLorentzVec(idxl1,true);
//    Lep2 = GetLepLorentzVec(idxl2,true);
//    Lep3 = GetLepLorentzVec(idxl3,true);
//    Lep4 = GetLepLorentzVec(idxl4,true);
//    
//    H = Lep1+Lep2+Lep3+Lep4;
//    
//    m_Mass4l.push_back( H.M() );
//    FourLepMMCPt.push_back( H.Pt() );
//    FourLepMMCEta.push_back( H.Eta() );
//    FourLepMMCPhi.push_back( H.Phi() );
//  }
//  return m_Mass4l;
//  
//}

std::vector<int> h4l::Z1MassCut(){
  
  Print("H4lNtupleCreator:: Applying Z1 Mass Cuts ...");
  
  m_MassZ1Cut.clear();
  bool z1masscut=false;
  for(UInt_t z1=0; z1<Z1Mass.size(); z1++){
    z1masscut=false;
    double mz1=Z1Mass.at(z1);
    PrintOut("                          mZ1 = "); PrintOut(mz1); 
    
    if(Z1Mass.at(z1)>m_Z1LowCut && Z1Mass.at(z1)<m_Z1HighCut){
      z1masscut=true;
    }
    m_MassZ1Cut.push_back(z1masscut);
    
    PrintOut("  CutPassed: "); PrintOut(m_MassZ1Cut.at(z1)); 
    PrintEndl();
  }
  return m_MassZ1Cut;
}

std::vector<int> h4l::Z2MassCut(){
  
  Print("H4lNtupleCreator:: Applying Z2 Mass Cuts ...");
  
  m_MassZ2Cut.clear();
  bool z2massgood=false;
  for(UInt_t z2=0; z2<Z2Mass.size(); z2++){
    z2massgood=false;
    
    double mz2=Z2Mass.at(z2);
    double m4l=FourLepMass.at(z2);
    PrintOut("                          mZ2 = "); PrintOut(mz2); 
    PrintOut("  m4l = "); PrintOut(m4l); 
    if(Z2Mass.at(z2)<115.){
      
      bool cond1=false,cond2=false,cond3=false;
      
      if( FourLepMass.at(z2)<=140. && Z2Mass.at(z2)>12 )
	cond1 = true;

      if( FourLepMass.at(z2)>140. && FourLepMass.at(z2)<=190. && 
	  Z2Mass.at(z2)>(12. + (50.-12.)/(190.-140.)*(FourLepMass.at(z2)-140.) ) )
	cond2 = true;
      
      if( FourLepMass.at(z2)>190. && Z2Mass.at(z2)>50. )
      	cond3 = true;
      
      if( cond1 || cond2 || cond3 )
      	z2massgood = true;
      
    }
    m_MassZ2Cut.push_back(z2massgood);
    PrintOut("  CutPassed: "); PrintOut(m_MassZ2Cut.at(z2));
    PrintEndl();
  }
  return m_MassZ2Cut;
}

std::vector<int> h4l::CheckDeltaR(){

  Print("H4lNtupleCreator:: Applying DeltaR Cut ...");
  
  m_DeltaR.clear();
  bool deltaRgood = true;
  for(UInt_t q=0; q<Quadruplets.size(); q++){
    deltaRgood=true;
    
    for(UInt_t l1=0; l1<Quadruplets.at(q).size()-1; l1++){
      int idxl1=Quadruplets.at(q).at(l1);
      
      for(UInt_t l2=l1+1; l2<Quadruplets.at(q).size(); l2++){
	int idxl2=Quadruplets.at(q).at(l2);
	
	double etaL1=0, phiL1=0, etaL2=0, phiL2=0;
	etaL1 = Lept_eta.at(idxl1);
	phiL1 = Lept_phi.at(idxl1);
	etaL2 = Lept_eta.at(idxl2);
	phiL2 = Lept_phi.at(idxl2);
	
	double dr=ComputeDeltaR(etaL1,phiL1,etaL2,phiL2);
	
	double DeltaRCut=0;
	if( abs(Lept_Id.at(idxl1))==abs(Lept_Id.at(idxl2)) ){
	  DeltaRCut = m_DeltaRCutSF;
	  PrintOut("                     Same Flavour Leptons: [l1=");
	  PrintOut((int)l1); PrintOut(",l2="); PrintOut((int)l2); 
	  PrintOut("]  DeltaR="); PrintOut(dr); PrintEndl();
	}
	else{
	  DeltaRCut = m_DeltaRCutOF;
	  PrintOut("                     Opposite Flavour Leptons: [l1=");
	  PrintOut((int)l1); PrintOut(",l2="); PrintOut((int)l2); 
	  PrintOut("]  DeltaR="); PrintOut(dr); PrintEndl();
	}
	if(dr<DeltaRCut){
	  PrintOut("                     DeltaR<"); PrintOut(DeltaRCut);
	  PrintOut(" found!"); PrintEndl();
	  deltaRgood=false;
	  break;
	}
      }
      if(!deltaRgood){
	break;
      }
    }
    m_DeltaR.push_back(deltaRgood);
    PrintOut("                          Quadruplet: ");
    PrintOut((int)q); PrintEndl();
    PrintOut("                             DR Good: ");
    PrintOut(m_DeltaR.at(q)); PrintEndl();
  }
  return m_DeltaR;
}

std::vector<int> h4l::CheckJpsiVeto(){
  
  Print("H4lNtupleCreator:: Applying JpsiVeto Cut ...");
  
  m_JpsiVeto.clear();
  bool jpsigood=true;
  for(UInt_t q=0; q<Quadruplets.size(); q++){
    jpsigood=true;
    if(QuadSubChan.at(q)==2 || QuadSubChan.at(q)==3 ||
       QuadSubChan.at(q)==5 || QuadSubChan.at(q)==6 ||
       QuadSubChan.at(q)==7 || QuadSubChan.at(q)==8  ){
      jpsigood=true;
    }
    else{
      for(UInt_t l1=0; l1<Quadruplets.at(q).size()-1; l1++){
	int idxl1=Quadruplets.at(q).at(l1);
	
	for(UInt_t l2=l1+1; l2<Quadruplets.at(q).size(); l2++){
	  int idxl2=Quadruplets.at(q).at(l2);
	  
	  if(Lept_charge.at(idxl1)*Lept_charge.at(idxl2)<0){
	    double m=DiLeptonMass(idxl1,idxl2);
	    if(m<m_JpsiCut){
	      PrintOut("                     Possible J/Psi mass found!");
	      PrintEndl();
	      jpsigood=false;
	      break;
	    }
	  }
	}
	if(!jpsigood){
	  break;
	}
      }
    }
    m_JpsiVeto.push_back(jpsigood);
    PrintOut("                          Quadruplet: ");
    PrintOut((int)q); PrintEndl();
    PrintOut("                             JpsiVeto Good: ");
    PrintOut(m_JpsiVeto.at(q)); PrintEndl();
  }
  return m_JpsiVeto;
  
}

std::vector<int> h4l::D0SignificanceCut(){

  Print("H4lNtupleCreator:: Applying D0 Significance Cut ...");
  
  m_D0Sign.clear();
  bool d0cut=false;
  for(UInt_t q=0; q<Quadruplets.size(); q++){
    d0cut=false;

    if(QuadSubChan.at(q)<4){
      int l1=Quadruplets.at(q).at(0);
      int l2=Quadruplets.at(q).at(1);
      int l3=Quadruplets.at(q).at(2);
      int l4=Quadruplets.at(q).at(3);
      
      //if( fabs(Lept_d0_unbias.at(l1))/Muon_sigd0_unbias.at(l1)<D0SignCutMu &&
      //	fabs(Lept_d0_unbias.at(l2))/Muon_sigd0_unbias.at(l2)<D0SignCutMu &&
      //	fabs(Lept_d0_unbias.at(l3))/Elec_sigd0_unbias.at(l3)<D0SignCutEl &&
      //	fabs(Lept_d0_unbias.at(l4))/Elec_sigd0_unbias.at(l4)<D0SignCutEl ){
      if( LeptonD0Sign(l1) && LeptonD0Sign(l2) &&
	  LeptonD0Sign(l3) && LeptonD0Sign(l4)   ){
	d0cut=true;
      }
    }
    m_D0Sign.push_back(d0cut);
    
    PrintOut("                           Quadruplet: "); 
    PrintOut((int)q); PrintEndl();
    PrintOut("                               D0SignGood: "); 
    PrintOut(m_D0Sign.at(q));PrintEndl();
  }
  return m_D0Sign;
}

bool h4l::LeptonD0Sign(int idl){
  
  double d0cut=0., sigd0=0.;
  int lind=Lept_Index.at(idl);
  
  if(abs(Lept_Id.at(idl))==m_MuId){
    d0cut=m_D0SignCutMu;
    if(Lept_CALOgood.at(idl)==1){ 
      sigd0=mu_calo_tracksigd0pvunbiased->at(lind);
    }
    else{
      sigd0=mu_staco_tracksigd0pvunbiased->at(lind);
    }
  }
  else if(abs(Lept_Id.at(idl))==m_ElId){
    d0cut=m_D0SignCutEl;
    if(m_year==YEAR_2011){
      sigd0=el_GSF_tracksigd0pvunbiased->at(lind);
    }
    else if(m_year==YEAR_2012){
      sigd0=el_tracksigd0pvunbiased->at(lind); 
    }
  }
  
  if( fabs(Lept_d0_unbias.at(idl))/sigd0<d0cut)
    return true;//passd0=true;

  return false;//return passd0;
  
}

//std::vector<int> h4l::ElectronMuonD0SignificanceCut(){
//
//  Print("H4lNtupleCreator:: Applying D0 Significance Cut (El-Mu Quad) ...");
//  
//  D0Sign.clear();
//  bool d0cut=false;
//  for(UInt_t q=0; q<ElMuQuadruplets.size(); q++){
//    d0cut=false;
//    
//    int l1=ElMuQuadruplets.at(q).at(0);
//    int l2=ElMuQuadruplets.at(q).at(1);
//    int l3=ElMuQuadruplets.at(q).at(2);
//    int l4=ElMuQuadruplets.at(q).at(3);
//    
//    if( fabs(Elec_d0_unbias.at(l1))/Elec_sigd0_unbias.at(l1)<D0SignCutEl &&
//	fabs(Elec_d0_unbias.at(l2))/Elec_sigd0_unbias.at(l2)<D0SignCutEl &&
//	fabs(Muon_d0_unbias.at(l3))/Muon_sigd0_unbias.at(l3)<D0SignCutMu &&
//	fabs(Muon_d0_unbias.at(l4))/Muon_sigd0_unbias.at(l4)<D0SignCutMu ){
//      d0cut=true;
//    }
//    D0Sign.push_back(d0cut);
//    
//    PrintOut("                           Quadruplet: "); 
//    PrintOut((int)q); PrintEndl();
//    PrintOut("                               D0SignGood: "); 
//    PrintOut(D0Sign.at(q));PrintEndl();
//  }
//  return D0Sign;
//}

std::vector<int> h4l::FindBestQuadruplet(){
  
  Print("H4lNtupleCreator:: Finding Best Quadruplet ...");
  
  m_BestQuadruplet.clear();
  
  if( !Z1Mass.size() )
    return m_BestQuadruplet;
  
  m_DmVal.clear();
  for(UInt_t pz1=0; pz1<Z1Mass.size(); pz1++){
    if( IsD0Good.at(pz1)==1        && 
	IsKinematicGood.at(pz1)==1 &&
	IsTrigMatchGood.at(pz1)==1   ){
      
      m_DmVal.push_back( fabs(Z1Mass.at(pz1)-m_Zmass) );
    }
  }

  if(m_DmVal.size()==0){
    for(UInt_t bad=0; bad<Z1Mass.size(); bad++){
      m_BestQuadruplet.push_back(false);
    }
    return m_BestQuadruplet;
  }
  sort(m_DmVal.begin(),m_DmVal.end());
  double bestDm=m_DmVal.at(0);
  int bestDmPos=-99;
  double tmp_mz2=-99999;
  
  for(UInt_t p=0; p<Z1Mass.size(); p++){
    
    if( IsD0Good.at(p)==1        &&
        IsKinematicGood.at(p)==1 &&
	IsTrigMatchGood.at(p)==1   ){
      
      if( fabs(Z1Mass.at(p)-m_Zmass)==bestDm ){
  	if( fabs(Z2Mass.at(p)-m_Zmass)<fabs(tmp_mz2-m_Zmass) ){
  	  tmp_mz2=Z2Mass.at(p);
  	  bestDmPos=p;
  	}
      }
      
    }
  }

  for(UInt_t b=0; b<Z1Mass.size(); b++){
    if((int)b==bestDmPos){
      m_BestQuadruplet.push_back(true);
    }
    else{
      m_BestQuadruplet.push_back(false);
    }
  }
  PrintOut("                        Best is |Mz1-Mz| = ");
  PrintOut(bestDm); PrintOut(" [Mz1="); 
  PrintOut(Z1Mass.at(bestDmPos)); PrintOut(",Mz2="); 
  PrintOut(Z2Mass.at(bestDmPos)); PrintOut("]  in Position: ");
  PrintOut(bestDmPos); PrintEndl();
  
  return m_BestQuadruplet;
}

//std::vector<int> h4l::FindElectronMuonBestQuadruplet(){
//  
//  Print("H4lNtupleCreator:: Finding Best Quadruplet (El-Mu Quad) ...");
//  
//  BestQuadruplet.clear();
//  
//  if( !ElMuZ1Mass.size() )
//    return BestQuadruplet;
//  
//  DmVal.clear();
//  for(UInt_t pz1=0; pz1<ElMuZ1Mass.size(); pz1++){
//    if( ElMuIsD0Good.at(pz1)==1 &&
//	ElMuIsKinematicGood.at(pz1)==1 &&
//        ElMuIsTrigMatchGood.at(pz1)==1  ){
//      
//      DmVal.push_back( fabs(ElMuZ1Mass.at(pz1)-Zmass) );
//    }
//  }
//  
//  if(DmVal.size()==0){
//    for(UInt_t bad=0; bad<ElMuZ1Mass.size(); bad++){
//      BestQuadruplet.push_back(false);
//    }
//    return BestQuadruplet;
//  }
//  sort(DmVal.begin(),DmVal.end());
//  double bestDm=DmVal.at(0);
//  int bestDmPos=-99;
//  double tmp_mz2=-99999;
//
//  for(UInt_t p=0; p<ElMuZ1Mass.size(); p++){
//    
//    if( ElMuIsD0Good.at(p)==1 && 
//	ElMuIsKinematicGood.at(p)==1 &&
//	ElMuIsTrigMatchGood.at(p)==1  ){
//      
//      if( fabs(ElMuZ1Mass.at(p)-Zmass)==bestDm ){
//  	if( fabs(ElMuZ2Mass.at(p)-Zmass)<fabs(tmp_mz2-Zmass) ){
//  	  tmp_mz2=ElMuZ2Mass.at(p);
//  	  bestDmPos=p;
//  	}
//      }
//      
//    }
//  }
//  
//  for(UInt_t b=0; b<ElMuZ1Mass.size(); b++){
//    if((int)b==bestDmPos){
//      BestQuadruplet.push_back(true);
//    }
//    else{
//      BestQuadruplet.push_back(false);
//    }
//  }
//  PrintOut("                        Best is |Mz1-Mz| = ");
//  PrintOut(bestDm); PrintOut(" [Mz1="); 
//  PrintOut(ElMuZ1Mass.at(bestDmPos)); PrintOut(",Mz2="); 
//  PrintOut(ElMuZ2Mass.at(bestDmPos)); PrintOut("]  in Position: ");
//  PrintOut(bestDmPos); PrintEndl();
//  
//  return BestQuadruplet;
//}

//void h4l::FindFinalBestQuadruplet(){
//  
//  for(UInt_t f=0; f<MuElIsBestQuadruplet.size(); f++){
//
//    if( MuElIsBestQuadruplet.at(f)==0 &&
//	ElMuIsBestQuadruplet.at(f)==0 ){
//      
//      MuElFinalQuadruplet.push_back(false);
//      ElMuFinalQuadruplet.push_back(false);
//      
//    }
//    
//    if( MuElIsBestQuadruplet.at(f)==1 &&
//	ElMuIsBestQuadruplet.at(f)==0 ){
//      
//      //FinalBestQuadrupletType.push_back("MuEl");
//      //FinalBestQuadrupletPos.push_back(f);
//      MuElFinalQuadruplet.push_back(true);
//      ElMuFinalQuadruplet.push_back(false);
//      
//    }
//    if( MuElIsBestQuadruplet.at(f)==0 &&
//	ElMuIsBestQuadruplet.at(f)==1 ){
//      
//      //FinalBestQuadrupletType.push_back("ElMu");
//      //FinalBestQuadrupletPos.push_back(f);
//      MuElFinalQuadruplet.push_back(false);
//      ElMuFinalQuadruplet.push_back(true);
//      
//    }
//    if( MuElIsBestQuadruplet.at(f)==1 &&
//	ElMuIsBestQuadruplet.at(f)==1 ){
//      
//      if( fabs(MuElZ1Mass.at(f)-Zmass)<fabs(ElMuZ1Mass.at(f)-Zmass) ){
//	//FinalBestQuadrupletType.push_back("MuEl");
//	//FinalBestQuadrupletPos.push_back(f);
//	MuElFinalQuadruplet.push_back(true);
//	ElMuFinalQuadruplet.push_back(false);
//      }
//      else{
//	//FinalBestQuadrupletType.push_back("ElMu");
//	//FinalBestQuadrupletPos.push_back(f);
//	MuElFinalQuadruplet.push_back(false);
//	ElMuFinalQuadruplet.push_back(true);
//      }
//    }
//    
//  }
//  
//  
//}

void h4l::ApplyZMassConstraint(){
  
  Print("H4lNtupleCreator:: Applying ZMass Constraint ...");
  
  //ConstrZ1Mass.clear();
  //ConstrZ2Mass.clear();
  //ConstrFourLepMass.clear();
  //for(UInt_t q=0; q<Quadruplets.size(); q++){
  //  
  //  double QuadMass = FourLepMass.at(q)*GeV;
  //
  //  int l1=Quadruplets.at(q).at(0);
  //  int l2=Quadruplets.at(q).at(1);
  //  int l3=Quadruplets.at(q).at(2);
  //  int l4=Quadruplets.at(q).at(3);
  //
  //  TLorentzVector Lep1,Lep2,Lep3,Lep4;
  //  //Mu1.SetPtEtaPhiM( Muon_pt.at(mu1)*GeV,Muon_eta.at(mu1),
  //  //		      Muon_phi.at(mu1),m_MuMass*GeV );
  //  //Mu2.SetPtEtaPhiM( Muon_pt.at(mu2)*GeV,Muon_eta.at(mu2),
  //  //		      Muon_phi.at(mu2),m_MuMass*GeV );
  //  //El1.SetPtEtaPhiM( Elec_pt.at(el1)*GeV,Elec_eta.at(el1),
  //  //		      Elec_phi.at(el1),m_ElMass*GeV );
  //  //El2.SetPtEtaPhiM( Elec_pt.at(el2)*GeV,Elec_eta.at(el2),
  //  //		      Elec_phi.at(el2),m_ElMass*GeV );
  //  Lep1=GetLepLorentzVec(l1,true);
  //  Lep2=GetLepLorentzVec(l2,true);
  //  Lep3=GetLepLorentzVec(l3,true);
  //  Lep4=GetLepLorentzVec(l4,true);
  //  
  //  CLHEP::HepMatrix HMl1,HMl2,HMl3,HMl4,HMph;
  //  
  //  bool hasWidth = true;
  //  ZMassConstraint::ConstraintFit *MassFit = new ZMassConstraint::ConstraintFit(m_Zmass*GeV,hasWidth,m_Zwidth*GeV);
  //  ZMassConstraint::ConstraintFitInput Z1Input, Z2Input;
  //  ZMassConstraint::ConstraintFitOutput FitRes, FitRes2;
  //  
  //  HMl1=GetLepCovMatrix(Lep1,l1);
  //  //HMmu1 = ZMassConstraint::getCovarianceMatrixd0z0PhiThetaPMuon( Mu1.P(),
  //  //								   Muon_cov_d0.at(mu1),
  //  //								   Muon_cov_z0.at(mu1),
  //  //								   Muon_cov_phi.at(mu1),
  //  //								   Muon_cov_theta.at(mu1),
  //  //								   Muon_cov_qoverp.at(mu1)*pow(Muon_resoscf.at(mu1),2),
  //  //								   Muon_cov_d0_z0.at(mu1),
  //  //								   Muon_cov_d0_phi.at(mu1),
  //  //								   Muon_cov_d0_theta.at(mu1),
  //  //								   Muon_cov_d0_qoverp.at(mu1)*Muon_resoscf.at(mu1),
  //  //								   Muon_cov_z0_phi.at(mu1),
  //  //								   Muon_cov_z0_theta.at(mu1),
  //  //								   Muon_cov_z0_qoverp.at(mu1)*Muon_resoscf.at(mu1),
  //  //								   Muon_cov_phi_theta.at(mu1),
  //  //								   Muon_cov_phi_qoverp.at(mu1)*Muon_resoscf.at(mu1),
  //  //								   Muon_cov_theta_qoverp.at(mu1)*Muon_resoscf.at(mu1) );
  //  
  //  HMl2=GetLepCovMatrix(Lep2,l2);
  //  //HMmu2 = ZMassConstraint::getCovarianceMatrixd0z0PhiThetaPMuon( Mu2.P(),
  //  //								   Muon_cov_d0.at(mu2),
  //  //								   Muon_cov_z0.at(mu2),
  //  //								   Muon_cov_phi.at(mu2),
  //  //								   Muon_cov_theta.at(mu2),
  //  //								   Muon_cov_qoverp.at(mu2)*pow(Muon_resoscf.at(mu2),2),
  //  //								   Muon_cov_d0_z0.at(mu2),
  //  //								   Muon_cov_d0_phi.at(mu2),
  //  //								   Muon_cov_d0_theta.at(mu2),
  //  //								   Muon_cov_d0_qoverp.at(mu2)*Muon_resoscf.at(mu2),
  //  //								   Muon_cov_z0_phi.at(mu2),
  //  //								   Muon_cov_z0_theta.at(mu2),
  //  //								   Muon_cov_z0_qoverp.at(mu2)*Muon_resoscf.at(mu2),
  //  //								   Muon_cov_phi_theta.at(mu2),
  //  //								   Muon_cov_phi_qoverp.at(mu2)*Muon_resoscf.at(mu2),
  //  //								   Muon_cov_theta_qoverp.at(mu2)*Muon_resoscf.at(mu2) );
  //
  //  
  //  
  //  Z1Input.addConstituent_FourVector_d0z0PhiThetaP( Lep1,HMl1 );
  //  Z1Input.addConstituent_FourVector_d0z0PhiThetaP( Lep2,HMl2 );
  //
  //  /*adding FSR*/
  //  //std::cout<<"Quad chan "<<QuadSubChan.at(q)<<" , FSR Cand"<<IsFsrCand.at(q)<<std::endl;
  //  if(IsFsrCand.at(q)==1){
  //    TLorentzVector Fph;
  //    Fph.SetPtEtaPhiM( FsrCandEt.at(q)*GeV,FsrCandEta.at(q),
  //			FsrCandPhi.at(q), 0.0 );
  //    
  //    if(Fph.Pt()>0.){
  //	double enRes = El_ERescaler->resolution(Fph.E()/GeV,
  //						Fph.Eta(),
  //						true)*Fph.E();
  //	
  //	HMph = GetPhCovMatrix(enRes);
  //	//HMph = ZMassConstraint::getCovarianceMatrixd0z0PhiThetaPElectron(enRes,
  //	//								 0.000001,  
  //	//								 0.000001,  
  //	//								 0.000001,  
  //	//								 0.000001,  
  //	//								 0.0,  
  //	//								 0.0,  
  //	//								 0.0,
  //	//								 0.0,
  //	//								 0.0,  
  //	//								 0.0);
  //	
  //	Z1Input.addConstituent_FourVector_d0z0PhiThetaP( Fph,HMph );
  //    }
  //  }
  //  
  //  MassFit->massFitInterface(Z1Input);
  //  FitRes = MassFit->massFitRun(-1.);
  //  
  //  if(QuadMass>190*GeV){
  //    HMl3=GetLepCovMatrix(Lep3,l3);
  //    //HMel1 = ZMassConstraint::getCovarianceMatrixd0z0PhiThetaPElectron( Elec_Ereso.at(el1),
  //    //									 Elec_cov_d0.at(el1),
  //    //									 Elec_cov_z0.at(el1),
  //    //									 Elec_cov_phi.at(el1),
  //    //									 Elec_cov_theta.at(el1),
  //    //									 Elec_cov_d0_z0.at(el1), 
  //    //									 Elec_cov_d0_phi.at(el1), 
  //    //									 Elec_cov_d0_theta.at(el1), 
  //    //									 Elec_cov_z0_phi.at(el1), 
  //    //									 Elec_cov_z0_theta.at(el1), 
  //    //									 Elec_cov_phi_theta.at(el1) );
  //    
  //    HMl4=GetLepCovMatrix(Lep4,l4);
  //    //HMel2 = ZMassConstraint::getCovarianceMatrixd0z0PhiThetaPElectron( Elec_Ereso.at(el2),
  //    //									 Elec_cov_d0.at(el2),
  //    //									 Elec_cov_z0.at(el2),
  //    //									 Elec_cov_phi.at(el2),
  //    //									 Elec_cov_theta.at(el2),
  //    //									 Elec_cov_d0_z0.at(el2), 
  //    //									 Elec_cov_d0_phi.at(el2), 
  //    //									 Elec_cov_d0_theta.at(el2), 
  //    //									 Elec_cov_z0_phi.at(el2), 
  //    //									 Elec_cov_z0_theta.at(el2), 
  //    //									 Elec_cov_phi_theta.at(el2) );
  //    
  //    Z2Input.addConstituent_FourVector_d0z0PhiThetaP( Lep3,HMl3 );
  //    Z2Input.addConstituent_FourVector_d0z0PhiThetaP( Lep4,HMl4 );
  //    
  //    MassFit->massFitInterface(Z2Input);
  //    FitRes2 = MassFit->massFitRun(-1.);
  //  }
  //  
  //  /* Results after the constraint for Z1 */
  //  TLorentzVector FLep1,FLep2,Ffph,FZ1;
  //  FLep1 = FitRes.getConstituentFourVector(0);
  //  FLep2 = FitRes.getConstituentFourVector(1);
  //  if(FitRes.getNConstituents()>2){
  //    Ffph = FitRes.getConstituentFourVector(2);
  //    FZ1  = ( FitRes.getConstituentFourVector(0) + 
  //	       FitRes.getConstituentFourVector(1) + 
  //	       FitRes.getConstituentFourVector(2) );
  //  }
  //  else{
  //    FZ1 = ( FitRes.getConstituentFourVector(0) + 
  //	      FitRes.getConstituentFourVector(1) );
  //  }
  //  
  //  /* Results after the constraint for Z2 */
  //  TLorentzVector FLep3,FLep4,FZ2;
  //  if(FitRes2.getNConstituents()>0){
  //    FLep3 = FitRes2.getConstituentFourVector(0);
  //    FLep4 = FitRes2.getConstituentFourVector(1);
  //    FZ2   = ( FitRes2.getConstituentFourVector(0) + 
  //		FitRes2.getConstituentFourVector(1) );
  //  }
  //  else{
  //    FLep3 = Lep3;//El1;
  //    FLep4 = Lep4;//El2;
  //    FZ2   = Lep3 + Lep4;//El1 + El2;
  //  }
  //  
  //  double constrMass    = (FZ1+FZ2).M();
  //  double constrZ1Mass  = FZ1.M();
  //  double constrZ2Mass  = FZ2.M();
  //  
  //  ConstrZ1Mass.push_back(constrZ1Mass/GeV);
  //  ConstrZ2Mass.push_back(constrZ2Mass/GeV);
  //  ConstrFourLepMass.push_back(constrMass/GeV);
  //  
  //  PrintOut("                    Fit Results:  Z1 Mass = "); PrintOut( Z1Mass.at(q) );
  //  PrintOut(" Z1 Const. Mass = "); PrintOut( ConstrZ1Mass.at(q) ); PrintEndl();
  //  PrintOut("                                  Z2 Mass = "); PrintOut( Z2Mass.at(q) );
  //  PrintOut(" Z2 Const. Mass = "); PrintOut( ConstrZ2Mass.at(q) ); PrintEndl();
  //  PrintOut("                                  M4lMass = "); PrintOut( FourLepMass.at(q) );
  //  PrintOut(" M4l Constr. Mass = "); PrintOut( ConstrFourLepMass.at(q) ); PrintEndl();
  //  
  //  delete MassFit;
  //}

}

double h4l::ComputeDeltaR(double eta1, double phi1,
			  double eta2, double phi2){
  
  double deltaR=0;
  double Dphi=phi1-phi2;
  double Deta=eta1-eta2;
  
  if( fabs(Dphi)>pi ) {
    if( Dphi>0 ){
      Dphi = 2*pi-Dphi;
    }
    else{
      Dphi = 2*pi+Dphi;
    }
  }
  
  deltaR=sqrt( pow(Deta,2)+pow(Dphi,2) );
  
  return deltaR;
}

//void h4l::ComputeAngularVariables(){
//  
//  ComputeMuElAngularVariables();
//  ComputeElMuAngularVariables();
//  
//}

void h4l::ComputeAngularVariables(){

  Print("H4lNtupleCreator:: Computing Angular Variables ...");
  
  DeltaPhiLab.clear();
  Acoplanarity.clear();
  CosThetaStar.clear();
  ThetaStar.clear();
  DeltaPhi.clear();
  DeltaPhi1.clear();
  CosTheta1.clear(); 
  CosTheta2.clear();

  for(UInt_t qd=0; qd<Quadruplets.size(); qd++){
    
    int l1=Quadruplets.at(qd).at(0);
    int l2=Quadruplets.at(qd).at(1);
    int l3=Quadruplets.at(qd).at(2);
    int l4=Quadruplets.at(qd).at(3);
    
    /*Defining needed 4-vector*/
    TLorentzVector Z1, Z2, H, v1, v2, v3, v4;
    //if( Muon_charge.at(mu1)>Muon_charge.at(mu2) ){
    if( Lept_charge.at(l1)>Lept_charge.at(l2) ){
      //v1.SetPtEtaPhiM( Muon_pt.at(mu2),Muon_eta.at(mu2),
      //Muon_phi.at(mu2),m_MuMass );
      //
      //v2.SetPtEtaPhiM( Muon_pt.at(mu1),Muon_eta.at(mu1),
      //Muon_phi.at(mu1),m_MuMass );
      v1=GetLepLorentzVec(l2);
      v2=GetLepLorentzVec(l1);
    }
    else{
      //v1.SetPtEtaPhiM( Muon_pt.at(mu1),Muon_eta.at(mu1),
      //Muon_phi.at(mu1),m_MuMass );
      //
      //v2.SetPtEtaPhiM( Muon_pt.at(mu2),Muon_eta.at(mu2),
      //Muon_phi.at(mu2),m_MuMass );
      v1=GetLepLorentzVec(l1);
      v2=GetLepLorentzVec(l2);
    }
    //if( Elec_charge.at(el3)>Elec_charge.at(el4) ){
    if( Lept_charge.at(l3)>Lept_charge.at(l4) ){
      //v3.SetPtEtaPhiM( Elec_pt.at(el4),Elec_eta.at(el4),
      //Elec_phi.at(el4),m_ElMass );
      //
      //v4.SetPtEtaPhiM( Elec_pt.at(el3),Elec_eta.at(el3),
      //Elec_phi.at(el3),m_ElMass );
      v3=GetLepLorentzVec(l4);
      v4=GetLepLorentzVec(l3);
    }
    else{
      //v3.SetPtEtaPhiM( Elec_pt.at(el3),Elec_eta.at(el3),
      //Elec_phi.at(el3),m_ElMass );
      
      //v4.SetPtEtaPhiM( Elec_pt.at(el4),Elec_eta.at(el4),
      //Elec_phi.at(el4),m_ElMass );
       v3=GetLepLorentzVec(l3);
       v4=GetLepLorentzVec(l4);
    }
    
    Z1=v1+v2;
    Z2=v3+v4;
    H=Z1+Z2;

    /*Calculate Z1-Z2 DeltaPhi in Lab*/
    double DPhi = fabs( Z1.Phi()-Z2.Phi() );
    double DPhiLab = 0;
    if( DPhi>TMath::Pi() ){
      DPhiLab = 2*TMath::Pi()-DPhi;
    }
    else{
      DPhiLab=DPhi;
    }
    DeltaPhiLab.push_back(DPhiLab);
    
    /*Calculate acoplanarity*/
    double phi_Z1 = atan2( Z1.Py(),Z1.Px() );
    double phi_Z2 = atan2( Z2.Py(),Z2.Px() );
    
    double a_cop = fabs(phi_Z1-phi_Z2)<TMath::Pi() ? fabs(phi_Z1-phi_Z2) : 
      2*TMath::Pi()-fabs(phi_Z1-phi_Z2);

    Acoplanarity.push_back(a_cop);
    
    /*Calculate thetastar and its cosin*/
    TLorentzVector q;
    q.SetPxPyPzE( 0, 0, ( H.E() + H.Pz() ) / 2., ( H.E() + H.Pz() ) / 2. );
    q.Boost( -( H.BoostVector() ) );
    
    const TVector3 parton = q.Vect().Unit();
    
    Z1.Boost( -( H.BoostVector() ) ); // go to Higgs RFR
    Z2.Boost( -( H.BoostVector() ) );
  
    const TVector3 z1 = Z1.Vect().Unit();
    const TVector3 z2 = Z2.Vect().Unit();
    
    double costhetastar = TMath::Cos( parton.Angle( z1 ) );
    double thetastar = parton.Angle( z1 );

    CosThetaStar.push_back(costhetastar);
    ThetaStar.push_back(thetastar);

    /*Boosting leptons into the Higgs rest frame*/
    TLorentzVector vv1( v1 ), vv2( v2 ), vv3( v3 ), vv4( v4 );
    vv1.Boost( -( H.BoostVector() ) ); // go to Higgs RFR
    vv2.Boost( -( H.BoostVector() ) );
    vv3.Boost( -( H.BoostVector() ) );
    vv4.Boost( -( H.BoostVector() ) );
    
    const TVector3 v1p = vv1.Vect();
    const TVector3 v2p = vv2.Vect();
    const TVector3 v3p = vv3.Vect();
    const TVector3 v4p = vv4.Vect();
    const TVector3 nz( 0, 0, 1. );
    
    /*Calculate DeltaPhi, DeltaPhi1*/
    const TVector3 n1p = v1p.Cross( v2p ).Unit();
    const TVector3 n2p = v3p.Cross( v4p ).Unit();
    const TVector3 nscp = nz.Cross( z1 ).Unit();
    double deltaphi = ( z1.Dot( n1p.Cross( n2p ) ) /
			TMath::Abs( z1.Dot( n1p.Cross( n2p ) ) ) *
			TMath::ACos( -n1p.Dot( n2p ) ) );
    double deltaphi1 = ( z1.Dot( n1p.Cross( nscp ) ) /
			 TMath::Abs( z1.Dot( n1p.Cross( nscp ) ) ) *
			 TMath::ACos( n1p.Dot( nscp ) ) );
    
    DeltaPhi.push_back(deltaphi);
    DeltaPhi1.push_back(deltaphi1);

    /*Calculating costheta1, costheta2*/
    TLorentzVector Z2_rfr_Z1 = Z2; // it's still in H RFR
    Z2_rfr_Z1.Boost( -( Z1.BoostVector() ) ); // now it's in Z1 RFR (both Z1 and Z2 are in H RFR)
    const TVector3 z2_rfr_Z1 = Z2_rfr_Z1.Vect();
    
    TLorentzVector Z1_rfr_Z2 = Z1; // it's still in H RFR
    Z1_rfr_Z2.Boost( -( Z2.BoostVector() ) ); // now it's in Z2 RFR (both Z1 and Z2 are still in H RFR)
    const TVector3 z1_rfr_Z2 = Z1_rfr_Z2.Vect();
    
    vv1.Boost( -( Z1.BoostVector() ) ); // Z1 and Z2 still in H RFR: put leptons in their Z's reference frame
    vv3.Boost( -( Z2.BoostVector() ) );
    
    double costheta1 = - ( z2_rfr_Z1.Dot( vv1.Vect() ) /
			   TMath::Abs( z2_rfr_Z1.Mag() * vv1.Vect().Mag() ) );
    double costheta2 = - ( z1_rfr_Z2.Dot( vv3.Vect() ) /
			   TMath::Abs( z1_rfr_Z2.Mag() * vv3.Vect().Mag() ) );

    CosTheta1.push_back(costheta1);
    CosTheta2.push_back(costheta2);
    
  }
  
}

//void h4l::ComputeElMuAngularVariables(){
//
//  Print("H4lNtupleCreator:: Computing Angular Variables (El-Mu Quad) ...");
//  
//  ElMuDeltaPhiLab.clear();
//  ElMuAcoplanarity.clear();
//  ElMuCosThetaStar.clear();
//  ElMuThetaStar.clear();
//  ElMuDeltaPhi.clear();
//  ElMuDeltaPhi1.clear();
//  ElMuCosTheta1.clear(); 
//  ElMuCosTheta2.clear();
//
//  for(UInt_t qd=0; qd<ElMuQuadruplets.size(); qd++){
//    
//    int el1=ElMuQuadruplets.at(qd).at(0);
//    int el2=ElMuQuadruplets.at(qd).at(1);
//    int mu3=ElMuQuadruplets.at(qd).at(2);
//    int mu4=ElMuQuadruplets.at(qd).at(3);
//    
//    /*Defining needed 4-vector*/
//    TLorentzVector Z1, Z2, H, v1, v2, v3, v4;
//    if( Elec_charge.at(el1)>Elec_charge.at(el2) ){
//      v1.SetPtEtaPhiM( Elec_pt.at(el2),Elec_eta.at(el2),
//		       Elec_phi.at(el2),m_ElMass );
//      
//      v2.SetPtEtaPhiM( Elec_pt.at(el1),Elec_eta.at(el1),
//		       Elec_phi.at(el1),m_ElMass );
//    }
//    else{
//      v1.SetPtEtaPhiM( Elec_pt.at(el1),Elec_eta.at(el1),
//		       Elec_phi.at(el1),m_ElMass );
//      
//      v2.SetPtEtaPhiM( Elec_pt.at(el2),Elec_eta.at(el2),
//		       Elec_phi.at(el2),m_ElMass );
//    }
//    if( Muon_charge.at(mu3)>Muon_charge.at(mu4) ){
//      v3.SetPtEtaPhiM( Muon_pt.at(mu4),Muon_eta.at(mu4),
//		       Muon_phi.at(mu4),m_MuMass );
//      
//      v4.SetPtEtaPhiM( Muon_pt.at(mu3),Muon_eta.at(mu3),
//		       Muon_phi.at(mu3),m_MuMass );
//    }
//    else{
//      v3.SetPtEtaPhiM( Muon_pt.at(mu3),Muon_eta.at(mu3),
//		       Muon_phi.at(mu3),m_MuMass );
//      
//      v4.SetPtEtaPhiM( Muon_pt.at(mu4),Muon_eta.at(mu4),
//		       Muon_phi.at(mu4),m_MuMass );
//    }
//    
//    Z1=v1+v2;
//    Z2=v3+v4;
//    H=Z1+Z2;
//
//    /*Calculate Z1-Z2 DeltaPhi in Lab*/
//    double DPhi = fabs( Z1.Phi()-Z2.Phi() );
//    double DPhiLab = 0;
//    if( DPhi>TMath::Pi() ){
//      DPhiLab = 2*TMath::Pi()-DPhi;
//    }
//    else{
//      DPhiLab=DPhi;
//    }
//    ElMuDeltaPhiLab.push_back(DPhiLab);
//    
//    /*Calculate acoplanarity*/
//    double phi_Z1 = atan2( Z1.Py(),Z1.Px() );
//    double phi_Z2 = atan2( Z2.Py(),Z2.Px() );
//    
//    double a_cop = fabs(phi_Z1-phi_Z2)<TMath::Pi() ? fabs(phi_Z1-phi_Z2) : 
//      2*TMath::Pi()-fabs(phi_Z1-phi_Z2);
//
//    ElMuAcoplanarity.push_back(a_cop);
//    
//    /*Calculate thetastar and its cosin*/
//    TLorentzVector q;
//    q.SetPxPyPzE( 0, 0, ( H.E() + H.Pz() ) / 2., ( H.E() + H.Pz() ) / 2. );
//    q.Boost( -( H.BoostVector() ) );
//    
//    const TVector3 parton = q.Vect().Unit();
//    
//    Z1.Boost( -( H.BoostVector() ) ); // go to Higgs RFR
//    Z2.Boost( -( H.BoostVector() ) );
//  
//    const TVector3 z1 = Z1.Vect().Unit();
//    const TVector3 z2 = Z2.Vect().Unit();
//    
//    double costhetastar = TMath::Cos( parton.Angle( z1 ) );
//    double thetastar = parton.Angle( z1 );
//
//    ElMuCosThetaStar.push_back(costhetastar);
//    ElMuThetaStar.push_back(thetastar);
//
//    /*Boosting leptons into the Higgs rest frame*/
//    TLorentzVector vv1( v1 ), vv2( v2 ), vv3( v3 ), vv4( v4 );
//    vv1.Boost( -( H.BoostVector() ) ); // go to Higgs RFR
//    vv2.Boost( -( H.BoostVector() ) );
//    vv3.Boost( -( H.BoostVector() ) );
//    vv4.Boost( -( H.BoostVector() ) );
//    
//    const TVector3 v1p = vv1.Vect();
//    const TVector3 v2p = vv2.Vect();
//    const TVector3 v3p = vv3.Vect();
//    const TVector3 v4p = vv4.Vect();
//    const TVector3 nz( 0, 0, 1. );
//    
//    /*Calculate DeltaPhi, DeltaPhi1*/
//    const TVector3 n1p = v1p.Cross( v2p ).Unit();
//    const TVector3 n2p = v3p.Cross( v4p ).Unit();
//    const TVector3 nscp = nz.Cross( z1 ).Unit();
//    double deltaphi = ( z1.Dot( n1p.Cross( n2p ) ) /
//			TMath::Abs( z1.Dot( n1p.Cross( n2p ) ) ) *
//			TMath::ACos( -n1p.Dot( n2p ) ) );
//    double deltaphi1 = ( z1.Dot( n1p.Cross( nscp ) ) /
//			 TMath::Abs( z1.Dot( n1p.Cross( nscp ) ) ) *
//			 TMath::ACos( n1p.Dot( nscp ) ) );
//    
//    ElMuDeltaPhi.push_back(deltaphi);
//    ElMuDeltaPhi1.push_back(deltaphi1);
//
//    /*Calculating costheta1, costheta2*/
//    TLorentzVector Z2_rfr_Z1 = Z2; // it's still in H RFR
//    Z2_rfr_Z1.Boost( -( Z1.BoostVector() ) ); // now it's in Z1 RFR (both Z1 and Z2 are in H RFR)
//    const TVector3 z2_rfr_Z1 = Z2_rfr_Z1.Vect();
//    
//    TLorentzVector Z1_rfr_Z2 = Z1; // it's still in H RFR
//    Z1_rfr_Z2.Boost( -( Z2.BoostVector() ) ); // now it's in Z2 RFR (both Z1 and Z2 are still in H RFR)
//    const TVector3 z1_rfr_Z2 = Z1_rfr_Z2.Vect();
//    
//    vv1.Boost( -( Z1.BoostVector() ) ); // Z1 and Z2 still in H RFR: put leptons in their Z's reference frame
//    vv3.Boost( -( Z2.BoostVector() ) );
//    
//    double costheta1 = - ( z2_rfr_Z1.Dot( vv1.Vect() ) /
//			   TMath::Abs( z2_rfr_Z1.Mag() * vv1.Vect().Mag() ) );
//    double costheta2 = - ( z1_rfr_Z2.Dot( vv3.Vect() ) /
//			   TMath::Abs( z1_rfr_Z2.Mag() * vv3.Vect().Mag() ) );
//    
//    ElMuCosTheta1.push_back(costheta1);
//    ElMuCosTheta2.push_back(costheta2);
//    
//  }
//  
//}

void h4l::ApplyFSRCorrection(){
  
  Print("H4lNtupleCreator:: Applying FSR Correction ...");
  
  IsFsrCand.clear();
  FsrCandEt.clear();
  FsrCandEta.clear();
  FsrCandPhi.clear();
  FsrZ1Mass.clear();
  FsrFourLepMass.clear();
  //for(UInt_t q=0; q<Quadruplets.size(); q++){
  //
  //  int l1 = Quadruplets.at(q).at(0);
  //  int l2 = Quadruplets.at(q).at(1);
  //  int l3 = Quadruplets.at(q).at(2);
  //  int l4 = Quadruplets.at(q).at(3);
  //
  //  bool isCand1=false, isCand2=false;
  //  FsrPhotons::FsrCandidate FsrCand1;
  //  FsrPhotons::FsrCandidate FsrCand2;
  //  
  //  /*tmp variables*/
  //  bool tmp_isfsr = false;
  //  double tmp_et  = 0;
  //  double tmp_eta = 0;
  //  double tmp_phi = 0;
  //  double tmp_z1  = 0;
  //  double tmp_m4l = 0;
  //
  //  if(QuadSubChan.at(q)==1 || QuadSubChan.at(q)==3){
  //    
  //    if(Z1Mass.at(q)>m_FsrZ1Low && Z1Mass.at(q)<m_FsrZ1High){
  //	
  //	//if(Muon_CBTAGgood.at(mu1)==1){
  //	if(Lept_SAgood.at(l1)==0 && Lept_CALOgood.at(l1)==0){
  //	  int idl1=Lept_Index.at(l1);
  //	  if(m_year==YEAR_2011){
  //	    if( FSRph->getFsrPhotons(Lept_eta.at(l1),
  //				     Lept_phi.at(l1),
  //				     //Muon_theta_id.at(mu1),
  //				     mu_staco_id_theta->at(idl1),
  //				     //Muon_phi_id.at(mu1),
  //				     mu_staco_id_phi->at(idl1),
  //				     ph_eta,
  //				     ph_phi,
  //				     ph_Et,
  //				     ph_f1,
  //				     ph_author,
  //				     el_GSF_cl_eta,
  //				     el_GSF_cl_phi,
  //				     el_GSF_cl_pt,
  //				     el_GSF_f1,
  //				     el_GSF_tracktheta,
  //				     el_GSF_trackphi,
  //				     FsrCand1) ){
  //	      isCand1=true;
  //	    }
  //	  }
  //	  else if(m_year==YEAR_2012){
  //	    if( FSRph->getFsrPhotons(Lept_eta.at(l1),
  //				     Lept_phi.at(l1),
  //				     //Muon_theta_id.at(mu1),
  //				     mu_staco_id_theta->at(idl1),
  //				     //Muon_phi_id.at(mu1),
  //				     mu_staco_id_phi->at(idl1),
  //				     ph_eta,
  //				     ph_phi,
  //				     ph_Et,
  //				     ph_f1,
  //				     ph_author,
  //				     el_cl_eta,
  //				     el_cl_phi,
  //				     el_cl_pt,
  //				     el_f1,
  //				     el_tracktheta,
  //				     el_trackphi,
  //				     FsrCand1) ){
  //	      isCand1=true;
  //	    }
  //	  }
  //	}
  //	
  //	//if(Muon_CBTAGgood.at(mu2)==1){
  //	if(Lept_SAgood.at(l2)==0 && Lept_CALOgood.at(l2)==0){
  //	  int idl2=Lept_Index.at(l2);
  //	  if(m_year==YEAR_2011){
  //	    if( FSRph->getFsrPhotons(Lept_eta.at(l2),
  //				     Lept_phi.at(l2),
  //				     //Muon_theta_id.at(mu2),
  //				     mu_staco_id_theta->at(idl2),
  //				     //Muon_phi_id.at(mu2),
  //				     mu_staco_id_phi->at(idl2),
  //				     ph_eta,
  //				     ph_phi,
  //				     ph_Et,
  //				     ph_f1,
  //				     ph_author,
  //				     el_GSF_cl_eta,
  //				     el_GSF_cl_phi,
  //				     el_GSF_cl_pt,
  //				     el_GSF_f1,
  //				     el_GSF_tracktheta,
  //				     el_GSF_trackphi,
  //				     FsrCand2) ){
  //	      isCand2=true;
  //	    }
  //	  }
  //	  else if(m_year==YEAR_2012){
  //	    if( FSRph->getFsrPhotons(Lept_eta.at(l2),
  //				     Lept_phi.at(l2),
  //				     //Muon_theta_id.at(mu2),
  //				     mu_staco_id_theta->at(idl2),
  //				     //Muon_phi_id.at(mu2),
  //				     mu_staco_id_phi->at(idl2),
  //				     ph_eta,
  //				     ph_phi,
  //				     ph_Et,
  //				     ph_f1,
  //				     ph_author,
  //				     el_cl_eta,
  //				     el_cl_phi,
  //				     el_cl_pt,
  //				     el_f1,
  //				     el_tracktheta,
  //				     el_trackphi,
  //				     FsrCand2) ){
  //	      isCand2=true;
  //	    }
  //	  }
  //	}
  //    }
  //  }
  //  if(isCand1 && isCand2){
  //    tmp_isfsr = true;
  //    if(FsrCand2.Et>FsrCand1.Et){	
  //	tmp_et  = FsrCand2.Et/GeV; 
  //	tmp_eta = FsrCand2.eta;
  //	tmp_phi = FsrCand2.phi;
  //    }
  //    else{
  //	tmp_et  = FsrCand1.Et/GeV; 
  //    	tmp_eta = FsrCand1.eta;
  //	tmp_phi = FsrCand1.phi;
  //    }
  //  }
  //  if(isCand1 && !isCand2){
  //    tmp_isfsr = true;
  //    tmp_et    = FsrCand1.Et/GeV; 
  //    tmp_eta   = FsrCand1.eta;
  //    tmp_phi   = FsrCand1.phi;
  //  }
  //  if(isCand2 && !isCand1){
  //    tmp_isfsr = true;
  //    tmp_et    = FsrCand2.Et/GeV; 
  //    tmp_eta   = FsrCand2.eta;
  //    tmp_phi   = FsrCand2.phi;
  //  }
  //  if(tmp_isfsr){
  //    TLorentzVector Lep1,Lep2,Lep3,Lep4,fph;
  //    //Mu1.SetPtEtaPhiM( Muon_pt.at(mu1),Muon_eta.at(mu1),
  //    //Muon_phi.at(mu1),m_MuMass );
  //    //Mu2.SetPtEtaPhiM( Muon_pt.at(mu2),Muon_eta.at(mu2),
  //    //Muon_phi.at(mu2),m_MuMass );
  //    //El1.SetPtEtaPhiM( Elec_pt.at(el1),Elec_eta.at(el1),
  //    //Elec_phi.at(el1),m_ElMass );
  //    //El2.SetPtEtaPhiM( Elec_pt.at(el2),Elec_eta.at(el2),
  //    //Elec_phi.at(el2),m_ElMass );
  //    Lep1=GetLepLorentzVec(l1);
  //    Lep2=GetLepLorentzVec(l2);
  //    Lep3=GetLepLorentzVec(l3);
  //    Lep4=GetLepLorentzVec(l4);
  //    fph.SetPtEtaPhiM(tmp_et,tmp_eta,tmp_phi,0.0);
  //    tmp_z1  = (Lep1+Lep2+fph).M();
  //    tmp_m4l = (Lep1+Lep2+Lep3+Lep4+fph).M();
  //    if(tmp_z1>m_FsrZ1Thr){
  //	tmp_isfsr=false;
  //    }
  //  }
  //  else{
  //    tmp_z1  = Z1Mass.at(q);
  //    tmp_m4l = FourLepMass.at(q);
  //  }
  //  IsFsrCand.push_back(tmp_isfsr);
  //  FsrCandEt.push_back(tmp_et);
  //  FsrCandEta.push_back(tmp_eta);
  //  FsrCandPhi.push_back(tmp_phi);
  //  FsrZ1Mass.push_back(tmp_z1);
  //  FsrFourLepMass.push_back(tmp_m4l);
  //  PrintOut("                    Results:  FSR Found = "); 
  //  PrintOut( IsFsrCand.at(q) ); PrintEndl();
  //  PrintOut("                                  Z1 Mass = ");
  //  PrintOut( Z1Mass.at(q) ); PrintOut(" Z1 FSR Mass = "); 
  //  PrintOut( FsrZ1Mass.at(q) ); PrintEndl();
  //  PrintOut("                                  M4lMass = "); 
  //  PrintOut( FourLepMass.at(q) ); PrintOut(" M4l FSR Mass = "); 
  //  PrintOut( FsrFourLepMass.at(q) ); PrintEndl();
  //}
  
}
