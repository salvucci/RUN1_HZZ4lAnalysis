#include "h4l.h"

void h4l::CheckTriggerMatch(MuonTriggerMatching* singlemu,
			       MuonTriggerMatching* dimu,
			       ElectronTriggerMatching* electron,
			       int period){
  
  Print("H4lNtupleCreator:: Checking Trigger Match ...");
  
  IsTrigMatchGood.clear();
  IsSingleLepMatch.clear();
  IsDiLepMatch.clear();
  for(UInt_t tr=0; tr<Quadruplets.size(); tr++){
    m_SingleLep_Match=false, m_DiLep_Match=false; m_ElMu_Match=false;

    if(QuadSubChan.at(tr)<4){
      m_SingleLep_Match = SingleTrigMatching(singlemu,electron,period,tr);
      m_DiLep_Match = DiLepTrigMatching(dimu,electron,period,tr);
      m_ElMu_Match = MuElTrigMatching(electron,tr);
    }
    
    if(m_SingleLep_Match || m_DiLep_Match || m_ElMu_Match){
      IsTrigMatchGood.push_back(true);
    }
    else{
      IsTrigMatchGood.push_back(false);
    }
    
    if(m_SingleLep_Match){
      IsSingleLepMatch.push_back(true);
    }
    else{
      IsSingleLepMatch.push_back(false);
    }
    
    if(m_DiLep_Match || m_ElMu_Match){
      IsDiLepMatch.push_back(true);
    }
    else{
      IsDiLepMatch.push_back(false);
    }
    
    PrintOut("                           Quadruplet: "); 
    PrintOut((int)tr); PrintEndl();
    PrintOut("                               SingleLep Match: "); 
    PrintOut(m_SingleLep_Match); 
    PrintOut("  DiLep Match: "); PrintOut(m_DiLep_Match);
    PrintOut("  ElMu Match: "); PrintOut(m_ElMu_Match); PrintEndl();
    PrintOut("                               Match: "); 
    PrintOut(IsTrigMatchGood.at(tr)); PrintEndl();
  }
  
}

bool h4l::SingleTrigMatching(MuonTriggerMatching* singlemu,
				ElectronTriggerMatching* electron,
				int period,
				UInt_t tr){
  
  Print("                              Checking Single Lepton Trigger Match ...");
  
  for(UInt_t l=0; l<Quadruplets.at(tr).size(); l++){
    
    m_match_tool=false;
    int idxl=Quadruplets.at(tr).at(l);
    
    if(abs(Lept_Id.at(idxl))==13){
      for(UInt_t ch=0; ch<m_Muchain.size(); ch++){
	
	if(m_year==YEAR_2011){
	  if(period<9){
	    m_match_tool = singlemu->match( Lept_eta.at(idxl),
					    Lept_phi.at(idxl),
					    m_Muchain.at(ch).c_str() );
	  }
	  else{
	    m_match_tool = singlemu->match( Lept_eta.at(idxl),
					    Lept_phi.at(idxl),
					    (m_Muchain.at(ch)+"_medium").c_str() );
	  }
	  if(m_match_tool){// && Lept_pt.at(idxl)>MuThreshold.at(ch)){
	    return true;
	  }
	}
	
	if(m_year==YEAR_2012){
	  m_match_tool = singlemu->match( Lept_eta.at(idxl),
					  Lept_phi.at(idxl),
					  m_Muchain.at(ch).c_str() );
	  if( m_match_tool){// && Lept_pt.at(idxl)>MuThreshold.at(ch) ){
	    return true;
	  }
	}
      }
    }
    
    else{
      for(UInt_t ch=0; ch<m_Elchain.size(); ch++){
	
	if(m_year==YEAR_2011){
	  if(period<10){
	    m_match_tool = electron->match( Lept_eta.at(idxl),
					    Lept_phi.at(idxl),
					    m_Elchain.at(ch).c_str() );
	  }
	  else if(period<11){
	    m_match_tool = electron->match( Lept_eta.at(idxl),
					    Lept_phi.at(idxl),
					    m_Elchain.at(ch).c_str() );
	  }
	  else{
	    m_match_tool = electron->match( Lept_eta.at(idxl),
					    Lept_phi.at(idxl),
					    m_Elchain.at(ch).c_str() );
	  }
	  if(m_match_tool){// && Elec_Et.at(idxl)>ElThreshold.at(ch)){
	    return true;
	  }
	}
	
	if(m_year==YEAR_2012){
	  m_match_tool = electron->match( Lept_eta.at(idxl),
					  Lept_phi.at(idxl),
					  m_Elchain.at(ch).c_str() );
	  if( m_match_tool){// && Elec_Et.at(idxl)>ElThreshold.at(ch) ){
	    return true;
	  }
	}
      }
    }
  }
  return false;
}

bool h4l::DiLepTrigMatching(MuonTriggerMatching* dimu,
			       ElectronTriggerMatching* electron,
			       int period,
			       UInt_t tr){
  
  Print("                              Checking Di-Lepton Trigger Match ...");
  
  int l1=Quadruplets.at(tr).at(0);
  int l2=Quadruplets.at(tr).at(1);
  int l3=Quadruplets.at(tr).at(2);
  int l4=Quadruplets.at(tr).at(3);
  
  bool DiLepMatch1=false, DiLepMatch2=false, DiLepMatch=false;
  if(QuadSubChan.at(tr)==0){
    DiLepMatch = DiElTrigMatch(electron,m_DiElchain,
			       m_DiElThreshold,tr,period);
  }
  if(QuadSubChan.at(tr)==1){
    DiLepMatch = DiMuTrigMatch(dimu,m_DiMuchain,m_DiMuThreshold,tr);
  }
  if(QuadSubChan.at(tr)==2){
    DiLepMatch1 = DiElTrigMatch(electron,m_DiElchain,
				m_DiElThreshold,l1,l2,period);
    DiLepMatch2 = DiMuTrigMatch(dimu,m_DiMuchain,
				m_DiMuThreshold,l3,l4);
  }
  if(QuadSubChan.at(tr)==3){
    DiLepMatch1 = DiMuTrigMatch(dimu,m_DiMuchain,
				m_DiMuThreshold,l1,l2);
    DiLepMatch2 = DiElTrigMatch(electron,m_DiElchain,
				m_DiElThreshold,l3,l4,period);
  }
  if(DiLepMatch1 || DiLepMatch2 || DiLepMatch)
    return true;
  
  return false;
  
}

bool h4l::MuElTrigMatching(ElectronTriggerMatching* electron,
			      UInt_t tr){
  
  Print("                              Checking Elec-Muon Trigger Match ...");
  
  int l1=Quadruplets.at(tr).at(0);
  int l2=Quadruplets.at(tr).at(1);
  int l3=Quadruplets.at(tr).at(2);
  int l4=Quadruplets.at(tr).at(3);

  if(QuadSubChan.at(tr)==0 || QuadSubChan.at(tr)==1){ return false; }

  bool elmuMatch1=false, elmuMatch2=false;
  bool elmuMatch3=false, elmuMatch4=false;
  
  if(QuadSubChan.at(tr)==2){
    elmuMatch1 = ElMuTrigMatch(electron,m_ElMuchain,l3,l1);
    elmuMatch2 = ElMuTrigMatch(electron,m_ElMuchain,l3,l2);
    elmuMatch3 = ElMuTrigMatch(electron,m_ElMuchain,l4,l1);
    elmuMatch4 = ElMuTrigMatch(electron,m_ElMuchain,l4,l2);
  }
  
  if(QuadSubChan.at(tr)==3){
    elmuMatch1 = ElMuTrigMatch(electron,m_ElMuchain,l1,l3);
    elmuMatch2 = ElMuTrigMatch(electron,m_ElMuchain,l1,l4);
    elmuMatch3 = ElMuTrigMatch(electron,m_ElMuchain,l2,l3);
    elmuMatch4 = ElMuTrigMatch(electron,m_ElMuchain,l2,l4);
  }
  
  if(elmuMatch1 || elmuMatch2 || elmuMatch3 || elmuMatch4){
    return true;
  }
  
  return false;
  
}

bool h4l::DiMuTrigMatch(MuonTriggerMatching* dimu,
			   std::vector<std::string> chain,
			   std::vector<double> Threshold,
			   UInt_t tr){
  
  for(UInt_t l1=0; l1<Quadruplets.at(tr).size()-1; l1++){
    
    for(UInt_t l2=l1+1; l2<Quadruplets.at(tr).size(); l2++){

      int idl1=Quadruplets.at(tr).at(l1);
      int idl2=Quadruplets.at(tr).at(l2);
      
      if( DiMuTrigMatch(dimu,chain,Threshold,idl1,idl2) )
	return true;
      
    }
  }

  return false;
  
}

bool h4l::DiMuTrigMatch(MuonTriggerMatching* dimu,
			   std::vector<std::string> chain,
			   std::vector<double> Threshold,
			   int idxmu1, int idxmu2){
  
  Print("                                 Checking Di-Muon Trigger Match ...");
  
  std::pair<bool,bool> res1(false,false), res2(false,false);
  
  TLorentzVector Mu1, Mu2;
  Mu1 = GetLepLorentzVec(idxmu1);
  Mu2 = GetLepLorentzVec(idxmu2);
  m_match_tool=false;
      
  for(UInt_t ch=0; ch<chain.size(); ch++){
    
    if(m_year==YEAR_2011){
      m_match_tool = dimu->matchDimuon(Mu1,Mu2,
				     chain.at(ch).c_str(),res1,res2);
      if(!m_match_tool){
	if(m_Debug){
	  std::cerr << chain.at(ch).c_str() << " is not supported" << std::endl;
	}
      } 
      else {
	if(m_match_tool){
	  if( res1.first && //Muon_pt.at(idxmu1)>Threshold.at(ch) && 
	      res2.first  ){//&& Muon_pt.at(idxmu2)>Threshold.at(ch) ){
	    return true;
	  }
	}
      }
    }
    
    if(m_year==YEAR_2012){
      m_match_tool = dimu->matchDimuon(Mu1,Mu2,
				     chain.at(ch).c_str(),res1,res2);
      if(!m_match_tool){
	if(m_Debug){
	  std::cerr << chain.at(ch).c_str() << " is not supported" << std::endl;
	}
      }
      else{
	if(chain.at(ch)=="EF_2mu13"){
	  if( res1.first && //Muon_pt.at(idxmu1)>Threshold.at(ch) && 
	      res2.first ){//&& Muon_pt.at(idxmu2)>Threshold.at(ch) ){
	    return true;
	  }
	}
	else{
	  if( (res1.first &&// Muon_pt.at(idxmu1)>Threshold.at(1) && 
	       res2.second) ||
	      //&& Muon_pt.at(idxmu2)>Threshold.at(2)) ||
	      
	      (res1.second && //Muon_pt.at(idxmu1)>Threshold.at(2) && 
	       res2.first) ){ //&& Muon_pt.at(idxmu2)>Threshold.at(1))   ){
	    return true;
	  }
	}
      }
    }
  }
  return false;
}

bool h4l::DiElTrigMatch(ElectronTriggerMatching* electron,
			   std::vector<std::string> chain,
			   std::vector<double> Threshold,
			   UInt_t quad,
			   int period){
  
  for(UInt_t l1=0; l1<Quadruplets.at(quad).size()-1; l1++){
    
    for(UInt_t l2=l1+1; l2<Quadruplets.at(quad).size(); l2++){
      
      int idl1=Quadruplets.at(quad).at(l1);
      int idl2=Quadruplets.at(quad).at(l2);
      
      if( DiElTrigMatch(electron,chain,Threshold,idl1,idl2,period) )
	return true;
      
    }
  }
  
  return false;
  
}

bool h4l::DiElTrigMatch(ElectronTriggerMatching* electron,
			   std::vector<std::string> chain,
			   std::vector<double> Threshold,
			   int idxel1, int idxel2, int period){

  Print("                                 Checking Di-Electron Trigger Match ...");
  
  bool res1=false, res2 =false;
  
  TLorentzVector El1, El2;
  El1=GetLepLorentzVec(idxel1);
  El2=GetLepLorentzVec(idxel2);
  m_match_tool=false;
  
  for(UInt_t ch=0; ch<chain.size(); ch++){
    
    if(m_year==YEAR_2011){
      if(period<10){
	m_match_tool = electron->matchDielectron(El1,El2,
					       chain.at(ch).c_str(),
					       res1,res2);
      }
      else if(period<11){
	m_match_tool = electron->matchDielectron(El1,El2,
					       chain.at(ch).c_str(),
					       res1,res2);
      }
      else{
	m_match_tool = electron->matchDielectron(El1,El2,
					       chain.at(ch).c_str(),
					       res1,res2);	    
      }
      if (!m_match_tool){
	if(m_Debug){
	  std::cerr << chain.at(ch).c_str() << " is not supported" << std::endl;
	}
      } else {
	if(m_match_tool){
	  if( res1 && //Elec_Et.at(idxel1)>Threshold.at(ch) && 
	      res2  ){//&& Elec_Et.at(idxel2)>Threshold.at(ch) ){
	    return true;
	  }
	}
      }
    }
    
    if(m_year==YEAR_2012){
      m_match_tool = electron->matchDielectron(El1,El2,
					     chain.at(ch).c_str(),
					     res1,res2);
      if(!m_match_tool){
	if(m_Debug){
	  std::cerr << chain.at(ch).c_str() << " is not supported" << std::endl;
	}
      }
      else{
	if( res1 && //Elec_Et.at(idxel1)>Threshold.at(ch) && 
	    res2 ){//&& Elec_Et.at(idxel2)>Threshold.at(ch) ){
	  return true;
	}
      }
    }
  }
  return false;
}

bool h4l::ElMuTrigMatch(ElectronTriggerMatching* electron,
			   std::vector<std::string> chain,
			   int muidx, int elidx){
  
  Print("                                 Checking Electron-Muon Trigger Match ...");
  
  TLorentzVector Mu, El;
  Mu=GetLepLorentzVec(muidx);
  El=GetLepLorentzVec(elidx);
  
  for(UInt_t ch=0; ch<chain.size(); ch++){
    m_match_tool = electron->matchElectronMuon(El,Mu,chain.at(ch));
    if(!m_match_tool){
      if(m_Debug){
	std::cerr << chain.at(ch).c_str() << " is not supported" << std::endl;
      }
    }
    else{
      if(m_match_tool){
	return true;
      }
    }
  }
  return false;
  
}
