#include "h4l.h"
#include "../DPDVar/DPDGeneralVariables.h"
#include "../DPDVar/DPDMuonVariables.h"
#include "../DPDVar/DPDElectronVariables.h"
#include "../DPDVar/DPDTauVariables.h"
#include "../DPDVar/DPDJetVariables.h"
#include "../DPDVar/DPDPhotonVariables.h"
#include "../Particles/Particles.h"

void h4l::SetYear(std::string year){
  
  if(year=="2011")     { m_year=YEAR_2011; }
  else if(year=="2012"){ m_year=YEAR_2012; }
  else if(year=="2015"){ m_year=YEAR_2015; }

}

void h4l::SetMCType(std::string mctype){
  
  if(mctype=="mc11c")      { m_mctype=MC11C;  }
  else if(mctype=="mc11d") { m_mctype=MC11D;  }
  else if(mctype=="mc12a") { m_mctype=MC12A;  }
  else if(mctype=="mc12b") { m_mctype=MC12B;  }
  else if(mctype=="mc12bd"){ m_mctype=MC12Bd; }
  else if(mctype=="mc12c") { m_mctype=MC12C;  }
  
}

void h4l::SetDataType(std::string type){
  
  if(type=="data"){ m_type=DATA; }
  else  { m_type=MC; }
    
}

void h4l::Config(std::string Channel,
		 std::string Type,
		 std::string MCtype,
		 std::string Year){
  
  std::cout<<"H4lNtupleCreator:: Initializing class ..."<<std::endl;
  
  m_channel = Channel;
  SetDataType(Type);
  SetMCType(MCtype);
  SetYear(Year);

  m_MuId = 13;
  m_ElId = 11;
  m_TaId = 15;
  
  pi         = TMath::Pi();
  m_Zmass    = 91.1876;
  m_Zwidth   = 2.4952;
  m_MuMass   = 0.1056583715;
  m_ElMass   = 0.000510998928;
  m_TaMass   = 1.77682;
  m_TaMass1p = 0.8;
  m_TaMass3p = 1.2;
  GeV        = 1000.;
  m_SelD0Cut = 1.;
  m_pT1Cut   = 20.;
  m_pT2Cut   = 15.;
  m_pT3Cut   = 10.;
  m_pT4CutMu = 6.;
  m_pT4CutEl = 7.;

  /*samples info counters*/
  Tprocessed   = 0;
  TnegWeight   = 0;
  TposWeight   = 0;
  
  /*Objects Counters*/
  VertexElec   = 0;
  VertexStaco  = 0;
  VertexCalo   = 0;
  VertexTau    = 0;
  VertexJet    = 0;
  TriggerElec  = 0;
  TriggerStaco = 0;
  TriggerCalo  = 0;
  TriggerTau   = 0;
  TriggerJet   = 0;
  nelectrons   = 0;
  nmuons       = 0;
  ntaus        = 0;
  njets        = 0;
  
  /*Events Counter*/
  LArEvt       = 0;
  TileEvt      = 0;
  BadEvt       = 0;
  TilCorrEvt   = 0;
  GRLEvt       = 0;
  VertexEvt    = 0;
  TriggerEvt   = 0;
  
  LeptonsSelEvt_2l2l = 0;
  SFOSEvt_2l2l       = 0;
  D0Evt_2l2l         = 0;
  KinematicEvt_2l2l  = 0;
  TrigMatchEvt_2l2l  = 0;
  Z1MassEvt_2l2l     = 0;
  Z2MassEvt_2l2l     = 0;
  DeltaREvt_2l2l     = 0;
  JPsiVetoEvt_2l2l   = 0;
  BestQuadEvt_2l2l   = 0;
  TrkIsoEvt_2l2l     = 0;
  CaloIsoEvt_2l2l    = 0;
  D0SignEvt_2l2l     = 0;

  LeptonsSelEvt_4mu  = 0;
  SFOSEvt_4mu        = 0;
  D0Evt_4mu          = 0;
  KinematicEvt_4mu   = 0;
  TrigMatchEvt_4mu   = 0;
  Z1MassEvt_4mu      = 0;
  Z2MassEvt_4mu      = 0;
  DeltaREvt_4mu      = 0;
  JPsiVetoEvt_4mu    = 0;
  BestQuadEvt_4mu    = 0;
  TrkIsoEvt_4mu      = 0;
  CaloIsoEvt_4mu     = 0;
  D0SignEvt_4mu      = 0;

  LeptonsSelEvt_4e   = 0;
  SFOSEvt_4e         = 0;
  D0Evt_4e           = 0;
  KinematicEvt_4e    = 0;
  TrigMatchEvt_4e    = 0;
  Z1MassEvt_4e       = 0;
  Z2MassEvt_4e       = 0;
  DeltaREvt_4e       = 0;
  JPsiVetoEvt_4e     = 0;
  BestQuadEvt_4e     = 0;
  TrkIsoEvt_4e       = 0;
  CaloIsoEvt_4e      = 0;
  D0SignEvt_4e       = 0;

  m_periodA = 0;
  m_periodB = 0;
  m_periodC = 0;
  m_periodD = 0;
  m_periodE = 0;
  m_periodF = 0;
  m_periodG = 0;
  m_periodH = 0;
  m_periodI = 0;
  m_periodJ = 0;
  m_periodK = 0;
  m_periodL = 0;
  m_periodM = 0;
  m_frac1   = 0;
  m_frac2   = 0;

  m_match_tool          = false;
  m_SingleLep_Match     = false;
  m_DiLep_Match         = false;
  m_ElMu_Match          = false;
  m_ElMatchCut_2011_a   = 21.;
  m_ElMatchCut_2011_b   = 23.;
  m_ElMatchCut_2012_a   = 25.;
  m_ElMatchCut_2012_b   = 61.;
  m_DiElMatchCut_2011   = 13.;
  m_DiElMatchCut_2012   = 13.;
  m_MuMatchCut_2011     = 20.;
  m_MuMatchCut_2012_a   = 25.;
  m_MuMatchCut_2012_b   = 37.;
  m_DiMuMatchCut_2011   = 12.;
  m_DiMuMatchCut_2012_a = 14.;
  m_DiMuMatchCut_2012_b = 19.;
  m_DiMuMatchCut_2012_c = 9.;
  m_Z1LowCut            = 50.;
  m_Z1HighCut           = 106.;
  m_FsrZ1Low            = 66.;
  m_FsrZ1High           = 89.;
  m_FsrZ1Thr            = 100.;
  m_DeltaRCutSF         = 0.1;
  m_DeltaRCutOF         = 0.2;
  m_JpsiCut             = 5.;
  m_TrkIsoCut           = 0.15;
  m_D0SignCutMu       = 3.5;
  m_D0SignCutEl       = 6.5;
  
  if(m_year==YEAR_2011){
    m_CalIsoCutEl     = 0.30;
    m_CalIsoCutMu     = 0.30;
    m_CalIsoCutSAMu   = 0.15;
    SettingTools_2011();
  }
  else if(m_year==YEAR_2012){
    m_CalIsoCutEl     = 0.20;
    m_CalIsoCutMu     = 0.30;
    m_CalIsoCutSAMu   = 0.15;
    SettingTools_2012();
  }
  m_JetAlgorithm = "AntiKt4TopoEM";
    
}

void h4l::SettingTools_2011(){
  
  /*Muon Trigger Match Configuration*/
  m_smu1=true;
  m_dmu1=false;
  m_smu2=false;
  m_dmu2=true;
  
  /*PileUp Configuration*/
  if(m_mctype==MC11C || m_mctype==MC11D){
    m_name_PU=m_DirPrefix+"Utils/Inputs/MC11c.prw.root";
    m_lumiCalcFile=m_DirPrefix+"Utils/Inputs/ilumicalc_2011_AllYear_All_Good.root";
    //}
  }
  m_defChannel=109292;
  m_dataAction=2;
    
  /*GRL Configuration*/
  if(m_type==DATA){
    m_GRLxml=m_DirPrefix+"Utils/GRL_xmls/AllGood/data11_7TeV.periodAllYear_DetStatus-v36-pro10-02_CoolRunQuery-00-04-08_All_Good.xml";
  }
  
  /* Electron Energy Rescaler input */
  m_EResInput=m_DirPrefix+"Packages/ElectronPhotonFourMomentumCorrection/data/egammaEnergyCorrectionData.root";
  //egammaAnalysisUtils/share/EnergyRescalerData.root";

  /* Electron Identification */
  m_ElIdConf = m_DirPrefix+"Packages/ElectronPhotonSelectorTools/python/ConfiguredTElectronIsEMSelectors.py";
    
  /* MuonMomentumCorrections */
  m_Data  = "Data11";
  m_Algo  = "staco";
  m_Smear = "q_pT";
  m_Rel   = "Rel17";
  m_SmDir = m_DirPrefix+"Packages/MuonMomentumCorrections/share/";
  
  /* MuonMomentumUncertainty */
  m_muResDir  = m_DirPrefix+"Packages/MuonMomentumCorrections/share/";
  if(m_type==DATA){
    m_muResFile = m_muResDir+"final_scale_factors_data2011.txt";
  }
  else{
    m_muResFile = m_muResDir+"final_scale_factors_MC11_smeared.txt";
  }

  /* MuonEfficiencyCorrections */
  m_confMu         =  Analysis::AnalysisMuonConfigurableScaleFactors::AverageOverPeriods;
  m_confMu_SA      =  Analysis::AnalysisMuonConfigurableScaleFactors::Default;
  m_muType_staco   = "STACO_CB_plus_ST_2011_SF.txt.gz";
  m_muType_stacoSA = "STACOHighEta.txt.gz";
  m_muType_calo    = "CaloTag_2011_SF.txt.gz";
  m_SfDir          = m_DirPrefix+"Packages/MuonEfficiencyCorrections/share/";
  
  /* ElectronEfficiencyCorrections */
  m_ElEffDir      = m_DirPrefix+"Packages/ElectronEfficiencyCorrection/data/";
  if(m_mctype==MC11C){
    m_ElEffFileId   = "efficiencySF.offline.Loose.2011.7TeV.rel17p0.v02.root";
    m_ElEffFileReco = "efficiencySF.offline.RecoTrk.2011.7TeV.rel17p0.v03.root";
  }
  else if(m_mctype==MC11D){
    m_ElEffFileId   = "efficiencySF.offline.Loose.2011.7TeV.rel17p0.GEO21.v01.root";
    m_ElEffFileReco = "efficiencySF.offline.RecoTrk.2011.7TeV.rel17p0.GEO21.v01.root";
  }
  
  /* TriggerScaleFactors */
  m_TrigYear  = 2011;
  m_TrigDir   = m_DirPrefix+"Packages/TrigMuonEfficiency/share/";
  m_TrigFile  = "muon_trigger_sf_mc11c.root";
  m_TrigElDir = m_DirPrefix+"Packages/ElectronEfficiencyCorrection/data/";
  m_TrigRecVr = "rel17p0.v02";
  
  /* VertexReweighting */
  m_VertDir = m_DirPrefix+"Packages/egammaAnalysisUtils/share/zvtx_weights_2011_2012.root";

  /* Jet Calibration */
  m_JetCalibConf = m_DirPrefix+"Packages/ApplyJetCalibration/data/CalibrationConfigs/InsituJES_2011_Preliminary.config";
    
  /* Trigger Chains and Thresholds*/
  m_Elchain.push_back("EF_e20_medium");
  m_Elchain.push_back("EF_e22_medium");
  m_Elchain.push_back("EF_e22vh_medium1");
  m_ElThreshold.push_back(m_ElMatchCut_2011_a);
  m_ElThreshold.push_back(m_ElMatchCut_2011_b);
  m_ElThreshold.push_back(m_ElMatchCut_2011_b);
  m_Muchain.push_back("EF_mu18_MG");
  m_MuThreshold.push_back(m_MuMatchCut_2011);
  
  m_DiElchain.push_back("EF_2e12_medium");
  m_DiElchain.push_back("EF_2e12T_medium");
  m_DiElchain.push_back("EF_2e12Tvh_medium");
  m_DiElThreshold.push_back(m_DiElMatchCut_2011);
  m_DiElThreshold.push_back(m_DiElMatchCut_2011);
  m_DiElThreshold.push_back(m_DiElMatchCut_2011);
  m_DiMuchain.push_back("EF_2mu10_loose");
  m_DiMuThreshold.push_back(m_DiMuMatchCut_2011);
  
  m_ElMuchain.push_back("EF_e10_medium_mu6");
  
}

void h4l::SettingTools_2012(){
  
  /*Muon Trigger Match Configuration*/
  m_smu1=false;
  m_dmu1=false;
  m_smu2=false;
  m_dmu2=false;
  
  /*PileUp Configuration*/
  if(m_mctype==MC12A){
    m_name_PU=m_DirPrefix+"Utils/Inputs/MC12a.prw.root";
  }
  else if(m_mctype==MC12B || m_mctype==MC12C){
    m_name_PU=m_DirPrefix+"Utils/Inputs/MC12b.prw.root";
  }
  else if(m_mctype==MC12Bd){
    m_name_PU=m_DirPrefix+"Utils/Inputs/MC12bBCH.prw.root";
  }
  m_lumiCalcFile=m_DirPrefix+"Utils/Inputs/ilumicalc_2012_AllYear_All_Good.root";
  if(m_mctype==MC12A){
    m_defChannel=160156;
  }
  else if(m_mctype==MC12B || m_mctype==MC12C){
    m_defChannel=181341;
  }
  else if(m_mctype==MC12Bd){
    m_defChannel=167992;
  }
  m_dataAction=2;
  
  /*GRL Configuration*/
  if(m_type==DATA){
    m_GRLxml=m_DirPrefix+"Utils/GRL_xmls/AllGood/data12_8TeV.periodAllYear_DetStatus-v61-pro14-02_DQDefects-00-01-00_PHYS_StandardGRL_All_Good.xml";
  }
  
  /* Electron Energy Rescaler input */
  m_EResInput=m_DirPrefix+"Packages/ElectronPhotonFourMomentumCorrection/data/egammaEnergyCorrectionData.root";
  //egammaAnalysisUtils/share/EnergyRescalerData.root";

  /* Electron Identification */
  m_ElIdInput=m_DirPrefix+"Packages/ElectronPhotonSelectorTools/data/ElectronLikelihoodPdfs.root";
  
  /* MuonMomentumCorrections */
  m_Data  = "Data12";
  m_Algo  = "staco";
  m_Smear = "q_pT";
  m_Rel   = "Rel17.2Sum13";//"Rel17.2Repro";
  m_SmDir = m_DirPrefix+"Packages/MuonMomentumCorrections/share/";
  
  /* MuonMomentumUncertainty */
  m_muResDir  = m_DirPrefix+"Packages/MuonMomentumCorrections/share/";
  if(m_type==DATA){
    m_muResFile = m_muResDir+"final_scale_factors_data2012.txt";
  }
  else{
    m_muResFile = m_muResDir+"final_scale_factors_MC12_smearing.txt";
  }
  
  /* MuonEfficiencyCorrections */
  m_confMu         = Analysis::AnalysisMuonConfigurableScaleFactors::AverageOverPeriods;
  m_confMu_SA      = Analysis::AnalysisMuonConfigurableScaleFactors::AverageOverPeriods;
  m_muType_staco   = "STACO_CB_plus_ST_2012_SF.txt.gz";
  m_muType_stacoSA = "STACO_CB_plus_ST_2012_SFms.txt.gz";
  m_muType_calo    = "CaloTag_2012_SF.txt.gz";
  m_SfDir          = m_DirPrefix+"Packages/MuonEfficiencyCorrections/share/";
  
  /* ElectronEfficiencyCorrections */
  m_ElEffDir      = m_DirPrefix+"Packages/ElectronEfficiencyCorrection/data/";
  if(m_mctype==MC12C){
    m_ElEffFileId   = "efficiencySF.offline.LooseLLH.2012.8TeV.rel17p2.GEO21.v01.root";
    m_ElEffFileReco = "efficiencySF.offline.RecoTrk.2012.8TeV.rel17p2.GEO21.v01.root";
  }
  else{
    m_ElEffFileId   = "efficiencySF.offline.LooseLLH.2012.8TeV.rel17p2.v07.root";
    m_ElEffFileReco = "efficiencySF.offline.RecoTrk.2012.8TeV.rel17p2.GEO20.v08.root";
  }

  /* Electron CaloIsoCorrection */
  m_LeakCorrFile = m_DirPrefix+"Packages/egammaAnalysisUtils/share/isolation_leakage_corrections.root";
  
  /* TriggerScaleFactors */
  m_TrigYear  = 2012;
  m_TrigDir   =  m_DirPrefix+"Packages/TrigMuonEfficiency/share/";
  m_TrigFile  = "muon_trigger_sf_2012_AtoL.p1328.root";
  m_TrigElDir = m_DirPrefix+"Packages/ElectronEfficiencyCorrection/data/";
  if(m_mctype==MC12C)
    m_TrigRecVr = "rel17p2.GEO21.v01";
  else
    m_TrigRecVr = "rel17p2.GEO20.v08";
    
  /* VertexReweighting */
  m_VertDir = m_DirPrefix+"Packages/egammaAnalysisUtils/share/zvtx_weights_2011_2012.root";

  /* Jet Calibration */
  if(m_type==DATA){
    m_JetCalibConf = m_DirPrefix+"Packages/ApplyJetCalibration/data/CalibrationConfigs/JES_Full2012dataset_Preliminary_Jan13.config";
  }
  else{
    if(m_mctype==MC12A){
      m_JetCalibConf = m_DirPrefix+"Packages/ApplyJetCalibration/data/CalibrationConfigs/JES_Full2012dataset_Preliminary_Jan13.config";
    }
    else if(m_mctype==MC12B || m_mctype==MC12Bd || m_mctype==MC12C){
      m_JetCalibConf = m_DirPrefix+"Packages/ApplyJetCalibration/data/CalibrationConfigs/JES_Full2012dataset_Preliminary_MC12b_Sep23.config";
    }
  }
  
  /* Trigger Chains and Thresholds*/
  m_Elchain.push_back("EF_e24vhi_medium1");
  m_Elchain.push_back("EF_e60_medium1");
  m_ElThreshold.push_back(m_ElMatchCut_2012_a);
  m_ElThreshold.push_back(m_ElMatchCut_2012_b);
  m_Muchain.push_back("EF_mu24i_tight");
  m_Muchain.push_back("EF_mu36_tight" );
  m_MuThreshold.push_back(m_MuMatchCut_2012_a);
  m_MuThreshold.push_back(m_MuMatchCut_2012_b);
  
  m_DiElchain.push_back("EF_2e12Tvh_loose1");
  m_DiElThreshold.push_back(m_DiElMatchCut_2012);
  m_DiMuchain.push_back("EF_2mu13");
  m_DiMuchain.push_back("EF_mu18_tight_mu8_EFFS");
  m_DiMuThreshold.push_back(m_DiMuMatchCut_2012_a);
  m_DiMuThreshold.push_back(m_DiMuMatchCut_2012_b);
  m_DiMuThreshold.push_back(m_DiMuMatchCut_2012_c);
  
  m_ElMuchain.push_back("EF_e12Tvh_medium1_mu8");
  m_ElMuchain.push_back("EF_e24vhi_loose1_mu8");
  
}


void h4l::SetOutFileName(std::string outname,
			    std::string infoFile){
  
  m_outname = outname;
  m_infoFile = infoFile;
}

void h4l::SetInitialValues(){
  
  Print("H4lNtupleCreator:: Setting Initial Values ...");
  
  run               = 0;
  event             = 0;
  lumiblock         = 0;
  LArErr            = 0;
  TileErr           = 0;
  nelecsel          = 0;
  nmusel            = 0;
  ntausel           = 0;
  njetsel           = 0;
  nvx               = 0;
  nPV               = 0;
  Period            = 0;
  isInGRL           = false;
  isLArError        = false;
  isTileError       = false;
  isBadEvent        = false;
  isTileCorrupted   = false;
  isTrigGood        = false;
  isTrigEFelecGood  = false;
  isTrigEF2elecGood = false;
  isTrigEFmuGood    = false;
  isTrigEF2muGood   = false;
  isTrigEFemuGood   = false;
  isVertexGood      = false;
  isGood4lCandidate = false;
  isAllPassed       = false;
  isAllPassedPos    = -1;
  Channel           = -1;
  met_et            = 0.;
  met_etx           = 0.;
  met_ety           = 0.;
  met_sumet         = 0.;
  met_phi           = 0.;
  met_bdt_et        = 0.;
  met_bdt_etx       = 0.;
  met_bdt_ety       = 0.;
  met_bdt_sumet     = 0.;
  met_bdt_phi       = 0.;
  hfor              = -10;
  MCchanNum         = -1;
  MCxsec            = 1;
  TrueMCxsec        = 1;
  mcweight          = -10000;
  pileupweight      = 0;
  vxtweight         = 0;

  /*truth info*/
  Z1mass_truth       = 0;
  Z2mass_truth       = 0;
  Hmass_truth        = 0;
  HpT_truth          = 0;
  Heta_truth         = 0;
  Acoplanarity_truth = 0;
  CosThetaStar_truth = 0;
  ThetaStar_truth    = 0;
  DeltaPhi_truth     = 0;
  DeltaPhi1_truth    = 0;
  CosTheta1_truth    = 0;
  CosTheta2_truth    = 0;

}

void h4l::SetPileUpReweight(){

  Print("H4lNtupleCreator:: Setting PileUpReweighting Tool ...");
  
  if(m_mctype==MC12Bd)
    PileUpTool->SetUnrepresentedDataAction(m_dataAction,1.00);
  else
    PileUpTool->SetUnrepresentedDataAction(m_dataAction);
  PileUpTool->AddConfigFile(m_name_PU);
  PileUpTool->AddLumiCalcFile(m_lumiCalcFile.c_str());
  PileUpTool->SetDefaultChannel(m_defChannel);
  PileUpTool->Initialize();
  
}

void h4l::SetMuonMomentumCorrections(){
  
  Print("H4lNtupleCreator:: Setting MuonMomentumCorrections Tool ...");
  
  if(m_year==YEAR_2011){
    MuSmear->SetAlgoSmearRelDir(m_Data,m_Algo,m_Smear,m_Rel,m_SmDir);
  }
  else if(m_year==YEAR_2012){
    MuSmear->SetAlgoSmearRelDir(m_Data,m_Algo,m_Smear,m_Rel,m_SmDir);
  }
  MuSmear->UseScale(true);
  MuSmear->UseImprovedCombine();
  
}

void h4l::SetMuonEfficiencyCorrections(){
  
  Print("H4lNtupleCreator:: Setting MuonEfficiencyCorrections Tool ...");
  
  StacoSCF->Initialise();
  StacoSCF->suppressCoverageWarnings();
  StacoSASCF->Initialise();
  StacoSASCF->suppressCoverageWarnings();
  CaloSCF->Initialise();
  CaloSCF->suppressCoverageWarnings();
  
}

void h4l::AddLumiPerPeriodToMuonEffCorr(Analysis::AnalysisMuonConfigurableScaleFactors *muonSCF){
  
  Print("H4lNtupleCreator:: Adding Luminosity Per Period To MuonEfficiencyCorrections Tools ...");
  
  if(m_year==YEAR_2011){
    muonSCF->addPeriod("B",m_periodB);
    muonSCF->addPeriod("D",m_periodD);
    muonSCF->addPeriod("E",m_periodE);
    muonSCF->addPeriod("F",m_periodF);
    muonSCF->addPeriod("G",m_periodG);
    muonSCF->addPeriod("H",m_periodH);
    muonSCF->addPeriod("I",m_periodI);
    muonSCF->addPeriod("J",m_periodJ);
    muonSCF->addPeriod("K",m_periodK);
    muonSCF->addPeriod("L",m_periodL);
    muonSCF->addPeriod("M",m_periodM);
  }
  else if(m_year==YEAR_2012){
    muonSCF->addPeriod("A",m_periodA);
    muonSCF->addPeriod("B",m_periodB);
    muonSCF->addPeriod("C",m_periodC);
    muonSCF->addPeriod("D",m_periodD);
    muonSCF->addPeriod("E",m_periodE);
    //muonSCF->addPeriod("F",m_periodF);
    muonSCF->addPeriod("G",m_periodG);
    muonSCF->addPeriod("H",m_periodH);
    muonSCF->addPeriod("I",m_periodI);
    muonSCF->addPeriod("J",m_periodJ);
    //muonSCF->addPeriod("K",m_periodK);
    muonSCF->addPeriod("L",m_periodL);
    //muonSCF->addPeriod("M",m_periodM);
  }
  
}

void h4l::SetElectronEfficiencyCorrections(){
    
  Print("H4lNtupleCreator:: Setting ElectronEfficiencyCorrections Tool ...");
  
  El_SCFID->addFileName( (m_ElEffDir+m_ElEffFileId).c_str() );
  El_SCFID->initialize();
  
  El_SCFReco->addFileName( (m_ElEffDir+m_ElEffFileReco).c_str() );
  El_SCFReco->initialize();
  
}

void h4l::SetElecEnergyRescaler(){
  
  Print("H4lNtupleCreator:: Setting Electron Energy Rescaler Tool ...");

  El_ERescaler->setFileName(m_EResInput.c_str());
  if(m_year==YEAR_2011){
    //El_ERescaler->Init(m_EResInput.c_str(),"2011","es2011a");
    if(m_mctype==MC11C)
      El_ERescaler->setESModel(egEnergyCorr::es2011c);
    else if(m_mctype==MC11D)
      El_ERescaler->setESModel(egEnergyCorr::es2011d);
  }
  else if(m_year==YEAR_2012){
    //El_ERescaler->Init(m_EResInput.c_str(),"2012","es2012");
    if(m_mctype==MC12A || m_mctype==MC12B || m_mctype==MC12Bd)
      El_ERescaler->setESModel(egEnergyCorr::es2012a);
    else if(m_mctype==MC12C){
      El_ERescaler->setESModel(egEnergyCorr::es2012c);
    }
  }
  El_ERescaler->initialize();
  
}

void h4l::SetElectronId(){

  Print("H4lNtupleCreator:: Setting Electron Identification Tool ...");
  if(m_year==YEAR_2011){
    /*2011*/
    TPython::LoadMacro(m_ElIdConf);
    //TPython::LoadMacro(m_ElIdConf.Data());
    ElId2011 = static_cast<Root::TElectronIsEMSelector*>((void*)TPython::Eval("ConfiguredTElectronIsEMSelector(ROOT.egammaPID.ElectronIDLoosePP,electronPIDmenu.menuH4l2011)"));
    ElId2011->initialize();
  }
  else if(m_year==YEAR_2012){
    /*2012*/
    ElId2012 = new Root::TElectronLikelihoodTool();
    ElId2012->setPDFFileName(m_ElIdInput.c_str());
    ElId2012->setOperatingPoint(LikeEnum::Loose);
    ElId2012->initialize();
  }
  
}

void h4l::SetMissMassCalc(){

  Print("H4lNtupleCreator:: Setting Missing Mass Calculator Tool ...");
  if(m_year==YEAR_2011)
    MMC->SetCalibrationSet(MMCCalibrationSet::MMC2011); 
  else if(m_year==YEAR_2012)
    MMC->SetCalibrationSet(MMCCalibrationSet::MMC2012);
  
}

void h4l::SetTriggerNavigation(TriggerNavigationVariables* trigNav,
				  bool isSingleMu,
				  bool isDiMu){
  
  Print("H4lNtupleCreator:: Setting TriggerNavigation Variables ...");
    
  trigNav->set_trig_DB_SMK(trig_DB_SMK);
  trigNav->set_trig_Nav_n(trig_Nav_n);
  trigNav->set_trig_Nav_chain_ChainId(trig_Nav_chain_ChainId);
  trigNav->set_trig_Nav_chain_RoIType(trig_Nav_chain_RoIType);
  trigNav->set_trig_Nav_chain_RoIIndex(trig_Nav_chain_RoIIndex);
  
  /* electrons */
  trigNav->set_trig_RoI_EF_e_egammaContainer_egamma_Electrons(trig_RoI_EF_e_egammaContainer_egamma_Electrons);
  trigNav->set_trig_RoI_EF_e_egammaContainer_egamma_ElectronsStatus(trig_RoI_EF_e_egammaContainer_egamma_ElectronsStatus);
  trigNav->set_trig_EF_el_n(trig_EF_el_n);
  trigNav->set_trig_EF_el_eta(trig_EF_el_eta);
  trigNav->set_trig_EF_el_phi(trig_EF_el_phi);
  trigNav->set_trig_EF_el_Et(trig_EF_el_Et);
 
  /*muons*/
  trigNav->set_trig_RoI_EF_mu_Muon_ROI(trig_RoI_EF_mu_Muon_ROI);
  trigNav->set_trig_RoI_L2_mu_CombinedMuonFeature(trig_RoI_L2_mu_CombinedMuonFeature);
  trigNav->set_trig_RoI_L2_mu_CombinedMuonFeatureStatus(trig_RoI_L2_mu_CombinedMuonFeatureStatus);
  trigNav->set_trig_RoI_L2_mu_MuonFeature(trig_RoI_L2_mu_MuonFeature);
  trigNav->set_trig_RoI_L2_mu_Muon_ROI(trig_RoI_L2_mu_Muon_ROI);
  trigNav->set_trig_EF_trigmuonef_track_CB_pt(trig_EF_trigmuonef_track_CB_pt);
  trigNav->set_trig_EF_trigmuonef_track_CB_eta(trig_EF_trigmuonef_track_CB_eta);
  trigNav->set_trig_EF_trigmuonef_track_CB_phi(trig_EF_trigmuonef_track_CB_phi);
  trigNav->set_trig_EF_trigmuonef_track_SA_pt(trig_EF_trigmuonef_track_SA_pt);
  trigNav->set_trig_EF_trigmuonef_track_SA_eta(trig_EF_trigmuonef_track_SA_eta);
  trigNav->set_trig_EF_trigmuonef_track_SA_phi(trig_EF_trigmuonef_track_SA_phi);
  trigNav->set_trig_EF_trigmuonef_track_MuonType(trig_EF_trigmuonef_track_MuonType);
  trigNav->set_trig_EF_trigmugirl_track_CB_pt(trig_EF_trigmugirl_track_CB_pt);
  trigNav->set_trig_EF_trigmugirl_track_CB_eta(trig_EF_trigmugirl_track_CB_eta);
  trigNav->set_trig_EF_trigmugirl_track_CB_phi(trig_EF_trigmugirl_track_CB_phi);
  trigNav->set_trig_L2_combmuonfeature_eta(trig_L2_combmuonfeature_eta);
  trigNav->set_trig_L2_combmuonfeature_phi(trig_L2_combmuonfeature_phi);
  trigNav->set_trig_L2_muonfeature_eta(trig_L2_muonfeature_eta);
  trigNav->set_trig_L2_muonfeature_phi(trig_L2_muonfeature_phi);
  trigNav->set_trig_L1_mu_eta(trig_L1_mu_eta);
  trigNav->set_trig_L1_mu_phi(trig_L1_mu_phi);
  trigNav->set_trig_L1_mu_thrName(trig_L1_mu_thrName);
  
  
  if(isSingleMu){
    // for only single mu trigger
    trigNav->set_trig_RoI_EF_mu_TrigMuonEFInfoContainerStatus(trig_RoI_EF_mu_TrigMuonEFInfoContainer_eMuonEFInfoStatus);
    trigNav->set_trig_RoI_EF_mu_TrigMuonEFInfoContainer(trig_RoI_EF_mu_TrigMuonEFInfoContainer_eMuonEFInfo); 
  }
  else if(isDiMu){
    //for only dimuon trigger
    trigNav->set_trig_RoI_EF_mu_TrigMuonEFInfoContainerStatus(trig_RoI_EF_mu_TrigMuonEFInfoContainer_MuonEFInfoStatus);
    trigNav->set_trig_RoI_EF_mu_TrigMuonEFInfoContainer(trig_RoI_EF_mu_TrigMuonEFInfoContainer_MuonEFInfo);
  }
  else{
    if(m_year==YEAR_2011){
      trigNav->set_trig_RoI_EF_mu_TrigMuonEFInfoContainerStatus(trig_RoI_EF_mu_TrigMuonEFInfoContainer_MuonEFInfoStatus);
      trigNav->set_trig_RoI_EF_mu_TrigMuonEFInfoContainer(trig_RoI_EF_mu_TrigMuonEFInfoContainer_MuonEFInfo);
    }
    else if(m_year==YEAR_2012){
      trigNav->set_trig_RoI_EF_mu_TrigMuonEFInfoContainer(trig_RoI_EF_mu_TrigMuonEFInfoContainer);
      trigNav->set_trig_RoI_EF_mu_TrigMuonEFInfoContainerStatus(trig_RoI_EF_mu_TrigMuonEFInfoContainerStatus);
    }
  }
  
  trigNav->set_trig_RoI_EF_mu_TrigMuonEFIsolationContainer(trig_RoI_EF_mu_TrigMuonEFIsolationContainer);
  trigNav->set_trig_RoI_EF_mu_TrigMuonEFIsolationContainerStatus(trig_RoI_EF_mu_TrigMuonEFIsolationContainerStatus);
  
  if ( not trigNav->isValid() ) {
    std::cerr <<"         TriggerNavigation ERROR:: Variables not correctly set!"<<std::endl;
  }
  
}

void h4l::SetGRL(){
  
  Print("H4lNtupleCreator:: Setting GRL xml file ...");
  
  GrlReader->AddXMLFile( m_GRLxml.c_str() );
  GrlReader->Interpret();
  Grl=GrlReader->GetMergedGoodRunsList();
  
}

void h4l::SetTauEventRemoval(bool notau){
  
  m_TauRemove = notau;
  if(m_TauRemove){
    std::cout<<"H4lNtupleCreator:: Tau Event Removal ON!!"<<std::endl;
  }
  else{
    std::cout<<"H4lNtupleCreator:: Tau Event Removal OFF!!"<<std::endl;
  }
}

void h4l::UseMinimalVariables(bool min){
  
  m_UseMinVar = min;
  if(m_UseMinVar){
    std::cout<<"H4lNtupleCreator:: Using Minimal Set of Variables "<<std::endl;
  }
  else{
    std::cout<<"H4lNtupleCreator:: Using Full Set of Variables "<<std::endl;
  }
  
}

void h4l::UseCorrections(bool corr){
  
  m_UseCorrections = corr;
  if(m_UseCorrections){
    std::cout<<"H4lNtupleCreator:: Using Corrections "<<std::endl;
  }
  else{
    std::cout<<"H4lNtupleCreator:: Not Using Corrections "<<std::endl;
  }
  
}

void h4l::UseWeights(bool weig){
 
  m_UseWeights = weig;
  if(m_UseWeights){
    std::cout<<"H4lNtupleCreator:: Using Weights "<<std::endl;
  }
  else{
    std::cout<<"H4lNtupleCreator:: Not Using Weights "<<std::endl;
  }
  
}

void h4l::UseParticleMinimalInfo(bool minInf){
  
  m_ParMinInfo = minInf;
  if(m_ParMinInfo){
    std::cout<<"H4lNtupleCreator:: Using Particles Minimal Information "<<std::endl;
  }
  else{
    std::cout<<"H4lNtupleCreator:: Using Particles Full Information "<<std::endl;
  }
  
}

void h4l::UseNtupTau(bool ntupTau){

  m_NtupTau=ntupTau;
  if(m_NtupTau){
    std::cout<<"H4lNtupleCreator:: Using NTUP TAU: reading MET_BDTMedium variables enabled "<<std::endl;
  }
  else{
    std::cout<<"H4lNtupleCreator:: Not Using NTUP TAU: reading MET_BDTMedium variables disabled "<<std::endl;
  }
}

void h4l::ResetEventFlags(){

  Print("H4lNtupleCreator:: Resetting Event Flags ...");

  isGood4lCandidate     = false;
  isGood4lSFOS          = false;
  isMisPairing          = false;
  isMisPairLabel        = false;
    
  isGoodCandidate_4mu   = false;
  isGoodSFOS_4mu        = false;
  isGoodD0_4mu          = false;
  isGoodKine_4mu        = false;
  isGoodTrigMatch_4mu   = false;
  isGoodMz1_4mu         = false;
  isGoodMz2_4mu         = false;
  isGoodDR_4mu          = false;
  isGoodJpsiVeto_4mu    = false;
  isBestQuadruplet_4mu  = false;
  isGoodTrkIso_4mu      = false;
  isGoodCaloIso_4mu     = false;
  isGoodD0Sign_4mu      = false;
  		        
  isGoodCandidate_4e    = false;
  isGoodSFOS_4e         = false;
  isGoodD0_4e           = false;
  isGoodKine_4e         = false;
  isGoodTrigMatch_4e    = false;
  isGoodMz1_4e          = false;
  isGoodMz2_4e          = false;
  isGoodDR_4e           = false;
  isGoodJpsiVeto_4e     = false;
  isBestQuadruplet_4e   = false;
  isGoodTrkIso_4e       = false;
  isGoodCaloIso_4e      = false;
  isGoodD0Sign_4e       = false;
  
  isGoodCandidate_2l2l  = false;
  isGoodSFOS_2l2l       = false;
  isGoodD0_2l2l         = false;
  isGoodKine_2l2l       = false;
  isGoodTrigMatch_2l2l  = false;
  isGoodMz1_2l2l        = false;
  isGoodMz2_2l2l        = false;
  isGoodDR_2l2l         = false;
  isGoodJpsiVeto_2l2l   = false;
  isBestQuadruplet_2l2l = false;
  isGoodTrkIso_2l2l     = false;
  isGoodCaloIso_2l2l    = false;
  isGoodD0Sign_2l2l     = false;
  
}
