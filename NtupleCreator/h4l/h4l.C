#define h4l_cxx

/*Class Files*/
#include "h4l.h"
#include "h4l_Config.cxx"
#include "h4l_Debug.cxx"
#include "h4l_Sel.cxx"
#include "h4l_TrigMatch.cxx"
#include "h4l_Isolation.cxx"
#include "h4l_Var.cxx"
#include "h4l_Util.cxx"
#include "../DPDVar/DataYearDef.h"
#include "../DPDVar/DPDGeneralVariables.cxx"
#include "../DPDVar/DPDElectronVariables.cxx"
#include "../DPDVar/DPDMuonVariables.cxx"
#include "../DPDVar/DPDTauVariables.cxx"
#include "../DPDVar/DPDJetVariables.cxx"
#include "../DPDVar/DPDPhotonVariables.cxx"
#include "LinkDef_h4l.h"
#include "../Dic_h4l.cxx"
#include "../Dic_h4l.h"
/*basic C++ commands*/
#include <iostream>
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
/*ROOT headers*/
#include "TROOT.h"
#include "TH1.h"
#include "TFile.h"
#include "TMath.h"
#include "TLorentzVector.h"
#include "TProof.h"
#include "TDSet.h"
/*Utils*/
#include "../HforToolD3PD/HforToolD3PD.cxx"
#include "../Utils/CrossSections.cxx"
#include "../Particles/Particles.cxx"
#include "../Particles/Particles_Get.cxx"
#include "../Particles/Particles_Clear.cxx"
#include "../Particles/Electron.cxx"
#include "../Particles/Electron_LoosePlusPlus.cxx"
#include "../Particles/Jet.cxx"
#include "../Particles/Tau.cxx"

TRandom3* h4l::m_rnd = 0;

ClassImp(h4l);

int main(int argc, char* argv[]){
  
  if(argc<3){
    std::cout<<"H4l: run H->4l selection!!"<<std::endl;
    std::cout<<"  Usage   : ./H4l [Options]"<<std::endl;
    std::cout<<"  Needed Options : "<<std::endl;
    std::cout<<"                   --channel data [Channel Name]"<<std::endl;
    std::cout<<"                   --type data [data or mc type]"<<std::endl;
    std::cout<<"                   --year 2011 [2011 or 2012]"<<std::endl;
    std::cout<<"                   --outname data_periodF.root"<<std::endl;
    std::cout<<"                   --mctype mc11c [MC Version]"<<std::endl;
    std::cout<<"  Other Options  :"<<std::endl;
    std::cout<<"                   --path /home/salvucci/Lists/ [path to Data/MC list: default is ./]"<<std::endl;
    std::cout<<"                   --list Signal.dat [List of files to process:"<<std::endl;
    std::cout<<"                                         default is 'input.txt']"<<std::endl;
    std::cout<<"                   --g or --debug [Enable Debug Mode]"<<std::endl;
    std::cout<<"                   --infoFile nelec.dat [Name Electrons Info Output File]"<<std::endl;
    std::cout<<"                   --proof lite [Enable Proof Run Mode (lite, pod or pod_panda) ]"<<std::endl;
    std::cout<<"                   --notau [Skip tau decay channel]"<<std::endl;
    std::cout<<"                   --minVar [Use minimal set of variabales]"<<std::endl;
    std::cout<<"                   --useCorr [Use MC Corrections]"<<std::endl;
    std::cout<<"                   --useWeight [Use MC Weights]"<<std::endl;
    std::cout<<"                   --useParMinInfo [Use minimal information for particles]"<<std::endl;
    std::cout<<"                   --useNTUPTAU [Use special Taus Ntuples]"<<std::endl;
    exit(0);
  };
  
  std::string path,type,outname,channel,year,mctype,listname;
  std::string infoFile,dbgOn,notauOn,minimalOn,correctOn,weightOn;
  std::string proofmode, minInfoPar, NtupTau;
  bool debug=false, proof=false, notau=false, minimal=false;
  bool correct=false, weight=false, useTDSet=false, minparinfo=false;
  bool usentuptau=false;
  channel=""; path=""; listname=""; proofmode="";
  for(int a=1; a<argc; a++){
    if(!strcmp(argv[a],"--path")){
      path = argv[a+1];
    }
    else if(!strcmp(argv[a],"--type")){
      type = argv[a+1];
    }
    else if(!strcmp(argv[a],"--outname")){
      outname = argv[a+1];
    }
    else if(!strcmp(argv[a],"--channel")){
      channel = argv[a+1];
    }
    else if(!strcmp(argv[a],"--mctype")){
      mctype = argv[a+1];
    }
    else if(!strcmp(argv[a],"--year")){
      year = argv[a+1];
    }
    else if(!strcmp(argv[a],"--debug")){
      debug = true;
      dbgOn = "dbgON";
    }
    else if(!strcmp(argv[a],"-g")){
      debug = true;
      dbgOn = "dbgON";
    }
    else if(!strcmp(argv[a],"--infoFile")){
      infoFile = argv[a+1];
    }
    else if(!strcmp(argv[a],"--proof")){
      proofmode = argv[a+1];
      proof = true;
      if(proofmode=="pod_panda"){
	useTDSet = true;
      }
    }
    else if(!strcmp(argv[a],"--notau")){
      notau   = true;
      notauOn = "notauOn";
    }
    else if(!strcmp(argv[a],"--minVar")){
      minimal = true;
      minimalOn = "minvarON";
    }
    else if(!strcmp(argv[a],"--useCorr")){
      correct = true;
      correctOn = "correctionsON";
    }
    else if(!strcmp(argv[a],"--useWeight")){
      weight = true;
      weightOn = "weightsON";
    }
    else if(!strcmp(argv[a],"--list")){
      listname = argv[a+1];
    }
    else if(!strcmp(argv[a],"--useParMinInfo")){
      minparinfo=true;
      minInfoPar="mininfoparON";
    }
    else if(!strcmp(argv[a],"--useNTUPTAU")){
      usentuptau=true;
      NtupTau="NtupTauON";
    }
  }
  std::cout<<"H4lNtupleCreator:: Running on channel "<<channel<<" ... "<<std::endl;
  if(listname==""){
    listname="input.txt";
  }
  
  /*Creating chain of files*/
  std::ifstream ifs( (path+listname).c_str() );
  if(!ifs.good()){
    std::cerr<<"                    ERROR:: File "<<path+listname
             <<" NOT FOUND!!"<<std::endl;
    return -1;
  }
  std::string treename="";
  if(usentuptau)
    treename="tau";
  else
    treename="physics";
  
  TChain *mychain = new TChain(treename.c_str());//"physics");
  TDSet *myset = new TDSet("TTree",treename.c_str());//"physics");
  std::string tmpFname;
  while( ifs.good() ){
    tmpFname="";
    getline(ifs,tmpFname);
    if(tmpFname!=""){
      if(useTDSet){
	myset->Add( tmpFname.c_str() );
      }
      else{
	mychain->Add( tmpFname.c_str() );
      }
    }
  }
  h4l sample;
  TProof *p = NULL;
  
  if(proof){
    std::string pmode="";
    if(proofmode=="lite"){
      pmode = "";
    }
    else if(proofmode=="pod" || proofmode=="pod_panda"){
      pmode = "pod://";
    }
    p = TProof::Open( pmode.c_str() );
    std::string proof = "proofON";
    TString options;
    options  = proof+","+channel+","+type+","+mctype+","+year+",";
    options += outname+","+infoFile+","+dbgOn+","+notauOn+",";
    options += minimalOn+","+correctOn+","+weightOn+","+minInfoPar;
    options += ","+NtupTau;
    
    mychain->SetProof();
    gProof->UploadPackage("H4lNtupleCreator.par");
    gProof->EnablePackage("H4lNtupleCreator");
    if(useTDSet){
      myset->Process(&sample,options);
    }
    else{
      mychain->Process(&sample,options);
    }
    
    TFile* output_file = new TFile(outname.c_str(),"recreate");
    TList* list = p->GetOutputList();
    TIter next_object((TList*) list);
    TObject* obj ;
    output_file->cd() ;
    while ((obj = next_object())) { 
      TString objname = obj->GetName();
      if(objname=="SampleInfo"       ||
	 objname=="CutFlow_4mu"      ||
	 objname=="CutFlow_4e"       ||
	 objname=="CutFlow_2l2l"     ||
	 objname=="ElecCount"        ||
	 objname=="MuCountCBST"      ||
	 objname=="MuCountCBSTSA"    ||
	 objname=="MuCountCBSTSACA"  ||
	 objname=="TauCount"         ||
	 objname=="JetCount"         ||
	 objname=="MuReco_pT"        ||
	 objname=="MuReco_eta"       ||
	 objname=="MuTruth_pT"       ||
	 objname=="MuTruth_eta"      ||
	 objname=="ElReco_eT"        ||
	 objname=="ElReco_eta"       ||
	 objname=="ElTruth_eT"       ||
	 objname=="ElTruth_eta"      ||
	 objname=="MuEff_pT"         ||
	 objname=="MuEff_eta"        ||
	 objname=="ElEff_eT"         ||
	 objname=="ElEff_eta"        ||
	 objname=="h4l"               ){
	
	//std::cout << "Object: "<<objname<<std::endl ;
	obj->Write() ;
	
      }
    }
    output_file->Write();
    p->Close("S");
    if(proofmode=="pod"){
      gSystem->Exit(0);
    }
  }
  else{
    sample.SetDebug(debug);
    sample.SetTauEventRemoval(notau);
    sample.UseMinimalVariables(minimal);
    sample.UseCorrections(correct);
    sample.UseWeights(weight);
    sample.UseParticleMinimalInfo(minparinfo);
    sample.UseNtupTau(usentuptau);
    sample.Config(channel,type,mctype,year);
    sample.SetOutFileName(outname,infoFile);
    mychain->Process(&sample);
  }
  
  return 0;
}

h4l::h4l(){

  if(!m_rnd) {
    m_rnd = new TRandom3();
  }

  m_DirPrefix      = "";
  m_Debug          = false;
  m_UseProof       = false;
  m_UseMinVar      = false;
  m_TauRemove      = false;
  m_UseCorrections = false;
  m_UseWeights     = false;
  m_ParMinInfo     = false;
  m_NtupTau        = false;
  info             = NULL;
  cutFlow_4mu      = NULL;
  cutFlow_4e       = NULL;
  cutFlow_2l2l     = NULL;
  MuCBST           = NULL;
  MuCBSTSA         = NULL;
  MuCBSTSACA       = NULL;
  ElecCount        = NULL;
  TauCount         = NULL;
  JetCount         = NULL;
  MuReco_pT        = NULL;
  MuReco_eta       = NULL;
  MuTruth_pT       = NULL;
  MuTruth_eta      = NULL;
  ElReco_eT        = NULL;
  ElReco_eta       = NULL;
  ElTruth_eT       = NULL;
  ElTruth_eta      = NULL;
  MuEff_pT         = NULL;
  MuEff_eta        = NULL;
  ElEff_eT         = NULL;
  ElEff_eta        = NULL;
  h4lTree          = NULL;
  
}

h4l::~h4l(){
}

void h4l::Begin(TTree */*tree*/){
  
  TString option = GetOption();
  
}

void h4l::SlaveBegin(TTree */*tree*/){
  
  TString option = GetOption();
  std::string argStr=(std::string)option;
  //if(argStr==""){
  //  std::cout<<" no options set!!!"<<std::endl;
  //}
  
  std::vector<std::string> OptList;
  for(size_t i=0,n; i<=argStr.length(); i=n+1){
    n=argStr.find_first_of(',',i);
    if(n==std::string::npos)
      n=argStr.length();
    std::string tmp = argStr.substr(i,n-i);
    OptList.push_back(tmp);
  }
  if(OptList[0]!=""){
    m_DirPrefix = "H4lNtupleCreator/";
    m_UseProof = true;
    
    if(OptList[7]!="")
      SetDebug(true);
    else
      SetDebug(false);
    
    if(OptList[8]!="")
      SetTauEventRemoval(true);
    else
      SetTauEventRemoval(false);

    if(OptList[9]!="")
      UseMinimalVariables(true);
    else
      UseMinimalVariables(false);

    if(OptList[10]!="")
      UseCorrections(true);
    else
      UseCorrections(false);
    
    if(OptList[11]!="")
      UseWeights(true);
    else
      UseWeights(false);

    if(OptList[12]!="")
      UseParticleMinimalInfo(true);
    else
      UseParticleMinimalInfo(false);

    if(OptList[12]!="")
      UseNtupTau(true);
    else
      UseNtupTau(false);
	 
    Config(OptList[1],OptList[2],OptList[3],OptList[4]);
    SetOutFileName(OptList[5],OptList[6]);
    
  }
  
  /* Creating Histograms */
  info         = new TH1F("SampleInfo","SampleInfo",3,0,3);
  cutFlow_4mu  = new TH1F("CutFlow_4mu","CutFlow_4mu",20,0,20);
  cutFlow_4e   = new TH1F("CutFlow_4e","CutFlow_4e",20,0,20);
  cutFlow_2l2l = new TH1F("CutFlow_2l2l","CutFlow_2l2l",20,0,20);
  ElecCount    = new TH1F("ElecCount","ElecCount",11,0,11);
  MuCBST       = new TH1F("MuCountCBST","MuCountCBST",12,0,12);
  MuCBSTSA     = new TH1F("MuCountCBSTSA","MuCountCBSTSA",12,0,12);
  MuCBSTSACA   = new TH1F("MuCountCBSTSACA","MuCountCBSTSACA",12,0,12);
  TauCount     = new TH1F("TauCount","TauCount",11,0,11);
  JetCount     = new TH1F("JetCount","JetCount",7,0,7);
  MuReco_pT    = new TH1F("MuReco_pT","MuReco_pT",30,0,150);
  MuReco_eta   = new TH1F("MuReco_eta","MuReco_eta",30,-3.0,3.0);
  MuTruth_pT   = new TH1F("MuTruth_pT","MuTruth_pT",30,0,150);
  MuTruth_eta  = new TH1F("MuTruth_eta","MuTruth_eta",30,-3.0,3.0);
  ElReco_eT    = new TH1F("ElReco_eT","ElReco_eT",30,0,150);
  ElReco_eta   = new TH1F("ElReco_eta","ElReco_eta",30,-3.0,3.0);
  ElTruth_eT   = new TH1F("ElTruth_eT","ElTruth_eT",30,0,150);
  ElTruth_eta  = new TH1F("ElTruth_eta","ElTruth_eta",30,-3.0,3.0);
  MuEff_pT     = new TH1F("MuEff_pT","MuEff_pT",30,0,150);
  MuEff_eta    = new TH1F("MuEff_eta","MuEff_eta",30,-3.0,3.0);
  ElEff_eT     = new TH1F("ElEff_eT","ElEff_eT",30,0,150);
  ElEff_eta    = new TH1F("ElEff_eta","ElEff_eta",30,-3.0,3.0);
  
  if(!m_UseProof){
    std::cout<<"H4lNtupleCreator:: Opening Output File ... "<<std::endl;
    newfile = new TFile( (m_DirPrefix+m_outname).c_str(),"recreate");
    Info.open( (m_DirPrefix+m_infoFile).c_str() );
  }
  
  /* h4l tree */
  h4lTree = new TTree("h4l","H4l tree variables");
  SetInitialValues();
  SetTreeBranches(h4lTree);

  fOutput->Add(info);
  fOutput->Add(cutFlow_4mu);
  fOutput->Add(cutFlow_4e);
  fOutput->Add(cutFlow_2l2l);
  fOutput->Add(ElecCount);
  fOutput->Add(MuCBST);
  fOutput->Add(MuCBSTSA);
  fOutput->Add(MuCBSTSACA);
  fOutput->Add(TauCount);
  fOutput->Add(JetCount);
  fOutput->Add(MuReco_pT);
  fOutput->Add(MuReco_eta);
  fOutput->Add(MuTruth_pT);
  fOutput->Add(MuTruth_eta);
  fOutput->Add(ElReco_eT);
  fOutput->Add(ElReco_eta);
  fOutput->Add(ElTruth_eT);
  fOutput->Add(ElTruth_eta);
  fOutput->Add(MuEff_pT);
  fOutput->Add(MuEff_eta);
  fOutput->Add(ElEff_eT);
  fOutput->Add(ElEff_eta);
  fOutput->Add(h4lTree);
  
  /* Setting GRL xml file */
  GrlReader = new Root::TGoodRunsListReader();
  SetGRL();
  
  /* Setting PileUpReweightingTool */
  PileUpTool = new Root::TPileupReweighting( "PileupReweightingTool" );
  SetPileUpReweight();
  RetrieveLumiFractions();

  /* Setting Electron Energy Rescaler */
  El_ERescaler = new AtlasRoot::egammaEnergyCorrectionTool();
  SetElecEnergyRescaler();

  /* Setting Up MuonMomentumCorrections Tool*/
  MuSmear = new MuonSmear::SmearingClass();
  SetMuonMomentumCorrections();

  /* Setting Up MuonMomentumUncertainty Corrections Tool*/
  MuResoSCF = new Analysis::MuonResolutionAndMomentumScaleFactors(m_muResFile);

  /* Setting up MuonEfficiencyCorrections Tool */
  StacoSCF = new Analysis::AnalysisMuonConfigurableScaleFactors(m_SfDir,
								m_muType_staco,
								"MeV",
								m_confMu);
  
  StacoSASCF = new Analysis::AnalysisMuonConfigurableScaleFactors(m_SfDir,
								  m_muType_stacoSA,
								  "MeV",
								  m_confMu_SA);
  CaloSCF = new Analysis::AnalysisMuonConfigurableScaleFactors(m_SfDir,
							       m_muType_calo,
							       "MeV",
							       m_confMu);
  AddLumiPerPeriodToMuonEffCorr(StacoSCF);
  AddLumiPerPeriodToMuonEffCorr(StacoSASCF);
  AddLumiPerPeriodToMuonEffCorr(CaloSCF);
  SetMuonEfficiencyCorrections();

  /* Setting Up ElectronEfficiencyCorrections Tool */
  El_SCFID = new Root::TElectronEfficiencyCorrectionTool;
  El_SCFReco = new Root::TElectronEfficiencyCorrectionTool;
  SetElectronEfficiencyCorrections();

  /* Setting Electron Identification */
  SetElectronId();

  /* Setting Up Jet Calibration Tool */
  Print("H4lNtupleCreator:: Setting Jet Calibration Tool ...");
  bool isData=false;
  if(m_type==DATA){ isData=true; }
  JetCalib = new JetAnalysisCalib::JetCalibrationTool(m_JetAlgorithm,
						      m_JetCalibConf,
						      isData);

  /* Setting Electron CaloIsoCorrection */
  if(m_year==YEAR_2012)
    CaloIsoCorrection::SetPtLeakageCorrectionsFile(m_LeakCorrFile.c_str());

  /* Setting Particles */
  Part.GeneralConf(m_Debug,m_year,m_type,m_UseCorrections,
		   m_UseWeights,m_ParMinInfo,m_DirPrefix);
  Part.ConfigMuonTools(MuSmear,StacoSCF,StacoSASCF,CaloSCF,MuResoSCF);
  Part.ConfigMuons(6.,15.,1.,10.);
  Part.ConfigElecTools(El_ERescaler,El_SCFID,El_SCFReco,PileUpTool,
		       ElId2011,ElId2012);
  Part.ConfigElectrons(7.,10.);
  Part.ConfigTaus(15.);
  Part.ConfigJetTools(JetCalib);
  Part.ConfigJets(25.,30.);
    
  /* Trigger Matching for electrons */
  TrigNav_Elec   = new TriggerNavigationVariables();
  TrigMatch_Elec = new ElectronTriggerMatching(TrigNav_Elec);
  /* Trigger Matching for muons */
  TrigNav_SingleMu   = new TriggerNavigationVariables();
  TrigNav_DiMu       = new TriggerNavigationVariables();
  TrigMatch_SingleMu = new MuonTriggerMatching(TrigNav_SingleMu);
  TrigMatch_DiMu     = new MuonTriggerMatching(TrigNav_DiMu);
  
  /* Setting Trigger Scale Factors Tool */
  Print("H4lNtupleCreator:: Setting Trigger Scale Factors Tool ...");
  LeptonSF = new LeptonTriggerSF(m_TrigYear,m_TrigDir,m_TrigFile,
				 m_TrigElDir,m_TrigRecVr);

  /* Setting ZVertexReweighting Tool */
  Print("H4lNtupleCreator:: Setting Vertex Reweighting Tool ...");
  if(m_year==YEAR_2011){
    //if mc_cha_mumber 105200 (std::string("s1272"),m_VertDir);
    VertexRew = new VertexPositionReweightingTool(std::string("s1310"),m_VertDir);
  }
  else if(m_year==YEAR_2012){
    VertexRew = new VertexPositionReweightingTool(VertexPositionReweightingTool::MC12a,m_VertDir);
  }
  
  /* Setting Up FSR photon Correction */
  Print("H4lNtupleCreator:: Setting FSR Correction Tool ...");
  FSRph = new FsrPhotons();

  /* Setting Up TTileTripReader Tool */
  Print("H4lNtupleCreator:: Setting TTile Trip Reader Tool ...");
  TripRead = new Root::TTileTripReader("myTripReader");

  /* Setting Up MissingMassCalculato Tool */
  MMC = new MissingMassCalculator();
  SetMissMassCalc();
  
}

Bool_t h4l::Process(Long64_t entry){
  
  fChain->GetTree()->GetEntry(entry);

  //if(EventNumber!=32642) return false;
  Tprocessed++;
  
  if(!m_UseProof){
    SetTriggerNavigation(TrigNav_Elec,false,false);
    SetTriggerNavigation(TrigNav_SingleMu,m_smu1,m_dmu1);
    SetTriggerNavigation(TrigNav_DiMu,m_smu2,m_dmu2);
  }
  else{
    if(entry==0){
      SetTriggerNavigation(TrigNav_Elec,false,false);
      SetTriggerNavigation(TrigNav_SingleMu,m_smu1,m_dmu1);
      SetTriggerNavigation(TrigNav_DiMu,m_smu2,m_dmu2);
    }
  }
  
  if(m_Debug){
    std::cout<<"      Event ============>  "<<entry<<"  RunNumber = "
	     <<RunNumber <<"  EventNumber = "<<EventNumber<<std::endl;
  }
  else{
    if(entry%1000==0){
      std::cout<<"      Event ============>  "<<entry<<"  RunNumber = "
	       <<RunNumber<<std::endl;
    }
  }
  
  /*Skipping events with taus*/
  if(m_TauRemove){
    if( TauEventRemoval() ) return kFALSE;
  }
  
  /* Setting Event Numbers*/
  run        = RunNumber;
  event      = EventNumber;
  lumiblock  = lbn;
  LArErr     = larError;
  TileErr    = tileError;
  if(m_year==YEAR_2011){
    IntPerXing = averageIntPerXing;
  }
  else if(m_year==YEAR_2012){
    IntPerXing = (lbn==1&&int(averageIntPerXing+0.5)==1)?0.:averageIntPerXing;
  }
  event_shRhoKt4EM = Eventshape_rhoKt4EM;
  
  /* Get PileUp Weight and MCWeight */
  pileupweight = GetPileUpWeight();
  if(m_type==MC){
    mcweight = (mcevt_weight->at(0)).at(0);
    if(mc_event_weight<0) { TnegWeight++ ;}
    else{ TposWeight++; }
  }
  else{
    mcweight = 1.;
  }
  
  /* Get Vertex Reweighting */
  vxtweight = GetVertexWeight();
  
  /* GRL checks */ 
  isInGRL = GRLCheck(run,lumiblock);
  
  /* LarError */
  isLArError = LarErrorCheck(LArErr);
  
  /* TileError */
  isTileError = TileErrorCheck(TileErr);
  
  /* Bad Events Check */
  isBadEvent = BadEventCheck();

  /* Tile Corruption Check */
  isTileCorrupted = TileCorruptionCheck(run,lumiblock,event);

  /* Detecting Period*/
  Period = DetectPeriod(run);

  /* Vertex Check */
  VertexCheck();
  
  /* Trigger Information checks */
  TriggerCheck(Period);
  
  /* Retrieve Missing Energy Variables*/
  MissingEnergy();

  /* Retrieve Photon Information */
  PhotonsInformation();
  
  /* HforTool: Cheching bb,cc events overlap*/
  if(m_type==MC){
    MCEventOverlap(hforTool,mc_channel_number);
    
    MCchanNum = mc_channel_number;
    if( mc_channel_number==167220 ||
	mc_channel_number==167222 ||
	mc_channel_number==160155 ||
	mc_channel_number==167225  ){
      
      if( TauEventRemoval() ){
	MCxsec = GetCrossSection(m_channel,mc_channel_number,m_year,"2mu2e");
      }
      else{
	MCxsec = 0.5*GetCrossSection(m_channel,mc_channel_number,m_year,"2mu2e");
      }
      TrueMCxsec = GetCrossSection(m_channel,mc_channel_number,m_year,"2mu2e");
      
    }
    else{
      MCxsec = GetCrossSection(m_channel,mc_channel_number,m_year,"2mu2e");
      TrueMCxsec = GetCrossSection(m_channel,mc_channel_number,m_year,"2mu2e");
    }
    PrintOut("DEBUG::H4lNtupleCreator:: MC sample "); PrintOut(m_channel);
    PrintOut(" ("); PrintOut((int)mc_channel_number); PrintOut(")  = ");
    PrintOut(MCxsec); PrintEndl();
  }

  /* Checking CB,Tag and SA muons from Staco Collection*/
  Part.ResetMuonQualityFlags();
  Part.CheckingStacoMuons(isVertexGood,isTrigGood);

  /* Check ST/SA muons Overlap */
  Part.CheckSTSAmuonOverlap();
  Part.ClearStacoMuonVec();

  /* Checking CALO muons from Calo Collection*/
  Part.ResetMuonQualityFlags();
  Part.CheckingCaloMuons(isVertexGood,isTrigGood);

  /* Checking electrons */
  Part.ResetElectronQualityFlags();
  Part.CheckingElectrons(isVertexGood,isTrigGood,nPV);

  /* Checking jets */
  Part.ResetJetQualityFlags();
  Part.CheckingJets(isVertexGood,isTrigGood,IntPerXing,nPV);

  /* Fill Muons Info */
  Part.FillMuonsInfo(isVertexGood,isTrigGood);

  /* Checking taus */
  Part.ResetTauQualityFlags();
  Part.CheckingTaus(isVertexGood,isTrigGood);
  
  /* Retrieve Particle and Muons/Electrons Vectors */
  LoadParticleVectors();
  if(!m_ParMinInfo){
    LoadMuonVectors();
    LoadElectronVectors();
    LoadTauVectors();
  }
  
  /* Clean Muon Algorithm Vectors */
  Part.ClearCBSTSAMuonVec();
  Part.ClearCaloMuonVec();

  /* Calculate Lepton Efficiencies */
  if( m_type==MC && 
      m_channel.find("ggH")!=string::npos ){
    LeptonHistForEfficiencies();
  }
  
  nelecsel = Part.getNelecSel();
  nmusel   = Part.getNmuSel();
  ntausel  = Part.getNtauSel();
  njetsel  = Part.getNjetSel();
  
  if(isTrigGood && isVertexGood){
    nelectrons += nelecsel;
    nmuons     += nmusel;
    ntaus      += ntausel;
    njets      += njetsel;
  }
  
  ResetEventFlags();
  isAllPassedPos   = -1;
  isAllPassed      = false;
  isKeepEvent      = false;
  
  if( nmusel>3 || nelecsel>3   ||
      (nmusel>1 && nelecsel>1) ||
      (nmusel>1 && ntausel>1)  ||
      (nelecsel>1 && ntausel>1) ){ 
    isGood4lCandidate=true;
    
    /* Retrieving Quadruplets */
    Quadruplets = SelectQuadruplets();
    
    /* Check Muons D0 */
    IsD0Good = CheckD0Cut();

    /* Checking Kinematics */
    IsKinematicGood = CheckKinematic();
    
    /* Checking Trigger Match */
    CheckTriggerMatch(TrigMatch_SingleMu,TrigMatch_DiMu,
		      TrigMatch_Elec,Period);
        
    /* Computing Event Trigger Scale Factor */
    TrigScaleFact = ComputeTriggerScaleFactor(LeptonSF);
    
    /* Computing Event Efficiency Scale Factor */
    EffyScaleFact = ComputeEfficiencyScaleFactor();
    
    /* Calculating Z1, Z2 and H masses */
    Z1Mass      = ComputeZ1Mass();
    Z2Mass      = ComputeZ2Mass();
    Z1MMCMass   = ComputeZ1MMCMass();
    Z2MMCMass   = ComputeZ2MMCMass();
    FourLepMass = Mass4Lep();
    
    /* Choosing Best Quadruplet */
    IsBestQuadruplet = FindBestQuadruplet();
    
    /* Checking Z1 Mass */
    IsZ1MassGood = Z1MassCut();

    /* Checking Z2 Mass */
    IsZ2MassGood = Z2MassCut();

    /* Checking DeltaR */
    IsDeltaRGood = CheckDeltaR();
    
    /* Checking J/Psi Veto */
    IsJpsiVetoGood = CheckJpsiVeto();
  
    /* Checking Track Isolation */
    IsTrackIsoGood = TrackIsoCut(true);
    
    /* Checking Calo Isolation */
    IsCaloIsoGood = CaloIsoCut(true);

    /* Checking D0 Significance */
    IsD0SignGood = D0SignificanceCut();
    
    /* Appling FSR Correction */
    //ApplyFSRCorrection();
    
    /* Appling ZMass Constraint */
    //ApplyZMassConstraint();
    
    /* Compute Angular Variables */
    //ComputeAngularVariables();
    
    /* Retrieve MC Information */
    if(m_type==MC){
      MCInfo();
      TauTruthMatch();
      QuadMisPairing   = MisPairing();
      QuadMisPairLabel = MisPairLabel();
      isKeepEvent      = MCSampleOverlap(mc_channel_number);
    }
    
    /* Retrieve Best Quadruplet Position */
    isAllPassedPos = GetGoodQuadrupletPosition();
    if(isAllPassedPos!=-1){
      isAllPassed = true;
      if( m_channel.find("JHU")!=std::string::npos         || 
	  (m_channel.find("Powheg")!=std::string::npos  &&
	   m_channel.find("_ZZ_")!=std::string::npos     ) ||
	  ( m_channel.find("Powheg")!=std::string::npos &&
	    m_channel.find("ggH")!=std::string::npos     )  ){
	isMisPairing = CheckMisPairing(isAllPassedPos);
	isMisPairLabel = CheckMisPairingLabeling(isAllPassedPos);
      }
    }
  }
  
  CutFlow();
  if(isGood4lCandidate){
    h4lTree->Fill();
  }
  
  /* cleaning vectors*/
  Part.ClearMuonVec();
  Part.ClearElecVec();
  Part.ClearJetVec();
  Part.ClearTauVec();
  Part.ClearParticleVec();
  ClearVectors();
  
  return kTRUE;
  
}

void h4l::SlaveTerminate(){
  
  Print("H4lNtupleCreator:: Writing Outputs ...");
  if(!m_UseProof){
    newfile->cd();
  }
  CreateInfoHistograms();
  if( m_type==MC && 
      m_channel.find("ggH")!=string::npos ){
    LeptonEfficiencies();
  }
  WriteElectronCounts();
  WriteMuonCounts();
  WriteTauCounts();
  WriteJetCounts();
  WriteCutFlow();
  if(!m_UseProof){
    h4lTree->Write();
  }

  std::cout<<"Number of tau-tau overlap "<<Part.getTauTauOverlap()<<std::endl;
  
  Print("H4lNtupleCreator:: Closing Output File ...!");
  if(!m_UseProof){
    newfile->Close();
  }
  Info.close();
  Part.Clean();

  Print("H4lNtupleCreator:: Deleting objects ...");
  delete PileUpTool;
  delete El_ERescaler;
  delete MuSmear;
  delete MuResoSCF;
  delete StacoSCF;
  delete StacoSASCF;
  delete CaloSCF;
  delete El_SCFID;
  delete El_SCFReco;
  delete LeptonSF;
  delete VertexRew;
  delete TripRead;
  delete GrlReader;
  //std::cout<<"deleting ElId2011"<<std::endl;
  //delete ElId2011;
  delete ElId2012;
  delete JetCalib;
  //std::cout<<"deleting LeptonSF"<<std::endl;
  //delete LeptonSF;
  delete MMC;
  std::cout<<"H4lNtupleCreator:: Executed successfully!!"<<std::endl;
  
}

void h4l::Terminate(){
  
}

void h4l::Init(TTree *tree){
  
  SetPointerGeneralVar();
  SetPointerMuonVar();
  SetPointerMuonTrigMatchVar();
  SetPointerElectronVar();
  SetPointerElectronTrigMatchVar();
  SetPointerJetVar();
  SetPointerPhotonVar();
  SetPointerTauVar();
  if (!tree) return;
  fChain = tree;
  fChain->SetMakeClass(1);
  SetBranchGeneralVar(fChain,m_type,m_year,m_UseMinVar);
  SetBranchMuonVar(fChain,m_type,m_year,m_UseMinVar);
  SetBranchElectronVar(fChain,m_type,m_year,m_UseMinVar);
  SetBranchJetVar(fChain,m_type,m_year,m_UseMinVar);
  SetBranchPhotonVar(fChain,m_type,m_UseMinVar);
  SetBranchTauVar(fChain,m_type,m_UseMinVar);
  SetBranchMuonTrigVar(fChain,m_year);
  SetBranchMuonTrigMatchVar(fChain,m_year);
  SetBranchElectronTrigVar(fChain,m_year);
  SetBranchElectronTrigMatchVar(fChain);
  
}

Bool_t h4l::Notify(){
   
  return kTRUE;
   
}
