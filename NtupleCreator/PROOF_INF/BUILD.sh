#!/bin/bash

unamestr=`uname -n`
if [ "$unamestr" == 'macpro' ]; then
    echo "Using macpro host!!"
    python packagesCompile.py
    make -f Makefile.MacPro
else
    echo "Using  CERN based machine!!"
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc5-gcc43-opt/17.7.0/sw/lcg/external/clhep/1.9.4.7/x86_64-slc5-gcc43-opt/lib/
    python packagesCompile.py
    make
fi
