#!/usr/bin/python
import os
import sys
from packagesDef import *

#Clean packages
print "\n"
for pack in Packages:

    if pack[0]=="egammaEvent":
        continue
    print "     Cleaning package "+pack[0]
    os.chdir(PackagesDir+pack[0]+"/cmt")
    retval = os.getcwd()

    print "Directory changed successfully %s" % retval
    
    os.system("make clean -f "+Makefile)
    print "  Package "+pack[0]+" cleaned!!"
    os.chdir("../../../")
    retval = os.getcwd()

    print "Directory changed successfully %s" % retval

    print ""
    
print "PackageClean:: Everything Done!!"
