#ifndef DPDJetVariables_h
#define DPDJetVariables_h
#include "TTree.h"
#include <vector>
#include <string>

void SetBranchJetVar(TTree*,int,int,bool minimal=false);

void SetPointerJetVar();

/* List of Jet DPD Variables */
Int_t           jet_akt4topoem_n;
vector<float>   *jet_akt4topoem_E;
vector<float>   *jet_akt4topoem_pt;
vector<float>   *jet_akt4topoem_m;
vector<float>   *jet_akt4topoem_eta;
vector<float>   *jet_akt4topoem_phi;
vector<float>   *jet_akt4topoem_EtaOrigin;
vector<float>   *jet_akt4topoem_PhiOrigin;
vector<float>   *jet_akt4topoem_MOrigin;
vector<float>   *jet_akt4topoem_WIDTH;
vector<float>   *jet_akt4topoem_n90;
vector<float>   *jet_akt4topoem_Timing;
vector<float>   *jet_akt4topoem_LArQuality;
vector<float>   *jet_akt4topoem_nTrk;
vector<float>   *jet_akt4topoem_sumPtTrk;
vector<float>   *jet_akt4topoem_OriginIndex;
vector<float>   *jet_akt4topoem_HECQuality;
vector<float>   *jet_akt4topoem_NegativeE;
vector<float>   *jet_akt4topoem_AverageLArQF;
vector<float>   *jet_akt4topoem_BCH_CORR_CELL;
vector<float>   *jet_akt4topoem_BCH_CORR_DOTX;
vector<float>   *jet_akt4topoem_BCH_CORR_JET;
vector<float>   *jet_akt4topoem_BCH_CORR_JET_FORCELL;
vector<float>   *jet_akt4topoem_ENG_BAD_CELLS;
vector<float>   *jet_akt4topoem_N_BAD_CELLS;
vector<float>   *jet_akt4topoem_N_BAD_CELLS_CORR;
vector<float>   *jet_akt4topoem_BAD_CELLS_CORR_E;
vector<float>   *jet_akt4topoem_NumTowers;
vector<float>   *jet_akt4topoem_ootFracCells5;
vector<float>   *jet_akt4topoem_ootFracCells10;
vector<float>   *jet_akt4topoem_ootFracClusters5;
vector<float>   *jet_akt4topoem_ootFracClusters10;
vector<int>     *jet_akt4topoem_SamplingMax;
vector<float>   *jet_akt4topoem_fracSamplingMax;
vector<float>   *jet_akt4topoem_hecf;
vector<float>   *jet_akt4topoem_tgap3f;
vector<int>     *jet_akt4topoem_isUgly;
vector<int>     *jet_akt4topoem_isBadLooseMinus;
vector<int>     *jet_akt4topoem_isBadLoose;
vector<int>     *jet_akt4topoem_isBadMedium;
vector<int>     *jet_akt4topoem_isBadTight;
vector<float>   *jet_akt4topoem_emfrac;
vector<float>   *jet_akt4topoem_Offset;
vector<float>   *jet_akt4topoem_EMJES;
vector<float>   *jet_akt4topoem_EMJES_EtaCorr;
vector<float>   *jet_akt4topoem_EMJESnooffset;
vector<float>   *jet_akt4topoem_LCJES;
vector<float>   *jet_akt4topoem_LCJES_EtaCorr;
vector<float>   *jet_akt4topoem_emscale_E;
vector<float>   *jet_akt4topoem_emscale_pt;
vector<float>   *jet_akt4topoem_emscale_m;
vector<float>   *jet_akt4topoem_emscale_eta;
vector<float>   *jet_akt4topoem_emscale_phi;
vector<float>   *jet_akt4topoem_ActiveArea;
vector<float>   *jet_akt4topoem_ActiveAreaPx;
vector<float>   *jet_akt4topoem_ActiveAreaPy;
vector<float>   *jet_akt4topoem_ActiveAreaPz;
vector<float>   *jet_akt4topoem_ActiveAreaE;
vector<float>   *jet_akt4topoem_jvtxf;
vector<vector<float> > *jet_akt4topoem_jvtxfFull;
vector<float>   *jet_akt4topoem_jvtx_x;
vector<float>   *jet_akt4topoem_jvtx_y;
vector<float>   *jet_akt4topoem_jvtx_z;
vector<float>   *jet_akt4topoem_TruthMFindex;
vector<float>   *jet_akt4topoem_TruthMF;
vector<float>   *jet_akt4topoem_GSCFactorF;
vector<float>   *jet_akt4topoem_WidthFraction;
vector<float>   *jet_akt4topoem_e_PreSamplerB;
vector<float>   *jet_akt4topoem_e_EMB1;
vector<float>   *jet_akt4topoem_e_EMB2;
vector<float>   *jet_akt4topoem_e_EMB3;
vector<float>   *jet_akt4topoem_e_PreSamplerE;
vector<float>   *jet_akt4topoem_e_EME1;
vector<float>   *jet_akt4topoem_e_EME2;
vector<float>   *jet_akt4topoem_e_EME3;
vector<float>   *jet_akt4topoem_e_HEC0;
vector<float>   *jet_akt4topoem_e_HEC1;
vector<float>   *jet_akt4topoem_e_HEC2;
vector<float>   *jet_akt4topoem_e_HEC3;
vector<float>   *jet_akt4topoem_e_TileBar0;
vector<float>   *jet_akt4topoem_e_TileBar1;
vector<float>   *jet_akt4topoem_e_TileBar2;
vector<float>   *jet_akt4topoem_e_TileGap1;
vector<float>   *jet_akt4topoem_e_TileGap2;
vector<float>   *jet_akt4topoem_e_TileGap3;
vector<float>   *jet_akt4topoem_e_TileExt0;
vector<float>   *jet_akt4topoem_e_TileExt1;
vector<float>   *jet_akt4topoem_e_TileExt2;
vector<float>   *jet_akt4topoem_e_FCAL0;
vector<float>   *jet_akt4topoem_e_FCAL1;
vector<float>   *jet_akt4topoem_e_FCAL2;
vector<int>     *jet_akt4topoem_Nconst;
vector<vector<float> > *jet_akt4topoem_ptconst_default;
vector<vector<float> > *jet_akt4topoem_econst_default;
vector<vector<float> > *jet_akt4topoem_etaconst_default;
vector<vector<float> > *jet_akt4topoem_phiconst_default;
vector<vector<float> > *jet_akt4topoem_weightconst_default;
vector<float>   *jet_akt4topoem_flavor_weight_Comb;
vector<float>   *jet_akt4topoem_flavor_weight_IP2D;
vector<float>   *jet_akt4topoem_flavor_weight_IP3D;
vector<float>   *jet_akt4topoem_flavor_weight_SV0;
vector<float>   *jet_akt4topoem_flavor_weight_SV1;
vector<float>   *jet_akt4topoem_flavor_weight_SV2;
vector<float>   *jet_akt4topoem_flavor_weight_SoftMuonTagChi2;
vector<float>   *jet_akt4topoem_flavor_weight_SecondSoftMuonTagChi2;
vector<float>   *jet_akt4topoem_flavor_weight_JetFitterTagNN;
vector<float>   *jet_akt4topoem_flavor_weight_JetFitterCOMBNN;
vector<float>   *jet_akt4topoem_flavor_weight_MV1;
vector<float>   *jet_akt4topoem_flavor_weight_MV2;
vector<float>   *jet_akt4topoem_flavor_weight_GbbNN;
vector<float>   *jet_akt4topoem_flavor_weight_JetFitterCharm;
vector<float>   *jet_akt4topoem_flavor_weight_MV3_bVSu;
vector<float>   *jet_akt4topoem_flavor_weight_MV3_bVSc;
vector<float>   *jet_akt4topoem_flavor_weight_MV3_cVSu;
vector<int>     *jet_akt4topoem_flavor_truth_label;
vector<float>   *jet_akt4topoem_flavor_truth_dRminToB;
vector<float>   *jet_akt4topoem_flavor_truth_dRminToC;
vector<float>   *jet_akt4topoem_flavor_truth_dRminToT;
vector<int>     *jet_akt4topoem_flavor_truth_BHadronpdg;
vector<float>   *jet_akt4topoem_flavor_truth_vx_x;
vector<float>   *jet_akt4topoem_flavor_truth_vx_y;
vector<float>   *jet_akt4topoem_flavor_truth_vx_z;
vector<vector<string> > *jet_akt4topoem_flavor_component_jfitc_doublePropName;
vector<vector<double> > *jet_akt4topoem_flavor_component_jfitc_doublePropValue;
vector<vector<string> > *jet_akt4topoem_flavor_component_jfitc_intPropName;
vector<vector<int> > *jet_akt4topoem_flavor_component_jfitc_intPropValue;
vector<float>   *jet_akt4topoem_flavor_component_jfitc_pu;
vector<float>   *jet_akt4topoem_flavor_component_jfitc_pb;
vector<float>   *jet_akt4topoem_flavor_component_jfitc_pc;
vector<int>     *jet_akt4topoem_flavor_component_jfitc_isValid;
vector<float>   *jet_akt4topoem_el_dr;
vector<int>     *jet_akt4topoem_el_matched;
vector<float>   *jet_akt4topoem_mu_dr;
vector<int>     *jet_akt4topoem_mu_matched;
vector<float>   *jet_akt4topoem_L1_dr;
vector<int>     *jet_akt4topoem_L1_matched;
vector<float>   *jet_akt4topoem_L2_dr;
vector<int>     *jet_akt4topoem_L2_matched;
vector<float>   *jet_akt4topoem_EF_dr;
vector<int>     *jet_akt4topoem_EF_matched;
vector<float>   *jet_akt4topoem_nTrk_pv0_1GeV;
vector<float>   *jet_akt4topoem_sumPtTrk_pv0_1GeV;
vector<float>   *jet_akt4topoem_nTrk_allpv_1GeV;
vector<float>   *jet_akt4topoem_sumPtTrk_allpv_1GeV;
vector<float>   *jet_akt4topoem_nTrk_pv0_500MeV;
vector<float>   *jet_akt4topoem_sumPtTrk_pv0_500MeV;
vector<float>   *jet_akt4topoem_trackWIDTH_pv0_1GeV;
vector<float>   *jet_akt4topoem_trackWIDTH_allpv_1GeV;
Int_t           jet_antikt4truth_n;
vector<float>   *jet_antikt4truth_E;
vector<float>   *jet_antikt4truth_pt;
vector<float>   *jet_antikt4truth_m;
vector<float>   *jet_antikt4truth_eta;
vector<float>   *jet_antikt4truth_phi;
vector<float>   *jet_antikt4truth_EtaOrigin;
vector<float>   *jet_antikt4truth_PhiOrigin;
vector<float>   *jet_antikt4truth_MOrigin;
vector<float>   *jet_antikt4truth_WIDTH;
vector<float>   *jet_antikt4truth_n90;
vector<float>   *jet_antikt4truth_Timing;
vector<float>   *jet_antikt4truth_LArQuality;
vector<float>   *jet_antikt4truth_nTrk;
vector<float>   *jet_antikt4truth_sumPtTrk;
vector<float>   *jet_antikt4truth_OriginIndex;
vector<float>   *jet_antikt4truth_HECQuality;
vector<float>   *jet_antikt4truth_NegativeE;
vector<float>   *jet_antikt4truth_AverageLArQF;
vector<float>   *jet_antikt4truth_BCH_CORR_CELL;
vector<float>   *jet_antikt4truth_BCH_CORR_DOTX;
vector<float>   *jet_antikt4truth_BCH_CORR_JET;
vector<float>   *jet_antikt4truth_BCH_CORR_JET_FORCELL;
vector<float>   *jet_antikt4truth_ENG_BAD_CELLS;
vector<float>   *jet_antikt4truth_N_BAD_CELLS;
vector<float>   *jet_antikt4truth_N_BAD_CELLS_CORR;
vector<float>   *jet_antikt4truth_BAD_CELLS_CORR_E;
vector<float>   *jet_antikt4truth_NumTowers;
vector<float>   *jet_antikt4truth_ootFracCells5;
vector<float>   *jet_antikt4truth_ootFracCells10;
vector<float>   *jet_antikt4truth_ootFracClusters5;
vector<float>   *jet_antikt4truth_ootFracClusters10;
vector<int>     *jet_antikt4truth_SamplingMax;
vector<float>   *jet_antikt4truth_fracSamplingMax;
vector<float>   *jet_antikt4truth_hecf;
vector<float>   *jet_antikt4truth_tgap3f;
vector<int>     *jet_antikt4truth_isUgly;
vector<int>     *jet_antikt4truth_isBadLooseMinus;
vector<int>     *jet_antikt4truth_isBadLoose;
vector<int>     *jet_antikt4truth_isBadMedium;
vector<int>     *jet_antikt4truth_isBadTight;
vector<float>   *jet_antikt4truth_emfrac;
vector<float>   *jet_antikt4truth_Offset;
vector<float>   *jet_antikt4truth_EMJES;
vector<float>   *jet_antikt4truth_EMJES_EtaCorr;
vector<float>   *jet_antikt4truth_EMJESnooffset;
vector<float>   *jet_antikt4truth_LCJES;
vector<float>   *jet_antikt4truth_LCJES_EtaCorr;
vector<float>   *jet_antikt4truth_emscale_E;
vector<float>   *jet_antikt4truth_emscale_pt;
vector<float>   *jet_antikt4truth_emscale_m;
vector<float>   *jet_antikt4truth_emscale_eta;
vector<float>   *jet_antikt4truth_emscale_phi;
vector<float>   *jet_antikt4truth_ActiveArea;
vector<float>   *jet_antikt4truth_ActiveAreaPx;
vector<float>   *jet_antikt4truth_ActiveAreaPy;
vector<float>   *jet_antikt4truth_ActiveAreaPz;
vector<float>   *jet_antikt4truth_ActiveAreaE;
vector<float>   *jet_antikt4truth_el_dr;
vector<int>     *jet_antikt4truth_el_matched;
vector<float>   *jet_antikt4truth_mu_dr;
vector<int>     *jet_antikt4truth_mu_matched;
vector<float>   *jet_antikt4truth_L1_dr;
vector<int>     *jet_antikt4truth_L1_matched;
vector<float>   *jet_antikt4truth_L2_dr;
vector<int>     *jet_antikt4truth_L2_matched;
vector<float>   *jet_antikt4truth_EF_dr;
vector<int>     *jet_antikt4truth_EF_matched;

/*List of branches*/
TBranch        *b_jet_akt4topoem_n;   //!
TBranch        *b_jet_akt4topoem_E;   //!
TBranch        *b_jet_akt4topoem_pt;   //!
TBranch        *b_jet_akt4topoem_m;   //!
TBranch        *b_jet_akt4topoem_eta;   //!
TBranch        *b_jet_akt4topoem_phi;   //!
TBranch        *b_jet_akt4topoem_EtaOrigin;   //!
TBranch        *b_jet_akt4topoem_PhiOrigin;   //!
TBranch        *b_jet_akt4topoem_MOrigin;   //!
TBranch        *b_jet_akt4topoem_WIDTH;   //!
TBranch        *b_jet_akt4topoem_n90;   //!
TBranch        *b_jet_akt4topoem_Timing;   //!
TBranch        *b_jet_akt4topoem_LArQuality;   //!
TBranch        *b_jet_akt4topoem_nTrk;   //!
TBranch        *b_jet_akt4topoem_sumPtTrk;   //!
TBranch        *b_jet_akt4topoem_OriginIndex;   //!
TBranch        *b_jet_akt4topoem_HECQuality;   //!
TBranch        *b_jet_akt4topoem_NegativeE;   //!
TBranch        *b_jet_akt4topoem_AverageLArQF;   //!
TBranch        *b_jet_akt4topoem_BCH_CORR_CELL;   //!
TBranch        *b_jet_akt4topoem_BCH_CORR_DOTX;   //!
TBranch        *b_jet_akt4topoem_BCH_CORR_JET;   //!
TBranch        *b_jet_akt4topoem_BCH_CORR_JET_FORCELL;   //!
TBranch        *b_jet_akt4topoem_ENG_BAD_CELLS;   //!
TBranch        *b_jet_akt4topoem_N_BAD_CELLS;   //!
TBranch        *b_jet_akt4topoem_N_BAD_CELLS_CORR;   //!
TBranch        *b_jet_akt4topoem_BAD_CELLS_CORR_E;   //!
TBranch        *b_jet_akt4topoem_NumTowers;   //!
TBranch        *b_jet_akt4topoem_ootFracCells5;   //!
TBranch        *b_jet_akt4topoem_ootFracCells10;   //!
TBranch        *b_jet_akt4topoem_ootFracClusters5;   //!
TBranch        *b_jet_akt4topoem_ootFracClusters10;   //!
TBranch        *b_jet_akt4topoem_SamplingMax;   //!
TBranch        *b_jet_akt4topoem_fracSamplingMax;   //!
TBranch        *b_jet_akt4topoem_hecf;   //!
TBranch        *b_jet_akt4topoem_tgap3f;   //!
TBranch        *b_jet_akt4topoem_isUgly;   //!
TBranch        *b_jet_akt4topoem_isBadLooseMinus;   //!
TBranch        *b_jet_akt4topoem_isBadLoose;   //!
TBranch        *b_jet_akt4topoem_isBadMedium;   //!
TBranch        *b_jet_akt4topoem_isBadTight;   //!
TBranch        *b_jet_akt4topoem_emfrac;   //!
TBranch        *b_jet_akt4topoem_Offset;   //!
TBranch        *b_jet_akt4topoem_EMJES;   //!
TBranch        *b_jet_akt4topoem_EMJES_EtaCorr;   //!
TBranch        *b_jet_akt4topoem_EMJESnooffset;   //!
TBranch        *b_jet_akt4topoem_LCJES;   //!
TBranch        *b_jet_akt4topoem_LCJES_EtaCorr;   //!
TBranch        *b_jet_akt4topoem_emscale_E;   //!
TBranch        *b_jet_akt4topoem_emscale_pt;   //!
TBranch        *b_jet_akt4topoem_emscale_m;   //!
TBranch        *b_jet_akt4topoem_emscale_eta;   //!
TBranch        *b_jet_akt4topoem_emscale_phi;   //!
TBranch        *b_jet_akt4topoem_ActiveArea;   //!
TBranch        *b_jet_akt4topoem_ActiveAreaPx;   //!
TBranch        *b_jet_akt4topoem_ActiveAreaPy;   //!
TBranch        *b_jet_akt4topoem_ActiveAreaPz;   //!
TBranch        *b_jet_akt4topoem_ActiveAreaE;   //!
TBranch        *b_jet_akt4topoem_jvtxf;   //!
TBranch        *b_jet_akt4topoem_jvtxfFull;   //!
TBranch        *b_jet_akt4topoem_jvtx_x;   //!
TBranch        *b_jet_akt4topoem_jvtx_y;   //!
TBranch        *b_jet_akt4topoem_jvtx_z;   //!
TBranch        *b_jet_akt4topoem_TruthMFindex;   //!
TBranch        *b_jet_akt4topoem_TruthMF;   //!
TBranch        *b_jet_akt4topoem_GSCFactorF;   //!
TBranch        *b_jet_akt4topoem_WidthFraction;   //!
TBranch        *b_jet_akt4topoem_e_PreSamplerB;   //!
TBranch        *b_jet_akt4topoem_e_EMB1;   //!
TBranch        *b_jet_akt4topoem_e_EMB2;   //!
TBranch        *b_jet_akt4topoem_e_EMB3;   //!
TBranch        *b_jet_akt4topoem_e_PreSamplerE;   //!
TBranch        *b_jet_akt4topoem_e_EME1;   //!
TBranch        *b_jet_akt4topoem_e_EME2;   //!
TBranch        *b_jet_akt4topoem_e_EME3;   //!
TBranch        *b_jet_akt4topoem_e_HEC0;   //!
TBranch        *b_jet_akt4topoem_e_HEC1;   //!
TBranch        *b_jet_akt4topoem_e_HEC2;   //!
TBranch        *b_jet_akt4topoem_e_HEC3;   //!
TBranch        *b_jet_akt4topoem_e_TileBar0;   //!
TBranch        *b_jet_akt4topoem_e_TileBar1;   //!
TBranch        *b_jet_akt4topoem_e_TileBar2;   //!
TBranch        *b_jet_akt4topoem_e_TileGap1;   //!
TBranch        *b_jet_akt4topoem_e_TileGap2;   //!
TBranch        *b_jet_akt4topoem_e_TileGap3;   //!
TBranch        *b_jet_akt4topoem_e_TileExt0;   //!
TBranch        *b_jet_akt4topoem_e_TileExt1;   //!
TBranch        *b_jet_akt4topoem_e_TileExt2;   //!
TBranch        *b_jet_akt4topoem_e_FCAL0;   //!
TBranch        *b_jet_akt4topoem_e_FCAL1;   //!
TBranch        *b_jet_akt4topoem_e_FCAL2;   //!
TBranch        *b_jet_akt4topoem_Nconst;   //!
TBranch        *b_jet_akt4topoem_ptconst_default;   //!
TBranch        *b_jet_akt4topoem_econst_default;   //!
TBranch        *b_jet_akt4topoem_etaconst_default;   //!
TBranch        *b_jet_akt4topoem_phiconst_default;   //!
TBranch        *b_jet_akt4topoem_weightconst_default;   //!
TBranch        *b_jet_akt4topoem_flavor_weight_Comb;   //!
TBranch        *b_jet_akt4topoem_flavor_weight_IP2D;   //!
TBranch        *b_jet_akt4topoem_flavor_weight_IP3D;   //!
TBranch        *b_jet_akt4topoem_flavor_weight_SV0;   //!
TBranch        *b_jet_akt4topoem_flavor_weight_SV1;   //!
TBranch        *b_jet_akt4topoem_flavor_weight_SV2;   //!
TBranch        *b_jet_akt4topoem_flavor_weight_SoftMuonTagChi2;   //!
TBranch        *b_jet_akt4topoem_flavor_weight_SecondSoftMuonTagChi2;   //!
TBranch        *b_jet_akt4topoem_flavor_weight_JetFitterTagNN;   //!
TBranch        *b_jet_akt4topoem_flavor_weight_JetFitterCOMBNN;   //!
TBranch        *b_jet_akt4topoem_flavor_weight_MV1;   //!
TBranch        *b_jet_akt4topoem_flavor_weight_MV2;   //!
TBranch        *b_jet_akt4topoem_flavor_weight_GbbNN;   //!
TBranch        *b_jet_akt4topoem_flavor_weight_JetFitterCharm;   //!
TBranch        *b_jet_akt4topoem_flavor_weight_MV3_bVSu;   //!
TBranch        *b_jet_akt4topoem_flavor_weight_MV3_bVSc;   //!
TBranch        *b_jet_akt4topoem_flavor_weight_MV3_cVSu;   //!
TBranch        *b_jet_akt4topoem_flavor_truth_label;   //!
TBranch        *b_jet_akt4topoem_flavor_truth_dRminToB;   //!
TBranch        *b_jet_akt4topoem_flavor_truth_dRminToC;   //!
TBranch        *b_jet_akt4topoem_flavor_truth_dRminToT;   //!
TBranch        *b_jet_akt4topoem_flavor_truth_BHadronpdg;   //!
TBranch        *b_jet_akt4topoem_flavor_truth_vx_x;   //!
TBranch        *b_jet_akt4topoem_flavor_truth_vx_y;   //!
TBranch        *b_jet_akt4topoem_flavor_truth_vx_z;   //!
TBranch        *b_jet_akt4topoem_flavor_component_jfitc_doublePropName;   //!
TBranch        *b_jet_akt4topoem_flavor_component_jfitc_doublePropValue;   //!
TBranch        *b_jet_akt4topoem_flavor_component_jfitc_intPropName;   //!
TBranch        *b_jet_akt4topoem_flavor_component_jfitc_intPropValue;   //!
TBranch        *b_jet_akt4topoem_flavor_component_jfitc_pu;   //!
TBranch        *b_jet_akt4topoem_flavor_component_jfitc_pb;   //!
TBranch        *b_jet_akt4topoem_flavor_component_jfitc_pc;   //!
TBranch        *b_jet_akt4topoem_flavor_component_jfitc_isValid;   //!
TBranch        *b_jet_akt4topoem_el_dr;   //!
TBranch        *b_jet_akt4topoem_el_matched;   //!
TBranch        *b_jet_akt4topoem_mu_dr;   //!
TBranch        *b_jet_akt4topoem_mu_matched;   //!
TBranch        *b_jet_akt4topoem_L1_dr;   //!
TBranch        *b_jet_akt4topoem_L1_matched;   //!
TBranch        *b_jet_akt4topoem_L2_dr;   //!
TBranch        *b_jet_akt4topoem_L2_matched;   //!
TBranch        *b_jet_akt4topoem_EF_dr;   //!
TBranch        *b_jet_akt4topoem_EF_matched;   //!
TBranch        *b_jet_akt4topoem_nTrk_pv0_1GeV;   //!
TBranch        *b_jet_akt4topoem_sumPtTrk_pv0_1GeV;   //!
TBranch        *b_jet_akt4topoem_nTrk_allpv_1GeV;   //!
TBranch        *b_jet_akt4topoem_sumPtTrk_allpv_1GeV;   //!
TBranch        *b_jet_akt4topoem_nTrk_pv0_500MeV;   //!
TBranch        *b_jet_akt4topoem_sumPtTrk_pv0_500MeV;   //!
TBranch        *b_jet_akt4topoem_trackWIDTH_pv0_1GeV;   //!
TBranch        *b_jet_akt4topoem_trackWIDTH_allpv_1GeV;   //!
TBranch        *b_jet_antikt4truth_n;   //!
TBranch        *b_jet_antikt4truth_E;   //!
TBranch        *b_jet_antikt4truth_pt;   //!
TBranch        *b_jet_antikt4truth_m;   //!
TBranch        *b_jet_antikt4truth_eta;   //!
TBranch        *b_jet_antikt4truth_phi;   //!
TBranch        *b_jet_antikt4truth_EtaOrigin;   //!
TBranch        *b_jet_antikt4truth_PhiOrigin;   //!
TBranch        *b_jet_antikt4truth_MOrigin;   //!
TBranch        *b_jet_antikt4truth_WIDTH;   //!
TBranch        *b_jet_antikt4truth_n90;   //!
TBranch        *b_jet_antikt4truth_Timing;   //!
TBranch        *b_jet_antikt4truth_LArQuality;   //!
TBranch        *b_jet_antikt4truth_nTrk;   //!
TBranch        *b_jet_antikt4truth_sumPtTrk;   //!
TBranch        *b_jet_antikt4truth_OriginIndex;   //!
TBranch        *b_jet_antikt4truth_HECQuality;   //!
TBranch        *b_jet_antikt4truth_NegativeE;   //!
TBranch        *b_jet_antikt4truth_AverageLArQF;   //!
TBranch        *b_jet_antikt4truth_BCH_CORR_CELL;   //!
TBranch        *b_jet_antikt4truth_BCH_CORR_DOTX;   //!
TBranch        *b_jet_antikt4truth_BCH_CORR_JET;   //!
TBranch        *b_jet_antikt4truth_BCH_CORR_JET_FORCELL;   //!
TBranch        *b_jet_antikt4truth_ENG_BAD_CELLS;   //!
TBranch        *b_jet_antikt4truth_N_BAD_CELLS;   //!
TBranch        *b_jet_antikt4truth_N_BAD_CELLS_CORR;   //!
TBranch        *b_jet_antikt4truth_BAD_CELLS_CORR_E;   //!
TBranch        *b_jet_antikt4truth_NumTowers;   //!
TBranch        *b_jet_antikt4truth_ootFracCells5;   //!
TBranch        *b_jet_antikt4truth_ootFracCells10;   //!
TBranch        *b_jet_antikt4truth_ootFracClusters5;   //!
TBranch        *b_jet_antikt4truth_ootFracClusters10;   //!
TBranch        *b_jet_antikt4truth_SamplingMax;   //!
TBranch        *b_jet_antikt4truth_fracSamplingMax;   //!
TBranch        *b_jet_antikt4truth_hecf;   //!
TBranch        *b_jet_antikt4truth_tgap3f;   //!
TBranch        *b_jet_antikt4truth_isUgly;   //!
TBranch        *b_jet_antikt4truth_isBadLooseMinus;   //!
TBranch        *b_jet_antikt4truth_isBadLoose;   //!
TBranch        *b_jet_antikt4truth_isBadMedium;   //!
TBranch        *b_jet_antikt4truth_isBadTight;   //!
TBranch        *b_jet_antikt4truth_emfrac;   //!
TBranch        *b_jet_antikt4truth_Offset;   //!
TBranch        *b_jet_antikt4truth_EMJES;   //!
TBranch        *b_jet_antikt4truth_EMJES_EtaCorr;   //!
TBranch        *b_jet_antikt4truth_EMJESnooffset;   //!
TBranch        *b_jet_antikt4truth_LCJES;   //!
TBranch        *b_jet_antikt4truth_LCJES_EtaCorr;   //!
TBranch        *b_jet_antikt4truth_emscale_E;   //!
TBranch        *b_jet_antikt4truth_emscale_pt;   //!
TBranch        *b_jet_antikt4truth_emscale_m;   //!
TBranch        *b_jet_antikt4truth_emscale_eta;   //!
TBranch        *b_jet_antikt4truth_emscale_phi;   //!
TBranch        *b_jet_antikt4truth_ActiveArea;   //!
TBranch        *b_jet_antikt4truth_ActiveAreaPx;   //!
TBranch        *b_jet_antikt4truth_ActiveAreaPy;   //!
TBranch        *b_jet_antikt4truth_ActiveAreaPz;   //!
TBranch        *b_jet_antikt4truth_ActiveAreaE;   //!
TBranch        *b_jet_antikt4truth_el_dr;   //!
TBranch        *b_jet_antikt4truth_el_matched;   //!
TBranch        *b_jet_antikt4truth_mu_dr;   //!
TBranch        *b_jet_antikt4truth_mu_matched;   //!
TBranch        *b_jet_antikt4truth_L1_dr;   //!
TBranch        *b_jet_antikt4truth_L1_matched;   //!
TBranch        *b_jet_antikt4truth_L2_dr;   //!
TBranch        *b_jet_antikt4truth_L2_matched;   //!
TBranch        *b_jet_antikt4truth_EF_dr;   //!
TBranch        *b_jet_antikt4truth_EF_matched;   //!

#endif
