#ifndef DPDTauVariables_h
#define DPDTauVariables_h
#include "TTree.h"
#include <vector>
#include <string>

void SetBranchTauVar(TTree*,int,bool minimal=false);

void SetPointerTauVar();

/* List of Tau DPD Variables */
Int_t           tau_n;
vector<float>   *tau_Et;
vector<float>   *tau_pt;
vector<float>   *tau_m;
vector<float>   *tau_eta;
vector<float>   *tau_phi;
vector<float>   *tau_charge;
vector<float>   *tau_BDTEleScore;
vector<float>   *tau_BDTJetScore;
vector<float>   *tau_likelihood;
vector<float>   *tau_SafeLikelihood;
vector<int>     *tau_electronVetoLoose;
vector<int>     *tau_electronVetoMedium;
vector<int>     *tau_electronVetoTight;
vector<int>     *tau_muonVeto;
vector<int>     *tau_tauLlhLoose;
vector<int>     *tau_tauLlhMedium;
vector<int>     *tau_tauLlhTight;
vector<int>     *tau_JetBDTSigLoose;
vector<int>     *tau_JetBDTSigMedium;
vector<int>     *tau_JetBDTSigTight;
vector<int>     *tau_EleBDTLoose;
vector<int>     *tau_EleBDTMedium;
vector<int>     *tau_EleBDTTight;
vector<int>     *tau_author;
vector<int>     *tau_RoIWord;
vector<int>     *tau_nProng;
vector<int>     *tau_numTrack;
vector<int>     *tau_seedCalo_numTrack;
vector<int>     *tau_seedCalo_nWideTrk;
vector<int>     *tau_nOtherTrk;
vector<float>   *tau_etOverPtLeadTrk;
vector<float>   *tau_ipZ0SinThetaSigLeadTrk;
vector<float>   *tau_leadTrkPt;
vector<int>     *tau_nLooseTrk;
vector<int>     *tau_nLooseConvTrk;
vector<int>     *tau_nProngLoose;
vector<float>   *tau_ipSigLeadTrk;
vector<float>   *tau_ipSigLeadLooseTrk;
vector<float>   *tau_etOverPtLeadLooseTrk;
vector<float>   *tau_leadLooseTrkPt;
vector<float>   *tau_chrgLooseTrk;
vector<float>   *tau_massTrkSys;
vector<float>   *tau_trkWidth2;
vector<float>   *tau_trFlightPathSig;
vector<float>   *tau_etEflow;
vector<float>   *tau_mEflow;
vector<int>     *tau_nPi0;
vector<float>   *tau_ele_E237E277;
vector<float>   *tau_ele_PresamplerFraction;
vector<float>   *tau_ele_ECALFirstFraction;
vector<float>   *tau_EM_TES_scale;
vector<float>   *tau_LC_TES_precalib;
vector<float>   *tau_TRTHTOverLT_LeadTrk;
vector<float>   *tau_seedCalo_EMRadius;
vector<float>   *tau_seedCalo_hadRadius;
vector<float>   *tau_seedCalo_etEMAtEMScale;
vector<float>   *tau_seedCalo_etHadAtEMScale;
vector<float>   *tau_seedCalo_isolFrac;
vector<float>   *tau_seedCalo_centFrac;
vector<float>   *tau_seedCalo_stripWidth2;
vector<int>     *tau_seedCalo_nStrip;
vector<float>   *tau_seedCalo_eta;
vector<float>   *tau_seedCalo_phi;
vector<float>   *tau_seedCalo_nIsolLooseTrk;
vector<float>   *tau_seedCalo_trkAvgDist;
vector<float>   *tau_seedCalo_trkRmsDist;
vector<float>   *tau_seedCalo_caloIso;
vector<float>   *tau_seedCalo_caloIsoCorrected;
vector<float>   *tau_seedCalo_dRmax;
vector<float>   *tau_seedCalo_lead2ClusterEOverAllClusterE;
vector<float>   *tau_seedCalo_lead3ClusterEOverAllClusterE;
vector<float>   *tau_seedCalo_etHadCalib;
vector<float>   *tau_seedCalo_etEMCalib;
vector<int>     *tau_numTopoClusters;
vector<float>   *tau_numEffTopoClusters;
vector<float>   *tau_topoInvMass;
vector<float>   *tau_effTopoInvMass;
vector<float>   *tau_topoMeanDeltaR;
vector<float>   *tau_effTopoMeanDeltaR;
vector<float>   *tau_numCells;
vector<float>   *tau_seedTrk_EMRadius;
vector<float>   *tau_seedTrk_isolFrac;
vector<float>   *tau_seedTrk_etChrgHadOverSumTrkPt;
vector<float>   *tau_seedTrk_isolFracWide;
vector<float>   *tau_seedTrk_etHadAtEMScale;
vector<float>   *tau_seedTrk_etEMAtEMScale;
vector<float>   *tau_seedTrk_etEMCL;
vector<float>   *tau_seedTrk_etChrgEM;
vector<float>   *tau_seedTrk_etNeuEM;
vector<float>   *tau_seedTrk_etResNeuEM;
vector<float>   *tau_seedTrk_hadLeakEt;
vector<float>   *tau_seedTrk_sumEMCellEtOverLeadTrkPt;
vector<float>   *tau_seedTrk_secMaxStripEt;
vector<float>   *tau_seedTrk_stripWidth2;
vector<int>     *tau_seedTrk_nStrip;
vector<float>   *tau_seedTrk_etChrgHad;
vector<int>     *tau_seedTrk_nOtherCoreTrk;
vector<int>     *tau_seedTrk_nIsolTrk;
vector<float>   *tau_seedTrk_etIsolEM;
vector<float>   *tau_seedTrk_etIsolHad;
vector<float>   *tau_cellBasedEnergyRing1;
vector<float>   *tau_cellBasedEnergyRing2;
vector<float>   *tau_cellBasedEnergyRing3;
vector<float>   *tau_cellBasedEnergyRing4;
vector<float>   *tau_cellBasedEnergyRing5;
vector<float>   *tau_cellBasedEnergyRing6;
vector<float>   *tau_cellBasedEnergyRing7;
vector<float>   *tau_calcVars_etHad_EMScale_Pt3Trks;
vector<float>   *tau_calcVars_etEM_EMScale_Pt3Trks;
vector<float>   *tau_calcVars_ipSigLeadLooseTrk;
vector<float>   *tau_calcVars_drMin;
vector<float>   *tau_calcVars_calRadius;
vector<float>   *tau_calcVars_EMFractionAtEMScale;
vector<float>   *tau_calcVars_trackIso;
vector<float>   *tau_calcVars_ChPiEMEOverCaloEME;
vector<float>   *tau_calcVars_PSSFraction;
vector<float>   *tau_calcVars_EMPOverTrkSysP;
vector<float>   *tau_calcVars_pi0BDTPrimaryScore;
vector<float>   *tau_calcVars_pi0BDTSecondaryScore;
vector<float>   *tau_calcVars_corrCentFrac;
vector<float>   *tau_calcVars_corrFTrk;
vector<float>   *tau_calcVars_interAxis_eta;
vector<float>   *tau_calcVars_interAxis_phi;
vector<float>   *tau_calcVars_absDeltaEta;
vector<float>   *tau_calcVars_absDeltaPhi;
vector<float>   *tau_calcVars_EMFractionAtEMScale_MoveE3;
vector<float>   *tau_calcVars_SecMaxStripEtOverPt;
vector<float>   *tau_pi0_cl1_pt;
vector<float>   *tau_pi0_cl1_eta;
vector<float>   *tau_pi0_cl1_phi;
vector<float>   *tau_pi0_cl2_pt;
vector<float>   *tau_pi0_cl2_eta;
vector<float>   *tau_pi0_cl2_phi;
vector<float>   *tau_pi0_vistau_pt;
vector<float>   *tau_pi0_vistau_eta;
vector<float>   *tau_pi0_vistau_phi;
vector<float>   *tau_pi0_vistau_m;
vector<int>     *tau_pi0_n;
vector<float>   *tau_leadTrack_eta;
vector<float>   *tau_leadTrack_phi;
vector<int>     *tau_track_atTJVA_n;
vector<int>     *tau_seedCalo_wideTrk_atTJVA_n;
vector<int>     *tau_otherTrk_atTJVA_n;
vector<int>     *tau_track_n;
vector<int>     *tau_seedCalo_track_n;
vector<int>     *tau_seedCalo_wideTrk_n;
vector<int>     *tau_otherTrk_n;
vector<float>   *tau_EF_dr;
vector<float>   *tau_EF_E;
vector<float>   *tau_EF_Et;
vector<float>   *tau_EF_pt;
vector<float>   *tau_EF_eta;
vector<float>   *tau_EF_phi;
vector<int>     *tau_EF_matched;
vector<float>   *tau_L2_dr;
vector<float>   *tau_L2_E;
vector<float>   *tau_L2_Et;
vector<float>   *tau_L2_pt;
vector<float>   *tau_L2_eta;
vector<float>   *tau_L2_phi;
vector<int>     *tau_L2_matched;
vector<float>   *tau_L1_dr;
vector<float>   *tau_L1_Et;
vector<float>   *tau_L1_pt;
vector<float>   *tau_L1_eta;
vector<float>   *tau_L1_phi;
vector<int>     *tau_L1_matched;

/*List of branches*/
TBranch        *b_tau_n;   //!
TBranch        *b_tau_Et;   //!
TBranch        *b_tau_pt;   //!
TBranch        *b_tau_m;   //!
TBranch        *b_tau_eta;   //!
TBranch        *b_tau_phi;   //!
TBranch        *b_tau_charge;   //!
TBranch        *b_tau_BDTEleScore;   //!
TBranch        *b_tau_BDTJetScore;   //!
TBranch        *b_tau_likelihood;   //!
TBranch        *b_tau_SafeLikelihood;   //!
TBranch        *b_tau_electronVetoLoose;   //!
TBranch        *b_tau_electronVetoMedium;   //!
TBranch        *b_tau_electronVetoTight;   //!
TBranch        *b_tau_muonVeto;   //!
TBranch        *b_tau_tauLlhLoose;   //!
TBranch        *b_tau_tauLlhMedium;   //!
TBranch        *b_tau_tauLlhTight;   //!
TBranch        *b_tau_JetBDTSigLoose;   //!
TBranch        *b_tau_JetBDTSigMedium;   //!
TBranch        *b_tau_JetBDTSigTight;   //!
TBranch        *b_tau_EleBDTLoose;   //!
TBranch        *b_tau_EleBDTMedium;   //!
TBranch        *b_tau_EleBDTTight;   //!
TBranch        *b_tau_author;   //!
TBranch        *b_tau_RoIWord;   //!
TBranch        *b_tau_nProng;   //!
TBranch        *b_tau_numTrack;   //!
TBranch        *b_tau_seedCalo_numTrack;   //!
TBranch        *b_tau_seedCalo_nWideTrk;   //!
TBranch        *b_tau_nOtherTrk;   //!
TBranch        *b_tau_etOverPtLeadTrk;   //!
TBranch        *b_tau_ipZ0SinThetaSigLeadTrk;   //!
TBranch        *b_tau_leadTrkPt;   //!
TBranch        *b_tau_nLooseTrk;   //!
TBranch        *b_tau_nLooseConvTrk;   //!
TBranch        *b_tau_nProngLoose;   //!
TBranch        *b_tau_ipSigLeadTrk;   //!
TBranch        *b_tau_ipSigLeadLooseTrk;   //!
TBranch        *b_tau_etOverPtLeadLooseTrk;   //!
TBranch        *b_tau_leadLooseTrkPt;   //!
TBranch        *b_tau_chrgLooseTrk;   //!
TBranch        *b_tau_massTrkSys;   //!
TBranch        *b_tau_trkWidth2;   //!
TBranch        *b_tau_trFlightPathSig;   //!
TBranch        *b_tau_etEflow;   //!
TBranch        *b_tau_mEflow;   //!
TBranch        *b_tau_nPi0;   //!
TBranch        *b_tau_ele_E237E277;   //!
TBranch        *b_tau_ele_PresamplerFraction;   //!
TBranch        *b_tau_ele_ECALFirstFraction;   //!
TBranch        *b_tau_EM_TES_scale;   //!
TBranch        *b_tau_LC_TES_precalib;   //!
TBranch        *b_tau_TRTHTOverLT_LeadTrk;   //!
TBranch        *b_tau_seedCalo_EMRadius;   //!
TBranch        *b_tau_seedCalo_hadRadius;   //!
TBranch        *b_tau_seedCalo_etEMAtEMScale;   //!
TBranch        *b_tau_seedCalo_etHadAtEMScale;   //!
TBranch        *b_tau_seedCalo_isolFrac;   //!
TBranch        *b_tau_seedCalo_centFrac;   //!
TBranch        *b_tau_seedCalo_stripWidth2;   //!
TBranch        *b_tau_seedCalo_nStrip;   //!
TBranch        *b_tau_seedCalo_eta;   //!
TBranch        *b_tau_seedCalo_phi;   //!
TBranch        *b_tau_seedCalo_nIsolLooseTrk;   //!
TBranch        *b_tau_seedCalo_trkAvgDist;   //!
TBranch        *b_tau_seedCalo_trkRmsDist;   //!
TBranch        *b_tau_seedCalo_caloIso;   //!
TBranch        *b_tau_seedCalo_caloIsoCorrected;   //!
TBranch        *b_tau_seedCalo_dRmax;   //!
TBranch        *b_tau_seedCalo_lead2ClusterEOverAllClusterE;   //!
TBranch        *b_tau_seedCalo_lead3ClusterEOverAllClusterE;   //!
TBranch        *b_tau_seedCalo_etHadCalib;   //!
TBranch        *b_tau_seedCalo_etEMCalib;   //!
TBranch        *b_tau_numTopoClusters;   //!
TBranch        *b_tau_numEffTopoClusters;   //!
TBranch        *b_tau_topoInvMass;   //!
TBranch        *b_tau_effTopoInvMass;   //!
TBranch        *b_tau_topoMeanDeltaR;   //!
TBranch        *b_tau_effTopoMeanDeltaR;   //!
TBranch        *b_tau_numCells;   //!
TBranch        *b_tau_seedTrk_EMRadius;   //!
TBranch        *b_tau_seedTrk_isolFrac;   //!
TBranch        *b_tau_seedTrk_etChrgHadOverSumTrkPt;   //!
TBranch        *b_tau_seedTrk_isolFracWide;   //!
TBranch        *b_tau_seedTrk_etHadAtEMScale;   //!
TBranch        *b_tau_seedTrk_etEMAtEMScale;   //!
TBranch        *b_tau_seedTrk_etEMCL;   //!
TBranch        *b_tau_seedTrk_etChrgEM;   //!
TBranch        *b_tau_seedTrk_etNeuEM;   //!
TBranch        *b_tau_seedTrk_etResNeuEM;   //!
TBranch        *b_tau_seedTrk_hadLeakEt;   //!
TBranch        *b_tau_seedTrk_sumEMCellEtOverLeadTrkPt;   //!
TBranch        *b_tau_seedTrk_secMaxStripEt;   //!
TBranch        *b_tau_seedTrk_stripWidth2;   //!
TBranch        *b_tau_seedTrk_nStrip;   //!
TBranch        *b_tau_seedTrk_etChrgHad;   //!
TBranch        *b_tau_seedTrk_nOtherCoreTrk;   //!
TBranch        *b_tau_seedTrk_nIsolTrk;   //!
TBranch        *b_tau_seedTrk_etIsolEM;   //!
TBranch        *b_tau_seedTrk_etIsolHad;   //!
TBranch        *b_tau_cellBasedEnergyRing1;   //!
TBranch        *b_tau_cellBasedEnergyRing2;   //!
TBranch        *b_tau_cellBasedEnergyRing3;   //!
TBranch        *b_tau_cellBasedEnergyRing4;   //!
TBranch        *b_tau_cellBasedEnergyRing5;   //!
TBranch        *b_tau_cellBasedEnergyRing6;   //!
TBranch        *b_tau_cellBasedEnergyRing7;   //!
TBranch        *b_tau_calcVars_etHad_EMScale_Pt3Trks;   //!
TBranch        *b_tau_calcVars_etEM_EMScale_Pt3Trks;   //!
TBranch        *b_tau_calcVars_ipSigLeadLooseTrk;   //!
TBranch        *b_tau_calcVars_drMin;   //!
TBranch        *b_tau_calcVars_calRadius;   //!
TBranch        *b_tau_calcVars_EMFractionAtEMScale;   //!
TBranch        *b_tau_calcVars_trackIso;   //!
TBranch        *b_tau_calcVars_ChPiEMEOverCaloEME;   //!
TBranch        *b_tau_calcVars_PSSFraction;   //!
TBranch        *b_tau_calcVars_EMPOverTrkSysP;   //!
TBranch        *b_tau_calcVars_pi0BDTPrimaryScore;   //!
TBranch        *b_tau_calcVars_pi0BDTSecondaryScore;   //!
TBranch        *b_tau_calcVars_corrCentFrac;   //!
TBranch        *b_tau_calcVars_corrFTrk;   //!
TBranch        *b_tau_calcVars_interAxis_eta;   //!
TBranch        *b_tau_calcVars_interAxis_phi;   //!
TBranch        *b_tau_calcVars_absDeltaEta;   //!
TBranch        *b_tau_calcVars_absDeltaPhi;   //!
TBranch        *b_tau_calcVars_EMFractionAtEMScale_MoveE3;   //!
TBranch        *b_tau_calcVars_SecMaxStripEtOverPt;   //!
TBranch        *b_tau_pi0_cl1_pt;   //!
TBranch        *b_tau_pi0_cl1_eta;   //!
TBranch        *b_tau_pi0_cl1_phi;   //!
TBranch        *b_tau_pi0_cl2_pt;   //!
TBranch        *b_tau_pi0_cl2_eta;   //!
TBranch        *b_tau_pi0_cl2_phi;   //!
TBranch        *b_tau_pi0_vistau_pt;   //!
TBranch        *b_tau_pi0_vistau_eta;   //!
TBranch        *b_tau_pi0_vistau_phi;   //!
TBranch        *b_tau_pi0_vistau_m;   //!
TBranch        *b_tau_pi0_n;   //!
TBranch        *b_tau_leadTrack_eta;   //!
TBranch        *b_tau_leadTrack_phi;   //!
TBranch        *b_tau_track_atTJVA_n;   //!
TBranch        *b_tau_seedCalo_wideTrk_atTJVA_n;   //!
TBranch        *b_tau_otherTrk_atTJVA_n;   //!
TBranch        *b_tau_track_n;   //!
TBranch        *b_tau_seedCalo_track_n;   //!
TBranch        *b_tau_seedCalo_wideTrk_n;   //!
TBranch        *b_tau_otherTrk_n;   //!
TBranch        *b_tau_EF_dr;   //!
TBranch        *b_tau_EF_E;   //!
TBranch        *b_tau_EF_Et;   //!
TBranch        *b_tau_EF_pt;   //!
TBranch        *b_tau_EF_eta;   //!
TBranch        *b_tau_EF_phi;   //!
TBranch        *b_tau_EF_matched;   //!
TBranch        *b_tau_L2_dr;   //!
TBranch        *b_tau_L2_E;   //!
TBranch        *b_tau_L2_Et;   //!
TBranch        *b_tau_L2_pt;   //!
TBranch        *b_tau_L2_eta;   //!
TBranch        *b_tau_L2_phi;   //!
TBranch        *b_tau_L2_matched;   //!
TBranch        *b_tau_L1_dr;   //!
TBranch        *b_tau_L1_Et;   //!
TBranch        *b_tau_L1_pt;   //!
TBranch        *b_tau_L1_eta;   //!
TBranch        *b_tau_L1_phi;   //!
TBranch        *b_tau_L1_matched;   //!

#endif
