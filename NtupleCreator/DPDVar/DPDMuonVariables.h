#ifndef DPDMuonVariables_h
#define DPDMuonVariables_h
#include "TTree.h"
#include <vector>
#include <string>

void SetBranchMuonVar(TTree*,int,int,bool minimal=false);

void SetBranchMuonTrigVar(TTree*,int);

void SetBranchMuonTrigMatchVar(TTree*,int);

void SetPointerMuonVar();

void SetPointerMuonTrigMatchVar();

/* List of Muon DPD Variables */
/*trigger 2011*/
Bool_t          EF_2mu10_loose;
Bool_t          EF_mu18;
Bool_t          EF_mu18_MG;
Bool_t          EF_mu18_MG_medium;
Bool_t          EF_mu18_medium;
/*trigger 2012*/
Bool_t          EF_mu24i_tight;
Bool_t          EF_mu36_tight;
Bool_t          EF_2mu13;
Bool_t          EF_mu18_tight_mu8_EFFS;

Int_t           mu_staco_n;
std::vector<float>   *mu_staco_E;
std::vector<float>   *mu_staco_pt;
std::vector<float>   *mu_staco_m;
std::vector<float>   *mu_staco_eta;
std::vector<float>   *mu_staco_phi;
std::vector<float>   *mu_staco_px;
std::vector<float>   *mu_staco_py;
std::vector<float>   *mu_staco_pz;
std::vector<float>   *mu_staco_charge;
std::vector<unsigned short> *mu_staco_allauthor;
std::vector<int>     *mu_staco_author;
std::vector<float>   *mu_staco_beta;
std::vector<float>   *mu_staco_isMuonLikelihood;
std::vector<float>   *mu_staco_matchchi2;
std::vector<int>     *mu_staco_matchndof;
std::vector<float>   *mu_staco_etcone20;
std::vector<float>   *mu_staco_etcone30;
std::vector<float>   *mu_staco_etcone40;
std::vector<float>   *mu_staco_nucone20;
std::vector<float>   *mu_staco_nucone30;
std::vector<float>   *mu_staco_nucone40;
std::vector<float>   *mu_staco_ptcone20;
std::vector<float>   *mu_staco_ptcone30;
std::vector<float>   *mu_staco_ptcone40;
std::vector<float>   *mu_staco_energyLossPar;
std::vector<float>   *mu_staco_energyLossErr;
std::vector<float>   *mu_staco_etCore;
std::vector<float>   *mu_staco_energyLossType;
std::vector<unsigned short> *mu_staco_caloMuonIdTag;
std::vector<double>  *mu_staco_caloLRLikelihood;
std::vector<int>     *mu_staco_bestMatch;
std::vector<int>     *mu_staco_isStandAloneMuon;
std::vector<int>     *mu_staco_isCombinedMuon;
std::vector<int>     *mu_staco_isLowPtReconstructedMuon;
std::vector<int>     *mu_staco_isSegmentTaggedMuon;
std::vector<int>     *mu_staco_isCaloMuonId;
std::vector<int>     *mu_staco_alsoFoundByLowPt;
std::vector<int>     *mu_staco_alsoFoundByCaloMuonId;
std::vector<int>     *mu_staco_loose;
std::vector<int>     *mu_staco_medium;
std::vector<int>     *mu_staco_tight;
std::vector<float>   *mu_staco_d0_exPV;
std::vector<float>   *mu_staco_z0_exPV;
std::vector<float>   *mu_staco_phi_exPV;
std::vector<float>   *mu_staco_theta_exPV;
std::vector<float>   *mu_staco_qoverp_exPV;
std::vector<float>   *mu_staco_cb_d0_exPV;
std::vector<float>   *mu_staco_cb_z0_exPV;
std::vector<float>   *mu_staco_cb_phi_exPV;
std::vector<float>   *mu_staco_cb_theta_exPV;
std::vector<float>   *mu_staco_cb_qoverp_exPV;
std::vector<float>   *mu_staco_id_d0_exPV;
std::vector<float>   *mu_staco_id_z0_exPV;
std::vector<float>   *mu_staco_id_phi_exPV;
std::vector<float>   *mu_staco_id_theta_exPV;
std::vector<float>   *mu_staco_id_qoverp_exPV;
std::vector<float>   *mu_staco_me_d0_exPV;
std::vector<float>   *mu_staco_me_z0_exPV;
std::vector<float>   *mu_staco_me_phi_exPV;
std::vector<float>   *mu_staco_me_theta_exPV;
std::vector<float>   *mu_staco_me_qoverp_exPV;
std::vector<float>   *mu_staco_ie_d0_exPV;
std::vector<float>   *mu_staco_ie_z0_exPV;
std::vector<float>   *mu_staco_ie_phi_exPV;
std::vector<float>   *mu_staco_ie_theta_exPV;
std::vector<float>   *mu_staco_ie_qoverp_exPV;
std::vector<std::vector<int> > *mu_staco_SpaceTime_detID;
std::vector<std::vector<float> > *mu_staco_SpaceTime_t;
std::vector<std::vector<float> > *mu_staco_SpaceTime_tError;
std::vector<std::vector<float> > *mu_staco_SpaceTime_weight;
std::vector<std::vector<float> > *mu_staco_SpaceTime_x;
std::vector<std::vector<float> > *mu_staco_SpaceTime_y;
std::vector<std::vector<float> > *mu_staco_SpaceTime_z;
std::vector<float>   *mu_staco_cov_d0_exPV;
std::vector<float>   *mu_staco_cov_z0_exPV;
std::vector<float>   *mu_staco_cov_phi_exPV;
std::vector<float>   *mu_staco_cov_theta_exPV;
std::vector<float>   *mu_staco_cov_qoverp_exPV;
std::vector<float>   *mu_staco_cov_d0_z0_exPV;
std::vector<float>   *mu_staco_cov_d0_phi_exPV;
std::vector<float>   *mu_staco_cov_d0_theta_exPV;
std::vector<float>   *mu_staco_cov_d0_qoverp_exPV;
std::vector<float>   *mu_staco_cov_z0_phi_exPV;
std::vector<float>   *mu_staco_cov_z0_theta_exPV;
std::vector<float>   *mu_staco_cov_z0_qoverp_exPV;
std::vector<float>   *mu_staco_cov_phi_theta_exPV;
std::vector<float>   *mu_staco_cov_phi_qoverp_exPV;
std::vector<float>   *mu_staco_cov_theta_qoverp_exPV;
std::vector<float>   *mu_staco_id_cov_d0_exPV;
std::vector<float>   *mu_staco_id_cov_z0_exPV;
std::vector<float>   *mu_staco_id_cov_phi_exPV;
std::vector<float>   *mu_staco_id_cov_theta_exPV;
std::vector<float>   *mu_staco_id_cov_qoverp_exPV;
std::vector<float>   *mu_staco_id_cov_d0_z0_exPV;
std::vector<float>   *mu_staco_id_cov_d0_phi_exPV;
std::vector<float>   *mu_staco_id_cov_d0_theta_exPV;
std::vector<float>   *mu_staco_id_cov_d0_qoverp_exPV;
std::vector<float>   *mu_staco_id_cov_z0_phi_exPV;
std::vector<float>   *mu_staco_id_cov_z0_theta_exPV;
std::vector<float>   *mu_staco_id_cov_z0_qoverp_exPV;
std::vector<float>   *mu_staco_id_cov_phi_theta_exPV;
std::vector<float>   *mu_staco_id_cov_phi_qoverp_exPV;
std::vector<float>   *mu_staco_id_cov_theta_qoverp_exPV;
std::vector<float>   *mu_staco_me_cov_d0_exPV;
std::vector<float>   *mu_staco_me_cov_z0_exPV;
std::vector<float>   *mu_staco_me_cov_phi_exPV;
std::vector<float>   *mu_staco_me_cov_theta_exPV;
std::vector<float>   *mu_staco_me_cov_qoverp_exPV;
std::vector<float>   *mu_staco_me_cov_d0_z0_exPV;
std::vector<float>   *mu_staco_me_cov_d0_phi_exPV;
std::vector<float>   *mu_staco_me_cov_d0_theta_exPV;
std::vector<float>   *mu_staco_me_cov_d0_qoverp_exPV;
std::vector<float>   *mu_staco_me_cov_z0_phi_exPV;
std::vector<float>   *mu_staco_me_cov_z0_theta_exPV;
std::vector<float>   *mu_staco_me_cov_z0_qoverp_exPV;
std::vector<float>   *mu_staco_me_cov_phi_theta_exPV;
std::vector<float>   *mu_staco_me_cov_phi_qoverp_exPV;
std::vector<float>   *mu_staco_me_cov_theta_qoverp_exPV;
std::vector<float>   *mu_staco_ms_d0;
std::vector<float>   *mu_staco_ms_z0;
std::vector<float>   *mu_staco_ms_phi;
std::vector<float>   *mu_staco_ms_theta;
std::vector<float>   *mu_staco_ms_qoverp;
std::vector<float>   *mu_staco_id_d0;
std::vector<float>   *mu_staco_id_z0;
std::vector<float>   *mu_staco_id_phi;
std::vector<float>   *mu_staco_id_theta;
std::vector<float>   *mu_staco_id_qoverp;
std::vector<float>   *mu_staco_me_d0;
std::vector<float>   *mu_staco_me_z0;
std::vector<float>   *mu_staco_me_phi;
std::vector<float>   *mu_staco_me_theta;
std::vector<float>   *mu_staco_me_qoverp;
std::vector<float>   *mu_staco_ie_d0;
std::vector<float>   *mu_staco_ie_z0;
std::vector<float>   *mu_staco_ie_phi;
std::vector<float>   *mu_staco_ie_theta;
std::vector<float>   *mu_staco_ie_qoverp;
std::vector<int>     *mu_staco_nOutliersOnTrack;
std::vector<int>     *mu_staco_nBLHits;
std::vector<int>     *mu_staco_nPixHits;
std::vector<int>     *mu_staco_nSCTHits;
std::vector<int>     *mu_staco_nTRTHits;
std::vector<int>     *mu_staco_nTRTHighTHits;
std::vector<int>     *mu_staco_nBLSharedHits;
std::vector<int>     *mu_staco_nPixSharedHits;
std::vector<int>     *mu_staco_nPixHoles;
std::vector<int>     *mu_staco_nSCTSharedHits;
std::vector<int>     *mu_staco_nSCTHoles;
std::vector<int>     *mu_staco_nTRTOutliers;
std::vector<int>     *mu_staco_nTRTHighTOutliers;
std::vector<int>     *mu_staco_nGangedPixels;
std::vector<int>     *mu_staco_nPixelDeadSensors;
std::vector<int>     *mu_staco_nSCTDeadSensors;
std::vector<int>     *mu_staco_nTRTDeadStraws;
std::vector<int>     *mu_staco_expectBLayerHit;
std::vector<int>     *mu_staco_nMDTHits;
std::vector<int>     *mu_staco_nMDTHoles;
std::vector<int>     *mu_staco_nCSCEtaHits;
std::vector<int>     *mu_staco_nCSCEtaHoles;
std::vector<int>     *mu_staco_nCSCPhiHits;
std::vector<int>     *mu_staco_nCSCPhiHoles;
std::vector<int>     *mu_staco_nRPCEtaHits;
std::vector<int>     *mu_staco_nRPCEtaHoles;
std::vector<int>     *mu_staco_nRPCPhiHits;
std::vector<int>     *mu_staco_nRPCPhiHoles;
std::vector<int>     *mu_staco_nTGCEtaHits;
std::vector<int>     *mu_staco_nTGCEtaHoles;
std::vector<int>     *mu_staco_nTGCPhiHits;
std::vector<int>     *mu_staco_nTGCPhiHoles;
std::vector<int>     *mu_staco_nMDTBIHits;
std::vector<int>     *mu_staco_nMDTBMHits;
std::vector<int>     *mu_staco_nMDTBOHits;
std::vector<int>     *mu_staco_nMDTBEEHits;
std::vector<int>     *mu_staco_nMDTBIS78Hits;
std::vector<int>     *mu_staco_nMDTEIHits;
std::vector<int>     *mu_staco_nMDTEMHits;
std::vector<int>     *mu_staco_nMDTEOHits;
std::vector<int>     *mu_staco_nMDTEEHits;
std::vector<int>     *mu_staco_nRPCLayer1EtaHits;
std::vector<int>     *mu_staco_nRPCLayer2EtaHits;
std::vector<int>     *mu_staco_nRPCLayer3EtaHits;
std::vector<int>     *mu_staco_nRPCLayer1PhiHits;
std::vector<int>     *mu_staco_nRPCLayer2PhiHits;
std::vector<int>     *mu_staco_nRPCLayer3PhiHits;
std::vector<int>     *mu_staco_nTGCLayer1EtaHits;
std::vector<int>     *mu_staco_nTGCLayer2EtaHits;
std::vector<int>     *mu_staco_nTGCLayer3EtaHits;
std::vector<int>     *mu_staco_nTGCLayer4EtaHits;
std::vector<int>     *mu_staco_nTGCLayer1PhiHits;
std::vector<int>     *mu_staco_nTGCLayer2PhiHits;
std::vector<int>     *mu_staco_nTGCLayer3PhiHits;
std::vector<int>     *mu_staco_nTGCLayer4PhiHits;
std::vector<int>     *mu_staco_barrelSectors;
std::vector<int>     *mu_staco_endcapSectors;
std::vector<float>   *mu_staco_trackd0;
std::vector<float>   *mu_staco_trackz0;
std::vector<float>   *mu_staco_trackphi;
std::vector<float>   *mu_staco_tracktheta;
std::vector<float>   *mu_staco_trackqoverp;
std::vector<float>   *mu_staco_trackcov_d0;
std::vector<float>   *mu_staco_trackcov_z0;
std::vector<float>   *mu_staco_trackcov_phi;
std::vector<float>   *mu_staco_trackcov_theta;
std::vector<float>   *mu_staco_trackcov_qoverp;
std::vector<float>   *mu_staco_trackcov_d0_z0;
std::vector<float>   *mu_staco_trackcov_d0_phi;
std::vector<float>   *mu_staco_trackcov_d0_theta;
std::vector<float>   *mu_staco_trackcov_d0_qoverp;
std::vector<float>   *mu_staco_trackcov_z0_phi;
std::vector<float>   *mu_staco_trackcov_z0_theta;
std::vector<float>   *mu_staco_trackcov_z0_qoverp;
std::vector<float>   *mu_staco_trackcov_phi_theta;
std::vector<float>   *mu_staco_trackcov_phi_qoverp;
std::vector<float>   *mu_staco_trackcov_theta_qoverp;
std::vector<float>   *mu_staco_trackfitchi2;
std::vector<int>     *mu_staco_trackfitndof;
std::vector<int>     *mu_staco_hastrack;
std::vector<float>   *mu_staco_trackd0beam;
std::vector<float>   *mu_staco_trackz0beam;
std::vector<float>   *mu_staco_tracksigd0beam;
std::vector<float>   *mu_staco_tracksigz0beam;
std::vector<float>   *mu_staco_trackd0pv;
std::vector<float>   *mu_staco_trackz0pv;
std::vector<float>   *mu_staco_tracksigd0pv;
std::vector<float>   *mu_staco_tracksigz0pv;
std::vector<float>   *mu_staco_trackIPEstimate_d0_biasedpvunbiased;
std::vector<float>   *mu_staco_trackIPEstimate_z0_biasedpvunbiased;
std::vector<float>   *mu_staco_trackIPEstimate_sigd0_biasedpvunbiased;
std::vector<float>   *mu_staco_trackIPEstimate_sigz0_biasedpvunbiased;
std::vector<float>   *mu_staco_trackIPEstimate_d0_unbiasedpvunbiased;
std::vector<float>   *mu_staco_trackIPEstimate_z0_unbiasedpvunbiased;
std::vector<float>   *mu_staco_trackIPEstimate_sigd0_unbiasedpvunbiased;
std::vector<float>   *mu_staco_trackIPEstimate_sigz0_unbiasedpvunbiased;
std::vector<float>   *mu_staco_trackd0pvunbiased;
std::vector<float>   *mu_staco_trackz0pvunbiased;
std::vector<float>   *mu_staco_tracksigd0pvunbiased;
std::vector<float>   *mu_staco_tracksigz0pvunbiased;
std::vector<float>   *mu_staco_truth_dr;
std::vector<float>   *mu_staco_truth_E;
std::vector<float>   *mu_staco_truth_pt;
std::vector<float>   *mu_staco_truth_eta;
std::vector<float>   *mu_staco_truth_phi;
std::vector<int>     *mu_staco_truth_type;
std::vector<int>     *mu_staco_truth_status;
std::vector<int>     *mu_staco_truth_barcode;
std::vector<int>     *mu_staco_truth_mothertype;
std::vector<int>     *mu_staco_truth_motherbarcode;
std::vector<int>     *mu_staco_truth_matched;
std::vector<float>   *mu_staco_EFCB_dr;
std::vector<int>     *mu_staco_EFCB_index;
std::vector<float>   *mu_staco_EFMG_dr;
std::vector<int>     *mu_staco_EFMG_index;
std::vector<float>   *mu_staco_EFME_dr;
std::vector<int>     *mu_staco_EFME_index;
Int_t           mu_calo_n;
std::vector<float>   *mu_calo_E;
std::vector<float>   *mu_calo_pt;
std::vector<float>   *mu_calo_m;
std::vector<float>   *mu_calo_eta;
std::vector<float>   *mu_calo_phi;
std::vector<float>   *mu_calo_px;
std::vector<float>   *mu_calo_py;
std::vector<float>   *mu_calo_pz;
std::vector<float>   *mu_calo_charge;
std::vector<unsigned short> *mu_calo_allauthor;
std::vector<int>     *mu_calo_author;
std::vector<float>   *mu_calo_beta;
std::vector<float>   *mu_calo_isMuonLikelihood;
std::vector<float>   *mu_calo_matchchi2;
std::vector<int>     *mu_calo_matchndof;
std::vector<float>   *mu_calo_etcone20;
std::vector<float>   *mu_calo_etcone30;
std::vector<float>   *mu_calo_etcone40;
std::vector<float>   *mu_calo_nucone20;
std::vector<float>   *mu_calo_nucone30;
std::vector<float>   *mu_calo_nucone40;
std::vector<float>   *mu_calo_ptcone20;
std::vector<float>   *mu_calo_ptcone30;
std::vector<float>   *mu_calo_ptcone40;
std::vector<float>   *mu_calo_energyLossPar;
std::vector<float>   *mu_calo_energyLossErr;
std::vector<float>   *mu_calo_etCore;
std::vector<float>   *mu_calo_energyLossType;
std::vector<unsigned short> *mu_calo_caloMuonIdTag;
std::vector<double>  *mu_calo_caloLRLikelihood;
std::vector<int>     *mu_calo_bestMatch;
std::vector<int>     *mu_calo_isStandAloneMuon;
std::vector<int>     *mu_calo_isCombinedMuon;
std::vector<int>     *mu_calo_isLowPtReconstructedMuon;
std::vector<int>     *mu_calo_isSegmentTaggedMuon;
std::vector<int>     *mu_calo_isCaloMuonId;
std::vector<int>     *mu_calo_alsoFoundByLowPt;
std::vector<int>     *mu_calo_alsoFoundByCaloMuonId;
std::vector<int>     *mu_calo_loose;
std::vector<int>     *mu_calo_medium;
std::vector<int>     *mu_calo_tight;
std::vector<float>   *mu_calo_d0_exPV;
std::vector<float>   *mu_calo_z0_exPV;
std::vector<float>   *mu_calo_phi_exPV;
std::vector<float>   *mu_calo_theta_exPV;
std::vector<float>   *mu_calo_qoverp_exPV;
std::vector<float>   *mu_calo_cb_d0_exPV;
std::vector<float>   *mu_calo_cb_z0_exPV;
std::vector<float>   *mu_calo_cb_phi_exPV;
std::vector<float>   *mu_calo_cb_theta_exPV;
std::vector<float>   *mu_calo_cb_qoverp_exPV;
std::vector<float>   *mu_calo_id_d0_exPV;
std::vector<float>   *mu_calo_id_z0_exPV;
std::vector<float>   *mu_calo_id_phi_exPV;
std::vector<float>   *mu_calo_id_theta_exPV;
std::vector<float>   *mu_calo_id_qoverp_exPV;
std::vector<float>   *mu_calo_me_d0_exPV;
std::vector<float>   *mu_calo_me_z0_exPV;
std::vector<float>   *mu_calo_me_phi_exPV;
std::vector<float>   *mu_calo_me_theta_exPV;
std::vector<float>   *mu_calo_me_qoverp_exPV;
std::vector<float>   *mu_calo_ie_d0_exPV;
std::vector<float>   *mu_calo_ie_z0_exPV;
std::vector<float>   *mu_calo_ie_phi_exPV;
std::vector<float>   *mu_calo_ie_theta_exPV;
std::vector<float>   *mu_calo_ie_qoverp_exPV;
std::vector<std::vector<int> > *mu_calo_SpaceTime_detID;
std::vector<std::vector<float> > *mu_calo_SpaceTime_t;
std::vector<std::vector<float> > *mu_calo_SpaceTime_tError;
std::vector<std::vector<float> > *mu_calo_SpaceTime_weight;
std::vector<std::vector<float> > *mu_calo_SpaceTime_x;
std::vector<std::vector<float> > *mu_calo_SpaceTime_y;
std::vector<std::vector<float> > *mu_calo_SpaceTime_z;
std::vector<float>   *mu_calo_cov_d0_exPV;
std::vector<float>   *mu_calo_cov_z0_exPV;
std::vector<float>   *mu_calo_cov_phi_exPV;
std::vector<float>   *mu_calo_cov_theta_exPV;
std::vector<float>   *mu_calo_cov_qoverp_exPV;
std::vector<float>   *mu_calo_cov_d0_z0_exPV;
std::vector<float>   *mu_calo_cov_d0_phi_exPV;
std::vector<float>   *mu_calo_cov_d0_theta_exPV;
std::vector<float>   *mu_calo_cov_d0_qoverp_exPV;
std::vector<float>   *mu_calo_cov_z0_phi_exPV;
std::vector<float>   *mu_calo_cov_z0_theta_exPV;
std::vector<float>   *mu_calo_cov_z0_qoverp_exPV;
std::vector<float>   *mu_calo_cov_phi_theta_exPV;
std::vector<float>   *mu_calo_cov_phi_qoverp_exPV;
std::vector<float>   *mu_calo_cov_theta_qoverp_exPV;
std::vector<float>   *mu_calo_id_cov_d0_exPV;
std::vector<float>   *mu_calo_id_cov_z0_exPV;
std::vector<float>   *mu_calo_id_cov_phi_exPV;
std::vector<float>   *mu_calo_id_cov_theta_exPV;
std::vector<float>   *mu_calo_id_cov_qoverp_exPV;
std::vector<float>   *mu_calo_id_cov_d0_z0_exPV;
std::vector<float>   *mu_calo_id_cov_d0_phi_exPV;
std::vector<float>   *mu_calo_id_cov_d0_theta_exPV;
std::vector<float>   *mu_calo_id_cov_d0_qoverp_exPV;
std::vector<float>   *mu_calo_id_cov_z0_phi_exPV;
std::vector<float>   *mu_calo_id_cov_z0_theta_exPV;
std::vector<float>   *mu_calo_id_cov_z0_qoverp_exPV;
std::vector<float>   *mu_calo_id_cov_phi_theta_exPV;
std::vector<float>   *mu_calo_id_cov_phi_qoverp_exPV;
std::vector<float>   *mu_calo_id_cov_theta_qoverp_exPV;
std::vector<float>   *mu_calo_me_cov_d0_exPV;
std::vector<float>   *mu_calo_me_cov_z0_exPV;
std::vector<float>   *mu_calo_me_cov_phi_exPV;
std::vector<float>   *mu_calo_me_cov_theta_exPV;
std::vector<float>   *mu_calo_me_cov_qoverp_exPV;
std::vector<float>   *mu_calo_me_cov_d0_z0_exPV;
std::vector<float>   *mu_calo_me_cov_d0_phi_exPV;
std::vector<float>   *mu_calo_me_cov_d0_theta_exPV;
std::vector<float>   *mu_calo_me_cov_d0_qoverp_exPV;
std::vector<float>   *mu_calo_me_cov_z0_phi_exPV;
std::vector<float>   *mu_calo_me_cov_z0_theta_exPV;
std::vector<float>   *mu_calo_me_cov_z0_qoverp_exPV;
std::vector<float>   *mu_calo_me_cov_phi_theta_exPV;
std::vector<float>   *mu_calo_me_cov_phi_qoverp_exPV;
std::vector<float>   *mu_calo_me_cov_theta_qoverp_exPV;
std::vector<float>   *mu_calo_ms_d0;
std::vector<float>   *mu_calo_ms_z0;
std::vector<float>   *mu_calo_ms_phi;
std::vector<float>   *mu_calo_ms_theta;
std::vector<float>   *mu_calo_ms_qoverp;
std::vector<float>   *mu_calo_id_d0;
std::vector<float>   *mu_calo_id_z0;
std::vector<float>   *mu_calo_id_phi;
std::vector<float>   *mu_calo_id_theta;
std::vector<float>   *mu_calo_id_qoverp;
std::vector<float>   *mu_calo_me_d0;
std::vector<float>   *mu_calo_me_z0;
std::vector<float>   *mu_calo_me_phi;
std::vector<float>   *mu_calo_me_theta;
std::vector<float>   *mu_calo_me_qoverp;
std::vector<float>   *mu_calo_ie_d0;
std::vector<float>   *mu_calo_ie_z0;
std::vector<float>   *mu_calo_ie_phi;
std::vector<float>   *mu_calo_ie_theta;
std::vector<float>   *mu_calo_ie_qoverp;
std::vector<int>     *mu_calo_nOutliersOnTrack;
std::vector<int>     *mu_calo_nBLHits;
std::vector<int>     *mu_calo_nPixHits;
std::vector<int>     *mu_calo_nSCTHits;
std::vector<int>     *mu_calo_nTRTHits;
std::vector<int>     *mu_calo_nTRTHighTHits;
std::vector<int>     *mu_calo_nBLSharedHits;
std::vector<int>     *mu_calo_nPixSharedHits;
std::vector<int>     *mu_calo_nPixHoles;
std::vector<int>     *mu_calo_nSCTSharedHits;
std::vector<int>     *mu_calo_nSCTHoles;
std::vector<int>     *mu_calo_nTRTOutliers;
std::vector<int>     *mu_calo_nTRTHighTOutliers;
std::vector<int>     *mu_calo_nGangedPixels;
std::vector<int>     *mu_calo_nPixelDeadSensors;
std::vector<int>     *mu_calo_nSCTDeadSensors;
std::vector<int>     *mu_calo_nTRTDeadStraws;
std::vector<int>     *mu_calo_expectBLayerHit;
std::vector<int>     *mu_calo_nMDTHits;
std::vector<int>     *mu_calo_nMDTHoles;
std::vector<int>     *mu_calo_nCSCEtaHits;
std::vector<int>     *mu_calo_nCSCEtaHoles;
std::vector<int>     *mu_calo_nCSCPhiHits;
std::vector<int>     *mu_calo_nCSCPhiHoles;
std::vector<int>     *mu_calo_nRPCEtaHits;
std::vector<int>     *mu_calo_nRPCEtaHoles;
std::vector<int>     *mu_calo_nRPCPhiHits;
std::vector<int>     *mu_calo_nRPCPhiHoles;
std::vector<int>     *mu_calo_nTGCEtaHits;
std::vector<int>     *mu_calo_nTGCEtaHoles;
std::vector<int>     *mu_calo_nTGCPhiHits;
std::vector<int>     *mu_calo_nTGCPhiHoles;
std::vector<int>     *mu_calo_nMDTBIHits;
std::vector<int>     *mu_calo_nMDTBMHits;
std::vector<int>     *mu_calo_nMDTBOHits;
std::vector<int>     *mu_calo_nMDTBEEHits;
std::vector<int>     *mu_calo_nMDTBIS78Hits;
std::vector<int>     *mu_calo_nMDTEIHits;
std::vector<int>     *mu_calo_nMDTEMHits;
std::vector<int>     *mu_calo_nMDTEOHits;
std::vector<int>     *mu_calo_nMDTEEHits;
std::vector<int>     *mu_calo_nRPCLayer1EtaHits;
std::vector<int>     *mu_calo_nRPCLayer2EtaHits;
std::vector<int>     *mu_calo_nRPCLayer3EtaHits;
std::vector<int>     *mu_calo_nRPCLayer1PhiHits;
std::vector<int>     *mu_calo_nRPCLayer2PhiHits;
std::vector<int>     *mu_calo_nRPCLayer3PhiHits;
std::vector<int>     *mu_calo_nTGCLayer1EtaHits;
std::vector<int>     *mu_calo_nTGCLayer2EtaHits;
std::vector<int>     *mu_calo_nTGCLayer3EtaHits;
std::vector<int>     *mu_calo_nTGCLayer4EtaHits;
std::vector<int>     *mu_calo_nTGCLayer1PhiHits;
std::vector<int>     *mu_calo_nTGCLayer2PhiHits;
std::vector<int>     *mu_calo_nTGCLayer3PhiHits;
std::vector<int>     *mu_calo_nTGCLayer4PhiHits;
std::vector<int>     *mu_calo_barrelSectors;
std::vector<int>     *mu_calo_endcapSectors;
std::vector<float>   *mu_calo_trackd0;
std::vector<float>   *mu_calo_trackz0;
std::vector<float>   *mu_calo_trackphi;
std::vector<float>   *mu_calo_tracktheta;
std::vector<float>   *mu_calo_trackqoverp;
std::vector<float>   *mu_calo_trackcov_d0;
std::vector<float>   *mu_calo_trackcov_z0;
std::vector<float>   *mu_calo_trackcov_phi;
std::vector<float>   *mu_calo_trackcov_theta;
std::vector<float>   *mu_calo_trackcov_qoverp;
std::vector<float>   *mu_calo_trackcov_d0_z0;
std::vector<float>   *mu_calo_trackcov_d0_phi;
std::vector<float>   *mu_calo_trackcov_d0_theta;
std::vector<float>   *mu_calo_trackcov_d0_qoverp;
std::vector<float>   *mu_calo_trackcov_z0_phi;
std::vector<float>   *mu_calo_trackcov_z0_theta;
std::vector<float>   *mu_calo_trackcov_z0_qoverp;
std::vector<float>   *mu_calo_trackcov_phi_theta;
std::vector<float>   *mu_calo_trackcov_phi_qoverp;
std::vector<float>   *mu_calo_trackcov_theta_qoverp;
std::vector<float>   *mu_calo_trackfitchi2;
std::vector<int>     *mu_calo_trackfitndof;
std::vector<int>     *mu_calo_hastrack;
std::vector<float>   *mu_calo_trackd0beam;
std::vector<float>   *mu_calo_trackz0beam;
std::vector<float>   *mu_calo_tracksigd0beam;
std::vector<float>   *mu_calo_tracksigz0beam;
std::vector<float>   *mu_calo_trackd0pv;
std::vector<float>   *mu_calo_trackz0pv;
std::vector<float>   *mu_calo_tracksigd0pv;
std::vector<float>   *mu_calo_tracksigz0pv;
std::vector<float>   *mu_calo_trackIPEstimate_d0_biasedpvunbiased;
std::vector<float>   *mu_calo_trackIPEstimate_z0_biasedpvunbiased;
std::vector<float>   *mu_calo_trackIPEstimate_sigd0_biasedpvunbiased;
std::vector<float>   *mu_calo_trackIPEstimate_sigz0_biasedpvunbiased;
std::vector<float>   *mu_calo_trackIPEstimate_d0_unbiasedpvunbiased;
std::vector<float>   *mu_calo_trackIPEstimate_z0_unbiasedpvunbiased;
std::vector<float>   *mu_calo_trackIPEstimate_sigd0_unbiasedpvunbiased;
std::vector<float>   *mu_calo_trackIPEstimate_sigz0_unbiasedpvunbiased;
std::vector<float>   *mu_calo_trackd0pvunbiased;
std::vector<float>   *mu_calo_trackz0pvunbiased;
std::vector<float>   *mu_calo_tracksigd0pvunbiased;
std::vector<float>   *mu_calo_tracksigz0pvunbiased;
std::vector<float>   *mu_calo_truth_dr;
std::vector<float>   *mu_calo_truth_E;
std::vector<float>   *mu_calo_truth_pt;
std::vector<float>   *mu_calo_truth_eta;
std::vector<float>   *mu_calo_truth_phi;
std::vector<int>     *mu_calo_truth_type;
std::vector<int>     *mu_calo_truth_status;
std::vector<int>     *mu_calo_truth_barcode;
std::vector<int>     *mu_calo_truth_mothertype;
std::vector<int>     *mu_calo_truth_motherbarcode;
std::vector<int>     *mu_calo_truth_matched;
std::vector<float>   *mu_calo_EFCB_dr;
std::vector<int>     *mu_calo_EFCB_n;
std::vector<std::vector<int> > *mu_calo_EFCB_MuonType;
std::vector<std::vector<float> > *mu_calo_EFCB_pt;
std::vector<std::vector<float> > *mu_calo_EFCB_eta;
std::vector<std::vector<float> > *mu_calo_EFCB_phi;
std::vector<std::vector<int> > *mu_calo_EFCB_hasCB;
std::vector<int>     *mu_calo_EFCB_matched;
std::vector<float>   *mu_calo_EFMG_dr;
std::vector<int>     *mu_calo_EFMG_n;
std::vector<std::vector<int> > *mu_calo_EFMG_MuonType;
std::vector<std::vector<float> > *mu_calo_EFMG_pt;
std::vector<std::vector<float> > *mu_calo_EFMG_eta;
std::vector<std::vector<float> > *mu_calo_EFMG_phi;
std::vector<std::vector<int> > *mu_calo_EFMG_hasMG;
std::vector<int>     *mu_calo_EFMG_matched;
std::vector<float>   *mu_calo_EFME_dr;
std::vector<int>     *mu_calo_EFME_n;
std::vector<std::vector<int> > *mu_calo_EFME_MuonType;
std::vector<std::vector<float> > *mu_calo_EFME_pt;
std::vector<std::vector<float> > *mu_calo_EFME_eta;
std::vector<std::vector<float> > *mu_calo_EFME_phi;
std::vector<std::vector<int> > *mu_calo_EFME_hasME;
std::vector<int>     *mu_calo_EFME_matched;
std::vector<float>   *mu_calo_L2CB_dr;
std::vector<float>   *mu_calo_L2CB_pt;
std::vector<float>   *mu_calo_L2CB_eta;
std::vector<float>   *mu_calo_L2CB_phi;
std::vector<float>   *mu_calo_L2CB_id_pt;
std::vector<float>   *mu_calo_L2CB_ms_pt;
std::vector<int>     *mu_calo_L2CB_nPixHits;
std::vector<int>     *mu_calo_L2CB_nSCTHits;
std::vector<int>     *mu_calo_L2CB_nTRTHits;
std::vector<int>     *mu_calo_L2CB_nTRTHighTHits;
std::vector<int>     *mu_calo_L2CB_matched;
std::vector<float>   *mu_calo_L1_dr;
std::vector<float>   *mu_calo_L1_pt;
std::vector<float>   *mu_calo_L1_eta;
std::vector<float>   *mu_calo_L1_phi;
std::vector<short>   *mu_calo_L1_thrNumber;
std::vector<short>   *mu_calo_L1_RoINumber;
std::vector<short>   *mu_calo_L1_sectorAddress;
std::vector<int>     *mu_calo_L1_firstCandidate;
std::vector<int>     *mu_calo_L1_moreCandInRoI;
std::vector<int>     *mu_calo_L1_moreCandInSector;
std::vector<short>   *mu_calo_L1_source;
std::vector<short>   *mu_calo_L1_hemisphere;
std::vector<short>   *mu_calo_L1_charge;
std::vector<int>     *mu_calo_L1_vetoed;
std::vector<int>     *mu_calo_L1_matched;

UInt_t          trig_DB_SMK;
std::vector<float>   *trig_L1_mu_eta;
std::vector<float>   *trig_L1_mu_phi;
std::vector<std::string>  *trig_L1_mu_thrName;
std::vector<float>   *trig_L2_muonfeature_eta;
std::vector<float>   *trig_L2_muonfeature_phi;
std::vector<float>   *trig_L2_combmuonfeature_eta;
std::vector<float>   *trig_L2_combmuonfeature_phi;
std::vector<std::vector<float> > *trig_EF_trigmuonef_track_SA_pt;
std::vector<std::vector<float> > *trig_EF_trigmuonef_track_SA_eta;
std::vector<std::vector<float> > *trig_EF_trigmuonef_track_SA_phi;
std::vector<std::vector<float> > *trig_EF_trigmuonef_track_CB_pt;
std::vector<std::vector<float> > *trig_EF_trigmuonef_track_CB_eta;
std::vector<std::vector<float> > *trig_EF_trigmuonef_track_CB_phi;
std::vector<std::vector<int> > *trig_EF_trigmuonef_track_MuonType;
std::vector<std::vector<float> > *trig_EF_trigmugirl_track_CB_pt;
std::vector<std::vector<float> > *trig_EF_trigmugirl_track_CB_eta;
std::vector<std::vector<float> > *trig_EF_trigmugirl_track_CB_phi;
Int_t           trig_Nav_n;
std::vector<short>   *trig_Nav_chain_ChainId;
std::vector<std::vector<int> > *trig_Nav_chain_RoIType;
std::vector<std::vector<int> > *trig_Nav_chain_RoIIndex;
std::vector<int>     *trig_RoI_L2_mu_MuonFeature;
std::vector<int>     *trig_RoI_L2_mu_CombinedMuonFeature;
std::vector<int>     *trig_RoI_L2_mu_CombinedMuonFeatureStatus;
std::vector<int>     *trig_RoI_L2_mu_Muon_ROI;
std::vector<int>     *trig_RoI_EF_mu_Muon_ROI;
/*p956 ntuples*/
std::vector<std::vector<int> > *trig_RoI_EF_mu_TrigMuonEFInfoContainer_MuonEFInfo;
std::vector<std::vector<int> > *trig_RoI_EF_mu_TrigMuonEFInfoContainer_MuonEFInfoStatus;
std::vector<std::vector<int> > *trig_RoI_EF_mu_TrigMuonEFInfoContainer_eMuonEFInfo;
std::vector<std::vector<int> > *trig_RoI_EF_mu_TrigMuonEFInfoContainer_eMuonEFInfoStatus;
/*p869 ntuples*/
std::vector<std::vector<int> > *trig_RoI_EF_mu_TrigMuonEFInfoContainer;
std::vector<std::vector<int> > *trig_RoI_EF_mu_TrigMuonEFInfoContainerStatus;

std::vector<std::vector<int> > *trig_RoI_EF_mu_TrigMuonEFIsolationContainer;
std::vector<std::vector<int> > *trig_RoI_EF_mu_TrigMuonEFIsolationContainerStatus;

/* List of branches */
/*2011 trigger*/
TBranch        *b_EF_2mu10_loose;   //!
TBranch        *b_EF_mu18;   //!
TBranch        *b_EF_mu18_MG;   //!
TBranch        *b_EF_mu18_MG_medium;   //!
TBranch        *b_EF_mu18_medium;   //!
/*2012 trigger*/
TBranch        *b_EF_mu24i_tight;   //!
TBranch        *b_EF_mu36_tight;   //!
TBranch        *b_EF_2mu13;   //!
TBranch        *b_EF_mu18_tight_mu8_EFFS;   //!

TBranch        *b_mu_staco_n;   //!
TBranch        *b_mu_staco_E;   //!
TBranch        *b_mu_staco_pt;   //!
TBranch        *b_mu_staco_m;   //!
TBranch        *b_mu_staco_eta;   //!
TBranch        *b_mu_staco_phi;   //!
TBranch        *b_mu_staco_px;   //!
TBranch        *b_mu_staco_py;   //!
TBranch        *b_mu_staco_pz;   //!
TBranch        *b_mu_staco_charge;   //!
TBranch        *b_mu_staco_allauthor;   //!
TBranch        *b_mu_staco_author;   //!
TBranch        *b_mu_staco_beta;   //!
TBranch        *b_mu_staco_isMuonLikelihood;   //!
TBranch        *b_mu_staco_matchchi2;   //!
TBranch        *b_mu_staco_matchndof;   //!
TBranch        *b_mu_staco_etcone20;   //!
TBranch        *b_mu_staco_etcone30;   //!
TBranch        *b_mu_staco_etcone40;   //!
TBranch        *b_mu_staco_nucone20;   //!
TBranch        *b_mu_staco_nucone30;   //!
TBranch        *b_mu_staco_nucone40;   //!
TBranch        *b_mu_staco_ptcone20;   //!
TBranch        *b_mu_staco_ptcone30;   //!
TBranch        *b_mu_staco_ptcone40;   //!
TBranch        *b_mu_staco_energyLossPar;   //!
TBranch        *b_mu_staco_energyLossErr;   //!
TBranch        *b_mu_staco_etCore;   //!
TBranch        *b_mu_staco_energyLossType;   //!
TBranch        *b_mu_staco_caloMuonIdTag;   //!
TBranch        *b_mu_staco_caloLRLikelihood;   //!
TBranch        *b_mu_staco_bestMatch;   //!
TBranch        *b_mu_staco_isStandAloneMuon;   //!
TBranch        *b_mu_staco_isCombinedMuon;   //!
TBranch        *b_mu_staco_isLowPtReconstructedMuon;   //!
TBranch        *b_mu_staco_isSegmentTaggedMuon;   //!
TBranch        *b_mu_staco_isCaloMuonId;   //!
TBranch        *b_mu_staco_alsoFoundByLowPt;   //!
TBranch        *b_mu_staco_alsoFoundByCaloMuonId;   //!
TBranch        *b_mu_staco_loose;   //!
TBranch        *b_mu_staco_medium;   //!
TBranch        *b_mu_staco_tight;   //!
TBranch        *b_mu_staco_d0_exPV;   //!
TBranch        *b_mu_staco_z0_exPV;   //!
TBranch        *b_mu_staco_phi_exPV;   //!
TBranch        *b_mu_staco_theta_exPV;   //!
TBranch        *b_mu_staco_qoverp_exPV;   //!
TBranch        *b_mu_staco_cb_d0_exPV;   //!
TBranch        *b_mu_staco_cb_z0_exPV;   //!
TBranch        *b_mu_staco_cb_phi_exPV;   //!
TBranch        *b_mu_staco_cb_theta_exPV;   //!
TBranch        *b_mu_staco_cb_qoverp_exPV;   //!
TBranch        *b_mu_staco_id_d0_exPV;   //!
TBranch        *b_mu_staco_id_z0_exPV;   //!
TBranch        *b_mu_staco_id_phi_exPV;   //!
TBranch        *b_mu_staco_id_theta_exPV;   //!
TBranch        *b_mu_staco_id_qoverp_exPV;   //!
TBranch        *b_mu_staco_me_d0_exPV;   //!
TBranch        *b_mu_staco_me_z0_exPV;   //!
TBranch        *b_mu_staco_me_phi_exPV;   //!
TBranch        *b_mu_staco_me_theta_exPV;   //!
TBranch        *b_mu_staco_me_qoverp_exPV;   //!
TBranch        *b_mu_staco_ie_d0_exPV;   //!
TBranch        *b_mu_staco_ie_z0_exPV;   //!
TBranch        *b_mu_staco_ie_phi_exPV;   //!
TBranch        *b_mu_staco_ie_theta_exPV;   //!
TBranch        *b_mu_staco_ie_qoverp_exPV;   //!
TBranch        *b_mu_staco_SpaceTime_detID;   //!
TBranch        *b_mu_staco_SpaceTime_t;   //!
TBranch        *b_mu_staco_SpaceTime_tError;   //!
TBranch        *b_mu_staco_SpaceTime_weight;   //!
TBranch        *b_mu_staco_SpaceTime_x;   //!
TBranch        *b_mu_staco_SpaceTime_y;   //!
TBranch        *b_mu_staco_SpaceTime_z;   //!
TBranch        *b_mu_staco_cov_d0_exPV;   //!
TBranch        *b_mu_staco_cov_z0_exPV;   //!
TBranch        *b_mu_staco_cov_phi_exPV;   //!
TBranch        *b_mu_staco_cov_theta_exPV;   //!
TBranch        *b_mu_staco_cov_qoverp_exPV;   //!
TBranch        *b_mu_staco_cov_d0_z0_exPV;   //!
TBranch        *b_mu_staco_cov_d0_phi_exPV;   //!
TBranch        *b_mu_staco_cov_d0_theta_exPV;   //!
TBranch        *b_mu_staco_cov_d0_qoverp_exPV;   //!
TBranch        *b_mu_staco_cov_z0_phi_exPV;   //!
TBranch        *b_mu_staco_cov_z0_theta_exPV;   //!
TBranch        *b_mu_staco_cov_z0_qoverp_exPV;   //!
TBranch        *b_mu_staco_cov_phi_theta_exPV;   //!
TBranch        *b_mu_staco_cov_phi_qoverp_exPV;   //!
TBranch        *b_mu_staco_cov_theta_qoverp_exPV;   //!
TBranch        *b_mu_staco_id_cov_d0_exPV;   //!
TBranch        *b_mu_staco_id_cov_z0_exPV;   //!
TBranch        *b_mu_staco_id_cov_phi_exPV;   //!
TBranch        *b_mu_staco_id_cov_theta_exPV;   //!
TBranch        *b_mu_staco_id_cov_qoverp_exPV;   //!
TBranch        *b_mu_staco_id_cov_d0_z0_exPV;   //!
TBranch        *b_mu_staco_id_cov_d0_phi_exPV;   //!
TBranch        *b_mu_staco_id_cov_d0_theta_exPV;   //!
TBranch        *b_mu_staco_id_cov_d0_qoverp_exPV;   //!
TBranch        *b_mu_staco_id_cov_z0_phi_exPV;   //!
TBranch        *b_mu_staco_id_cov_z0_theta_exPV;   //!
TBranch        *b_mu_staco_id_cov_z0_qoverp_exPV;   //!
TBranch        *b_mu_staco_id_cov_phi_theta_exPV;   //!
TBranch        *b_mu_staco_id_cov_phi_qoverp_exPV;   //!
TBranch        *b_mu_staco_id_cov_theta_qoverp_exPV;   //!
TBranch        *b_mu_staco_me_cov_d0_exPV;   //!
TBranch        *b_mu_staco_me_cov_z0_exPV;   //!
TBranch        *b_mu_staco_me_cov_phi_exPV;   //!
TBranch        *b_mu_staco_me_cov_theta_exPV;   //!
TBranch        *b_mu_staco_me_cov_qoverp_exPV;   //!
TBranch        *b_mu_staco_me_cov_d0_z0_exPV;   //!
TBranch        *b_mu_staco_me_cov_d0_phi_exPV;   //!
TBranch        *b_mu_staco_me_cov_d0_theta_exPV;   //!
TBranch        *b_mu_staco_me_cov_d0_qoverp_exPV;   //!
TBranch        *b_mu_staco_me_cov_z0_phi_exPV;   //!
TBranch        *b_mu_staco_me_cov_z0_theta_exPV;   //!
TBranch        *b_mu_staco_me_cov_z0_qoverp_exPV;   //!
TBranch        *b_mu_staco_me_cov_phi_theta_exPV;   //!
TBranch        *b_mu_staco_me_cov_phi_qoverp_exPV;   //!
TBranch        *b_mu_staco_me_cov_theta_qoverp_exPV;   //!
TBranch        *b_mu_staco_ms_d0;   //!
TBranch        *b_mu_staco_ms_z0;   //!
TBranch        *b_mu_staco_ms_phi;   //!
TBranch        *b_mu_staco_ms_theta;   //!
TBranch        *b_mu_staco_ms_qoverp;   //!
TBranch        *b_mu_staco_id_d0;   //!
TBranch        *b_mu_staco_id_z0;   //!
TBranch        *b_mu_staco_id_phi;   //!
TBranch        *b_mu_staco_id_theta;   //!
TBranch        *b_mu_staco_id_qoverp;   //!
TBranch        *b_mu_staco_me_d0;   //!
TBranch        *b_mu_staco_me_z0;   //!
TBranch        *b_mu_staco_me_phi;   //!
TBranch        *b_mu_staco_me_theta;   //!
TBranch        *b_mu_staco_me_qoverp;   //!
TBranch        *b_mu_staco_ie_d0;   //!
TBranch        *b_mu_staco_ie_z0;   //!
TBranch        *b_mu_staco_ie_phi;   //!
TBranch        *b_mu_staco_ie_theta;   //!
TBranch        *b_mu_staco_ie_qoverp;   //!
TBranch        *b_mu_staco_nOutliersOnTrack;   //!
TBranch        *b_mu_staco_nBLHits;   //!
TBranch        *b_mu_staco_nPixHits;   //!
TBranch        *b_mu_staco_nSCTHits;   //!
TBranch        *b_mu_staco_nTRTHits;   //!
TBranch        *b_mu_staco_nTRTHighTHits;   //!
TBranch        *b_mu_staco_nBLSharedHits;   //!
TBranch        *b_mu_staco_nPixSharedHits;   //!
TBranch        *b_mu_staco_nPixHoles;   //!
TBranch        *b_mu_staco_nSCTSharedHits;   //!
TBranch        *b_mu_staco_nSCTHoles;   //!
TBranch        *b_mu_staco_nTRTOutliers;   //!
TBranch        *b_mu_staco_nTRTHighTOutliers;   //!
TBranch        *b_mu_staco_nGangedPixels;   //!
TBranch        *b_mu_staco_nPixelDeadSensors;   //!
TBranch        *b_mu_staco_nSCTDeadSensors;   //!
TBranch        *b_mu_staco_nTRTDeadStraws;   //!
TBranch        *b_mu_staco_expectBLayerHit;   //!
TBranch        *b_mu_staco_nMDTHits;   //!
TBranch        *b_mu_staco_nMDTHoles;   //!
TBranch        *b_mu_staco_nCSCEtaHits;   //!
TBranch        *b_mu_staco_nCSCEtaHoles;   //!
TBranch        *b_mu_staco_nCSCPhiHits;   //!
TBranch        *b_mu_staco_nCSCPhiHoles;   //!
TBranch        *b_mu_staco_nRPCEtaHits;   //!
TBranch        *b_mu_staco_nRPCEtaHoles;   //!
TBranch        *b_mu_staco_nRPCPhiHits;   //!
TBranch        *b_mu_staco_nRPCPhiHoles;   //!
TBranch        *b_mu_staco_nTGCEtaHits;   //!
TBranch        *b_mu_staco_nTGCEtaHoles;   //!
TBranch        *b_mu_staco_nTGCPhiHits;   //!
TBranch        *b_mu_staco_nTGCPhiHoles;   //!
TBranch        *b_mu_staco_nMDTBIHits;   //!
TBranch        *b_mu_staco_nMDTBMHits;   //!
TBranch        *b_mu_staco_nMDTBOHits;   //!
TBranch        *b_mu_staco_nMDTBEEHits;   //!
TBranch        *b_mu_staco_nMDTBIS78Hits;   //!
TBranch        *b_mu_staco_nMDTEIHits;   //!
TBranch        *b_mu_staco_nMDTEMHits;   //!
TBranch        *b_mu_staco_nMDTEOHits;   //!
TBranch        *b_mu_staco_nMDTEEHits;   //!
TBranch        *b_mu_staco_nRPCLayer1EtaHits;   //!
TBranch        *b_mu_staco_nRPCLayer2EtaHits;   //!
TBranch        *b_mu_staco_nRPCLayer3EtaHits;   //!
TBranch        *b_mu_staco_nRPCLayer1PhiHits;   //!
TBranch        *b_mu_staco_nRPCLayer2PhiHits;   //!
TBranch        *b_mu_staco_nRPCLayer3PhiHits;   //!
TBranch        *b_mu_staco_nTGCLayer1EtaHits;   //!
TBranch        *b_mu_staco_nTGCLayer2EtaHits;   //!
TBranch        *b_mu_staco_nTGCLayer3EtaHits;   //!
TBranch        *b_mu_staco_nTGCLayer4EtaHits;   //!
TBranch        *b_mu_staco_nTGCLayer1PhiHits;   //!
TBranch        *b_mu_staco_nTGCLayer2PhiHits;   //!
TBranch        *b_mu_staco_nTGCLayer3PhiHits;   //!
TBranch        *b_mu_staco_nTGCLayer4PhiHits;   //!
TBranch        *b_mu_staco_barrelSectors;   //!
TBranch        *b_mu_staco_endcapSectors;   //!
TBranch        *b_mu_staco_trackd0;   //!
TBranch        *b_mu_staco_trackz0;   //!
TBranch        *b_mu_staco_trackphi;   //!
TBranch        *b_mu_staco_tracktheta;   //!
TBranch        *b_mu_staco_trackqoverp;   //!
TBranch        *b_mu_staco_trackcov_d0;   //!
TBranch        *b_mu_staco_trackcov_z0;   //!
TBranch        *b_mu_staco_trackcov_phi;   //!
TBranch        *b_mu_staco_trackcov_theta;   //!
TBranch        *b_mu_staco_trackcov_qoverp;   //!
TBranch        *b_mu_staco_trackcov_d0_z0;   //!
TBranch        *b_mu_staco_trackcov_d0_phi;   //!
TBranch        *b_mu_staco_trackcov_d0_theta;   //!
TBranch        *b_mu_staco_trackcov_d0_qoverp;   //!
TBranch        *b_mu_staco_trackcov_z0_phi;   //!
TBranch        *b_mu_staco_trackcov_z0_theta;   //!
TBranch        *b_mu_staco_trackcov_z0_qoverp;   //!
TBranch        *b_mu_staco_trackcov_phi_theta;   //!
TBranch        *b_mu_staco_trackcov_phi_qoverp;   //!
TBranch        *b_mu_staco_trackcov_theta_qoverp;   //!
TBranch        *b_mu_staco_trackfitchi2;   //!
TBranch        *b_mu_staco_trackfitndof;   //!
TBranch        *b_mu_staco_hastrack;   //!
TBranch        *b_mu_staco_trackd0beam;   //!
TBranch        *b_mu_staco_trackz0beam;   //!
TBranch        *b_mu_staco_tracksigd0beam;   //!
TBranch        *b_mu_staco_tracksigz0beam;   //!
TBranch        *b_mu_staco_trackd0pv;   //!
TBranch        *b_mu_staco_trackz0pv;   //!
TBranch        *b_mu_staco_tracksigd0pv;   //!
TBranch        *b_mu_staco_tracksigz0pv;   //!
TBranch        *b_mu_staco_trackIPEstimate_d0_biasedpvunbiased;   //!
TBranch        *b_mu_staco_trackIPEstimate_z0_biasedpvunbiased;   //!
TBranch        *b_mu_staco_trackIPEstimate_sigd0_biasedpvunbiased;   //!
TBranch        *b_mu_staco_trackIPEstimate_sigz0_biasedpvunbiased;   //!
TBranch        *b_mu_staco_trackIPEstimate_d0_unbiasedpvunbiased;   //!
TBranch        *b_mu_staco_trackIPEstimate_z0_unbiasedpvunbiased;   //!
TBranch        *b_mu_staco_trackIPEstimate_sigd0_unbiasedpvunbiased;   //!
TBranch        *b_mu_staco_trackIPEstimate_sigz0_unbiasedpvunbiased;   //!
TBranch        *b_mu_staco_trackd0pvunbiased;   //!
TBranch        *b_mu_staco_trackz0pvunbiased;   //!
TBranch        *b_mu_staco_tracksigd0pvunbiased;   //!
TBranch        *b_mu_staco_tracksigz0pvunbiased;   //!
TBranch        *b_mu_staco_type;   //!
TBranch        *b_mu_staco_origin;   //!
TBranch        *b_mu_staco_truth_dr;   //!
TBranch        *b_mu_staco_truth_E;   //!
TBranch        *b_mu_staco_truth_pt;   //!
TBranch        *b_mu_staco_truth_eta;   //!
TBranch        *b_mu_staco_truth_phi;   //!
TBranch        *b_mu_staco_truth_type;   //!
TBranch        *b_mu_staco_truth_status;   //!
TBranch        *b_mu_staco_truth_barcode;   //!
TBranch        *b_mu_staco_truth_mothertype;   //!
TBranch        *b_mu_staco_truth_motherbarcode;   //!
TBranch        *b_mu_staco_truth_matched;   //!
TBranch        *b_mu_staco_EFCB_dr;   //!
TBranch        *b_mu_staco_EFCB_index;   //!
TBranch        *b_mu_staco_EFMG_dr;   //!
TBranch        *b_mu_staco_EFMG_index;   //!
TBranch        *b_mu_staco_EFME_dr;   //!
TBranch        *b_mu_staco_EFME_index;   //!
TBranch        *b_mu_calo_n;   //!
TBranch        *b_mu_calo_E;   //!
TBranch        *b_mu_calo_pt;   //!
TBranch        *b_mu_calo_m;   //!
TBranch        *b_mu_calo_eta;   //!
TBranch        *b_mu_calo_phi;   //!
TBranch        *b_mu_calo_px;   //!
TBranch        *b_mu_calo_py;   //!
TBranch        *b_mu_calo_pz;   //!
TBranch        *b_mu_calo_charge;   //!
TBranch        *b_mu_calo_allauthor;   //!
TBranch        *b_mu_calo_author;   //!
TBranch        *b_mu_calo_beta;   //!
TBranch        *b_mu_calo_isMuonLikelihood;   //!
TBranch        *b_mu_calo_matchchi2;   //!
TBranch        *b_mu_calo_matchndof;   //!
TBranch        *b_mu_calo_etcone20;   //!
TBranch        *b_mu_calo_etcone30;   //!
TBranch        *b_mu_calo_etcone40;   //!
TBranch        *b_mu_calo_nucone20;   //!
TBranch        *b_mu_calo_nucone30;   //!
TBranch        *b_mu_calo_nucone40;   //!
TBranch        *b_mu_calo_ptcone20;   //!
TBranch        *b_mu_calo_ptcone30;   //!
TBranch        *b_mu_calo_ptcone40;   //!
TBranch        *b_mu_calo_energyLossPar;   //!
TBranch        *b_mu_calo_energyLossErr;   //!
TBranch        *b_mu_calo_etCore;   //!
TBranch        *b_mu_calo_energyLossType;   //!
TBranch        *b_mu_calo_caloMuonIdTag;   //!
TBranch        *b_mu_calo_caloLRLikelihood;   //!
TBranch        *b_mu_calo_bestMatch;   //!
TBranch        *b_mu_calo_isStandAloneMuon;   //!
TBranch        *b_mu_calo_isCombinedMuon;   //!
TBranch        *b_mu_calo_isLowPtReconstructedMuon;   //!
TBranch        *b_mu_calo_isSegmentTaggedMuon;   //!
TBranch        *b_mu_calo_isCaloMuonId;   //!
TBranch        *b_mu_calo_alsoFoundByLowPt;   //!
TBranch        *b_mu_calo_alsoFoundByCaloMuonId;   //!
TBranch        *b_mu_calo_loose;   //!
TBranch        *b_mu_calo_medium;   //!
TBranch        *b_mu_calo_tight;   //!
TBranch        *b_mu_calo_d0_exPV;   //!
TBranch        *b_mu_calo_z0_exPV;   //!
TBranch        *b_mu_calo_phi_exPV;   //!
TBranch        *b_mu_calo_theta_exPV;   //!
TBranch        *b_mu_calo_qoverp_exPV;   //!
TBranch        *b_mu_calo_cb_d0_exPV;   //!
TBranch        *b_mu_calo_cb_z0_exPV;   //!
TBranch        *b_mu_calo_cb_phi_exPV;   //!
TBranch        *b_mu_calo_cb_theta_exPV;   //!
TBranch        *b_mu_calo_cb_qoverp_exPV;   //!
TBranch        *b_mu_calo_id_d0_exPV;   //!
TBranch        *b_mu_calo_id_z0_exPV;   //!
TBranch        *b_mu_calo_id_phi_exPV;   //!
TBranch        *b_mu_calo_id_theta_exPV;   //!
TBranch        *b_mu_calo_id_qoverp_exPV;   //!
TBranch        *b_mu_calo_me_d0_exPV;   //!
TBranch        *b_mu_calo_me_z0_exPV;   //!
TBranch        *b_mu_calo_me_phi_exPV;   //!
TBranch        *b_mu_calo_me_theta_exPV;   //!
TBranch        *b_mu_calo_me_qoverp_exPV;   //!
TBranch        *b_mu_calo_ie_d0_exPV;   //!
TBranch        *b_mu_calo_ie_z0_exPV;   //!
TBranch        *b_mu_calo_ie_phi_exPV;   //!
TBranch        *b_mu_calo_ie_theta_exPV;   //!
TBranch        *b_mu_calo_ie_qoverp_exPV;   //!
TBranch        *b_mu_calo_SpaceTime_detID;   //!
TBranch        *b_mu_calo_SpaceTime_t;   //!
TBranch        *b_mu_calo_SpaceTime_tError;   //!
TBranch        *b_mu_calo_SpaceTime_weight;   //!
TBranch        *b_mu_calo_SpaceTime_x;   //!
TBranch        *b_mu_calo_SpaceTime_y;   //!
TBranch        *b_mu_calo_SpaceTime_z;   //!
TBranch        *b_mu_calo_cov_d0_exPV;   //!
TBranch        *b_mu_calo_cov_z0_exPV;   //!
TBranch        *b_mu_calo_cov_phi_exPV;   //!
TBranch        *b_mu_calo_cov_theta_exPV;   //!
TBranch        *b_mu_calo_cov_qoverp_exPV;   //!
TBranch        *b_mu_calo_cov_d0_z0_exPV;   //!
TBranch        *b_mu_calo_cov_d0_phi_exPV;   //!
TBranch        *b_mu_calo_cov_d0_theta_exPV;   //!
TBranch        *b_mu_calo_cov_d0_qoverp_exPV;   //!
TBranch        *b_mu_calo_cov_z0_phi_exPV;   //!
TBranch        *b_mu_calo_cov_z0_theta_exPV;   //!
TBranch        *b_mu_calo_cov_z0_qoverp_exPV;   //!
TBranch        *b_mu_calo_cov_phi_theta_exPV;   //!
TBranch        *b_mu_calo_cov_phi_qoverp_exPV;   //!
TBranch        *b_mu_calo_cov_theta_qoverp_exPV;   //!
TBranch        *b_mu_calo_id_cov_d0_exPV;   //!
TBranch        *b_mu_calo_id_cov_z0_exPV;   //!
TBranch        *b_mu_calo_id_cov_phi_exPV;   //!
TBranch        *b_mu_calo_id_cov_theta_exPV;   //!
TBranch        *b_mu_calo_id_cov_qoverp_exPV;   //!
TBranch        *b_mu_calo_id_cov_d0_z0_exPV;   //!
TBranch        *b_mu_calo_id_cov_d0_phi_exPV;   //!
TBranch        *b_mu_calo_id_cov_d0_theta_exPV;   //!
TBranch        *b_mu_calo_id_cov_d0_qoverp_exPV;   //!
TBranch        *b_mu_calo_id_cov_z0_phi_exPV;   //!
TBranch        *b_mu_calo_id_cov_z0_theta_exPV;   //!
TBranch        *b_mu_calo_id_cov_z0_qoverp_exPV;   //!
TBranch        *b_mu_calo_id_cov_phi_theta_exPV;   //!
TBranch        *b_mu_calo_id_cov_phi_qoverp_exPV;   //!
TBranch        *b_mu_calo_id_cov_theta_qoverp_exPV;   //!
TBranch        *b_mu_calo_me_cov_d0_exPV;   //!
TBranch        *b_mu_calo_me_cov_z0_exPV;   //!
TBranch        *b_mu_calo_me_cov_phi_exPV;   //!
TBranch        *b_mu_calo_me_cov_theta_exPV;   //!
TBranch        *b_mu_calo_me_cov_qoverp_exPV;   //!
TBranch        *b_mu_calo_me_cov_d0_z0_exPV;   //!
TBranch        *b_mu_calo_me_cov_d0_phi_exPV;   //!
TBranch        *b_mu_calo_me_cov_d0_theta_exPV;   //!
TBranch        *b_mu_calo_me_cov_d0_qoverp_exPV;   //!
TBranch        *b_mu_calo_me_cov_z0_phi_exPV;   //!
TBranch        *b_mu_calo_me_cov_z0_theta_exPV;   //!
TBranch        *b_mu_calo_me_cov_z0_qoverp_exPV;   //!
TBranch        *b_mu_calo_me_cov_phi_theta_exPV;   //!
TBranch        *b_mu_calo_me_cov_phi_qoverp_exPV;   //!
TBranch        *b_mu_calo_me_cov_theta_qoverp_exPV;   //!
TBranch        *b_mu_calo_ms_d0;   //!
TBranch        *b_mu_calo_ms_z0;   //!
TBranch        *b_mu_calo_ms_phi;   //!
TBranch        *b_mu_calo_ms_theta;   //!
TBranch        *b_mu_calo_ms_qoverp;   //!
TBranch        *b_mu_calo_id_d0;   //!
TBranch        *b_mu_calo_id_z0;   //!
TBranch        *b_mu_calo_id_phi;   //!
TBranch        *b_mu_calo_id_theta;   //!
TBranch        *b_mu_calo_id_qoverp;   //!
TBranch        *b_mu_calo_me_d0;   //!
TBranch        *b_mu_calo_me_z0;   //!
TBranch        *b_mu_calo_me_phi;   //!
TBranch        *b_mu_calo_me_theta;   //!
TBranch        *b_mu_calo_me_qoverp;   //!
TBranch        *b_mu_calo_ie_d0;   //!
TBranch        *b_mu_calo_ie_z0;   //!
TBranch        *b_mu_calo_ie_phi;   //!
TBranch        *b_mu_calo_ie_theta;   //!
TBranch        *b_mu_calo_ie_qoverp;   //!
TBranch        *b_mu_calo_nOutliersOnTrack;   //!
TBranch        *b_mu_calo_nBLHits;   //!
TBranch        *b_mu_calo_nPixHits;   //!
TBranch        *b_mu_calo_nSCTHits;   //!
TBranch        *b_mu_calo_nTRTHits;   //!
TBranch        *b_mu_calo_nTRTHighTHits;   //!
TBranch        *b_mu_calo_nBLSharedHits;   //!
TBranch        *b_mu_calo_nPixSharedHits;   //!
TBranch        *b_mu_calo_nPixHoles;   //!
TBranch        *b_mu_calo_nSCTSharedHits;   //!
TBranch        *b_mu_calo_nSCTHoles;   //!
TBranch        *b_mu_calo_nTRTOutliers;   //!
TBranch        *b_mu_calo_nTRTHighTOutliers;   //!
TBranch        *b_mu_calo_nGangedPixels;   //!
TBranch        *b_mu_calo_nPixelDeadSensors;   //!
TBranch        *b_mu_calo_nSCTDeadSensors;   //!
TBranch        *b_mu_calo_nTRTDeadStraws;   //!
TBranch        *b_mu_calo_expectBLayerHit;   //!
TBranch        *b_mu_calo_nMDTHits;   //!
TBranch        *b_mu_calo_nMDTHoles;   //!
TBranch        *b_mu_calo_nCSCEtaHits;   //!
TBranch        *b_mu_calo_nCSCEtaHoles;   //!
TBranch        *b_mu_calo_nCSCPhiHits;   //!
TBranch        *b_mu_calo_nCSCPhiHoles;   //!
TBranch        *b_mu_calo_nRPCEtaHits;   //!
TBranch        *b_mu_calo_nRPCEtaHoles;   //!
TBranch        *b_mu_calo_nRPCPhiHits;   //!
TBranch        *b_mu_calo_nRPCPhiHoles;   //!
TBranch        *b_mu_calo_nTGCEtaHits;   //!
TBranch        *b_mu_calo_nTGCEtaHoles;   //!
TBranch        *b_mu_calo_nTGCPhiHits;   //!
TBranch        *b_mu_calo_nTGCPhiHoles;   //!
TBranch        *b_mu_calo_nMDTBIHits;   //!
TBranch        *b_mu_calo_nMDTBMHits;   //!
TBranch        *b_mu_calo_nMDTBOHits;   //!
TBranch        *b_mu_calo_nMDTBEEHits;   //!
TBranch        *b_mu_calo_nMDTBIS78Hits;   //!
TBranch        *b_mu_calo_nMDTEIHits;   //!
TBranch        *b_mu_calo_nMDTEMHits;   //!
TBranch        *b_mu_calo_nMDTEOHits;   //!
TBranch        *b_mu_calo_nMDTEEHits;   //!
TBranch        *b_mu_calo_nRPCLayer1EtaHits;   //!
TBranch        *b_mu_calo_nRPCLayer2EtaHits;   //!
TBranch        *b_mu_calo_nRPCLayer3EtaHits;   //!
TBranch        *b_mu_calo_nRPCLayer1PhiHits;   //!
TBranch        *b_mu_calo_nRPCLayer2PhiHits;   //!
TBranch        *b_mu_calo_nRPCLayer3PhiHits;   //!
TBranch        *b_mu_calo_nTGCLayer1EtaHits;   //!
TBranch        *b_mu_calo_nTGCLayer2EtaHits;   //!
TBranch        *b_mu_calo_nTGCLayer3EtaHits;   //!
TBranch        *b_mu_calo_nTGCLayer4EtaHits;   //!
TBranch        *b_mu_calo_nTGCLayer1PhiHits;   //!
TBranch        *b_mu_calo_nTGCLayer2PhiHits;   //!
TBranch        *b_mu_calo_nTGCLayer3PhiHits;   //!
TBranch        *b_mu_calo_nTGCLayer4PhiHits;   //!
TBranch        *b_mu_calo_barrelSectors;   //!
TBranch        *b_mu_calo_endcapSectors;   //!
TBranch        *b_mu_calo_trackd0;   //!
TBranch        *b_mu_calo_trackz0;   //!
TBranch        *b_mu_calo_trackphi;   //!
TBranch        *b_mu_calo_tracktheta;   //!
TBranch        *b_mu_calo_trackqoverp;   //!
TBranch        *b_mu_calo_trackcov_d0;   //!
TBranch        *b_mu_calo_trackcov_z0;   //!
TBranch        *b_mu_calo_trackcov_phi;   //!
TBranch        *b_mu_calo_trackcov_theta;   //!
TBranch        *b_mu_calo_trackcov_qoverp;   //!
TBranch        *b_mu_calo_trackcov_d0_z0;   //!
TBranch        *b_mu_calo_trackcov_d0_phi;   //!
TBranch        *b_mu_calo_trackcov_d0_theta;   //!
TBranch        *b_mu_calo_trackcov_d0_qoverp;   //!
TBranch        *b_mu_calo_trackcov_z0_phi;   //!
TBranch        *b_mu_calo_trackcov_z0_theta;   //!
TBranch        *b_mu_calo_trackcov_z0_qoverp;   //!
TBranch        *b_mu_calo_trackcov_phi_theta;   //!
TBranch        *b_mu_calo_trackcov_phi_qoverp;   //!
TBranch        *b_mu_calo_trackcov_theta_qoverp;   //!
TBranch        *b_mu_calo_trackfitchi2;   //!
TBranch        *b_mu_calo_trackfitndof;   //!
TBranch        *b_mu_calo_hastrack;   //!
TBranch        *b_mu_calo_trackd0beam;   //!
TBranch        *b_mu_calo_trackz0beam;   //!
TBranch        *b_mu_calo_tracksigd0beam;   //!
TBranch        *b_mu_calo_tracksigz0beam;   //!
TBranch        *b_mu_calo_trackd0pv;   //!
TBranch        *b_mu_calo_trackz0pv;   //!
TBranch        *b_mu_calo_tracksigd0pv;   //!
TBranch        *b_mu_calo_tracksigz0pv;   //!
TBranch        *b_mu_calo_trackIPEstimate_d0_biasedpvunbiased;   //!
TBranch        *b_mu_calo_trackIPEstimate_z0_biasedpvunbiased;   //!
TBranch        *b_mu_calo_trackIPEstimate_sigd0_biasedpvunbiased;   //!
TBranch        *b_mu_calo_trackIPEstimate_sigz0_biasedpvunbiased;   //!
TBranch        *b_mu_calo_trackIPEstimate_d0_unbiasedpvunbiased;   //!
TBranch        *b_mu_calo_trackIPEstimate_z0_unbiasedpvunbiased;   //!
TBranch        *b_mu_calo_trackIPEstimate_sigd0_unbiasedpvunbiased;   //!
TBranch        *b_mu_calo_trackIPEstimate_sigz0_unbiasedpvunbiased;   //!
TBranch        *b_mu_calo_trackd0pvunbiased;   //!
TBranch        *b_mu_calo_trackz0pvunbiased;   //!
TBranch        *b_mu_calo_tracksigd0pvunbiased;   //!
TBranch        *b_mu_calo_tracksigz0pvunbiased;   //!
TBranch        *b_mu_calo_type;   //!
TBranch        *b_mu_calo_origin;   //!
TBranch        *b_mu_calo_truth_dr;   //!
TBranch        *b_mu_calo_truth_E;   //!
TBranch        *b_mu_calo_truth_pt;   //!
TBranch        *b_mu_calo_truth_eta;   //!
TBranch        *b_mu_calo_truth_phi;   //!
TBranch        *b_mu_calo_truth_type;   //!
TBranch        *b_mu_calo_truth_status;   //!
TBranch        *b_mu_calo_truth_barcode;   //!
TBranch        *b_mu_calo_truth_mothertype;   //!
TBranch        *b_mu_calo_truth_motherbarcode;   //!
TBranch        *b_mu_calo_truth_matched;   //!
TBranch        *b_mu_calo_EFCB_dr;   //!
TBranch        *b_mu_calo_EFCB_n;   //!
TBranch        *b_mu_calo_EFCB_MuonType;   //!
TBranch        *b_mu_calo_EFCB_pt;   //!
TBranch        *b_mu_calo_EFCB_eta;   //!
TBranch        *b_mu_calo_EFCB_phi;   //!
TBranch        *b_mu_calo_EFCB_hasCB;   //!
TBranch        *b_mu_calo_EFCB_matched;   //!
TBranch        *b_mu_calo_EFMG_dr;   //!
TBranch        *b_mu_calo_EFMG_n;   //!
TBranch        *b_mu_calo_EFMG_MuonType;   //!
TBranch        *b_mu_calo_EFMG_pt;   //!
TBranch        *b_mu_calo_EFMG_eta;   //!
TBranch        *b_mu_calo_EFMG_phi;   //!
TBranch        *b_mu_calo_EFMG_hasMG;   //!
TBranch        *b_mu_calo_EFMG_matched;   //!
TBranch        *b_mu_calo_EFME_dr;   //!
TBranch        *b_mu_calo_EFME_n;   //!
TBranch        *b_mu_calo_EFME_MuonType;   //!
TBranch        *b_mu_calo_EFME_pt;   //!
TBranch        *b_mu_calo_EFME_eta;   //!
TBranch        *b_mu_calo_EFME_phi;   //!
TBranch        *b_mu_calo_EFME_hasME;   //!
TBranch        *b_mu_calo_EFME_matched;   //!
TBranch        *b_mu_calo_L2CB_dr;   //!
TBranch        *b_mu_calo_L2CB_pt;   //!
TBranch        *b_mu_calo_L2CB_eta;   //!
TBranch        *b_mu_calo_L2CB_phi;   //!
TBranch        *b_mu_calo_L2CB_id_pt;   //!
TBranch        *b_mu_calo_L2CB_ms_pt;   //!
TBranch        *b_mu_calo_L2CB_nPixHits;   //!
TBranch        *b_mu_calo_L2CB_nSCTHits;   //!
TBranch        *b_mu_calo_L2CB_nTRTHits;   //!
TBranch        *b_mu_calo_L2CB_nTRTHighTHits;   //!
TBranch        *b_mu_calo_L2CB_matched;   //!
TBranch        *b_mu_calo_L1_dr;   //!
TBranch        *b_mu_calo_L1_pt;   //!
TBranch        *b_mu_calo_L1_eta;   //!
TBranch        *b_mu_calo_L1_phi;   //!
TBranch        *b_mu_calo_L1_thrNumber;   //!
TBranch        *b_mu_calo_L1_RoINumber;   //!
TBranch        *b_mu_calo_L1_sectorAddress;   //!
TBranch        *b_mu_calo_L1_firstCandidate;   //!
TBranch        *b_mu_calo_L1_moreCandInRoI;   //!
TBranch        *b_mu_calo_L1_moreCandInSector;   //!
TBranch        *b_mu_calo_L1_source;   //!
TBranch        *b_mu_calo_L1_hemisphere;   //!
TBranch        *b_mu_calo_L1_charge;   //!
TBranch        *b_mu_calo_L1_vetoed;   //!
TBranch        *b_mu_calo_L1_matched;   //!
TBranch        *b_muonTruth_n;   //!
TBranch        *b_muonTruth_pt;   //!
TBranch        *b_muonTruth_m;   //!
TBranch        *b_muonTruth_eta;   //!
TBranch        *b_muonTruth_phi;   //!
TBranch        *b_muonTruth_charge;   //!
TBranch        *b_muonTruth_PDGID;   //!
TBranch        *b_muonTruth_barcode;   //!
TBranch        *b_muonTruth_type;   //!
TBranch        *b_muonTruth_origin;   //!

TBranch        *b_trig_DB_SMK;   //!
TBranch        *b_trig_L1_mu_eta;   //!
TBranch        *b_trig_L1_mu_phi;   //!
TBranch        *b_trig_L1_mu_thrName;   //!
TBranch        *b_trig_L2_muonfeature_eta;   //!
TBranch        *b_trig_L2_muonfeature_phi;   //!
TBranch        *b_trig_L2_combmuonfeature_eta;   //!
TBranch        *b_trig_L2_combmuonfeature_phi;   //!
TBranch        *b_trig_EF_trigmuonef_track_SA_pt;   //!
TBranch        *b_trig_EF_trigmuonef_track_SA_eta;   //!
TBranch        *b_trig_EF_trigmuonef_track_SA_phi;   //!
TBranch        *b_trig_EF_trigmuonef_track_CB_pt;   //!
TBranch        *b_trig_EF_trigmuonef_track_CB_eta;   //!
TBranch        *b_trig_EF_trigmuonef_track_CB_phi;   //!
TBranch        *b_trig_EF_trigmuonef_track_MuonType;   //!
TBranch        *b_trig_EF_trigmugirl_track_CB_pt;   //!
TBranch        *b_trig_EF_trigmugirl_track_CB_eta;   //!
TBranch        *b_trig_EF_trigmugirl_track_CB_phi;   //!
TBranch        *b_trig_Nav_n;   //!
TBranch        *b_trig_Nav_chain_ChainId;   //!
TBranch        *b_trig_Nav_chain_RoIType;   //!
TBranch        *b_trig_Nav_chain_RoIIndex;   //!
TBranch        *b_trig_RoI_L2_mu_MuonFeature;   //!
TBranch        *b_trig_RoI_L2_mu_CombinedMuonFeature;   //!
TBranch        *b_trig_RoI_L2_mu_CombinedMuonFeatureStatus;   //!
TBranch        *b_trig_RoI_L2_mu_Muon_ROI;   //!
TBranch        *b_trig_RoI_EF_mu_Muon_ROI;   //!
/*p956 ntuples*/
TBranch        *b_trig_RoI_EF_mu_TrigMuonEFInfoContainer_MuonEFInfo;   //!
TBranch        *b_trig_RoI_EF_mu_TrigMuonEFInfoContainer_MuonEFInfoStatus;   //!
TBranch        *b_trig_RoI_EF_mu_TrigMuonEFInfoContainer_eMuonEFInfo;   //!
TBranch        *b_trig_RoI_EF_mu_TrigMuonEFInfoContainer_eMuonEFInfoStatus;   //!
/*p869 ntuples*/
TBranch        *b_trig_RoI_EF_mu_TrigMuonEFInfoContainer;   //!
TBranch        *b_trig_RoI_EF_mu_TrigMuonEFInfoContainerStatus;   //!

TBranch        *b_trig_RoI_EF_mu_TrigMuonEFIsolationContainer;   //!
TBranch        *b_trig_RoI_EF_mu_TrigMuonEFIsolationContainerStatus;   //!

#endif
