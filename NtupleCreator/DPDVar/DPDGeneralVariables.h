#ifndef DPDGeneralVariables_h
#define DPDGeneralVariables_h
#include "TTree.h"
#include <vector>
#include <string>

void SetBranchGeneralVar(TTree*,int,int,bool minimal=false);

void SetPointerGeneralVar();

/* List of General DPD Variables */
UInt_t          RunNumber;
UInt_t          EventNumber;
UInt_t          lbn;
UInt_t          bcid;
UInt_t          mc_channel_number;
UInt_t          mc_event_number;
Float_t         mc_event_weight;
Float_t         averageIntPerXing;
UInt_t          pixelFlags;
UInt_t          sctFlags;
UInt_t          trtFlags;
UInt_t          larFlags;
UInt_t          tileFlags;
UInt_t          muonFlags;
UInt_t          fwdFlags;
UInt_t          coreFlags;
UInt_t          pixelError;
UInt_t          sctError;
UInt_t          trtError;
UInt_t          larError;
UInt_t          tileError;
UInt_t          muonError;
UInt_t          fwdError;
UInt_t          coreError;
Float_t         Eventshape_rhoKt3EM;
Float_t         Eventshape_rhoKt4EM;
Float_t         Eventshape_rhoKt3LC;
Float_t         Eventshape_rhoKt4LC;
Float_t         MET_RefFinal_phi;
Float_t         MET_RefFinal_et;
Float_t         MET_RefFinal_sumet;

Float_t         MET_RefFinal_BDTMedium_etx;
Float_t         MET_RefFinal_BDTMedium_ety;
Float_t         MET_RefFinal_BDTMedium_phi;
Float_t         MET_RefFinal_BDTMedium_et;
Float_t         MET_RefFinal_BDTMedium_sumet;

Int_t           vxp_n;
std::vector<float>   *vxp_x;
std::vector<float>   *vxp_y;
std::vector<float>   *vxp_z;
std::vector<float>   *vxp_cov_x;
std::vector<float>   *vxp_cov_y;
std::vector<float>   *vxp_cov_z;
std::vector<float>   *vxp_cov_xy;
std::vector<float>   *vxp_cov_xz;
std::vector<float>   *vxp_cov_yz;
std::vector<int>     *vxp_type;
std::vector<float>   *vxp_chi2;
std::vector<int>     *vxp_ndof;
std::vector<float>   *vxp_px;
std::vector<float>   *vxp_py;
std::vector<float>   *vxp_pz;
std::vector<float>   *vxp_E;
std::vector<float>   *vxp_m;
std::vector<int>     *vxp_nTracks;
std::vector<float>   *vxp_sumPt;
std::vector<std::vector<float> > *vxp_trk_weight;
std::vector<int>     *vxp_trk_n;
std::vector<std::vector<int> > *vxp_trk_index;
Int_t           mcevt_n;
std::vector<int>     *mcevt_signal_process_id;
std::vector<int>     *mcevt_event_number;
std::vector<double>  *mcevt_event_scale;
std::vector<double>  *mcevt_alphaQCD;
std::vector<double>  *mcevt_alphaQED;
std::vector<int>     *mcevt_pdf_id1;
std::vector<int>     *mcevt_pdf_id2;
std::vector<double>  *mcevt_pdf_x1;
std::vector<double>  *mcevt_pdf_x2;
std::vector<double>  *mcevt_pdf_scale;
std::vector<double>  *mcevt_pdf1;
std::vector<double>  *mcevt_pdf2;
std::vector<std::vector<double> > *mcevt_weight;
std::vector<int>     *mcevt_nparticle;
std::vector<short>   *mcevt_pileUpType;
Int_t           mc_n;
std::vector<float>   *mc_pt;
std::vector<float>   *mc_m;
std::vector<float>   *mc_eta;
std::vector<float>   *mc_phi;
std::vector<int>     *mc_status;
std::vector<int>     *mc_barcode;
std::vector<int>     *mc_pdgId;
std::vector<float>   *mc_charge;
std::vector<std::vector<int> > *mc_parents;
std::vector<std::vector<int> > *mc_children;
std::vector<float>   *mc_vx_x;
std::vector<float>   *mc_vx_y;
std::vector<float>   *mc_vx_z;
std::vector<int>     *mc_vx_barcode;
std::vector<std::vector<int> > *mc_child_index;
std::vector<std::vector<int> > *mc_parent_index;

/* List of branches */
TBranch        *b_RunNumber;   //!
TBranch        *b_EventNumber;   //!
TBranch        *b_lbn;   //!
TBranch        *b_bcid;   //!
TBranch        *b_mc_channel_number;   //!
TBranch        *b_mc_event_number;   //!
TBranch        *b_mc_event_weight;   //!
TBranch        *b_averageIntPerXing;   //!
TBranch        *b_pixelFlags;   //!
TBranch        *b_sctFlags;   //!
TBranch        *b_trtFlags;   //!
TBranch        *b_larFlags;   //!
TBranch        *b_tileFlags;   //!
TBranch        *b_muonFlags;   //!
TBranch        *b_fwdFlags;   //!
TBranch        *b_coreFlags;   //!
TBranch        *b_pixelError;   //!
TBranch        *b_sctError;   //!
TBranch        *b_trtError;   //!
TBranch        *b_larError;   //!
TBranch        *b_tileError;   //!
TBranch        *b_muonError;   //!
TBranch        *b_fwdError;   //!
TBranch        *b_coreError;   //!
TBranch        *b_Eventshape_rhoKt3EM;   //!
TBranch        *b_Eventshape_rhoKt4EM;   //!
TBranch        *b_Eventshape_rhoKt3LC;   //!
TBranch        *b_Eventshape_rhoKt4LC;   //!
TBranch        *b_MET_RefFinal_phi;   //!
TBranch        *b_MET_RefFinal_et;   //!
TBranch        *b_MET_RefFinal_sumet;   //!

TBranch        *b_MET_RefFinal_BDTMedium_etx;   //!
TBranch        *b_MET_RefFinal_BDTMedium_ety;   //!
TBranch        *b_MET_RefFinal_BDTMedium_phi;   //!
TBranch        *b_MET_RefFinal_BDTMedium_et;   //!
TBranch        *b_MET_RefFinal_BDTMedium_sumet;   //!

TBranch        *b_vxp_n;   //!
TBranch        *b_vxp_x;   //!
TBranch        *b_vxp_y;   //!
TBranch        *b_vxp_z;   //!
TBranch        *b_vxp_cov_x;   //!
TBranch        *b_vxp_cov_y;   //!
TBranch        *b_vxp_cov_z;   //!
TBranch        *b_vxp_cov_xy;   //!
TBranch        *b_vxp_cov_xz;   //!
TBranch        *b_vxp_cov_yz;   //!
TBranch        *b_vxp_type;   //!
TBranch        *b_vxp_chi2;   //!
TBranch        *b_vxp_ndof;   //!
TBranch        *b_vxp_px;   //!
TBranch        *b_vxp_py;   //!
TBranch        *b_vxp_pz;   //!
TBranch        *b_vxp_E;   //!
TBranch        *b_vxp_m;   //!
TBranch        *b_vxp_nTracks;   //!
TBranch        *b_vxp_sumPt;   //!
TBranch        *b_vxp_trk_weight;   //!
TBranch        *b_vxp_trk_n;   //!
TBranch        *b_vxp_trk_index;   //!
TBranch        *b_mcevt_n;   //!
TBranch        *b_mcevt_signal_process_id;   //!
TBranch        *b_mcevt_event_number;   //!
TBranch        *b_mcevt_event_scale;   //!
TBranch        *b_mcevt_alphaQCD;   //!
TBranch        *b_mcevt_alphaQED;   //!
TBranch        *b_mcevt_pdf_id1;   //!
TBranch        *b_mcevt_pdf_id2;   //!
TBranch        *b_mcevt_pdf_x1;   //!
TBranch        *b_mcevt_pdf_x2;   //!
TBranch        *b_mcevt_pdf_scale;   //!
TBranch        *b_mcevt_pdf1;   //!
TBranch        *b_mcevt_pdf2;   //!
TBranch        *b_mcevt_weight;   //!
TBranch        *b_mcevt_nparticle;   //!
TBranch        *b_mcevt_pileUpType;   //!
TBranch        *b_mc_n;   //!
TBranch        *b_mc_pt;   //!
TBranch        *b_mc_m;   //!
TBranch        *b_mc_eta;   //!
TBranch        *b_mc_phi;   //!
TBranch        *b_mc_status;   //!
TBranch        *b_mc_barcode;   //!
TBranch        *b_mc_pdgId;   //!
TBranch        *b_mc_charge;   //!
TBranch        *b_mc_parents;   //!
TBranch        *b_mc_children;   //!
TBranch        *b_mc_vx_x;   //!
TBranch        *b_mc_vx_y;   //!
TBranch        *b_mc_vx_z;   //!
TBranch        *b_mc_vx_barcode;   //!
TBranch        *b_mc_child_index;   //!
TBranch        *b_mc_parent_index;   //!

#endif
