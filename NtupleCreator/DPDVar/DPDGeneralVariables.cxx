#include "DPDGeneralVariables.h"

void SetPointerGeneralVar(){

  vxp_x = 0;
  vxp_y = 0;
  vxp_z = 0;
  vxp_cov_x = 0;
  vxp_cov_y = 0;
  vxp_cov_z = 0;
  vxp_cov_xy = 0;
  vxp_cov_xz = 0;
  vxp_cov_yz = 0;
  vxp_type = 0;
  vxp_chi2 = 0;
  vxp_ndof = 0;
  vxp_px = 0;
  vxp_py = 0;
  vxp_pz = 0;
  vxp_E = 0;
  vxp_m = 0;
  vxp_nTracks = 0;
  vxp_sumPt = 0;
  vxp_trk_weight = 0;
  vxp_trk_n = 0;
  vxp_trk_index = 0;
  mcevt_signal_process_id = 0;
  mcevt_event_number = 0;
  mcevt_event_scale = 0;
  mcevt_alphaQCD = 0;
  mcevt_alphaQED = 0;
  mcevt_pdf_id1 = 0;
  mcevt_pdf_id2 = 0;
  mcevt_pdf_x1 = 0;
  mcevt_pdf_x2 = 0;
  mcevt_pdf_scale = 0;
  mcevt_pdf1 = 0;
  mcevt_pdf2 = 0;
  mcevt_weight = 0;
  mcevt_nparticle = 0;
  mcevt_pileUpType = 0;
  mc_pt = 0;
  mc_m = 0;
  mc_eta = 0;
  mc_phi = 0;
  mc_status = 0;
  mc_barcode = 0;
  mc_pdgId = 0;
  mc_charge = 0;
  mc_parents = 0;
  mc_children = 0;
  mc_vx_x = 0;
  mc_vx_y = 0;
  mc_vx_z = 0;
  mc_vx_barcode = 0;
  mc_child_index = 0;
  mc_parent_index = 0;
  
}

void SetBranchGeneralVar(TTree* fChain,
			 int type,
			 int year,
			 bool minimal){
  
  fChain->SetBranchAddress("RunNumber", &RunNumber, &b_RunNumber);
  fChain->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
  fChain->SetBranchAddress("lbn", &lbn, &b_lbn);
  fChain->SetBranchAddress("bcid", &bcid, &b_bcid);
  fChain->SetBranchAddress("pixelFlags", &pixelFlags, &b_pixelFlags);
  fChain->SetBranchAddress("sctFlags", &sctFlags, &b_sctFlags);
  fChain->SetBranchAddress("trtFlags", &trtFlags, &b_trtFlags);
  fChain->SetBranchAddress("larFlags", &larFlags, &b_larFlags);
  fChain->SetBranchAddress("tileFlags", &tileFlags, &b_tileFlags);
  fChain->SetBranchAddress("muonFlags", &muonFlags, &b_muonFlags);
  fChain->SetBranchAddress("fwdFlags", &fwdFlags, &b_fwdFlags);
  fChain->SetBranchAddress("coreFlags", &coreFlags, &b_coreFlags);
  fChain->SetBranchAddress("pixelError", &pixelError, &b_pixelError);
  fChain->SetBranchAddress("sctError", &sctError, &b_sctError);
  fChain->SetBranchAddress("trtError", &trtError, &b_trtError);
  fChain->SetBranchAddress("larError", &larError, &b_larError);
  fChain->SetBranchAddress("tileError", &tileError, &b_tileError);
  fChain->SetBranchAddress("muonError", &muonError, &b_muonError);
  fChain->SetBranchAddress("fwdError", &fwdError, &b_fwdError);
  fChain->SetBranchAddress("coreError", &coreError, &b_coreError);
  if(year==m_YEAR_2012){
    fChain->SetBranchAddress("Eventshape_rhoKt3EM", &Eventshape_rhoKt3EM, &b_Eventshape_rhoKt3EM);
    fChain->SetBranchAddress("Eventshape_rhoKt4EM", &Eventshape_rhoKt4EM, &b_Eventshape_rhoKt4EM);
    fChain->SetBranchAddress("Eventshape_rhoKt3LC", &Eventshape_rhoKt3LC, &b_Eventshape_rhoKt3LC);
    fChain->SetBranchAddress("Eventshape_rhoKt4LC", &Eventshape_rhoKt4LC, &b_Eventshape_rhoKt4LC);
  }
  fChain->SetBranchAddress("averageIntPerXing", &averageIntPerXing, &b_averageIntPerXing);
  
  if(type==m_MC){
    fChain->SetBranchAddress("mc_channel_number", &mc_channel_number, &b_mc_channel_number);
    fChain->SetBranchAddress("mc_event_number", &mc_event_number, &b_mc_event_number);
    fChain->SetBranchAddress("mc_event_weight", &mc_event_weight, &b_mc_event_weight);
    fChain->SetBranchAddress("mcevt_n", &mcevt_n, &b_mcevt_n);
    fChain->SetBranchAddress("mcevt_signal_process_id", &mcevt_signal_process_id, &b_mcevt_signal_process_id);
    fChain->SetBranchAddress("mcevt_event_number", &mcevt_event_number, &b_mcevt_event_number);
    fChain->SetBranchAddress("mcevt_event_scale", &mcevt_event_scale, &b_mcevt_event_scale);
    fChain->SetBranchAddress("mcevt_alphaQCD", &mcevt_alphaQCD, &b_mcevt_alphaQCD);
    fChain->SetBranchAddress("mcevt_alphaQED", &mcevt_alphaQED, &b_mcevt_alphaQED);
    fChain->SetBranchAddress("mcevt_pdf_id1", &mcevt_pdf_id1, &b_mcevt_pdf_id1);
    fChain->SetBranchAddress("mcevt_pdf_id2", &mcevt_pdf_id2, &b_mcevt_pdf_id2);
    fChain->SetBranchAddress("mcevt_pdf_x1", &mcevt_pdf_x1, &b_mcevt_pdf_x1);
    fChain->SetBranchAddress("mcevt_pdf_x2", &mcevt_pdf_x2, &b_mcevt_pdf_x2);
    fChain->SetBranchAddress("mcevt_pdf_scale", &mcevt_pdf_scale, &b_mcevt_pdf_scale);
    fChain->SetBranchAddress("mcevt_pdf1", &mcevt_pdf1, &b_mcevt_pdf1);
    fChain->SetBranchAddress("mcevt_pdf2", &mcevt_pdf2, &b_mcevt_pdf2);
    fChain->SetBranchAddress("mcevt_weight", &mcevt_weight, &b_mcevt_weight);
    fChain->SetBranchAddress("mcevt_nparticle", &mcevt_nparticle, &b_mcevt_nparticle);
    fChain->SetBranchAddress("mcevt_pileUpType", &mcevt_pileUpType, &b_mcevt_pileUpType);
    fChain->SetBranchAddress("mc_n", &mc_n, &b_mc_n);
    fChain->SetBranchAddress("mc_pt", &mc_pt, &b_mc_pt);
    fChain->SetBranchAddress("mc_m", &mc_m, &b_mc_m);
    fChain->SetBranchAddress("mc_eta", &mc_eta, &b_mc_eta);
    fChain->SetBranchAddress("mc_phi", &mc_phi, &b_mc_phi);
    fChain->SetBranchAddress("mc_status", &mc_status, &b_mc_status);
    fChain->SetBranchAddress("mc_barcode", &mc_barcode, &b_mc_barcode);
    fChain->SetBranchAddress("mc_pdgId", &mc_pdgId, &b_mc_pdgId);
    fChain->SetBranchAddress("mc_charge", &mc_charge, &b_mc_charge);
    fChain->SetBranchAddress("mc_parents", &mc_parents, &b_mc_parents);
    fChain->SetBranchAddress("mc_children", &mc_children, &b_mc_children);
    fChain->SetBranchAddress("mc_vx_x", &mc_vx_x, &b_mc_vx_x);
    fChain->SetBranchAddress("mc_vx_y", &mc_vx_y, &b_mc_vx_y);
    fChain->SetBranchAddress("mc_vx_z", &mc_vx_z, &b_mc_vx_z);
    fChain->SetBranchAddress("mc_vx_barcode", &mc_vx_barcode, &b_mc_vx_barcode);
    fChain->SetBranchAddress("mc_child_index", &mc_child_index, &b_mc_child_index);
    fChain->SetBranchAddress("mc_parent_index", &mc_parent_index, &b_mc_parent_index);
  }
  
  fChain->SetBranchAddress("MET_RefFinal_phi", &MET_RefFinal_phi, &b_MET_RefFinal_phi);
  fChain->SetBranchAddress("MET_RefFinal_et", &MET_RefFinal_et, &b_MET_RefFinal_et);
  fChain->SetBranchAddress("MET_RefFinal_sumet", &MET_RefFinal_sumet, &b_MET_RefFinal_sumet);

  /*Replace with MET_RefFinal_BDTMedium*/
  fChain->SetBranchAddress("MET_RefFinal_BDTMedium_etx", &MET_RefFinal_BDTMedium_etx, &b_MET_RefFinal_BDTMedium_etx);
  fChain->SetBranchAddress("MET_RefFinal_BDTMedium_ety", &MET_RefFinal_BDTMedium_ety, &b_MET_RefFinal_BDTMedium_ety);
  fChain->SetBranchAddress("MET_RefFinal_BDTMedium_phi", &MET_RefFinal_BDTMedium_phi, &b_MET_RefFinal_BDTMedium_phi);
  fChain->SetBranchAddress("MET_RefFinal_BDTMedium_et", &MET_RefFinal_BDTMedium_et, &b_MET_RefFinal_BDTMedium_et);
  fChain->SetBranchAddress("MET_RefFinal_BDTMedium_sumet", &MET_RefFinal_BDTMedium_sumet, &b_MET_RefFinal_BDTMedium_sumet);
   
  fChain->SetBranchAddress("vxp_n", &vxp_n, &b_vxp_n);
  fChain->SetBranchAddress("vxp_trk_n", &vxp_trk_n, &b_vxp_trk_n);
  
  if(!minimal){
    fChain->SetBranchAddress("vxp_x", &vxp_x, &b_vxp_x);
    fChain->SetBranchAddress("vxp_y", &vxp_y, &b_vxp_y);
    fChain->SetBranchAddress("vxp_z", &vxp_z, &b_vxp_z);
    fChain->SetBranchAddress("vxp_cov_x", &vxp_cov_x, &b_vxp_cov_x);
    fChain->SetBranchAddress("vxp_cov_y", &vxp_cov_y, &b_vxp_cov_y);
    fChain->SetBranchAddress("vxp_cov_z", &vxp_cov_z, &b_vxp_cov_z);
    fChain->SetBranchAddress("vxp_cov_xy", &vxp_cov_xy, &b_vxp_cov_xy);
    fChain->SetBranchAddress("vxp_cov_xz", &vxp_cov_xz, &b_vxp_cov_xz);
    fChain->SetBranchAddress("vxp_cov_yz", &vxp_cov_yz, &b_vxp_cov_yz);
    fChain->SetBranchAddress("vxp_type", &vxp_type, &b_vxp_type);
    fChain->SetBranchAddress("vxp_chi2", &vxp_chi2, &b_vxp_chi2);
    fChain->SetBranchAddress("vxp_ndof", &vxp_ndof, &b_vxp_ndof);
    fChain->SetBranchAddress("vxp_px", &vxp_px, &b_vxp_px);
    fChain->SetBranchAddress("vxp_py", &vxp_py, &b_vxp_py);
    fChain->SetBranchAddress("vxp_pz", &vxp_pz, &b_vxp_pz);
    fChain->SetBranchAddress("vxp_E", &vxp_E, &b_vxp_E);
    fChain->SetBranchAddress("vxp_m", &vxp_m, &b_vxp_m);
    fChain->SetBranchAddress("vxp_nTracks", &vxp_nTracks, &b_vxp_nTracks);
    fChain->SetBranchAddress("vxp_sumPt", &vxp_sumPt, &b_vxp_sumPt);
    fChain->SetBranchAddress("vxp_trk_weight", &vxp_trk_weight, &b_vxp_trk_weight);
    fChain->SetBranchAddress("vxp_trk_index", &vxp_trk_index, &b_vxp_trk_index);
  }
  
}
