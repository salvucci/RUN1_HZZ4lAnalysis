#ifndef DPDPhotonVariables_h
#define DPDPhotonVariables_h
#include "TTree.h"
#include <vector>
#include <string>

void SetBranchPhotonVar(TTree*,int,bool minimal=false);

void SetPointerPhotonVar();

/* List of Photon DPD Variables */
Int_t           ph_n;
std::vector<float>   *ph_E;
std::vector<float>   *ph_Et;
std::vector<float>   *ph_pt;
std::vector<float>   *ph_m;
std::vector<float>   *ph_eta;
std::vector<float>   *ph_phi;
std::vector<float>   *ph_px;
std::vector<float>   *ph_py;
std::vector<float>   *ph_pz;
std::vector<int>     *ph_author;
std::vector<int>     *ph_isRecovered;
std::vector<unsigned int> *ph_isEM;
std::vector<unsigned int> *ph_isEMLoose;
std::vector<unsigned int> *ph_isEMMedium;
std::vector<unsigned int> *ph_isEMTight;
std::vector<unsigned int> *ph_OQ;
std::vector<int>     *ph_convFlag;
std::vector<int>     *ph_isConv;
std::vector<int>     *ph_nConv;
std::vector<int>     *ph_nSingleTrackConv;
std::vector<int>     *ph_nDoubleTrackConv;
std::vector<int>     *ph_type;
std::vector<int>     *ph_origin;
std::vector<float>   *ph_truth_deltaRRecPhoton;
std::vector<float>   *ph_truth_E;
std::vector<float>   *ph_truth_pt;
std::vector<float>   *ph_truth_eta;
std::vector<float>   *ph_truth_phi;
std::vector<int>     *ph_truth_type;
std::vector<int>     *ph_truth_status;
std::vector<int>     *ph_truth_barcode;
std::vector<int>     *ph_truth_mothertype;
std::vector<int>     *ph_truth_motherbarcode;
std::vector<int>     *ph_truth_index;
std::vector<int>     *ph_truth_matched;
std::vector<int>     *ph_loose;
std::vector<int>     *ph_looseIso;
std::vector<int>     *ph_tight;
std::vector<int>     *ph_tightIso;
std::vector<int>     *ph_looseAR;
std::vector<int>     *ph_looseARIso;
std::vector<int>     *ph_tightAR;
std::vector<int>     *ph_tightARIso;
std::vector<int>     *ph_goodOQ;
std::vector<float>   *ph_Ethad;
std::vector<float>   *ph_Ethad1;
std::vector<float>   *ph_E033;
std::vector<float>   *ph_f1;
std::vector<float>   *ph_f1core;
std::vector<float>   *ph_Emins1;
std::vector<float>   *ph_fside;
std::vector<float>   *ph_Emax2;
std::vector<float>   *ph_ws3;
std::vector<float>   *ph_wstot;
std::vector<float>   *ph_E132;
std::vector<float>   *ph_E1152;
std::vector<float>   *ph_emaxs1;
std::vector<float>   *ph_deltaEs;
std::vector<float>   *ph_E233;
std::vector<float>   *ph_E237;
std::vector<float>   *ph_E277;
std::vector<float>   *ph_weta2;
std::vector<float>   *ph_f3;
std::vector<float>   *ph_f3core;
std::vector<float>   *ph_rphiallcalo;
std::vector<float>   *ph_Etcone45;
std::vector<float>   *ph_Etcone15;
std::vector<float>   *ph_Etcone20;
std::vector<float>   *ph_Etcone25;
std::vector<float>   *ph_Etcone30;
std::vector<float>   *ph_Etcone35;
std::vector<float>   *ph_Etcone40;
std::vector<float>   *ph_ptcone20;
std::vector<float>   *ph_ptcone30;
std::vector<float>   *ph_ptcone40;
std::vector<float>   *ph_nucone20;
std::vector<float>   *ph_nucone30;
std::vector<float>   *ph_nucone40;
std::vector<float>   *ph_Etcone15_pt_corrected;
std::vector<float>   *ph_Etcone20_pt_corrected;
std::vector<float>   *ph_Etcone25_pt_corrected;
std::vector<float>   *ph_Etcone30_pt_corrected;
std::vector<float>   *ph_Etcone35_pt_corrected;
std::vector<float>   *ph_Etcone40_pt_corrected;
std::vector<float>   *ph_convanglematch;
std::vector<float>   *ph_convtrackmatch;
std::vector<int>     *ph_hasconv;
std::vector<float>   *ph_convvtxx;
std::vector<float>   *ph_convvtxy;
std::vector<float>   *ph_convvtxz;
std::vector<float>   *ph_Rconv;
std::vector<float>   *ph_zconv;
std::vector<float>   *ph_convvtxchi2;
std::vector<float>   *ph_pt1conv;
std::vector<int>     *ph_convtrk1nBLHits;
std::vector<int>     *ph_convtrk1nPixHits;
std::vector<int>     *ph_convtrk1nSCTHits;
std::vector<int>     *ph_convtrk1nTRTHits;
std::vector<float>   *ph_pt2conv;
std::vector<int>     *ph_convtrk2nBLHits;
std::vector<int>     *ph_convtrk2nPixHits;
std::vector<int>     *ph_convtrk2nSCTHits;
std::vector<int>     *ph_convtrk2nTRTHits;
std::vector<float>   *ph_ptconv;
std::vector<float>   *ph_pzconv;
std::vector<float>   *ph_reta;
std::vector<float>   *ph_rphi;
std::vector<float>   *ph_topoEtcone20;
std::vector<float>   *ph_topoEtcone30;
std::vector<float>   *ph_topoEtcone40;
std::vector<float>   *ph_materialTraversed;
std::vector<float>   *ph_EtringnoisedR03sig2;
std::vector<float>   *ph_EtringnoisedR03sig3;
std::vector<float>   *ph_EtringnoisedR03sig4;
std::vector<float>   *ph_ptcone20_zpv05;
std::vector<float>   *ph_ptcone30_zpv05;
std::vector<float>   *ph_ptcone40_zpv05;
std::vector<float>   *ph_nucone20_zpv05;
std::vector<float>   *ph_nucone30_zpv05;
std::vector<float>   *ph_nucone40_zpv05;
std::vector<double>  *ph_isolationlikelihoodjets;
std::vector<double>  *ph_isolationlikelihoodhqelectrons;
std::vector<double>  *ph_loglikelihood;
std::vector<double>  *ph_photonweight;
std::vector<double>  *ph_photonbgweight;
std::vector<double>  *ph_neuralnet;
std::vector<double>  *ph_Hmatrix;
std::vector<double>  *ph_Hmatrix5;
std::vector<double>  *ph_adaboost;
std::vector<double>  *ph_ringernn;
std::vector<float>   *ph_zvertex;
std::vector<float>   *ph_errz;
std::vector<float>   *ph_etap;
std::vector<float>   *ph_depth;
std::vector<float>   *ph_cl_E;
std::vector<float>   *ph_cl_pt;
std::vector<float>   *ph_cl_eta;
std::vector<float>   *ph_cl_phi;
std::vector<double>  *ph_cl_etaCalo;
std::vector<double>  *ph_cl_phiCalo;
std::vector<float>   *ph_Es0;
std::vector<float>   *ph_etas0;
std::vector<float>   *ph_phis0;
std::vector<float>   *ph_Es1;
std::vector<float>   *ph_etas1;
std::vector<float>   *ph_phis1;
std::vector<float>   *ph_Es2;
std::vector<float>   *ph_etas2;
std::vector<float>   *ph_phis2;
std::vector<float>   *ph_Es3;
std::vector<float>   *ph_etas3;
std::vector<float>   *ph_phis3;
std::vector<float>   *ph_rawcl_Es0;
std::vector<float>   *ph_rawcl_etas0;
std::vector<float>   *ph_rawcl_phis0;
std::vector<float>   *ph_rawcl_Es1;
std::vector<float>   *ph_rawcl_etas1;
std::vector<float>   *ph_rawcl_phis1;
std::vector<float>   *ph_rawcl_Es2;
std::vector<float>   *ph_rawcl_etas2;
std::vector<float>   *ph_rawcl_phis2;
std::vector<float>   *ph_rawcl_Es3;
std::vector<float>   *ph_rawcl_etas3;
std::vector<float>   *ph_rawcl_phis3;
std::vector<float>   *ph_rawcl_E;
std::vector<float>   *ph_rawcl_pt;
std::vector<float>   *ph_rawcl_eta;
std::vector<float>   *ph_rawcl_phi;
std::vector<float>   *ph_convMatchDeltaEta1;
std::vector<float>   *ph_convMatchDeltaEta2;
std::vector<float>   *ph_convMatchDeltaPhi1;
std::vector<float>   *ph_convMatchDeltaPhi2;
std::vector<std::vector<float> > *ph_rings_E;
std::vector<int>     *ph_vx_n;
std::vector<std::vector<float> > *ph_vx_x;
std::vector<std::vector<float> > *ph_vx_y;
std::vector<std::vector<float> > *ph_vx_z;
std::vector<std::vector<float> > *ph_vx_px;
std::vector<std::vector<float> > *ph_vx_py;
std::vector<std::vector<float> > *ph_vx_pz;
std::vector<std::vector<float> > *ph_vx_E;
std::vector<std::vector<float> > *ph_vx_m;
std::vector<std::vector<int> > *ph_vx_nTracks;
std::vector<std::vector<float> > *ph_vx_sumPt;
std::vector<std::vector<std::vector<float> > > *ph_vx_convTrk_weight;
std::vector<std::vector<int> > *ph_vx_convTrk_n;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_fitter;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_patternReco1;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_patternReco2;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_trackProperties;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_particleHypothesis;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_nBLHits;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_nPixHits;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_nSCTHits;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_nTRTHits;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_nTRTHighTHits;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_nTRTXenonHits;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_nPixHoles;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_nSCTHoles;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_nTRTHoles;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_nPixelDeadSensors;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_nSCTDeadSensors;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_nBLSharedHits;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_nPixSharedHits;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_nSCTSharedHits;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_nBLayerSplitHits;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_nPixSplitHits;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_nBLayerOutliers;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_nPixelOutliers;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_nSCTOutliers;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_nTRTOutliers;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_nTRTHighTOutliers;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_nContribPixelLayers;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_nGangedPixels;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_nGangedFlaggedFakes;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_nPixelSpoiltHits;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_nSCTDoubleHoles;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_nSCTSpoiltHits;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_nTRTDeadStraws;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_nTRTTubeHits;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_nOutliersOnTrack;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_standardDeviationOfChi2OS;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_expectBLayerHit;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_nMDTHits;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_nCSCEtaHits;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_nCSCPhiHits;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_nRPCEtaHits;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_nRPCPhiHits;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_nTGCEtaHits;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_nTGCPhiHits;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_nMdtHoles;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_nCscEtaHoles;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_nCscPhiHoles;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_nRpcEtaHoles;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_nRpcPhiHoles;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_nTgcEtaHoles;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_nTgcPhiHoles;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_nHits;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_nHoles;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_hitPattern;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_nSiHits;
std::vector<std::vector<std::vector<float> > > *ph_vx_convTrk_TRTHighTHitsRatio;
std::vector<std::vector<std::vector<float> > > *ph_vx_convTrk_TRTHighTOutliersRatio;
std::vector<std::vector<std::vector<float> > > *ph_vx_convTrk_eProbabilityComb;
std::vector<std::vector<std::vector<float> > > *ph_vx_convTrk_eProbabilityHT;
std::vector<std::vector<std::vector<float> > > *ph_vx_convTrk_eProbabilityToT;
std::vector<std::vector<std::vector<float> > > *ph_vx_convTrk_eProbabilityBrem;
std::vector<std::vector<std::vector<float> > > *ph_vx_convTrk_chi2;
std::vector<std::vector<std::vector<int> > > *ph_vx_convTrk_ndof;
std::vector<int>     *ph_truth_isConv;
std::vector<int>     *ph_truth_isBrem;
std::vector<int>     *ph_truth_isFromHardProc;
std::vector<int>     *ph_truth_isPhotonFromHardProc;
std::vector<float>   *ph_truth_Rconv;
std::vector<float>   *ph_truth_zconv;
std::vector<float>   *ph_deltaEmax2;
std::vector<float>   *ph_calibHitsShowerDepth;
std::vector<unsigned int> *ph_isIso;
std::vector<float>   *ph_mvaptcone20;
std::vector<float>   *ph_mvaptcone30;
std::vector<float>   *ph_mvaptcone40;
std::vector<float>   *ph_topoEtcone60;
std::vector<std::vector<float> > *ph_vx_Chi2;
std::vector<std::vector<float> > *ph_vx_Dcottheta;
std::vector<std::vector<float> > *ph_vx_Dphi;
std::vector<std::vector<float> > *ph_vx_Dist;
std::vector<std::vector<float> > *ph_vx_DR1R2;
std::vector<float>   *ph_CaloPointing_eta;
std::vector<float>   *ph_CaloPointing_sigma_eta;
std::vector<float>   *ph_CaloPointing_zvertex;
std::vector<float>   *ph_CaloPointing_sigma_zvertex;
std::vector<float>   *ph_HPV_eta;
std::vector<float>   *ph_HPV_sigma_eta;
std::vector<float>   *ph_HPV_zvertex;
std::vector<float>   *ph_HPV_sigma_zvertex;
std::vector<int>     *ph_NN_passes;
std::vector<float>   *ph_NN_discriminant;
std::vector<float>   *ph_ES0_real;
std::vector<float>   *ph_ES1_real;
std::vector<float>   *ph_ES2_real;
std::vector<float>   *ph_ES3_real;
std::vector<float>   *ph_EcellS0;
std::vector<float>   *ph_etacellS0;
std::vector<float>   *ph_Etcone40_ED_corrected;
std::vector<float>   *ph_Etcone40_corrected;
std::vector<float>   *ph_topoEtcone20_corrected;
std::vector<float>   *ph_topoEtcone30_corrected;
std::vector<float>   *ph_topoEtcone40_corrected;
std::vector<float>   *ph_ED_median;
std::vector<float>   *ph_ED_sigma;
std::vector<float>   *ph_ED_Njets;
std::vector<float>   *ph_convIP;
std::vector<float>   *ph_convIPRev;
std::vector<float>   *ph_jet_dr;
std::vector<float>   *ph_jet_E;
std::vector<float>   *ph_jet_pt;
std::vector<float>   *ph_jet_m;
std::vector<float>   *ph_jet_eta;
std::vector<float>   *ph_jet_phi;
std::vector<float>   *ph_jet_truth_dr;
std::vector<float>   *ph_jet_truth_E;
std::vector<float>   *ph_jet_truth_pt;
std::vector<float>   *ph_jet_truth_m;
std::vector<float>   *ph_jet_truth_eta;
std::vector<float>   *ph_jet_truth_phi;
std::vector<int>     *ph_jet_truth_matched;
std::vector<int>     *ph_jet_matched;
std::vector<float>   *ph_topodr;
std::vector<float>   *ph_topopt;
std::vector<float>   *ph_topoeta;
std::vector<float>   *ph_topophi;
std::vector<int>     *ph_topomatched;
std::vector<float>   *ph_topoEMdr;
std::vector<float>   *ph_topoEMpt;
std::vector<float>   *ph_topoEMeta;
std::vector<float>   *ph_topoEMphi;
std::vector<int>     *ph_topoEMmatched;
std::vector<int>     *ph_el_index;
std::vector<float>   *ph_EF_dr;
std::vector<int>     *ph_EF_index;
std::vector<float>   *ph_L2_dr;
std::vector<int>     *ph_L2_index;
std::vector<float>   *ph_L1_dr;
std::vector<int>     *ph_L1_index;

/*List of branches*/
TBranch        *b_ph_n;   //!
TBranch        *b_ph_E;   //!
TBranch        *b_ph_Et;   //!
TBranch        *b_ph_pt;   //!
TBranch        *b_ph_m;   //!
TBranch        *b_ph_eta;   //!
TBranch        *b_ph_phi;   //!
TBranch        *b_ph_px;   //!
TBranch        *b_ph_py;   //!
TBranch        *b_ph_pz;   //!
TBranch        *b_ph_author;   //!
TBranch        *b_ph_isRecovered;   //!
TBranch        *b_ph_isEM;   //!
TBranch        *b_ph_isEMLoose;   //!
TBranch        *b_ph_isEMMedium;   //!
TBranch        *b_ph_isEMTight;   //!
TBranch        *b_ph_OQ;   //!
TBranch        *b_ph_convFlag;   //!
TBranch        *b_ph_isConv;   //!
TBranch        *b_ph_nConv;   //!
TBranch        *b_ph_nSingleTrackConv;   //!
TBranch        *b_ph_nDoubleTrackConv;   //!
TBranch        *b_ph_type;   //!
TBranch        *b_ph_origin;   //!
TBranch        *b_ph_truth_deltaRRecPhoton;   //!
TBranch        *b_ph_truth_E;   //!
TBranch        *b_ph_truth_pt;   //!
TBranch        *b_ph_truth_eta;   //!
TBranch        *b_ph_truth_phi;   //!
TBranch        *b_ph_truth_type;   //!
TBranch        *b_ph_truth_status;   //!
TBranch        *b_ph_truth_barcode;   //!
TBranch        *b_ph_truth_mothertype;   //!
TBranch        *b_ph_truth_motherbarcode;   //!
TBranch        *b_ph_truth_index;   //!
TBranch        *b_ph_truth_matched;   //!
TBranch        *b_ph_loose;   //!
TBranch        *b_ph_looseIso;   //!
TBranch        *b_ph_tight;   //!
TBranch        *b_ph_tightIso;   //!
TBranch        *b_ph_looseAR;   //!
TBranch        *b_ph_looseARIso;   //!
TBranch        *b_ph_tightAR;   //!
TBranch        *b_ph_tightARIso;   //!
TBranch        *b_ph_goodOQ;   //!
TBranch        *b_ph_Ethad;   //!
TBranch        *b_ph_Ethad1;   //!
TBranch        *b_ph_E033;   //!
TBranch        *b_ph_f1;   //!
TBranch        *b_ph_f1core;   //!
TBranch        *b_ph_Emins1;   //!
TBranch        *b_ph_fside;   //!
TBranch        *b_ph_Emax2;   //!
TBranch        *b_ph_ws3;   //!
TBranch        *b_ph_wstot;   //!
TBranch        *b_ph_E132;   //!
TBranch        *b_ph_E1152;   //!
TBranch        *b_ph_emaxs1;   //!
TBranch        *b_ph_deltaEs;   //!
TBranch        *b_ph_E233;   //!
TBranch        *b_ph_E237;   //!
TBranch        *b_ph_E277;   //!
TBranch        *b_ph_weta2;   //!
TBranch        *b_ph_f3;   //!
TBranch        *b_ph_f3core;   //!
TBranch        *b_ph_rphiallcalo;   //!
TBranch        *b_ph_Etcone45;   //!
TBranch        *b_ph_Etcone15;   //!
TBranch        *b_ph_Etcone20;   //!
TBranch        *b_ph_Etcone25;   //!
TBranch        *b_ph_Etcone30;   //!
TBranch        *b_ph_Etcone35;   //!
TBranch        *b_ph_Etcone40;   //!
TBranch        *b_ph_ptcone20;   //!
TBranch        *b_ph_ptcone30;   //!
TBranch        *b_ph_ptcone40;   //!
TBranch        *b_ph_nucone20;   //!
TBranch        *b_ph_nucone30;   //!
TBranch        *b_ph_nucone40;   //!
TBranch        *b_ph_Etcone15_pt_corrected;   //!
TBranch        *b_ph_Etcone20_pt_corrected;   //!
TBranch        *b_ph_Etcone25_pt_corrected;   //!
TBranch        *b_ph_Etcone30_pt_corrected;   //!
TBranch        *b_ph_Etcone35_pt_corrected;   //!
TBranch        *b_ph_Etcone40_pt_corrected;   //!
TBranch        *b_ph_convanglematch;   //!
TBranch        *b_ph_convtrackmatch;   //!
TBranch        *b_ph_hasconv;   //!
TBranch        *b_ph_convvtxx;   //!
TBranch        *b_ph_convvtxy;   //!
TBranch        *b_ph_convvtxz;   //!
TBranch        *b_ph_Rconv;   //!
TBranch        *b_ph_zconv;   //!
TBranch        *b_ph_convvtxchi2;   //!
TBranch        *b_ph_pt1conv;   //!
TBranch        *b_ph_convtrk1nBLHits;   //!
TBranch        *b_ph_convtrk1nPixHits;   //!
TBranch        *b_ph_convtrk1nSCTHits;   //!
TBranch        *b_ph_convtrk1nTRTHits;   //!
TBranch        *b_ph_pt2conv;   //!
TBranch        *b_ph_convtrk2nBLHits;   //!
TBranch        *b_ph_convtrk2nPixHits;   //!
TBranch        *b_ph_convtrk2nSCTHits;   //!
TBranch        *b_ph_convtrk2nTRTHits;   //!
TBranch        *b_ph_ptconv;   //!
TBranch        *b_ph_pzconv;   //!
TBranch        *b_ph_reta;   //!
TBranch        *b_ph_rphi;   //!
TBranch        *b_ph_topoEtcone20;   //!
TBranch        *b_ph_topoEtcone30;   //!
TBranch        *b_ph_topoEtcone40;   //!
TBranch        *b_ph_materialTraversed;   //!
TBranch        *b_ph_EtringnoisedR03sig2;   //!
TBranch        *b_ph_EtringnoisedR03sig3;   //!
TBranch        *b_ph_EtringnoisedR03sig4;   //!
TBranch        *b_ph_ptcone20_zpv05;   //!
TBranch        *b_ph_ptcone30_zpv05;   //!
TBranch        *b_ph_ptcone40_zpv05;   //!
TBranch        *b_ph_nucone20_zpv05;   //!
TBranch        *b_ph_nucone30_zpv05;   //!
TBranch        *b_ph_nucone40_zpv05;   //!
TBranch        *b_ph_isolationlikelihoodjets;   //!
TBranch        *b_ph_isolationlikelihoodhqelectrons;   //!
TBranch        *b_ph_loglikelihood;   //!
TBranch        *b_ph_photonweight;   //!
TBranch        *b_ph_photonbgweight;   //!
TBranch        *b_ph_neuralnet;   //!
TBranch        *b_ph_Hmatrix;   //!
TBranch        *b_ph_Hmatrix5;   //!
TBranch        *b_ph_adaboost;   //!
TBranch        *b_ph_ringernn;   //!
TBranch        *b_ph_zvertex;   //!
TBranch        *b_ph_errz;   //!
TBranch        *b_ph_etap;   //!
TBranch        *b_ph_depth;   //!
TBranch        *b_ph_cl_E;   //!
TBranch        *b_ph_cl_pt;   //!
TBranch        *b_ph_cl_eta;   //!
TBranch        *b_ph_cl_phi;   //!
TBranch        *b_ph_cl_etaCalo;   //!
TBranch        *b_ph_cl_phiCalo;   //!
TBranch        *b_ph_Es0;   //!
TBranch        *b_ph_etas0;   //!
TBranch        *b_ph_phis0;   //!
TBranch        *b_ph_Es1;   //!
TBranch        *b_ph_etas1;   //!
TBranch        *b_ph_phis1;   //!
TBranch        *b_ph_Es2;   //!
TBranch        *b_ph_etas2;   //!
TBranch        *b_ph_phis2;   //!
TBranch        *b_ph_Es3;   //!
TBranch        *b_ph_etas3;   //!
TBranch        *b_ph_phis3;   //!
TBranch        *b_ph_rawcl_Es0;   //!
TBranch        *b_ph_rawcl_etas0;   //!
TBranch        *b_ph_rawcl_phis0;   //!
TBranch        *b_ph_rawcl_Es1;   //!
TBranch        *b_ph_rawcl_etas1;   //!
TBranch        *b_ph_rawcl_phis1;   //!
TBranch        *b_ph_rawcl_Es2;   //!
TBranch        *b_ph_rawcl_etas2;   //!
TBranch        *b_ph_rawcl_phis2;   //!
TBranch        *b_ph_rawcl_Es3;   //!
TBranch        *b_ph_rawcl_etas3;   //!
TBranch        *b_ph_rawcl_phis3;   //!
TBranch        *b_ph_rawcl_E;   //!
TBranch        *b_ph_rawcl_pt;   //!
TBranch        *b_ph_rawcl_eta;   //!
TBranch        *b_ph_rawcl_phi;   //!
TBranch        *b_ph_convMatchDeltaEta1;   //!
TBranch        *b_ph_convMatchDeltaEta2;   //!
TBranch        *b_ph_convMatchDeltaPhi1;   //!
TBranch        *b_ph_convMatchDeltaPhi2;   //!
TBranch        *b_ph_rings_E;   //!
TBranch        *b_ph_vx_n;   //!
TBranch        *b_ph_vx_x;   //!
TBranch        *b_ph_vx_y;   //!
TBranch        *b_ph_vx_z;   //!
TBranch        *b_ph_vx_px;   //!
TBranch        *b_ph_vx_py;   //!
TBranch        *b_ph_vx_pz;   //!
TBranch        *b_ph_vx_E;   //!
TBranch        *b_ph_vx_m;   //!
TBranch        *b_ph_vx_nTracks;   //!
TBranch        *b_ph_vx_sumPt;   //!
TBranch        *b_ph_vx_convTrk_weight;   //!
TBranch        *b_ph_vx_convTrk_n;   //!
TBranch        *b_ph_vx_convTrk_fitter;   //!
TBranch        *b_ph_vx_convTrk_patternReco1;   //!
TBranch        *b_ph_vx_convTrk_patternReco2;   //!
TBranch        *b_ph_vx_convTrk_trackProperties;   //!
TBranch        *b_ph_vx_convTrk_particleHypothesis;   //!
TBranch        *b_ph_vx_convTrk_nBLHits;   //!
TBranch        *b_ph_vx_convTrk_nPixHits;   //!
TBranch        *b_ph_vx_convTrk_nSCTHits;   //!
TBranch        *b_ph_vx_convTrk_nTRTHits;   //!
TBranch        *b_ph_vx_convTrk_nTRTHighTHits;   //!
TBranch        *b_ph_vx_convTrk_nTRTXenonHits;   //!
TBranch        *b_ph_vx_convTrk_nPixHoles;   //!
TBranch        *b_ph_vx_convTrk_nSCTHoles;   //!
TBranch        *b_ph_vx_convTrk_nTRTHoles;   //!
TBranch        *b_ph_vx_convTrk_nPixelDeadSensors;   //!
TBranch        *b_ph_vx_convTrk_nSCTDeadSensors;   //!
TBranch        *b_ph_vx_convTrk_nBLSharedHits;   //!
TBranch        *b_ph_vx_convTrk_nPixSharedHits;   //!
TBranch        *b_ph_vx_convTrk_nSCTSharedHits;   //!
TBranch        *b_ph_vx_convTrk_nBLayerSplitHits;   //!
TBranch        *b_ph_vx_convTrk_nPixSplitHits;   //!
TBranch        *b_ph_vx_convTrk_nBLayerOutliers;   //!
TBranch        *b_ph_vx_convTrk_nPixelOutliers;   //!
TBranch        *b_ph_vx_convTrk_nSCTOutliers;   //!
TBranch        *b_ph_vx_convTrk_nTRTOutliers;   //!
TBranch        *b_ph_vx_convTrk_nTRTHighTOutliers;   //!
TBranch        *b_ph_vx_convTrk_nContribPixelLayers;   //!
TBranch        *b_ph_vx_convTrk_nGangedPixels;   //!
TBranch        *b_ph_vx_convTrk_nGangedFlaggedFakes;   //!
TBranch        *b_ph_vx_convTrk_nPixelSpoiltHits;   //!
TBranch        *b_ph_vx_convTrk_nSCTDoubleHoles;   //!
TBranch        *b_ph_vx_convTrk_nSCTSpoiltHits;   //!
TBranch        *b_ph_vx_convTrk_nTRTDeadStraws;   //!
TBranch        *b_ph_vx_convTrk_nTRTTubeHits;   //!
TBranch        *b_ph_vx_convTrk_nOutliersOnTrack;   //!
TBranch        *b_ph_vx_convTrk_standardDeviationOfChi2OS;   //!
TBranch        *b_ph_vx_convTrk_expectBLayerHit;   //!
TBranch        *b_ph_vx_convTrk_nMDTHits;   //!
TBranch        *b_ph_vx_convTrk_nCSCEtaHits;   //!
TBranch        *b_ph_vx_convTrk_nCSCPhiHits;   //!
TBranch        *b_ph_vx_convTrk_nRPCEtaHits;   //!
TBranch        *b_ph_vx_convTrk_nRPCPhiHits;   //!
TBranch        *b_ph_vx_convTrk_nTGCEtaHits;   //!
TBranch        *b_ph_vx_convTrk_nTGCPhiHits;   //!
TBranch        *b_ph_vx_convTrk_nMdtHoles;   //!
TBranch        *b_ph_vx_convTrk_nCscEtaHoles;   //!
TBranch        *b_ph_vx_convTrk_nCscPhiHoles;   //!
TBranch        *b_ph_vx_convTrk_nRpcEtaHoles;   //!
TBranch        *b_ph_vx_convTrk_nRpcPhiHoles;   //!
TBranch        *b_ph_vx_convTrk_nTgcEtaHoles;   //!
TBranch        *b_ph_vx_convTrk_nTgcPhiHoles;   //!
TBranch        *b_ph_vx_convTrk_nHits;   //!
TBranch        *b_ph_vx_convTrk_nHoles;   //!
TBranch        *b_ph_vx_convTrk_hitPattern;   //!
TBranch        *b_ph_vx_convTrk_nSiHits;   //!
TBranch        *b_ph_vx_convTrk_TRTHighTHitsRatio;   //!
TBranch        *b_ph_vx_convTrk_TRTHighTOutliersRatio;   //!
TBranch        *b_ph_vx_convTrk_eProbabilityComb;   //!
TBranch        *b_ph_vx_convTrk_eProbabilityHT;   //!
TBranch        *b_ph_vx_convTrk_eProbabilityToT;   //!
TBranch        *b_ph_vx_convTrk_eProbabilityBrem;   //!
TBranch        *b_ph_vx_convTrk_chi2;   //!
TBranch        *b_ph_vx_convTrk_ndof;   //!
TBranch        *b_ph_truth_isConv;   //!
TBranch        *b_ph_truth_isBrem;   //!
TBranch        *b_ph_truth_isFromHardProc;   //!
TBranch        *b_ph_truth_isPhotonFromHardProc;   //!
TBranch        *b_ph_truth_Rconv;   //!
TBranch        *b_ph_truth_zconv;   //!
TBranch        *b_ph_deltaEmax2;   //!
TBranch        *b_ph_calibHitsShowerDepth;   //!
TBranch        *b_ph_isIso;   //!
TBranch        *b_ph_mvaptcone20;   //!
TBranch        *b_ph_mvaptcone30;   //!
TBranch        *b_ph_mvaptcone40;   //!
TBranch        *b_ph_topoEtcone60;   //!
TBranch        *b_ph_vx_Chi2;   //!
TBranch        *b_ph_vx_Dcottheta;   //!
TBranch        *b_ph_vx_Dphi;   //!
TBranch        *b_ph_vx_Dist;   //!
TBranch        *b_ph_vx_DR1R2;   //!
TBranch        *b_ph_CaloPointing_eta;   //!
TBranch        *b_ph_CaloPointing_sigma_eta;   //!
TBranch        *b_ph_CaloPointing_zvertex;   //!
TBranch        *b_ph_CaloPointing_sigma_zvertex;   //!
TBranch        *b_ph_HPV_eta;   //!
TBranch        *b_ph_HPV_sigma_eta;   //!
TBranch        *b_ph_HPV_zvertex;   //!
TBranch        *b_ph_HPV_sigma_zvertex;   //!
TBranch        *b_ph_NN_passes;   //!
TBranch        *b_ph_NN_discriminant;   //!
TBranch        *b_ph_ES0_real;   //!
TBranch        *b_ph_ES1_real;   //!
TBranch        *b_ph_ES2_real;   //!
TBranch        *b_ph_ES3_real;   //!
TBranch        *b_ph_EcellS0;   //!
TBranch        *b_ph_etacellS0;   //!
TBranch        *b_ph_Etcone40_ED_corrected;   //!
TBranch        *b_ph_Etcone40_corrected;   //!
TBranch        *b_ph_topoEtcone20_corrected;   //!
TBranch        *b_ph_topoEtcone30_corrected;   //!
TBranch        *b_ph_topoEtcone40_corrected;   //!
TBranch        *b_ph_ED_median;   //!
TBranch        *b_ph_ED_sigma;   //!
TBranch        *b_ph_ED_Njets;   //!
TBranch        *b_ph_convIP;   //!
TBranch        *b_ph_convIPRev;   //!
TBranch        *b_ph_jet_dr;   //!
TBranch        *b_ph_jet_E;   //!
TBranch        *b_ph_jet_pt;   //!
TBranch        *b_ph_jet_m;   //!
TBranch        *b_ph_jet_eta;   //!
TBranch        *b_ph_jet_phi;   //!
TBranch        *b_ph_jet_truth_dr;   //!
TBranch        *b_ph_jet_truth_E;   //!
TBranch        *b_ph_jet_truth_pt;   //!
TBranch        *b_ph_jet_truth_m;   //!
TBranch        *b_ph_jet_truth_eta;   //!
TBranch        *b_ph_jet_truth_phi;   //!
TBranch        *b_ph_jet_truth_matched;   //!
TBranch        *b_ph_jet_matched;   //!
TBranch        *b_ph_topodr;   //!
TBranch        *b_ph_topopt;   //!
TBranch        *b_ph_topoeta;   //!
TBranch        *b_ph_topophi;   //!
TBranch        *b_ph_topomatched;   //!
TBranch        *b_ph_topoEMdr;   //!
TBranch        *b_ph_topoEMpt;   //!
TBranch        *b_ph_topoEMeta;   //!
TBranch        *b_ph_topoEMphi;   //!
TBranch        *b_ph_topoEMmatched;   //!
TBranch        *b_ph_el_index;   //!
TBranch        *b_ph_EF_dr;   //!
TBranch        *b_ph_EF_index;   //!
TBranch        *b_ph_L2_dr;   //!
TBranch        *b_ph_L2_index;   //!
TBranch        *b_ph_L1_dr;   //!
TBranch        *b_ph_L1_index;   //!

#endif
