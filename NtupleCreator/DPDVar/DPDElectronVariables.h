#ifndef DPDElectronVariables_h
#define DPDElectronVariables_h
#include "TTree.h"
#include <vector>
#include <string>

void SetBranchElectronVar(TTree*,int,int,bool minimal=false);

void SetBranchElectronVar2011(TTree*,int,bool minimal=false);

void SetBranchElectronVar2012(TTree*,int,bool minimal=false);

void SetBranchElectronTrigVar(TTree*,int);

void SetBranchElectronTrigMatchVar(TTree*);

void SetPointerElectronVar();

void SetPointerElectronTrigMatchVar();

/* List of Electron DPD Variables */
/*trigger 2011: electron*/
Bool_t          EF_e20_medium;
Bool_t          EF_e22_medium;
Bool_t          EF_e22vh_medium1;
Bool_t          EF_2e12_medium;
Bool_t          EF_2e12T_medium;
Bool_t          EF_2e12Tvh_medium;
/*trigger 2011: emu*/
Bool_t          EF_e10_medium_mu6;
/*trigger 2012: electron*/
Bool_t          EF_e24vhi_medium1;
Bool_t          EF_e60_medium1;
Bool_t          EF_2e12Tvh_loose1_;//EF_2e12Tvh_loose1
/*trigger 2012: emu*/
Bool_t          EF_e12Tvh_medium1_mu8_;//EF_e12Tvh_medium1_mu8
Bool_t          EF_e24vhi_loose1_mu8; 

/*2011*/
Int_t           el_GSF_n;
std::vector<float>   *el_GSF_E;
std::vector<float>   *el_GSF_Et;
std::vector<float>   *el_GSF_pt;
std::vector<float>   *el_GSF_m;
std::vector<float>   *el_GSF_eta;
std::vector<float>   *el_GSF_phi;
std::vector<float>   *el_GSF_px;
std::vector<float>   *el_GSF_py;
std::vector<float>   *el_GSF_pz;
std::vector<float>   *el_GSF_charge;
std::vector<int>     *el_GSF_author;
std::vector<unsigned int> *el_GSF_isEM;
std::vector<unsigned int> *el_GSF_isEMLoose;
std::vector<unsigned int> *el_GSF_isEMMedium;
std::vector<unsigned int> *el_GSF_isEMTight;
std::vector<unsigned int> *el_GSF_OQ;
std::vector<int>     *el_GSF_convFlag;
std::vector<int>     *el_GSF_isConv;
std::vector<int>     *el_GSF_nConv;
std::vector<int>     *el_GSF_nSingleTrackConv;
std::vector<int>     *el_GSF_nDoubleTrackConv;
std::vector<int>     *el_GSF_type;
std::vector<int>     *el_GSF_origin;
std::vector<int>     *el_GSF_typebkg;
std::vector<int>     *el_GSF_originbkg;
std::vector<float>   *el_GSF_truth_E;
std::vector<float>   *el_GSF_truth_pt;
std::vector<float>   *el_GSF_truth_eta;
std::vector<float>   *el_GSF_truth_phi;
std::vector<int>     *el_GSF_truth_type;
std::vector<int>     *el_GSF_truth_status;
std::vector<int>     *el_GSF_truth_barcode;
std::vector<int>     *el_GSF_truth_mothertype;
std::vector<int>     *el_GSF_truth_motherbarcode;
std::vector<int>     *el_GSF_truth_hasHardBrem;
std::vector<int>     *el_GSF_truth_index;
std::vector<int>     *el_GSF_truth_matched;
std::vector<int>     *el_GSF_mediumWithoutTrack;
std::vector<int>     *el_GSF_mediumIsoWithoutTrack;
std::vector<int>     *el_GSF_tightWithoutTrack;
std::vector<int>     *el_GSF_tightIsoWithoutTrack;
std::vector<int>     *el_GSF_loose;
std::vector<int>     *el_GSF_looseIso;
std::vector<int>     *el_GSF_medium;
std::vector<int>     *el_GSF_mediumIso;
std::vector<int>     *el_GSF_tight;
std::vector<int>     *el_GSF_tightIso;
std::vector<int>     *el_GSF_loosePP;
std::vector<int>     *el_GSF_loosePPIso;
std::vector<int>     *el_GSF_mediumPP;
std::vector<int>     *el_GSF_mediumPPIso;
std::vector<int>     *el_GSF_tightPP;
std::vector<int>     *el_GSF_tightPPIso;
std::vector<int>     *el_GSF_goodOQ;
std::vector<float>   *el_GSF_Ethad;
std::vector<float>   *el_GSF_Ethad1;
std::vector<float>   *el_GSF_f1;
std::vector<float>   *el_GSF_f1core;
std::vector<float>   *el_GSF_Emins1;
std::vector<float>   *el_GSF_fside;
std::vector<float>   *el_GSF_Emax2;
std::vector<float>   *el_GSF_ws3;
std::vector<float>   *el_GSF_wstot;
std::vector<float>   *el_GSF_emaxs1;
std::vector<float>   *el_GSF_deltaEs;
std::vector<float>   *el_GSF_E233;
std::vector<float>   *el_GSF_E237;
std::vector<float>   *el_GSF_E277;
std::vector<float>   *el_GSF_weta2;
std::vector<float>   *el_GSF_f3;
std::vector<float>   *el_GSF_f3core;
std::vector<float>   *el_GSF_rphiallcalo;
std::vector<float>   *el_GSF_Etcone45;
std::vector<float>   *el_GSF_Etcone15;
std::vector<float>   *el_GSF_Etcone20;
std::vector<float>   *el_GSF_Etcone25;
std::vector<float>   *el_GSF_Etcone30;
std::vector<float>   *el_GSF_Etcone35;
std::vector<float>   *el_GSF_Etcone40;
std::vector<float>   *el_GSF_ptcone20;
std::vector<float>   *el_GSF_ptcone30;
std::vector<float>   *el_GSF_ptcone40;
std::vector<float>   *el_GSF_nucone20;
std::vector<float>   *el_GSF_nucone30;
std::vector<float>   *el_GSF_nucone40;
std::vector<float>   *el_GSF_Etcone15_pt_corrected;
std::vector<float>   *el_GSF_Etcone20_pt_corrected;
std::vector<float>   *el_GSF_Etcone25_pt_corrected;
std::vector<float>   *el_GSF_Etcone30_pt_corrected;
std::vector<float>   *el_GSF_Etcone35_pt_corrected;
std::vector<float>   *el_GSF_Etcone40_pt_corrected;
std::vector<float>   *el_GSF_convanglematch;
std::vector<float>   *el_GSF_convtrackmatch;
std::vector<int>     *el_GSF_hasconv;
std::vector<float>   *el_GSF_convvtxx;
std::vector<float>   *el_GSF_convvtxy;
std::vector<float>   *el_GSF_convvtxz;
std::vector<float>   *el_GSF_Rconv;
std::vector<float>   *el_GSF_zconv;
std::vector<float>   *el_GSF_convvtxchi2;
std::vector<float>   *el_GSF_pt1conv;
std::vector<int>     *el_GSF_convtrk1nBLHits;
std::vector<int>     *el_GSF_convtrk1nPixHits;
std::vector<int>     *el_GSF_convtrk1nSCTHits;
std::vector<int>     *el_GSF_convtrk1nTRTHits;
std::vector<float>   *el_GSF_pt2conv;
std::vector<int>     *el_GSF_convtrk2nBLHits;
std::vector<int>     *el_GSF_convtrk2nPixHits;
std::vector<int>     *el_GSF_convtrk2nSCTHits;
std::vector<int>     *el_GSF_convtrk2nTRTHits;
std::vector<float>   *el_GSF_ptconv;
std::vector<float>   *el_GSF_pzconv;
std::vector<float>   *el_GSF_pos7;
std::vector<float>   *el_GSF_etacorrmag;
std::vector<float>   *el_GSF_deltaeta1;
std::vector<float>   *el_GSF_deltaeta2;
std::vector<float>   *el_GSF_deltaphi2;
std::vector<float>   *el_GSF_deltaphiRescaled;
std::vector<float>   *el_GSF_deltaPhiFromLastMeasurement;
std::vector<float>   *el_GSF_deltaPhiRot;
std::vector<float>   *el_GSF_expectHitInBLayer;
std::vector<float>   *el_GSF_trackd0_physics;
std::vector<float>   *el_GSF_etaSampling1;
std::vector<float>   *el_GSF_reta;
std::vector<float>   *el_GSF_rphi;
std::vector<float>   *el_GSF_EtringnoisedR03sig2;
std::vector<float>   *el_GSF_EtringnoisedR03sig3;
std::vector<float>   *el_GSF_EtringnoisedR03sig4;
std::vector<double>  *el_GSF_isolationlikelihoodjets;
std::vector<double>  *el_GSF_isolationlikelihoodhqelectrons;
std::vector<double>  *el_GSF_electronweight;
std::vector<double>  *el_GSF_electronbgweight;
std::vector<double>  *el_GSF_softeweight;
std::vector<double>  *el_GSF_softebgweight;
std::vector<double>  *el_GSF_neuralnet;
std::vector<double>  *el_GSF_Hmatrix;
std::vector<double>  *el_GSF_Hmatrix5;
std::vector<double>  *el_GSF_adaboost;
std::vector<double>  *el_GSF_softeneuralnet;
std::vector<float>   *el_GSF_zvertex;
std::vector<float>   *el_GSF_errz;
std::vector<float>   *el_GSF_etap;
std::vector<float>   *el_GSF_depth;
std::vector<int>     *el_GSF_refittedTrack_n;
std::vector<std::vector<int> > *el_GSF_refittedTrack_author;
std::vector<std::vector<float> > *el_GSF_refittedTrack_chi2;
std::vector<std::vector<int> > *el_GSF_refittedTrack_hasBrem;
std::vector<std::vector<float> > *el_GSF_refittedTrack_bremRadius;
std::vector<std::vector<float> > *el_GSF_refittedTrack_bremZ;
std::vector<std::vector<float> > *el_GSF_refittedTrack_bremRadiusErr;
std::vector<std::vector<float> > *el_GSF_refittedTrack_bremZErr;
std::vector<std::vector<int> > *el_GSF_refittedTrack_bremFitStatus;
std::vector<std::vector<float> > *el_GSF_refittedTrack_qoverp;
std::vector<std::vector<float> > *el_GSF_refittedTrack_d0;
std::vector<std::vector<float> > *el_GSF_refittedTrack_z0;
std::vector<std::vector<float> > *el_GSF_refittedTrack_theta;
std::vector<std::vector<float> > *el_GSF_refittedTrack_phi;
std::vector<std::vector<float> > *el_GSF_refittedTrack_LMqoverp;
std::vector<std::vector<float> > *el_GSF_refittedTrack_covd0;
std::vector<std::vector<float> > *el_GSF_refittedTrack_covz0;
std::vector<std::vector<float> > *el_GSF_refittedTrack_covphi;
std::vector<std::vector<float> > *el_GSF_refittedTrack_covtheta;
std::vector<std::vector<float> > *el_GSF_refittedTrack_covqoverp;
std::vector<std::vector<float> > *el_GSF_refittedTrack_covd0z0;
std::vector<std::vector<float> > *el_GSF_refittedTrack_covz0phi;
std::vector<std::vector<float> > *el_GSF_refittedTrack_covz0theta;
std::vector<std::vector<float> > *el_GSF_refittedTrack_covz0qoverp;
std::vector<std::vector<float> > *el_GSF_refittedTrack_covd0phi;
std::vector<std::vector<float> > *el_GSF_refittedTrack_covd0theta;
std::vector<std::vector<float> > *el_GSF_refittedTrack_covd0qoverp;
std::vector<std::vector<float> > *el_GSF_refittedTrack_covphitheta;
std::vector<std::vector<float> > *el_GSF_refittedTrack_covphiqoverp;
std::vector<std::vector<float> > *el_GSF_refittedTrack_covthetaqoverp;
std::vector<float>   *el_GSF_Es0;
std::vector<float>   *el_GSF_etas0;
std::vector<float>   *el_GSF_phis0;
std::vector<float>   *el_GSF_Es1;
std::vector<float>   *el_GSF_etas1;
std::vector<float>   *el_GSF_phis1;
std::vector<float>   *el_GSF_Es2;
std::vector<float>   *el_GSF_etas2;
std::vector<float>   *el_GSF_phis2;
std::vector<float>   *el_GSF_Es3;
std::vector<float>   *el_GSF_etas3;
std::vector<float>   *el_GSF_phis3;
std::vector<float>   *el_GSF_E_PreSamplerB;
std::vector<float>   *el_GSF_E_EMB1;
std::vector<float>   *el_GSF_E_EMB2;
std::vector<float>   *el_GSF_E_EMB3;
std::vector<float>   *el_GSF_E_PreSamplerE;
std::vector<float>   *el_GSF_E_EME1;
std::vector<float>   *el_GSF_E_EME2;
std::vector<float>   *el_GSF_E_EME3;
std::vector<float>   *el_GSF_E_HEC0;
std::vector<float>   *el_GSF_E_HEC1;
std::vector<float>   *el_GSF_E_HEC2;
std::vector<float>   *el_GSF_E_HEC3;
std::vector<float>   *el_GSF_E_TileBar0;
std::vector<float>   *el_GSF_E_TileBar1;
std::vector<float>   *el_GSF_E_TileBar2;
std::vector<float>   *el_GSF_E_TileGap1;
std::vector<float>   *el_GSF_E_TileGap2;
std::vector<float>   *el_GSF_E_TileGap3;
std::vector<float>   *el_GSF_E_TileExt0;
std::vector<float>   *el_GSF_E_TileExt1;
std::vector<float>   *el_GSF_E_TileExt2;
std::vector<float>   *el_GSF_E_FCAL0;
std::vector<float>   *el_GSF_E_FCAL1;
std::vector<float>   *el_GSF_E_FCAL2;
std::vector<float>   *el_GSF_cl_E;
std::vector<float>   *el_GSF_cl_pt;
std::vector<float>   *el_GSF_cl_eta;
std::vector<float>   *el_GSF_cl_phi;
std::vector<double>  *el_GSF_cl_etaCalo;
std::vector<double>  *el_GSF_cl_phiCalo;
std::vector<float>   *el_GSF_firstEdens;
std::vector<float>   *el_GSF_cellmaxfrac;
std::vector<float>   *el_GSF_longitudinal;
std::vector<float>   *el_GSF_secondlambda;
std::vector<float>   *el_GSF_lateral;
std::vector<float>   *el_GSF_secondR;
std::vector<float>   *el_GSF_centerlambda;
std::vector<float>   *el_GSF_rawcl_Es0;
std::vector<float>   *el_GSF_rawcl_etas0;
std::vector<float>   *el_GSF_rawcl_phis0;
std::vector<float>   *el_GSF_rawcl_Es1;
std::vector<float>   *el_GSF_rawcl_etas1;
std::vector<float>   *el_GSF_rawcl_phis1;
std::vector<float>   *el_GSF_rawcl_Es2;
std::vector<float>   *el_GSF_rawcl_etas2;
std::vector<float>   *el_GSF_rawcl_phis2;
std::vector<float>   *el_GSF_rawcl_Es3;
std::vector<float>   *el_GSF_rawcl_etas3;
std::vector<float>   *el_GSF_rawcl_phis3;
std::vector<float>   *el_GSF_rawcl_E;
std::vector<float>   *el_GSF_rawcl_pt;
std::vector<float>   *el_GSF_rawcl_eta;
std::vector<float>   *el_GSF_rawcl_phi;
std::vector<float>   *el_GSF_trackd0;
std::vector<float>   *el_GSF_trackz0;
std::vector<float>   *el_GSF_trackphi;
std::vector<float>   *el_GSF_tracktheta;
std::vector<float>   *el_GSF_trackqoverp;
std::vector<float>   *el_GSF_trackpt;
std::vector<float>   *el_GSF_tracketa;
std::vector<float>   *el_GSF_trackcov_d0;
std::vector<float>   *el_GSF_trackcov_z0;
std::vector<float>   *el_GSF_trackcov_phi;
std::vector<float>   *el_GSF_trackcov_theta;
std::vector<float>   *el_GSF_trackcov_qoverp;
std::vector<float>   *el_GSF_trackcov_d0_z0;
std::vector<float>   *el_GSF_trackcov_d0_phi;
std::vector<float>   *el_GSF_trackcov_d0_theta;
std::vector<float>   *el_GSF_trackcov_d0_qoverp;
std::vector<float>   *el_GSF_trackcov_z0_phi;
std::vector<float>   *el_GSF_trackcov_z0_theta;
std::vector<float>   *el_GSF_trackcov_z0_qoverp;
std::vector<float>   *el_GSF_trackcov_phi_theta;
std::vector<float>   *el_GSF_trackcov_phi_qoverp;
std::vector<float>   *el_GSF_trackcov_theta_qoverp;
std::vector<float>   *el_GSF_trackfitchi2;
std::vector<int>     *el_GSF_trackfitndof;
std::vector<int>     *el_GSF_nBLHits;
std::vector<int>     *el_GSF_nPixHits;
std::vector<int>     *el_GSF_nSCTHits;
std::vector<int>     *el_GSF_nTRTHits;
std::vector<int>     *el_GSF_nTRTHighTHits;
std::vector<int>     *el_GSF_nPixHoles;
std::vector<int>     *el_GSF_nSCTHoles;
std::vector<int>     *el_GSF_nTRTHoles;
std::vector<int>     *el_GSF_nBLSharedHits;
std::vector<int>     *el_GSF_nPixSharedHits;
std::vector<int>     *el_GSF_nSCTSharedHits;
std::vector<int>     *el_GSF_nBLayerOutliers;
std::vector<int>     *el_GSF_nPixelOutliers;
std::vector<int>     *el_GSF_nSCTOutliers;
std::vector<int>     *el_GSF_nTRTOutliers;
std::vector<int>     *el_GSF_nTRTHighTOutliers;
std::vector<int>     *el_GSF_nContribPixelLayers;
std::vector<int>     *el_GSF_nGangedPixels;
std::vector<int>     *el_GSF_nGangedFlaggedFakes;
std::vector<int>     *el_GSF_nPixelDeadSensors;
std::vector<int>     *el_GSF_nPixelSpoiltHits;
std::vector<int>     *el_GSF_nSCTDoubleHoles;
std::vector<int>     *el_GSF_nSCTDeadSensors;
std::vector<int>     *el_GSF_nSCTSpoiltHits;
std::vector<int>     *el_GSF_expectBLayerHit;
std::vector<int>     *el_GSF_nSiHits;
std::vector<float>   *el_GSF_TRTHighTHitsRatio;
std::vector<float>   *el_GSF_TRTHighTOutliersRatio;
std::vector<float>   *el_GSF_pixeldEdx;
std::vector<int>     *el_GSF_nGoodHitsPixeldEdx;
std::vector<float>   *el_GSF_massPixeldEdx;
std::vector<std::vector<float> > *el_GSF_likelihoodsPixeldEdx;
std::vector<float>   *el_GSF_eProbabilityComb;
std::vector<float>   *el_GSF_eProbabilityHT;
std::vector<float>   *el_GSF_eProbabilityToT;
std::vector<float>   *el_GSF_eProbabilityBrem;
std::vector<float>   *el_GSF_vertweight;
std::vector<float>   *el_GSF_vertx;
std::vector<float>   *el_GSF_verty;
std::vector<float>   *el_GSF_vertz;
std::vector<float>   *el_GSF_trackd0beam;
std::vector<float>   *el_GSF_trackz0beam;
std::vector<float>   *el_GSF_tracksigd0beam;
std::vector<float>   *el_GSF_tracksigz0beam;
std::vector<float>   *el_GSF_trackd0pv;
std::vector<float>   *el_GSF_trackz0pv;
std::vector<float>   *el_GSF_tracksigd0pv;
std::vector<float>   *el_GSF_tracksigz0pv;
std::vector<float>   *el_GSF_trackIPEstimate_d0_biasedpvunbiased;
std::vector<float>   *el_GSF_trackIPEstimate_z0_biasedpvunbiased;
std::vector<float>   *el_GSF_trackIPEstimate_sigd0_biasedpvunbiased;
std::vector<float>   *el_GSF_trackIPEstimate_sigz0_biasedpvunbiased;
std::vector<float>   *el_GSF_trackIPEstimate_d0_unbiasedpvunbiased;
std::vector<float>   *el_GSF_trackIPEstimate_z0_unbiasedpvunbiased;
std::vector<float>   *el_GSF_trackIPEstimate_sigd0_unbiasedpvunbiased;
std::vector<float>   *el_GSF_trackIPEstimate_sigz0_unbiasedpvunbiased;
std::vector<float>   *el_GSF_trackd0pvunbiased;
std::vector<float>   *el_GSF_trackz0pvunbiased;
std::vector<float>   *el_GSF_tracksigd0pvunbiased;
std::vector<float>   *el_GSF_tracksigz0pvunbiased;
std::vector<int>     *el_GSF_hastrack;
std::vector<float>   *el_GSF_deltaEmax2;
std::vector<float>   *el_GSF_calibHitsShowerDepth;
std::vector<unsigned int> *el_GSF_isIso;
std::vector<float>   *el_GSF_mvaptcone20;
std::vector<float>   *el_GSF_mvaptcone30;
std::vector<float>   *el_GSF_mvaptcone40;
std::vector<float>   *el_GSF_truth_bremSi;
std::vector<float>   *el_GSF_truth_bremLoc;
std::vector<float>   *el_GSF_truth_sumbrem;
std::vector<float>   *el_GSF_Etcone40_ED_corrected;
std::vector<float>   *el_GSF_Etcone40_corrected;
std::vector<float>   *el_GSF_jet_dr;
std::vector<float>   *el_GSF_jet_E;
std::vector<float>   *el_GSF_jet_pt;
std::vector<float>   *el_GSF_jet_m;
std::vector<float>   *el_GSF_jet_eta;
std::vector<float>   *el_GSF_jet_phi;
std::vector<float>   *el_GSF_jet_truth_dr;
std::vector<float>   *el_GSF_jet_truth_E;
std::vector<float>   *el_GSF_jet_truth_pt;
std::vector<float>   *el_GSF_jet_truth_m;
std::vector<float>   *el_GSF_jet_truth_eta;
std::vector<float>   *el_GSF_jet_truth_phi;
std::vector<int>     *el_GSF_jet_truth_matched;
std::vector<int>     *el_GSF_jet_matched;
std::vector<float>   *el_GSF_EF_dr;
std::vector<int>     *el_GSF_EF_index;
std::vector<float>   *el_GSF_L2_dr;
std::vector<int>     *el_GSF_L2_index;
std::vector<float>   *el_GSF_L1_dr;
std::vector<int>     *el_GSF_L1_index;

/*2012*/
Int_t                 el_n;
std::vector<float>   *el_E;
std::vector<float>   *el_Et;
std::vector<float>   *el_pt;
std::vector<float>   *el_m;
std::vector<float>   *el_eta;
std::vector<float>   *el_phi;
std::vector<float>   *el_px;
std::vector<float>   *el_py;
std::vector<float>   *el_pz;
std::vector<float>   *el_charge;
std::vector<int>     *el_author;
std::vector<unsigned int> *el_isEM;
std::vector<unsigned int> *el_isEMLoose;
std::vector<unsigned int> *el_isEMMedium;
std::vector<unsigned int> *el_isEMTight;
std::vector<unsigned int> *el_OQ;
std::vector<int>     *el_convFlag;
std::vector<int>     *el_isConv;
std::vector<int>     *el_nConv;
std::vector<int>     *el_nSingleTrackConv;
std::vector<int>     *el_nDoubleTrackConv;
std::vector<int>     *el_type;
std::vector<int>     *el_origin;
std::vector<int>     *el_typebkg;
std::vector<int>     *el_originbkg;
std::vector<float>   *el_truth_E;
std::vector<float>   *el_truth_pt;
std::vector<float>   *el_truth_eta;
std::vector<float>   *el_truth_phi;
std::vector<int>     *el_truth_type;
std::vector<int>     *el_truth_status;
std::vector<int>     *el_truth_barcode;
std::vector<int>     *el_truth_mothertype;
std::vector<int>     *el_truth_motherbarcode;
std::vector<int>     *el_truth_hasHardBrem;
std::vector<int>     *el_truth_index;
std::vector<int>     *el_truth_matched;
std::vector<int>     *el_mediumWithoutTrack;
std::vector<int>     *el_mediumIsoWithoutTrack;
std::vector<int>     *el_tightWithoutTrack;
std::vector<int>     *el_tightIsoWithoutTrack;
std::vector<int>     *el_loose;
std::vector<int>     *el_looseIso;
std::vector<int>     *el_medium;
std::vector<int>     *el_mediumIso;
std::vector<int>     *el_tight;
std::vector<int>     *el_tightIso;
std::vector<int>     *el_loosePP;
std::vector<int>     *el_loosePPIso;
std::vector<int>     *el_mediumPP;
std::vector<int>     *el_mediumPPIso;
std::vector<int>     *el_tightPP;
std::vector<int>     *el_tightPPIso;
std::vector<int>     *el_goodOQ;
std::vector<float>   *el_Ethad;
std::vector<float>   *el_Ethad1;
std::vector<float>   *el_f1;
std::vector<float>   *el_f1core;
std::vector<float>   *el_Emins1;
std::vector<float>   *el_fside;
std::vector<float>   *el_Emax2;
std::vector<float>   *el_ws3;
std::vector<float>   *el_wstot;
std::vector<float>   *el_emaxs1;
std::vector<float>   *el_deltaEs;
std::vector<float>   *el_E233;
std::vector<float>   *el_E237;
std::vector<float>   *el_E277;
std::vector<float>   *el_weta2;
std::vector<float>   *el_f3;
std::vector<float>   *el_f3core;
std::vector<float>   *el_rphiallcalo;
std::vector<float>   *el_Etcone45;
std::vector<float>   *el_Etcone15;
std::vector<float>   *el_Etcone20;
std::vector<float>   *el_Etcone25;
std::vector<float>   *el_Etcone30;
std::vector<float>   *el_Etcone35;
std::vector<float>   *el_Etcone40;
std::vector<float>   *el_ptcone20;
std::vector<float>   *el_ptcone30;
std::vector<float>   *el_ptcone40;
std::vector<float>   *el_nucone20;
std::vector<float>   *el_nucone30;
std::vector<float>   *el_nucone40;
std::vector<float>   *el_Etcone15_pt_corrected;
std::vector<float>   *el_Etcone20_pt_corrected;
std::vector<float>   *el_Etcone25_pt_corrected;
std::vector<float>   *el_Etcone30_pt_corrected;
std::vector<float>   *el_Etcone35_pt_corrected;
std::vector<float>   *el_Etcone40_pt_corrected;
std::vector<float>   *el_convanglematch;
std::vector<float>   *el_convtrackmatch;
std::vector<int>     *el_hasconv;
std::vector<float>   *el_convvtxx;
std::vector<float>   *el_convvtxy;
std::vector<float>   *el_convvtxz;
std::vector<float>   *el_Rconv;
std::vector<float>   *el_zconv;
std::vector<float>   *el_convvtxchi2;
std::vector<float>   *el_pt1conv;
std::vector<int>     *el_convtrk1nBLHits;
std::vector<int>     *el_convtrk1nPixHits;
std::vector<int>     *el_convtrk1nSCTHits;
std::vector<int>     *el_convtrk1nTRTHits;
std::vector<float>   *el_pt2conv;
std::vector<int>     *el_convtrk2nBLHits;
std::vector<int>     *el_convtrk2nPixHits;
std::vector<int>     *el_convtrk2nSCTHits;
std::vector<int>     *el_convtrk2nTRTHits;
std::vector<float>   *el_ptconv;
std::vector<float>   *el_pzconv;
std::vector<float>   *el_pos7;
std::vector<float>   *el_etacorrmag;
std::vector<float>   *el_deltaeta1;
std::vector<float>   *el_deltaeta2;
std::vector<float>   *el_deltaphi2;
std::vector<float>   *el_deltaphiRescaled;
std::vector<float>   *el_deltaPhiFromLast;
std::vector<float>   *el_deltaPhiRot;
std::vector<float>   *el_expectHitInBLayer;
std::vector<float>   *el_trackd0_physics;
std::vector<float>   *el_etaSampling1;
std::vector<float>   *el_reta;
std::vector<float>   *el_rphi;
std::vector<float>   *el_topoEtcone20;
std::vector<float>   *el_topoEtcone30;
std::vector<float>   *el_topoEtcone40;
std::vector<float>   *el_EtringnoisedR03sig2;
std::vector<float>   *el_EtringnoisedR03sig3;
std::vector<float>   *el_EtringnoisedR03sig4;
std::vector<double>  *el_isolationlikelihoodjets;
std::vector<double>  *el_isolationlikelihoodhqelectrons;
std::vector<double>  *el_electronweight;
std::vector<double>  *el_electronbgweight;
std::vector<double>  *el_softeweight;
std::vector<double>  *el_softebgweight;
std::vector<double>  *el_neuralnet;
std::vector<double>  *el_Hmatrix;
std::vector<double>  *el_Hmatrix5;
std::vector<double>  *el_adaboost;
std::vector<double>  *el_softeneuralnet;
std::vector<double>  *el_ringernn;
std::vector<float>   *el_zvertex;
std::vector<float>   *el_errz;
std::vector<float>   *el_etap;
std::vector<float>   *el_depth;
std::vector<int>     *el_refittedTrack_n;
std::vector<std::vector<int> > *el_refittedTrack_author;
std::vector<std::vector<float> > *el_refittedTrack_chi2;
std::vector<std::vector<int> > *el_refittedTrack_hasBrem;
std::vector<std::vector<float> > *el_refittedTrack_bremRadius;
std::vector<std::vector<float> > *el_refittedTrack_bremZ;
std::vector<std::vector<float> > *el_refittedTrack_bremRadiusErr;
std::vector<std::vector<float> > *el_refittedTrack_bremZErr;
std::vector<std::vector<int> > *el_refittedTrack_bremFitStatus;
std::vector<std::vector<float> > *el_refittedTrack_qoverp;
std::vector<std::vector<float> > *el_refittedTrack_d0;
std::vector<std::vector<float> > *el_refittedTrack_z0;
std::vector<std::vector<float> > *el_refittedTrack_theta;
std::vector<std::vector<float> > *el_refittedTrack_phi;
std::vector<std::vector<float> > *el_refittedTrack_LMqoverp;
std::vector<std::vector<float> > *el_refittedTrack_covd0;
std::vector<std::vector<float> > *el_refittedTrack_covz0;
std::vector<std::vector<float> > *el_refittedTrack_covphi;
std::vector<std::vector<float> > *el_refittedTrack_covtheta;
std::vector<std::vector<float> > *el_refittedTrack_covqoverp;
std::vector<std::vector<float> > *el_refittedTrack_covd0z0;
std::vector<std::vector<float> > *el_refittedTrack_covz0phi;
std::vector<std::vector<float> > *el_refittedTrack_covz0theta;
std::vector<std::vector<float> > *el_refittedTrack_covz0qoverp;
std::vector<std::vector<float> > *el_refittedTrack_covd0phi;
std::vector<std::vector<float> > *el_refittedTrack_covd0theta;
std::vector<std::vector<float> > *el_refittedTrack_covd0qoverp;
std::vector<std::vector<float> > *el_refittedTrack_covphitheta;
std::vector<std::vector<float> > *el_refittedTrack_covphiqoverp;
std::vector<std::vector<float> > *el_refittedTrack_covthetaqoverp;
std::vector<float>   *el_Es0;
std::vector<float>   *el_etas0;
std::vector<float>   *el_phis0;
std::vector<float>   *el_Es1;
std::vector<float>   *el_etas1;
std::vector<float>   *el_phis1;
std::vector<float>   *el_Es2;
std::vector<float>   *el_etas2;
std::vector<float>   *el_phis2;
std::vector<float>   *el_Es3;
std::vector<float>   *el_etas3;
std::vector<float>   *el_phis3;
std::vector<float>   *el_E_PreSamplerB;
std::vector<float>   *el_E_EMB1;
std::vector<float>   *el_E_EMB2;
std::vector<float>   *el_E_EMB3;
std::vector<float>   *el_E_PreSamplerE;
std::vector<float>   *el_E_EME1;
std::vector<float>   *el_E_EME2;
std::vector<float>   *el_E_EME3;
std::vector<float>   *el_E_HEC0;
std::vector<float>   *el_E_HEC1;
std::vector<float>   *el_E_HEC2;
std::vector<float>   *el_E_HEC3;
std::vector<float>   *el_E_TileBar0;
std::vector<float>   *el_E_TileBar1;
std::vector<float>   *el_E_TileBar2;
std::vector<float>   *el_E_TileGap1;
std::vector<float>   *el_E_TileGap2;
std::vector<float>   *el_E_TileGap3;
std::vector<float>   *el_E_TileExt0;
std::vector<float>   *el_E_TileExt1;
std::vector<float>   *el_E_TileExt2;
std::vector<float>   *el_E_FCAL0;
std::vector<float>   *el_E_FCAL1;
std::vector<float>   *el_E_FCAL2;
std::vector<float>   *el_cl_E;
std::vector<float>   *el_cl_pt;
std::vector<float>   *el_cl_eta;
std::vector<float>   *el_cl_phi;
std::vector<double>  *el_cl_etaCalo;
std::vector<double>  *el_cl_phiCalo;
std::vector<float>   *el_firstEdens;
std::vector<float>   *el_cellmaxfrac;
std::vector<float>   *el_longitudinal;
std::vector<float>   *el_secondlambda;
std::vector<float>   *el_lateral;
std::vector<float>   *el_secondR;
std::vector<float>   *el_centerlambda;
std::vector<float>   *el_rawcl_Es0;
std::vector<float>   *el_rawcl_etas0;
std::vector<float>   *el_rawcl_phis0;
std::vector<float>   *el_rawcl_Es1;
std::vector<float>   *el_rawcl_etas1;
std::vector<float>   *el_rawcl_phis1;
std::vector<float>   *el_rawcl_Es2;
std::vector<float>   *el_rawcl_etas2;
std::vector<float>   *el_rawcl_phis2;
std::vector<float>   *el_rawcl_Es3;
std::vector<float>   *el_rawcl_etas3;
std::vector<float>   *el_rawcl_phis3;
std::vector<float>   *el_rawcl_E;
std::vector<float>   *el_rawcl_pt;
std::vector<float>   *el_rawcl_eta;
std::vector<float>   *el_rawcl_phi;
std::vector<std::vector<float> > *el_rings_E;
std::vector<float>   *el_trackd0;
std::vector<float>   *el_trackz0;
std::vector<float>   *el_trackphi;
std::vector<float>   *el_tracktheta;
std::vector<float>   *el_trackqoverp;
std::vector<float>   *el_trackpt;
std::vector<float>   *el_tracketa;
std::vector<float>   *el_trackcov_d0;
std::vector<float>   *el_trackcov_z0;
std::vector<float>   *el_trackcov_phi;
std::vector<float>   *el_trackcov_theta;
std::vector<float>   *el_trackcov_qoverp;
std::vector<float>   *el_trackcov_d0_z0;
std::vector<float>   *el_trackcov_d0_phi;
std::vector<float>   *el_trackcov_d0_theta;
std::vector<float>   *el_trackcov_d0_qoverp;
std::vector<float>   *el_trackcov_z0_phi;
std::vector<float>   *el_trackcov_z0_theta;
std::vector<float>   *el_trackcov_z0_qoverp;
std::vector<float>   *el_trackcov_phi_theta;
std::vector<float>   *el_trackcov_phi_qoverp;
std::vector<float>   *el_trackcov_theta_qoverp;
std::vector<float>   *el_trackfitchi2;
std::vector<int>     *el_trackfitndof;
std::vector<int>     *el_nBLHits;
std::vector<int>     *el_nPixHits;
std::vector<int>     *el_nSCTHits;
std::vector<int>     *el_nTRTHits;
std::vector<int>     *el_nTRTHighTHits;
std::vector<int>     *el_nPixHoles;
std::vector<int>     *el_nSCTHoles;
std::vector<int>     *el_nTRTHoles;
std::vector<int>     *el_nPixelDeadSensors;
std::vector<int>     *el_nSCTDeadSensors;
std::vector<int>     *el_nBLSharedHits;
std::vector<int>     *el_nPixSharedHits;
std::vector<int>     *el_nSCTSharedHits;
std::vector<int>     *el_nBLayerSplitHits;
std::vector<int>     *el_nPixSplitHits;
std::vector<int>     *el_nBLayerOutliers;
std::vector<int>     *el_nPixelOutliers;
std::vector<int>     *el_nSCTOutliers;
std::vector<int>     *el_nTRTOutliers;
std::vector<int>     *el_nTRTHighTOutliers;
std::vector<int>     *el_nContribPixelLayers;
std::vector<int>     *el_nGangedPixels;
std::vector<int>     *el_nGangedFlaggedFakes;
std::vector<int>     *el_nPixelSpoiltHits;
std::vector<int>     *el_nSCTDoubleHoles;
std::vector<int>     *el_nSCTSpoiltHits;
std::vector<int>     *el_expectBLayerHit;
std::vector<int>     *el_nSiHits;
std::vector<float>   *el_TRTHighTHitsRatio;
std::vector<float>   *el_TRTHighTOutliersRatio;
std::vector<float>   *el_pixeldEdx;
std::vector<int>     *el_nGoodHitsPixeldEdx;
std::vector<float>   *el_massPixeldEdx;
std::vector<std::vector<float> > *el_likelihoodsPixeldEdx;
std::vector<float>   *el_eProbabilityComb;
std::vector<float>   *el_eProbabilityHT;
std::vector<float>   *el_eProbabilityToT;
std::vector<float>   *el_eProbabilityBrem;
std::vector<float>   *el_vertweight;
std::vector<float>   *el_vertx;
std::vector<float>   *el_verty;
std::vector<float>   *el_vertz;
std::vector<float>   *el_trackd0beam;
std::vector<float>   *el_trackz0beam;
std::vector<float>   *el_tracksigd0beam;
std::vector<float>   *el_tracksigz0beam;
std::vector<float>   *el_trackd0pv;
std::vector<float>   *el_trackz0pv;
std::vector<float>   *el_tracksigd0pv;
std::vector<float>   *el_tracksigz0pv;
std::vector<float>   *el_trackIPEstimate_d0_biasedpvunbiased;
std::vector<float>   *el_trackIPEstimate_z0_biasedpvunbiased;
std::vector<float>   *el_trackIPEstimate_sigd0_biasedpvunbiased;
std::vector<float>   *el_trackIPEstimate_sigz0_biasedpvunbiased;
std::vector<float>   *el_trackIPEstimate_d0_unbiasedpvunbiased;
std::vector<float>   *el_trackIPEstimate_z0_unbiasedpvunbiased;
std::vector<float>   *el_trackIPEstimate_sigd0_unbiasedpvunbiased;
std::vector<float>   *el_trackIPEstimate_sigz0_unbiasedpvunbiased;
std::vector<float>   *el_trackd0pvunbiased;
std::vector<float>   *el_trackz0pvunbiased;
std::vector<float>   *el_tracksigd0pvunbiased;
std::vector<float>   *el_tracksigz0pvunbiased;
std::vector<float>   *el_Unrefittedtrack_d0;
std::vector<float>   *el_Unrefittedtrack_z0;
std::vector<float>   *el_Unrefittedtrack_phi;
std::vector<float>   *el_Unrefittedtrack_theta;
std::vector<float>   *el_Unrefittedtrack_qoverp;
std::vector<float>   *el_Unrefittedtrack_pt;
std::vector<float>   *el_Unrefittedtrack_eta;
std::vector<int>     *el_hastrack;
std::vector<float>   *el_deltaEmax2;
std::vector<float>   *el_calibHitsShowerDepth;
std::vector<unsigned int> *el_isIso;
std::vector<float>   *el_mvaptcone20;
std::vector<float>   *el_mvaptcone30;
std::vector<float>   *el_mvaptcone40;
std::vector<float>   *el_CaloPointing_eta;
std::vector<float>   *el_CaloPointing_sigma_eta;
std::vector<float>   *el_CaloPointing_zvertex;
std::vector<float>   *el_CaloPointing_sigma_zvertex;
std::vector<float>   *el_HPV_eta;
std::vector<float>   *el_HPV_sigma_eta;
std::vector<float>   *el_HPV_zvertex;
std::vector<float>   *el_HPV_sigma_zvertex;
std::vector<float>   *el_truth_bremSi;
std::vector<float>   *el_truth_bremLoc;
std::vector<float>   *el_truth_sumbrem;
std::vector<float>   *el_topoEtcone60;
std::vector<float>   *el_ES0_real;
std::vector<float>   *el_ES1_real;
std::vector<float>   *el_ES2_real;
std::vector<float>   *el_ES3_real;
std::vector<float>   *el_EcellS0;
std::vector<float>   *el_etacellS0;
std::vector<float>   *el_Etcone40_ED_corrected;
std::vector<float>   *el_Etcone40_corrected;
std::vector<float>   *el_topoEtcone20_corrected;
std::vector<float>   *el_topoEtcone30_corrected;
std::vector<float>   *el_topoEtcone40_corrected;
std::vector<float>   *el_ED_median;
std::vector<float>   *el_ED_sigma;
std::vector<float>   *el_ED_Njets;
std::vector<float>   *el_jet_dr;
std::vector<float>   *el_jet_E;
std::vector<float>   *el_jet_pt;
std::vector<float>   *el_jet_m;
std::vector<float>   *el_jet_eta;
std::vector<float>   *el_jet_phi;
std::vector<float>   *el_jet_truth_dr;
std::vector<float>   *el_jet_truth_E;
std::vector<float>   *el_jet_truth_pt;
std::vector<float>   *el_jet_truth_m;
std::vector<float>   *el_jet_truth_eta;
std::vector<float>   *el_jet_truth_phi;
std::vector<int>     *el_jet_truth_matched;
std::vector<int>     *el_jet_matched;
std::vector<float>   *el_EF_dr;
std::vector<int>     *el_EF_index;

/*trigger match*/
std::vector<std::vector<int> > *trig_RoI_EF_e_egammaContainer_egamma_Electrons;
std::vector<std::vector<int> > *trig_RoI_EF_e_egammaContainer_egamma_ElectronsStatus;
Int_t           trig_EF_el_n;
std::vector<float>   *trig_EF_el_eta;
std::vector<float>   *trig_EF_el_phi;
std::vector<float>   *trig_EF_el_Et;

/* List of Branches */
/*trigger 2011*/
TBranch        *b_EF_e20_medium;   //!
TBranch        *b_EF_e22_medium;   //!
TBranch        *b_EF_e22vh_medium1;   //!
TBranch        *b_EF_2e12_medium;   //!
TBranch        *b_EF_2e12T_medium;   //!
TBranch        *b_EF_2e12Tvh_medium;   //!
TBranch        *b_EF_e10_medium_mu6;   //!
/*trigger 2012*/
TBranch        *b_EF_e24vhi_medium1;   //!
TBranch        *b_EF_e60_medium1;   //!
TBranch        *b_EF_2e12Tvh_loose1;   //!
TBranch        *b_EF_e12Tvh_medium1_mu8;  //!
TBranch        *b_EF_e24vhi_loose1_mu8;  //!

/*2011*/
TBranch        *b_el_GSF_n;   //!
TBranch        *b_el_GSF_E;   //!
TBranch        *b_el_GSF_Et;   //!
TBranch        *b_el_GSF_pt;   //!
TBranch        *b_el_GSF_m;   //!
TBranch        *b_el_GSF_eta;   //!
TBranch        *b_el_GSF_phi;   //!
TBranch        *b_el_GSF_px;   //!
TBranch        *b_el_GSF_py;   //!
TBranch        *b_el_GSF_pz;   //!
TBranch        *b_el_GSF_charge;   //!
TBranch        *b_el_GSF_author;   //!
TBranch        *b_el_GSF_isEM;   //!
TBranch        *b_el_GSF_isEMLoose;   //!
TBranch        *b_el_GSF_isEMMedium;   //!
TBranch        *b_el_GSF_isEMTight;   //!
TBranch        *b_el_GSF_OQ;   //!
TBranch        *b_el_GSF_convFlag;   //!
TBranch        *b_el_GSF_isConv;   //!
TBranch        *b_el_GSF_nConv;   //!
TBranch        *b_el_GSF_nSingleTrackConv;   //!
TBranch        *b_el_GSF_nDoubleTrackConv;   //!
TBranch        *b_el_GSF_type;   //!
TBranch        *b_el_GSF_origin;   //!
TBranch        *b_el_GSF_typebkg;   //!
TBranch        *b_el_GSF_originbkg;   //!
TBranch        *b_el_GSF_truth_E;   //!
TBranch        *b_el_GSF_truth_pt;   //!
TBranch        *b_el_GSF_truth_eta;   //!
TBranch        *b_el_GSF_truth_phi;   //!
TBranch        *b_el_GSF_truth_type;   //!
TBranch        *b_el_GSF_truth_status;   //!
TBranch        *b_el_GSF_truth_barcode;   //!
TBranch        *b_el_GSF_truth_mothertype;   //!
TBranch        *b_el_GSF_truth_motherbarcode;   //!
TBranch        *b_el_GSF_truth_hasHardBrem;   //!
TBranch        *b_el_GSF_truth_index;   //!
TBranch        *b_el_GSF_truth_matched;   //!
TBranch        *b_el_GSF_mediumWithoutTrack;   //!
TBranch        *b_el_GSF_mediumIsoWithoutTrack;   //!
TBranch        *b_el_GSF_tightWithoutTrack;   //!
TBranch        *b_el_GSF_tightIsoWithoutTrack;   //!
TBranch        *b_el_GSF_loose;   //!
TBranch        *b_el_GSF_looseIso;   //!
TBranch        *b_el_GSF_medium;   //!
TBranch        *b_el_GSF_mediumIso;   //!
TBranch        *b_el_GSF_tight;   //!
TBranch        *b_el_GSF_tightIso;   //!
TBranch        *b_el_GSF_loosePP;   //!
TBranch        *b_el_GSF_loosePPIso;   //!
TBranch        *b_el_GSF_mediumPP;   //!
TBranch        *b_el_GSF_mediumPPIso;   //!
TBranch        *b_el_GSF_tightPP;   //!
TBranch        *b_el_GSF_tightPPIso;   //!
TBranch        *b_el_GSF_goodOQ;   //!
TBranch        *b_el_GSF_Ethad;   //!
TBranch        *b_el_GSF_Ethad1;   //!
TBranch        *b_el_GSF_f1;   //!
TBranch        *b_el_GSF_f1core;   //!
TBranch        *b_el_GSF_Emins1;   //!
TBranch        *b_el_GSF_fside;   //!
TBranch        *b_el_GSF_Emax2;   //!
TBranch        *b_el_GSF_ws3;   //!
TBranch        *b_el_GSF_wstot;   //!
TBranch        *b_el_GSF_emaxs1;   //!
TBranch        *b_el_GSF_deltaEs;   //!
TBranch        *b_el_GSF_E233;   //!
TBranch        *b_el_GSF_E237;   //!
TBranch        *b_el_GSF_E277;   //!
TBranch        *b_el_GSF_weta2;   //!
TBranch        *b_el_GSF_f3;   //!
TBranch        *b_el_GSF_f3core;   //!
TBranch        *b_el_GSF_rphiallcalo;   //!
TBranch        *b_el_GSF_Etcone45;   //!
TBranch        *b_el_GSF_Etcone15;   //!
TBranch        *b_el_GSF_Etcone20;   //!
TBranch        *b_el_GSF_Etcone25;   //!
TBranch        *b_el_GSF_Etcone30;   //!
TBranch        *b_el_GSF_Etcone35;   //!
TBranch        *b_el_GSF_Etcone40;   //!
TBranch        *b_el_GSF_ptcone20;   //!
TBranch        *b_el_GSF_ptcone30;   //!
TBranch        *b_el_GSF_ptcone40;   //!
TBranch        *b_el_GSF_nucone20;   //!
TBranch        *b_el_GSF_nucone30;   //!
TBranch        *b_el_GSF_nucone40;   //!
TBranch        *b_el_GSF_Etcone15_pt_corrected;   //!
TBranch        *b_el_GSF_Etcone20_pt_corrected;   //!
TBranch        *b_el_GSF_Etcone25_pt_corrected;   //!
TBranch        *b_el_GSF_Etcone30_pt_corrected;   //!
TBranch        *b_el_GSF_Etcone35_pt_corrected;   //!
TBranch        *b_el_GSF_Etcone40_pt_corrected;   //!
TBranch        *b_el_GSF_convanglematch;   //!
TBranch        *b_el_GSF_convtrackmatch;   //!
TBranch        *b_el_GSF_hasconv;   //!
TBranch        *b_el_GSF_convvtxx;   //!
TBranch        *b_el_GSF_convvtxy;   //!
TBranch        *b_el_GSF_convvtxz;   //!
TBranch        *b_el_GSF_Rconv;   //!
TBranch        *b_el_GSF_zconv;   //!
TBranch        *b_el_GSF_convvtxchi2;   //!
TBranch        *b_el_GSF_pt1conv;   //!
TBranch        *b_el_GSF_convtrk1nBLHits;   //!
TBranch        *b_el_GSF_convtrk1nPixHits;   //!
TBranch        *b_el_GSF_convtrk1nSCTHits;   //!
TBranch        *b_el_GSF_convtrk1nTRTHits;   //!
TBranch        *b_el_GSF_pt2conv;   //!
TBranch        *b_el_GSF_convtrk2nBLHits;   //!
TBranch        *b_el_GSF_convtrk2nPixHits;   //!
TBranch        *b_el_GSF_convtrk2nSCTHits;   //!
TBranch        *b_el_GSF_convtrk2nTRTHits;   //!
TBranch        *b_el_GSF_ptconv;   //!
TBranch        *b_el_GSF_pzconv;   //!
TBranch        *b_el_GSF_pos7;   //!
TBranch        *b_el_GSF_etacorrmag;   //!
TBranch        *b_el_GSF_deltaeta1;   //!
TBranch        *b_el_GSF_deltaeta2;   //!
TBranch        *b_el_GSF_deltaphi2;   //!
TBranch        *b_el_GSF_deltaphiRescaled;   //!
TBranch        *b_el_GSF_deltaPhiFromLastMeasurement;   //!
TBranch        *b_el_GSF_deltaPhiRot;   //!
TBranch        *b_el_GSF_expectHitInBLayer;   //!
TBranch        *b_el_GSF_trackd0_physics;   //!
TBranch        *b_el_GSF_etaSampling1;   //!
TBranch        *b_el_GSF_reta;   //!
TBranch        *b_el_GSF_rphi;   //!
TBranch        *b_el_GSF_EtringnoisedR03sig2;   //!
TBranch        *b_el_GSF_EtringnoisedR03sig3;   //!
TBranch        *b_el_GSF_EtringnoisedR03sig4;   //!
TBranch        *b_el_GSF_isolationlikelihoodjets;   //!
TBranch        *b_el_GSF_isolationlikelihoodhqelectrons;   //!
TBranch        *b_el_GSF_electronweight;   //!
TBranch        *b_el_GSF_electronbgweight;   //!
TBranch        *b_el_GSF_softeweight;   //!
TBranch        *b_el_GSF_softebgweight;   //!
TBranch        *b_el_GSF_neuralnet;   //!
TBranch        *b_el_GSF_Hmatrix;   //!
TBranch        *b_el_GSF_Hmatrix5;   //!
TBranch        *b_el_GSF_adaboost;   //!
TBranch        *b_el_GSF_softeneuralnet;   //!
TBranch        *b_el_GSF_zvertex;   //!
TBranch        *b_el_GSF_errz;   //!
TBranch        *b_el_GSF_etap;   //!
TBranch        *b_el_GSF_depth;   //!
TBranch        *b_el_GSF_refittedTrack_n;   //!
TBranch        *b_el_GSF_refittedTrack_author;   //!
TBranch        *b_el_GSF_refittedTrack_chi2;   //!
TBranch        *b_el_GSF_refittedTrack_hasBrem;   //!
TBranch        *b_el_GSF_refittedTrack_bremRadius;   //!
TBranch        *b_el_GSF_refittedTrack_bremZ;   //!
TBranch        *b_el_GSF_refittedTrack_bremRadiusErr;   //!
TBranch        *b_el_GSF_refittedTrack_bremZErr;   //!
TBranch        *b_el_GSF_refittedTrack_bremFitStatus;   //!
TBranch        *b_el_GSF_refittedTrack_qoverp;   //!
TBranch        *b_el_GSF_refittedTrack_d0;   //!
TBranch        *b_el_GSF_refittedTrack_z0;   //!
TBranch        *b_el_GSF_refittedTrack_theta;   //!
TBranch        *b_el_GSF_refittedTrack_phi;   //!
TBranch        *b_el_GSF_refittedTrack_LMqoverp;   //!
TBranch        *b_el_GSF_refittedTrack_covd0;   //!
TBranch        *b_el_GSF_refittedTrack_covz0;   //!
TBranch        *b_el_GSF_refittedTrack_covphi;   //!
TBranch        *b_el_GSF_refittedTrack_covtheta;   //!
TBranch        *b_el_GSF_refittedTrack_covqoverp;   //!
TBranch        *b_el_GSF_refittedTrack_covd0z0;   //!
TBranch        *b_el_GSF_refittedTrack_covz0phi;   //!
TBranch        *b_el_GSF_refittedTrack_covz0theta;   //!
TBranch        *b_el_GSF_refittedTrack_covz0qoverp;   //!
TBranch        *b_el_GSF_refittedTrack_covd0phi;   //!
TBranch        *b_el_GSF_refittedTrack_covd0theta;   //!
TBranch        *b_el_GSF_refittedTrack_covd0qoverp;   //!
TBranch        *b_el_GSF_refittedTrack_covphitheta;   //!
TBranch        *b_el_GSF_refittedTrack_covphiqoverp;   //!
TBranch        *b_el_GSF_refittedTrack_covthetaqoverp;   //!
TBranch        *b_el_GSF_Es0;   //!
TBranch        *b_el_GSF_etas0;   //!
TBranch        *b_el_GSF_phis0;   //!
TBranch        *b_el_GSF_Es1;   //!
TBranch        *b_el_GSF_etas1;   //!
TBranch        *b_el_GSF_phis1;   //!
TBranch        *b_el_GSF_Es2;   //!
TBranch        *b_el_GSF_etas2;   //!
TBranch        *b_el_GSF_phis2;   //!
TBranch        *b_el_GSF_Es3;   //!
TBranch        *b_el_GSF_etas3;   //!
TBranch        *b_el_GSF_phis3;   //!
TBranch        *b_el_GSF_E_PreSamplerB;   //!
TBranch        *b_el_GSF_E_EMB1;   //!
TBranch        *b_el_GSF_E_EMB2;   //!
TBranch        *b_el_GSF_E_EMB3;   //!
TBranch        *b_el_GSF_E_PreSamplerE;   //!
TBranch        *b_el_GSF_E_EME1;   //!
TBranch        *b_el_GSF_E_EME2;   //!
TBranch        *b_el_GSF_E_EME3;   //!
TBranch        *b_el_GSF_E_HEC0;   //!
TBranch        *b_el_GSF_E_HEC1;   //!
TBranch        *b_el_GSF_E_HEC2;   //!
TBranch        *b_el_GSF_E_HEC3;   //!
TBranch        *b_el_GSF_E_TileBar0;   //!
TBranch        *b_el_GSF_E_TileBar1;   //!
TBranch        *b_el_GSF_E_TileBar2;   //!
TBranch        *b_el_GSF_E_TileGap1;   //!
TBranch        *b_el_GSF_E_TileGap2;   //!
TBranch        *b_el_GSF_E_TileGap3;   //!
TBranch        *b_el_GSF_E_TileExt0;   //!
TBranch        *b_el_GSF_E_TileExt1;   //!
TBranch        *b_el_GSF_E_TileExt2;   //!
TBranch        *b_el_GSF_E_FCAL0;   //!
TBranch        *b_el_GSF_E_FCAL1;   //!
TBranch        *b_el_GSF_E_FCAL2;   //!
TBranch        *b_el_GSF_cl_E;   //!
TBranch        *b_el_GSF_cl_pt;   //!
TBranch        *b_el_GSF_cl_eta;   //!
TBranch        *b_el_GSF_cl_phi;   //!
TBranch        *b_el_GSF_cl_etaCalo;   //!
TBranch        *b_el_GSF_cl_phiCalo;   //!
TBranch        *b_el_GSF_firstEdens;   //!
TBranch        *b_el_GSF_cellmaxfrac;   //!
TBranch        *b_el_GSF_longitudinal;   //!
TBranch        *b_el_GSF_secondlambda;   //!
TBranch        *b_el_GSF_lateral;   //!
TBranch        *b_el_GSF_secondR;   //!
TBranch        *b_el_GSF_centerlambda;   //!
TBranch        *b_el_GSF_rawcl_Es0;   //!
TBranch        *b_el_GSF_rawcl_etas0;   //!
TBranch        *b_el_GSF_rawcl_phis0;   //!
TBranch        *b_el_GSF_rawcl_Es1;   //!
TBranch        *b_el_GSF_rawcl_etas1;   //!
TBranch        *b_el_GSF_rawcl_phis1;   //!
TBranch        *b_el_GSF_rawcl_Es2;   //!
TBranch        *b_el_GSF_rawcl_etas2;   //!
TBranch        *b_el_GSF_rawcl_phis2;   //!
TBranch        *b_el_GSF_rawcl_Es3;   //!
TBranch        *b_el_GSF_rawcl_etas3;   //!
TBranch        *b_el_GSF_rawcl_phis3;   //!
TBranch        *b_el_GSF_rawcl_E;   //!
TBranch        *b_el_GSF_rawcl_pt;   //!
TBranch        *b_el_GSF_rawcl_eta;   //!
TBranch        *b_el_GSF_rawcl_phi;   //!
TBranch        *b_el_GSF_trackd0;   //!
TBranch        *b_el_GSF_trackz0;   //!
TBranch        *b_el_GSF_trackphi;   //!
TBranch        *b_el_GSF_tracktheta;   //!
TBranch        *b_el_GSF_trackqoverp;   //!
TBranch        *b_el_GSF_trackpt;   //!
TBranch        *b_el_GSF_tracketa;   //!
TBranch        *b_el_GSF_trackcov_d0;   //!
TBranch        *b_el_GSF_trackcov_z0;   //!
TBranch        *b_el_GSF_trackcov_phi;   //!
TBranch        *b_el_GSF_trackcov_theta;   //!
TBranch        *b_el_GSF_trackcov_qoverp;   //!
TBranch        *b_el_GSF_trackcov_d0_z0;   //!
TBranch        *b_el_GSF_trackcov_d0_phi;   //!
TBranch        *b_el_GSF_trackcov_d0_theta;   //!
TBranch        *b_el_GSF_trackcov_d0_qoverp;   //!
TBranch        *b_el_GSF_trackcov_z0_phi;   //!
TBranch        *b_el_GSF_trackcov_z0_theta;   //!
TBranch        *b_el_GSF_trackcov_z0_qoverp;   //!
TBranch        *b_el_GSF_trackcov_phi_theta;   //!
TBranch        *b_el_GSF_trackcov_phi_qoverp;   //!
TBranch        *b_el_GSF_trackcov_theta_qoverp;   //!
TBranch        *b_el_GSF_trackfitchi2;   //!
TBranch        *b_el_GSF_trackfitndof;   //!
TBranch        *b_el_GSF_nBLHits;   //!
TBranch        *b_el_GSF_nPixHits;   //!
TBranch        *b_el_GSF_nSCTHits;   //!
TBranch        *b_el_GSF_nTRTHits;   //!
TBranch        *b_el_GSF_nTRTHighTHits;   //!
TBranch        *b_el_GSF_nPixHoles;   //!
TBranch        *b_el_GSF_nSCTHoles;   //!
TBranch        *b_el_GSF_nTRTHoles;   //!
TBranch        *b_el_GSF_nBLSharedHits;   //!
TBranch        *b_el_GSF_nPixSharedHits;   //!
TBranch        *b_el_GSF_nSCTSharedHits;   //!
TBranch        *b_el_GSF_nBLayerOutliers;   //!
TBranch        *b_el_GSF_nPixelOutliers;   //!
TBranch        *b_el_GSF_nSCTOutliers;   //!
TBranch        *b_el_GSF_nTRTOutliers;   //!
TBranch        *b_el_GSF_nTRTHighTOutliers;   //!
TBranch        *b_el_GSF_nContribPixelLayers;   //!
TBranch        *b_el_GSF_nGangedPixels;   //!
TBranch        *b_el_GSF_nGangedFlaggedFakes;   //!
TBranch        *b_el_GSF_nPixelDeadSensors;   //!
TBranch        *b_el_GSF_nPixelSpoiltHits;   //!
TBranch        *b_el_GSF_nSCTDoubleHoles;   //!
TBranch        *b_el_GSF_nSCTDeadSensors;   //!
TBranch        *b_el_GSF_nSCTSpoiltHits;   //!
TBranch        *b_el_GSF_expectBLayerHit;   //!
TBranch        *b_el_GSF_nSiHits;   //!
TBranch        *b_el_GSF_TRTHighTHitsRatio;   //!
TBranch        *b_el_GSF_TRTHighTOutliersRatio;   //!
TBranch        *b_el_GSF_pixeldEdx;   //!
TBranch        *b_el_GSF_nGoodHitsPixeldEdx;   //!
TBranch        *b_el_GSF_massPixeldEdx;   //!
TBranch        *b_el_GSF_likelihoodsPixeldEdx;   //!
TBranch        *b_el_GSF_eProbabilityComb;   //!
TBranch        *b_el_GSF_eProbabilityHT;   //!
TBranch        *b_el_GSF_eProbabilityToT;   //!
TBranch        *b_el_GSF_eProbabilityBrem;   //!
TBranch        *b_el_GSF_vertweight;   //!
TBranch        *b_el_GSF_vertx;   //!
TBranch        *b_el_GSF_verty;   //!
TBranch        *b_el_GSF_vertz;   //!
TBranch        *b_el_GSF_trackd0beam;   //!
TBranch        *b_el_GSF_trackz0beam;   //!
TBranch        *b_el_GSF_tracksigd0beam;   //!
TBranch        *b_el_GSF_tracksigz0beam;   //!
TBranch        *b_el_GSF_trackd0pv;   //!
TBranch        *b_el_GSF_trackz0pv;   //!
TBranch        *b_el_GSF_tracksigd0pv;   //!
TBranch        *b_el_GSF_tracksigz0pv;   //!
TBranch        *b_el_GSF_trackIPEstimate_d0_biasedpvunbiased;   //!
TBranch        *b_el_GSF_trackIPEstimate_z0_biasedpvunbiased;   //!
TBranch        *b_el_GSF_trackIPEstimate_sigd0_biasedpvunbiased;   //!
TBranch        *b_el_GSF_trackIPEstimate_sigz0_biasedpvunbiased;   //!
TBranch        *b_el_GSF_trackIPEstimate_d0_unbiasedpvunbiased;   //!
TBranch        *b_el_GSF_trackIPEstimate_z0_unbiasedpvunbiased;   //!
TBranch        *b_el_GSF_trackIPEstimate_sigd0_unbiasedpvunbiased;   //!
TBranch        *b_el_GSF_trackIPEstimate_sigz0_unbiasedpvunbiased;   //!
TBranch        *b_el_GSF_trackd0pvunbiased;   //!
TBranch        *b_el_GSF_trackz0pvunbiased;   //!
TBranch        *b_el_GSF_tracksigd0pvunbiased;   //!
TBranch        *b_el_GSF_tracksigz0pvunbiased;   //!
TBranch        *b_el_GSF_hastrack;   //!
TBranch        *b_el_GSF_deltaEmax2;   //!
TBranch        *b_el_GSF_calibHitsShowerDepth;   //!
TBranch        *b_el_GSF_isIso;   //!
TBranch        *b_el_GSF_mvaptcone20;   //!
TBranch        *b_el_GSF_mvaptcone30;   //!
TBranch        *b_el_GSF_mvaptcone40;   //!
TBranch        *b_el_GSF_truth_bremSi;   //!
TBranch        *b_el_GSF_truth_bremLoc;   //!
TBranch        *b_el_GSF_truth_sumbrem;   //!
TBranch        *b_el_GSF_Etcone40_ED_corrected;   //!
TBranch        *b_el_GSF_Etcone40_corrected;   //!
TBranch        *b_el_GSF_jet_dr;   //!
TBranch        *b_el_GSF_jet_E;   //!
TBranch        *b_el_GSF_jet_pt;   //!
TBranch        *b_el_GSF_jet_m;   //!
TBranch        *b_el_GSF_jet_eta;   //!
TBranch        *b_el_GSF_jet_phi;   //!
TBranch        *b_el_GSF_jet_truth_dr;   //!
TBranch        *b_el_GSF_jet_truth_E;   //!
TBranch        *b_el_GSF_jet_truth_pt;   //!
TBranch        *b_el_GSF_jet_truth_m;   //!
TBranch        *b_el_GSF_jet_truth_eta;   //!
TBranch        *b_el_GSF_jet_truth_phi;   //!
TBranch        *b_el_GSF_jet_truth_matched;   //!
TBranch        *b_el_GSF_jet_matched;   //!
TBranch        *b_el_GSF_EF_dr;   //!
TBranch        *b_el_GSF_EF_index;   //!
TBranch        *b_el_GSF_L2_dr;   //!
TBranch        *b_el_GSF_L2_index;   //!
TBranch        *b_el_GSF_L1_dr;   //!
TBranch        *b_el_GSF_L1_index;   //!

/*2012*/
TBranch        *b_el_n;   //!
TBranch        *b_el_E;   //!
TBranch        *b_el_Et;   //!
TBranch        *b_el_pt;   //!
TBranch        *b_el_m;   //!
TBranch        *b_el_eta;   //!
TBranch        *b_el_phi;   //!
TBranch        *b_el_px;   //!
TBranch        *b_el_py;   //!
TBranch        *b_el_pz;   //!
TBranch        *b_el_charge;   //!
TBranch        *b_el_author;   //!
TBranch        *b_el_isEM;   //!
TBranch        *b_el_isEMLoose;   //!
TBranch        *b_el_isEMMedium;   //!
TBranch        *b_el_isEMTight;   //!
TBranch        *b_el_OQ;   //!
TBranch        *b_el_convFlag;   //!
TBranch        *b_el_isConv;   //!
TBranch        *b_el_nConv;   //!
TBranch        *b_el_nSingleTrackConv;   //!
TBranch        *b_el_nDoubleTrackConv;   //!
TBranch        *b_el_type;   //!
TBranch        *b_el_origin;   //!
TBranch        *b_el_typebkg;   //!
TBranch        *b_el_originbkg;   //!
TBranch        *b_el_truth_E;   //!
TBranch        *b_el_truth_pt;   //!
TBranch        *b_el_truth_eta;   //!
TBranch        *b_el_truth_phi;   //!
TBranch        *b_el_truth_type;   //!
TBranch        *b_el_truth_status;   //!
TBranch        *b_el_truth_barcode;   //!
TBranch        *b_el_truth_mothertype;   //!
TBranch        *b_el_truth_motherbarcode;   //!
TBranch        *b_el_truth_hasHardBrem;   //!
TBranch        *b_el_truth_index;   //!
TBranch        *b_el_truth_matched;   //!
TBranch        *b_el_mediumWithoutTrack;   //!
TBranch        *b_el_mediumIsoWithoutTrack;   //!
TBranch        *b_el_tightWithoutTrack;   //!
TBranch        *b_el_tightIsoWithoutTrack;   //!
TBranch        *b_el_loose;   //!
TBranch        *b_el_looseIso;   //!
TBranch        *b_el_medium;   //!
TBranch        *b_el_mediumIso;   //!
TBranch        *b_el_tight;   //!
TBranch        *b_el_tightIso;   //!
TBranch        *b_el_loosePP;   //!
TBranch        *b_el_loosePPIso;   //!
TBranch        *b_el_mediumPP;   //!
TBranch        *b_el_mediumPPIso;   //!
TBranch        *b_el_tightPP;   //!
TBranch        *b_el_tightPPIso;   //!
TBranch        *b_el_goodOQ;   //!
TBranch        *b_el_Ethad;   //!
TBranch        *b_el_Ethad1;   //!
TBranch        *b_el_f1;   //!
TBranch        *b_el_f1core;   //!
TBranch        *b_el_Emins1;   //!
TBranch        *b_el_fside;   //!
TBranch        *b_el_Emax2;   //!
TBranch        *b_el_ws3;   //!
TBranch        *b_el_wstot;   //!
TBranch        *b_el_emaxs1;   //!
TBranch        *b_el_deltaEs;   //!
TBranch        *b_el_E233;   //!
TBranch        *b_el_E237;   //!
TBranch        *b_el_E277;   //!
TBranch        *b_el_weta2;   //!
TBranch        *b_el_f3;   //!
TBranch        *b_el_f3core;   //!
TBranch        *b_el_rphiallcalo;   //!
TBranch        *b_el_Etcone45;   //!
TBranch        *b_el_Etcone15;   //!
TBranch        *b_el_Etcone20;   //!
TBranch        *b_el_Etcone25;   //!
TBranch        *b_el_Etcone30;   //!
TBranch        *b_el_Etcone35;   //!
TBranch        *b_el_Etcone40;   //!
TBranch        *b_el_ptcone20;   //!
TBranch        *b_el_ptcone30;   //!
TBranch        *b_el_ptcone40;   //!
TBranch        *b_el_nucone20;   //!
TBranch        *b_el_nucone30;   //!
TBranch        *b_el_nucone40;   //!
TBranch        *b_el_Etcone15_pt_corrected;   //!
TBranch        *b_el_Etcone20_pt_corrected;   //!
TBranch        *b_el_Etcone25_pt_corrected;   //!
TBranch        *b_el_Etcone30_pt_corrected;   //!
TBranch        *b_el_Etcone35_pt_corrected;   //!
TBranch        *b_el_Etcone40_pt_corrected;   //!
TBranch        *b_el_convanglematch;   //!
TBranch        *b_el_convtrackmatch;   //!
TBranch        *b_el_hasconv;   //!
TBranch        *b_el_convvtxx;   //!
TBranch        *b_el_convvtxy;   //!
TBranch        *b_el_convvtxz;   //!
TBranch        *b_el_Rconv;   //!
TBranch        *b_el_zconv;   //!
TBranch        *b_el_convvtxchi2;   //!
TBranch        *b_el_pt1conv;   //!
TBranch        *b_el_convtrk1nBLHits;   //!
TBranch        *b_el_convtrk1nPixHits;   //!
TBranch        *b_el_convtrk1nSCTHits;   //!
TBranch        *b_el_convtrk1nTRTHits;   //!
TBranch        *b_el_pt2conv;   //!
TBranch        *b_el_convtrk2nBLHits;   //!
TBranch        *b_el_convtrk2nPixHits;   //!
TBranch        *b_el_convtrk2nSCTHits;   //!
TBranch        *b_el_convtrk2nTRTHits;   //!
TBranch        *b_el_ptconv;   //!
TBranch        *b_el_pzconv;   //!
TBranch        *b_el_pos7;   //!
TBranch        *b_el_etacorrmag;   //!
TBranch        *b_el_deltaeta1;   //!
TBranch        *b_el_deltaeta2;   //!
TBranch        *b_el_deltaphi2;   //!
TBranch        *b_el_deltaphiRescaled;   //!
TBranch        *b_el_deltaPhiFromLast;   //!
TBranch        *b_el_deltaPhiRot;   //!
TBranch        *b_el_expectHitInBLayer;   //!
TBranch        *b_el_trackd0_physics;   //!
TBranch        *b_el_etaSampling1;   //!
TBranch        *b_el_reta;   //!
TBranch        *b_el_rphi;   //!
TBranch        *b_el_topoEtcone20;   //!
TBranch        *b_el_topoEtcone30;   //!
TBranch        *b_el_topoEtcone40;   //!
TBranch        *b_el_EtringnoisedR03sig2;   //!
TBranch        *b_el_EtringnoisedR03sig3;   //!
TBranch        *b_el_EtringnoisedR03sig4;   //!
TBranch        *b_el_isolationlikelihoodjets;   //!
TBranch        *b_el_isolationlikelihoodhqelectrons;   //!
TBranch        *b_el_electronweight;   //!
TBranch        *b_el_electronbgweight;   //!
TBranch        *b_el_softeweight;   //!
TBranch        *b_el_softebgweight;   //!
TBranch        *b_el_neuralnet;   //!
TBranch        *b_el_Hmatrix;   //!
TBranch        *b_el_Hmatrix5;   //!
TBranch        *b_el_adaboost;   //!
TBranch        *b_el_softeneuralnet;   //!
TBranch        *b_el_ringernn;   //!
TBranch        *b_el_zvertex;   //!
TBranch        *b_el_errz;   //!
TBranch        *b_el_etap;   //!
TBranch        *b_el_depth;   //!
TBranch        *b_el_refittedTrack_n;   //!
TBranch        *b_el_refittedTrack_author;   //!
TBranch        *b_el_refittedTrack_chi2;   //!
TBranch        *b_el_refittedTrack_hasBrem;   //!
TBranch        *b_el_refittedTrack_bremRadius;   //!
TBranch        *b_el_refittedTrack_bremZ;   //!
TBranch        *b_el_refittedTrack_bremRadiusErr;   //!
TBranch        *b_el_refittedTrack_bremZErr;   //!
TBranch        *b_el_refittedTrack_bremFitStatus;   //!
TBranch        *b_el_refittedTrack_qoverp;   //!
TBranch        *b_el_refittedTrack_d0;   //!
TBranch        *b_el_refittedTrack_z0;   //!
TBranch        *b_el_refittedTrack_theta;   //!
TBranch        *b_el_refittedTrack_phi;   //!
TBranch        *b_el_refittedTrack_LMqoverp;   //!
TBranch        *b_el_refittedTrack_covd0;   //!
TBranch        *b_el_refittedTrack_covz0;   //!
TBranch        *b_el_refittedTrack_covphi;   //!
TBranch        *b_el_refittedTrack_covtheta;   //!
TBranch        *b_el_refittedTrack_covqoverp;   //!
TBranch        *b_el_refittedTrack_covd0z0;   //!
TBranch        *b_el_refittedTrack_covz0phi;   //!
TBranch        *b_el_refittedTrack_covz0theta;   //!
TBranch        *b_el_refittedTrack_covz0qoverp;   //!
TBranch        *b_el_refittedTrack_covd0phi;   //!
TBranch        *b_el_refittedTrack_covd0theta;   //!
TBranch        *b_el_refittedTrack_covd0qoverp;   //!
TBranch        *b_el_refittedTrack_covphitheta;   //!
TBranch        *b_el_refittedTrack_covphiqoverp;   //!
TBranch        *b_el_refittedTrack_covthetaqoverp;   //!
TBranch        *b_el_Es0;   //!
TBranch        *b_el_etas0;   //!
TBranch        *b_el_phis0;   //!
TBranch        *b_el_Es1;   //!
TBranch        *b_el_etas1;   //!
TBranch        *b_el_phis1;   //!
TBranch        *b_el_Es2;   //!
TBranch        *b_el_etas2;   //!
TBranch        *b_el_phis2;   //!
TBranch        *b_el_Es3;   //!
TBranch        *b_el_etas3;   //!
TBranch        *b_el_phis3;   //!
TBranch        *b_el_E_PreSamplerB;   //!
TBranch        *b_el_E_EMB1;   //!
TBranch        *b_el_E_EMB2;   //!
TBranch        *b_el_E_EMB3;   //!
TBranch        *b_el_E_PreSamplerE;   //!
TBranch        *b_el_E_EME1;   //!
TBranch        *b_el_E_EME2;   //!
TBranch        *b_el_E_EME3;   //!
TBranch        *b_el_E_HEC0;   //!
TBranch        *b_el_E_HEC1;   //!
TBranch        *b_el_E_HEC2;   //!
TBranch        *b_el_E_HEC3;   //!
TBranch        *b_el_E_TileBar0;   //!
TBranch        *b_el_E_TileBar1;   //!
TBranch        *b_el_E_TileBar2;   //!
TBranch        *b_el_E_TileGap1;   //!
TBranch        *b_el_E_TileGap2;   //!
TBranch        *b_el_E_TileGap3;   //!
TBranch        *b_el_E_TileExt0;   //!
TBranch        *b_el_E_TileExt1;   //!
TBranch        *b_el_E_TileExt2;   //!
TBranch        *b_el_E_FCAL0;   //!
TBranch        *b_el_E_FCAL1;   //!
TBranch        *b_el_E_FCAL2;   //!
TBranch        *b_el_cl_E;   //!
TBranch        *b_el_cl_pt;   //!
TBranch        *b_el_cl_eta;   //!
TBranch        *b_el_cl_phi;   //!
TBranch        *b_el_cl_etaCalo;   //!
TBranch        *b_el_cl_phiCalo;   //!
TBranch        *b_el_firstEdens;   //!
TBranch        *b_el_cellmaxfrac;   //!
TBranch        *b_el_longitudinal;   //!
TBranch        *b_el_secondlambda;   //!
TBranch        *b_el_lateral;   //!
TBranch        *b_el_secondR;   //!
TBranch        *b_el_centerlambda;   //!
TBranch        *b_el_rawcl_Es0;   //!
TBranch        *b_el_rawcl_etas0;   //!
TBranch        *b_el_rawcl_phis0;   //!
TBranch        *b_el_rawcl_Es1;   //!
TBranch        *b_el_rawcl_etas1;   //!
TBranch        *b_el_rawcl_phis1;   //!
TBranch        *b_el_rawcl_Es2;   //!
TBranch        *b_el_rawcl_etas2;   //!
TBranch        *b_el_rawcl_phis2;   //!
TBranch        *b_el_rawcl_Es3;   //!
TBranch        *b_el_rawcl_etas3;   //!
TBranch        *b_el_rawcl_phis3;   //!
TBranch        *b_el_rawcl_E;   //!
TBranch        *b_el_rawcl_pt;   //!
TBranch        *b_el_rawcl_eta;   //!
TBranch        *b_el_rawcl_phi;   //!
TBranch        *b_el_rings_E;   //!
TBranch        *b_el_trackd0;   //!
TBranch        *b_el_trackz0;   //!
TBranch        *b_el_trackphi;   //!
TBranch        *b_el_tracktheta;   //!
TBranch        *b_el_trackqoverp;   //!
TBranch        *b_el_trackpt;   //!
TBranch        *b_el_tracketa;   //!
TBranch        *b_el_trackcov_d0;   //!
TBranch        *b_el_trackcov_z0;   //!
TBranch        *b_el_trackcov_phi;   //!
TBranch        *b_el_trackcov_theta;   //!
TBranch        *b_el_trackcov_qoverp;   //!
TBranch        *b_el_trackcov_d0_z0;   //!
TBranch        *b_el_trackcov_d0_phi;   //!
TBranch        *b_el_trackcov_d0_theta;   //!
TBranch        *b_el_trackcov_d0_qoverp;   //!
TBranch        *b_el_trackcov_z0_phi;   //!
TBranch        *b_el_trackcov_z0_theta;   //!
TBranch        *b_el_trackcov_z0_qoverp;   //!
TBranch        *b_el_trackcov_phi_theta;   //!
TBranch        *b_el_trackcov_phi_qoverp;   //!
TBranch        *b_el_trackcov_theta_qoverp;   //!
TBranch        *b_el_trackfitchi2;   //!
TBranch        *b_el_trackfitndof;   //!
TBranch        *b_el_nBLHits;   //!
TBranch        *b_el_nPixHits;   //!
TBranch        *b_el_nSCTHits;   //!
TBranch        *b_el_nTRTHits;   //!
TBranch        *b_el_nTRTHighTHits;   //!
TBranch        *b_el_nPixHoles;   //!
TBranch        *b_el_nSCTHoles;   //!
TBranch        *b_el_nTRTHoles;   //!
TBranch        *b_el_nPixelDeadSensors;   //!
TBranch        *b_el_nSCTDeadSensors;   //!
TBranch        *b_el_nBLSharedHits;   //!
TBranch        *b_el_nPixSharedHits;   //!
TBranch        *b_el_nSCTSharedHits;   //!
TBranch        *b_el_nBLayerSplitHits;   //!
TBranch        *b_el_nPixSplitHits;   //!
TBranch        *b_el_nBLayerOutliers;   //!
TBranch        *b_el_nPixelOutliers;   //!
TBranch        *b_el_nSCTOutliers;   //!
TBranch        *b_el_nTRTOutliers;   //!
TBranch        *b_el_nTRTHighTOutliers;   //!
TBranch        *b_el_nContribPixelLayers;   //!
TBranch        *b_el_nGangedPixels;   //!
TBranch        *b_el_nGangedFlaggedFakes;   //!
TBranch        *b_el_nPixelSpoiltHits;   //!
TBranch        *b_el_nSCTDoubleHoles;   //!
TBranch        *b_el_nSCTSpoiltHits;   //!
TBranch        *b_el_expectBLayerHit;   //!
TBranch        *b_el_nSiHits;   //!
TBranch        *b_el_TRTHighTHitsRatio;   //!
TBranch        *b_el_TRTHighTOutliersRatio;   //!
TBranch        *b_el_pixeldEdx;   //!
TBranch        *b_el_nGoodHitsPixeldEdx;   //!
TBranch        *b_el_massPixeldEdx;   //!
TBranch        *b_el_likelihoodsPixeldEdx;   //!
TBranch        *b_el_eProbabilityComb;   //!
TBranch        *b_el_eProbabilityHT;   //!
TBranch        *b_el_eProbabilityToT;   //!
TBranch        *b_el_eProbabilityBrem;   //!
TBranch        *b_el_vertweight;   //!
TBranch        *b_el_vertx;   //!
TBranch        *b_el_verty;   //!
TBranch        *b_el_vertz;   //!
TBranch        *b_el_trackd0beam;   //!
TBranch        *b_el_trackz0beam;   //!
TBranch        *b_el_tracksigd0beam;   //!
TBranch        *b_el_tracksigz0beam;   //!
TBranch        *b_el_trackd0pv;   //!
TBranch        *b_el_trackz0pv;   //!
TBranch        *b_el_tracksigd0pv;   //!
TBranch        *b_el_tracksigz0pv;   //!
TBranch        *b_el_trackIPEstimate_d0_biasedpvunbiased;   //!
TBranch        *b_el_trackIPEstimate_z0_biasedpvunbiased;   //!
TBranch        *b_el_trackIPEstimate_sigd0_biasedpvunbiased;   //!
TBranch        *b_el_trackIPEstimate_sigz0_biasedpvunbiased;   //!
TBranch        *b_el_trackIPEstimate_d0_unbiasedpvunbiased;   //!
TBranch        *b_el_trackIPEstimate_z0_unbiasedpvunbiased;   //!
TBranch        *b_el_trackIPEstimate_sigd0_unbiasedpvunbiased;   //!
TBranch        *b_el_trackIPEstimate_sigz0_unbiasedpvunbiased;   //!
TBranch        *b_el_trackd0pvunbiased;   //!
TBranch        *b_el_trackz0pvunbiased;   //!
TBranch        *b_el_tracksigd0pvunbiased;   //!
TBranch        *b_el_tracksigz0pvunbiased;   //!
TBranch        *b_el_Unrefittedtrack_d0;   //!
TBranch        *b_el_Unrefittedtrack_z0;   //!
TBranch        *b_el_Unrefittedtrack_phi;   //!
TBranch        *b_el_Unrefittedtrack_theta;   //!
TBranch        *b_el_Unrefittedtrack_qoverp;   //!
TBranch        *b_el_Unrefittedtrack_pt;   //!
TBranch        *b_el_Unrefittedtrack_eta;   //!
TBranch        *b_el_hastrack;   //!
TBranch        *b_el_deltaEmax2;   //!
TBranch        *b_el_calibHitsShowerDepth;   //!
TBranch        *b_el_isIso;   //!
TBranch        *b_el_mvaptcone20;   //!
TBranch        *b_el_mvaptcone30;   //!
TBranch        *b_el_mvaptcone40;   //!
TBranch        *b_el_CaloPointing_eta;   //!
TBranch        *b_el_CaloPointing_sigma_eta;   //!
TBranch        *b_el_CaloPointing_zvertex;   //!
TBranch        *b_el_CaloPointing_sigma_zvertex;   //!
TBranch        *b_el_HPV_eta;   //!
TBranch        *b_el_HPV_sigma_eta;   //!
TBranch        *b_el_HPV_zvertex;   //!
TBranch        *b_el_HPV_sigma_zvertex;   //!
TBranch        *b_el_truth_bremSi;   //!
TBranch        *b_el_truth_bremLoc;   //!
TBranch        *b_el_truth_sumbrem;   //!
TBranch        *b_el_topoEtcone60;   //!
TBranch        *b_el_ES0_real;   //!
TBranch        *b_el_ES1_real;   //!
TBranch        *b_el_ES2_real;   //!
TBranch        *b_el_ES3_real;   //!
TBranch        *b_el_EcellS0;   //!
TBranch        *b_el_etacellS0;   //!
TBranch        *b_el_Etcone40_ED_corrected;   //!
TBranch        *b_el_Etcone40_corrected;   //!
TBranch        *b_el_topoEtcone20_corrected;   //!
TBranch        *b_el_topoEtcone30_corrected;   //!
TBranch        *b_el_topoEtcone40_corrected;   //!
TBranch        *b_el_ED_median;   //!
TBranch        *b_el_ED_sigma;   //!
TBranch        *b_el_ED_Njets;   //!
TBranch        *b_el_jet_dr;   //!
TBranch        *b_el_jet_E;   //!
TBranch        *b_el_jet_pt;   //!
TBranch        *b_el_jet_m;   //!
TBranch        *b_el_jet_eta;   //!
TBranch        *b_el_jet_phi;   //!
TBranch        *b_el_jet_truth_dr;   //!
TBranch        *b_el_jet_truth_E;   //!
TBranch        *b_el_jet_truth_pt;   //!
TBranch        *b_el_jet_truth_m;   //!
TBranch        *b_el_jet_truth_eta;   //!
TBranch        *b_el_jet_truth_phi;   //!
TBranch        *b_el_jet_truth_matched;   //!
TBranch        *b_el_jet_matched;   //!
TBranch        *b_el_EF_dr;   //!
TBranch        *b_el_EF_index;   //!

TBranch        *b_trig_RoI_EF_e_egammaContainer_egamma_Electrons;   //!
TBranch        *b_trig_RoI_EF_e_egammaContainer_egamma_ElectronsStatus;   //!
TBranch        *b_trig_EF_el_n;   //!
TBranch        *b_trig_EF_el_eta;   //!
TBranch        *b_trig_EF_el_phi;   //!
TBranch        *b_trig_EF_el_Et;   //!

#endif
