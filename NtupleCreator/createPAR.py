#!/usr/bin/python
import os
import sys

PreparePROOF = True

#Create Proof package
print "\n"
if PreparePROOF:
    print "Preparing Proof package ... "

    os.system("mv PROOF_INF PROOF-INF")

    os.chdir("../")
    
    retval = os.getcwd()
    print "Directory changed successfully %s" % retval
    
    os.system("tar -czf H4lNtupleCreator.par H4lNtupleCreator/")
    os.system("mv H4lNtupleCreator.par H4lNtupleCreator/.")
    print "H4lNtupleCreator.par file created!"
    
    print "Proof package prepared"
    os.chdir("H4lNtupleCreator/")
    retval = os.getcwd()
    print "Directory changed successfully %s" % retval

print "createPAR:: Everything Done!!"
