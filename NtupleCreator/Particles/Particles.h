#ifndef Particles_h
#define Particles_h

/* ROOT headers */
#include <TH2.h>
#include <TFile.h>
#include <TRandom.h>
#include <TMath.h>

/* basic C++ headers */
#include <vector>
#include <string>
#include <iostream>

/* Utils */
//#include "egammaAnalysisUtils/EnergyRescalerUpgrade.h"
#include "ElectronPhotonFourMomentumCorrection/egammaEnergyCorrectionTool.h"
#include "ElectronEfficiencyCorrection/TElectronEfficiencyCorrectionTool.h"
#include "MuonMomentumCorrections/SmearingClass.h"
#include "MuonMomentumCorrections/MuonResolutionAndMomentumScaleFactors.h"
#include "MuonEfficiencyCorrections/AnalysisMuonEfficiencyScaleFactors.h"
#include "MuonEfficiencyCorrections/AnalysisMuonConfigurableScaleFactors.h"
#include "PileupReweighting/TPileupReweighting.h"
//#include "egammaAnalysisUtils/H4l2011Defs.h"
//#include "TPython.h"
#include "ElectronPhotonSelectorTools/TElectronIsEMSelector.h"
#include "egammaEvent/EMAmbiguityToolDefs.h"
#include "ApplyJetCalibration/ApplyJetCalibration.h"

class Particles{
 public:
  /*Constructors*/
  Particles();
  
  /*Destructots*/
  virtual ~Particles();

  /* General Configuration */
  void GeneralConf(bool,int,int,bool,bool,bool,std::string);

  /* General Tools Configuration */
  void ConfigMuonTools(MuonSmear::SmearingClass*,
		       Analysis::AnalysisMuonConfigurableScaleFactors*,
		       Analysis::AnalysisMuonConfigurableScaleFactors*,
		       Analysis::AnalysisMuonConfigurableScaleFactors*,
		       Analysis::MuonResolutionAndMomentumScaleFactors*);
  void ConfigElecTools(//egRescaler::EnergyRescalerUpgrade*,
		       AtlasRoot::egammaEnergyCorrectionTool*,
		       Root::TElectronEfficiencyCorrectionTool*,
		       Root::TElectronEfficiencyCorrectionTool*,
		       Root::TPileupReweighting*,
		       Root::TElectronIsEMSelector*,
		       Root::TElectronLikelihoodTool*);
  void ConfigJetTools(JetAnalysisCalib::JetCalibrationTool*);

  /* General Cleaning */
  void Clean();

  /* Configure Muons */
  void ConfigMuons(double stacopt=5.,double calopt=15.,
		   double d0=3.,double z0=10.);

  /* Configure Electrons */
  void ConfigElectrons(double elecEt=7.,double z0=10.);

  /* Configure Taus */
  void ConfigTaus(double tauPt=25.);

  /* COnfigure Jets */
  void ConfigJets(double jetpt1=25.,double jetpt2=30.);
  
  /* Reset MuonQualityFlags */
  void ResetMuonQualityFlags();

  /* Reset ElectronQualityFlags */
  void ResetElectronQualityFlags();

  /* Reset JetQualityFlags */
  void ResetJetQualityFlags();

  /* Reset TauQualityFlags */
  void ResetTauQualityFlags();
  
  /* Check Staco Muons */
  void CheckingStacoMuons(bool,bool);
  
  /* Check Calo Muons */
  void CheckingCaloMuons(bool,bool);

  /* Check ST-SA Muons Overlap */
  void CheckSTSAmuonOverlap();

  /* Fill CB, ST and SA muons info */
  void FillMuonsInfo(bool,bool);
  
  /* Merge Vectors*/
  void MergeGeneralVec_CBSTSA();
  void MergeGeneralVec_Calo();

  /* Clear Vectors*/
  void ClearStacoMuonVec();
  void ClearCBSTSAMuonVec();
  void ClearCaloMuonVec();
  void ClearMuonVec();
  void ClearElecVec();
  void ClearJetVec();
  void ClearTauVec();
  void ClearParticleVec();

  /* Check Electrons */
  void CheckingElectrons(bool,bool,int);
  void CheckingElectrons2011(bool,bool);
  void CheckingElectrons2012(bool,bool,int);

  /* Check Jets */
  void CheckingJets(bool,bool,double,int);

  /* Apply Hot Tile Cell Veto for Jets */
  bool ApplyHotTileCellVeto(int);

  /* Apply Jet Calibration */
  TLorentzVector ApplyJetCalibration(int,double,int);

  /* Check Taus */
  void CheckingTaus(bool,bool);
  
  /* Check Electrons/Muons Overlap */
  bool CheckemuCBSTOverlap2011(UInt_t);
  bool CheckemuCBSTOverlap2012(UInt_t);
  void CheckemuCaloOverlap2011(UInt_t);
  void CheckemuCaloOverlap2012(UInt_t);

  /* Check Taus/Muons-Electrons Overlap */
  bool ChecktmuOverlap(UInt_t);
  bool ChecktelOverlap(UInt_t);
    
  /* Check Jet/Electrons Overlap */
  bool CheckJetElOverlap(UInt_t,double,int);

  /* Check Photons */
  void CorrectPhotons();
  
  /* get muons counter variables*/
  int getStacoAthCBTag();
  int getStacoAthSA();
  int getStacoPtCBTag();
  int getStacoPtSA();
  int getStacoBl();
  int getStacoPxl();
  int getStacoSCT();
  int getStacoHol();
  int getStacoTRT();
  int getStacoD0Z0();
  int getStacoMS();
  int getCaloAth();
  int getCaloPt();
  int getCaloBl();
  int getCaloPxl();
  int getCaloSCT();
  int getCaloHol();
  int getCaloTRT();
  int getCaloD0Z0();
  int getOverlapSTSA();
  int getOverlapCalo();
  int getNmuCBsel();
  int getNmuSAsel();
  int getNmuCALOsel();
  int getNmuCBSAsel();
  int getNmuCBSACALOsel();
  int getNmuSel();
  
  /* get particle vectors */
  std::vector<int>    getLep_Index();
  std::vector<int>    getLep_Id();
  std::vector<int>    getLep_Author();
  std::vector<int>    getLep_charge();
  std::vector<double> getLep_d0_unbias();
  std::vector<double> getLep_scf();
  std::vector<double> getLep_resoscf();
  std::vector<double> getLep_z0_unbias();
  std::vector<double> getLep_E();
  std::vector<double> getLep_pt();
  std::vector<double> getLep_eta();
  std::vector<double> getLep_phi();
  std::vector<double> getLep_Ereso();
  std::vector<int>    getLep_SAgood();
  std::vector<int>    getLep_CALOgood();
  std::vector<int>    getLep_numTrack();
  
  /* get muon vectors */
  std::vector<int>    getMu_Index();
  std::vector<int>    getMu_Id();
  std::vector<int>    getMu_Author();
  std::vector<int>    getMu_charge();
  std::vector<double> getMu_d0_unbias();
  std::vector<double> getMu_scf();
  std::vector<double> getMu_resoscf();
  std::vector<double> getMu_z0_unbias();
  std::vector<double> getMu_pt();
  std::vector<double> getMu_eta();
  std::vector<double> getMu_phi();
  std::vector<int>    getMu_isSAmu();
  std::vector<int>    getMu_isSTmu();
  std::vector<int>    getMu_BLYgood();
  std::vector<int>    getMu_PXLgood();
  std::vector<int>    getMu_SCTgood();
  std::vector<int>    getMu_HOLgood();
  std::vector<int>    getMu_TRTgood();
  std::vector<int>    getMu_D0good();
  std::vector<int>    getMu_Z0good();
  std::vector<int>    getMu_MCPgood();
  std::vector<int>    getMu_MSgood();
  //std::vector<int>    getMu_OurMSgood();
  std::vector<int>    getMu_CBTAGgood();
  std::vector<int>    getMu_SAgood();
  std::vector<int>    getMu_CALOgood();
  std::vector<int>    getMu_n_exblh();
  std::vector<int>    getMu_n_blh();
  std::vector<int>    getMu_n_pxlh();
  std::vector<int>    getMu_n_pxlds();
  std::vector<int>    getMu_n_pxlhol();
  std::vector<int>    getMu_n_scth();
  std::vector<int>    getMu_n_sctds();
  std::vector<int>    getMu_n_scthol();
  std::vector<int>    getMu_n_hol();
  std::vector<int>    getMu_n_trth();
  std::vector<int>    getMu_n_trto();
  std::vector<int>    getMu_n_trtho();
  std::vector<int>    getMu_n_mdth();
  std::vector<int>    getMu_n_mdtBIh();
  std::vector<int>    getMu_n_mdtBMh();
  std::vector<int>    getMu_n_mdtBOh();
  std::vector<int>    getMu_n_mdtBEEh();
  std::vector<int>    getMu_n_mdtBIS78h();
  std::vector<int>    getMu_n_mdtEIh();
  std::vector<int>    getMu_n_mdtEMh();
  std::vector<int>    getMu_n_mdtEOh();
  std::vector<int>    getMu_n_mdtEEh();
  std::vector<int>    getMu_n_cscEtah();
  std::vector<int>    getMu_n_cscPhih();
  std::vector<int>    getMu_n_rpcEtah();
  std::vector<int>    getMu_n_rpcPhih();
  std::vector<int>    getMu_n_tgcEtah();
  std::vector<int>    getMu_n_tgcPhih();
  std::vector<double> getMu_d0();
  std::vector<double> getMu_errd0();
  std::vector<double> getMu_sigd0_unbias();
  std::vector<double> getMu_z0();
  std::vector<double> getMu_p();
  std::vector<double> getMu_px();
  std::vector<double> getMu_py();
  std::vector<double> getMu_pz();
  std::vector<double> getMu_pid();
  std::vector<double> getMu_E();
  std::vector<double> getMu_theta();           
  std::vector<double> getMu_theta_id();
  std::vector<double> getMu_eta_id();
  std::vector<double> getMu_phi_id();
  std::vector<double> getMu_isocalo20();
  std::vector<double> getMu_isocalo30();
  std::vector<double> getMu_isocalo40();
  std::vector<double> getMu_isotrk20();
  std::vector<double> getMu_isotrk30();
  std::vector<double> getMu_isotrk40();
  std::vector<double> getMu_pt_truth();
  std::vector<double> getMu_eta_truth();
  std::vector<double> getMu_phi_truth();
  std::vector<double> getMu_E_truth();
  std::vector<int>    getMu_mother_truth();
  std::vector<int>    getMu_motherbcode_truth();
  std::vector<double> getMu_cov_d0();
  std::vector<double> getMu_cov_z0();
  std::vector<double> getMu_cov_phi();
  std::vector<double> getMu_cov_theta();
  std::vector<double> getMu_cov_qoverp();
  std::vector<double> getMu_cov_d0_z0();
  std::vector<double> getMu_cov_d0_phi();
  std::vector<double> getMu_cov_d0_theta();
  std::vector<double> getMu_cov_d0_qoverp();
  std::vector<double> getMu_cov_z0_phi();
  std::vector<double> getMu_cov_z0_theta();
  std::vector<double> getMu_cov_z0_qoverp();
  std::vector<double> getMu_cov_phi_theta();
  std::vector<double> getMu_cov_phi_qoverp();
  std::vector<double> getMu_cov_theta_qoverp();

  /* get electron counter variable */
  int getElAuthor();
  int getElLoosePP();
  int getElEta();
  int getElEt();
  int getElOQ();
  int getElZ0();
  int getElOverlap();
  int getElOverlapCl();
  int getElMuOverlap();
  int getNelecSel();

  /* get electron vectors */
  std::vector<int>    getEl_Index();
  std::vector<int>    getEl_Id();
  std::vector<int>    getEl_Author();
  std::vector<int>    getEl_charge();
  std::vector<double> getEl_d0_unbias();
  std::vector<double> getEl_scf();
  std::vector<double> getEl_z0_unbias();
  std::vector<double> getEl_E();
  std::vector<double> getEl_pt();
  std::vector<double> getEl_eta();
  std::vector<double> getEl_phi();
  std::vector<double> getEl_Ereso();
  std::vector<double> getEl_d0();
  std::vector<double> getEl_sigd0_unbias();
  std::vector<double> getEl_z0();
  std::vector<double> getEl_Eclraw(); //-> 2012?
  std::vector<double> getEl_p();
  std::vector<double> getEl_px();
  std::vector<double> getEl_py();
  std::vector<double> getEl_pz();
  std::vector<double> getEl_pid();
  std::vector<double> getEl_trkpt();
  std::vector<double> getEl_Et();
  std::vector<double> getEl_theta();
  std::vector<double> getEl_etaS2();
  std::vector<double> getEl_etacl();
  std::vector<double> getEl_etap(); //->2012?
  std::vector<double> getEl_phiS2(); //->2012?
  std::vector<double> getEl_phicl();
  std::vector<double> getEl_isocalo20();
  std::vector<double> getEl_isocalo30();
  std::vector<double> getEl_isocalo40();
  std::vector<double> getEl_topoisocalo20(); //->2012?
  std::vector<double> getEl_topoisocalo30(); //->2012?
  std::vector<double> getEl_topoisocalo40(); //->2012?
  std::vector<double> getEl_isotrk20();
  std::vector<double> getEl_isotrk30();
  std::vector<double> getEl_isotrk40();
  std::vector<double> getEl_EDmedian(); //->2012?
  std::vector<double> getEl_pt_truth();
  std::vector<double> getEl_eta_truth();
  std::vector<double> getEl_phi_truth();
  std::vector<double> getEl_E_truth();
  std::vector<int>    getEl_mother_truth();
  std::vector<int>    getEl_motherbcode_truth();
  std::vector<double> getEl_cov_d0();
  std::vector<double> getEl_cov_z0();
  std::vector<double> getEl_cov_phi();
  std::vector<double> getEl_cov_theta();
  std::vector<double> getEl_cov_d0_z0();
  std::vector<double> getEl_cov_d0_phi();
  std::vector<double> getEl_cov_d0_theta();
  std::vector<double> getEl_cov_z0_phi();
  std::vector<double> getEl_cov_z0_theta();
  std::vector<double> getEl_cov_phi_theta();

  /* get jet counter variables */
  int getJetPt();
  int getJetEta();
  int getJetPU();
  int getJetClean();
  int getJetElOverlap();
  int getNjetSel();
  int getNjet30();

  /* get jet vectors */
  std::vector<int>    getJet_Index();
  std::vector<double> getJet_pt();
  std::vector<double> getJet_eta();
  std::vector<double> getJet_phi();
  std::vector<double> getJet_E();
  std::vector<double> getJet_m();
  std::vector<double> getJet_jvtx();
  
  /* get tau counter variables */
  int getTauPt();
  int getTauEta();
  int getTauNTrk();
  int getTauCharge();
  int getTauJetBDT();
  int getTauElVeto();
  int getTauMuVeto();
  int getTauTauOverlap();
  int getTauMuOverlap();
  int getTauElOverlap();
  int getNtauSel();
  
  /* get tau vectors */
  std::vector<int>    getTau_Index();
  std::vector<int>    getTau_Id();
  std::vector<int>    getTau_Author();
  std::vector<double> getTau_Et(); 
  std::vector<double> getTau_pt(); 
  std::vector<double> getTau_m(); 
  std::vector<double> getTau_eta(); 
  std::vector<double> getTau_phi(); 
  std::vector<int>    getTau_charge(); 
  std::vector<double> getTau_likelihood(); 
  std::vector<double> getTau_SafeLikelihood(); 
  std::vector<double> getTau_electronVetoLoose();
  std::vector<double> getTau_electronVetoMedium();
  std::vector<double> getTau_electronVetoTight(); 
  std::vector<double> getTau_muonVeto(); 
  std::vector<double> getTau_tauLlhLoose(); 
  std::vector<double> getTau_tauLlhMedium(); 
  std::vector<double> getTau_tauLlhTight(); 
  std::vector<double> getTau_JetBDTSigLoose(); 
  std::vector<double> getTau_JetBDTSigMedium();
  std::vector<double> getTau_JetBDTSigTight();
  std::vector<double> getTau_EleBDTLoose(); 
  std::vector<double> getTau_EleBDTMedium();
  std::vector<double> getTau_EleBDTTight(); 
  std::vector<int>    getTau_numTrack();
  std::vector<double> getTau_BDTJetScore();
  std::vector<double> getTau_BDTEleScore();
  
  /* Print Information */
  void Print(std::string);
  void PrintOut(int);
  void PrintOut(std::string);
  void PrintOut(float);
  void PrintOut(double);
  void PrintEndl();

  /*Electron LoosePP*/
  bool isLoosePlusPlusH4l(double,double,double,double,double,
			  double,double,double,double,double,
			  int,int,int,int,bool,bool);
  
  bool passRHad_looseH4l(double,double,unsigned int,unsigned int);
  
  bool passReta_looseH4l(double,unsigned int,unsigned int);
  
  bool passW2_looseH4l(double,unsigned int,unsigned int);
  
  bool passWstot_looseH4l(double,unsigned int,unsigned int);
  
  bool passEratio_looseH4l(double,unsigned int,unsigned int);
  
  bool passDeltaEta_looseH4l(double);
  
  unsigned int getEtaBinH4l(double);
  
  unsigned int getEtBinH4l(double);

  /* Compute DeltaR */
  double DeltaR(double,double,double,double);

  /* D0 Smearing */
  double D0Smearing(double,int,int,double,double);

  /* Z0 Smearing */
  double Z0Smearing(double,int,int,double,double);
  
 protected:
  double m_GeV;
  int m_year;
  int m_type;
  std::string m_DirPrefix;
  bool m_debug;
  bool m_UseCorr;
  bool m_UseW;
  bool m_MinParInfo;
  double m_pi;
  double m_Melec;
  double m_Mmuon;
  int m_IdMuon;
  int m_IdElectron;
  int m_IdTau;
  /* muons counter variables*/
  int StacoAthCBTag;
  int StacoAthSA;
  int StacoPtCBTag;
  int StacoPtSA;
  int StacoBl;
  int StacoPxl;
  int StacoSCT;
  int StacoHol;
  int StacoTRT;
  int StacoD0Z0;
  int StacoMS;
  int CaloAth;
  int CaloPt;
  int CaloBl;
  int CaloPxl;
  int CaloSCT;
  int CaloHol;
  int CaloTRT;
  int CaloD0Z0;
  int OverlapSTSA;
  int OverlapCalo;
  int m_nmuCBsel;
  int m_nmuSAsel;
  int m_nmuCALOsel;
  int m_nmuCBSAsel;
  int m_nmusel;
  /* electrons counter variables */
  int ElecAuthor;
  int ElecLoosePP;
  int ElecEta;
  int ElecEt;
  int ElecOQ;
  int ElecZ0;
  int ElecOverlap;
  int ElecOverlapCl;
  int ElecMuOverlap;
  int m_nelecsel;
  /* jet counter variables */
  int JetPt;
  int JetEta;
  int JetPU;
  int JetClean;
  int JetElOverlap;
  int m_njetsel;
  int m_njet30;
  /* tau counter variables */
  int TauPt;
  int TauEta;
  int TauNTrk;
  int TauCharge;
  int TauJetBDT;
  int TauElVeto;
  int TauMuVeto;
  int TauTauOverlap;
  int TauMuOverlap;
  int TauElOverlap;
  int m_ntausel;
  /* muons quality cuts */
  double m_MuonStacoPtCut;
  double m_MuonCaloPtCut;
  double m_MuonD0Cut;
  double m_MuonZ0Cut;
  /* electrons quality cuts */
  double m_ElecEtCut;
  double m_ElecZ0Cut;
  /* taus qaulity cuts */
  double m_TauPtCut;
  /* jets quality cuts */
  double m_JetPtCut1;
  double m_JetPtCut2;
  /* muons proprierties: algorithms vectors */
  std::vector<int>    Staco_Index,             Calo_Index,             CBSTSA_Index;
  std::vector<int>    Staco_Id,                Calo_Id,                CBSTSA_Id;
  std::vector<int>    Staco_Author,            Calo_Author,            CBSTSA_Author;
  std::vector<int>    Staco_charge,            Calo_charge,	       CBSTSA_charge;
  std::vector<double> Staco_d0_unbias,         Calo_d0_unbias,	       CBSTSA_d0_unbias;
  std::vector<double> Staco_scf,               Calo_scf,	       CBSTSA_scf;
  std::vector<double> Staco_resoscf,           Calo_resoscf,	       CBSTSA_resoscf;
  std::vector<double> Staco_z0_unbias,         Calo_z0_unbias,	       CBSTSA_z0_unbias;
  std::vector<double> Staco_pt,                Calo_pt,		       CBSTSA_pt;
  std::vector<double> Staco_eta,               Calo_eta,	       CBSTSA_eta;
  std::vector<double> Staco_phi,               Calo_phi,	       CBSTSA_phi;
  //------- optional -------//
  std::vector<int>    Staco_isSAmu,            Calo_isSAmu,	       CBSTSA_isSAmu;
  std::vector<int>    Staco_isSTmu,            Calo_isSTmu,	       CBSTSA_isSTmu;
  std::vector<int>    Staco_BLYgood,           Calo_BLYgood,	       CBSTSA_BLYgood;
  std::vector<int>    Staco_PXLgood,           Calo_PXLgood,	       CBSTSA_PXLgood;
  std::vector<int>    Staco_SCTgood,           Calo_SCTgood,	       CBSTSA_SCTgood;
  std::vector<int>    Staco_HOLgood,           Calo_HOLgood,	       CBSTSA_HOLgood;
  std::vector<int>    Staco_TRTgood,           Calo_TRTgood,	       CBSTSA_TRTgood;
  std::vector<int>    Staco_D0good,            Calo_D0good,	       CBSTSA_D0good;
  std::vector<int>    Staco_Z0good,            Calo_Z0good,	       CBSTSA_Z0good;
  std::vector<int>    Staco_MCPgood,           Calo_MCPgood,	       CBSTSA_MCPgood;
  std::vector<int>    Staco_MSgood,            Calo_MSgood,	       CBSTSA_MSgood;
  //std::vector<int>    Staco_OurMSgood,         Calo_OurMSgood,	       CBSTSA_OurMSgood;
  std::vector<int>    Staco_CBTAGgood,         Calo_CBTAGgood,	       CBSTSA_CBTAGgood;
  std::vector<int>    Staco_SAgood,            Calo_SAgood,	       CBSTSA_SAgood;
  std::vector<int>    Staco_CALOgood,          Calo_CALOgood,	       CBSTSA_CALOgood;
  std::vector<int>    Staco_n_exblh,           Calo_n_exblh,	       CBSTSA_n_exblh;
  std::vector<int>    Staco_n_blh,             Calo_n_blh,	       CBSTSA_n_blh;
  std::vector<int>    Staco_n_pxlh,            Calo_n_pxlh,	       CBSTSA_n_pxlh;
  std::vector<int>    Staco_n_pxlds,           Calo_n_pxlds,	       CBSTSA_n_pxlds;
  std::vector<int>    Staco_n_pxlhol,          Calo_n_pxlhol,	       CBSTSA_n_pxlhol;
  std::vector<int>    Staco_n_scth,            Calo_n_scth,	       CBSTSA_n_scth;
  std::vector<int>    Staco_n_sctds,           Calo_n_sctds,	       CBSTSA_n_sctds;
  std::vector<int>    Staco_n_scthol,          Calo_n_scthol,	       CBSTSA_n_scthol;
  std::vector<int>    Staco_n_hol,             Calo_n_hol,	       CBSTSA_n_hol;
  std::vector<int>    Staco_n_trth,            Calo_n_trth,	       CBSTSA_n_trth;
  std::vector<int>    Staco_n_trto,            Calo_n_trto,	       CBSTSA_n_trto;
  std::vector<int>    Staco_n_trtho,           Calo_n_trtho,	       CBSTSA_n_trtho;
  std::vector<int>    Staco_n_mdth,            Calo_n_mdth,	       CBSTSA_n_mdth;
  std::vector<int>    Staco_n_mdtBIh,          Calo_n_mdtBIh,	       CBSTSA_n_mdtBIh;
  std::vector<int>    Staco_n_mdtBMh,          Calo_n_mdtBMh,	       CBSTSA_n_mdtBMh;
  std::vector<int>    Staco_n_mdtBOh,          Calo_n_mdtBOh,	       CBSTSA_n_mdtBOh;
  std::vector<int>    Staco_n_mdtBEEh,         Calo_n_mdtBEEh,	       CBSTSA_n_mdtBEEh;
  std::vector<int>    Staco_n_mdtBIS78h,       Calo_n_mdtBIS78h,       CBSTSA_n_mdtBIS78h;
  std::vector<int>    Staco_n_mdtEIh,          Calo_n_mdtEIh,	       CBSTSA_n_mdtEIh;
  std::vector<int>    Staco_n_mdtEMh,          Calo_n_mdtEMh,	       CBSTSA_n_mdtEMh;
  std::vector<int>    Staco_n_mdtEOh,          Calo_n_mdtEOh,	       CBSTSA_n_mdtEOh;
  std::vector<int>    Staco_n_mdtEEh,          Calo_n_mdtEEh,	       CBSTSA_n_mdtEEh;
  std::vector<int>    Staco_n_cscEtah,         Calo_n_cscEtah,	       CBSTSA_n_cscEtah;
  std::vector<int>    Staco_n_cscPhih,         Calo_n_cscPhih,	       CBSTSA_n_cscPhih;
  std::vector<int>    Staco_n_rpcEtah,         Calo_n_rpcEtah,	       CBSTSA_n_rpcEtah;
  std::vector<int>    Staco_n_rpcPhih,         Calo_n_rpcPhih,	       CBSTSA_n_rpcPhih;
  std::vector<int>    Staco_n_tgcEtah,         Calo_n_tgcEtah,	       CBSTSA_n_tgcEtah;
  std::vector<int>    Staco_n_tgcPhih,         Calo_n_tgcPhih,	       CBSTSA_n_tgcPhih;
  std::vector<double> Staco_d0,                Calo_d0,		       CBSTSA_d0;
  std::vector<double> Staco_errd0,             Calo_errd0,	       CBSTSA_errd0;
  std::vector<double> Staco_sigd0_unbias,      Calo_sigd0_unbias,      CBSTSA_sigd0_unbias;
  std::vector<double> Staco_z0,                Calo_z0,		       CBSTSA_z0;
  std::vector<double> Staco_p,                 Calo_p,		       CBSTSA_p;
  std::vector<double> Staco_px,                Calo_px,		       CBSTSA_px;
  std::vector<double> Staco_py,                Calo_py,		       CBSTSA_py;
  std::vector<double> Staco_pz,                Calo_pz,		       CBSTSA_pz;
  std::vector<double> Staco_pid,               Calo_pid,	       CBSTSA_pid;
  std::vector<double> Staco_E,                 Calo_E,		       CBSTSA_E;
  std::vector<double> Staco_theta,             Calo_theta,	       CBSTSA_theta;
  std::vector<double> Staco_theta_id,          Calo_theta_id,	       CBSTSA_theta_id;
  std::vector<double> Staco_eta_id,            Calo_eta_id,	       CBSTSA_eta_id;
  std::vector<double> Staco_phi_id,            Calo_phi_id,	       CBSTSA_phi_id;
  std::vector<double> Staco_isocalo20,         Calo_isocalo20,	       CBSTSA_isocalo20;
  std::vector<double> Staco_isocalo30,	       Calo_isocalo30,	       CBSTSA_isocalo30;      
  std::vector<double> Staco_isocalo40,	       Calo_isocalo40,	       CBSTSA_isocalo40;      
  std::vector<double> Staco_isotrk20,          Calo_isotrk20,	       CBSTSA_isotrk20;
  std::vector<double> Staco_isotrk30,          Calo_isotrk30,	       CBSTSA_isotrk30;
  std::vector<double> Staco_isotrk40,          Calo_isotrk40,	       CBSTSA_isotrk40;
  std::vector<double> Staco_pt_truth,          Calo_pt_truth,	       CBSTSA_pt_truth;
  std::vector<double> Staco_eta_truth,         Calo_eta_truth,	       CBSTSA_eta_truth;
  std::vector<double> Staco_phi_truth,         Calo_phi_truth,	       CBSTSA_phi_truth;
  std::vector<double> Staco_E_truth,           Calo_E_truth,	       CBSTSA_E_truth;
  std::vector<int>    Staco_mother_truth,      Calo_mother_truth,      CBSTSA_mother_truth;
  std::vector<int>    Staco_motherbcode_truth, Calo_motherbcode_truth, CBSTSA_motherbcode_truth;
  std::vector<double> Staco_cov_d0,	       Calo_cov_d0,	       CBSTSA_cov_d0;
  std::vector<double> Staco_cov_z0,	       Calo_cov_z0,	       CBSTSA_cov_z0;
  std::vector<double> Staco_cov_phi,	       Calo_cov_phi,	       CBSTSA_cov_phi;
  std::vector<double> Staco_cov_theta,	       Calo_cov_theta,	       CBSTSA_cov_theta;
  std::vector<double> Staco_cov_qoverp,	       Calo_cov_qoverp,	       CBSTSA_cov_qoverp;
  std::vector<double> Staco_cov_d0_z0,	       Calo_cov_d0_z0,	       CBSTSA_cov_d0_z0;
  std::vector<double> Staco_cov_d0_phi,	       Calo_cov_d0_phi,	       CBSTSA_cov_d0_phi;
  std::vector<double> Staco_cov_d0_theta,      Calo_cov_d0_theta,      CBSTSA_cov_d0_theta;
  std::vector<double> Staco_cov_d0_qoverp,     Calo_cov_d0_qoverp,     CBSTSA_cov_d0_qoverp;
  std::vector<double> Staco_cov_z0_phi,	       Calo_cov_z0_phi,	       CBSTSA_cov_z0_phi;
  std::vector<double> Staco_cov_z0_theta,      Calo_cov_z0_theta,      CBSTSA_cov_z0_theta;
  std::vector<double> Staco_cov_z0_qoverp,     Calo_cov_z0_qoverp,     CBSTSA_cov_z0_qoverp;
  std::vector<double> Staco_cov_phi_theta,     Calo_cov_phi_theta,     CBSTSA_cov_phi_theta;
  std::vector<double> Staco_cov_phi_qoverp,    Calo_cov_phi_qoverp,    CBSTSA_cov_phi_qoverp;
  std::vector<double> Staco_cov_theta_qoverp,  Calo_cov_theta_qoverp,  CBSTSA_cov_theta_qoverp;
  /* general muon vectors */
  std::vector<int>    Mu_Index;                //muon index
  std::vector<int>    Mu_Id;                   //muon id
  std::vector<int>    Mu_Author;               //muon author
  std::vector<int>    Mu_charge;               //muon charge
  std::vector<double> Mu_d0_unbias;            //muon d0 unbiased
  std::vector<double> Mu_scf;                  //muon scale factor
  std::vector<double> Mu_resoscf;              //muon resolution scale factor
  std::vector<double> Mu_z0_unbias;            //muon z0 unbiased
  std::vector<double> Mu_pt;                   //muon pt
  std::vector<double> Mu_eta;                  //muon eta
  std::vector<double> Mu_phi;                  //muon phi
  std::vector<int>    Mu_isSAmu;               //isSAmuon
  std::vector<int>    Mu_isSTmu;               //isSTmuon
  std::vector<int>    Mu_BLYgood;              //is good Blayer muon
  std::vector<int>    Mu_PXLgood;              //is good Pixel muon
  std::vector<int>    Mu_SCTgood;              //is good SCT muon
  std::vector<int>    Mu_HOLgood;              //is good holes muon
  std::vector<int>    Mu_TRTgood;              //is good TRT muon
  std::vector<int>    Mu_D0good;               //is good D0 muon
  std::vector<int>    Mu_Z0good;               //is good Z0 muon
  std::vector<int>    Mu_MCPgood;              //is good MCP muon
  std::vector<int>    Mu_MSgood;               //is good MS muon
  //std::vector<int>    Mu_OurMSgood;            //is good MS muon (our definition)
  std::vector<int>    Mu_CBTAGgood;            //is good CB or Tag muon
  std::vector<int>    Mu_SAgood;               //is good SA muon
  std::vector<int>    Mu_CALOgood;             //is good CALO muon
  std::vector<int>    Mu_n_exblh;              //expect Blayer hit
  std::vector<int>    Mu_n_blh;                //Blayer hits
  std::vector<int>    Mu_n_pxlh;               //pixel hits
  std::vector<int>    Mu_n_pxlds;              //pixel dead sensors
  std::vector<int>    Mu_n_pxlhol;             //pixel holes
  std::vector<int>    Mu_n_scth;               //sct hits
  std::vector<int>    Mu_n_sctds;              //sct dead sensors
  std::vector<int>    Mu_n_scthol;             //sct holes
  std::vector<int>    Mu_n_hol;                //pixel+sct holes
  std::vector<int>    Mu_n_trth;               //trt hits
  std::vector<int>    Mu_n_trto;               //trt out layers
  std::vector<int>    Mu_n_trtho;              //trt hits+outlayers
  std::vector<int>    Mu_n_mdth;               //mdt hits
  std::vector<int>    Mu_n_mdtBIh;             //mdt BI hits
  std::vector<int>    Mu_n_mdtBMh;             //mdt BM hits
  std::vector<int>    Mu_n_mdtBOh;             //mdt BO hits
  std::vector<int>    Mu_n_mdtBEEh;            //mdt BEE hits
  std::vector<int>    Mu_n_mdtBIS78h;          //mdt BIS78 hits
  std::vector<int>    Mu_n_mdtEIh;             //mdt EI hits
  std::vector<int>    Mu_n_mdtEMh;             //mdt EM hits
  std::vector<int>    Mu_n_mdtEOh;             //mdt EO hits
  std::vector<int>    Mu_n_mdtEEh;             //mdt EE hits
  std::vector<int>    Mu_n_cscEtah;            //cscEta hits
  std::vector<int>    Mu_n_cscPhih;            //cscPhi hits
  std::vector<int>    Mu_n_rpcEtah;            //rpc Eta hits
  std::vector<int>    Mu_n_rpcPhih;            //prc Phi hits
  std::vector<int>    Mu_n_tgcEtah;            //tgc Eta hits 
  std::vector<int>    Mu_n_tgcPhih;            //tgc Phi hits
  std::vector<double> Mu_d0;                   //muon d0 (exPV)
  std::vector<double> Mu_errd0;                //muon d0 error (exPV)
  std::vector<double> Mu_sigd0_unbias;         //muon sigd0 unbiased
  std::vector<double> Mu_z0;                   //muon z0 (exPV)
  std::vector<double> Mu_p;                    //muon p
  std::vector<double> Mu_px;                   //muon px
  std::vector<double> Mu_py;                   //muon py
  std::vector<double> Mu_pz;                   //muon pz
  std::vector<double> Mu_pid;                  //muon pid
  std::vector<double> Mu_E;                    //muon pt
  std::vector<double> Mu_theta;                //muon theta 
  std::vector<double> Mu_theta_id;             //muon theta on ID
  std::vector<double> Mu_eta_id;               //muon eta on ID
  std::vector<double> Mu_phi_id;               //muon phi on ID
  std::vector<double> Mu_isocalo20;            //muon calo isol. cone 20
  std::vector<double> Mu_isocalo30;            //muon calo isol. cone 30
  std::vector<double> Mu_isocalo40;            //muon calo isol. cone 40
  std::vector<double> Mu_isotrk20;             //muon track isol. cone 20 
  std::vector<double> Mu_isotrk30;             //muon track isol. cone 30 
  std::vector<double> Mu_isotrk40;             //muon track isol. cone 40 
  std::vector<double> Mu_pt_truth;             //muon pt (truth)
  std::vector<double> Mu_eta_truth;            //muon eta (truth)
  std::vector<double> Mu_phi_truth;            //muon phi (truth)
  std::vector<double> Mu_E_truth;              //muon E (truth)
  std::vector<int>    Mu_mother_truth;         //muon mother (truth)
  std::vector<int>    Mu_motherbcode_truth;    //muon mother barcode(truth)
  std::vector<double> Mu_cov_d0;               //muon cov. matrix d0
  std::vector<double> Mu_cov_z0;	       //muon cov. matrix z0
  std::vector<double> Mu_cov_phi;	       //muon cov. matrix phi
  std::vector<double> Mu_cov_theta;	       //muon cov. matrix theta    
  std::vector<double> Mu_cov_qoverp;	       //muon cov. matrix qoverp
  std::vector<double> Mu_cov_d0_z0;	       //muon cov. matrix d0-z0   
  std::vector<double> Mu_cov_d0_phi;	       //muon cov. matrix d0-phi
  std::vector<double> Mu_cov_d0_theta;	       //muon cov. matrix d0-theta
  std::vector<double> Mu_cov_d0_qoverp;	       //muon cov. matrix d0-qoverp
  std::vector<double> Mu_cov_z0_phi;	       //muon cov. matrix z0-phi
  std::vector<double> Mu_cov_z0_theta;         //muon cov. matrix z0-theta
  std::vector<double> Mu_cov_z0_qoverp;        //muon cov. matrix z0-qoverp
  std::vector<double> Mu_cov_phi_theta;        //muon cov. matrix phi-theta
  std::vector<double> Mu_cov_phi_qoverp;       //muon cov. matrix phi-qoverp
  std::vector<double> Mu_cov_theta_qoverp;     //muon cov. matrix theta-qoverp
  /* electron proprierties*/
  std::vector<int>    m_ElIndex;             //electron index 
  std::vector<int>    m_ElIndex_eeOverlap;   //electron index after ee Overlap
  std::vector<int>    m_ElIndex_eeOverlapCL; //electron index after ee Overlap in cluster
  std::vector<int>    m_ElIndex_emuOverlap;  //electron index after emu Overlap
  
  std::vector<int>    El_Index;              //electron index
  std::vector<int>    El_Id;                 //electron id
  std::vector<int>    El_Author;             //electron author
  std::vector<int>    El_charge;             //electron charge
  std::vector<double> El_d0_unbias;          //electron d0 unbiased
  std::vector<double> El_scf;                //electron efficiency scale factor
  std::vector<double> El_z0_unbias;          //electron z0 unbiased
  std::vector<double> El_E;                  //electron E
  std::vector<double> El_pt;                 //electron pt
  std::vector<double> El_eta;                //electron eta
  std::vector<double> El_phi;                //electron phi
  std::vector<double> El_Ereso;              //electron energy resolution
  std::vector<double> El_d0;                 //electron d0 (trkd0)
  std::vector<double> El_sigd0_unbias;       //electron sigd0 unbiased
  std::vector<double> El_z0;                 //electron z0
  std::vector<double> El_Eclraw;             //electron E cluster raw
  std::vector<double> El_p;                  //electron p
  std::vector<double> El_px;                 //electron px
  std::vector<double> El_py;                 //electron py
  std::vector<double> El_pz;                 //electron pz
  std::vector<double> El_pid;                //electron pid
  std::vector<double> El_trkpt;              //electron trkpt
  std::vector<double> El_Et;                 //electron Et
  std::vector<double> El_theta;              //electron theta 
  std::vector<double> El_etaS2;              //electron eta in cluster
  std::vector<double> El_etacl;              //electron eta in cluster
  std::vector<double> El_etap;               //electron eta in cluster (topo)
  std::vector<double> El_phiS2;              //electron phi in cluster (S2)
  std::vector<double> El_phicl;              //electron phi in cluster
  std::vector<double> El_isocalo20;          //electron calo isol. cone 20
  std::vector<double> El_isocalo30;          //electron calo isol. cone 30
  std::vector<double> El_isocalo40;          //electron calo isol. cone 40
  std::vector<double> El_topoisocalo20;      //electron calo isol. cone 20 (topo)
  std::vector<double> El_topoisocalo30;      //electron calo isol. cone 30 (topo)
  std::vector<double> El_topoisocalo40;      //electron calo isol. cone 40 (topo)
  std::vector<double> El_isotrk20;           //electron track isol. cone 20 
  std::vector<double> El_isotrk30;           //electron track isol. cone 30 
  std::vector<double> El_isotrk40;           //electron track isol. cone 40
  std::vector<double> El_EDmedian;           //electron ED median
  std::vector<double> El_pt_truth;           //electron pt (truth)
  std::vector<double> El_eta_truth;          //electron eta (truth)
  std::vector<double> El_phi_truth;          //electron phi (truth)
  std::vector<double> El_E_truth;            //electron E (truth)
  std::vector<int>    El_mother_truth;       //electron mother (truth)
  std::vector<int>    El_motherbcode_truth;  //electron mother barcode (truth)
  std::vector<double> El_cov_d0;             //electron cov. matrix d0       
  std::vector<double> El_cov_z0;             //electron cov. matrix z0       
  std::vector<double> El_cov_phi;            //electron cov. matrix phi      
  std::vector<double> El_cov_theta;          //electron cov. matrix theta    
  std::vector<double> El_cov_d0_z0;          //electron cov. matrix d0-z0    
  std::vector<double> El_cov_d0_phi;         //electron cov. matrix d0-phi   
  std::vector<double> El_cov_d0_theta;       //electron cov. matrix d0-theta 
  std::vector<double> El_cov_z0_phi;         //electron cov. matrix z0-phi   
  std::vector<double> El_cov_z0_theta;       //electron cov. matrix z0-theta 
  std::vector<double> El_cov_phi_theta;      //electron cov. matrix phi-theta
  /* tau properties */
  std::vector<int>     m_TaIndex;
  std::vector<int>     m_TaIndex_tmuOverlap;
  std::vector<int>     m_TaIndex_telOverlap;
  std::vector<int>     Ta_Index;
  std::vector<int>     Ta_Id;
  std::vector<int>     Ta_Author;
  std::vector<double>  Ta_Et;	  
  std::vector<double>  Ta_pt;	  
  std::vector<double>  Ta_m;	  
  std::vector<double>  Ta_eta;	  
  std::vector<double>  Ta_phi;	  
  std::vector<int>     Ta_charge;
  std::vector<double>  Ta_likelihood;
  std::vector<double>  Ta_SafeLikelihood;
  std::vector<double>  Ta_electronVetoLoose;
  std::vector<double>  Ta_electronVetoMedium;
  std::vector<double>  Ta_electronVetoTight;
  std::vector<double>  Ta_muonVeto;
  std::vector<double>  Ta_tauLlhLoose;
  std::vector<double>  Ta_tauLlhMedium;
  std::vector<double>  Ta_tauLlhTight;
  std::vector<double>  Ta_JetBDTSigLoose;
  std::vector<double>  Ta_JetBDTSigMedium;
  std::vector<double>  Ta_JetBDTSigTight;
  std::vector<double>  Ta_EleBDTLoose;
  std::vector<double>  Ta_EleBDTMedium;
  std::vector<double>  Ta_EleBDTTight;
  std::vector<int>     Ta_numTrack;
  std::vector<double>  Ta_BDTJetScore;
  std::vector<double>  Ta_BDTEleScore;
  /* jet properties */
  std::vector<int>    m_JetIndex;            //jet index
  std::vector<int>    m_JetIndex_jeteOverlap;//jet index after jet/e overlap
  std::vector<int>    Jt_Index;             //jet index 
  std::vector<double> Jt_pt;                //jet pt
  std::vector<double> Jt_eta;               //jet eta
  std::vector<double> Jt_phi;               //jet phi
  std::vector<double> Jt_E;                 //jet E
  std::vector<double> Jt_m;                 //jet m
  std::vector<double> Jt_jvtx;              //jet vertex fraction
  /* general particle vectors*/
  std::vector<int>    Lep_Index;             //lepton index
  std::vector<int>    Lep_Id;                //lepton id
  std::vector<int>    Lep_Author;            //lepton author
  std::vector<int>    Lep_charge;            //lepton charge
  std::vector<double> Lep_d0_unbias;         //lepton d0 unbiased
  std::vector<double> Lep_scf;               //lepton scale factor
  std::vector<double> Lep_resoscf;           //lepton resolution scale factor
  std::vector<double> Lep_z0_unbias;         //lepton z0 unbiased
  std::vector<double> Lep_E;                 //lepton E
  std::vector<double> Lep_pt;                //lepton pt
  std::vector<double> Lep_eta;               //lepton eta
  std::vector<double> Lep_phi;               //lepton phi
  std::vector<double> Lep_Ereso;             //lepton E resolution
  std::vector<int>    Lep_SAgood;            //is good SA lepton
  std::vector<int>    Lep_CALOgood;          //is good CALO lepton
  std::vector<int>    Lep_numTrack;          //num. hadron tracks
  
  /* muons quality flags */
  bool isGoodTRTmuon;
  bool isGoodMCPmuon;
  bool isGoodMSmuonSA;
  //bool isGoodOurMSmuonSA;
  bool isGoodBlayer;
  bool isGoodPixel;
  bool isGoodSCT;
  bool isGoodHoles;
  bool isGoodZ0muon;
  bool isGoodD0muon;
  bool isAuthorCBGood;
  bool isAuthorSAGood;
  bool isAuthorCALOGood;
  bool isGoodCBTagMuon;
  bool isGoodSAMuon;
  bool isGoodCALOmuon;
  int nbl;
  int npixl;
  int nscth;
  int nhol;
  int ntrth;
  int ntrto;
  int ni;
  int nm;
  int no;
  int nphi;
  /* electron quality flags */
  bool isGoodAuthorElec;
  bool isGoodLoosePPElec;
  bool isGoodEtaElec;
  bool isGoodEtElec;
  bool isGoodOQElec;
  bool isGoodZ0Elec;
  bool isGoodElectron;
  /* jets quality flags */
  bool isGoodJetPt;
  bool isGoodJetEta;
  bool isGoodJetPU;
  bool isGoodJetBLM;
  bool isGoodJetClean;
  /* taus quality flags */
  bool isGoodTauPt;
  bool isGoodTauEta;
  bool isGoodTauNTrk;
  bool isGoodTauCharge;
  bool isGoodTauJetBDT;
  bool isGoodTauElVeto;
  bool isGoodTauMuVeto;
  bool isGoodTau;
  
  /* utils objects */
  //egRescaler::EnergyRescalerUpgrade* m_El_ERescaler;
  AtlasRoot::egammaEnergyCorrectionTool* m_El_ERescaler;
  MuonSmear::SmearingClass* m_MuonSmear;
  Analysis::MuonResolutionAndMomentumScaleFactors *m_MuonResoSCF;
  Analysis::AnalysisMuonConfigurableScaleFactors* m_StacoSCF;
  Analysis::AnalysisMuonConfigurableScaleFactors* m_StacoSASCF;
  Analysis::AnalysisMuonConfigurableScaleFactors* m_CaloSCF;
  Root::TElectronEfficiencyCorrectionTool *m_El_SCFId;
  Root::TElectronEfficiencyCorrectionTool *m_El_SCFReco;
  Root::TPileupReweighting* m_PileUp;
  Root::TElectronLikelihoodTool* m_ElId2012;
  //H4l2011Defs* m_ElId2011;
  Root::TElectronIsEMSelector* m_ElId2011;
  JetAnalysisCalib::JetCalibrationTool* m_JetCalib;
  EMAmbiguityType::AmbiguityResult m_Amb;
  Root::TAccept m_Taccept;
  Double_t m_Discriminant;
  static TRandom3* m_rand;
  PATCore::ParticleDataType::DataType m_DataType;
  PATCore::ParticleType::Type m_ParType;
  egEnergyCorr::Scale::Variation m_ScaleVar;
  egEnergyCorr::Resolution::Variation m_Reso;
  egEnergyCorr::Resolution::resolutionType m_ResType;

  /* D0 and Z0 Smearing objects */
  TFile *m_ip_smear;
  TH2F  *m_smearD0_0;
  TH2F  *m_smearD0_1;
  TH2F  *m_smearD0_2;
  TH2F  *m_smearZ0_0;
  TH2F  *m_smearZ0_1;
  TH2F  *m_smearZ0_2;
  
};
#endif
