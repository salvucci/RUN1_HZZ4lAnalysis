#include "Particles.h"
#include "../DPDVar/DPDJetVariables.h"

void Particles::ResetJetQualityFlags(){

  isGoodJetPt    = false;
  isGoodJetEta   = false;
  isGoodJetPU    = true;
  isGoodJetBLM   = false;
  isGoodJetClean = false;

}

void Particles::CheckingJets(bool isVertexGood,
			     bool isTrigGood,
			     double mu,
			     int nPV){

  Print("                   Particles:: Checking jets ... ");

  int njet=jet_akt4topoem_n;
  m_njetsel=0;
  m_njet30=0;
  
  for (int i=0; i<njet; i++) {

    double jetPT=0;
    if(m_UseCorr){
      TLorentzVector m_jet=ApplyJetCalibration(i,mu,nPV);
      jetPT=m_jet.Pt()/m_GeV;
    }
    else{
      jetPT=jet_akt4topoem_pt->at(i)/m_GeV;
    }
    double jetEta=jet_akt4topoem_emscale_eta->at(i);

    isGoodJetPt=false;
    if(jetPT>m_JetPtCut1){
      isGoodJetPt=true;
    }

    isGoodJetEta=false;
    if( ( jetPT>m_JetPtCut1 && fabs(jetEta)<2.4 )                    ||
	( jetPT>m_JetPtCut2 && fabs(jetEta)>2.4 && abs(jetEta)<4.5 )  ){
      isGoodJetEta=true;
    }

    double JvfThr=0;
    if(m_year==m_YEAR_2011) { JvfThr = 0.75; }
    else if (m_year==m_YEAR_2012) { JvfThr = 0.50; }

    isGoodJetPU=true;
    if( jetPT<50. && fabs(jetEta)<2.4 &&
	fabs(jet_akt4topoem_jvtxf->at(i))<JvfThr ){
      isGoodJetPU=false;
    }

    isGoodJetBLM=false;
    if( !jet_akt4topoem_isBadLooseMinus->at(i) )
      isGoodJetBLM=true;

    isGoodJetClean=false;
    if( isGoodJetBLM && !ApplyHotTileCellVeto(i) ){
      isGoodJetClean=true;
    }

    if(jetPT>30.   && fabs(jetEta)<4.5 &&
       isGoodJetPU && isGoodJetClean    ){
      m_njet30=m_njet30+1;
    }

    PrintOut("                  Jet:: Eta = ");
    PrintOut(jet_akt4topoem_emscale_eta->at(i));
    PrintOut(" pt = "); PrintOut(jetPT); 
    PrintOut(" PileUp check = "); PrintOut(isGoodJetPU);
    PrintOut(" Jet Clean = "); PrintOut(isGoodJetClean); PrintEndl();
    
    if( isGoodJetPt && isGoodJetEta  &&
	isGoodJetPU && isGoodJetClean ){
      m_JetIndex.push_back(i);
      PrintOut("                  Jet:: Good Jet Found!!"); PrintEndl();
    }

    if(isVertexGood && isTrigGood){
      if(isGoodJetPt){
	JetPt++;
	if(isGoodJetEta){
	  JetEta++;
	  if(isGoodJetPU){
	    JetPU++;
	    if(isGoodJetClean){
	      JetClean++;
	    }
	  }
	}
      }
    }
  }

  Print("                   Particles:: Removing jets-electrons overlap");
  bool isNot_jetel_Overlap=true;
  for(UInt_t jt=0; jt<m_JetIndex.size(); jt++){
    isNot_jetel_Overlap = CheckJetElOverlap(m_JetIndex.at(jt),mu,nPV);
    if(isNot_jetel_Overlap){
      m_JetIndex_jeteOverlap.push_back(m_JetIndex.at(jt));
    }
    if(isVertexGood && isTrigGood && isNot_jetel_Overlap){
      JetElOverlap++;
    }
  }
  m_njetsel=(int)m_JetIndex_jeteOverlap.size();

  Print("                   Particles:: Filling jets quantities");
  for(UInt_t jt=0; jt<m_JetIndex_jeteOverlap.size(); jt++){
    
    
    double pT=0, eta=0, phi=0, E=0, m=0;
    if(m_UseCorr){
      TLorentzVector jet = ApplyJetCalibration(m_JetIndex_jeteOverlap.at(jt),
					       mu,nPV);
      pT  = jet.Pt();
      eta = jet.Eta();
      phi = jet.Phi();
      E   = jet.E();
      m   = jet.M();
    }
    else{
      pT  = jet_akt4topoem_emscale_pt->at(m_JetIndex_jeteOverlap.at(jt));
      eta = jet_akt4topoem_emscale_eta->at(m_JetIndex_jeteOverlap.at(jt));
      phi = jet_akt4topoem_emscale_phi->at(m_JetIndex_jeteOverlap.at(jt));
      E   = jet_akt4topoem_emscale_E->at(m_JetIndex_jeteOverlap.at(jt));
      m   = jet_akt4topoem_emscale_m->at(m_JetIndex_jeteOverlap.at(jt));
    }
    
    Jt_Index.push_back( m_JetIndex_jeteOverlap.at(jt) );
    Jt_pt.push_back( pT );
    Jt_eta.push_back( eta );
    Jt_phi.push_back( phi );
    Jt_E.push_back( E );
    Jt_m.push_back( m );
    Jt_jvtx.push_back( jet_akt4topoem_jvtxf->at(m_JetIndex_jeteOverlap.at(jt)) );
    
  }
  
}

bool Particles::ApplyHotTileCellVeto(int idx){

  bool etaphi28 = false;
  if( jet_akt4topoem_eta->at(idx)>-0.2 && jet_akt4topoem_eta->at(idx)>-0.1 &&
      jet_akt4topoem_phi->at(idx)>2.65 && jet_akt4topoem_phi->at(idx)>2.75  ){
    etaphi28=true;
  }
  bool affected = (RunNumber==202660 || RunNumber==202668 ||
		   RunNumber==202712 || RunNumber==202740 ||
		   RunNumber==202965 || RunNumber==202987 ||
		   RunNumber==202991 || RunNumber==203027) ? true : false;
  
  if( jet_akt4topoem_fracSamplingMax->at(idx)>0.6 &&
      jet_akt4topoem_SamplingMax->at(idx)==13     &&
      etaphi28 && affected                         ){
    return true;
  }
  
  return false;
  
}

bool Particles::CheckJetElOverlap(UInt_t jt,
				  double mu,
				  int nPV){

  bool NoOverlap=true;
  double phi1=0, eta1=0;
  if(m_UseCorr){
    TLorentzVector m_jet=ApplyJetCalibration(jt,mu,nPV);
    phi1=m_jet.Phi();
    eta1=m_jet.Eta();
  }
  else{
  phi1 = jet_akt4topoem_emscale_phi->at(jt);
  eta1 = jet_akt4topoem_emscale_eta->at(jt);
  }
  
  for(UInt_t el=0; el<Lep_Index.size(); el++){

    if( abs(Lep_Id.at(el))!=11) continue;

    //double phi1 = jet_akt4topoem_emscale_phi->at(jt);
    //double eta1 = jet_akt4topoem_emscale_eta->at(jt);

    double phi2=0, eta2=0;
    if(m_year==m_YEAR_2011){
      phi2 = el_GSF_trackphi->at(Lep_Index.at(el));
      eta2 = el_GSF_tracketa->at(Lep_Index.at(el));
    }
    else if(m_year==m_YEAR_2012){
      phi2 = el_trackphi->at(Lep_Index.at(el));
      eta2 = el_tracketa->at(Lep_Index.at(el));
    }
    double dR = DeltaR(eta1,phi1,eta2,phi2);

    PrintOut("                               DeltaR "); PrintOut(dR);
    PrintEndl();

    if(dR<0.2){
      Print("                   Particles:: Overlap jet/e Found -> jet removed!");
      NoOverlap = false;
    }
  }
  return NoOverlap;
  
}

TLorentzVector Particles::ApplyJetCalibration(int idx,
					     double mu,
					     int nPV){
  
  Print("                               Applying Jet Calibration ... ");
  
  Double_t Eraw    = 0.0;
  //Double_t Ptraw   = 0.0;
  Double_t eta     = 0.0;
  Double_t eta_det = 0.0;
  Double_t phi     = 0.0;
  Double_t m       = 0.0;
  Double_t Ax      = 0.0;
  Double_t Ay      = 0.0;
  Double_t Az      = 0.0;
  Double_t Ae      = 0.0;
  Double_t rho     = 0.0;
  
  if(m_year==m_YEAR_2011){
    Eraw    = jet_akt4topoem_emscale_E->at(idx);
    eta_det = jet_akt4topoem_emscale_eta->at(idx);
    eta     = jet_akt4topoem_EtaOrigin->at(idx);
    phi     = jet_akt4topoem_PhiOrigin->at(idx);
    m       = jet_akt4topoem_MOrigin->at(idx);
  }
  else if(m_year==m_YEAR_2012){
    Eraw    = jet_akt4topoem_emscale_E->at(idx);
    //Ptraw   = jet_akt4topoem_emscale_pt->at(idx);
    eta     = jet_akt4topoem_emscale_eta->at(idx);
    phi     = jet_akt4topoem_emscale_phi->at(idx);
    m       = jet_akt4topoem_emscale_m->at(idx);
    Ax      = jet_akt4topoem_ActiveAreaPx->at(idx);
    Ay      = jet_akt4topoem_ActiveAreaPy->at(idx);
    Az      = jet_akt4topoem_ActiveAreaPz->at(idx);
    Ae      = jet_akt4topoem_ActiveAreaE->at(idx);
    rho     = Eventshape_rhoKt4EM;
  }

  TLorentzVector jet;
  if(m_year==m_YEAR_2011){
    jet = m_JetCalib->ApplyOffsetEtaJES(Eraw,eta_det,eta,
					phi,m,mu,nPV);
  }
  else if(m_year==m_YEAR_2012){
    jet = m_JetCalib->ApplyJetAreaOffsetEtaJES(Eraw,eta,phi,m,Ax,
					       Ay,Az,Ae,rho,mu,nPV);
  }
  return jet;

}
