#define Particles_cxx

#include "Particles.h"

TRandom3* Particles::m_rand = 0;

Particles::Particles(){

  if(!m_rand) {
    m_rand = new TRandom3();
  }

  m_GeV        = 1000.;
  m_pi         = TMath::Pi();
  m_Melec      = 0.0005109989;
  m_Mmuon      = 0.1056583;
  m_IdMuon     = 13;
  m_IdElectron = 11;
  m_IdTau      = 15;
  
  /* Muons Counters */
  StacoAthCBTag = 0;
  StacoAthSA    = 0;
  StacoPtCBTag  = 0;
  StacoPtSA     = 0;
  StacoBl       = 0;
  StacoPxl      = 0;
  StacoSCT      = 0;
  StacoHol      = 0;
  StacoTRT      = 0;
  StacoD0Z0     = 0;
  StacoMS       = 0;
  CaloAth       = 0;
  CaloPt        = 0;
  CaloBl        = 0;
  CaloPxl       = 0;
  CaloSCT       = 0;
  CaloHol       = 0;
  CaloTRT       = 0;
  CaloD0Z0      = 0;
  OverlapSTSA   = 0;
  OverlapCalo   = 0;

  /* Electrons Counters */
  ElecAuthor    = 0;
  ElecLoosePP   = 0;
  ElecEta       = 0;
  ElecEt        = 0;
  ElecOQ        = 0;
  ElecZ0        = 0;
  ElecOverlap   = 0;
  ElecOverlapCl = 0;
  ElecMuOverlap = 0;

  /* Jet Counters */
  JetPt         = 0;
  JetEta        = 0;
  JetPU         = 0;
  JetClean      = 0;
  JetElOverlap  = 0;

  /* Tau Counters */
  TauPt         = 0;
  TauEta        = 0;
  TauNTrk       = 0;
  TauCharge     = 0;
  TauJetBDT     = 0;
  TauElVeto     = 0;
  TauMuVeto     = 0;
  TauTauOverlap = 0;
  TauMuOverlap  = 0;
  TauElOverlap  = 0;

  /* leptons selected */
  m_nmuCBsel   = 0;
  m_nmuSAsel   = 0;
  m_nmuCBSAsel = 0;
  m_nmuCALOsel = 0;
  m_nmusel     = 0;
  m_nelecsel   = 0;
  m_ntausel    = 0;
  m_njetsel    = 0;
  m_njet30     = 0;
  
}

Particles::~Particles(){
}

void Particles::GeneralConf(bool debug,
			    int year,
			    int type,
			    bool usecorr,
			    bool useweight,
			    bool mininfo,
			    std::string prefix){
  
  m_debug      = debug;
  m_year       = year;
  m_type       = type;
  m_UseCorr    = usecorr;
  m_UseW       = useweight;
  m_MinParInfo = mininfo;
  m_DirPrefix  = prefix;
  m_Amb        = EMAmbiguityType::ELECTRON;
  m_ip_smear   = new TFile( (m_DirPrefix+"Utils/Inputs/impact_parameter_smearing.root").c_str() );
  m_smearD0_0  = (TH2F*)m_ip_smear->Get("smearD0_0");
  m_smearD0_1  = (TH2F*)m_ip_smear->Get("smearD0_1");
  m_smearD0_2  = (TH2F*)m_ip_smear->Get("smearD0_2");
  m_smearZ0_0  = (TH2F*)m_ip_smear->Get("smearZ0_0");
  m_smearZ0_1  = (TH2F*)m_ip_smear->Get("smearZ0_1");
  m_smearZ0_2  = (TH2F*)m_ip_smear->Get("smearZ0_2");
    
}

void Particles::Clean(){
  
  Print("                   Particles:: Cleaning objects ... ");
  
  m_smearD0_0->Delete();
  m_smearD0_1->Delete();
  m_smearD0_2->Delete();
  m_smearZ0_0->Delete();
  m_smearZ0_1->Delete();
  m_smearZ0_2->Delete();
  m_ip_smear->Close();
  
}

void Particles::ConfigMuonTools(MuonSmear::SmearingClass *muonsmear,
				Analysis::AnalysisMuonConfigurableScaleFactors *stacoSCF,
				Analysis::AnalysisMuonConfigurableScaleFactors *stacoSASCF,
				Analysis::AnalysisMuonConfigurableScaleFactors *caloSCF,
				Analysis::MuonResolutionAndMomentumScaleFactors *muonresoSCF){
  
  m_MuonSmear      = muonsmear;
  m_MuonResoSCF    = muonresoSCF;
  m_StacoSCF       = stacoSCF;
  m_StacoSASCF     = stacoSASCF;
  m_CaloSCF        = caloSCF;
  
}

void Particles::ConfigElecTools(//egRescaler::EnergyRescalerUpgrade *EResc,
				AtlasRoot::egammaEnergyCorrectionTool *EResc,
				Root::TElectronEfficiencyCorrectionTool *ScfId,
				Root::TElectronEfficiencyCorrectionTool *ScfReco,
				Root::TPileupReweighting* PileUp,
				Root::TElectronIsEMSelector* ElId2011,
				Root::TElectronLikelihoodTool* ElId2012){

  m_El_ERescaler = EResc;
  m_El_SCFId     = ScfId;
  m_El_SCFReco   = ScfReco;
  m_PileUp       = PileUp;
  m_ElId2012     = ElId2012;
  //m_ElId2011     = new H4l2011Defs();
  m_ElId2011     = ElId2011;

  m_ParType  = PATCore::ParticleType::Electron;
  m_ScaleVar = egEnergyCorr::Scale::Nominal;
  m_Reso     = egEnergyCorr::Resolution::Nominal;
  m_ResType  = egEnergyCorr::Resolution::SigmaEff90;
  
  if(m_type==m_DATA)
    m_DataType=PATCore::ParticleDataType::Data;
  else
    m_DataType=PATCore::ParticleDataType::Full;
  
}

void Particles::ConfigJetTools(JetAnalysisCalib::JetCalibrationTool* JetCal){

  m_JetCalib = JetCal;
  
}

void Particles::ConfigMuons(double stacopt,
			    double calopt,
			    double d0,
			    double z0){

  m_MuonStacoPtCut = stacopt;
  m_MuonCaloPtCut  = calopt;
  m_MuonD0Cut      = d0;
  m_MuonZ0Cut      = z0;
  
}

void Particles::ConfigElectrons(double elecEt,
				double z0){
			      
  
  m_ElecEtCut = elecEt;
  m_ElecZ0Cut = z0;
  
}

void Particles::ConfigTaus(double tauPt){

  m_TauPtCut = tauPt;
  
}

void Particles::ConfigJets(double jetpt1,
			   double jetpt2){

  m_JetPtCut1 = jetpt1;
  m_JetPtCut2 = jetpt2;
  
}

void Particles::Print(std::string field){
  if(m_debug){
    std::cout<<"DEBUG:: "+field<<std::endl;
  }
}

void Particles::PrintOut(int value){
  if(m_debug){
    std::cout<<value;
  }
}

void Particles::PrintOut(std::string field){
  if(m_debug){
    std::cout<<field;
  }
}

void Particles::PrintOut(float value){
  if(m_debug){
    std::cout<<value;
  }
}

void Particles::PrintOut(double value){
  if(m_debug){
    std::cout<<value;
  }
}

void Particles::PrintEndl(){
  if(m_debug){
    std::cout<<""<<std::endl;
  }
}

double Particles::DeltaR(double eta1,
			 double phi1,
			 double eta2,
			 double phi2){
  
  double dr = 0;
  double Deta = eta1-eta2;
  double Dphi = phi1-phi2;
  
  if( fabs(Dphi)>m_pi ) {
    if( Dphi>0 ){
      Dphi = 2*m_pi-Dphi;
    }
    else{
      Dphi = 2*m_pi+Dphi;
    }
  }
  
  dr = sqrt( pow(Deta,2)+pow(Dphi,2) );
  
  return dr;
  
}

double Particles::D0Smearing(double d0,
			     int lepidx,
			     int nBL,
			     double eta,
			     double pT){
  
  double d0smeared=d0;
  if(nBL < 0) { nBL = 0; }
  if(nBL > 2) { nBL = 2; }
  
  double sinTheta = 1./TMath::CosH(eta);
  double p_quant = 1./sqrt(pow(pT,2)*sinTheta)/m_GeV;
  int Xbin = 0;
  int Ybin = 0;
  
  double sigma = 0;
  if(nBL==0){
    Xbin  = m_smearD0_0->GetXaxis()->FindFixBin(eta);
    Ybin  = m_smearD0_0->GetYaxis()->FindFixBin(p_quant);
    sigma = m_smearD0_0->GetBinContent(Xbin, Ybin);
  }
  else if(nBL==1){
    Xbin  = m_smearD0_1->GetXaxis()->FindFixBin(eta);
    Ybin  = m_smearD0_1->GetYaxis()->FindFixBin(p_quant);
    sigma = m_smearD0_1->GetBinContent(Xbin, Ybin);
  }
  else if(nBL==2){
    Xbin  = m_smearD0_2->GetXaxis()->FindFixBin(eta);
    Ybin  = m_smearD0_2->GetYaxis()->FindFixBin(p_quant);
    sigma = m_smearD0_2->GetBinContent(Xbin, Ybin);
  }
  m_rand->SetSeed(EventNumber+100*lepidx);
  d0smeared += m_rand->Gaus(0,sigma);
  
  PrintOut("d0_orig = "); PrintOut(d0); PrintEndl();
  PrintOut("                         corr. = ");   PrintOut(d0smeared-d0);
  PrintOut(" -> d0_corr. = "); PrintOut(d0smeared); PrintEndl();
  
  return d0smeared;
  
}

double Particles::Z0Smearing(double z0,
			     int lepidx,
			     int nBL,
			     double eta,
			     double pT){
  
  double z0smeared=z0;
  if(nBL < 0) { nBL = 0; }
  if(nBL > 2) { nBL = 2; }

  double sinTheta = 1./cosh(eta);
  double p_quant = 1./sqrt(pow(pT,2)*sinTheta)/m_GeV;
  int Xbin = 0;
  int Ybin = 0;
  
  float sigma = 0;
  if(nBL==0){
    Xbin  = m_smearZ0_0->GetXaxis()->FindFixBin(eta);
    Ybin  = m_smearZ0_0->GetYaxis()->FindFixBin(p_quant);
    sigma = m_smearZ0_0->GetBinContent(Xbin, Ybin);
  }
  else if(nBL==1){
    Xbin  = m_smearZ0_1->GetXaxis()->FindFixBin(eta);
    Ybin  = m_smearZ0_1->GetYaxis()->FindFixBin(p_quant);
    sigma = m_smearZ0_1->GetBinContent(Xbin, Ybin);
  }
  else if(nBL==2){
    Xbin  = m_smearZ0_1->GetXaxis()->FindFixBin(eta);
    Ybin  = m_smearZ0_1->GetYaxis()->FindFixBin(p_quant);
    sigma = m_smearZ0_2->GetBinContent(Xbin, Ybin);
  }
  
  m_rand->SetSeed(EventNumber+100*lepidx);
  z0smeared += m_rand->Gaus(0,sigma);
  PrintOut("                         z0_orig = "); PrintOut(z0); PrintEndl();
  PrintOut("                         corr. = ");   PrintOut(z0smeared-z0);
  PrintOut(" -> z0_corr. = "); PrintOut(z0smeared); PrintEndl();
  return z0smeared;
  
}
