#include "Particles.h"

/* particles vectors*/
std::vector<int> Particles::getLep_Index() { return Lep_Index; }

std::vector<int> Particles::getLep_Id() { return Lep_Id; }

std::vector<int> Particles::getLep_Author() { return Lep_Author; }

std::vector<int> Particles::getLep_charge() { return Lep_charge; }

std::vector<double> Particles::getLep_d0_unbias() { return Lep_d0_unbias; }

std::vector<double> Particles::getLep_scf() { return Lep_scf; }

std::vector<double> Particles::getLep_resoscf() { return Lep_resoscf; }

std::vector<double> Particles::getLep_z0_unbias() { return Lep_z0_unbias;}

std::vector<double> Particles::getLep_E() { return Lep_E; }

std::vector<double> Particles::getLep_pt() { return Lep_pt; }

std::vector<double> Particles::getLep_eta() { return Lep_eta; }

std::vector<double> Particles::getLep_phi() { return Lep_phi; }

std::vector<double> Particles::getLep_Ereso() { return Lep_Ereso; }

std::vector<int> Particles::getLep_SAgood() { return Lep_SAgood; }

std::vector<int> Particles::getLep_CALOgood() { return Lep_CALOgood; }

std::vector<int> Particles::getLep_numTrack() { return Lep_numTrack; }

/* muons counters */
int Particles::getStacoAthCBTag(){ return StacoAthCBTag; }

int Particles::getStacoAthSA(){ return StacoAthSA; }

int Particles::getStacoPtCBTag(){ return StacoPtCBTag; }

int Particles::getStacoPtSA(){ return StacoPtSA; }

int Particles::getStacoBl(){ return StacoBl; }

int Particles::getStacoPxl(){ return StacoPxl; }

int Particles::getStacoSCT(){ return StacoSCT; }

int Particles::getStacoHol(){ return StacoHol; }

int Particles::getStacoTRT(){ return StacoTRT; }

int Particles::getStacoD0Z0(){ return StacoD0Z0; }

int Particles::getStacoMS(){ return StacoMS; }

int Particles::getCaloAth(){ return CaloAth; }

int Particles::getCaloPt(){ return CaloPt; }

int Particles::getCaloBl(){ return CaloBl; }

int Particles::getCaloPxl(){ return CaloPxl; }

int Particles::getCaloSCT(){ return CaloSCT; }

int Particles::getCaloHol(){ return CaloHol; }

int Particles::getCaloTRT(){ return CaloTRT; }

int Particles::getCaloD0Z0(){ return CaloD0Z0; }

int Particles::getOverlapCalo(){ return OverlapCalo; }

int Particles::getOverlapSTSA(){ return OverlapSTSA; }

int Particles::getNmuCBsel(){ return m_nmuCBsel; }

int Particles::getNmuSAsel(){ return m_nmuSAsel; }

int Particles::getNmuCALOsel(){ return m_nmuCALOsel; }

int Particles::getNmuCBSAsel(){ return m_nmuCBSAsel; }

int Particles::getNmuSel(){ return m_nmusel; }

/* muons vectors */
std::vector<int> Particles::getMu_Index(){ return Mu_Index; }

std::vector<int> Particles::getMu_Id(){ return Mu_Id; }

std::vector<int> Particles::getMu_Author(){ return Mu_Author; }

std::vector<int> Particles::getMu_charge(){ return Mu_charge; }

std::vector<double> Particles::getMu_d0_unbias(){ return Mu_d0_unbias; }

std::vector<double> Particles::getMu_scf(){ return Mu_scf; }

std::vector<double> Particles::getMu_resoscf(){ return Mu_resoscf; }

std::vector<double> Particles::getMu_z0_unbias(){ return Mu_z0_unbias; }

std::vector<double> Particles::getMu_pt(){ return Mu_pt; }

std::vector<double> Particles::getMu_eta(){ return Mu_eta; }

std::vector<double> Particles::getMu_phi(){ return Mu_phi; }

std::vector<int> Particles::getMu_isSAmu(){ return Mu_isSAmu; }

std::vector<int> Particles::getMu_isSTmu(){ return Mu_isSTmu; }

std::vector<int> Particles::getMu_BLYgood(){ return Mu_BLYgood; }

std::vector<int> Particles::getMu_PXLgood(){ return Mu_PXLgood; }

std::vector<int> Particles::getMu_SCTgood(){ return Mu_SCTgood; }

std::vector<int> Particles::getMu_HOLgood(){ return Mu_HOLgood; }

std::vector<int> Particles::getMu_TRTgood(){ return Mu_TRTgood; }

std::vector<int> Particles::getMu_D0good(){ return Mu_D0good; }

std::vector<int> Particles::getMu_Z0good(){ return Mu_Z0good; }

std::vector<int> Particles::getMu_MCPgood(){ return Mu_MCPgood; }

std::vector<int> Particles::getMu_MSgood(){ return Mu_MSgood; }

//std::vector<int> Particles::getMu_OurMSgood(){ return Mu_OurMSgood; }

std::vector<int> Particles::getMu_CBTAGgood(){ return Mu_CBTAGgood; }

std::vector<int> Particles::getMu_SAgood(){ return Mu_SAgood; }

std::vector<int> Particles::getMu_CALOgood(){ return Mu_CALOgood; }

std::vector<int> Particles::getMu_n_exblh(){ return Mu_n_exblh; }

std::vector<int> Particles::getMu_n_blh(){ return Mu_n_blh; }

std::vector<int> Particles::getMu_n_pxlh(){ return Mu_n_pxlh; }

std::vector<int> Particles::getMu_n_pxlds(){ return Mu_n_pxlds; }

std::vector<int> Particles::getMu_n_pxlhol(){ return Mu_n_pxlhol; }

std::vector<int> Particles::getMu_n_scth(){ return Mu_n_scth; }

std::vector<int> Particles::getMu_n_sctds(){ return Mu_n_sctds; }

std::vector<int> Particles::getMu_n_scthol(){ return Mu_n_scthol; }

std::vector<int> Particles::getMu_n_hol(){ return Mu_n_hol; }

std::vector<int> Particles::getMu_n_trth(){ return Mu_n_trth; }

std::vector<int> Particles::getMu_n_trto(){ return Mu_n_trto; }

std::vector<int> Particles::getMu_n_trtho(){ return Mu_n_trtho; }

std::vector<int> Particles::getMu_n_mdth(){ return Mu_n_mdth; }

std::vector<int> Particles::getMu_n_mdtBIh(){ return Mu_n_mdtBIh; }

std::vector<int> Particles::getMu_n_mdtBMh(){ return Mu_n_mdtBMh; }

std::vector<int> Particles::getMu_n_mdtBOh(){ return Mu_n_mdtBOh; }

std::vector<int> Particles::getMu_n_mdtBEEh(){ return Mu_n_mdtBEEh; }

std::vector<int> Particles::getMu_n_mdtBIS78h(){ return Mu_n_mdtBIS78h; }

std::vector<int> Particles::getMu_n_mdtEIh(){ return Mu_n_mdtEIh; }

std::vector<int> Particles::getMu_n_mdtEMh(){ return Mu_n_mdtEMh; }

std::vector<int> Particles::getMu_n_mdtEOh(){ return Mu_n_mdtEOh; }

std::vector<int> Particles::getMu_n_mdtEEh(){ return Mu_n_mdtEEh; }

std::vector<int> Particles::getMu_n_cscEtah(){ return Mu_n_cscEtah; }

std::vector<int> Particles::getMu_n_cscPhih(){ return Mu_n_cscPhih; }

std::vector<int> Particles::getMu_n_rpcEtah(){ return Mu_n_rpcEtah; }

std::vector<int> Particles::getMu_n_rpcPhih(){ return Mu_n_rpcPhih; }

std::vector<int> Particles::getMu_n_tgcEtah(){ return Mu_n_tgcEtah; }

std::vector<int> Particles::getMu_n_tgcPhih(){ return Mu_n_tgcPhih; }

std::vector<double> Particles::getMu_d0(){ return Mu_d0; }

std::vector<double> Particles::getMu_errd0(){ return Mu_errd0; }

std::vector<double> Particles::getMu_sigd0_unbias(){ return Mu_sigd0_unbias; }

std::vector<double> Particles::getMu_z0(){ return Mu_z0; }

std::vector<double> Particles::getMu_p(){ return Mu_p; }

std::vector<double> Particles::getMu_px(){ return Mu_px; }

std::vector<double> Particles::getMu_py(){ return Mu_py; }

std::vector<double> Particles::getMu_pz(){ return Mu_pz; }

std::vector<double> Particles::getMu_pid(){ return Mu_pid; }

std::vector<double> Particles::getMu_E(){ return Mu_E; }

std::vector<double> Particles::getMu_theta(){ return Mu_theta; }

std::vector<double> Particles::getMu_theta_id(){ return Mu_theta_id; }

std::vector<double> Particles::getMu_eta_id(){ return Mu_eta_id; }

std::vector<double> Particles::getMu_phi_id(){ return Mu_phi_id; }

std::vector<double> Particles::getMu_isocalo20(){ return Mu_isocalo20; }

std::vector<double> Particles::getMu_isocalo30(){ return Mu_isocalo30; }

std::vector<double> Particles::getMu_isocalo40(){ return Mu_isocalo40; }

std::vector<double> Particles::getMu_isotrk20(){ return Mu_isotrk20; }

std::vector<double> Particles::getMu_isotrk30(){ return Mu_isotrk30; }

std::vector<double> Particles::getMu_isotrk40(){ return Mu_isotrk40; }

std::vector<double> Particles::getMu_cov_d0(){ return Mu_cov_d0; }

std::vector<double> Particles::getMu_cov_z0(){ return Mu_cov_z0; }

std::vector<double> Particles::getMu_cov_phi(){ return Mu_cov_phi; }

std::vector<double> Particles::getMu_cov_theta(){ return Mu_cov_theta; }

std::vector<double> Particles::getMu_cov_qoverp(){ return Mu_cov_qoverp; }

std::vector<double> Particles::getMu_cov_d0_z0(){ return Mu_cov_d0_z0; }

std::vector<double> Particles::getMu_cov_d0_phi(){ return Mu_cov_d0_phi; }

std::vector<double> Particles::getMu_cov_d0_theta(){ return Mu_cov_d0_theta; }

std::vector<double> Particles::getMu_cov_d0_qoverp(){ return Mu_cov_d0_qoverp; }

std::vector<double> Particles::getMu_cov_z0_phi(){ return Mu_cov_z0_phi; }

std::vector<double> Particles::getMu_cov_z0_theta(){ return Mu_cov_z0_theta; }

std::vector<double> Particles::getMu_cov_z0_qoverp(){ return Mu_cov_z0_qoverp; }

std::vector<double> Particles::getMu_cov_phi_theta(){ return Mu_cov_phi_theta; }

std::vector<double> Particles::getMu_cov_phi_qoverp(){ return Mu_cov_phi_qoverp; }

std::vector<double> Particles::getMu_cov_theta_qoverp(){ return Mu_cov_theta_qoverp; }

std::vector<double> Particles::getMu_pt_truth(){ return Mu_pt_truth; }

std::vector<double> Particles::getMu_eta_truth(){ return Mu_eta_truth; }

std::vector<double> Particles::getMu_phi_truth(){ return Mu_phi_truth; }

std::vector<double> Particles::getMu_E_truth(){ return Mu_E_truth; }

std::vector<int> Particles::getMu_mother_truth(){ return Mu_mother_truth; }

std::vector<int> Particles::getMu_motherbcode_truth(){ return Mu_motherbcode_truth; }

/* electrons counters */
int Particles::getElAuthor(){ return ElecAuthor; }

int Particles::getElLoosePP(){ return ElecLoosePP; }

int Particles::getElEta(){ return ElecEta; }

int Particles::getElEt(){ return ElecEt; }

int Particles::getElOQ(){ return ElecOQ; }

int Particles::getElZ0(){ return ElecZ0; }

int Particles::getElOverlap(){ return ElecOverlap; }

int Particles::getElOverlapCl(){ return ElecOverlapCl; }

int Particles::getElMuOverlap(){ return ElecMuOverlap; }

int Particles::getNelecSel(){ return m_nelecsel; }

/* electrons vectors*/
std::vector<int> Particles::getEl_Index(){ return El_Index; }

std::vector<int> Particles::getEl_Id(){ return El_Id; }

std::vector<int> Particles::getEl_Author(){ return El_Author; }

std::vector<int> Particles::getEl_charge(){ return El_charge; }

std::vector<double> Particles::getEl_d0_unbias(){ return El_d0_unbias; }

std::vector<double> Particles::getEl_scf(){ return El_scf; }

std::vector<double> Particles::getEl_z0_unbias(){ return El_z0_unbias; }

std::vector<double> Particles::getEl_E(){ return El_E; }

std::vector<double> Particles::getEl_pt(){ return El_pt; }

std::vector<double> Particles::getEl_eta(){ return El_eta; }

std::vector<double> Particles::getEl_phi(){ return El_phi; }

std::vector<double> Particles::getEl_Ereso(){ return El_Ereso; }

std::vector<double> Particles::getEl_d0(){ return El_d0; }

std::vector<double> Particles::getEl_sigd0_unbias(){ return El_sigd0_unbias; }

std::vector<double> Particles::getEl_z0(){ return El_z0; }

std::vector<double> Particles::getEl_Eclraw(){ return El_Eclraw; }

std::vector<double> Particles::getEl_p(){ return El_p; }

std::vector<double> Particles::getEl_px(){ return El_px; }

std::vector<double> Particles::getEl_py(){ return El_py; }

std::vector<double> Particles::getEl_pz(){ return El_pz; }

std::vector<double> Particles::getEl_pid(){ return El_pid; }

std::vector<double> Particles::getEl_trkpt(){ return El_trkpt; }

std::vector<double> Particles::getEl_Et(){ return El_Et; }

std::vector<double> Particles::getEl_theta(){ return El_theta; }

std::vector<double> Particles::getEl_etaS2(){ return El_etaS2; }

std::vector<double> Particles::getEl_etacl(){ return El_etacl; }

std::vector<double> Particles::getEl_etap(){ return El_etap; }

std::vector<double> Particles::getEl_phiS2(){ return El_phiS2; }

std::vector<double> Particles::getEl_phicl(){ return El_phicl; }

std::vector<double> Particles::getEl_isocalo20(){ return El_isocalo20; }

std::vector<double> Particles::getEl_isocalo30(){ return El_isocalo30; }

std::vector<double> Particles::getEl_isocalo40(){ return El_isocalo40; }

std::vector<double> Particles::getEl_topoisocalo20(){ return El_topoisocalo20; }

std::vector<double> Particles::getEl_topoisocalo30(){ return El_topoisocalo30; }

std::vector<double> Particles::getEl_topoisocalo40(){ return El_topoisocalo40; }

std::vector<double> Particles::getEl_isotrk20(){ return El_isotrk20; }

std::vector<double> Particles::getEl_isotrk30(){ return El_isotrk30; }

std::vector<double> Particles::getEl_isotrk40(){ return El_isotrk40; }

std::vector<double> Particles::getEl_EDmedian(){ return El_EDmedian; }

std::vector<double> Particles::getEl_pt_truth(){ return El_pt_truth; }

std::vector<double> Particles::getEl_eta_truth(){ return El_eta_truth; }

std::vector<double> Particles::getEl_phi_truth(){ return El_phi_truth; }

std::vector<double> Particles::getEl_E_truth(){ return El_E_truth; }

std::vector<int> Particles::getEl_mother_truth(){ return El_mother_truth; }

std::vector<int> Particles::getEl_motherbcode_truth(){ return El_motherbcode_truth; }

std::vector<double> Particles::getEl_cov_d0(){ return El_cov_d0; }

std::vector<double> Particles::getEl_cov_z0(){ return El_cov_z0; }

std::vector<double> Particles::getEl_cov_phi(){ return El_cov_phi; }

std::vector<double> Particles::getEl_cov_theta(){ return El_cov_theta; }

std::vector<double> Particles::getEl_cov_d0_z0(){ return El_cov_d0_z0; }

std::vector<double> Particles::getEl_cov_d0_phi(){ return El_cov_d0_phi; }

std::vector<double> Particles::getEl_cov_d0_theta(){ return El_cov_d0_theta; }

std::vector<double> Particles::getEl_cov_z0_phi(){ return El_cov_z0_phi; }

std::vector<double> Particles::getEl_cov_z0_theta(){ return El_cov_z0_theta; }

std::vector<double> Particles::getEl_cov_phi_theta(){ return El_cov_phi_theta; }

/* jet counters */
int Particles::getJetPt() { return JetPt; }

int Particles::getJetEta() { return JetEta; }

int Particles::getJetPU() { return JetPU; }

int Particles::getJetClean() { return JetClean; }

int Particles::getJetElOverlap() { return JetElOverlap; }

int Particles::getNjetSel(){ return m_njetsel; }

int Particles::getNjet30(){ return m_njet30; }

/* jet vectors */
std::vector<int> Particles::getJet_Index(){ return Jt_Index; }

std::vector<double> Particles::getJet_pt(){ return Jt_pt; }

std::vector<double> Particles::getJet_eta(){ return Jt_eta; }

std::vector<double> Particles::getJet_phi(){ return Jt_phi; }

std::vector<double> Particles::getJet_E(){ return Jt_E; }

std::vector<double> Particles::getJet_m(){ return Jt_m; }

std::vector<double> Particles::getJet_jvtx(){ return Jt_jvtx; }

/* tau counters */
int Particles::getTauPt(){ return TauPt; }

int Particles::getTauEta(){ return TauEta; }

int Particles::getTauNTrk(){ return TauNTrk; }

int Particles::getTauCharge(){ return TauCharge; }

int Particles::getTauJetBDT(){ return TauJetBDT; }

int Particles::getTauElVeto(){ return TauElVeto; }

int Particles::getTauMuVeto(){ return TauMuVeto; }

int Particles::getTauTauOverlap(){ return TauTauOverlap; }

int Particles::getTauMuOverlap(){ return TauMuOverlap; }

int Particles::getTauElOverlap(){ return TauElOverlap; }

int Particles::getNtauSel(){ return m_ntausel; }

/* tau vectors */
std::vector<int> Particles::getTau_Index() { return Ta_Index; }

std::vector<int> Particles::getTau_Id() { return Ta_Id; }

std::vector<int> Particles::getTau_Author() { return Ta_Author; }

std::vector<double> Particles::getTau_Et() { return Ta_Et; }

std::vector<double> Particles::getTau_pt() { return Ta_pt; }

std::vector<double> Particles::getTau_m() { return Ta_m; }

std::vector<double> Particles::getTau_eta() { return Ta_eta; }

std::vector<double> Particles::getTau_phi() { return Ta_phi; }

std::vector<int> Particles::getTau_charge() { return Ta_charge; }

std::vector<double> Particles::getTau_likelihood() { return Ta_likelihood; }

std::vector<double> Particles::getTau_SafeLikelihood() { return Ta_SafeLikelihood; }

std::vector<double> Particles::getTau_electronVetoLoose() { return Ta_electronVetoLoose; }

std::vector<double> Particles::getTau_electronVetoMedium() { return Ta_electronVetoMedium; }

std::vector<double> Particles::getTau_electronVetoTight() { return Ta_electronVetoTight; }

std::vector<double> Particles::getTau_muonVeto() { return Ta_muonVeto; }

std::vector<double> Particles::getTau_tauLlhLoose() { return Ta_tauLlhLoose; }

std::vector<double> Particles::getTau_tauLlhMedium() { return Ta_tauLlhMedium; }

std::vector<double> Particles::getTau_tauLlhTight() { return Ta_tauLlhTight; }

std::vector<double> Particles::getTau_JetBDTSigLoose() { return Ta_JetBDTSigLoose; }

std::vector<double> Particles::getTau_JetBDTSigMedium() { return Ta_JetBDTSigMedium; }

std::vector<double> Particles::getTau_JetBDTSigTight() { return Ta_JetBDTSigTight; }

std::vector<double> Particles::getTau_EleBDTLoose() { return Ta_EleBDTLoose; }

std::vector<double> Particles::getTau_EleBDTMedium() { return Ta_EleBDTMedium; }

std::vector<double> Particles::getTau_EleBDTTight() { return Ta_EleBDTTight; }

std::vector<int> Particles::getTau_numTrack(){ return Ta_numTrack; }

std::vector<double> Particles::getTau_BDTJetScore(){ return Ta_BDTJetScore; }

std::vector<double> Particles::getTau_BDTEleScore(){ return Ta_BDTEleScore; }
