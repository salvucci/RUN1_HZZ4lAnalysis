#include "Particles.h"
#include "../DPDVar/DPDElectronVariables.h"
#include "Muon.cxx"

void Particles::ResetElectronQualityFlags(){
  
  isGoodAuthorElec   = false;
  isGoodLoosePPElec  = false;
  isGoodEtaElec      = false;
  isGoodEtElec       = false;
  isGoodOQElec       = false;
  isGoodZ0Elec       = false;
  isGoodElectron     = false;
}

void Particles::CheckingElectrons(bool isVertexGood,
				  bool isTrigGood,
				  int nPV){
  if(m_year==m_YEAR_2011)
    CheckingElectrons2011(isVertexGood,isTrigGood);
  
  else if(m_year==m_YEAR_2012)
    CheckingElectrons2012(isVertexGood,isTrigGood,nPV);
  
}

void Particles::CheckingElectrons2011(bool isVertexGood,
				      bool isTrigGood){
  
  Print("                   Particles:: Checking electrons (2011 reconstruction) ... ");
  
  int nelec = el_GSF_n;
  m_nelecsel = 0;
  
  for(int i=0; i<nelec; i++){
    
    isGoodAuthorElec=false;
    if(el_GSF_author->at(i)==1 || el_GSF_author->at(i)==3){
      isGoodAuthorElec = true;
    }
    
    double Et_clS2 = el_GSF_cl_E->at(i)/TMath::CosH(el_GSF_etas2->at(i));
    //double rEtHad  = el_GSF_Ethad->at(i)/Et_clS2;
    //double rEtHad1 = el_GSF_Ethad1->at(i)/Et_clS2;
    //double DEmaxs1 = (el_GSF_emaxs1->at(i)-el_GSF_Emax2->at(i))/(el_GSF_emaxs1->at(i)+el_GSF_Emax2->at(i));
    
    isGoodLoosePPElec=false;
    //isLoosePlusPlusH4l
    //if( m_ElId2011->passH4l2011(el_GSF_etas2->at(i),
    //				Et_clS2,
    //				rEtHad,
    //				rEtHad1,
    //				el_GSF_reta->at(i),
    //				el_GSF_weta2->at(i),
    //				el_GSF_f1->at(i),
    //				el_GSF_wstot->at(i),
    //				DEmaxs1,
    //				el_GSF_deltaeta1->at(i),
    //				el_GSF_nSiHits->at(i),
    //				el_GSF_nSCTOutliers->at(i),
    //				el_GSF_nPixHits->at(i),
    //				el_GSF_nPixelOutliers->at(i),
    //				false,
    //				false) ){
    //  isGoodLoosePPElec=true;
    //}
    m_Taccept=m_ElId2011->accept(el_GSF_etas2->at(i),//
				 fabs(Et_clS2),//
				 el_GSF_E237->at(i),
				 el_GSF_E277->at(i),
				 el_GSF_Ethad1->at(i),
				 el_GSF_Ethad->at(i),
				 el_GSF_ws3->at(i),
				 el_GSF_weta2->at(i),
				 el_GSF_f1->at(i),
				 el_GSF_Emax2->at(i),    
				 el_GSF_emaxs1->at(i),
				 el_GSF_Emins1->at(i),
				 el_GSF_wstot->at(i),
				 el_GSF_fside->at(i),
				 el_GSF_f3->at(i),
				 el_GSF_nBLHits->at(i),
				 el_GSF_nBLayerOutliers->at(i),
				 el_GSF_nPixHits->at(i),
				 el_GSF_nPixelOutliers->at(i),
				 el_GSF_nSCTHits->at(i),
				 el_GSF_nSCTOutliers->at(i),
				 el_GSF_nTRTHighTHits->at(i),
				 el_GSF_nTRTHighTOutliers->at(i),
				 el_GSF_nTRTHits->at(i),
				 el_GSF_nTRTOutliers->at(i),
				 1.0,//el_GSF_nTRTXenonHits->at(i),
				 el_GSF_trackd0_physics->at(i),
				 fabs(el_GSF_deltaeta1->at(i)),
				 el_GSF_deltaphi2->at(i),
				 1.,              
				 el_GSF_expectHitInBLayer->at(i),
				 m_Amb);
    
    isGoodLoosePPElec=(bool)m_Taccept;
    
    isGoodEtaElec=false;
    if( fabs(el_GSF_cl_eta->at(i))<2.47 ){
      isGoodEtaElec=true;
    }
    
    isGoodEtElec=false;
    double ECorr = 0;
    if(m_type==m_DATA){
      //double calib = m_El_ERescaler->applyMCCalibration(el_GSF_cl_eta->at(i),
      //el_GSF_cl_E->at(i)/TMath::CosH(el_GSF_tracketa->at(i)),
      //egRescaler::EnergyRescalerUpgrade::Electron);
      
      //ECorr = m_El_ERescaler->applyEnergyCorrection(el_GSF_cl_eta->at(i),
      //el_GSF_cl_E->at(i)*fabs(calib),
      //egRescaler::EnergyRescalerUpgrade::Electron, 
      //egRescaler::EnergyRescalerUpgrade::Nominal,
      //1.0,
      //RunNumber);
      if(isGoodAuthorElec){
	m_El_ERescaler->setRandomSeed(EventNumber+100*i);
	ECorr = m_El_ERescaler->getCorrectedEnergy(RunNumber,
						   m_DataType,
						   AtlasRoot::egammaEnergyCorrectionTool::ParticleInformation( el_GSF_rawcl_Es0->at(i),
													       el_GSF_rawcl_Es1->at(i),
													       el_GSF_rawcl_Es2->at(i),
													       el_GSF_rawcl_Es3->at(i),
													       el_GSF_cl_eta->at(i),
													       el_GSF_cl_phi->at(i),
													       el_GSF_tracketa->at(i),
													       el_GSF_cl_E->at(i),
													       el_GSF_cl_etaCalo->at(i),
													       el_GSF_cl_phiCalo->at(i) ),
						   m_ScaleVar,
						   m_Reso,
						   m_ResType,
						   1.0);
      }
      else{
	ECorr = el_GSF_cl_E->at(i);
      }
    }
    else if(m_UseCorr){
      //double calib = m_El_ERescaler->applyMCCalibration(el_GSF_cl_eta->at(i),
      //el_GSF_cl_E->at(i)/TMath::CosH(el_GSF_tracketa->at(i)),
      //egRescaler::EnergyRescalerUpgrade::Electron);
      
      //m_El_ERescaler->SetRandomSeed(EventNumber+ 100*i);
      //double smear = m_El_ERescaler->getSmearingCorrection(el_GSF_cl_eta->at(i),
      //el_GSF_cl_E->at(i)*fabs(calib),
      //egRescaler::EnergyRescalerUpgrade::NOMINAL);
      
      //ECorr = el_GSF_cl_E->at(i)*fabs(calib)*fabs(smear);
      if(isGoodAuthorElec){
	m_El_ERescaler->setRandomSeed(EventNumber+100*i);
	ECorr = m_El_ERescaler->getCorrectedEnergy(RunNumber,
						   m_DataType,
						   AtlasRoot::egammaEnergyCorrectionTool::ParticleInformation( el_GSF_rawcl_Es0->at(i),
													       el_GSF_rawcl_Es1->at(i),
													       el_GSF_rawcl_Es2->at(i),
													       el_GSF_rawcl_Es3->at(i),
													       el_GSF_cl_eta->at(i),
													       el_GSF_cl_phi->at(i),
													       el_GSF_tracketa->at(i),
													       el_GSF_cl_E->at(i),
													       el_GSF_cl_etaCalo->at(i),
													       el_GSF_cl_phiCalo->at(i) ),
						   m_ScaleVar,
						   m_Reso,
						   m_ResType,
						   1.0);
      }
      else{
	ECorr = el_GSF_cl_E->at(i);
      }
    }
    else{
      ECorr = el_GSF_cl_E->at(i);
    }
    double Et = ECorr/TMath::CosH(el_GSF_tracketa->at(i));
    if( Et/m_GeV>m_ElecEtCut ){
      isGoodEtElec=true;
    }

    isGoodOQElec=false;
    if( (el_GSF_OQ->at(i)&1446)==0 ){
      isGoodOQElec=true;
    }

    isGoodZ0Elec=false;
    if( fabs(el_GSF_trackz0pvunbiased->at(i))<m_ElecZ0Cut ){
      isGoodZ0Elec=true;
    }
    
    PrintOut("                  GSF:: Eta = "); PrintOut(el_GSF_cl_eta->at(i));
    PrintOut(" Author = "); PrintOut(el_GSF_author->at(i)); 
    PrintOut(" ET = "); PrintOut(Et/m_GeV); 
    PrintOut(" LoosePP = "); PrintOut(isGoodLoosePPElec);
    PrintOut(" ObjQua = "); PrintOut(isGoodOQElec); 
    PrintOut(" isGoodZ0 = "); PrintOut(isGoodZ0Elec); PrintEndl();
    
    isGoodElectron=false;
    if( isGoodAuthorElec && isGoodLoosePPElec &&
	isGoodEtaElec && isGoodEtElec &&
	isGoodOQElec && isGoodZ0Elec ){
      
      isGoodElectron=true;
      PrintOut("                  GSF:: Good electron Found!!"); PrintEndl();
    }

    if(isGoodElectron){
      m_ElIndex.push_back(i);
    }
    
    if(isVertexGood && isTrigGood){
      if(isGoodAuthorElec){
	ElecAuthor++;
	if(isGoodLoosePPElec){
	  ElecLoosePP++;
	  if(isGoodEtaElec){
	    ElecEta++;
	    if(isGoodEtElec){
	      ElecEt++;
	      if(isGoodOQElec){
		ElecOQ++;
		if(isGoodZ0Elec){
		  ElecZ0++;
		}
	      }
	    }
	  }
	}
      }
    }
  }
  
  Print("                   Particles:: Removing electrons overlap");
  for(UInt_t id1=0; id1<m_ElIndex.size(); id1++){
    bool m_IseeOverlap=false;
    int el1=m_ElIndex.at(id1);
    
    for(UInt_t id2=0; id2<m_ElIndex.size(); id2++){
      int el2=m_ElIndex.at(id2);
      if(id1!=id2){
	if( el_GSF_trackqoverp->at(el1) == el_GSF_trackqoverp->at(el2) &&
            el_GSF_tracktheta->at(el1) == el_GSF_tracktheta->at(el2)   &&
            el_GSF_trackphi->at(el1) == el_GSF_trackphi->at(el2)       &&
            el_GSF_trackd0->at(el1) == el_GSF_trackd0->at(el2)         &&
            el_GSF_trackz0->at(el1) == el_GSF_trackz0->at(el2)          ){
	  
	  Print("                             Overlap Found -> choosing Highest Et ...");
	  if( el_GSF_Et->at(el1) < el_GSF_Et->at(el2) )
	    m_IseeOverlap=true;
	}
      }
    }
    if(!m_IseeOverlap)
      m_ElIndex_eeOverlap.push_back(el1);
    
    if(isVertexGood && isTrigGood && !m_IseeOverlap)
      ElecOverlap++;
  }
  
  Print("                   Particles:: Removing electrons-muons (CB or ST) overlap");
  bool isNot_emu_Overlap=true;
  for(UInt_t el=0; el<m_ElIndex_eeOverlap.size(); el++){
    isNot_emu_Overlap = CheckemuCBSTOverlap2011(el);
    if(isNot_emu_Overlap){
      m_ElIndex_emuOverlap.push_back(m_ElIndex_eeOverlap.at(el));
      }
    if(isVertexGood && isTrigGood && isNot_emu_Overlap){
      ElecMuOverlap++;
    }
  }
  
  Print("                   Particles:: Removing electrons-muons (Calo) overlap");
  m_nelecsel=(int)m_ElIndex_emuOverlap.size();
  for(UInt_t el=0; el<m_ElIndex_emuOverlap.size(); el++){
    CheckemuCaloOverlap2011(el);
  }
  
  Print("                   Particles:: Filling electrons vectors ...");
  for(UInt_t el=0; el<m_ElIndex_emuOverlap.size(); el++){
    int elidx=m_ElIndex_emuOverlap.at(el);
    
    double ECorr = 0;
    double Ereso = 0;
    if(m_type==m_DATA){
      //double calib = m_El_ERescaler->applyMCCalibration(el_GSF_cl_eta->at(elidx),
      //el_GSF_cl_E->at(elidx)/TMath::CosH(el_GSF_tracketa->at(elidx)),
      //egRescaler::EnergyRescalerUpgrade::Electron);
      
      //ECorr = m_El_ERescaler->applyEnergyCorrection(el_GSF_cl_eta->at(elidx)*fabs(calib),
      //el_GSF_cl_E->at(elidx),
      //egRescaler::EnergyRescalerUpgrade::Electron, 
      //egRescaler::EnergyRescalerUpgrade::Nominal,
      //1.0,
      //RunNumber);
      
      //Ereso = m_El_ERescaler->resolution( ECorr/m_GeV,
      //el_GSF_cl_eta->at(elidx),
      //true )*ECorr;
      m_El_ERescaler->setRandomSeed(EventNumber+100*elidx);
      ECorr = m_El_ERescaler->getCorrectedEnergy(RunNumber,
						 m_DataType,
						 AtlasRoot::egammaEnergyCorrectionTool::ParticleInformation( el_GSF_rawcl_Es0->at(elidx),
													     el_GSF_rawcl_Es1->at(elidx),
													     el_GSF_rawcl_Es2->at(elidx),
													     el_GSF_rawcl_Es3->at(elidx),
													     el_GSF_cl_eta->at(elidx),
													     el_GSF_cl_phi->at(elidx),
													     el_GSF_tracketa->at(elidx),
													     el_GSF_cl_E->at(elidx),
													     el_GSF_cl_etaCalo->at(elidx),
													     el_GSF_cl_phiCalo->at(elidx) ),
						 m_ScaleVar,
						 m_Reso,
						 m_ResType,
						 1.0);
      
    }
    else if(m_UseCorr){
      //double calib = m_El_ERescaler->applyMCCalibration(el_GSF_cl_eta->at(elidx),
      //el_GSF_cl_E->at(elidx)/TMath::CosH(el_GSF_tracketa->at(elidx)),
      //egRescaler::EnergyRescalerUpgrade::Electron);
      
      //m_El_ERescaler->SetRandomSeed(EventNumber+100*elidx);
      //double smear = m_El_ERescaler->getSmearingCorrection(el_GSF_cl_eta->at(elidx),
      //el_GSF_cl_E->at(elidx)*fabs(calib),
      //egRescaler::EnergyRescalerUpgrade::NOMINAL);
      
      //ECorr = el_GSF_cl_E->at(elidx)*fabs(calib)*fabs(smear);
      //Ereso = m_El_ERescaler->resolution( ECorr/m_GeV,
      //el_GSF_cl_eta->at(elidx),
      //true )*ECorr;
      m_El_ERescaler->setRandomSeed(EventNumber+100*elidx);
      ECorr = m_El_ERescaler->getCorrectedEnergy(RunNumber,
						 m_DataType,
						 AtlasRoot::egammaEnergyCorrectionTool::ParticleInformation( el_GSF_rawcl_Es0->at(elidx),
													     el_GSF_rawcl_Es1->at(elidx),
													     el_GSF_rawcl_Es2->at(elidx),
													     el_GSF_rawcl_Es3->at(elidx),
													     el_GSF_cl_eta->at(elidx),
													     el_GSF_cl_phi->at(elidx),
													     el_GSF_tracketa->at(elidx),
													     el_GSF_cl_E->at(elidx),
													     el_GSF_cl_etaCalo->at(elidx),
													     el_GSF_cl_phiCalo->at(elidx) ),
						 m_ScaleVar,
						 m_Reso,
						 m_ResType,
						 1.0);
    }
    else{
      ECorr = el_GSF_cl_E->at(elidx);
      //Ereso = m_El_ERescaler->resolution( el_GSF_cl_E->at(elidx)/m_GeV,
      //el_GSF_cl_eta->at(elidx),
      //true )*el_GSF_cl_E->at(elidx);
    }
    
    double scfId   = 1.;
    double scfReco = 1.;
    if(m_UseW){
      m_PileUp->SetRandomSeed(EventNumber+100*elidx);
      UInt_t RunNum = m_PileUp->GetRandomRunNumber(RunNumber);
      
      const Root::TResult &IdRes = m_El_SCFId->calculate(PATCore::ParticleDataType::Full,
							 RunNum,
							 el_GSF_cl_eta->at(elidx),
							 ECorr/TMath::CosH(el_GSF_tracketa->at(elidx)));
      scfId = IdRes.getScaleFactor();

      const Root::TResult &RecoRes = m_El_SCFReco->calculate(PATCore::ParticleDataType::Full,
							     RunNum,
							     el_GSF_cl_eta->at(elidx),
							     ECorr/TMath::CosH(el_GSF_tracketa->at(elidx)));
      
      scfReco = RecoRes.getScaleFactor();
    }
    
    Lep_Index.push_back( elidx );
    Lep_Id.push_back( -m_IdElectron*el_GSF_charge->at(elidx) );
    Lep_Author.push_back( el_GSF_author->at(elidx) );
    Lep_charge.push_back( el_GSF_charge->at(elidx) );
    Lep_d0_unbias.push_back( el_GSF_trackd0pvunbiased->at(elidx) );
    Lep_scf.push_back( scfId*scfReco );
    Lep_resoscf.push_back( 0 );
    Lep_z0_unbias.push_back( el_GSF_trackz0pvunbiased->at(elidx) );
    Lep_E.push_back( ECorr/m_GeV );
    Lep_pt.push_back( fabs( sqrt( pow(ECorr/m_GeV,2)
				   -pow(m_Melec,2))/
			     TMath::CosH(el_GSF_tracketa->at(elidx)) ) );
    Lep_eta.push_back( el_GSF_tracketa->at(elidx) );
    Lep_phi.push_back( el_GSF_trackphi->at(elidx) );
    Lep_Ereso.push_back( Ereso );
    Lep_SAgood.push_back( false );
    Lep_CALOgood.push_back( false );
    Lep_numTrack.push_back( 0 );

    if(!m_MinParInfo){
      El_Index.push_back( elidx );
      El_Id.push_back( -m_IdElectron*el_GSF_charge->at(elidx) );
      El_Author.push_back( el_GSF_author->at(elidx) );
      El_charge.push_back( el_GSF_charge->at(elidx) );
      El_d0_unbias.push_back( el_GSF_trackd0pvunbiased->at(elidx) );
      El_scf.push_back( scfId*scfReco );
      El_z0_unbias.push_back( el_GSF_trackz0pvunbiased->at(elidx) );
      El_E.push_back( ECorr/m_GeV );
      El_pt.push_back( fabs( sqrt( pow(ECorr/m_GeV,2)
				     -pow(m_Melec,2))/
			       TMath::CosH(el_GSF_tracketa->at(elidx)) ) );
      El_eta.push_back( el_GSF_tracketa->at(elidx) );
      El_phi.push_back( el_GSF_trackphi->at(elidx) );
      El_Ereso.push_back( Ereso );
      El_d0.push_back( el_GSF_trackd0->at(elidx) );
      El_sigd0_unbias.push_back( el_GSF_tracksigd0pvunbiased->at(elidx) );
      El_z0.push_back( el_GSF_trackz0->at(elidx) );
      //El_Ecl.push_back( ECorr/m_GeV );
      TLorentzVector El_obj;
      El_obj.SetPtEtaPhiM(ECorr/m_GeV/
			  TMath::CosH(el_GSF_tracketa->at(elidx)),
			  el_GSF_tracketa->at(elidx),
			  el_GSF_trackphi->at(elidx),
			  m_Melec);
      El_p.push_back( El_obj.P() );
      El_px.push_back( El_obj.Px() );
      El_py.push_back( El_obj.Py() );
      El_pz.push_back( El_obj.Pz() );
      El_pid.push_back( fabs(1./el_GSF_trackqoverp->at(elidx)/m_GeV) );
      El_trkpt.push_back( el_GSF_trackpt->at(elidx)/m_GeV );
      El_Et.push_back( ECorr/m_GeV/
		       TMath::CosH(el_GSF_tracketa->at(elidx)) );
      El_theta.push_back( el_GSF_tracktheta->at(elidx) );
      El_etaS2.push_back( el_GSF_etas2->at(elidx) );
      El_etacl.push_back( el_GSF_cl_eta->at(elidx) );
      
      /*to remove under if
      if(m_type=="mc"){
	El_Es0rawcl.push_back( el_GSF_rawcl_Es0->at(elidx)/m_GeV );
	El_Es1rawcl.push_back( el_GSF_rawcl_Es1->at(elidx)/m_GeV );
	El_Es2rawcl.push_back( el_GSF_rawcl_Es2->at(elidx)/m_GeV );
	El_Es3rawcl.push_back( el_GSF_rawcl_Es3->at(elidx)/m_GeV );
	El_calHitShDep.push_back( el_GSF_calibHitsShowerDepth->at(elidx) );
	El_Ecb.push_back( el_GSF_E->at(elidx)/m_GeV );
	El_ptcb.push_back( el_GSF_pt->at(elidx)/m_GeV );
	El_pxcb.push_back( el_GSF_px->at(elidx)/m_GeV );
	El_pycb.push_back( el_GSF_py->at(elidx)/m_GeV );
	El_pzcb.push_back( el_GSF_pz->at(elidx)/m_GeV );
      }
      */
      
      El_phicl.push_back( el_GSF_cl_phi->at(elidx) );
      El_isocalo20.push_back( el_GSF_Etcone20->at(elidx)/m_GeV );
      El_isocalo30.push_back( el_GSF_Etcone30->at(elidx)/m_GeV );
      El_isocalo40.push_back( el_GSF_Etcone40->at(elidx)/m_GeV );
      El_isotrk20.push_back( el_GSF_ptcone20->at(elidx)/m_GeV );
      El_isotrk30.push_back( el_GSF_ptcone30->at(elidx)/m_GeV );
      El_isotrk40.push_back( el_GSF_ptcone40->at(elidx)/m_GeV );
      /*cov*/
      El_cov_d0.push_back( el_GSF_trackcov_d0->at(elidx) );
      El_cov_z0.push_back( el_GSF_trackcov_z0->at(elidx) );
      El_cov_phi.push_back( el_GSF_trackcov_phi->at(elidx) );
      El_cov_theta.push_back( el_GSF_trackcov_theta->at(elidx) );
      El_cov_d0_z0.push_back( el_GSF_trackcov_d0_z0->at(elidx) );
      El_cov_d0_phi.push_back( el_GSF_trackcov_d0_phi->at(elidx) );
      El_cov_d0_theta.push_back( el_GSF_trackcov_d0_theta->at(elidx) );
      El_cov_z0_phi.push_back( el_GSF_trackcov_z0_phi->at(elidx) );
      El_cov_z0_theta.push_back( el_GSF_trackcov_z0_theta->at(elidx) );
      El_cov_phi_theta.push_back( el_GSF_trackcov_phi_theta->at(elidx) );
      
      if(m_type==m_MC){
	El_pt_truth.push_back( el_GSF_truth_pt->at(elidx)/m_GeV );
	El_eta_truth.push_back( el_GSF_truth_eta->at(elidx) );
	El_phi_truth.push_back( el_GSF_truth_phi->at(elidx) );
	El_E_truth.push_back( el_GSF_truth_E->at(elidx)/m_GeV );
	El_mother_truth.push_back( el_GSF_truth_mothertype->at(elidx) );
	El_motherbcode_truth.push_back( el_GSF_truth_motherbarcode->at(elidx) );
      }
    }
  }
  Print("                   Particles:: Electrons checked!");
  
}

void Particles::CheckingElectrons2012(bool isVertexGood,
				      bool isTrigGood,
				      int nPV){
  
  Print("                   Particles:: Checking electrons (2012 reconstruction) ... ");
  
  int nelec  = el_n;
  m_nelecsel = 0;
  
  for(int i=0; i<nelec; i++){
    
    isGoodAuthorElec=false;
    if(el_author->at(i)==1 || el_author->at(i)==3){
      isGoodAuthorElec = true;
    }
    
    double Et_clS2 = el_cl_E->at(i)/TMath::CosH(el_etas2->at(i));
    double rEtHad  = el_Ethad->at(i)/Et_clS2;
    double rEtHad1 = el_Ethad1->at(i)/Et_clS2;
    double DEmaxs1 = fabs(el_emaxs1->at(i)+el_Emax2->at(i))>0. ? (el_emaxs1->at(i)-el_Emax2->at(i))/(el_emaxs1->at(i)+el_Emax2->at(i)) : 0.;
    double dpOverp = 0.;
    for(UInt_t el=0; el<el_refittedTrack_LMqoverp->at(i).size(); el++){
      if( el_refittedTrack_author->at(i).at(el)==4 ){
	dpOverp = 1-(el_trackqoverp->at(i)/el_refittedTrack_LMqoverp->at(i).at(el));
      }
    }
    int nSiOutliers=el_nSCTDeadSensors->at(i)+el_nPixelDeadSensors->at(i);
    bool expectBlayer=el_expectHitInBLayer->at(i);
    if(el_expectHitInBLayer->at(i)==-999) { expectBlayer=1; }
    Int_t convBit=el_isEM->at(i) & (0x1 << 1);
  
    isGoodLoosePPElec=false;
    m_Discriminant=m_ElId2012->calculate( el_etas2->at(i),
					  Et_clS2,
					  el_f3->at(i),
					  rEtHad,
					  rEtHad1,
					  el_reta->at(i),
					  el_weta2->at(i),
					  el_f1->at(i),
					  DEmaxs1,
					  el_deltaeta1->at(i),
					  el_trackd0pvunbiased->at(i),
					  el_TRTHighTOutliersRatio->at(i),
					  el_tracksigd0pvunbiased->at(i),
					  el_rphi->at(i),
					  dpOverp,
					  el_deltaphiRescaled->at(i),
					  Double_t(nPV) );
    
    m_Taccept=m_ElId2012->accept( m_Discriminant,
				  el_etas2->at(i),
				  Et_clS2,
				  el_nSiHits->at(i),
				  nSiOutliers,
				  el_nPixHits->at(i),
				  el_nPixelDeadSensors->at(i),
				  el_nBLHits->at(i),
				  el_nBLayerOutliers->at(i),
				  expectBlayer,
				  convBit,
				  Double_t(nPV) );
    
    isGoodLoosePPElec=(bool)m_Taccept;

    isGoodEtaElec=false;
    if( fabs(el_cl_eta->at(i))<2.47 ){
      isGoodEtaElec=true;
    }
    
    isGoodEtElec=false;
    double ECorr = 0;
    if(m_type==m_DATA){
      //ECorr = m_El_ERescaler->applyEnergyCorrection(el_cl_eta->at(i),
      //el_cl_E->at(i),
      //egRescaler::EnergyRescalerUpgrade::Electron, 
      //egRescaler::EnergyRescalerUpgrade::Nominal,
      //1.0,
      //RunNumber);
      if(isGoodAuthorElec){
	m_El_ERescaler->setRandomSeed(EventNumber+100*i);
	ECorr = m_El_ERescaler->getCorrectedEnergy(RunNumber,
						   m_DataType,
						   AtlasRoot::egammaEnergyCorrectionTool::ParticleInformation( el_rawcl_Es0->at(i),
													       el_rawcl_Es1->at(i),
													       el_rawcl_Es2->at(i),
													       el_rawcl_Es3->at(i),
													       el_cl_eta->at(i),
													       el_cl_phi->at(i),
													       el_tracketa->at(i),
													       el_cl_E->at(i),
													       el_cl_etaCalo->at(i),
													       el_cl_phiCalo->at(i) ),
						   m_ScaleVar,
						   m_Reso,
						   m_ResType,
						   1.0);
      }
      else{
	ECorr = el_cl_E->at(i);
      }
    }
    else{
      if(m_UseCorr){
	//m_El_ERescaler->SetRandomSeed(EventNumber+100*i);
	//double smear = m_El_ERescaler->getSmearingCorrection(el_cl_eta->at(i),
	//el_cl_E->at(i),
	//egRescaler::EnergyRescalerUpgrade::NOMINAL);
        
	//ECorr = el_cl_E->at(i)*fabs(smear);
	if(isGoodAuthorElec){
	  m_El_ERescaler->setRandomSeed(EventNumber+100*i);
	  ECorr = m_El_ERescaler->getCorrectedEnergy(RunNumber,
						     m_DataType,
						     AtlasRoot::egammaEnergyCorrectionTool::ParticleInformation( el_rawcl_Es0->at(i),
														 el_rawcl_Es1->at(i),
														 el_rawcl_Es2->at(i),
														 el_rawcl_Es3->at(i),
														 el_cl_eta->at(i),
														 el_cl_phi->at(i),
														 el_tracketa->at(i),
														 el_cl_E->at(i),
														 el_cl_etaCalo->at(i),
														 el_cl_phiCalo->at(i) ),
						     m_ScaleVar,
						     m_Reso,
						     m_ResType,
						     1.0);
	}
	else{
	  ECorr = el_cl_E->at(i);
	}
      }
      else{
	ECorr = el_cl_E->at(i);
      }
    }
    double Et = ECorr/TMath::CosH(el_tracketa->at(i));
    if( Et/m_GeV>m_ElecEtCut ){
      isGoodEtElec=true;
    }
    
    isGoodOQElec=false;
    if( (el_OQ->at(i)&1446)==0 ){
      isGoodOQElec=true;
    }
    
    double z0_v = 0;
    if(m_type==m_DATA){
      z0_v = el_trackz0pvunbiased->at(i);
    }
    else{
      if(m_UseCorr){
	z0_v = Z0Smearing(el_trackz0pvunbiased->at(i),
			  i,el_nBLHits->at(i),
			  el_cl_eta->at(i),Et);
      }
      else{
	z0_v = el_trackz0pvunbiased->at(i);
      }
    }
    
    isGoodZ0Elec=false;
    if( fabs(z0_v)<m_ElecZ0Cut ){
      isGoodZ0Elec=true;
    }
    
    PrintOut("                exGSF:: Eta = "); PrintOut(el_cl_eta->at(i));
    PrintOut(" Author = "); PrintOut(el_author->at(i)); 
    PrintOut(" ET = "); PrintOut(Et/m_GeV); 
    PrintOut(" LoosePP = "); PrintOut(isGoodLoosePPElec);
    PrintOut(" ObjQua = "); PrintOut(isGoodOQElec); 
    PrintOut(" isGoodZ0 = "); PrintOut(isGoodZ0Elec); PrintEndl();
    
    isGoodElectron=false;
    if( isGoodAuthorElec && isGoodLoosePPElec &&
	isGoodEtaElec && isGoodEtElec &&
	isGoodOQElec && isGoodZ0Elec ){
      
      isGoodElectron=true;
      PrintOut("                exGSF:: Good electron Found!!"); PrintEndl();
    }

    if(isGoodElectron){
      m_ElIndex.push_back(i);
    }
    
    if(isVertexGood && isTrigGood){
      if(isGoodAuthorElec){
	ElecAuthor++;
	if(isGoodLoosePPElec){
	  ElecLoosePP++;
	  if(isGoodEtaElec){
	    ElecEta++;
	    if(isGoodEtElec){
	      ElecEt++;
	      if(isGoodOQElec){
		ElecOQ++;
		if(isGoodZ0Elec){
		  ElecZ0++;
		}
	      }
	    }
	  }
	}
      }
    }
  }
  
  Print("                   Particles:: Removing electrons overlap");
  for(UInt_t id1=0; id1<m_ElIndex.size(); id1++){
    bool m_IseeOverlap=false;
    int el1=m_ElIndex.at(id1);
    
    for(UInt_t id2=0; id2<m_ElIndex.size(); id2++){
      int el2=m_ElIndex.at(id2);
      if(id1!=id2){
	if( el_Unrefittedtrack_qoverp->at(el1) == el_Unrefittedtrack_qoverp->at(el2) &&
	    el_Unrefittedtrack_theta->at(el1) == el_Unrefittedtrack_theta->at(el2)   &&
	    el_Unrefittedtrack_phi->at(el1) == el_Unrefittedtrack_phi->at(el2)       &&
	    el_Unrefittedtrack_d0->at(el1) == el_Unrefittedtrack_d0->at(el2)         &&
	    el_Unrefittedtrack_z0->at(el1) == el_Unrefittedtrack_z0->at(el2)          ) {
	  
	  Print("                             Overlap Found -> choosing Highest Et ...");
	  double et1 = el_cl_E->at(el1)/TMath::CosH(el_tracketa->at(el1));
	  double et2 = el_cl_E->at(el2)/TMath::CosH(el_tracketa->at(el2));
	  if( et1<et2 ){
	    m_IseeOverlap=true;
	    break;
	  }
	}
      }
    }
    if(!m_IseeOverlap)
      m_ElIndex_eeOverlap.push_back(el1);
    
    if(isVertexGood && isTrigGood && !m_IseeOverlap)
      ElecOverlap++;
  }

  Print("                   Particles:: Removing electrons overlap in cluster");
  for(UInt_t id1=0; id1<m_ElIndex_eeOverlap.size(); id1++){
    bool m_IseeOverlapCL=false;
    int el1=m_ElIndex_eeOverlap.at(id1);
    
    for(UInt_t id2=0; id2<m_ElIndex_eeOverlap.size(); id2++){
      int el2=m_ElIndex_eeOverlap.at(id2);
      
      if(id1!=id2){
	double DEta = fabs(el_cl_eta->at(el1)-el_cl_eta->at(el2));
	double DPhi = fabs(el_cl_phi->at(el1)-el_cl_phi->at(el2));
	if(DPhi>m_pi){ 
	  DPhi = 2*m_pi-DPhi; 
	}
	if( fabs(DEta)<3*0.025 && fabs(DPhi)<5*0.025 ){
	  Print("                             Overlap in cluster Found -> choosing Highest Et ...");
	  double et1 = el_cl_E->at(el1)/TMath::CosH(el_tracketa->at(el1));
	  double et2 = el_cl_E->at(el2)/TMath::CosH(el_tracketa->at(el2));
	  if( et1<et2 ){
	    m_IseeOverlapCL=true;
	    break;
	  }
	}
      }
    }
    if(!m_IseeOverlapCL)
      m_ElIndex_eeOverlapCL.push_back(el1);

    if(isVertexGood && isTrigGood && !m_IseeOverlapCL)
      ElecOverlapCl++;
  }

  Print("                   Particles:: Removing electrons-muons (CB or ST) overlap");
  bool isNot_emu_Overlap=true;
  for(UInt_t el=0; el<m_ElIndex_eeOverlapCL.size(); el++){
    isNot_emu_Overlap = CheckemuCBSTOverlap2012(el);
    if(isNot_emu_Overlap){
      m_ElIndex_emuOverlap.push_back(m_ElIndex_eeOverlapCL.at(el));
    }
    if(isVertexGood && isTrigGood && isNot_emu_Overlap){
      ElecMuOverlap++;
    }
  }
  
  Print("                   Particles:: Removing electrons-muons (Calo) overlap");
  m_nelecsel=(int)m_ElIndex_emuOverlap.size();
  for(UInt_t el=0; el<m_ElIndex_emuOverlap.size(); el++){
    CheckemuCaloOverlap2012(el);
  }
  
  Print("                   Particles:: Filling electrons vectors ...");
  for(UInt_t el=0; el<m_ElIndex_emuOverlap.size(); el++){
    int elidx=m_ElIndex_emuOverlap.at(el);

    PrintOut("                exGSF:: ");
    double ECorr = 0;
    double Ereso = 0;
    double d0_v  = 0;
    if(m_type==m_DATA){
      //ECorr = m_El_ERescaler->applyEnergyCorrection( el_cl_eta->at(elidx),
      //el_cl_E->at(elidx),
      //egRescaler::EnergyRescalerUpgrade::Electron, 
      //egRescaler::EnergyRescalerUpgrade::Nominal,
      //1.0,
      //RunNumber );
      
      //Ereso = m_El_ERescaler->resolution( ECorr/m_GeV,
      //el_cl_eta->at(elidx),
      //true )*ECorr;
      m_El_ERescaler->setRandomSeed(EventNumber+100*elidx);
      ECorr = m_El_ERescaler->getCorrectedEnergy(RunNumber,
						 m_DataType,
						 AtlasRoot::egammaEnergyCorrectionTool::ParticleInformation( el_rawcl_Es0->at(elidx),
													     el_rawcl_Es1->at(elidx),
													     el_rawcl_Es2->at(elidx),
													     el_rawcl_Es3->at(elidx),
													     el_cl_eta->at(elidx),
													     el_cl_phi->at(elidx),
													     el_tracketa->at(elidx),
													     el_cl_E->at(elidx),
													     el_cl_etaCalo->at(elidx),
													     el_cl_phiCalo->at(elidx) ),
						 m_ScaleVar,
						 m_Reso,
						 m_ResType,
						 1.0);
      
      d0_v=el_trackd0pvunbiased->at(elidx);
      
    }
    else{
      if(m_UseCorr){
	//m_El_ERescaler->SetRandomSeed(EventNumber+ 100*elidx);
	//double smear = m_El_ERescaler->getSmearingCorrection( el_cl_eta->at(elidx),
	//el_cl_E->at(elidx),
	//egRescaler::EnergyRescalerUpgrade::NOMINAL );
	
	//ECorr = el_cl_E->at(elidx)*fabs(smear);
	//Ereso = m_El_ERescaler->resolution( ECorr/m_GeV,
	//el_cl_eta->at(elidx),
	//true )*ECorr;
	m_El_ERescaler->setRandomSeed(EventNumber+100*elidx);
	ECorr = m_El_ERescaler->getCorrectedEnergy(RunNumber,
						   m_DataType,
						   AtlasRoot::egammaEnergyCorrectionTool::ParticleInformation( el_rawcl_Es0->at(elidx),
													       el_rawcl_Es1->at(elidx),
													       el_rawcl_Es2->at(elidx),
													       el_rawcl_Es3->at(elidx),
													       el_cl_eta->at(elidx),
													       el_cl_phi->at(elidx),
													       el_tracketa->at(elidx),
													       el_cl_E->at(elidx),
													       el_cl_etaCalo->at(elidx),
													       el_cl_phiCalo->at(elidx) ),
						   m_ScaleVar,
						   m_Reso,
						   m_ResType,
						   1.0);
	
	d0_v = D0Smearing( el_trackd0pvunbiased->at(elidx),
			   elidx,el_nBLHits->at(elidx),
			   el_tracketa->at(elidx),
			   fabs( sqrt( pow(el_cl_E->at(elidx)/m_GeV,2)
				       -pow(m_Melec,2))/
				 TMath::CosH(el_tracketa->at(elidx)) ) )-0.002;
      }
      else{
	ECorr = el_cl_E->at(elidx);
	//Ereso = m_El_ERescaler->resolution( el_cl_E->at(elidx)/m_GeV,
	//el_cl_eta->at(elidx),
	//true )*el_cl_E->at(elidx);
	d0_v=el_trackd0pvunbiased->at(elidx)-0.002;
      }
    }
    PrintOut("                         d0 = "); PrintOut(d0_v); PrintEndl();
    
    
    double scfId   = 1.;
    double scfReco = 1.;
    if(m_UseW){
      m_PileUp->SetRandomSeed(EventNumber+100*elidx);
      UInt_t RunNum = m_PileUp->GetRandomRunNumber(RunNumber);
      
      const Root::TResult &IdRes = m_El_SCFId->calculate( PATCore::ParticleDataType::Full,
							  RunNum,
							  el_cl_eta->at(elidx),
							  ECorr/TMath::CosH(el_tracketa->at(elidx)) );
      scfId = IdRes.getScaleFactor();
      
      const Root::TResult &RecoRes = m_El_SCFReco->calculate( PATCore::ParticleDataType::Full,
							      RunNum,
							      el_cl_eta->at(elidx),
							      ECorr/TMath::CosH(el_tracketa->at(elidx)) );
      
      scfReco = RecoRes.getScaleFactor();
      
    }

    Lep_Index.push_back( elidx );
    Lep_Id.push_back( -m_IdElectron*el_charge->at(elidx) );
    Lep_Author.push_back( el_author->at(elidx) );
    Lep_charge.push_back( el_charge->at(elidx) );
    Lep_d0_unbias.push_back( d0_v );
    Lep_scf.push_back( scfId*scfReco );
    Lep_resoscf.push_back( 0 );
    Lep_z0_unbias.push_back( el_trackz0pvunbiased->at(elidx) );
    Lep_E.push_back( ECorr/m_GeV );
    Lep_pt.push_back( fabs( sqrt( pow(ECorr/m_GeV,2)
				  -pow(m_Melec,2))/
			    TMath::CosH(el_tracketa->at(elidx)) ) );
    Lep_eta.push_back( el_tracketa->at(elidx) );
    Lep_phi.push_back( el_trackphi->at(elidx) );
    Lep_Ereso.push_back( Ereso );
    Lep_SAgood.push_back( false );
    Lep_CALOgood.push_back( false );
    Lep_numTrack.push_back( 0 );

    if(!m_MinParInfo){
      El_Index.push_back( elidx );
      El_Id.push_back( -m_IdElectron*el_charge->at(elidx) );
      El_Author.push_back( el_author->at(elidx) );
      El_charge.push_back( el_charge->at(elidx) );
      El_d0_unbias.push_back( d0_v );
      El_scf.push_back( scfId*scfReco );
      El_z0_unbias.push_back( el_trackz0pvunbiased->at(elidx) );
      El_E.push_back( ECorr/m_GeV );
      El_pt.push_back( fabs( sqrt( pow(ECorr/m_GeV,2)
				     -pow(m_Melec,2))/
			       TMath::CosH(el_tracketa->at(elidx)) ) );
      El_eta.push_back( el_tracketa->at(elidx) );
      El_phi.push_back( el_trackphi->at(elidx) );
      El_Ereso.push_back( Ereso );
      El_d0.push_back( el_trackd0->at(elidx) );
      El_sigd0_unbias.push_back( el_tracksigd0pvunbiased->at(elidx) );
      El_z0.push_back( el_trackz0->at(elidx) );
      //El_Ecl.push_back( ECorr/m_GeV );
      El_Eclraw.push_back( el_rawcl_E->at(elidx)/m_GeV );
      /*to remove if*/
      //if(m_type=="mc"){
      //El_Es0rawcl.push_back( el_rawcl_Es0->at(elidx)/m_GeV );
      //El_Es1rawcl.push_back( el_rawcl_Es1->at(elidx)/m_GeV );
      //El_Es2rawcl.push_back( el_rawcl_Es2->at(elidx)/m_GeV );
      //El_Es3rawcl.push_back( el_rawcl_Es3->at(elidx)/m_GeV );
      //El_calHitShDep.push_back( el_calibHitsShowerDepth->at(elidx) );
      //El_Ecb.push_back( el_E->at(elidx)/m_GeV );
      //El_ptcb.push_back( el_pt->at(elidx)/m_GeV );
      //El_pxcb.push_back( el_px->at(elidx)/m_GeV );
      //El_pycb.push_back( el_py->at(elidx)/m_GeV );
      //El_pzcb.push_back( el_pz->at(elidx)/m_GeV );
      //}
      TLorentzVector El_obj;
      El_obj.SetPtEtaPhiM(ECorr/m_GeV/
			  TMath::CosH(el_tracketa->at(elidx)),
			  el_tracketa->at(elidx),
			  el_trackphi->at(elidx),
			  m_Melec);
      El_p.push_back( El_obj.P() );
      El_px.push_back( El_obj.Px() );
      El_py.push_back( El_obj.Py() );
      El_pz.push_back( El_obj.Pz() );
      El_pid.push_back( fabs(1./el_trackqoverp->at(elidx)/m_GeV) );
      El_trkpt.push_back( el_trackpt->at(elidx)/m_GeV );
      El_Et.push_back( ECorr/m_GeV/
		       TMath::CosH(el_tracketa->at(elidx)) );
      El_theta.push_back( el_tracktheta->at(elidx) );
      El_etaS2.push_back( el_etas2->at(elidx) );
      El_etacl.push_back( el_cl_eta->at(elidx) );
      El_etap.push_back( el_etap->at(elidx) );
      El_phiS2.push_back( el_phis2->at(elidx) );
      El_phicl.push_back( el_cl_phi->at(elidx) );
      El_isocalo20.push_back( el_Etcone20->at(elidx)/m_GeV );
      El_isocalo30.push_back( el_Etcone30->at(elidx)/m_GeV );
      El_isocalo40.push_back( el_Etcone40->at(elidx)/m_GeV );
      El_topoisocalo20.push_back( el_topoEtcone20->at(elidx)/m_GeV );
      El_topoisocalo30.push_back( el_topoEtcone30->at(elidx)/m_GeV );
      El_topoisocalo40.push_back( el_topoEtcone40->at(elidx)/m_GeV );
      El_isotrk20.push_back( el_ptcone20->at(elidx)/m_GeV );
      El_isotrk30.push_back( el_ptcone30->at(elidx)/m_GeV );
      El_isotrk40.push_back( el_ptcone40->at(elidx)/m_GeV );
      El_EDmedian.push_back( el_ED_median->at(elidx) );
      El_cov_d0.push_back( el_trackcov_d0->at(elidx) );
      El_cov_z0.push_back( el_trackcov_z0->at(elidx) );
      El_cov_phi.push_back( el_trackcov_phi->at(elidx) );
      El_cov_theta.push_back( el_trackcov_theta->at(elidx) );
      El_cov_d0_z0.push_back( el_trackcov_d0_z0->at(elidx) );
      El_cov_d0_phi.push_back( el_trackcov_d0_phi->at(elidx) );
      El_cov_d0_theta.push_back( el_trackcov_d0_theta->at(elidx) );
      El_cov_z0_phi.push_back( el_trackcov_z0_phi->at(elidx) );
      El_cov_z0_theta.push_back( el_trackcov_z0_theta->at(elidx) );
      El_cov_phi_theta.push_back( el_trackcov_phi_theta->at(elidx) );
      if(m_type==m_MC){
	El_pt_truth.push_back( el_truth_pt->at(elidx)/m_GeV );
	El_eta_truth.push_back( el_truth_eta->at(elidx) );
	El_phi_truth.push_back( el_truth_phi->at(elidx) );
	El_E_truth.push_back( el_truth_E->at(elidx)/m_GeV );
	El_mother_truth.push_back( el_truth_mothertype->at(elidx) );
	El_motherbcode_truth.push_back( el_truth_motherbarcode->at(elidx) );
      }
    }
  }
  Print("                   Particles:: Electrons checked!");
  
}

bool Particles::CheckemuCBSTOverlap2011(UInt_t el){
  
  bool NoOverlap=true;
  for(UInt_t mu=0; mu<CBSTSA_Author.size(); mu++){
    
    if(CBSTSA_SAgood.at(mu)==1){ continue; }
    
    double phi1 = mu_staco_id_phi->at(CBSTSA_Index.at(mu));
    double phi2 = el_GSF_trackphi->at(m_ElIndex_eeOverlap.at(el));
    double eta1 = -log( tan(mu_staco_id_theta->at(CBSTSA_Index.at(mu))/2) );
    double eta2 = el_GSF_tracketa->at(m_ElIndex_eeOverlap.at(el));
    double dR = DeltaR(eta1,phi1,eta2,phi2);
    
    PrintOut("               DeltaR "); PrintOut(dR); PrintEndl();
    if(dR<0.02){
      Print("                   Particles:: Overlap e/mu (CB or ST) Found -> electron removed!");
      NoOverlap = false;
    }
  }
  return NoOverlap;
}

bool Particles::CheckemuCBSTOverlap2012(UInt_t el){
  
  bool NoOverlap=true;
  for(UInt_t mu=0; mu<CBSTSA_Author.size(); mu++){
    
    if(CBSTSA_SAgood.at(mu)==1){ continue; }
    
    double phi1 = mu_staco_id_phi->at(CBSTSA_Index.at(mu));
    double phi2 = el_Unrefittedtrack_phi->at(m_ElIndex_eeOverlapCL.at(el));
    double eta1 = -log( tan(mu_staco_id_theta->at(CBSTSA_Index.at(mu))/2) );
    double eta2 = el_Unrefittedtrack_eta->at(m_ElIndex_eeOverlapCL.at(el));
    double dR = DeltaR(eta1,phi1,eta2,phi2);
    
    PrintOut("                               DeltaR "); PrintOut(dR);
    PrintEndl();
    
    if(dR<0.02){
      Print("                   Particles:: Overlap e/mu (CB or ST) Found -> electron removed!");
      NoOverlap = false;
    }
  }
  return NoOverlap;

}

void Particles::CheckemuCaloOverlap2011(UInt_t el){
  
  std::vector<UInt_t> CaloErasePos;
  for(UInt_t mu=0; mu<Calo_Author.size(); mu++){
    
    double phi1 = mu_calo_id_phi->at(Calo_Index.at(mu));
    double phi2 = el_GSF_trackphi->at(m_ElIndex_eeOverlap.at(el));
    double eta1 = -log( tan(mu_calo_id_theta->at(Calo_Index.at(mu))/2) );
    double eta2 = el_GSF_tracketa->at(m_ElIndex_eeOverlap.at(el));
    double dR = DeltaR(eta1,phi1,eta2,phi2);
    
    PrintOut("                               DeltaR "); PrintOut(dR);
    PrintEndl();
    
    if(dR<0.02){
      Print("                   Particles:: Overlap e/mu (CALO) Found -> calo muon removed!");
      CaloErasePos.push_back(mu);
    }
  }
  if(CaloErasePos.size()!=0){
    
    for(UInt_t er=CaloErasePos.size(); er>0; er--){
      Calo_Index.erase( Calo_Index.begin()+CaloErasePos.at(er-1) );
      Calo_Id.erase( Calo_Id.begin()+CaloErasePos.at(er-1) );
      Calo_Author.erase( Calo_Author.begin()+CaloErasePos.at(er-1) );
      Calo_charge.erase( Calo_charge.begin()+CaloErasePos.at(er-1) );
      Calo_d0_unbias.erase( Calo_d0_unbias.begin()+CaloErasePos.at(er-1) );
      Calo_scf.erase( Calo_scf.begin()+CaloErasePos.at(er-1) );
      Calo_resoscf.erase( Calo_resoscf.begin()+CaloErasePos.at(er-1) );
      Calo_z0_unbias.erase( Calo_z0_unbias.begin()+CaloErasePos.at(er-1) );
      Calo_pt.erase( Calo_pt.begin()+CaloErasePos.at(er-1) );
      Calo_eta.erase( Calo_eta.begin()+CaloErasePos.at(er-1) );
      Calo_phi.erase( Calo_phi.begin()+CaloErasePos.at(er-1) );
      Calo_SAgood.erase( Calo_SAgood.begin()+CaloErasePos.at(er-1) );
      Calo_CALOgood.erase( Calo_CALOgood.begin()+CaloErasePos.at(er-1) );
	
      if(!m_MinParInfo){
	Calo_isSAmu.erase( Calo_isSAmu.begin()+CaloErasePos.at(er-1) );
	Calo_isSTmu.erase( Calo_isSTmu.begin()+CaloErasePos.at(er-1) );
	Calo_BLYgood.erase( Calo_BLYgood.begin()+CaloErasePos.at(er-1) );
	Calo_PXLgood.erase( Calo_PXLgood.begin()+CaloErasePos.at(er-1) );
	Calo_SCTgood.erase( Calo_SCTgood.begin()+CaloErasePos.at(er-1) );
	Calo_HOLgood.erase( Calo_HOLgood.begin()+CaloErasePos.at(er-1) );
	Calo_TRTgood.erase( Calo_TRTgood.begin()+CaloErasePos.at(er-1) );
	Calo_D0good.erase(  Calo_D0good.begin()+CaloErasePos.at(er-1) );
	Calo_Z0good.erase(  Calo_Z0good.begin()+CaloErasePos.at(er-1) );
	Calo_MCPgood.erase( Calo_MCPgood.begin()+CaloErasePos.at(er-1) );
	Calo_MSgood.erase(  Calo_MSgood.begin()+CaloErasePos.at(er-1) );
	//Calo_OurMSgood.erase( 
	Calo_CBTAGgood.erase( Calo_CBTAGgood.begin()+CaloErasePos.at(er-1) );
	Calo_n_exblh.erase( Calo_n_exblh.begin()+CaloErasePos.at(er-1) );
	Calo_n_blh.erase( Calo_n_blh.begin()+CaloErasePos.at(er-1) );
	Calo_n_pxlh.erase( Calo_n_pxlh.begin()+CaloErasePos.at(er-1) );
	Calo_n_pxlds.erase( Calo_n_pxlds.begin()+CaloErasePos.at(er-1) );
	Calo_n_pxlhol.erase( Calo_n_pxlhol.begin()+CaloErasePos.at(er-1) );
	Calo_n_scth.erase( Calo_n_scth.begin()+CaloErasePos.at(er-1) );
	Calo_n_sctds.erase( Calo_n_sctds.begin()+CaloErasePos.at(er-1) );
	Calo_n_scthol.erase( Calo_n_scthol.begin()+CaloErasePos.at(er-1) );
	Calo_n_hol.erase( Calo_n_hol.begin()+CaloErasePos.at(er-1) );
	Calo_n_trth.erase( Calo_n_trth.begin()+CaloErasePos.at(er-1) );
	Calo_n_trto.erase( Calo_n_trto.begin()+CaloErasePos.at(er-1) );
	Calo_n_trtho.erase( Calo_n_trtho.begin()+CaloErasePos.at(er-1) );
	Calo_n_mdth.erase( Calo_n_mdth.begin()+CaloErasePos.at(er-1) );
	Calo_n_mdtBIh.erase( Calo_n_mdtBIh.begin()+CaloErasePos.at(er-1) );
	Calo_n_mdtBMh.erase( Calo_n_mdtBMh.begin()+CaloErasePos.at(er-1) );
	Calo_n_mdtBOh.erase( Calo_n_mdtBOh.begin()+CaloErasePos.at(er-1) );
	Calo_n_mdtBEEh.erase( Calo_n_mdtBEEh.begin()+CaloErasePos.at(er-1) );
	Calo_n_mdtBIS78h.erase( Calo_n_mdtBIS78h.begin()+CaloErasePos.at(er-1) );
	Calo_n_mdtEIh.erase( Calo_n_mdtEIh.begin()+CaloErasePos.at(er-1) );
	Calo_n_mdtEMh.erase( Calo_n_mdtEMh.begin()+CaloErasePos.at(er-1) );
	Calo_n_mdtEOh.erase( Calo_n_mdtEOh.begin()+CaloErasePos.at(er-1) );
	Calo_n_mdtEEh.erase( Calo_n_mdtEEh.begin()+CaloErasePos.at(er-1) );
	Calo_n_cscEtah.erase( Calo_n_cscEtah.begin()+CaloErasePos.at(er-1) );
	Calo_n_cscPhih.erase( Calo_n_cscPhih.begin()+CaloErasePos.at(er-1) );
	Calo_n_rpcEtah.erase( Calo_n_rpcEtah.begin()+CaloErasePos.at(er-1) );
	Calo_n_rpcPhih.erase( Calo_n_rpcPhih.begin()+CaloErasePos.at(er-1) );
	Calo_n_tgcEtah.erase( Calo_n_tgcEtah.begin()+CaloErasePos.at(er-1) );
	Calo_n_tgcPhih.erase( Calo_n_tgcPhih.begin()+CaloErasePos.at(er-1) );
	Calo_d0.erase( Calo_d0.begin()+CaloErasePos.at(er-1) );
	Calo_errd0.erase( Calo_errd0.begin()+CaloErasePos.at(er-1) );
	Calo_sigd0_unbias.erase( Calo_sigd0_unbias.begin()+CaloErasePos.at(er-1) );
	Calo_z0.erase( Calo_z0.begin()+CaloErasePos.at(er-1) );
	Calo_p.erase( Calo_p.begin()+CaloErasePos.at(er-1) );
	Calo_px.erase( Calo_px.begin()+CaloErasePos.at(er-1) );
	Calo_py.erase( Calo_py.begin()+CaloErasePos.at(er-1) );
	Calo_pz.erase( Calo_pz.begin()+CaloErasePos.at(er-1) );
	Calo_pid.erase( Calo_pid.begin()+CaloErasePos.at(er-1) );
	Calo_E.erase( Calo_E.begin()+CaloErasePos.at(er-1) );
	Calo_theta.erase( Calo_theta.begin()+CaloErasePos.at(er-1) );
	Calo_theta_id.erase( Calo_theta_id.begin()+CaloErasePos.at(er-1) );
	Calo_eta_id.erase( Calo_eta_id.begin()+CaloErasePos.at(er-1) );
	Calo_phi_id.erase( Calo_phi_id.begin()+CaloErasePos.at(er-1) );
	Calo_isocalo20.erase( Calo_isocalo20.begin()+CaloErasePos.at(er-1) );
	Calo_isocalo30.erase( Calo_isocalo30.begin()+CaloErasePos.at(er-1) );
	Calo_isocalo40.erase( Calo_isocalo40.begin()+CaloErasePos.at(er-1) );
	Calo_isotrk20.erase( Calo_isotrk20.begin()+CaloErasePos.at(er-1) );
	Calo_isotrk30.erase( Calo_isotrk30.begin()+CaloErasePos.at(er-1) );
	Calo_isotrk40.erase( Calo_isotrk40.begin()+CaloErasePos.at(er-1) );
	/*cov*/
	Calo_cov_d0.erase( Calo_cov_d0.begin()+CaloErasePos.at(er-1) );
	Calo_cov_z0.erase( Calo_cov_z0.begin()+CaloErasePos.at(er-1) );
	Calo_cov_phi.erase( Calo_cov_phi.begin()+CaloErasePos.at(er-1) );
	Calo_cov_theta.erase( Calo_cov_theta.begin()+CaloErasePos.at(er-1) );
	Calo_cov_qoverp.erase( Calo_cov_qoverp.begin()+CaloErasePos.at(er-1) );
	Calo_cov_d0_z0.erase( Calo_cov_d0_z0.begin()+CaloErasePos.at(er-1) );
	Calo_cov_d0_phi.erase( Calo_cov_d0_phi.begin()+CaloErasePos.at(er-1) );
	Calo_cov_d0_theta.erase( Calo_cov_d0_theta.begin()+CaloErasePos.at(er-1) );
	Calo_cov_d0_qoverp.erase( Calo_cov_d0_qoverp.begin()+CaloErasePos.at(er-1) );
	Calo_cov_z0_phi.erase( Calo_cov_z0_phi.begin()+CaloErasePos.at(er-1) );
	Calo_cov_z0_theta.erase( Calo_cov_z0_theta.begin()+CaloErasePos.at(er-1) );
	Calo_cov_z0_qoverp.erase( Calo_cov_z0_qoverp.begin()+CaloErasePos.at(er-1) );
	Calo_cov_phi_theta.erase( Calo_cov_phi_theta.begin()+CaloErasePos.at(er-1) );
	Calo_cov_phi_qoverp.erase( Calo_cov_phi_qoverp.begin()+CaloErasePos.at(er-1) );
	Calo_cov_theta_qoverp.erase( Calo_cov_theta_qoverp.begin()+CaloErasePos.at(er-1) );
	
	if(m_type==m_MC){
	  Calo_pt_truth.erase( Calo_pt_truth.begin()+CaloErasePos.at(er-1) );
	  Calo_eta_truth.erase( Calo_eta_truth.begin()+CaloErasePos.at(er-1) );
	  Calo_phi_truth.erase( Calo_phi_truth.begin()+CaloErasePos.at(er-1) );
	  Calo_E_truth.erase( Calo_E_truth.begin()+CaloErasePos.at(er-1) );
	  Calo_mother_truth.erase( Calo_mother_truth.begin()+CaloErasePos.at(er-1) );
	  Calo_motherbcode_truth.erase( Calo_motherbcode_truth.begin()+CaloErasePos.at(er-1) );
	}
      }
    }
  }
  
}

void Particles::CheckemuCaloOverlap2012(UInt_t el){
  
  std::vector<UInt_t> CaloErasePos;
  for(UInt_t mu=0; mu<Calo_Author.size(); mu++){
    
    double phi1 = mu_calo_id_phi->at(Calo_Index.at(mu));
    double phi2 = el_Unrefittedtrack_phi->at(m_ElIndex_eeOverlapCL.at(el));
    double eta1 = -log( tan(mu_calo_id_theta->at(Calo_Index.at(mu))/2) );
    double eta2 = el_Unrefittedtrack_eta->at(m_ElIndex_eeOverlapCL.at(el));
    double dR = DeltaR(eta1,phi1,eta2,phi2);
    
    PrintOut("                               DeltaR "); PrintOut(dR);
    PrintEndl();
    
    if(dR<0.02){
      Print("                   Particles:: Overlap e/mu (CALO) Found -> calo muon removed!");
      CaloErasePos.push_back(mu);
    }
  }
  if(CaloErasePos.size()!=0){
    
    for(UInt_t er=CaloErasePos.size(); er>0; er--){
      Calo_Index.erase( Calo_Index.begin()+CaloErasePos.at(er-1) );
      Calo_Id.erase( Calo_Id.begin()+CaloErasePos.at(er-1) );
      Calo_Author.erase( Calo_Author.begin()+CaloErasePos.at(er-1) );
      Calo_charge.erase( Calo_charge.begin()+CaloErasePos.at(er-1) );
      Calo_d0_unbias.erase( Calo_d0_unbias.begin()+CaloErasePos.at(er-1) );
      Calo_scf.erase( Calo_scf.begin()+CaloErasePos.at(er-1) );
      Calo_resoscf.erase( Calo_resoscf.begin()+CaloErasePos.at(er-1) );
      Calo_z0_unbias.erase( Calo_z0_unbias.begin()+CaloErasePos.at(er-1) );
      Calo_pt.erase( Calo_pt.begin()+CaloErasePos.at(er-1) );
      Calo_eta.erase( Calo_eta.begin()+CaloErasePos.at(er-1) );
      Calo_phi.erase( Calo_phi.begin()+CaloErasePos.at(er-1) );
      Calo_SAgood.erase( Calo_SAgood.begin()+CaloErasePos.at(er-1) );
      Calo_CALOgood.erase( Calo_CALOgood.begin()+CaloErasePos.at(er-1) );
      
      if(!m_MinParInfo){
	Calo_isSAmu.erase( Calo_isSAmu.begin()+CaloErasePos.at(er-1) );
	Calo_isSTmu.erase( Calo_isSTmu.begin()+CaloErasePos.at(er-1) );
	Calo_BLYgood.erase( Calo_BLYgood.begin()+CaloErasePos.at(er-1) );
	Calo_PXLgood.erase( Calo_PXLgood.begin()+CaloErasePos.at(er-1) );
	Calo_SCTgood.erase( Calo_SCTgood.begin()+CaloErasePos.at(er-1) );
	Calo_HOLgood.erase( Calo_HOLgood.begin()+CaloErasePos.at(er-1) );
	Calo_TRTgood.erase( Calo_TRTgood.begin()+CaloErasePos.at(er-1) );
	Calo_D0good.erase(  Calo_D0good.begin()+CaloErasePos.at(er-1) );
	Calo_Z0good.erase(  Calo_Z0good.begin()+CaloErasePos.at(er-1) );
	Calo_MCPgood.erase( Calo_MCPgood.begin()+CaloErasePos.at(er-1) );
	Calo_MSgood.erase(  Calo_MSgood.begin()+CaloErasePos.at(er-1) );
	//Calo_OurMSgood.erase( 
	Calo_CBTAGgood.erase( Calo_CBTAGgood.begin()+CaloErasePos.at(er-1) );
	Calo_n_exblh.erase( Calo_n_exblh.begin()+CaloErasePos.at(er-1) );
	Calo_n_blh.erase( Calo_n_blh.begin()+CaloErasePos.at(er-1) );
	Calo_n_pxlh.erase( Calo_n_pxlh.begin()+CaloErasePos.at(er-1) );
	Calo_n_pxlds.erase( Calo_n_pxlds.begin()+CaloErasePos.at(er-1) );
	Calo_n_pxlhol.erase( Calo_n_pxlhol.begin()+CaloErasePos.at(er-1) );
	Calo_n_scth.erase( Calo_n_scth.begin()+CaloErasePos.at(er-1) );
	Calo_n_sctds.erase( Calo_n_sctds.begin()+CaloErasePos.at(er-1) );
	Calo_n_scthol.erase( Calo_n_scthol.begin()+CaloErasePos.at(er-1) );
	Calo_n_hol.erase( Calo_n_hol.begin()+CaloErasePos.at(er-1) );
	Calo_n_trth.erase( Calo_n_trth.begin()+CaloErasePos.at(er-1) );
	Calo_n_trto.erase( Calo_n_trto.begin()+CaloErasePos.at(er-1) );
	Calo_n_trtho.erase( Calo_n_trtho.begin()+CaloErasePos.at(er-1) );
	Calo_n_mdth.erase( Calo_n_mdth.begin()+CaloErasePos.at(er-1) );
	Calo_n_mdtBIh.erase( Calo_n_mdtBIh.begin()+CaloErasePos.at(er-1) );
	Calo_n_mdtBMh.erase( Calo_n_mdtBMh.begin()+CaloErasePos.at(er-1) );
	Calo_n_mdtBOh.erase( Calo_n_mdtBOh.begin()+CaloErasePos.at(er-1) );
	Calo_n_mdtBEEh.erase( Calo_n_mdtBEEh.begin()+CaloErasePos.at(er-1) );
	Calo_n_mdtBIS78h.erase( Calo_n_mdtBIS78h.begin()+CaloErasePos.at(er-1) );
	Calo_n_mdtEIh.erase( Calo_n_mdtEIh.begin()+CaloErasePos.at(er-1) );
	Calo_n_mdtEMh.erase( Calo_n_mdtEMh.begin()+CaloErasePos.at(er-1) );
	Calo_n_mdtEOh.erase( Calo_n_mdtEOh.begin()+CaloErasePos.at(er-1) );
	Calo_n_mdtEEh.erase( Calo_n_mdtEEh.begin()+CaloErasePos.at(er-1) );
	Calo_n_cscEtah.erase( Calo_n_cscEtah.begin()+CaloErasePos.at(er-1) );
	Calo_n_cscPhih.erase( Calo_n_cscPhih.begin()+CaloErasePos.at(er-1) );
	Calo_n_rpcEtah.erase( Calo_n_rpcEtah.begin()+CaloErasePos.at(er-1) );
	Calo_n_rpcPhih.erase( Calo_n_rpcPhih.begin()+CaloErasePos.at(er-1) );
	Calo_n_tgcEtah.erase( Calo_n_tgcEtah.begin()+CaloErasePos.at(er-1) );
	Calo_n_tgcPhih.erase( Calo_n_tgcPhih.begin()+CaloErasePos.at(er-1) );
	Calo_d0.erase( Calo_d0.begin()+CaloErasePos.at(er-1) );
	Calo_errd0.erase( Calo_errd0.begin()+CaloErasePos.at(er-1) );
	Calo_sigd0_unbias.erase( Calo_sigd0_unbias.begin()+CaloErasePos.at(er-1) );
	Calo_z0.erase( Calo_z0.begin()+CaloErasePos.at(er-1) );
	Calo_p.erase( Calo_p.begin()+CaloErasePos.at(er-1) );
	Calo_px.erase( Calo_px.begin()+CaloErasePos.at(er-1) );
	Calo_py.erase( Calo_py.begin()+CaloErasePos.at(er-1) );
	Calo_pz.erase( Calo_pz.begin()+CaloErasePos.at(er-1) );
	Calo_pid.erase( Calo_pid.begin()+CaloErasePos.at(er-1) );
	Calo_E.erase( Calo_E.begin()+CaloErasePos.at(er-1) );
	Calo_theta.erase( Calo_theta.begin()+CaloErasePos.at(er-1) );
	Calo_theta_id.erase( Calo_theta_id.begin()+CaloErasePos.at(er-1) );
	Calo_eta_id.erase( Calo_eta_id.begin()+CaloErasePos.at(er-1) );
	Calo_phi_id.erase( Calo_phi_id.begin()+CaloErasePos.at(er-1) );
	Calo_isocalo20.erase( Calo_isocalo20.begin()+CaloErasePos.at(er-1) );
	Calo_isocalo30.erase( Calo_isocalo30.begin()+CaloErasePos.at(er-1) );
	Calo_isocalo40.erase( Calo_isocalo40.begin()+CaloErasePos.at(er-1) );
	Calo_isotrk20.erase( Calo_isotrk20.begin()+CaloErasePos.at(er-1) );
	Calo_isotrk30.erase( Calo_isotrk30.begin()+CaloErasePos.at(er-1) );
	Calo_isotrk40.erase( Calo_isotrk40.begin()+CaloErasePos.at(er-1) );
	/*cov*/
	Calo_cov_d0.erase( Calo_cov_d0.begin()+CaloErasePos.at(er-1) );
	Calo_cov_z0.erase( Calo_cov_z0.begin()+CaloErasePos.at(er-1) );
	Calo_cov_phi.erase( Calo_cov_phi.begin()+CaloErasePos.at(er-1) );
	Calo_cov_theta.erase( Calo_cov_theta.begin()+CaloErasePos.at(er-1) );
	Calo_cov_qoverp.erase( Calo_cov_qoverp.begin()+CaloErasePos.at(er-1) );
	Calo_cov_d0_z0.erase( Calo_cov_d0_z0.begin()+CaloErasePos.at(er-1) );
	Calo_cov_d0_phi.erase( Calo_cov_d0_phi.begin()+CaloErasePos.at(er-1) );
	Calo_cov_d0_theta.erase( Calo_cov_d0_theta.begin()+CaloErasePos.at(er-1) );
	Calo_cov_d0_qoverp.erase( Calo_cov_d0_qoverp.begin()+CaloErasePos.at(er-1) );
	Calo_cov_z0_phi.erase( Calo_cov_z0_phi.begin()+CaloErasePos.at(er-1) );
	Calo_cov_z0_theta.erase( Calo_cov_z0_theta.begin()+CaloErasePos.at(er-1) );
	Calo_cov_z0_qoverp.erase( Calo_cov_z0_qoverp.begin()+CaloErasePos.at(er-1) );
	Calo_cov_phi_theta.erase( Calo_cov_phi_theta.begin()+CaloErasePos.at(er-1) );
	Calo_cov_phi_qoverp.erase( Calo_cov_phi_qoverp.begin()+CaloErasePos.at(er-1) );
	Calo_cov_theta_qoverp.erase( Calo_cov_theta_qoverp.begin()+CaloErasePos.at(er-1) );
	
	if(m_type==m_MC){
	  Calo_pt_truth.erase( Calo_pt_truth.begin()+CaloErasePos.at(er-1) );
	  Calo_eta_truth.erase( Calo_eta_truth.begin()+CaloErasePos.at(er-1) );
	  Calo_phi_truth.erase( Calo_phi_truth.begin()+CaloErasePos.at(er-1) );
	  Calo_E_truth.erase( Calo_E_truth.begin()+CaloErasePos.at(er-1) );
	  Calo_mother_truth.erase( Calo_mother_truth.begin()+CaloErasePos.at(er-1) );
	  Calo_motherbcode_truth.erase( Calo_motherbcode_truth.begin()+CaloErasePos.at(er-1) );
	}
      }
    }
  }

}
