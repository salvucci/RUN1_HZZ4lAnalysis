#include "Particles.h"
#include "../DPDVar/DPDMuonVariables.h"

void Particles::ResetMuonQualityFlags(){

  isGoodTRTmuon     = false;
  isGoodMCPmuon     = false;
  isGoodMSmuonSA    = false;
  //isGoodOurMSmuonSA = false;
  isGoodBlayer      = false;
  isGoodPixel       = false;
  isGoodSCT         = false;
  isGoodHoles       = false;
  isGoodZ0muon      = false;
  isGoodD0muon      = false;
  isAuthorCBGood    = false;
  isAuthorSAGood    = false;
  isAuthorCALOGood  = false;
  isGoodCBTagMuon   = false;
  isGoodSAMuon      = false;
  isGoodCALOmuon    = false;
  nbl   = 0;
  npixl = 0;
  nscth = 0;
  nhol  = 0;
  ntrth = 0;
  ntrto = 0;
  ni    = 0;
  nm    = 0;
  no    = 0;
  nphi  = 0;
  
}

void Particles::CheckingStacoMuons(bool isVertexGood,
				   bool isTrigGood){
  
  Print("                   Particles:: Checking CB, Tag and SA muons ... ");

  int nmuCBSA=mu_staco_n;
  m_nmuCBsel       = 0;
  m_nmuSAsel       = 0;
  m_nmuCBSAsel     = 0;
  
  for (int i=0; i<nmuCBSA; i++) {
    
    m_MuonSmear->SetSeed(EventNumber,i);
    
    /* Checking CB muons */
    PrintOut(""); PrintEndl(); 
    
    /* Selection variables on ID hits MCP prescription */
    nbl=0,npixl=0,nscth=0,nhol=0,ntrto=0,ntrth=0;
    
    nbl   = mu_staco_nBLHits->at(i);
    npixl = mu_staco_nPixHits->at(i) + mu_staco_nPixelDeadSensors->at(i); 
    nscth = mu_staco_nSCTHits->at(i) + mu_staco_nSCTDeadSensors->at(i);
    nhol  = mu_staco_nPixHoles->at(i) + mu_staco_nSCTHoles->at(i);
    ntrth = mu_staco_nTRTHits->at(i) + mu_staco_nTRTOutliers->at(i);
    ntrto = mu_staco_nTRTOutliers->at(i);
    
    isGoodTRTmuon=false;
    Bool_t case1=false, case2=false, trt_ext=false;
    if(m_year==m_YEAR_2011){
      case1 = ( (ntrth>5) && ntrto<0.9*ntrth ) ;
      case2 = (ntrth>5) ? (ntrto<0.9*ntrth) : true ;
      trt_ext = (fabs(mu_staco_eta->at(i))<1.9) ? case1 : case2 ;
    }
    if(m_year==m_YEAR_2012){
      if( (fabs(mu_staco_eta->at(i))>0.1 && fabs(mu_staco_eta->at(i))<1.9) ){
	case1 = ( (ntrth>5) && ntrto<0.9*ntrth);
	trt_ext = case1;
      }
      else{
	trt_ext = true;
      }
    }
    if( trt_ext ){
      isGoodTRTmuon=true;
    }
    
    isAuthorCBGood=false;
    if( (mu_staco_author->at(i)==6 || mu_staco_author->at(i)==7) && mu_staco_isStandAloneMuon->at(i)==0 ){
      isAuthorCBGood=true;
    }
    
    isGoodBlayer=false;
    if(m_year==m_YEAR_2011){
      if( !mu_staco_expectBLayerHit->at(i) || nbl>0){
	isGoodBlayer=true;
      }
    }
    else{ 
      isGoodBlayer=true; 
    }
    
    isGoodPixel=false;
    int Npix=0;
    if(m_year==m_YEAR_2011) { Npix=1; }
    if(m_year==m_YEAR_2012) { Npix=0; }
    if(npixl>Npix){
      isGoodPixel=true;
    }
    
    isGoodSCT=false;
    int Nsct=0;
    if(m_year==m_YEAR_2011) { Nsct=5; }
    if(m_year==m_YEAR_2012) { Nsct=4; }
    if(nscth>Nsct){
      isGoodSCT=true;
    }

    isGoodHoles=false;
    if(nhol<3){
      isGoodHoles=true;
    }
    
    isGoodMCPmuon=false;
    if(isGoodBlayer && isGoodPixel && isGoodSCT 
       && isGoodHoles && isGoodTRTmuon ){
      isGoodMCPmuon=true;
      PrintOut("                  Staco:: GoodMCP muon Found!"); PrintEndl();
      PrintOut("                          nbl = "); PrintOut(nbl);
      PrintOut(" npixl = "); PrintOut(npixl); PrintOut(" nscth = "); PrintOut(nscth);
      PrintOut(" nhol = "); PrintOut(nhol); PrintOut(" isGoodTRTmuon = "); 
      PrintOut(isGoodTRTmuon); PrintEndl();
    }

    /* Checking SA muons */
    ni=0, nm=0, no=0, nphi=0;
    ni   = mu_staco_nMDTEIHits->at(i)  + mu_staco_nMDTBIHits->at(i) + mu_staco_nCSCEtaHits->at(i);
    nm   = mu_staco_nMDTEMHits->at(i)  + mu_staco_nMDTBMHits->at(i);
    no   = mu_staco_nMDTEOHits->at(i)  + mu_staco_nMDTBOHits->at(i);
    nphi = mu_staco_nCSCPhiHits->at(i) + mu_staco_nTGCPhiHits->at(i) + mu_staco_nRPCPhiHits->at(i);
    
    /*
    isGoodOurMSmuonSA=false;
    if (ni>1 && nm>1 && no>1 && nphi>1) {
      isGoodOurMSmuonSA=true;
      PrintOut("                  Staco:: GoodMS muon Found!"); PrintEndl();
      PrintOut("                          ni = "); PrintOut(ni); PrintOut(" nm = "); 
      PrintOut(nm); PrintOut(" no = "); PrintOut(no); PrintOut(" nphi = ");
      PrintOut(nphi); PrintEndl();
    }
    */
    
    Int_t mu_cscetahits = mu_staco_nCSCEtaHits->at(i);
    Int_t mu_cscphihits = mu_staco_nCSCPhiHits->at(i);
    Int_t mu_emhits = mu_staco_nMDTEMHits->at(i);
    Int_t mu_eohits = mu_staco_nMDTEOHits->at(i);
    
    isGoodMSmuonSA=false;
    if( (mu_cscetahits+mu_cscphihits)>0 && mu_emhits>0 && mu_eohits>0 ){
      isGoodMSmuonSA=true;
    }
    
    isAuthorSAGood=false;
    if(mu_staco_author->at(i)==6 && mu_staco_isStandAloneMuon->at(i)==1){
      isAuthorSAGood=true;
    }
    
    /* Selecting good CB, Tag and SA muons */
    isGoodCBTagMuon=false;
    isGoodSAMuon=false;
    
    double mupT = 0;
    if(m_UseCorr){
      double ptms = fabs((fabs(1/mu_staco_me_qoverp->at(i)))*sin(mu_staco_me_theta->at(i)));
      double ptid = fabs((fabs(1/mu_staco_id_qoverp->at(i)))*sin(mu_staco_id_theta->at(i)));
      
      if(mu_staco_isCombinedMuon->at(i)==1){
	m_MuonSmear->Event(ptms,ptid,mu_staco_pt->at(i),mu_staco_eta->at(i),
			   mu_staco_charge->at(i),mu_staco_phi->at(i));
	mupT=m_MuonSmear->pTCB();
      }
      else if(mu_staco_isStandAloneMuon->at(i)==1){
	//m_MuonSmear->Event(ptms,mu_staco_eta->at(i),"MS");
	m_MuonSmear->Event(mu_staco_pt->at(i),mu_staco_eta->at(i),"MS",
			   mu_staco_charge->at(i),mu_staco_phi->at(i));
	mupT=m_MuonSmear->pTMS();
      }
      else if(mu_staco_isSegmentTaggedMuon->at(i)==1){
	//m_MuonSmear->Event(ptid,mu_staco_eta->at(i),"ID");
	m_MuonSmear->Event(mu_staco_pt->at(i),mu_staco_eta->at(i),"ID",
			   mu_staco_charge->at(i),mu_staco_phi->at(i));
	mupT=m_MuonSmear->pTID();
      }
      
    }
    else{
      mupT=mu_staco_pt->at(i);
    }
    
    double resscf = 1.;
    TLorentzVector Mu;
    Mu.SetPtEtaPhiM(mupT,mu_staco_eta->at(i),
		    mu_staco_phi->at(i),m_Mmuon*m_GeV);
    if(mu_staco_isCombinedMuon->at(i)==1){
      resscf = m_MuonResoSCF->getResolutionScaleFactor(Mu,1);
    }
    else if(mu_staco_isSegmentTaggedMuon->at(i)==1){
      resscf = m_MuonResoSCF->getResolutionScaleFactor(Mu,2);
    }
    else if(mu_staco_isStandAloneMuon->at(i)==1){
      resscf = m_MuonResoSCF->getResolutionScaleFactor(Mu,3);
    }
    
    PrintOut("                  Staco:: ");
    double d0_v = 0;
    double z0_v = 0;
    if(m_type==m_DATA){
      d0_v = mu_staco_trackd0pvunbiased->at(i);
      z0_v = mu_staco_trackz0pvunbiased->at(i);
    }
    else{
      if(m_year==m_YEAR_2012){
	if(m_UseCorr){
	  double m_etaid = -log( tan(mu_staco_id_theta->at(i)/2) );
	  double m_ptid  = fabs(1./mu_staco_id_qoverp->at(i))/cosh(m_etaid);
	  
	  if(mu_staco_isStandAloneMuon->at(i)==1){
	    d0_v = mu_staco_trackd0pvunbiased->at(i);
	  }
	  else{
	    d0_v = D0Smearing( mu_staco_trackd0pvunbiased->at(i),
			     i,mu_staco_nBLHits->at(i),
			       m_etaid,m_ptid )-0.002;
	  }
	  z0_v = Z0Smearing( mu_staco_trackz0pvunbiased->at(i),
			     i,mu_staco_nBLHits->at(i),
			     m_etaid,m_ptid );
	}
	else{
	  if(mu_staco_isStandAloneMuon->at(i)==1){
	    d0_v = mu_staco_trackd0pvunbiased->at(i);
	  }
	  else{
	    d0_v = mu_staco_trackd0pvunbiased->at(i)-0.002;
	  }
	  z0_v = mu_staco_trackz0pvunbiased->at(i);
	}
      }
      else if(m_year==m_YEAR_2011){
	d0_v = mu_staco_trackd0pvunbiased->at(i);
	z0_v = mu_staco_trackz0pvunbiased->at(i);
      }
    }
    PrintOut("                         d0 = "); PrintOut(d0_v); PrintEndl();
    PrintOut("                         z0 = "); PrintOut(z0_v); PrintEndl();
    
    isGoodD0muon=false;
    if ( fabs(d0_v)<m_MuonD0Cut ){
      isGoodD0muon=true;
    }

    isGoodZ0muon=false;
    if( fabs(z0_v)<m_MuonZ0Cut ){
      isGoodZ0muon=true;
    }
    
    PrintOut("                  Staco:: Eta = "); PrintOut(mu_staco_eta->at(i));
    PrintOut(" Author = "); PrintOut(mu_staco_author->at(i)); 
    PrintOut(" pT = "); PrintOut(mupT/m_GeV);
    PrintOut(" AuthorCB = "); PrintOut(isAuthorCBGood);
    PrintOut(" isCB = "); PrintOut(mu_staco_isCombinedMuon->at(i)); PrintEndl();
    PrintOut("                          AuthorSA = "); PrintOut(isAuthorSAGood);
    PrintOut(" isSA = "); PrintOut(mu_staco_isStandAloneMuon->at(i)); 
    PrintOut(" isGoodD0 = "); PrintOut(isGoodD0muon);
    PrintOut(" isGoodZ0 = "); PrintOut(isGoodZ0muon); PrintEndl();
    
    if( isAuthorCBGood && mupT/m_GeV>m_MuonStacoPtCut && 
	fabs(mu_staco_eta->at(i))<=2.7 && isGoodMCPmuon
	&& isGoodZ0muon && isGoodD0muon){
      isGoodCBTagMuon=true;
      m_nmuCBsel=m_nmuCBsel+1;
      PrintOut("                  Staco:: Good CB or Tag muon Found!!"); PrintEndl();
    }
    else if( mupT/m_GeV>m_MuonStacoPtCut && fabs(mu_staco_eta->at(i))>2.5 
	     && fabs(mu_staco_eta->at(i))<2.7 && isGoodMSmuonSA && isAuthorSAGood ){
      isGoodSAMuon=true;
      m_nmuSAsel=m_nmuSAsel+1;
      PrintOut("                  Staco:: Good SA muon Found!!"); PrintEndl();
    }
    
    if(isGoodCBTagMuon || isGoodSAMuon){
      
      m_nmuCBSAsel=m_nmuCBSAsel+1;
      
      double scf = 1;
      if(m_UseW){
	if(m_year==m_YEAR_2012){
	  if(isGoodSAMuon){
	    scf=m_StacoSASCF->scaleFactor(mu_staco_charge->at(i),Mu);
	  }
	  else if(isGoodCBTagMuon){
	    scf=m_StacoSCF->scaleFactor(mu_staco_charge->at(i),Mu);
	  }
	}
	else if(m_year==m_YEAR_2011){
	  if(isGoodSAMuon){
	    scf=m_StacoSASCF->scaleFactor(Mu);
	  }
	  else if(isGoodCBTagMuon){
	    scf=m_StacoSCF->scaleFactor(mu_staco_charge->at(i),Mu);
	  }
	}
      }
      
      Staco_Index.push_back( i );
      Staco_Id.push_back( -m_IdMuon*mu_staco_charge->at(i) );
      Staco_Author.push_back( mu_staco_author->at(i) );
      Staco_charge.push_back(mu_staco_charge->at(i) );
      Staco_d0_unbias.push_back( d0_v );
      Staco_scf.push_back( scf );
      Staco_resoscf.push_back( resscf );
      Staco_z0_unbias.push_back( z0_v );
      Staco_E.push_back( mu_staco_E->at(i)/m_GeV );
      Staco_pt.push_back( mupT/m_GeV );
      Staco_eta.push_back( mu_staco_eta->at(i) );
      Staco_phi.push_back( mu_staco_phi->at(i) );
      Staco_SAgood.push_back( isGoodSAMuon );
      Staco_CALOgood.push_back( isGoodCALOmuon );
      
      if(!m_MinParInfo){
	Staco_isSAmu.push_back( mu_staco_isStandAloneMuon->at(i) );
	Staco_isSTmu.push_back( mu_staco_isSegmentTaggedMuon->at(i) );
	Staco_BLYgood.push_back( isGoodBlayer );
	Staco_PXLgood.push_back( isGoodPixel );
	Staco_SCTgood.push_back( isGoodSCT );
	Staco_HOLgood.push_back( isGoodHoles);
	Staco_TRTgood.push_back( isGoodTRTmuon );
	Staco_D0good.push_back( isGoodD0muon );
	Staco_Z0good.push_back( isGoodZ0muon );
	Staco_MCPgood.push_back( isGoodMCPmuon );
	Staco_MSgood.push_back( isGoodMSmuonSA );
	//Staco_OurMSgood.push_back( isGoodOurMSmuonSA );
	Staco_CBTAGgood.push_back( isGoodCBTagMuon );
	Staco_n_exblh.push_back( mu_staco_expectBLayerHit->at(i) );
	Staco_n_blh.push_back( mu_staco_nBLHits->at(i) );  
	Staco_n_pxlh.push_back( mu_staco_nPixHits->at(i) );
	Staco_n_pxlds.push_back( mu_staco_nPixelDeadSensors->at(i) );
	Staco_n_pxlhol.push_back( mu_staco_nPixHoles->at(i) );
	Staco_n_scth.push_back( mu_staco_nSCTHits->at(i) );
	Staco_n_sctds.push_back( mu_staco_nSCTDeadSensors->at(i) );
	Staco_n_scthol.push_back( mu_staco_nSCTHoles->at(i) );
	Staco_n_hol.push_back( mu_staco_nPixHoles->at(i)+mu_staco_nSCTHoles->at(i) ); 
	Staco_n_trth.push_back( mu_staco_nTRTHits->at(i) );
	Staco_n_trto.push_back( mu_staco_nTRTOutliers->at(i) );
	Staco_n_trtho.push_back( mu_staco_nTRTHits->at(i) + mu_staco_nTRTOutliers->at(i) );
	Staco_n_mdth.push_back( mu_staco_nMDTHits->at(i) );
	Staco_n_mdtBIh.push_back( mu_staco_nMDTBIHits->at(i) );
	Staco_n_mdtBMh.push_back( mu_staco_nMDTBMHits->at(i) );
	Staco_n_mdtBOh.push_back( mu_staco_nMDTBOHits->at(i) );
	Staco_n_mdtBEEh.push_back( mu_staco_nMDTBEEHits->at(i) );
	Staco_n_mdtBIS78h.push_back( mu_staco_nMDTBIS78Hits->at(i) );
	Staco_n_mdtEIh.push_back( mu_staco_nMDTEIHits->at(i) );
	Staco_n_mdtEMh.push_back( mu_staco_nMDTEMHits->at(i) );
	Staco_n_mdtEOh.push_back( mu_staco_nMDTEOHits->at(i) );
	Staco_n_mdtEEh.push_back( mu_staco_nMDTEEHits->at(i) );
	Staco_n_cscEtah.push_back( mu_staco_nCSCEtaHits->at(i) );
	Staco_n_cscPhih.push_back( mu_staco_nCSCPhiHits->at(i) );
	Staco_n_rpcEtah.push_back( mu_staco_nRPCEtaHits->at(i) );
	Staco_n_rpcPhih.push_back( mu_staco_nRPCPhiHits->at(i) );
	Staco_n_tgcEtah.push_back( mu_staco_nTGCEtaHits->at(i) );
	Staco_n_tgcPhih.push_back( mu_staco_nTGCPhiHits->at(i) );
	Staco_d0.push_back( mu_staco_d0_exPV->at(i) );
	Staco_errd0.push_back( mu_staco_cov_d0_exPV->at(i) );
	Staco_sigd0_unbias.push_back( mu_staco_tracksigd0pvunbiased->at(i) );
	Staco_z0.push_back( mu_staco_z0_exPV->at(i) );
	Staco_p.push_back( fabs(1./mu_staco_qoverp_exPV->at(i)/m_GeV) );
	Staco_px.push_back( mu_staco_px->at(i)/m_GeV );
	Staco_py.push_back( mu_staco_py->at(i)/m_GeV );
	Staco_pz.push_back( mu_staco_pz->at(i)/m_GeV );
	Staco_pid.push_back( fabs(1./mu_staco_id_qoverp->at(i))/m_GeV );
	Staco_theta.push_back( mu_staco_theta_exPV->at(i) );
	Staco_theta_id.push_back( mu_staco_id_theta->at(i) );	
	Staco_eta_id.push_back( -log( tan(mu_staco_id_theta->at(i)/2) ) );
	Staco_phi_id.push_back( mu_staco_id_phi->at(i) );
	Staco_isocalo20.push_back( mu_staco_etcone20->at(i)/m_GeV );
	Staco_isocalo30.push_back( mu_staco_etcone30->at(i)/m_GeV );
	Staco_isocalo40.push_back( mu_staco_etcone40->at(i)/m_GeV );
	Staco_isotrk20.push_back( mu_staco_ptcone20->at(i)/m_GeV );
	Staco_isotrk30.push_back( mu_staco_ptcone30->at(i)/m_GeV );
	Staco_isotrk40.push_back( mu_staco_ptcone40->at(i)/m_GeV );
	/*cov*/
	Staco_cov_d0.push_back( mu_staco_cov_d0_exPV->at(i) );
	Staco_cov_z0.push_back( mu_staco_cov_z0_exPV->at(i) );
	Staco_cov_phi.push_back( mu_staco_cov_phi_exPV->at(i) );
	Staco_cov_theta.push_back( mu_staco_cov_theta_exPV->at(i) );
	Staco_cov_qoverp.push_back( mu_staco_cov_qoverp_exPV->at(i) );
	Staco_cov_d0_z0.push_back( mu_staco_cov_d0_z0_exPV->at(i) );
	Staco_cov_d0_phi.push_back( mu_staco_cov_d0_phi_exPV->at(i) );
	Staco_cov_d0_theta.push_back( mu_staco_cov_d0_theta_exPV->at(i) );
	Staco_cov_d0_qoverp.push_back( mu_staco_cov_d0_qoverp_exPV->at(i) );
	Staco_cov_z0_phi.push_back( mu_staco_cov_z0_phi_exPV->at(i) );
	Staco_cov_z0_theta.push_back( mu_staco_cov_z0_theta_exPV->at(i) );
	Staco_cov_z0_qoverp.push_back( mu_staco_cov_z0_qoverp_exPV->at(i) );
	Staco_cov_phi_theta.push_back( mu_staco_cov_phi_theta_exPV->at(i) );
	Staco_cov_phi_qoverp.push_back( mu_staco_cov_phi_qoverp_exPV->at(i) );
	Staco_cov_theta_qoverp.push_back( mu_staco_cov_theta_qoverp_exPV->at(i) );
	
	if(m_type==m_MC){
	  Staco_pt_truth.push_back( mu_staco_truth_pt->at(i)/m_GeV );
	  Staco_eta_truth.push_back( mu_staco_truth_eta->at(i) ); 
	  Staco_phi_truth.push_back( mu_staco_truth_phi->at(i) );
	  Staco_E_truth.push_back( mu_staco_truth_E->at(i)/m_GeV );
	  Staco_mother_truth.push_back( mu_staco_truth_mothertype->at(i) );
	  Staco_motherbcode_truth.push_back( mu_staco_truth_motherbarcode->at(i) );
	}
      }
    }
    if(isVertexGood && isTrigGood){
      if(isAuthorCBGood){
	StacoAthCBTag++;
	if( mupT/m_GeV>6.&& fabs(mu_staco_eta->at(i))<2.7 ){
	  StacoPtCBTag++;
	  if(isGoodBlayer){
	    StacoBl++;
	    if(isGoodPixel){
	      StacoPxl++;
	      if(isGoodSCT){
		StacoSCT++;
		if(isGoodHoles){
		  StacoHol++;
		  if(isGoodTRTmuon){
		    StacoTRT++;
		    if(isGoodD0muon && isGoodZ0muon){
		      StacoD0Z0++;
		    }
		  }
		}
	      }
	    }
	  }
	}
      }
      if(isAuthorSAGood){
	StacoAthSA++;
	//if( mu_staco_pt->at(i)/m_GeV>6.&& fabs(mu_staco_eta->at(i))>2.5 &&
	if( mupT/m_GeV>6.&& fabs(mu_staco_eta->at(i))>2.5 &&
	    fabs(mu_staco_eta->at(i))<2.7 ){
	  StacoPtSA++;
	  if(isGoodMSmuonSA){
	    StacoMS++;
	  }
	}
      }
    }
  }
  Print("                   Particles:: CB, Tag and SA muons checked!");
  
}


void Particles::CheckingCaloMuons(bool isVertexGood,
				  bool isTrigGood){
  
  Print("                   Particles:: Checking CALO muons ... ");
  
  int nmuCALO=mu_calo_n;
  m_nmuCALOsel     = 0;
  
  for (int j=0; j<nmuCALO; j++) {
    
    m_MuonSmear->SetSeed(EventNumber,j);
    
    PrintOut(""); PrintEndl(); 
    nbl=0,npixl=0,nscth=0,nhol=0,ntrto=0,ntrth=0;
    
    nbl   = mu_calo_nBLHits->at(j);
    npixl = mu_calo_nPixHits->at(j) + mu_calo_nPixelDeadSensors->at(j); 
    nscth = mu_calo_nSCTHits->at(j) + mu_calo_nSCTDeadSensors->at(j);
    nhol  = mu_calo_nPixHoles->at(j) + mu_calo_nSCTHoles->at(j);
    ntrto = mu_calo_nTRTOutliers->at(j);
    ntrth = mu_calo_nTRTHits->at(j) + mu_calo_nTRTOutliers->at(j);
    
    isGoodTRTmuon=false;
    if(m_year==m_YEAR_2011){
      if( (fabs(mu_calo_eta->at(j)))<0.1 && (ntrth<6 || ntrto<0.9*ntrth) ){
	isGoodTRTmuon=true;
      }
    }
    else{
      isGoodTRTmuon=true;
    }
    
    isAuthorCALOGood=false;
    if( mu_calo_author->at(j)==16 && 
	(mu_calo_caloMuonIdTag->at(j)>10 || mu_calo_caloLRLikelihood->at(j)>0.9) ){
      isAuthorCALOGood=true;
    }
    PrintOut("                  Calo:: Eta = "); PrintOut(mu_calo_eta->at(j));
    PrintOut(" Author = "); PrintOut(mu_calo_author->at(j)); PrintEndl();
    
    isGoodBlayer=false;
    if(m_year==m_YEAR_2011){
      if(!mu_calo_expectBLayerHit->at(j) || nbl>0){
	isGoodBlayer=true;
      }
    }
    else{
      isGoodBlayer=true;
    }
    
    isGoodPixel=false;
    int Npix=0;
    if(m_year==m_YEAR_2011) { Npix=1; }
    if(m_year==m_YEAR_2012) { Npix=0; }
    if(npixl>Npix){
      isGoodPixel=true;
    }
    
    isGoodSCT=false;
    int Nsct=0;
    if(m_year==m_YEAR_2011) { Nsct=5; }
    if(m_year==m_YEAR_2012) { Nsct=4; }
    if(nscth>Nsct){
      isGoodSCT=true;
    }
    
    isGoodHoles=false;
    if(nhol<3){
      isGoodHoles=true;
    }

    isGoodMCPmuon=false;
    if(isGoodBlayer && isGoodPixel && isGoodSCT && 
       isGoodHoles && isGoodTRTmuon){
      isGoodMCPmuon=true;
      PrintOut("                  Calo:: GoodMCP muon Found!"); PrintEndl();
      PrintOut("                         nbl = "); PrintOut(nbl);
      PrintOut(" npixl = "); PrintOut(npixl); PrintOut(" nscth = "); PrintOut(nscth);
      PrintOut(" nhol = "); PrintOut(nhol); PrintOut(" isGoodTRTmuon = "); 
      PrintOut(isGoodTRTmuon); PrintEndl();
    }
    
    isGoodCALOmuon=false;
    double mupT=0;
    if(m_UseCorr){
      //m_MuonSmear->Event(mu_calo_pt->at(j),mu_calo_eta->at(j),"ID");
      m_MuonSmear->Event(mu_calo_pt->at(j),mu_calo_eta->at(j),"ID",
			 mu_calo_charge->at(j),mu_calo_phi->at(j));
      mupT = m_MuonSmear->pTID();
    }
    else{
      mupT = mu_calo_pt->at(j);
    }
    
    double resscf = 1.;
    TLorentzVector Mu;
    Mu.SetPtEtaPhiM(mupT,mu_calo_eta->at(j),
		    mu_calo_phi->at(j),m_Mmuon*m_GeV);
    resscf = m_MuonResoSCF->getResolutionScaleFactor(Mu,2);
    
    PrintOut("                  Calo:: ");
    double d0_v = 0;
    double z0_v = 0;
    if(m_type==m_DATA){
      d0_v = mu_calo_trackd0pvunbiased->at(j);
      z0_v = mu_calo_trackz0pvunbiased->at(j);
    }
    else{
      if(m_year==m_YEAR_2012){
	if(m_UseCorr){
	  double m_etaid = -log( tan(mu_calo_id_theta->at(j)/2) );
	  double m_ptid  = fabs(1./mu_calo_id_qoverp->at(j))/cosh(m_etaid);
	  
	  d0_v = D0Smearing( mu_calo_trackd0pvunbiased->at(j),
			     j,mu_calo_nBLHits->at(j),
			     m_etaid,m_ptid )-0.002;
	  
	  z0_v = Z0Smearing( mu_calo_trackz0pvunbiased->at(j),
			     j,mu_calo_nBLHits->at(j),
			     m_etaid,m_ptid );
	}
	else{
	  d0_v = mu_calo_trackd0pvunbiased->at(j)-0.002;
	  z0_v = mu_calo_trackz0pvunbiased->at(j);
	}
      }
      else if(m_year==m_YEAR_2011){
	d0_v = mu_calo_trackd0pvunbiased->at(j);
	z0_v = mu_calo_trackz0pvunbiased->at(j);
      }
    }
    PrintOut("                         d0 = "); PrintOut(d0_v); PrintEndl();
    PrintOut("                         z0 = "); PrintOut(z0_v); PrintEndl();
    
    isGoodD0muon=false;
    if ( fabs(d0_v)<m_MuonD0Cut ){
      isGoodD0muon=true;
    }

    isGoodZ0muon=false;
    if( fabs(z0_v)<m_MuonZ0Cut ){
      isGoodZ0muon=true;
    }
    
    if( mupT/m_GeV>m_MuonCaloPtCut && fabs(mu_calo_eta->at(j))<0.1 &&
	isAuthorCALOGood && isGoodMCPmuon
	&& isGoodZ0muon && isGoodD0muon){
      isGoodCALOmuon=true;
      PrintOut("                  Calo:: Good CALO muon Found!!"); PrintEndl();
    }
    
    if(isGoodCALOmuon){
      Print("                   Particles:: Removing Calo/Staco muons overlap");
      bool isNotOverlap=true;
      double tmpeta_id=0;
      tmpeta_id=-log( tan( (mu_calo_id_theta->at(j))/2) );
      for(UInt_t c=0; c<CBSTSA_Author.size(); c++){
	double phiC = mu_calo_id_phi->at(j);
	double phiS = mu_staco_id_phi->at(CBSTSA_Index.at(c));
	double etaC = tmpeta_id;
	double etaS = -log( tan(mu_staco_id_theta->at(CBSTSA_Index.at(c))/2) );
	double dR = DeltaR(etaC,phiC,etaS,phiS);
	PrintOut("               DeltaR "); PrintOut(dR); PrintEndl();
	if(dR<0.1){
	  isNotOverlap=false;
	  Print("                   Particles:: Overlap Calo/Staco Found -> Calo muon removed!");
	  break;
	}
      }
      if(isNotOverlap){
	m_nmuCALOsel=m_nmuCALOsel+1;
	Print("                   Particles:: Not Overlap Calo/Staco Found -> Good Calo muon!");

	double scf=1;
	if(m_UseW){
	  if(m_year==m_YEAR_2011){
	    scf = m_CaloSCF->scaleFactor(Mu);
	  }
	  else if(m_year==m_YEAR_2012){
	    scf = m_CaloSCF->scaleFactor(mu_calo_charge->at(j),Mu);
	  }
	}
	
	Calo_Index.push_back( j );
	Calo_Id.push_back( -m_IdMuon*mu_calo_charge->at(j) );
	Calo_Author.push_back( mu_calo_author->at(j) );
	Calo_charge.push_back( mu_calo_charge->at(j) );
	Calo_d0_unbias.push_back( d0_v );
	Calo_scf.push_back( scf );
	Calo_resoscf.push_back( resscf );
	Calo_z0_unbias.push_back( z0_v );
	Calo_E.push_back( mu_calo_E->at(j)/m_GeV );
	Calo_pt.push_back( mupT/m_GeV );
	Calo_eta.push_back( mu_calo_eta->at(j) );
	Calo_phi.push_back( mu_calo_phi->at(j) );
	Calo_SAgood.push_back( isGoodSAMuon );
	Calo_CALOgood.push_back( isGoodCALOmuon );
	
	if(!m_MinParInfo){
	  Calo_isSAmu.push_back( mu_calo_isStandAloneMuon->at(j) );
	  Calo_isSTmu.push_back( mu_calo_isSegmentTaggedMuon->at(j) );
	  Calo_BLYgood.push_back( isGoodBlayer );
	  Calo_PXLgood.push_back( isGoodPixel );
	  Calo_SCTgood.push_back( isGoodSCT );
	  Calo_HOLgood.push_back( isGoodHoles);
	  Calo_TRTgood.push_back( isGoodTRTmuon );
	  Calo_D0good.push_back( isGoodD0muon );
	  Calo_Z0good.push_back( isGoodZ0muon );
	  Calo_MCPgood.push_back( isGoodMCPmuon );
	  Calo_MSgood.push_back( isGoodMSmuonSA );
	  //Calo_OurMSgood.push_back( isGoodOurMSmuonSA );
	  Calo_CBTAGgood.push_back( isGoodCBTagMuon );
	  Calo_n_exblh.push_back( mu_calo_expectBLayerHit->at(j) );
	  Calo_n_blh.push_back( mu_calo_nBLHits->at(j) );  
	  Calo_n_pxlh.push_back( mu_calo_nPixHits->at(j) );
	  Calo_n_pxlds.push_back( mu_calo_nPixelDeadSensors->at(j) );
	  Calo_n_pxlhol.push_back( mu_calo_nPixHoles->at(j) );
	  Calo_n_scth.push_back( mu_calo_nSCTHits->at(j) );
	  Calo_n_sctds.push_back( mu_calo_nSCTDeadSensors->at(j) );
	  Calo_n_scthol.push_back( mu_calo_nSCTHoles->at(j) );
	  Calo_n_hol.push_back( mu_calo_nPixHoles->at(j)+mu_calo_nSCTHoles->at(j) ); 
	  Calo_n_trth.push_back( mu_calo_nTRTHits->at(j) );
	  Calo_n_trto.push_back( mu_calo_nTRTOutliers->at(j) );
	  Calo_n_trtho.push_back( mu_calo_nTRTHits->at(j) + mu_calo_nTRTOutliers->at(j) );
	  Calo_n_mdth.push_back( mu_calo_nMDTHits->at(j) );
	  Calo_n_mdtBIh.push_back( mu_calo_nMDTBIHits->at(j) );
	  Calo_n_mdtBMh.push_back( mu_calo_nMDTBMHits->at(j) );
	  Calo_n_mdtBOh.push_back( mu_calo_nMDTBOHits->at(j) );
	  Calo_n_mdtBEEh.push_back( mu_calo_nMDTBEEHits->at(j) );
	  Calo_n_mdtBIS78h.push_back( mu_calo_nMDTBIS78Hits->at(j) );
	  Calo_n_mdtEIh.push_back( mu_calo_nMDTEIHits->at(j) );
	  Calo_n_mdtEMh.push_back( mu_calo_nMDTEMHits->at(j) );
	  Calo_n_mdtEOh.push_back( mu_calo_nMDTEOHits->at(j) );
	  Calo_n_mdtEEh.push_back( mu_calo_nMDTEEHits->at(j) );
	  Calo_n_cscEtah.push_back( mu_calo_nCSCEtaHits->at(j) );
	  Calo_n_cscPhih.push_back( mu_calo_nCSCPhiHits->at(j) );
	  Calo_n_rpcEtah.push_back( mu_calo_nRPCEtaHits->at(j) );
	  Calo_n_rpcPhih.push_back( mu_calo_nRPCPhiHits->at(j) );
	  Calo_n_tgcEtah.push_back( mu_calo_nTGCEtaHits->at(j) );
	  Calo_n_tgcPhih.push_back( mu_calo_nTGCPhiHits->at(j) );
	  Calo_d0.push_back( mu_calo_d0_exPV->at(j) );
	  Calo_errd0.push_back( mu_calo_cov_d0_exPV->at(j) );
	  Calo_sigd0_unbias.push_back( mu_calo_tracksigd0pvunbiased->at(j) );
	  Calo_z0.push_back( mu_calo_z0_exPV->at(j) );
	  Calo_p.push_back( fabs(1./mu_calo_qoverp_exPV->at(j)/m_GeV) );
	  Calo_px.push_back( mu_calo_px->at(j)/m_GeV );
	  Calo_py.push_back( mu_calo_py->at(j)/m_GeV );
	  Calo_pz.push_back( mu_calo_pz->at(j)/m_GeV );
	  Calo_pid.push_back( fabs(1./mu_calo_id_qoverp->at(j))/m_GeV );
	  Calo_theta.push_back( mu_calo_theta_exPV->at(j) );
	  Calo_theta_id.push_back( mu_calo_id_theta->at(j) );	
	  Calo_eta_id.push_back( -log( tan(mu_calo_id_theta->at(j)/2) ) );
	  Calo_phi_id.push_back( mu_calo_id_phi->at(j) );
	  Calo_isocalo20.push_back( mu_calo_etcone20->at(j)/m_GeV );
	  Calo_isocalo30.push_back( mu_calo_etcone30->at(j)/m_GeV );
	  Calo_isocalo40.push_back( mu_calo_etcone40->at(j)/m_GeV );
	  Calo_isotrk20.push_back( mu_calo_ptcone20->at(j)/m_GeV );
	  Calo_isotrk30.push_back( mu_calo_ptcone30->at(j)/m_GeV );
	  Calo_isotrk40.push_back( mu_calo_ptcone40->at(j)/m_GeV );
	  /*cov*/
	  Calo_cov_d0.push_back( mu_calo_cov_d0_exPV->at(j) );
	  Calo_cov_z0.push_back( mu_calo_cov_z0_exPV->at(j) );
	  Calo_cov_phi.push_back( mu_calo_cov_phi_exPV->at(j) );
	  Calo_cov_theta.push_back( mu_calo_cov_theta_exPV->at(j) );
	  Calo_cov_qoverp.push_back( mu_calo_cov_qoverp_exPV->at(j) );
	  Calo_cov_d0_z0.push_back( mu_calo_cov_d0_z0_exPV->at(j) );
	  Calo_cov_d0_phi.push_back( mu_calo_cov_d0_phi_exPV->at(j) );
	  Calo_cov_d0_theta.push_back( mu_calo_cov_d0_theta_exPV->at(j) );
	  Calo_cov_d0_qoverp.push_back( mu_calo_cov_d0_qoverp_exPV->at(j) );
	  Calo_cov_z0_phi.push_back( mu_calo_cov_z0_phi_exPV->at(j) );
	  Calo_cov_z0_theta.push_back( mu_calo_cov_z0_theta_exPV->at(j) );
	  Calo_cov_z0_qoverp.push_back( mu_calo_cov_z0_qoverp_exPV->at(j) );
	  Calo_cov_phi_theta.push_back( mu_calo_cov_phi_theta_exPV->at(j) );
	  Calo_cov_phi_qoverp.push_back( mu_calo_cov_phi_qoverp_exPV->at(j) );
	  Calo_cov_theta_qoverp.push_back( mu_calo_cov_theta_qoverp_exPV->at(j) );
	
	  if(m_type==m_MC){
	    Calo_pt_truth.push_back( mu_calo_truth_pt->at(j)/m_GeV );
	    Calo_eta_truth.push_back( mu_calo_truth_eta->at(j) ); 
	    Calo_phi_truth.push_back( mu_calo_truth_phi->at(j) );
	    Calo_E_truth.push_back( mu_calo_truth_E->at(j)/m_GeV );
	    Calo_mother_truth.push_back( mu_calo_truth_mothertype->at(j) );
	    Calo_motherbcode_truth.push_back( mu_calo_truth_motherbarcode->at(j) );
	  }
	}
      }
    }
    if(isVertexGood&&isTrigGood){
      if(isAuthorCALOGood){
	CaloAth++;
	//if( mu_calo_pt->at(j)/m_GeV>15 && fabs(mu_calo_eta->at(j))<0.1 ){
	if( mupT/m_GeV>15 && fabs(mu_calo_eta->at(j))<0.1 ){
	  CaloPt++;
	  if(isGoodBlayer){
	    CaloBl++;
	    if(isGoodPixel){
	      CaloPxl++;
	      if(isGoodSCT){
		CaloSCT++;
		if(isGoodHoles){
		  CaloHol++;
		  if(isGoodTRTmuon){
		    CaloTRT++;
		    if(isGoodD0muon && isGoodZ0muon){
		      CaloD0Z0++;
		    }
		  }
		}
	      }
	    }
	  }
	}
      }
    }
  }
  Print("                   Particles:: CALO muons checked!");
  
}

void Particles::FillMuonsInfo(bool isTrigGood,
			      bool isVertexGood){
  
  m_nmusel = 0;
  
  MergeGeneralVec_CBSTSA();
  MergeGeneralVec_Calo();
  m_nmusel = CBSTSA_Author.size()+Calo_Author.size();
  if(isTrigGood&&isVertexGood){
    OverlapCalo += Calo_Author.size();
    OverlapSTSA += CBSTSA_Author.size()+Calo_Author.size();
  }
  
}

void Particles::CheckSTSAmuonOverlap(){

  for(UInt_t mu1=0; mu1<Staco_Index.size(); mu1++){
    bool isSTSAoverlap = false;
    if(mu_staco_isStandAloneMuon->at(Staco_Index.at(mu1))==1){
      double etasa = Staco_eta.at(mu1);
      double phisa = Staco_phi.at(mu1);
      
      for(UInt_t mu2=0; mu2<Staco_Index.size(); mu2++){
	if(mu_staco_isSegmentTaggedMuon->at(Staco_Index.at(mu2))==1){
	  double etast = Staco_eta.at(mu2);
	  double phist = Staco_phi.at(mu2);
	  
	  double dR = DeltaR(etast,phist,etasa,phisa);
	  if(dR<0.2){
	    isSTSAoverlap = true;
	  }
	}
      }
    }
    
    if(!isSTSAoverlap){
      CBSTSA_Index.push_back( Staco_Index[mu1] );
      CBSTSA_Id.push_back( Staco_Id[mu1] );
      CBSTSA_Author.push_back( Staco_Author[mu1] );
      CBSTSA_charge.push_back( Staco_charge[mu1] );
      CBSTSA_d0_unbias.push_back( Staco_d0_unbias[mu1] );
      CBSTSA_scf.push_back( Staco_scf[mu1] );
      CBSTSA_resoscf.push_back( Staco_resoscf[mu1] );
      CBSTSA_z0_unbias.push_back( Staco_z0_unbias[mu1] );
      CBSTSA_E.push_back( Staco_E[mu1] );
      CBSTSA_pt.push_back( Staco_pt[mu1] );
      CBSTSA_eta.push_back( Staco_eta[mu1] );
      CBSTSA_phi.push_back( Staco_phi[mu1] );
      CBSTSA_SAgood.push_back( Staco_SAgood[mu1] );
      CBSTSA_CALOgood.push_back( Staco_CALOgood[mu1] );
      
      if(!m_MinParInfo){
	CBSTSA_isSAmu.push_back( Staco_isSAmu[mu1] );
	CBSTSA_isSTmu.push_back( Staco_isSTmu[mu1] );
	CBSTSA_BLYgood.push_back( Staco_BLYgood[mu1] );
	CBSTSA_PXLgood.push_back( Staco_PXLgood[mu1] );
	CBSTSA_SCTgood.push_back( Staco_SCTgood[mu1] );
	CBSTSA_HOLgood.push_back( Staco_HOLgood[mu1] );
	CBSTSA_TRTgood.push_back( Staco_TRTgood[mu1] );
	CBSTSA_D0good.push_back( Staco_D0good[mu1] );
	CBSTSA_Z0good.push_back( Staco_Z0good[mu1] );
	CBSTSA_MCPgood.push_back( Staco_MCPgood[mu1] );
	CBSTSA_MSgood.push_back( Staco_MSgood[mu1] );
	//CBSTSA_OurMSgood.push_back( Staco_OurMSgood[mu1] );
	CBSTSA_CBTAGgood.push_back( Staco_CBTAGgood[mu1] );
	CBSTSA_n_exblh.push_back( Staco_n_exblh[mu1] );
	CBSTSA_n_blh.push_back( Staco_n_blh[mu1] );
	CBSTSA_n_pxlh.push_back( Staco_n_pxlh[mu1] );
	CBSTSA_n_pxlds.push_back( Staco_n_pxlds[mu1] );
	CBSTSA_n_pxlhol.push_back( Staco_n_pxlhol[mu1] );
	CBSTSA_n_scth.push_back( Staco_n_scth[mu1] );
	CBSTSA_n_sctds.push_back( Staco_n_sctds[mu1] );
	CBSTSA_n_scthol.push_back( Staco_n_scthol[mu1] );
	CBSTSA_n_hol.push_back( Staco_n_hol[mu1] );
	CBSTSA_n_trth.push_back( Staco_n_trth[mu1] );
	CBSTSA_n_trto.push_back( Staco_n_trto[mu1] );
	CBSTSA_n_trtho.push_back( Staco_n_trtho[mu1] );
	CBSTSA_n_mdth.push_back( Staco_n_mdth[mu1] );
	CBSTSA_n_mdtBIh.push_back( Staco_n_mdtBIh[mu1] );
	CBSTSA_n_mdtBMh.push_back( Staco_n_mdtBMh[mu1] );
	CBSTSA_n_mdtBOh.push_back( Staco_n_mdtBOh[mu1] );
	CBSTSA_n_mdtBEEh.push_back( Staco_n_mdtBEEh[mu1] );
	CBSTSA_n_mdtBIS78h.push_back( Staco_n_mdtBIS78h[mu1] );
	CBSTSA_n_mdtEIh.push_back( Staco_n_mdtEIh[mu1] );
	CBSTSA_n_mdtEMh.push_back( Staco_n_mdtEMh[mu1] );
	CBSTSA_n_mdtEOh.push_back( Staco_n_mdtEOh[mu1] );
	CBSTSA_n_mdtEEh.push_back( Staco_n_mdtEEh[mu1] );
	CBSTSA_n_cscEtah.push_back( Staco_n_cscEtah[mu1] );
	CBSTSA_n_cscPhih.push_back( Staco_n_cscPhih[mu1] );
	CBSTSA_n_rpcEtah.push_back( Staco_n_rpcEtah[mu1] );
	CBSTSA_n_rpcPhih.push_back( Staco_n_rpcPhih[mu1] );
	CBSTSA_n_tgcEtah.push_back( Staco_n_tgcEtah[mu1] );
	CBSTSA_n_tgcPhih.push_back( Staco_n_tgcPhih[mu1] );
      	CBSTSA_d0.push_back( Staco_d0[mu1] );
	CBSTSA_errd0.push_back( Staco_errd0[mu1] );
	CBSTSA_sigd0_unbias.push_back( Staco_sigd0_unbias[mu1] );
	CBSTSA_z0.push_back( Staco_z0[mu1] );
	CBSTSA_p.push_back( Staco_p[mu1] );
	CBSTSA_px.push_back( Staco_px[mu1] );
	CBSTSA_py.push_back( Staco_py[mu1] );
	CBSTSA_pz.push_back( Staco_pz[mu1] );
	CBSTSA_pid.push_back( Staco_pid[mu1] );
	CBSTSA_theta.push_back( Staco_theta[mu1] );
	CBSTSA_theta_id.push_back( Staco_theta_id[mu1] );
	CBSTSA_eta_id.push_back( Staco_eta_id[mu1] );
	CBSTSA_phi_id.push_back( Staco_phi_id[mu1] );
	CBSTSA_isocalo20.push_back( Staco_isocalo20[mu1] );
	CBSTSA_isocalo30.push_back( Staco_isocalo30[mu1] );
	CBSTSA_isocalo40.push_back( Staco_isocalo40[mu1] );
	CBSTSA_isotrk20.push_back( Staco_isotrk20[mu1] );
	CBSTSA_isotrk30.push_back( Staco_isotrk30[mu1] );
	CBSTSA_isotrk40.push_back( Staco_isotrk40[mu1] );
	/*cov*/
	CBSTSA_cov_d0.push_back( Staco_cov_d0[mu1] );
	CBSTSA_cov_z0.push_back( Staco_cov_z0[mu1] );
	CBSTSA_cov_phi.push_back( Staco_cov_phi[mu1] );
	CBSTSA_cov_theta.push_back( Staco_cov_theta[mu1] );
	CBSTSA_cov_qoverp.push_back( Staco_cov_qoverp[mu1] );
	CBSTSA_cov_d0_z0.push_back( Staco_cov_d0_z0[mu1] );
	CBSTSA_cov_d0_phi.push_back( Staco_cov_d0_phi[mu1] );
	CBSTSA_cov_d0_theta.push_back( Staco_cov_d0_theta[mu1] );
	CBSTSA_cov_d0_qoverp.push_back( Staco_cov_d0_qoverp[mu1] );
	CBSTSA_cov_z0_phi.push_back( Staco_cov_z0_phi[mu1] );
	CBSTSA_cov_z0_theta.push_back( Staco_cov_z0_theta[mu1] );
	CBSTSA_cov_z0_qoverp.push_back( Staco_cov_z0_qoverp[mu1] );
	CBSTSA_cov_phi_theta.push_back( Staco_cov_phi_theta[mu1] );
	CBSTSA_cov_phi_qoverp.push_back( Staco_cov_phi_qoverp[mu1] );
	CBSTSA_cov_theta_qoverp.push_back( Staco_cov_theta_qoverp[mu1] );
	if(m_type==m_MC){
	  CBSTSA_pt_truth.push_back( Staco_pt_truth[mu1] );
	  CBSTSA_eta_truth.push_back( Staco_eta_truth[mu1] );
	  CBSTSA_phi_truth.push_back( Staco_phi_truth[mu1] );
	  CBSTSA_E_truth.push_back( Staco_E_truth[mu1] );
	  CBSTSA_mother_truth.push_back( Staco_mother_truth[mu1] );
	  CBSTSA_motherbcode_truth.push_back( Staco_motherbcode_truth[mu1] );
	}
      }
    }
  }
  
}

void Particles::MergeGeneralVec_CBSTSA(){
  
  PrintOut("                  Staco:: Merging CBSTSA vectors with general one ..."); PrintEndl();
  
  for(UInt_t st=0; st<CBSTSA_Author.size(); st++){
    Lep_Index.push_back( CBSTSA_Index[st] );
    Lep_Id.push_back( CBSTSA_Id[st] );
    Lep_Author.push_back( CBSTSA_Author[st] );
    Lep_charge.push_back( CBSTSA_charge[st] );
    Lep_d0_unbias.push_back( CBSTSA_d0_unbias[st] );
    Lep_scf.push_back( CBSTSA_scf[st] );
    Lep_resoscf.push_back( CBSTSA_resoscf[st] );
    Lep_z0_unbias.push_back( CBSTSA_z0_unbias[st] );
    Lep_E.push_back( CBSTSA_E[st] );
    Lep_pt.push_back( CBSTSA_pt[st] );
    Lep_eta.push_back( CBSTSA_eta[st] );
    Lep_phi.push_back( CBSTSA_phi[st] );
    Lep_Ereso.push_back( 0 );
    Lep_SAgood.push_back( CBSTSA_SAgood[st] );
    Lep_CALOgood.push_back( CBSTSA_CALOgood[st] );
    Lep_numTrack.push_back( 0 );
      
    if(!m_MinParInfo){
      Mu_Index.push_back( CBSTSA_Index[st] );
      Mu_Id.push_back( CBSTSA_Id[st] );
      Mu_Author.push_back( CBSTSA_Author[st] );
      Mu_charge.push_back( CBSTSA_charge[st] );
      Mu_d0_unbias.push_back( CBSTSA_d0_unbias[st] );
      Mu_scf.push_back( CBSTSA_scf[st] );
      Mu_resoscf.push_back( CBSTSA_resoscf[st] );
      Mu_z0_unbias.push_back( CBSTSA_z0_unbias[st] );
      Mu_pt.push_back( CBSTSA_pt[st] );
      Mu_eta.push_back( CBSTSA_eta[st] );
      Mu_phi.push_back( CBSTSA_phi[st] );
      Mu_isSAmu.push_back( CBSTSA_isSAmu[st] );
      Mu_isSTmu.push_back( CBSTSA_isSTmu[st] );
      Mu_BLYgood.push_back( CBSTSA_BLYgood[st] );
      Mu_PXLgood.push_back( CBSTSA_PXLgood[st] );
      Mu_SCTgood.push_back( CBSTSA_SCTgood[st] );
      Mu_HOLgood.push_back( CBSTSA_HOLgood[st] );
      Mu_TRTgood.push_back( CBSTSA_TRTgood[st] );
      Mu_D0good.push_back( CBSTSA_D0good[st] );
      Mu_Z0good.push_back( CBSTSA_Z0good[st] );
      Mu_MCPgood.push_back( CBSTSA_MCPgood[st] );
      Mu_MSgood.push_back( CBSTSA_MSgood[st] );
      //Mu_OurMSgood.push_back( CBSTSA_OurMSgood[st] );
      Mu_CBTAGgood.push_back( CBSTSA_CBTAGgood[st] );
      Mu_SAgood.push_back( CBSTSA_SAgood[st] );
      Mu_CALOgood.push_back( CBSTSA_CALOgood[st] );
      Mu_n_exblh.push_back( CBSTSA_n_exblh[st] );
      Mu_n_blh.push_back( CBSTSA_n_blh[st] );
      Mu_n_pxlh.push_back( CBSTSA_n_pxlh[st] );
      Mu_n_pxlds.push_back( CBSTSA_n_pxlds[st] );
      Mu_n_pxlhol.push_back( CBSTSA_n_pxlhol[st] );
      Mu_n_scth.push_back( CBSTSA_n_scth[st] );
      Mu_n_sctds.push_back( CBSTSA_n_sctds[st] );
      Mu_n_scthol.push_back( CBSTSA_n_scthol[st] );
      Mu_n_hol.push_back( CBSTSA_n_hol[st] );
      Mu_n_trth.push_back( CBSTSA_n_trth[st] );
      Mu_n_trto.push_back( CBSTSA_n_trto[st] );
      Mu_n_trtho.push_back( CBSTSA_n_trtho[st] );
      Mu_n_mdth.push_back( CBSTSA_n_mdth[st] );
      Mu_n_mdtBIh.push_back( CBSTSA_n_mdtBIh[st] );
      Mu_n_mdtBMh.push_back( CBSTSA_n_mdtBMh[st] );
      Mu_n_mdtBOh.push_back( CBSTSA_n_mdtBOh[st] );
      Mu_n_mdtBEEh.push_back( CBSTSA_n_mdtBEEh[st] );
      Mu_n_mdtBIS78h.push_back( CBSTSA_n_mdtBIS78h[st] );
      Mu_n_mdtEIh.push_back( CBSTSA_n_mdtEIh[st] );
      Mu_n_mdtEMh.push_back( CBSTSA_n_mdtEMh[st] );
      Mu_n_mdtEOh.push_back( CBSTSA_n_mdtEOh[st] );
      Mu_n_mdtEEh.push_back( CBSTSA_n_mdtEEh[st] );
      Mu_n_cscEtah.push_back( CBSTSA_n_cscEtah[st] );
      Mu_n_cscPhih.push_back( CBSTSA_n_cscPhih[st] );
      Mu_n_rpcEtah.push_back( CBSTSA_n_rpcEtah[st] );
      Mu_n_rpcPhih.push_back( CBSTSA_n_rpcPhih[st] );
      Mu_n_tgcEtah.push_back( CBSTSA_n_tgcEtah[st] );
      Mu_n_tgcPhih.push_back( CBSTSA_n_tgcPhih[st] );
      Mu_d0.push_back( CBSTSA_d0[st] );
      Mu_errd0.push_back( CBSTSA_errd0[st] );
      Mu_sigd0_unbias.push_back( CBSTSA_sigd0_unbias[st] );
      Mu_z0.push_back( CBSTSA_z0[st] );
      Mu_p.push_back( CBSTSA_p[st] );
      Mu_px.push_back( CBSTSA_px[st] );
      Mu_py.push_back( CBSTSA_py[st] );
      Mu_pz.push_back( CBSTSA_pz[st] );
      Mu_pid.push_back( CBSTSA_pid[st] );
      Mu_E.push_back( CBSTSA_E[st] );
      Mu_theta.push_back( CBSTSA_theta[st] );
      Mu_theta_id.push_back( CBSTSA_theta_id[st] );
      Mu_eta_id.push_back( CBSTSA_eta_id[st] );
      Mu_phi_id.push_back( CBSTSA_phi_id[st] );
      Mu_isocalo20.push_back( CBSTSA_isocalo20[st] );
      Mu_isocalo30.push_back( CBSTSA_isocalo30[st] );
      Mu_isocalo40.push_back( CBSTSA_isocalo40[st] );
      Mu_isotrk20.push_back( CBSTSA_isotrk20[st] );
      Mu_isotrk30.push_back( CBSTSA_isotrk30[st] );
      Mu_isotrk40.push_back( CBSTSA_isotrk40[st] );
      /*cov*/
      Mu_cov_d0.push_back( CBSTSA_cov_d0[st] );
      Mu_cov_z0.push_back( CBSTSA_cov_z0[st] );
      Mu_cov_phi.push_back( CBSTSA_cov_phi[st] );
      Mu_cov_theta.push_back( CBSTSA_cov_theta[st] );
      Mu_cov_qoverp.push_back( CBSTSA_cov_qoverp[st] );
      Mu_cov_d0_z0.push_back( CBSTSA_cov_d0_z0[st] );
      Mu_cov_d0_phi.push_back( CBSTSA_cov_d0_phi[st] );
      Mu_cov_d0_theta.push_back( CBSTSA_cov_d0_theta[st] );
      Mu_cov_d0_qoverp.push_back( CBSTSA_cov_d0_qoverp[st] );
      Mu_cov_z0_phi.push_back( CBSTSA_cov_z0_phi[st] );
      Mu_cov_z0_theta.push_back( CBSTSA_cov_z0_theta[st] );
      Mu_cov_z0_qoverp.push_back( CBSTSA_cov_z0_qoverp[st] );
      Mu_cov_phi_theta.push_back( CBSTSA_cov_phi_theta[st] );
      Mu_cov_phi_qoverp.push_back( CBSTSA_cov_phi_qoverp[st] );
      Mu_cov_theta_qoverp.push_back( CBSTSA_cov_theta_qoverp[st] );
      
      if(m_type==m_MC){
	Mu_pt_truth.push_back( CBSTSA_pt_truth[st] );
	Mu_eta_truth.push_back( CBSTSA_eta_truth[st] );
	Mu_phi_truth.push_back( CBSTSA_phi_truth[st] );
	Mu_E_truth.push_back( CBSTSA_E_truth[st] );
	Mu_mother_truth.push_back( CBSTSA_mother_truth[st] );
	Mu_motherbcode_truth.push_back( CBSTSA_motherbcode_truth[st] );
      }
    }
  }
  
}

void Particles::MergeGeneralVec_Calo(){
  
  PrintOut("                  Calo:: Merging Calo vectors with general one ..."); PrintEndl();
  for(UInt_t m=0; m<Calo_Author.size(); m++){
    Lep_Index.push_back( Calo_Index[m] );
    Lep_Id.push_back( Calo_Id[m] );
    Lep_Author.push_back( Calo_Author[m] );
    Lep_charge.push_back( Calo_charge[m] );
    Lep_d0_unbias.push_back( Calo_d0_unbias[m] );
    Lep_scf.push_back( Calo_scf[m] );
    Lep_resoscf.push_back( Calo_resoscf[m] );
    Lep_z0_unbias.push_back( Calo_z0_unbias[m] );
    Lep_E.push_back( Calo_E[m] );
    Lep_pt.push_back( Calo_pt[m] );
    Lep_eta.push_back( Calo_eta[m] );
    Lep_phi.push_back( Calo_phi[m] );
    Lep_Ereso.push_back( 0 );
    Lep_SAgood.push_back( Calo_SAgood[m] );
    Lep_CALOgood.push_back( Calo_CALOgood[m] );
    Lep_numTrack.push_back( 0 );
    
    if(!m_MinParInfo){
      Mu_Index.push_back( Calo_Index[m] );
      Mu_Id.push_back( Calo_Id[m] );
      Mu_Author.push_back( Calo_Author[m] );
      Mu_charge.push_back( Calo_charge[m] );
      Mu_d0_unbias.push_back( Calo_d0_unbias[m] );
      Mu_scf.push_back( Calo_scf[m] );
      Mu_resoscf.push_back( Calo_resoscf[m] );
      Mu_z0_unbias.push_back( Calo_z0_unbias[m] );
      Mu_pt.push_back( Calo_pt[m] );
      Mu_eta.push_back( Calo_eta[m] );
      Mu_phi.push_back( Calo_phi[m] );
      Mu_isSAmu.push_back( Calo_isSAmu[m] );
      Mu_isSTmu.push_back( Calo_isSTmu[m] );
      Mu_BLYgood.push_back( Calo_BLYgood[m] );
      Mu_PXLgood.push_back( Calo_PXLgood[m] );
      Mu_SCTgood.push_back( Calo_SCTgood[m] );
      Mu_HOLgood.push_back( Calo_HOLgood[m] );
      Mu_TRTgood.push_back( Calo_TRTgood[m] );
      Mu_D0good.push_back( Calo_D0good[m] );
      Mu_Z0good.push_back( Calo_Z0good[m] );
      Mu_MCPgood.push_back( Calo_MCPgood[m] );
      Mu_MSgood.push_back( Calo_MSgood[m] );
      //Mu_OurMSgood.push_back( Calo_OurMSgood[m] );
      Mu_CBTAGgood.push_back( Calo_CBTAGgood[m] );
      Mu_SAgood.push_back( Calo_SAgood[m] );
      Mu_CALOgood.push_back( Calo_CALOgood[m] );
      Mu_n_exblh.push_back( Calo_n_exblh[m] );
      Mu_n_blh.push_back( Calo_n_blh[m] );
      Mu_n_pxlh.push_back( Calo_n_pxlh[m] );
      Mu_n_pxlds.push_back( Calo_n_pxlds[m] );
      Mu_n_pxlhol.push_back( Calo_n_pxlhol[m] );
      Mu_n_scth.push_back( Calo_n_scth[m] );
      Mu_n_sctds.push_back( Calo_n_sctds[m] );
      Mu_n_scthol.push_back( Calo_n_scthol[m] );
      Mu_n_hol.push_back( Calo_n_hol[m] );
      Mu_n_trth.push_back( Calo_n_trth[m] );
      Mu_n_trto.push_back( Calo_n_trto[m] );
      Mu_n_trtho.push_back( Calo_n_trtho[m] );
      Mu_n_mdth.push_back( Calo_n_mdth[m] );
      Mu_n_mdtBIh.push_back( Calo_n_mdtBIh[m] );
      Mu_n_mdtBMh.push_back( Calo_n_mdtBMh[m] );
      Mu_n_mdtBOh.push_back( Calo_n_mdtBOh[m] );
      Mu_n_mdtBEEh.push_back( Calo_n_mdtBEEh[m] );
      Mu_n_mdtBIS78h.push_back( Calo_n_mdtBIS78h[m] );
      Mu_n_mdtEIh.push_back( Calo_n_mdtEIh[m] );
      Mu_n_mdtEMh.push_back( Calo_n_mdtEMh[m] );
      Mu_n_mdtEOh.push_back( Calo_n_mdtEOh[m] );
      Mu_n_mdtEEh.push_back( Calo_n_mdtEEh[m] );
      Mu_n_cscEtah.push_back( Calo_n_cscEtah[m] );
      Mu_n_cscPhih.push_back( Calo_n_cscPhih[m] );
      Mu_n_rpcEtah.push_back( Calo_n_rpcEtah[m] );
      Mu_n_rpcPhih.push_back( Calo_n_rpcPhih[m] );
      Mu_n_tgcEtah.push_back( Calo_n_tgcEtah[m] );
      Mu_n_tgcPhih.push_back( Calo_n_tgcPhih[m] );
      Mu_d0.push_back( Calo_d0[m] );
      Mu_errd0.push_back( Calo_errd0[m] );
      Mu_sigd0_unbias.push_back( Calo_sigd0_unbias[m] );
      Mu_z0.push_back( Calo_z0[m] );
      Mu_p.push_back( Calo_p[m] );
      Mu_px.push_back( Calo_px[m] );
      Mu_py.push_back( Calo_py[m] );
      Mu_pz.push_back( Calo_pz[m] );
      Mu_pid.push_back( Calo_pid[m] );
      Mu_E.push_back( Calo_E[m] );
      Mu_theta.push_back( Calo_theta[m] );
      Mu_theta_id.push_back( Calo_theta_id[m] );
      Mu_eta_id.push_back( Calo_eta_id[m] );
      Mu_phi_id.push_back( Calo_phi_id[m] );
      Mu_isocalo20.push_back( Calo_isocalo20[m] );
      Mu_isocalo30.push_back( Calo_isocalo30[m] );
      Mu_isocalo40.push_back( Calo_isocalo40[m] );
      Mu_isotrk20.push_back( Calo_isotrk20[m] );
      Mu_isotrk30.push_back( Calo_isotrk30[m] );
      Mu_isotrk40.push_back( Calo_isotrk40[m] );
      /*cov*/
      Mu_cov_d0.push_back( Calo_cov_d0[m] );
      Mu_cov_z0.push_back( Calo_cov_z0[m] );
      Mu_cov_phi.push_back( Calo_cov_phi[m] );
      Mu_cov_theta.push_back( Calo_cov_theta[m] );
      Mu_cov_qoverp.push_back( Calo_cov_qoverp[m] );
      Mu_cov_d0_z0.push_back( Calo_cov_d0_z0[m] );
      Mu_cov_d0_phi.push_back( Calo_cov_d0_phi[m] );
      Mu_cov_d0_theta.push_back( Calo_cov_d0_theta[m] );
      Mu_cov_d0_qoverp.push_back( Calo_cov_d0_qoverp[m] );
      Mu_cov_z0_phi.push_back( Calo_cov_z0_phi[m] );
      Mu_cov_z0_theta.push_back( Calo_cov_z0_theta[m] );
      Mu_cov_z0_qoverp.push_back( Calo_cov_z0_qoverp[m] );
      Mu_cov_phi_theta.push_back( Calo_cov_phi_theta[m] );
      Mu_cov_phi_qoverp.push_back( Calo_cov_phi_qoverp[m] );
      Mu_cov_theta_qoverp.push_back( Calo_cov_theta_qoverp[m] );
      
      if(m_type==m_MC){
	Mu_pt_truth.push_back( Calo_pt_truth[m] );
	Mu_eta_truth.push_back( Calo_eta_truth[m] );
	Mu_phi_truth.push_back( Calo_phi_truth[m] );
	Mu_E_truth.push_back( Calo_E_truth[m] );
	Mu_mother_truth.push_back( Calo_mother_truth[m] );
	Mu_motherbcode_truth.push_back( Calo_motherbcode_truth[m] );
      }
    }
  }
  
}
