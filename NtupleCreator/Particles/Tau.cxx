#include "Particles.h"
#include "../DPDVar/DPDTauVariables.h"

void Particles::ResetTauQualityFlags(){

  isGoodTauPt     = false;
  isGoodTauEta    = false;
  isGoodTauNTrk   = false;
  isGoodTauCharge = false;
  isGoodTauJetBDT = false;
  isGoodTauElVeto = false;
  isGoodTauMuVeto = false;
  isGoodTau       = false;

}

void Particles::CheckingTaus(bool isVertexGood,
			     bool isTrigGood){

  Print("                   Particles:: Checking taus ... ");
  
  int ntau  = tau_n;
  m_ntausel = 0;
  for (int i=0; i<ntau; i++){

    isGoodTauPt=false;
    if(tau_pt->at(i)/m_GeV>m_TauPtCut){
      isGoodTauPt=true;
    }

    isGoodTauEta=false;
    if( fabs(tau_eta->at(i))<2.47 ){
      isGoodTauEta=true;
    }

    isGoodTauNTrk=false;
    if(tau_numTrack->at(i)==1 || tau_numTrack->at(i)==3){
      isGoodTauNTrk=true;
    }

    isGoodTauCharge=false;
    if(fabs(tau_charge->at(i))==1)
      isGoodTauCharge=true;

    isGoodTauJetBDT=false;
    if(tau_JetBDTSigLoose)
      isGoodTauJetBDT=true;

    isGoodTauElVeto=false;
    if(!tau_EleBDTLoose->at(i))
      isGoodTauElVeto=true;

    isGoodTauMuVeto=false;
    if(!tau_muonVeto->at(i))
      isGoodTauMuVeto=true;

    PrintOut("                  Tau:: Eta = "); PrintOut(tau_eta->at(i));
    PrintOut(" Author = "); PrintOut(tau_author->at(i)); 
    PrintOut(" pt = "); PrintOut(tau_pt->at(i)/m_GeV); 
    PrintOut(" Num. Trk = "); PrintOut(tau_numTrack->at(i));
    PrintOut(" Charge = "); PrintOut(tau_charge->at(i)); 
    PrintOut(" JetBDT = "); PrintOut(isGoodTauJetBDT);
    PrintOut(" El. Veto = "); PrintOut(isGoodTauElVeto);
    PrintOut(" Mu. Veto = "); PrintOut(isGoodTauMuVeto); PrintEndl();
    
    isGoodTau=false;
    if(isGoodTauPt     && isGoodTauEta    &&
       isGoodTauNTrk   && isGoodTauCharge &&
       isGoodTauJetBDT && isGoodTauElVeto &&
       isGoodTauMuVeto                     ){
      isGoodTau=true;
      PrintOut("                  Tau:: Good tau found!!");
      PrintEndl();
    }

    if(isGoodTau){
      m_TaIndex.push_back(i);
    }

    if(isVertexGood && isTrigGood){
      if(isGoodTauPt){
	TauPt++;
	if(isGoodTauEta){
	  TauEta++;
	  if(isGoodTauNTrk){
	    TauNTrk++;
	    if(isGoodTauCharge){
	      TauCharge++;
	      if(isGoodTauJetBDT){
		TauJetBDT++;
		if(isGoodTauElVeto){
		  TauElVeto++;
		  if(isGoodTauMuVeto){
		    TauMuVeto++;
		  }
		}
	      }
	    }
	  }
	}
      }
    }
  }
  Print("                   Particles:: Removing taus-taus overlap");
  for(UInt_t t1=0; t1<m_TaIndex.size(); t1++){
    bool ttover=false;
    for(UInt_t t2=0; t2<m_TaIndex.size(); t2++){
      if(t2==t1) continue;
      double phi1 = tau_phi->at(m_TaIndex.at(t1));
      double phi2 = tau_phi->at(m_TaIndex.at(t2));
      double eta1 = tau_eta->at(m_TaIndex.at(t1));
      double eta2 = tau_eta->at(m_TaIndex.at(t2));
      double dR = DeltaR(eta1,phi1,eta2,phi2);

      if(dR<0.1){
	ttover=true;
	break;
      }
    }
    if(ttover)
      TauTauOverlap++;
  }
  
  Print("                   Particles:: Removing taus-muons overlap");
  bool isNot_tmu_Overlap=true;
  for(UInt_t t=0; t<m_TaIndex.size(); t++){
    isNot_tmu_Overlap = ChecktmuOverlap(t);
    if(isNot_tmu_Overlap){
      m_TaIndex_tmuOverlap.push_back(m_TaIndex.at(t));
    }
    if(isVertexGood && isTrigGood && isNot_tmu_Overlap){
      TauMuOverlap++;
    }
  }

  Print("                   Particles:: Removing taus-electrons overlap");

  bool isNot_tel_Overlap=true;
  for(UInt_t t=0; t<m_TaIndex_tmuOverlap.size(); t++){
    isNot_tel_Overlap = ChecktelOverlap(t);
    if(isNot_tel_Overlap){
      m_TaIndex_telOverlap.push_back(m_TaIndex_tmuOverlap.at(t));
    }
    if(isVertexGood && isTrigGood && isNot_tel_Overlap){
      TauElOverlap++;
    }
  }
  m_ntausel=(int)m_TaIndex_telOverlap.size();

  Print("                   Particles:: Filling taus vectors ...");
  for(UInt_t ta=0; ta<m_TaIndex_telOverlap.size(); ta++){
    int taidx=m_TaIndex_telOverlap.at(ta);
    
    Lep_Index.push_back( taidx );
    Lep_Id.push_back( -m_IdTau*tau_charge->at(taidx) );
    Lep_Author.push_back( tau_author->at(taidx) );
    Lep_charge.push_back( tau_charge->at(taidx) );
    Lep_d0_unbias.push_back( 9999 ) ;
    Lep_scf.push_back( 1. );
    Lep_resoscf.push_back( 1 );
    Lep_z0_unbias.push_back( 9999 ); 
    Lep_E.push_back( tau_Et->at(taidx)/m_GeV );
    Lep_pt.push_back( tau_pt->at(taidx)/m_GeV );
    Lep_eta.push_back( tau_eta->at(taidx) );
    Lep_phi.push_back( tau_phi->at(taidx) );
    Lep_Ereso.push_back( 1 );
    Lep_SAgood.push_back( false );
    Lep_CALOgood.push_back( false );
    Lep_numTrack.push_back( tau_numTrack->at(taidx) );

    if(!m_MinParInfo){
      Ta_Index.push_back( taidx );
      Ta_Id.push_back( -m_IdTau*tau_charge->at(taidx) );
      Ta_Author.push_back( tau_author->at(taidx) );
      Ta_Et.push_back( tau_Et->at(taidx)/m_GeV );
      Ta_pt.push_back( tau_pt->at(taidx)/m_GeV );
      Ta_m.push_back( tau_m->at(taidx)/m_GeV );
      Ta_eta.push_back( tau_eta->at(taidx) );
      Ta_phi.push_back( tau_phi->at(taidx) );
      Ta_charge.push_back( tau_charge->at(taidx) );
      Ta_likelihood.push_back( tau_likelihood->at(taidx) );
      Ta_SafeLikelihood.push_back( tau_SafeLikelihood->at(taidx) );
      Ta_electronVetoLoose.push_back( tau_electronVetoLoose->at(taidx) );
      Ta_electronVetoMedium.push_back( tau_electronVetoMedium->at(taidx) );
      Ta_electronVetoTight.push_back( tau_electronVetoTight->at(taidx) );
      Ta_muonVeto.push_back( tau_muonVeto->at(taidx) );
      Ta_tauLlhLoose.push_back( tau_tauLlhLoose->at(taidx) );
      Ta_tauLlhMedium.push_back( tau_tauLlhMedium->at(taidx) );
      Ta_tauLlhTight.push_back( tau_tauLlhTight->at(taidx) );
      Ta_JetBDTSigLoose.push_back( tau_JetBDTSigLoose->at(taidx) );
      Ta_JetBDTSigMedium.push_back( tau_JetBDTSigMedium->at(taidx) );
      Ta_JetBDTSigTight.push_back( tau_JetBDTSigTight->at(taidx) );
      Ta_EleBDTLoose.push_back( tau_EleBDTLoose->at(taidx) );
      Ta_EleBDTMedium.push_back( tau_EleBDTMedium->at(taidx) );
      Ta_EleBDTTight.push_back( tau_EleBDTTight->at(taidx) );
      Ta_numTrack.push_back( tau_numTrack->at(taidx) );
      Ta_BDTJetScore.push_back( tau_BDTJetScore->at(taidx) );
      Ta_BDTEleScore.push_back( tau_BDTEleScore->at(taidx) );
    }
  }
  Print("                   Particles:: Taus checked!");
  
}

bool Particles::ChecktmuOverlap(UInt_t t){

  bool NoOverlap=true;
  for(UInt_t mu=0; mu<Lep_Index.size(); mu++){
    
    if(abs(Lep_Id.at(mu))!=m_IdMuon) continue;

    double phi1 = Lep_phi.at(mu);
    double phi2 = tau_phi->at(m_TaIndex.at(t));
    double eta1 = Lep_eta.at(mu);
    double eta2 = tau_eta->at(m_TaIndex.at(t));
    double dR = DeltaR(eta1,phi1,eta2,phi2);

    PrintOut("                               DeltaR "); PrintOut(dR);
    PrintEndl();
    
    if(dR<0.02){
      Print("                   Particles:: Overlap tau/mu Found -> tau removed!");
      NoOverlap = false;
    }
  }
  return NoOverlap;
  
}

bool Particles::ChecktelOverlap(UInt_t t){

  bool NoOverlap=true;
  for(UInt_t el=0; el<Lep_Index.size(); el++){
    
    if(abs(Lep_Id.at(el))!=m_IdElectron) continue;

    double phi1 = Lep_phi.at(el);
    double phi2 = tau_phi->at(m_TaIndex_tmuOverlap.at(t));
    double eta1 = Lep_eta.at(el);
    double eta2 = tau_eta->at(m_TaIndex_tmuOverlap.at(t));
    double dR = DeltaR(eta1,phi1,eta2,phi2);

    PrintOut("                               DeltaR "); PrintOut(dR);
    PrintEndl();
    
    if(dR<0.02){
      Print("                   Particles:: Overlap tau/el Found -> tau removed!");
      NoOverlap = false;
    }
  }
  return NoOverlap;
  
}
