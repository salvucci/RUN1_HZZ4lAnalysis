#!/bin/bash

echo "Using CERN based machine:: addid CLHEP path ... "
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc48-opt/19.1.0/sw/lcg/external/clhep/1.9.4.7/x86_64-slc6-gcc48-opt/lib
echo "Adding Input files path ... "
export ROOTCOREBIN=Utils/Inputs
python packagesCompile.py
make
python Prepare_InputList.py
