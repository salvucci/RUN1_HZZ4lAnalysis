H4lNtupleCreator - Building information

1) Download needed packages and Create Proof Package:

   ./packagesConf.py

2) Compile needed packages:

   ./packagesCompile.py

3) Compile H4lNtupleCreator

   make


H4lNtupleCreator - Running information

Once built, an executable is provided: H4l

This executable has the following running options

Below the output of the --help command:


   "H4l: run H->ZZ->4l selection!!                                                     "
   "Usage   : ./H4l [Options]							       "
   "Needed Options : 								       "
   "                 --channel data [Channel Name]				       "
   "                 --type data [data or mc type]				       "
   "                 --year 2011 [2011 or 2012]					       "
   "                 --outname data_periodF.root				       "
   "                 --mctype mc11c [MC Version]				       "
   "Other Options  :								       "
   "                 --path /home/salvucci/Lists/ [path to Data/MC list: default is ./]"
   "                 --list Signal.dat [List of files to process:		       "
   "                                       default is 'input.txt']		       "
   "                 --g or --debug [Enable Debug Mode]				       "
   "                 --infoFile nelec.dat [Name Electrons Info Output File]	       "
   "                 --proof lite [Enable Proof Run Mode (lite, pod or pod_panda)]     "
   "                 --notau [Skip tau decay channel]				       "
   "                 --minVar [Use minimal set of variabales]			       "
   "                 --useCorr [Use MC Corrections]				       "
   "                 --useWeight [Use MC Weights]				       "
   "                 --useParMinInfo [Use minimal information for particles]	       "
   "                 --runmode [grid or local: default is local]                       "
   
The program is loading input files from a list, so you need to create a list before.
The default listname is 'input.txt' and the default path is ./ (working directory)

An usual way to run is:

1) locally: 

./H4l --year 2012 --type mc --mctype mc12a --runMode local --channel VBF --outname test.root
      --path ./ --list input.txt --useCorr --useWeight --minVar

2) proof lite:

./H4l --year 2012 --type mc --mctype mc12a --runMode local --channel VBF --outname test.root
      --path ./ --list input.txt --useCorr --useWeight --minVar --proof lite

3) PoD:

./H4l --year 2012 --type mc --mctype mc12a --runMode local --channel VBF --outname test.root
      --path ./ --list input.txt --useCorr --useWeight --minVar --proof pod

4) PoD with Panda:

./H4l --year 2012 --type mc --mctype mc12a --runMode local --channel VBF --outname test.root
      --path ./ --list input.txt --useCorr --useWeight --minVar --proof pod_panda



H4lNtupleCreator - Using a python script for multiple runs

######################## not use at the moment RunH4lNtupleCreator.py ##################
######################## need to change some parameters   ##############################

5) use script (with default path):

   - 4mu channel:

   	data   -> run on data periods and Total
	          ./RunH4lNtupleCreator.py decay=4mu year=2011/2012 type=data
   	mc bkg -> run on MC, background samples
	       	  ./RunH4lNtupleCreator.py decay=4mu year=2011/2012 type=mc simultype=background
   	mc sig -> run on MC, signal samples
	       	  ./RunH4lNtupleCreator.py decay=4mu year=2011/2012 type=mc simultype=signal
   
   - 4e channel:
	
	data   -> run on data periods and Total
	          ./RunH4lNtupleCreator.py decay=4e year=2011/2012 type=data
   	mc bkg -> run on MC, background samples
	       	  ./RunH4lNtupleCreator.py decay=4e year=2011/2012 type=mc simultype=background
   	mc sig -> run on MC, signal samples
	       	  ./RunH4lNtupleCreator.py decay=4e year=2011/2012 type=mc simultype=signal

   - 2mu2e or 2e2mu channel:

     	data   -> run on data periods and Total
	          ./RunH4lNtupleCreator.py decay=2mu2e year=2011/2012 type=data
   	mc bkg -> run on MC, background samples
	       	  ./RunH4lNtupleCreator.py decay=2mu2e year=2011/2012 type=mc simultype=background
   	mc sig -> run on MC, signal samples
	       	  ./RunH4lNtupleCreator.py decay=2mu2e year=2011/2012 type=mc simultype=signal

6) RunH4lNtupleCreator.py script options:

    typing './RunH4lNtupleCreator.py -h' or './RunH4lNtupleCreator.py --help' 
    
    the script will display all options and some examples, reported below:



****************************************************************************************************************************
*    															   *
*    Script to run H4lNtupleCreator over all datasets!									   *
*      Usage   : ./RunH4lNtupleCreator.py [Options]!									   *
*      Needed Options : 												   *
*                       decay=[4mu,4e,2mu2e,2e2mu]      : Higgs decay mode						   *
*                       year=[2011,2012]                : year of data/MC or selection to run				   *
*                       type=[data,mc]                  : type of data							   *
*                       simultype=[signal,background]   : needed if type=mc, typology of MC 				   *
*                       datastream=[Muon,Egamma]        : nedeed if type=data, data Stream				   *
*      Other Options  : 												   *
*                       debug=[True,False]              : enable/disable debug mode					   *
*                       writeinfo=[True,False]          : enable/disable writing information on file:			   *
*                                                       : (Selected Leptons,Selection,CutFlow)				   *
*                       proof=[True,False]              : enable/disable proof mode					   *
*                       path=/tmp/D3PD/                 : specify a different path					   *
*                       singlepath=True/False           : needed if path set and not other sub-directories inside,	   *
*                                                         specify not to loop into directory choosen			   *
*                       channame=JHU_ggH125             : needed if path set, nd not other sub-directories inside,	   *
*                                                         channel's name of data/MC					   *
*                       fileprefix=NTUP                 : needed if path set, prefix of files to create a Chain:	   *
*                                                         if not set, default prefix is 'user'				   *
*                       tauremove=[True,False]          : enable/disable skipping events with taus			   *
*                       minvar=[True,False]             : enable/disable using minimal set of variables		   	   *
*                       corrections=[True,False]        : enable/disable MC corrections (smearing,scale factors,etc)	   *
*                       weights=[True,False]            : enable/disable MC weights (vertex,pileup,trigger,etc)	   	   *
*    															   *
*    Examples:														   *
*       1) Run over data (default path) the 4mu 2011 selection:							   	   *
*              ./RunH4lNtupleCreator.py decay=4mu year=2011 type=data datastream=Muon					   *
*    															   *
*       2) Run over MC (default path) the 4e 2012 selection:								   *
*              ./RunH4lNtupleCreator.py decay=4e year=2012 type=mc simultype=signal					   *
*    															   *
*       3) Run over data the 2mu2e and 2e2mu 2012 selection using and user defined path				   	   *
*          without sub-directories:											   *
*              ./RunH4lNtupleCreator.py decay=2mu2e year=2012 type=data datastream=Muon				   	   *
*              path=/home/salvucci/data/ singlepath=True channame=data fileprefix=slimmed				   *
*    															   *
*       4) Run over MC the 4mu 2011 selection using an user defined path with						   *
*          sub-directories:												   *
*              ./RunH4lNtupleCreator.py decay=4mu year=2011 type=mc simultype=signal					   *
*              path=/home/salvucci/signaMC/ fileprefix=NTUP								   *
*    															   *
*       5) Run over MC (default path) the 4e 2012 analysis enabling MC corrections					   *
*          and weights:												   	   *
*              ./RunH4lNtupleCreator.py decay=4e year=2012 type=mc simultype=signal					   *
*              corrections=True weights=True										   *
* 															   *
****************************************************************************************************************************
