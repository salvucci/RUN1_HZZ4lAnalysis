#ifndef CrossSections_h
#define CrossSections_h
#include <string>

float Cross;
float DecayBr;

void CrossSection(std::string,UInt_t,int,std::string);

float GetCrossSection();

float GetCrossSection(std::string,UInt_t,int,std::string);

float GetH4lBr(double,std::string);

#endif
