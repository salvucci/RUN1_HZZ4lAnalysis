#include <iostream>
#include "CrossSections.h"

void CrossSection(std::string channel, 
		  UInt_t runNumber,
		  int year,
		  std::string decay){
  
  /* All Cross-Sections are in fb */
  
  Cross   = -1.;
  DecayBr = 1.;
  
  if(year==m_YEAR_2011){
    /* Signals */
    
    /* H=120 GeV */
    if(channel=="PowHegPythia_ggH120_ZZ4lep" || runNumber==116763){
      Cross = 2.780;
      DecayBr = GetH4lBr(120,decay);
    }
    else if(channel=="PowHegPythia_VBFH120_ZZ4lep" || runNumber==125065){
      Cross = 0.213;
      DecayBr = GetH4lBr(120,decay);
    }
    else if(channel=="PythiaWH120_ZZ4lep" || runNumber==125267){
      Cross = 0.109;
      DecayBr = GetH4lBr(120,decay);
    }
    else if(channel=="PythiaZH120_ZZ4lep" || runNumber==125427){
      Cross = 0.060;
      DecayBr = GetH4lBr(120,decay);
    }
    /* H=125 GeV */
    else if(channel=="PowHegPythia_ggH125_ZZ4lep" || runNumber==116764){
      Cross = 4.228;
      DecayBr = GetH4lBr(125,decay);
    }
    else if(channel=="PowHegPythia_VBFH125_ZZ4lep" || runNumber==125066){
      Cross = 0.337;
      DecayBr = GetH4lBr(125,decay);
    }
    else if(channel=="PythiaWH125_ZZ4lep" || runNumber==125268){
      Cross = 0.158;
      DecayBr = GetH4lBr(125,decay);
    }
    else if(channel=="PythiaZH125_ZZ4lep" || runNumber==125428){
      Cross = 0.087;
      DecayBr = GetH4lBr(125,decay);
    }
    /* H=130 GeV */
    else if(channel=="PowHegPythia_ggH130_ZZ4lep" || runNumber==116611){
      Cross = 5.862;
      DecayBr = GetH4lBr(130,decay);
    }
    else if(channel=="PowHegPythia_VBFH130_ZZ4lep" || runNumber==116621){
      Cross = 0.483;
      DecayBr = GetH4lBr(130,decay);
    }
    else if(channel=="PythiaWH130_ZZ4lep" || runNumber==125269){
      Cross = 0.207;
      DecayBr = GetH4lBr(130,decay);
    }
    else if(channel=="PythiaZH130_ZZ4lep" || runNumber==125429){
      Cross = 0.115;
      DecayBr = GetH4lBr(130,decay);
    }
    
    /* ZZ Background */
    else if(channel=="PowHegZZ_4mu_trilep5GeV_Pythia" || runNumber==126860){
      Cross = 46.6;
    }
    else if(channel=="PowHegBoxZZeeee_Pythia_mll025_m4l40" || runNumber==126400){
      Cross = -1;
    }
    else if(channel=="PowHegBoxZZeemm_Pythia_mll025_m4l40" || runNumber==126399){
      Cross = -1;
    }
    else if(channel=="Pythiazz4l_3MultiLeptonFilterElecMu" || runNumber==109292){
      Cross = 91.54;
    }
    else if(channel=="gg2ZZ_JIMMY_ZZ4mu" || runNumber==116602){
      Cross = 0.43;
    }
    
    /* Zbb Background */
    else if(channel=="AlpgenHWfZmumubbNp0_4LepM" || runNumber==116965){
      Cross = 21.516*1.4;
    }
    else if(channel=="AlpgenHWfZmumubbNp1_4LepM" || runNumber==116966){
      Cross = 19.6674*1.4;
    }
    else if(channel=="AlpgenHWfZmumubbNp2_4LepM" || runNumber==116967){
      Cross = 10.516*1.4;
    }
    else if(channel=="AlpgenHWfZmumubbNp3_4LepM" || runNumber==116968){
      Cross = 7.93834*1.4;
    }
    else if(channel=="AlpgenHWfZmumubbNp0_Veto4LepM_Pass3Lep" || runNumber==116955){
      Cross = 730.24*1.4;
    }
    else if(channel=="AlpgenHWfZmumubbNp1_Veto4LepM_Pass3Lep" || runNumber==116956){
      Cross = 432.25*1.4;
    }
    else if(channel=="AlpgenHWfZmumubbNp2_Veto4LepM_Pass3Lep" || runNumber==116957){
      Cross = 179.3*1.4;
    }
    else if(channel=="AlpgenHWfZmumubbNp3_Veto4LepM_Pass3Lep" || runNumber==116958){
      Cross = 92.3962*1.4;
    }
    /* Z+jets BAckground */
    else if(channel=="AlpgenJimmyZmumuNp0_pt20" || runNumber==107660){
      Cross = 822125.;
    }
    else if(channel=="AlpgenJimmyZmumuNp1_pt20" || runNumber==107661){
      Cross = 166000.;
    }
    else if(channel=="AlpgenJimmyZmumuNp2_pt20" || runNumber==107662){
      Cross = 49500.;
    }
    else if(channel=="AlpgenJimmyZmumuNp3_pt20" || runNumber==107663){
      Cross = 13875.;
    }
    else if(channel=="AlpgenJimmyZmumuNp4_pt20" || runNumber==107664){
      Cross = 3500.;
    }
    else if(channel=="AlpgenJimmyZmumuNp5_pt20" || runNumber==107665){
      Cross = 1000.;
    }
    /* TTbar Background */
    else if(channel=="T1_McAtNlo_Jimmy" || runNumber==105200){
      Cross = 91550.6;
    }
    /* WZ Background */
    else if(channel=="WZ_Herwig" || runNumber==105987){
      Cross = 11.485;
    }
    /* DY Background */
    else if(channel=="AlpgenJimmyZmumuNp0_Mll10to40" || runNumber==116260){
      Cross = -1;
    }
    else if(channel=="AlpgenJimmyZmumuNp1_Mll10to40" || runNumber==116261){
      Cross = -1;
    }
    else if(channel=="AlpgenJimmyZmumuNp2_Mll10to40" || runNumber==116262){
    Cross = 41428.;
    }
    else if(channel=="AlpgenJimmyZmumuNp3_Mll10to40" || runNumber==116263){
      Cross = 8370.5;
    }
    else if(channel=="AlpgenJimmyZmumuNp4_Mll10to40" || runNumber==116264){
      Cross = 1855.4;
    }
    else if(channel=="AlpgenJimmyZmumuNp5_Mll10to40" || runNumber==116265){
      Cross = 454.3;
    }
    else if(channel=="AlpgenJimmyZmumuNp0_Mll10to40Pt15Pt5" || runNumber==116948){
      Cross = 3055700*0.0373;
    }
    else if(channel=="AlpgenJimmyZmumuNp1_Mll10to40Pt15Pt5" || runNumber==116949){
      Cross = 85070*0.48874;
    }
    else if(channel=="AlpgenJimmyLowMassDYmumubbNp0" || runNumber==128135){
      Cross = 11675.;
    }
    else if(channel=="AlpgenJimmyLowMassDYmumubbNp1" || runNumber==128136){
      Cross = 1856.6;
    }
    else if(channel=="AlpgenJimmyLowMassDYmumubbNp2" || runNumber==128137){
      Cross = 692.62;
    }
    else if(channel=="AlpgenJimmyLowMassDYmumubbNp3" || runNumber==128138){
      Cross = 333.27;
    }
  }
  
  else if(year==m_YEAR_2012){
    
    /* H=120 GeV signal*/
    if(channel=="PowhegPythia8_AU2CT10_ggH120_ZZ4lep" || runNumber==160154){
      Cross = 3.529;
      DecayBr = GetH4lBr(120,decay);
    }
    else if(channel=="PowhegPythia8_AU2CT10_VBFH120_ZZ4lep" || runNumber==160204){
      Cross = 0.275;
      DecayBr = GetH4lBr(120,decay);
    }
    else if(channel=="Pythia8_AU2CTEQ6L1_WH120_ZZ4lep" || runNumber==160254){
      Cross = 0.133;
      DecayBr = GetH4lBr(120,decay);
    }
    else if(channel=="Pythia8_AU2CTEQ6L1_ZH120_ZZ4lep" || runNumber==0000000){
      Cross = 0.075;
      DecayBr = GetH4lBr(120,decay);
    }

    /* H=123 GeV signal*/
    else if(channel=="PowhegPythia8_AU2CT10_ggH123_ZZ4lep" || runNumber==167220){
      Cross = 4.614;
      DecayBr = GetH4lBr(123,decay);
    }
    else if(channel=="PowhegPythia8_AU2CT10_ggH123_ZZ4lep_noTau" || runNumber==167890){
      Cross = 4.614*0.5;
      DecayBr = GetH4lBr(123,decay);
    }
    else if(channel=="PowhegPythia8_AU2CT10_VBFH123_ZZ4lep" || runNumber==167230){
      Cross = 0.368;
      DecayBr = GetH4lBr(123,decay);
    }
    else if(channel=="Pythia8_AU2CTEQ6L1_WH123_ZZ4lep" || runNumber==167240){
      Cross = 0.168;
      DecayBr = GetH4lBr(123,decay);
    }
    else if(channel=="Pythia8_AU2CTEQ6L1_ZH123_ZZ4lep" || runNumber==167250){
      Cross = 0.095;
      DecayBr = GetH4lBr(123,decay);
    }
    
    /* H=124 GeV signal*/
    else if(channel=="PowhegPythia8_AU2CT10_ggH124_ZZ4lep" || runNumber==167222){
      Cross = 4.997;
      DecayBr = GetH4lBr(124,decay);
    }
    else if(channel=="PowhegPythia8_AU2CT10_ggH124_ZZ4lep_noTau" || runNumber==167891){
      Cross = 4.997*0.5;
      DecayBr = GetH4lBr(124,decay);
    }
    else if(channel=="PowhegPythia8_AU2CT10_VBFH124_ZZ4lep" || runNumber==167232){
      Cross = 0.402;
      DecayBr = GetH4lBr(124,decay);
    }
    else if(channel=="Pythia8_AU2CTEQ6L1_WH124_ZZ4lep" || runNumber==167242){
      Cross = 0.180;
      DecayBr = GetH4lBr(124,decay);
    }
    else if(channel=="Pythia8_AU2CTEQ6L1_ZH124_ZZ4lep" || runNumber==167252){
      Cross = 0.102;
      DecayBr = GetH4lBr(124,decay);
    }

    /* H=125 GeV signal*/
    else if(channel=="PowhegPythia8_AU2CT10_ggH125_ZZ4lep" || runNumber==160155){
      Cross = 5.387; 
      DecayBr = GetH4lBr(125,decay);
    }
    else if(channel=="PowhegPythia8_AU2CT10_ggH125_ZZ4lep_noTau" || runNumber==167892){
      Cross = 5.387*0.5; 
      DecayBr = GetH4lBr(125,decay);
    }
    else if(channel=="PowhegPythia8_AU2CT10_VBFH125_ZZ4lep" || runNumber==160205){
      Cross = 0.435;
      DecayBr = GetH4lBr(125,decay);
    }
    else if(channel=="Pythia8_AU2CTEQ6L1_WH125_ZZ4lep" || runNumber==160255){
      Cross = 0.192;
      DecayBr = GetH4lBr(125,decay);
    }
    else if(channel=="Pythia8_AU2CTEQ6L1_ZH125_ZZ4lep" || runNumber==160305){
      Cross = 0.119;
      DecayBr = GetH4lBr(125,decay);
    }
    else if(channel=="JHUPythia8_AU2CTEQ6L1_ggH125_Spin0p_ZZ4lep" || runNumber==167120){
      Cross = 5.387; 
      DecayBr = GetH4lBr(125,decay);
    }
    else if(channel=="JHUPythia8_AU2CTEQ6L1_ggH125_Spin0m_ZZ4lep" || runNumber==167121){
      Cross = 5.387; 
      DecayBr = GetH4lBr(125,decay);
    }
    else if(channel=="JHUPythia8_AU2CTEQ6L1_qqH125_Spin1p_ZZ4lep" || runNumber==169710){
      Cross = 5.387;
      DecayBr = GetH4lBr(125,decay);
    }
    else if(channel=="JHUPythia8_AU2CTEQ6L1_qqH125_Spin1m_ZZ4lep" || runNumber==169711){
      Cross = 5.387;
      DecayBr = GetH4lBr(125,decay);
    }
    else if(channel=="JHUPythia8_AU2CTEQ6L1_ggH125_Spin2p_ZZ4lep" || runNumber==167122){
      Cross = 5.387;
      DecayBr = GetH4lBr(125,decay);
    }
    else if(channel=="JHUPythia8_AU2CTEQ6L1_ggH125_Spin2m_ZZ4lep" || runNumber==167123){
      Cross = 5.387;
      DecayBr = GetH4lBr(125,decay);
    }
    
    /* H=126 GeV signal */
    else if(channel=="PowhegPythia8_AU2CT10_ggH126_ZZ4lep" || runNumber==167225){
      Cross = 5.804;
      DecayBr = GetH4lBr(126,decay);
    }
    else if(channel=="PowhegPythia8_AU2CT10_ggH126_ZZ4lep_noTau" || runNumber==167893){
      Cross = 5.804*0.5;
      DecayBr = GetH4lBr(126,decay);
    }
    else if(channel=="PowhegPythia8_AU2CT10_VBFH126_ZZ4lep" || runNumber==167235){
      Cross = 0.473;
      DecayBr = GetH4lBr(126,decay);
    }
    else if(channel=="Pythia8_AU2CTEQ6L1_WH126_ZZ4lep" || runNumber==167245){
      Cross = 0.205;
      DecayBr = GetH4lBr(126,decay);
    }
    else if(channel=="Pythia8_AU2CTEQ6L1_ZH126_ZZ4lep" || runNumber==167255){
      Cross = 0.116;
      DecayBr = GetH4lBr(126,decay);
    }

    /* H=130 GeV signal*/
    else if(channel=="PowhegPythia8_AU2CT10_ggH130_ZZ4lep" || runNumber==160156){
      Cross = 7.481;
      DecayBr = GetH4lBr(130,decay);
    }
    else if(channel=="PowhegPythia8_AU2CT10_VBFH130_ZZ4lep" || runNumber==160206){
      Cross = 0.625;
      DecayBr = GetH4lBr(130,decay);
    }
    else if(channel=="Pythia8_AU2CTEQ6L1_WH130_ZZ4lep" || runNumber==160256){
      Cross = 0.252;
      DecayBr = GetH4lBr(130,decay);
    }
    else if(channel=="Pythia8_AU2CTEQ6L1_ZH130_ZZ4lep" || runNumber==160306){
      Cross = 0.144;
      DecayBr = GetH4lBr(130,decay);
    }
    
    /* ZZ Background*/
    else if(channel=="PowhegPythia8_AU2CT10_ZZ_4e_mll4_2pt5" || runNumber==126937){
      Cross = 69.75;
    }
    else if(channel=="PowhegPythia8_AU2CT10_ZZ_2e2mu_mll4_2pt5" || runNumber==126938){
      Cross = 145.37;
    }
    else if(channel=="PowhegPythia8_AU2CT10_ZZ_2e2tau_mll4_2pt5" || runNumber==126939){
      Cross = 103.06;
    }
    else if(channel=="PowhegPythia8_AU2CT10_ZZ_4mu_mll4_2pt5" || runNumber==126940){
      Cross = 69.75;
    }
    else if(channel=="PowhegPythia8_AU2CT10_ZZ_2mu2tau_mll4_2pt5" || runNumber==126941){
      Cross = 103.06;
    }
    else if(channel=="PowhegPythia8_AU2CT10_ZZ_4tau_mll4_2pt5" || runNumber==126942){
      Cross = 8.15;
    }
    else if(channel=="PowhegPythia8_AU2CT10_ZZ_4e_m4l100_150_mll4_4pt3" || runNumber==167162){
      Cross = 5.22;
    }
    else if(channel=="PowhegPythia8_AU2CT10_ZZ_2e2mu_m4l100_150_mll4_4pt3" || runNumber==167163){
      Cross = 10.69;
    }
    else if(channel=="PowhegPythia8_AU2CT10_ZZ_4mu_m4l100_150_mll4_4pt3" || runNumber==167165){
      Cross = 5.45;
    }
    else if(channel=="PowhegPythia8_AU2CT10_ZZ_4e_m4l500_50000_mll4_4pt3" || runNumber==169690){
      Cross = 0.42;
    }
    else if(channel=="PowhegPythia8_AU2CT10_ZZ_2e2mu_m4l500_50000_mll4_4pt3" || runNumber==169691){
      Cross = 0.84;
    }
    else if(channel=="PowhegPythia8_AU2CT10_ZZ_4mu_m4l500_50000_mll4_4pt3" || runNumber==169692){
      Cross = 0.45;
    }
    else if(channel=="gg2ZZJimmy_AUET2CT10_ZZ4e" || runNumber==116601){
      Cross = 1.345/2.;
    }
    else if(channel=="gg2ZZJimmy_AUET2CT10_ZZ4mu" || runNumber==116602){
      Cross = 1.345/2.;
    }
    else if(channel=="gg2ZZJimmy_AUET2CT10_ZZ2e2mu" || runNumber==116603){
      Cross = 1.345;
    }
    else if(channel=="Sherpa_CT10_llll_ZZ_EW6_noHiggs" || runNumber==161988){
      Cross = 0.77; //check
    }
    
    /*TTbar Background*/
    else if(channel=="McAtNloJimmy_CT10_ttbar_LeptonFilter" || runNumber==105200){
      Cross = 238.06e3*0.648;
    }
    else if(channel=="McAtNloJimmy_CT10_ttbar_dilepton" || runNumber==110001){
      Cross = 238.06e3*0.1045;
    }
    else if(channel=="McAtNloJimmy_CT10_ttbar_4LepMass_Mll40GeV12GeV" || runNumber==146369){
      Cross = 238.06e3*0.1045*0.0306;
    }
    
    /*Z-jets Background*/
    else if(channel=="AlpgenJimmy_AUET2CTEQ6L1_ZeeNp0" || runNumber==107650){
      Cross = 712000.*1.23;
    }
    else if(channel=="AlpgenJimmy_AUET2CTEQ6L1_ZeeNp1" || runNumber==107651){
      Cross = 155000.*1.23;
    }
    else if(channel=="AlpgenJimmy_AUET2CTEQ6L1_ZeeNp2" || runNumber==107652){
      Cross = 48800.*1.23;
    }
    else if(channel=="AlpgenJimmy_AUET2CTEQ6L1_ZeeNp3" || runNumber==107653){
      Cross = 14200.*1.23;
    }
    else if(channel=="AlpgenJimmy_AUET2CTEQ6L1_ZeeNp4" || runNumber==107654){
      Cross = 3770.*1.23;
    }
    else if(channel=="AlpgenJimmy_AUET2CTEQ6L1_ZeeNp5" || runNumber==107655){
      Cross = 1120.*1.23;
    }
    else if(channel=="AlpgenJimmy_AUET2CTEQ6L1_ZmumuNp0" || runNumber==107660){
      Cross = 712000.*1.23;
    }
    else if(channel=="AlpgenJimmy_AUET2CTEQ6L1_ZmumuNp1" || runNumber==107661){
      Cross = 155000.*1.23;
    }
    else if(channel=="AlpgenJimmy_AUET2CTEQ6L1_ZmumuNp2" || runNumber==107662){
      Cross = 48800.*1.23;
    }
    else if(channel=="AlpgenJimmy_AUET2CTEQ6L1_ZmumuNp3" || runNumber==107663){
      Cross = 14200.*1.23;
    }
    else if(channel=="AlpgenJimmy_AUET2CTEQ6L1_ZmumuNp4" || runNumber==107664){
      Cross = 3770.*1.23;
    }
    else if(channel=="AlpgenJimmy_AUET2CTEQ6L1_ZmumuNp5" || runNumber==107665){
      Cross = 1120.*1.23;
    }
    else if(channel=="AlpgenJimmy_AUET2CTEQ6L1_ZtautauNp0" || runNumber==107670){
      Cross = 712000.*1.23;
    }
    else if(channel=="AlpgenJimmy_AUET2CTEQ6L1_ZtautauNp1" || runNumber==107671){
      Cross = 155000.*1.23;
    }
    else if(channel=="AlpgenJimmy_AUET2CTEQ6L1_ZtautauNp2" || runNumber==107672){
      Cross = 48800.*1.23;
    }
    else if(channel=="AlpgenJimmy_AUET2CTEQ6L1_ZtautauNp3" || runNumber==107673){
      Cross = 14200.*1.23;
    }
    else if(channel=="AlpgenJimmy_AUET2CTEQ6L1_ZtautauNp4" || runNumber==107674){
      Cross = 3770.*1.23;
    }
    else if(channel=="AlpgenJimmy_AUET2CTEQ6L1_ZtautauNp5" || runNumber==107675){
      Cross = 1120.*1.23;
    }
    else if(channel=="AlpgenJimmy_Auto_AUET2CTEQ6L1_ZeeNp0Excl_Mll10to60" || runNumber==146830){
      Cross = 3.48e6*1.19;
    }
    else if(channel=="AlpgenJimmy_Auto_AUET2CTEQ6L1_ZeeNp1Excl_Mll10to60" || runNumber==146831){
      Cross = 0.11e6*1.19;
    }
    else if(channel=="AlpgenJimmy_Auto_AUET2CTEQ6L1_ZeeNp2Excl_Mll10to60" || runNumber==146832){
      Cross = 0.0523e6*1.19;
    }
    else if(channel=="AlpgenJimmy_Auto_AUET2CTEQ6L1_ZeeNp3Excl_Mll10to60" || runNumber==146833){
      Cross = 0.0113e6*1.19;
    }
    else if(channel=="AlpgenJimmy_Auto_AUET2CTEQ6L1_ZeeNp4Excl_Mll10to60" || runNumber==146834){
      Cross = 2.59e3*1.19;
    }
    else if(channel=="AlpgenJimmy_Auto_AUET2CTEQ6L1_ZmumuNp0Excl_Mll10to60" || runNumber==146840){
      Cross = 3.48e6*1.19;
    }
    else if(channel=="AlpgenJimmy_Auto_AUET2CTEQ6L1_ZmumuNp1Excl_Mll10to60" || runNumber==146841){
      Cross = 0.11e6*1.19;
    }
    else if(channel=="AlpgenJimmy_Auto_AUET2CTEQ6L1_ZmumuNp2Excl_Mll10to60" || runNumber==146842){
      Cross = 0.0523e6*1.19;
    }
    else if(channel=="AlpgenJimmy_Auto_AUET2CTEQ6L1_ZmumuNp3Excl_Mll10to60" || runNumber==146843){
      Cross = 0.0113e6*1.19;
    }
    else if(channel=="AlpgenJimmy_Auto_AUET2CTEQ6L1_ZmumuNp4Excl_Mll10to60" || runNumber==146844){
      Cross = 2.59e3*1.19;
    }
    else if(channel=="AlpgenJimmy_Auto_AUET2CTEQ6L1_ZtautauNp0Excl_Mll10to60" || runNumber==146850){
      Cross = 3.48e6*1.19;
    }
    else if(channel=="AlpgenJimmy_Auto_AUET2CTEQ6L1_ZtautauNp1Excl_Mll10to60" || runNumber==146851){
      Cross = 0.11e6*1.19;
    }
    else if(channel=="AlpgenJimmy_Auto_AUET2CTEQ6L1_ZtautauNp2Excl_Mll10to60" || runNumber==146852){
      Cross = 0.0523e6*1.19;
    }
    else if(channel=="AlpgenJimmy_Auto_AUET2CTEQ6L1_ZtautauNp3Excl_Mll10to60" || runNumber==146853){
      Cross = 0.0113e6*1.19;
    }
    else if(channel=="AlpgenJimmy_Auto_AUET2CTEQ6L1_ZtautauNp4Excl_Mll10to60" || runNumber==146854){
      Cross = 2.59e3*1.19;
    }
    
    /* Z Background*/
    else if(channel=="PowhegPythia8_AU2CT10_Zmumu" || runNumber==147807){
      Cross = 1.109e6*1.04;
    }
    else if(channel=="PowhegPythia8_AU2CT10_Zee" || runNumber==147806){
      Cross = 1.109e6*1.04;
    }

    /* Zbb Background*/
    else if(channel=="AlpgenJimmy_Auto_AUET2CTEQ6L1_4lFilter_ZbbeeNp0" || runNumber==146980){
      Cross = 1.6*30.2473;
    }										                     
    else if(channel=="AlpgenJimmy_Auto_AUET2CTEQ6L1_4lFilter_ZbbeeNp1" || runNumber==146981){
      Cross = 1.6*25.9125;
    }										                      
    else if(channel=="AlpgenJimmy_Auto_AUET2CTEQ6L1_4lFilter_ZbbeeNp2" || runNumber==146982){
      Cross = 1.6*15.0649;
    }										                      
    else if(channel=="AlpgenJimmy_Auto_AUET2CTEQ6L1_4lFilter_ZbbmumuNp0" || runNumber==146985){
      Cross = 1.6*30.4634;
    }										                      
    else if(channel=="AlpgenJimmy_Auto_AUET2CTEQ6L1_4lFilter_ZbbmumuNp1" || runNumber==146986){
      Cross = 1.6*26.0188;
    }										                      
    else if(channel=="AlpgenJimmy_Auto_AUET2CTEQ6L1_4lFilter_ZbbmumuNp2" || runNumber==146987){
      Cross = 1.6*14.9299;
    }										                      
    else if(channel=="AlpgenJimmy_Auto_AUET2CTEQ6L1_3lFilter_4lVeto_ZbbeeNp0" || runNumber==146990){
      Cross = 1.6*749.9056;
    }										                    
    else if(channel=="AlpgenJimmy_Auto_AUET2CTEQ6L1_3lFilter_4lVeto_ZbbeeNp1" || runNumber==146991){
      Cross = 1.6*449.5598;
    }										                    
    else if(channel=="AlpgenJimmy_Auto_AUET2CTEQ6L1_3lFilter_4lVeto_ZbbeeNp2" || runNumber==146992){
      Cross = 1.6*192.7407;
    }										                      
    else if(channel=="AlpgenJimmy_Auto_AUET2CTEQ6L1_3lFilter_4lVeto_ZbbmumuNp0" || runNumber==146995){
      Cross = 1.6*752.9395;
    }										                    
    else if(channel=="AlpgenJimmy_Auto_AUET2CTEQ6L1_3lFilter_4lVeto_ZbbmumuNp1" || runNumber==146996){
      Cross = 1.6*449.7719;
    }										                    
    else if(channel=="AlpgenJimmy_Auto_AUET2CTEQ6L1_3lFilter_4lVeto_ZbbmumuNp2" || runNumber==146997){
      Cross = 1.6*192.7161; 
    }

    //else{
    //  std::cout<<"WARNING: samples NOT FOUND!!"<<std::endl;
    //}
    
  }
  
  //if(Cross<0){
  //  std::cout<<"WARNING: No USEFUL cross-section value"<<std::endl;
  //  std::cout<<"            for channel "<<channel<<std::endl;
  //}
  //else{
  //  if(channel!="" && runNumber!=0)
  //    std::cout<<"  Cross-Section for channel "<<channel
  //	       <<" ("<<runNumber<<") is "<<Cross*DecayBr
  //	       <<" fb"<<std::endl; 
  //  else if(channel!="")
  //    std::cout<<"  Cross-Section for channel "<<channel
  //	       <<" is "<<Cross*DecayBr<<" fb"<<std::endl;
  //  else if(runNumber!=0)
  //    std::cout<<"  Cross-Section for RunNumber "<<runNumber
  //	       <<" is "<<Cross*DecayBr<<" fb"<<std::endl;
  //}
}


float GetCrossSection(std::string channel,
		      UInt_t runNumber,
		      int year,
		      std::string decay){
  if(year!=0){
    if(runNumber!=0 && channel!=""){
      CrossSection(channel,runNumber,year,decay);
    }
    else if(channel!=""){
      CrossSection(channel,0,year,decay);
    }
    else if(runNumber!=0){
      CrossSection("",runNumber,year,decay);
    }
  }
  else{
    std::cerr<<"CrossSections ERROR:: NO YEAR SPECIFIED!!"<<std::endl;
  }
    
  return Cross*DecayBr;
}


float GetCrossSection(){
  return Cross*DecayBr;
}

float GetH4lBr(double Hm,
	       std::string dcy){

  float BR = 1.;

  if(Hm==120){
    if( dcy=="4mu" || dcy=="4e" ){ 
      BR = 1.08323;
    }
    else if( dcy=="2mu2e" || dcy=="2e2mu" ){
      BR = 0.959281;
    }
  }
  
  else if(Hm==123){
    if( dcy=="4mu" || dcy=="4e" ){ 
      BR = 1.069;
    }
    else if( dcy=="2mu2e" || dcy=="2e2mu" ){
      BR = 0.962882;
    }
  }
  
  else if(Hm==124){
    if( dcy=="4mu" || dcy=="4e" ){ 
      BR = 1.06786;
    }
    else if( dcy=="2mu2e" || dcy=="2e2mu" ){
      BR = 0.964286;
    }
  }
  
  else if(Hm==125){
    if( dcy=="4mu" || dcy=="4e" ){ 
      BR = 1.0663;
    }
    else if( dcy=="2mu2e" || dcy=="2e2mu" ){
      BR = 0.966848;
    }
  }
  else if(Hm==126){
    if( dcy=="4mu"   || dcy=="4e" ){ 
      BR = 1.06093;
    }
    else if( dcy=="2mu2e" || dcy=="2e2mu" ){
      BR = 0.967053;
    }
  }
  
  else if(Hm==130){
    if( dcy=="4mu" || dcy=="4e" ){ 
      BR = 1.05435;
    }
    else if( dcy=="2mu2e" || dcy=="2e2mu" ){
      BR = 0.973913;
    }
  }

  return BR;

}
