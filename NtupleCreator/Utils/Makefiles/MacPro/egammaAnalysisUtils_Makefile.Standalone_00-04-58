#
# Include the architecture definitions from the ROOT sources
#   code taken from:
#   PhysicsAnalysis/AnalysisCommon/PileupReweighting/tags/PileupReweighting-00-00-15/cmt/Makefile.Standalone
#
#  Makefile.arch can be in two different locations depending on the system
#  you're compiling on. The Fink installed version of ROOT has this file
#  in a different location than the "normally installed" ROOT versions...
#
ARCH_LOC_1 := $(wildcard $(shell root-config --prefix)/test/Makefile.arch)
ARCH_LOC_2 := $(wildcard $(shell root-config --prefix)/share/root/test/Makefile.arch)
ARCH_LOC_3 := $(wildcard $(shell root-config --prefix)/share/doc/root/test/Makefile.arch)
ARCH_LOC_4 := $(wildcard $(shell root-config --prefix)/etc/Makefile.arch)
ifneq ($(strip $(ARCH_LOC_1)),)
  $(info Using $(ARCH_LOC_1))
  include $(ARCH_LOC_1)
else
  ifneq ($(strip $(ARCH_LOC_2)),)
    $(info Using $(ARCH_LOC_2))
    include $(ARCH_LOC_2)
  else
    ifneq ($(strip $(ARCH_LOC_3)),)
      $(info Using $(ARCH_LOC_3))
      include $(ARCH_LOC_3)
    else
      ifneq ($(strip $(ARCH_LOC_4)),)
        $(info Using $(ARCH_LOC_4))
        include $(ARCH_LOC_4)
      else
        $(error Could not find Makefile.arch!)
      endif
    endif
  endif
endif

# -------------------------------------------------------------

PACKAGE    = egammaAnalysisUtils
OUTPUTDIR  = ../StandAlone

# take this from Makefile.arch
#CXXFLAGS   += -fPIC -m64

MFLAGS     = -MM -Wall -W -Woverloaded-virtual
INCLUDES   += -I$(shell root-config --incdir) -I.. -I../Root/ -IegammaAnalysisUtils
LINKLIBS = $(shell root-config --libs)

# for cross-compilation. ATLAS env sets CPPEXPFLAGS if it is needed
# If you want to cross-compile standalone just set CPPEXPFLAGS, for host-slc6&target-slc5 with gcc43 it is -D__USE_XOPEN2K8
ifneq ($(strip $(CPPEXPFLAGS)),)
CXXFLAGS += $(CPPEXPFLAGS)
endif

# -------------------------------------------------------------
# Libraries
# -------------------------------------------------------------
SHLIBFILE  = $(OUTPUTDIR)/libegammaAnalysisUtils.so

ifeq ($(PLATFORM),macosx)
###EXTRALDFLAGS = -install_name @rpath/$(SHLIBFILE)
EXTRALDFLAGS = -install_name $(PACKAGE)/StandAlone/lib$(PACKAGE).so
endif

# get libraries of ROOT
define ldlinksuffixROOT
   $(addsuffix $(LDLINKSUFFIX),$(Lib)) $(shell if [ "$(findstring -Ldlink2,$(OPTIONS))" ]; then echo $(addsuffix _pkgid_$(ROOTVER),$(Lib)); fi)
endef

# -------------------------------------------------------------
# Compilation
# -------------------------------------------------------------

default: shlib
#CCFILES =  BosonPtReweightingTool.cxx      egammaTriggerMatching.cxx      EnergyRescalerUpgrade.cxx    MultiLeptonMenu.cxx
#CCFILES += CaloIsoCorrection.cxx           EisoTool2012.cxx               FsrPhotons.cxx               PhotonEfficiencySFTool.cxx
#CCFILES += CaloPointingCorrectionTool.cxx  EisoTool.cxx                   FudgeMCTool.cxx              PhotonIDTool.cxx
#CCFILES += checkOQ.cxx                     ElectronEfficiencySFTool.cxx   H4l2011Defs.cxx              ProbeQualityCuts.cxx
#CCFILES += ElectronLikelihoodTool.cxx      IsEMForwardDefs.cxx            ShowerDepthTool.cxx          VertexPositionReweightingTool.cxx
#CCFILES += DiphotonVertexIDTool.cxx        ElectronMCChargeCorrector.cxx  IsEMPlusPlusDefs.cxx
#CCFILES += egammaSFclass.cxx               EnergyRescaler.cxx             MultiLeptonDefs_HCP2012.cxx

CCFILES =  BosonPtReweightingTool.cxx      EisoTool.cxx                   FsrPhotons.cxx               ProbeQualityCuts.cxx
CCFILES += ShowerDepthTool.cxx             CaloIsoCorrection.cxx          DiphotonVertexIDTool.cxx     ElectronMCChargeCorrector.cxx  
CCFILES += FudgeMCTool.cxx                 PhotonEfficiencySFTool.cxx     IsEMPlusPlusDefs.cxx         VertexPositionReweightingTool.cxx
CCFILES += CaloPointingCorrectionTool.cxx  egammaTriggerMatching.cxx      EnergyRescaler.cxx           IsEMForwardDefs.cxx   
CCFILES += PhotonIDTool.cxx                checkOQ.cxx                    EisoTool2012.cxx             EnergyRescalerUpgrade.cxx      

#CCFILES += ConvertedPhotonScaleTool.cxx       #other packages required

CCLISTC = $(addprefix ../Root/,$(CCFILES))
CCLISTCXX = $(patsubst %.cxx,%.o,$(CCLISTC))
OFILES = $(patsubst %.C,%.o,$(CCLISTCXX))

# Implicit rule making all dependency Makefiles included at the end of this makefile
%.d: %.cxx $(HLIST)
	@echo "Making $@"
	@set -e; $(CC) $(MFLAGS) $(CXXFLAGS) $< \
		| awk '{ sub("^$(notdir $*).o:","$*.o $@:") ; print }' > $@ ;\
		[ -s $@ ] || rm -f $@

# Implicit rule to compile all classes
%.o : %.cxx
	@echo "Compiling $<"
	@$(CXX) $(CXXFLAGS) $(INCLUDES) -c $< -o $*.o


# Rule to combine objects into a shared library
$(SHLIBFILE): $(OFILES)
	@echo "Linking $(SHLIBFILE)"
	@mkdir -p $(OUTPUTDIR)
	@rm -f $(SHLIBFILE)
	$(CXX) $(CXXFLAGS) $(SOFLAGS) $(LINKLIBS) $(EXTRALDFLAGS) -shared -o $(SHLIBFILE) $^

test_iso_corrections: ../src/examples/test_iso_corrections.cxx
	$(CXX) $(CXXFLAGS) $(LINKLIBS) $(EXTRALDFLAGS) $(INCLUDES) $(CCFLAGS) -legammaAnalysisUtils $< -o ../StandAlone/$@ 

shlib: $(SHLIBFILE)

clean:
	@rm -f ../*/*.o ../*/*.d
	@rm -rf ../StandAlone
