# TrigMuonEfficiency root stand-alone makefile

#
# Include the architecture definitions from the ROOT sources
#
#  Makefile.arch can be in two different locations depending on the system
#  you're compiling on. The Fink installed version of ROOT has this file
#  in a different location than the "normally installed" ROOT versions...
#
ARCH_LOC_1 := $(wildcard $(shell root-config --prefix)/test/Makefile.arch)
ARCH_LOC_2 := $(wildcard $(shell root-config --prefix)/share/root/test/Makefile.arch)
ARCH_LOC_3 := $(wildcard $(shell root-config --prefix)/share/doc/root/test/Makefile.arch)
ARCH_LOC_4 := $(wildcard $(shell root-config --prefix)/etc/Makefile.arch)
ifneq ($(strip $(ARCH_LOC_1)),)
  $(info Using $(ARCH_LOC_1))
  include $(ARCH_LOC_1)
else
  ifneq ($(strip $(ARCH_LOC_2)),)
    $(info Using $(ARCH_LOC_2))
    include $(ARCH_LOC_2)
  else
    ifneq ($(strip $(ARCH_LOC_3)),)
      $(info Using $(ARCH_LOC_3))
      include $(ARCH_LOC_3)
    else
      ifneq ($(strip $(ARCH_LOC_4)),)
        $(info Using $(ARCH_LOC_4))
        include $(ARCH_LOC_4)
      else
        $(error Could not find Makefile.arch!)
      endif
    endif
  endif
endif

# -------------------------------------------------------------
# General flags
# -------------------------------------------------------------
PACKAGE    = TrigMuonEfficiency
OUTPUTDIR  = ../StandAlone

MFLAGS     = -MM -Wall -W -Woverloaded-virtual
INCLUDES   += -I${ROOTSYS}/include -I.. -I../Root -I../TrigMuonEfficiency 
INCLUDES  += -I../../egammaAnalysisUtils -I../../PATCore -I../../ElectronEfficiencyCorrection 

# Need these to avoid loading dependent libraries when ROOT starts
LINKLIBS = -L${ROOTSYS}/lib

CCLIST     = $(wildcard ../Root/*.cxx)

HLIST      = $(wildcard ../TrigMuonEfficiency/*.h)
OLIST      = $(patsubst %.cxx,%.o,$(CCLIST))
DLIST      = $(patsubst %.h,%.d,$(HLIST))

# -------------------------------------------------------------
# Libraries
# -------------------------------------------------------------
SHLIBFILE  = $(OUTPUTDIR)/libTrigMuonEfficiency.so

ifeq ($(PLATFORM),macosx)
EXTRALDFLAGS = -install_name @rpath/$(SHLIBFILE)
endif

# get libraries of ROOT
define ldlinksuffixROOT
   $(addsuffix $(LDLINKSUFFIX),$(Lib)) $(shell if [ "$(findstring -Ldlink2,$(OPTIONS))" ]; then echo $(addsuffix _pkgid_$(ROOTVER),$(Lib)); fi)
endef

# -------------------------------------------------------------
# Compilation
# -------------------------------------------------------------

default: shlib

# Implicit rule making all dependency Makefiles included at the end of this makefile
%.d: %.cxx $(HLIST)
	@echo "Making $@"
	@set -e; $(CC) $(MFLAGS) $(CXXFLAGS) $< \
		| awk '{ sub("^$(notdir $*).o:","$*.o $@:") ; print }' > $@ ;\
		[ -s $@ ] || rm -f $@

# Implicit rule to compile all classes
%.o : %.cxx
	@echo "Compiling $<"
	@$(CXX) $(CXXFLAGS) $(INCLUDES) -c $< -o $*.o 

# Rule to combine objects into a shared library
$(SHLIBFILE): $(OLIST)
	@echo "Linking $(SHLIBFILE)"
	@mkdir -p $(OUTPUTDIR)
	@rm -f $(SHLIBFILE)
	@$(LD) $(CXXFLAGS) $(SOFLAGS) $(LINKLIBS) $(EXTRALDFLAGS) $(OLIST) -o $(SHLIBFILE) 
	@rm -f $(OUTPUTDIR)/TrigMuonEfficiencyLib.so
	@ln -s $(SHLIBFILE) $(OUTPUTDIR)/TrigMuonEfficiencyLib.so 
	
-include $(DLIST)

shlib: $(SHLIBFILE)

clean:
	@rm -f ../*/*.o ../*/*.d ../*/*Cint.*
	@rm -rf $(OUTPUTDIR)