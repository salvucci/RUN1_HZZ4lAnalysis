#!/usr/bin/perl


open FILE_MINE,   "mine_chan3_event.dat" or die $!;
open FILE_ATTILA, "attila_chan3_event.txt" or die $!;
$count_MINE=0;
$count_ATTILA=0;

while(<FILE_MINE>){
    chomp($_);
    push(@event_mine,$_);
}
while(<FILE_ATTILA>){
    chomp($_);
    push(@event_attila,$_);
}

for($i = 0; $i < scalar(@event_mine); $i++){
    print "mine event $i -> @event_mine[$i] \n"
}

for($j = 0; $j < scalar(@event_attila); $j++){
    print "attila event $j -> @event_attila[$j] \n"
}

for($h = 0; $h < scalar(@event_mine); $h++){
    for($hh = 0; $hh < scalar(@event_attila); $hh++){
	if(@event_mine[$h]==@event_attila[$hh]){
	    $count_901++;
	    splice @event_attila, $hh, 1;     
	}
    }
}

print " \n";

for($n = 0; $n < scalar(@event_attila); $n++){
    print "missing event -> @event_attila[$n] \n"
}


close(FILE_MINE);
close(FILE_ATTIL);

## search 901 events selected from Abdollah and not from us
#
#
#for($h = 0; $h < scalar(@event_se_901); $h++){
#	for($hh = 0; $hh < scalar(@ulb_901_event); $hh++){
#		if(@ulb_901_event[$hh] == @event_se_901[$h]){
#                  $count_overlapCheck_901_ULB++;
#                  splice @ulb_901_event, $hh, 1;     
#                  splice @ulb_901_lumi, $hh, 1;     
#                  splice @ulb_901_z, $hh, 1;     
#                  }
#        }
#	for($k = 0; $k < scalar(@ucl_901_event); $k++){
#		if(@ucl_901_event[$k] == @event_se_901[$h]){
#                $count_overlapCheck_901_UCL++;
#                splice @ucl_901_event, $k, 1;     
#                splice @ucl_901_lumi, $k, 1;     
#                splice @ucl_901_z, $k, 1;     
#                }
#        }
#}
#
#for($h = 0; $h < scalar(@event_se_904); $h++){
#        for($hh = 0; $hh < scalar(@ulb_904_event); $hh++){
#                if(@ulb_904_event[$hh] == @event_se_904[$h]){
#                  $count_overlapCheck_904_ULB++;
#                  splice @ulb_904_event, $hh, 1;
#                  splice @ulb_904_lumi, $hh, 1; 
#                  splice @ulb_904_z, $hh, 1;   
#                  }
#        }
#        for($k = 0; $k < scalar(@ucl_904_event); $k++){
#                if(@ucl_904_event[$k] == @event_se_904[$h]){
#                $count_overlapCheck_904_UCL++;
#                splice @ucl_904_event, $k, 1;
#                splice @ucl_904_lumi, $k, 1; 
#                splice @ucl_904_z, $k, 1;   
#                }
#        }
#}
#
#
#
#print "count_overlapCheck_901_ULB: $count_overlapCheck_901_ULB\n";
#print "count_overlapCheck_901_UCL: $count_overlapCheck_901_UCL\n";
#$size_yULBnUCL_901 = scalar(@ulb_901_event);
#$size_yUCLnULB_901 = scalar(@ucl_901_event);
#print "only abdollah 901 events : $size_yULBnUCL_901\n";
#print "only lucia 901 events : $size_yUCLnULB_901\n";
#
#
#print "count_overlapCheck_904_ULB: $count_overlapCheck_904_ULB\n";
#print "count_overlapCheck_904_UCL: $count_overlapCheck_904_UCL\n";
#$size_yULBnUCL_904 = scalar(@ulb_904_event);
#$size_yUCLnULB_904 = scalar(@ucl_904_event);
#print "only abdollah 904 events : $size_yULBnUCL_904\n";
#print "only lucia 904 events : $size_yUCLnULB_904\n";
#
#
#open (OnlyULB_901, '>onlyULB_901.txt');
#open (OnlyUCL_901, '>onlyUCL_901.txt');
#open (OnlyULB_904, '>onlyULB_904.txt');
#open (OnlyUCL_904, '>onlyUCL_904.txt');
#
#for($hh = 0; $hh < scalar(@ulb_901_event); $hh++){
#                print OnlyULB_901 "@ulb_901_lumi[$hh] @ulb_901_event[$hh] @ulb_901_z[$hh]\n";
#}
#for($hh = 0; $hh < scalar(@ucl_901_event); $hh++){
#                print OnlyUCL_901 "@ucl_901_lumi[$hh] @ucl_901_event[$hh] @ucl_901_z[$hh]\n";
#}
#for($hh = 0; $hh < scalar(@ulb_904_event); $hh++){
#                print OnlyULB_904 "@ulb_904_lumi[$hh] @ulb_904_event[$hh] @ulb_904_z[$hh]\n";
#}
#for($hh = 0; $hh < scalar(@ucl_904_event); $hh++){
#                print OnlyUCL_904 "@ucl_904_lumi[$hh] @ucl_904_event[$hh] @ucl_901_z[$hh]\n";
#}
#
#close(OnlyULB_901);
#close(OnlyUCL_901);
#close(OnlyULB_904);
#close(OnlyUCL_904);
#
#
#print "901 ULB: $count_ulb_901 - 904 ULB: $count_ulb_904\n";
#print "901 UCL: $count_ucl_901 - 904 UCL: $count_ucl_904\n";
#print "901 ULB=UCL: $count_901 - 904 ULB=UCL: $count_904\n";
#
#$agreement_901 = $count_901/($count_901+$size_yULBnUCL_901+$size_yUCLnULB_901);
#$agreement_904 = $count_904/($count_904+$size_yULBnUCL_904+$size_yUCLnULB_904);
#
#print "901 agreement: $agreement_901\n";
#print "904 agreement: $agreement_904\n";


