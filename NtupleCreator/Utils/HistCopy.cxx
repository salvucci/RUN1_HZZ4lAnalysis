#include "TFile.h"
#include "TH1.h"
#include "TGraphAsymmErrors.h"
#include "TObject.h"
#include "TList.h"
#include "TKey.h"

#include <vector>
#include <iostream>
#include <string>

void HistCopy(std::string InFileName,
	      std::string OutFileName,
	      std::vector<std::string> HList){
  
  TFile *InFile = new TFile(InFileName.c_str());
  TFile *OutFile = new TFile(OutFileName.c_str(),"recreate");
  
  if(InFile){
    std::cout<<"InputFile Found!"<<std::endl;
    for(UInt_t h=0; h<HList.size(); h++){
      std::cout<<"Finding object "<<HList.at(h)<<std::endl;
      TObject* obj = InFile->FindKey( HList[h].c_str() )->ReadObj();
      if(obj){
	std::cout<<"Object Found"<<std::endl;
	OutFile->cd();
      obj->Write();
      }
    }
  }
  InFile->Close();
  OutFile->Close();
}

void ConfHistCopy(){
  
  std::string infile="../PowhegPythia8_AU2CT10_ggH125_2mu2e.root";
  std::string outfile="LeptonEff.root";
  
  std::vector<std::string> list;
  list.push_back("MuReco_pT");
  list.push_back("MuTruth_pT");
  list.push_back("MuReco_eta");
  list.push_back("MuTruth_eta");
  list.push_back("ElReco_eT");
  list.push_back("ElTruth_eT");
  list.push_back("ElReco_eta");
  list.push_back("ElTruth_eta");
  list.push_back("MuEff_pT");
  list.push_back("MuEff_eta");
  list.push_back("ElEff_eT");
  list.push_back("ElEff_eta");
  list.push_back("MuEff_pT_Graph");
  list.push_back("MuEff_eta_Graph");
  list.push_back("ElEff_eT_Graph");
  list.push_back("ElEff_eta_Graph");
  
  HistCopy(infile,outfile,list);
}
